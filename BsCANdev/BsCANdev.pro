#
# Qmake 'pro' file for the BsCANdev subprojects
#
# For Windows:
# to generate a Visual Studio solution file and project files:
#   qmake -tp vc BsCANdev.pro -r
# or,
# using the Qt Visual Studio Add-In, select 'Open Qt Project File..."
#
# For Linux:
#   qmake BsCANdev.pro -r
# to generate a 32-bit version on a 64-bit machine:
#   qmake -spec linux-g++-32 BsCANdev.pro -r
#

TEMPLATE = subdirs

# The subprojects

# Libraries
SUBDIRS += CAN/CANopen/CANopen.pro
SUBDIRS += BsCAN/BsCAN.pro
win32 {
  SUBDIRS += CAN/KvaserInterface/KvaserInterface.pro
  #SUBDIRS += CAN/SystecInterface/SystecInterface.pro
  #SUBDIRS += CAN/PeakInterface/PeakInterface.pro
  SUBDIRS += CAN/NiCanInterface/NiCanInterface.pro
}
unix {
  SUBDIRS += CAN/KvaserInterface/KvaserInterface.pro
  SUBDIRS += CAN/SocketCanInterface/SocketCanInterface.pro
}

# Executables
SUBDIRS += Blocator/Blocator.pro
SUBDIRS += BsCAN3ui/BsCAN3ui.pro
SUBDIRS += bcaliblist/bcaliblist.pro
SUBDIRS += bcalib/bcalib.pro
#SUBDIRS += BsCANtest/BsCANtest.pro
