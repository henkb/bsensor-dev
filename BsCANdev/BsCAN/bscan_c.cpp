#include "bscan.h"

// When used in a DLL, the instantiation of the bscan BsCan object hangs
// on a semaphore 'acquire()' in the CanReadThread instantiation...
// (this is not the case if this is compiled into a static library),
// so now we use a pointer instead and the BsCan object is only instantiated
// when first used; this line was added to all functions:
//   "if( !bscan ) bscan = new BsCan;"
//static BsCan bscan;
static BsCan *bscan = 0;

#ifdef __cplusplus
extern "C" {
#endif
// ----------------------------------------------------------------------------
// Configuration
// ----------------------------------------------------------------------------

MY_LIB int BsCanSetCanPort( int portno )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->setCanPort( portno );
}

MY_LIB int BsCanConnected( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->connected();
}

MY_LIB int BsCanGetConfiguration( int include_offsgains )
{
  if( !bscan ) bscan = new BsCan;
  if( include_offsgains )
    return bscan->getConfiguration( true );
  else
    return bscan->getConfiguration( false );
}

MY_LIB int BsCanBsensorProbe( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorProbe( node_i );
}

MY_LIB int BsCanBsensorProbeInProgress( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorProbeInProgress( node_i );
}

MY_LIB int BsCanBsensorReset( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorReset( node_i );
}

MY_LIB int BsCanBsensorResetInProgress( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorResetInProgress( node_i );
}

MY_LIB int BsCanBsensorProbeAtPowerup( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorProbeAtPowerup( node_i );
}

MY_LIB int BsCanSetBsensorProbeAtPowerup( uint32_t node_i, int enable )
{
  if( !bscan ) bscan = new BsCan;
  if( enable )
    return bscan->setBsensorProbeAtPowerup( node_i, true );
  else
    return bscan->setBsensorProbeAtPowerup( node_i, false );
}

MY_LIB int BsCanTriggerInputEnabled( uint32_t node_i,
                              uint32_t trigger_input )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->triggerInputEnabled( node_i, trigger_input );
}

MY_LIB int BsCanTriggerInputRisingEdge( uint32_t node_i,
                                 uint32_t trigger_input )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->triggerInputRisingEdge( node_i, trigger_input );
}

MY_LIB int BsCanSetTriggerInputEnabled( uint32_t node_i,
                                 uint32_t trigger_input,
                                 int      enable,
                                 int      rising_edge )
{
  if( !bscan ) bscan = new BsCan;
  bool enable_b = false;
  bool rising_edge_b = false;
  if( enable ) enable_b = true;
  if( rising_edge ) rising_edge_b = true;
  return bscan->setTriggerInputEnabled( node_i, trigger_input,
                                       enable_b, rising_edge_b );
}

MY_LIB uint32_t BsCanNodeCount( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->nodeCount();
}

MY_LIB uint32_t BsCanBsensorCount( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorCount( node_i );
}

MY_LIB uint32_t BsCanBsensorStringCount( uint32_t node_i, uint32_t str_no )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorStringCount( node_i, str_no );
}

MY_LIB int BsCanBsensorFirstIndex( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorFirstIndex( node_i );
}

MY_LIB int BsCanNodeId( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->nodeId( node_i );
}

MY_LIB const char * BsCanNodeFirmwareVersion( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->nodeFirmwareVersion( node_i ).c_str();
}

MY_LIB int BsCanBsensorNodeId( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorNodeId( i );
}

MY_LIB int BsCanBsensorRemoteIndex( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorRemoteIndex( i );
}

MY_LIB uint32_t BsCanBsensorIdHi( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorIdHi( i );
}

MY_LIB uint32_t BsCanBsensorIdLo( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorIdLo( i );
}

MY_LIB int BsCanBsensorId( uint32_t i, uint8_t *id )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorId( i, id );
}

MY_LIB uint64_t BsCanBsensorId64( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorId( i );
}

MY_LIB uint64_t BsCanBsensorIdAlias( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorIdAlias( i );
}

MY_LIB int BsCanBsensorOrderedIndex( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorOrderedIndex( i );
}

MY_LIB int BsCanSetBsensorOrderedIndex( uint32_t index,
                                        uint64_t id, int alias )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->setBsensorOrderedIndex( index, id, alias );
}

MY_LIB const char * BsCanBsensorIdString( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string idstr;
  idstr = bscan->bSensorIdString( i );
  return idstr.c_str();
}

MY_LIB int BsCanBsensorOffsets( uint32_t i, uint32_t *offs )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorOffsets( i, offs );
}

MY_LIB int BsCanBsensorGains( uint32_t i, uint32_t *gain )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorGains( i, gain );
}

MY_LIB uint32_t BsCanBsensorErrStatus( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorErrStatus( i );
}

MY_LIB int BsCanBsensorError( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorError();
}

MY_LIB int BsCanGetBsensorErrStatus( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->getBsensorErrStatus( i );
}

MY_LIB int BsCanGetBsensorOffsGains( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->getBsensorOffsGains( i );
}

MY_LIB const char * BsCanConfigSummaryString( int ordered )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string summstr;
  if( ordered )
    summstr = bscan->configSummaryString( true );
  else
    summstr = bscan->configSummaryString( false );
  return summstr.c_str();
}

MY_LIB void BsCanSetCalibConstFile( char *filename )
{
  if( !bscan ) bscan = new BsCan;
  bscan->setCalibConstFile( std::string(filename) );
}

MY_LIB const char * BsCanCalibConstFile( void )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string filestr;
  filestr = bscan->calibConstFile();
  return filestr.c_str();
}

MY_LIB void BsCanSetBsensorsFieldRange( double tesla )
{
  if( !bscan ) bscan = new BsCan;
  bscan->setBsensorsFieldRange( tesla );
}

MY_LIB void BsCanSetBsensorsPre2016Type( int set )
{
  if( !bscan ) bscan = new BsCan;
  if( set )
    bscan->setBsensorsPre2016Type( true );
  else
    bscan->setBsensorsPre2016Type( false );
}

// ----------------------------------------------------------------------------
// Data-acquisition
// ----------------------------------------------------------------------------

MY_LIB void BsCanSetReadoutTriggerSec( int secs )
{
  if( !bscan ) bscan = new BsCan;
  bscan->setReadoutTriggerSec( secs );
}

MY_LIB void BsCanSetReadoutTriggerMsec( int msecs )
{
  if( !bscan ) bscan = new BsCan;
  bscan->setReadoutTriggerMsec( msecs );
}

MY_LIB int BsCanReadoutTriggerMsec( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->readoutTriggerMsec();
}

MY_LIB void BsCanTriggerSingleReadout( void )
{
  if( !bscan ) bscan = new BsCan;
  bscan->triggerSingleReadout();
}

MY_LIB int BsCanBsensorDataAvailable( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDataAvailable( node_i );
}

MY_LIB int BsCanBsensorWaitDataAvailable( uint32_t timeout_ms )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorWaitDataAvailable( timeout_ms );
}

MY_LIB void BsCanBsensorWaitDataAbort( void )
{
  if( !bscan ) bscan = new BsCan;
  bscan->bSensorWaitDataAbort();
}

MY_LIB uint32_t BsCanBsensorDataRaw( int32_t *data, uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDataRaw( data, i );
}

MY_LIB uint32_t BsCanBsensorDataCalibrated( double *data,
                                            int32_t *err,
                                            uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDataCalibrated( data, err, i );
}

MY_LIB uint32_t BsCanBsensorDataHallRaw( int32_t *data )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDataHallRaw( data );
}

MY_LIB uint32_t BsCanBsensorDataTemperature( int32_t *data )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDataTemperature( data );
}

MY_LIB void BsCanBsensorDataRelease( void )
{
  if( !bscan ) bscan = new BsCan;
  bscan->bSensorDataRelease();
}

MY_LIB const char * BsCanDataTimeStampString( int include_date )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string tstr;
  if( include_date )
    tstr = bscan->dataTimeStampString( true );
  else
    tstr = bscan->dataTimeStampString( false );
  return tstr.c_str();
}

MY_LIB long long BsCanDataTimeStampMsec( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->dataTimeStampMsec();
}

// ----------------------------------------------------------------------------
// Various
// ----------------------------------------------------------------------------

MY_LIB int BsCanBsensorAdcCalibrate( uint32_t i,
                                     uint32_t calib_cnt,
                                     double   max_sigma,
                                     double  *sigma,
                                     int      include_tsensor )
{
  if( !bscan ) bscan = new BsCan;
  if( include_tsensor )
    return bscan->bSensorAdcCalibrate( i, calib_cnt, max_sigma, sigma, true );
  else
    return bscan->bSensorAdcCalibrate( i, calib_cnt, max_sigma, sigma, false );
}

MY_LIB int BsCanBsensorSelect( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorSelect( i );
}

MY_LIB int BsCanBsensorDeselect( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->bSensorDeselect( i );
}

MY_LIB uint32_t BsCanNodeErrStatus( uint32_t node_i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->nodeErrStatus( node_i );
}

MY_LIB uint32_t BsCanErrStatus( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->errStatus();
}

MY_LIB void BsCanClearErrStatus( void )
{
  if( !bscan ) bscan = new BsCan;
  bscan->clearErrStatus();
}

MY_LIB const char * BsCanErrString( void )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string errstr;
  errstr = bscan->errString();
  return errstr.c_str();
}

MY_LIB uint32_t BsCanEmgCount( void )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->emgCount();
}

MY_LIB int BsCanEmgClear( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  return bscan->emgClear( i );
}

MY_LIB const char * BsCanEmgTimeStampString( uint32_t i, int include_date )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string tstr;
  if( include_date )
    tstr = bscan->emgTimeStampString( i, true );
  else
    tstr = bscan->emgTimeStampString( i, false );
  return tstr.c_str();
}

MY_LIB const char * BsCanEmgDataString( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string emgstr;
  emgstr = bscan->emgDataString( i );
  return emgstr.c_str();
}

MY_LIB const char * BsCanEmgDescription( uint32_t i )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string emgstr;
  emgstr = bscan->emgDescription( i );
  return emgstr.c_str();
}

MY_LIB const char * BsCanVersion( void )
{
  if( !bscan ) bscan = new BsCan;
  // Static or else 'c_str()' result does not exist anymore outside this call
  static std::string versionstr;
  versionstr = bscan->version();
  return versionstr.c_str();
}

// ----------------------------------------------------------------------------
// Clean-up
// ----------------------------------------------------------------------------

MY_LIB void BsCanClose( void )
{
  if( bscan )
    {
      bscan->stop();
      delete bscan;
      bscan = 0;
    }
}

// ----------------------------------------------------------------------------
#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif
