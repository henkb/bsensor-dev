#ifndef CALIBCALC_H
#define CALIBCALC_H

#include <string>
#include <vector>

class CalibCalc
{
public:
  CalibCalc();
  ~CalibCalc();

  void listBsensorIds( bool long_list = true, bool no_spaces = true );

  int calvtb( char   *id,
              int    &i1, int &i2, int &i3,
              double &v4,
              double &b1, double &b2, double &b3,
              int    &info );

  void setFieldRange( int range )                { _fieldRange = range; }
  void setPre2016Type( bool pre2016 )            { _pre2016Type = pre2016; }

  void setCalibConstFile( std::string filename ) { _fileName = filename; }
  std::string calibConstFile()                   { return _fileName; }
  
 private:
  void dsnleq( int     n,
               double *x,
               double *f,
               double  ftol,
               double  xtol,
               int     maxf,
               int     iprt,
               int    *info,
               double *w );
  void   sub1( int n, double *x, double *f, int k );
  double chbv();

  unsigned char   crc8( unsigned char *data, int len );

  // Structure containing one B-sensor's calibration data
  struct SensorCalConst
  {
    char  id[8];
    char  bite1, bite2;
    int   ilen;
    char *calibconst;  // Will be pointed to by iar1 and iar4
  };

  // Vector for storing calibration constants from multiple B-sensors
  std::vector<struct SensorCalConst> _calConst;

  // Help variables with original names as used in the FORTRAN version
  int   *iar4;
  char  *iar1;
  int    ipt1, ipt2, ipt3, ipt4, ipt5;
  double vv1, vv2, vv3, vv4;

  // Range for which sensors were calibrated, in units of 0.1T
  // (valid values are: 1.4, 2.5 and 4.5 Tesla)
  int _fieldRange;

  // Whether the conversion calculation is for pre-year-2016 B-sensor types
  // (different hall-sensor orientations)
  bool _pre2016Type;

  // Calibration constants file name
  std::string _fileName;
};

#endif // CALIBCALC_H
