/* -------------------------------------------------------------------------
FILE   : calibcalc.cpp

DESCR  : Class to retrieve a B-sensor's calibration constants and to calculate
         a calibrated B-field reading from the provided raw voltage readout
         from a B-sensor.
         The code is a one-to-one translation to C/C++ code from the original
         FORTRAN code by Felix Bergsma, translated 'manually' line-by-line.
         The original files are these:
          - calvtb.for
          - dsnleq.for
          - sub1.for
          - chbv.for
          - ilcb.for
         Things to consider when translating FORTRAN to C:
         - Fortran array index starts from 1, C array from 0
         - 2D Fortran array is stored column-wise, 2D C array row-wise,
           which is especially relevant when using a C-pointer to step
           through a 2D array; also in this case (C-pointer to 2D Fortran
           array) the C index starts from 0.
         - in the code below the original Fortran indexing is still kept
           visible, on purpose, to be able to compare the original FORTRAN
           and this C/C++, in case changes are required or bugs to be fixed.

HISTORY:
05MAY12; v1.0.0: First version.
18NOV16; v1.1.0; - Added function listBsensorIds().
                 - Added '_fieldRange' int member, plus 'set' function for it,
                   to account for the field strength used when the sensors
                   were calibrated.
                 - Added '_pre2016Type' bool member, plus 'set' function for it,
                   used to make a distinction between B-sensors with different
                   hall-sensor spatial orientation ('2016' and 'pre-2016' types;
                   e.g. '2016'-type used for ATLAS NSW).
                 - Use _fieldRange and '_pre2016Type' to control the conversion
                   to B-field value (in calvtb() and sub1()).
12APR17; v1.2.0; - Added a couple more calibration head rotation matrices
                   and corresponding 'bite1' values for selection.
                 - Allow multiple calibration constants blocks of the same ID
                   in the same file and only use the last one found.
                 - Untabify source.
27APR17; v1.2.1; - Bug fixes in reading multiple blocks of the same ID.
17JUL17; v1.2.2; - Fix bug in reading final block: check for EOF.
---------------------------------------------------------------------------- */

//#define DEBUG_OUTPUT_

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include <math.h>
#include "calibcalc.h"

// Transformation matrices for calibration head rotational correction
static double RMB[3*3] =
  {
    0.99996873,    -0.0040750209, -0.0067774618,
    0.00411989742,  0.999969596,   0.00662070658,
    0.00675027622, -0.006648422,   0.999955115
  };
static double RMO[3*3] =
  {
    0.999414708,   0.0341141201, -0.00254339253,
   -0.0341744146,  0.998979548,  -0.0295291784,
    0.00153343518, 0.0295988142,  0.999560683
  };
static double RM3[3*3] =
  {
    0.9955,  0.0522,  0.0794,
   -0.0513,  0.9986, -0.0145,
   -0.0800,  0.0104,  0.9967
  };
static double RM5[3*3] =
  {
    0.9987, -0.0492, -0.0148,
    0.0493,  0.9987,  0.0090,
    0.0143, -0.0098,  0.9999
  };
static double RM_NOROT[3*3] =
  {
    1.0,     0.0,     0.0,
    0.0,     1.0,     0.0,
    0.0,     0.0,     1.0
  };

// Constant for member function dsnleq()
static const double EPS = 0.1490116119384766e-07;

// Constants for member function dsnleq()
static const int MPT[288+1] =
  {
    0,1,2,3,3,3,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,9,10,
    10,10,10,10,11,11,11,11,11,12,12,12,12,12,13,13,13,13,13,14,14,
    14,14,14,15,15,15,15,15,15,16,16,16,16,16,16,17,17,17,17,17,18,
    18,18,18,18,18,19,19,19,19,19,19,20,20,20,20,20,20,21,21,21,21,
    21,21,21,22,22,22,22,22,22,23,23,23,23,23,23,24,24,24,24,24,24,
    24,25,25,25,25,25,25,26,26,26,26,26,26,26,27,27,27,27,27,27,28,
    28,28,28,28,28,28,29,29,29,29,29,29,29,30,30,30,30,30,30,30,31,
    31,31,31,31,31,31,32,32,32,32,32,32,32,33,33,33,33,33,33,33,34,
    34,34,34,34,34,34,35,35,35,35,35,35,35,36,36,36,36,36,36,36,37,
    37,37,37,37,37,37,37,38,38,38,38,38,38,38,39,39,39,39,39,39,39,
    40,40,40,40,40,40,40,40,41,41,41,41,41,41,41,42,42,42,42,42,42,
    42,42,43,43,43,43,43,43,43,44,44,44,44,44,44,44,44,45,45,45,45,
    45,45,45,45,46,46,46,46,46,46,46,47,47,47,47,47,47,47,47,48,48,
    48,48,48,48,48,48
  };

// ----------------------------------------------------------------------------

CalibCalc::CalibCalc()
  : iar4(0), iar1(0),
    _fieldRange( 25 ),     // Range: 2.5T
    _pre2016Type( false ), // Latest B-sensor type
    _fileName("caldat.dat")
{
}

// ----------------------------------------------------------------------------

CalibCalc::~CalibCalc()
{
  for( unsigned int i=0; i<_calConst.size(); ++i )
    delete [] _calConst[i].calibconst;
  _calConst.clear();
}

//-----------------------------------------------------------------------------

void CalibCalc::listBsensorIds( bool long_list, bool no_spaces )
{
  if( long_list )
    cout << "Calibration constants B-sensor ID list:" << endl;
  if( _fileName.empty() )
    {
      cout << "### No calibration constants file set" << endl;
      return;
    }
  ifstream fin;
  fin.open( _fileName.c_str(), ios::binary );
  if( !fin.is_open() )
    {
      cout << "### Failed to open calibration constants file: "
           << _fileName << endl;
      return;
    }
  std::vector<struct SensorCalConst> scclist;
  struct SensorCalConst scc;
  scc.calibconst = 0;
  scc.ilen       = 0;
  int   fortran_hdr, fortran_len;
  char  dummyblock[4096];
  int   cnt = 0, double_cnt = 0;
  while( !fin.eof() )
    {
      fin.read( (char *) &fortran_hdr, 4 ); // E0 00 00 00 ?
      if( fin.eof() ) break;
      fin.read( (char *) &scc.bite1, 1 );
      fin.read( (char *) &scc.bite2, 1 );
      fin.read( scc.id, 8 );
      fin.read( (char *) &scc.ilen, 4 );

      ++cnt;
      if( long_list )
        {
          cout << "ID " << setw(3) << cnt << ":"
               << hex << setfill('0') << uppercase;
          if( no_spaces )
            {
              cout << " ";
              for( int i=0; i<8; ++i )
                cout << setw(2) << (unsigned int) (scc.id[i] & 0xFF);
            }
          else
            {
              for( int i=0; i<8; ++i )
                cout << " " << setw(2) << (unsigned int) (scc.id[i] & 0xFF);
            }
          /*
          // Some additional tech information:
          cout << " (bite1/2= " << (((int) scc.bite1) & 0xFF)
          << " " << (((int) scc.bite2) & 0xFF)
          << " ilen=" << dec << scc.ilen << ")";
          */
          cout << dec << setfill(' ') << "  len=" << scc.ilen;
        }

      // Check for doubles
      bool double_found = false;
      unsigned int double_i;
      for( double_i=0; double_i<scclist.size(); ++double_i )
        {
          double_found = true;
          for( int k=0; k<8; ++k )
            if( scc.id[k] != scclist[double_i].id[k] )
              {
                double_found = false;
                break;
              }
          if( double_found )
            break;
        }
      if( double_found )
        {
          ++double_cnt;
          if( long_list )
            cout << " -->ID " << double_i+1;
        }

      // Check ID's CRC
      if( this->crc8( (unsigned char *) scc.id, 8 ) != 0 )
        {
          if( !long_list )
            cout << "ID " << setw(2) << cnt << ":";
          cout << "  ###CRC error" << endl;
        }
      else if( long_list )
        {
          cout << dec << endl;
        }

      scclist.push_back( scc );

      // Skip over calibration constants datablock
      fin.read( (char *) &fortran_hdr, 4 ); // E0 00 00 00 ?
      fin.read( (char *) &fortran_len, 4 );
      fin.read( (char *) &dummyblock[0], scc.ilen );
      fin.read( (char *) &fortran_len, 4 );
      /*
      if( scc.id[0] == 80 )
        {
          cout << "ID 50:" << hex << endl;
          //for( int i=0; i<scc.ilen; ++i )
          for( int i=0; i<500; ++i )
            {
              cout << " " << setw(2) << ((unsigned int) dummyblock[i] & 0xFF);
              if( (i & 0xF) == 0xF ) cout << endl;
            }
          cout << dec << endl;
        }
      */
    }
  cout << "Found " << cnt << " B-sensor IDs ";
  if( double_cnt > 0 )
    cout << "(" << double_cnt << " doubles) ";
  cout << "in file: " << _fileName << endl;
}

//-----------------------------------------------------------------------------

int CalibCalc::calvtb( char   *id,
                       int    &i1, int &i2, int &i3,
                       double &v4,
                       double &b1, double &b2, double &b3,
                       int    &info )
{
  // Initialize the B-field value results
  b1 = 0.0;
  b2 = 0.0;
  b3 = 0.0;

  // See if the ID is already in our stored list of calibration info
  // and if calibration constants for that ID were found
  bool found = false;
  size_t index = 0;
  while( !found && index<_calConst.size() )
    {
      found = true;
      for( int j=0; j<8; ++j )
        if( _calConst[index].id[j] != id[j] )
          {
            found = false;
            break;
          }
      // If not found, go to next calibration constants set in the list
      if( !found ) ++index;
    }

  if( found )
    {
      if( !_calConst[index].calibconst )
        {
          // ID is in our list, but corresponding calibration constants
          // were not found in the calibration constants file
          info = 14;
          return info;
        }
      else
        {
          // This ID plus calibration constants are in our list
#ifdef DEBUG_OUTPUT_
          cout << " ### FOUND in list" << endl;
#endif
          // Set up the calibration constants pointers
          iar1 = &(_calConst[index].calibconst[4]);
          iar4 = (int *) &(_calConst[index].calibconst[0]);
        }
    }
  else
    {
      // Not found in our current list in memory,
      // look for the ID in the calibration constants file
      //char *filename = getenv( "CALDAT" ); No more getenv, Henk 11 May 2012
      if( _fileName.empty() )
        {
          info = 12;
          return info;
        }
#ifdef DEBUG_OUTPUT_
      //cout << "CalibCalc CALDAT=" << filename << endl;
      cout << "CalibCalc _fileName=" << _fileName << endl;
#endif

      ifstream fin;
      fin.open( _fileName.c_str(), ios::binary );
      if( !fin.is_open() )
        {
          info = 13;
          return info;
        }

      // ### Initialization not necessary ?
      //for( int i=0; i<sizeof(iar4)/sizeof(int); ++i ) iar4[i] = 0;
      //for( int i=0; i<sizeof(w)/sizeof(double); ++i ) w[i]    = 0.0;

      struct SensorCalConst scc;
      scc.calibconst = 0;
      scc.ilen       = 0;
      int   fortran_hdr, fortran_len, ilen;
      char  bite1, bite2, id_tmp[8];
      char *datablock = 0;
      char  dummyblock[4096];
      //while( !found && !fin.eof() )
      // Find last constants block for this ID, there may be multiple,
      // so always read until the end-of-file
      bool found_next;
      while( !fin.eof() )
        {
          fin.read( (char *) &fortran_hdr, 4 ); // E0 00 00 00 ?
          if( fin.eof() ) break;
          fin.read( (char *) &bite1, 1 );
          fin.read( (char *) &bite2, 1 );
          fin.read( id_tmp, 8 );
          fin.read( (char *) &ilen, 4 );

          found_next = true;
          for( int i=0; i<8; ++i )
            if( id_tmp[i] != id[i] )
              {
                found_next = false;
                break;
              }
          if( found_next )
            {
              found = true;
              // Copy calibration parameters read sofar into struct 'scc'
              scc.bite1 = bite1;
              scc.bite2 = bite2;
              scc.ilen  = ilen;
              for( int i=0; i<8; ++i )
                scc.id[i] = id_tmp[i];
#ifdef DEBUG_OUTPUT_
              cout << "CalibCalc: ";
              cout << hex << (((int) scc.bite1) & 0xFF)
                   << " " << (((int) scc.bite2) & 0xFF)
                   << " ilen=" << dec << scc.ilen << hex
                   << " ID=";
              cout << setfill('0') << uppercase;
              for( int i=0; i<8; ++i )
                cout << setw(2) << (((int) scc.id[i]) & 0xFF);
              cout << dec << std::endl;
#endif
              // Read calibration constants datablock:
              // Delete possibly previously allocated space
              if( datablock ) delete[] datablock; 
              // Allocate space for calibration constants
              datablock = new char[4+scc.ilen];
              fin.read( (char *) &fortran_hdr, 4 ); // E0 00 00 00 ?
              fin.read( (char *) &fortran_len, 4 );
              fin.read( (char *) &datablock[4], scc.ilen );
              fin.read( (char *) &fortran_len, 4 );

              // Break out of while-loop
              //break; // Outcommented: there may be multiple blocks for the ID
            }
          else
            {
#ifdef DEBUG_OUTPUT_
              //cout << " ilen=" << dec << ilen << hex << endl;
#endif
              // Skip over calibration constants datablock
              fin.read( (char *) &fortran_hdr, 4 ); // E0 00 00 00 ?
              fin.read( (char *) &fortran_len, 4 );
              fin.read( (char *) &dummyblock[0], ilen );
              fin.read( (char *) &fortran_len, 4 );
            }
        }
      // Add this ID (plus calibration constants) to our list
      if( found )
        {
          scc.calibconst = datablock;
          // Set up the calibration constants pointers
          iar1 = &datablock[4];
          iar4 = (int *) &datablock[0];
        }
      // Add this ID to the list (plus its calibration constants if found)
      _calConst.push_back( scc );
      // This is the index of the newly added list item
      index = _calConst.size() - 1;
    }

  if( !found )
    {
      // ID not found in our list or in the calibration constants file
#ifdef DEBUG_OUTPUT_
      cout << "### ID not found" << endl;
#endif
      info = 14;
      return info;
    }

  // Raw values are signed 24-bit ADC values:
  if( _pre2016Type )
    {
      // Pre-2016 B-sensor types:
      if( i1 > 0x007FFFFF ) i1 = i1 - 0x01000000;
      vv3 = (double) i1 / (double) 0x007FFFFF;
      if( i2 > 0x007FFFFF ) i2 = i2 - 0x01000000;
      vv1 = (double) -i2 / (double) 0x007FFFFF;
      if( i3 > 0x007FFFFF ) i3 = i3 - 0x01000000;
      vv2 = (double) i3 / (double) 0x007FFFFF;
      vv4 = v4;
    }
  else
    {
      // New 2016 B-sensor types:
      if( i1 > 0x007FFFFF ) i1 = i1 - 0x01000000;
      vv3 = (double) -i1 / (double) 0x007FFFFF;
      if( i3 > 0x007FFFFF ) i3 = i3 - 0x01000000;
      vv1 = (double) -i3 / (double) 0x007FFFFF;
      if( i2 > 0x007FFFFF ) i2 = i2 - 0x01000000;
      vv2 = (double) i2 / (double) 0x007FFFFF;
      vv4 = v4;
    }

  double x[4+1], f[4+1];
  // w[][] should have a minimum dimension of n*(n+3),
  // i.e. here with n=3 -> 18
  double w[2*18];

  // CALVTB
  if( _fieldRange <= 14 )
    {
      // 1.4T
      x[1] = vv1 * 1.4;
      x[2] = vv2 * 1.4;
      x[3] = vv3 * 1.4;
    }
  else if( _fieldRange <= 25 )
    {
      // 2.5T
      x[1] = vv1 * 2.5;
      x[2] = vv2 * 2.5;
      x[3] = vv3 * 2.5;
    }
  else
    {
      // 4.5T
      x[1] = vv1 * 4.5;
      x[2] = vv2 * 4.5;
      x[3] = vv3 * 4.5;
    }

  this->dsnleq( 3, x, f, 1.0e-10, 1.0e-10, 300, 0, &info, w );

  char bite1 = _calConst[index].bite1;
  char bite2 = _calConst[index].bite2;
  info |= (bite1 << 6) | ((bite2-1) << 4);

  // Rotate according to calibration head used
  double *rm;
  if( bite1 == 1 )
    rm = RMB;
  else if( bite1 == 2 )
    rm = RMO;
  else if( bite1 == 3 )
    rm = RM3;
  else if( bite1 == 5 )
    rm = RM5;
  else
    // Unknown? Then don't apply rotation..
    rm = RM_NOROT;
  b1 = (rm[(1-1)+(1-1)*3] * x[1] +
        rm[(2-1)+(1-1)*3] * x[2] +
        rm[(3-1)+(1-1)*3] * x[3]);
  b2 = (rm[(1-1)+(2-1)*3] * x[1] +
        rm[(2-1)+(2-1)*3] * x[2] +
        rm[(3-1)+(2-1)*3] * x[3]);
  b3 = (rm[(1-1)+(3-1)*3] * x[1] +
        rm[(2-1)+(3-1)*3] * x[2] +
        rm[(3-1)+(3-1)*3] * x[3]);

  return info;
}

// ----------------------------------------------------------------------------
/* Comment taken from file dsnleq.for:
   ===================================
   c The following subroutine has been taken from CERN ASIS MATHLIB
   *
   * $Id: snleq64.F,v 1.1.1.1 1996/04/01 15:01:52 mclareni Exp $
   * $Log: snleq64.F,v $
   * Revision 1.1.1.1  1996/04/01 15:01:52  mclareni
   * Mathlib gen
   *
   C  Based on   J.J. More  and  M.Y. Cosnard
   C
   C  ALGORITHM 554 BRENTM, A Fortran Subroutine for the
   C  Numerical Solution of Systems of Nonlinear Equations [C5]
   C
   C  ACM Trans. Math. Software 6 (1980) 240-251.
*/

void CalibCalc::dsnleq( int     n,
                        double *x,
                        double *f,
                        double  ftol,
                        double  xtol,
                        int     maxf,
                        int     iprt,
                        int    *info,
                        double *w )
{
  double z1    = 1.0;
  double scale = 10.0;
  double p05   = 5.0*z1/100.0; 

#ifdef DEBUG_OUTPUT_
  cout.precision( 15 );
  cout << scientific << "dsnleq: x1=" << x[1]
       << " x2=" << x[2] << " x3=" << x[3] << endl;
#endif
  *info = 0;
  if( n <= 0 || ftol <= 0.0 || xtol <= 0 ) return;

  // Find optimal 'mopt' for iterative refinement
  int mopt = 0;
  if( n <= 288 )
    {
      mopt = MPT[n];
    }
  else
    {
      double h = 0.0;
      double temp;
      for( int i=49; i<=n; ++i )
        {
          temp = log( (double)i+z1 )/(n+2*i+1);
          if( temp < h )
            {
              mopt = i-1;
              break;
            }
          h = temp;
        }
    }

  int    wdim   = n+3; // For use in w[] indexing, added 2 May 2012, Henk B
                       // NB: index order FORTRAN/C++ is reversed because
                       //     of Fortran-column and C-row storage
  int    iflag  = 0;
  int    numf   = 0;
  int    nfcall = 0;
  int    nier6 = -1;
  int    nier7 = -1;
  int    nier8 = 0;
  double fnorm = 0.0;
  double difit = 0.0;
  double xnorm = 0.0;

  for( int i=1; i<=n; ++i )
    if( xnorm < fabs(x[i]) ) xnorm = fabs( x[i] );
  double delta = scale * xnorm;
  if( xnorm == 0.0 ) delta = scale;

 // This label and corresponding goto's below could be replaced
  // by a while(1) loop (Henk, 13 May 2012)
 label20:

  // Print something on request? (in original code)
  // ....

  int    nsing  = n;
  double fnorm1 = fnorm;
  double difit1 = difit;
  fnorm = 0.0;

  // Compute step H for the divided difference which approximates
  // the K-th row of the Jacobian matrix
  double h = EPS * xnorm;
  if( h == 0.0 ) h = EPS;
  // NB: array w[] is used like a C-array, with index start at 0
  // hence i and j start at 0 (Henk B);
  // NB: also note the first-column-then-row order of FORTRAN when storing
  //     a 2D array in memory, is shown in the use of 'wdim' in the index
  for( int j=0; j<n; ++j )
    {
      for( int i=0; i<n; ++i )
        w[i + (j+3)*wdim] = 0.0;
      w[j + (j+3)*wdim] = h;
      w[j + (2-1)*wdim] = x[j+1]; // x[] starts at index 1!
    }

  // Enter a subiteration
  for( int k=1; k<=n; ++k )
    {
      iflag = k;
      this->sub1( n, &w[(1-1) + (2-1)*wdim], &f[1], iflag );
      double fky = f[k];
      nfcall = nfcall + 1;
      numf   = nfcall / n;
      // IF(IFLAG .LT. 0) GO TO 230 (but this won't happen here..)
      if( fnorm < fabs(fky) ) fnorm = fabs( fky );

      // Compute the K-th row of the Jacobian matrix
      for( int j=k; j<=n; ++j )
        {
          // NB: array w[] is used like a C-array, with index start at 0
          for( int i=0; i<n; ++i )
            w[i + (3-1)*wdim] = w[i + (2-1)*wdim] + w[i + (j+3-1)*wdim];
          this->sub1( n, &w[(1-1) + (3-1)*wdim], &f[1], iflag );
          double fkz = f[k];
          nfcall = nfcall + 1;
          numf   = nfcall / n;
          // IF(IFLAG .LT. 0) GO TO 230 (but this won't happen here..)
          w[(j-1) + (1-1)*wdim] = fkz - fky;
        }
      f[k] = fky;

      // Compute the Householder transformation to reduce the K-th row
      // of the Jacobian matrix to a multiple of the K-th unit vector
      double eta = 0.0;
      for( int i=k-1; i<n; ++i )
        if( eta < fabs(w[i + (1-1)*wdim]) ) eta = fabs( w[i + (1-1)*wdim] );
      if( eta == 0.0 ) continue; // Jump to start of for-loop
      --nsing;
      double sknorm = 0.0;
      for( int i=k-1; i<n; ++i )
        {
          w[i + (1-1)*wdim] /= eta;
          sknorm += w[i + (1-1)*wdim] * w[i + (1-1)*wdim];
        }
      sknorm = sqrt( sknorm );
      if( w[(k-1) + (1-1)*wdim] < 0.0 ) sknorm = -sknorm;
      w[(k-1) + (1-1)*wdim] += sknorm;

      // Apply the transformation
      double temp;
      for( int i=0; i<n; ++i )
        w[i + (3-1)*wdim] = 0;
      for( int j=k-1; j<n; ++j )
        for( int i=0; i<n; ++i )
          w[i + (3-1)*wdim] += w[j + (1-1)*wdim] * w[i + (j+3)*wdim];
      for( int j=k-1; j<n; ++j )
        {
          temp = w[j + (1-1)*wdim] / (sknorm * w[(k-1) + (1-1)*wdim]);
          for( int i=0; i<n; ++i )
            w[i + (j+3)*wdim] -= temp * w[i + (3-1)*wdim];
        }

      // Compute the subiterate
      w[(k-1) + (1-1)*wdim] = sknorm * eta;
      temp = fky / w[(k-1) + (1-1)*wdim];
      if( h*fabs(temp) > delta )
        {
          // TEMP=SIGN(DELTA/H,TEMP)
          // translated into:
          if( temp >= 0 )
            temp = fabs( delta/h );
          else
            temp = -fabs( delta/h );
        }
      for( int i=0; i<n; ++i )
        w[i + (2-1)*wdim] += temp * w[i + (k+3-1)*wdim];
    }

  // Compute the norms of the iterate and correction vector
  xnorm = 0.0;
  difit = 0.0;
  for( int i=0; i<n; ++i )
    {
      if( xnorm < fabs(w[i + (2-1)*wdim]) )
        xnorm = fabs( w[i + (2-1)*wdim] );
      if( difit < fabs(x[i+1] - w[i + (2-1)*wdim]) )
        difit = fabs( x[i+1] - w[i + (2-1)*wdim] );
      x[i+1] = w[i + (2-1)*wdim];
    }

  // Update the bound on the correction vector
  if( delta < scale*xnorm ) delta = scale * xnorm;

  // Determine the progress of the iteration
  bool lcv = (fnorm < fnorm1) && (difit < difit1) && (nsing == 0);
  ++nier6;
  ++nier7;
  ++nier8;
  if( lcv ) nier6 = 0;
  if( fnorm < fnorm1 || difit < difit1 ) nier7 = 0;
  if( difit > EPS*xnorm ) nier8 = 0;

  // Tests for convergence
  if( fnorm <= ftol )               *info = 1;
  if( difit <= xtol*xnorm && lcv )  *info = 2;
  if( fnorm <= ftol && *info == 2 ) *info = 3;
  if( *info != 0 ) return;

  // Tests for termination
  if( numf >= maxf ) *info = 4;
  if( nsing == n )   *info = 5;
  if( nier6 == 5 )   *info = 6;
  if( nier7 == 3 )   *info = 7;
  if( nier8 == 4 )   *info = 8;
  if( *info != 0 ) return;
  if( !lcv || difit > p05*xnorm ) goto label20;

  // Iterative refinement (if the iteration is converging)
  for( int m=2; m<=mopt; ++m )
    {
      fnorm1 = fnorm;
      fnorm = 0;
      for( int k=1; k<=n; ++k )
        {
          iflag = k;
          this->sub1( n, &w[(1-1) + (2-1)*wdim], &f[1], iflag );
          double fky = f[k];
          nfcall = nfcall + 1;
          numf   = nfcall / n;
          // IF(IFLAG .LT. 0) GO TO 230 (but this won't happen here..)
          if( fnorm < fabs(fky) ) fnorm = fabs( fky );

          // Iterative refinement is terminated if it does not give a
          // reduction on residuals
          if( fnorm >= fnorm1 )
            {
              fnorm = fnorm1;
              goto label20;
            }
          double temp = fky / w[(k-1) + (1-1)*wdim];
          for( int i=0; i<n; ++i )
            w[i + (2-1)*wdim] += temp * w[i + (k+3-1)*wdim];
        }

      // Compute the norms of the iterate and correction vector
      xnorm = 0.0;
      difit = 0.0;
      for( int i=0; i<n; ++i )
        {
          if( xnorm < fabs(w[i + (2-1)*wdim]) )
            xnorm = fabs( w[i + (2-1)*wdim] );
          if( difit < fabs(x[i+1] - w[i + (2-1)*wdim]) )
            difit = fabs( x[i+1] - w[i + (2-1)*wdim] );
          x[i+1] = w[i + (2-1)*wdim];
        }

      // Stopping criteria for iterative refinement
      if( fnorm <= ftol )               *info = 1;
      if( difit <= xtol*xnorm )         *info = 2;
      if( fnorm <= ftol && *info == 2 ) *info = 3;
      if( numf >= maxf && *info == 0 )  *info = 4;
      if( *info != 0 ) return;
    }
  goto label20;
}

// ----------------------------------------------------------------------------

void CalibCalc::sub1( int n, double *x, double *f, int k )
{
  // NB: parameters *x and *f have index starting at 0 !

  double leg[10+1][10+1], fvolt[4+1], cfa[180+1];
  double ar1[8+1][8+1], v1[8+1], v2[8+1], v3[8+1];
  int    ind2[10+1];//, isz2[10+1]; // NB: isz2[] not used
  int    i, m, ihl;
  int    nord = 7;
  double dpi = acos( -1.0 );

  // NB: index order FORTRAN/C++ is reversed because FORTRAN
  // stores 2D arrays column-wise in memory
  double *cf = &cfa[1]; // cf[60][3], NB: in C need to add -1,
                        // and note the use of '*3' in the indexing below
  ind2[1] = 1;
  for( i=1; i<=9; ++i )
    {
      ind2[i+1] = ind2[i] + i;
      //isz2[i] = ind2[i+1] - 1;
    }
  //isz2[10] = ind2[10] + 9;

  ipt1 = 17;
  ihl  = 3;
  if( k == 2 )
    {
      ipt1 = iar4[1] + 8;
      ihl  = (iar4[1] - 1)/4 + 1;
    }
  else if( k == 3 )
    {
      ipt1 = iar4[2] + 8;
      ihl  = (iar4[2] - 1)/4 + 1;
    }
#ifdef DEBUG_OUTPUT_
  //cout << "ipt1=" << ipt1 << " ihl=" << ihl << endl;
  cout << "sub1: x1=" << x[0]
       << " x2=" << x[1] << " x3=" << x[2] << endl;
#endif

  ipt2 = 1;

  double bm = sqrt( x[0]*x[0] + x[1]*x[1] + x[2]*x[2] );

  // The following choice depends on whether the calibration took place
  // in a moderate (1.4T), medium (2.5T) or high field (4.5T) strength field
  // (see also further down)
  // CALVTB:
  double tbm;
  if( _fieldRange <= 14 )
    tbm = 2.0 * bm / 3.05; // 1.4T
  else if( _fieldRange <= 25 )
    tbm = bm / 2.5;        // 2.5T
  else
    tbm = bm / 4.5;        // 4.5T

  tbm = 2.0 * tbm*tbm - 1.0;

  double ttm = 2.0 * (vv4 - 20.0) / 10.0;

  double x2 = 2.0 * tbm;    
  double y2 = 2.0 * ttm;
  int iold = 0;
  while( iar1[ipt1-1] != 0 )
    {
      int    ilcb, ilct;
      double ac1, ac2, ac3 = 0;
      double bc1 = 0, bc2;
      ac1 = 0.0;
      ac2 = 0.0;
      // Inlined function ilcb():
      //int kk_max = ilcb( &ilct );
      {
        // Rather use inline code than integer function ilcb(ilct)...
        ilcb = ((iar1[ipt1-1] >> 4) & 0x0F);
        ilct = (iar1[ipt1-1] & 0x07);
#ifdef DEBUG_OUTPUT_
        //cout << "ilcb=" << ilcb << " ilct=" << ilct << endl;
#endif
        if( iar1[ipt1-1] & (1<<3) )
          {
            ipt3 = (ipt2/2)*3 + 1;
            ipt4 = ipt3;
            ipt5 = ipt4 + 1;
            iold = 1;
          }
        else if( iold == 1 )
          {
            ipt3 = (ipt2/2)*3;
            ipt4 = ipt3 + 1;
            ipt5 = ipt4 + 1;
            iold = 0;
          }
        else
          {
            ipt3 = ipt3 + 3;
            ipt4 = ipt3 + 1;
            ipt5 = ipt4 + 1;
          }

        ++ipt1;
        ++ipt2;
      }
      for( int kk=1; kk<=ilcb; ++kk )
        {
          bc1 = 0.0;
          bc2 = 0.0;
          for( int ll=1; ll<=ilct; ++ll )
            {
              ac3 = bc1;
              bc1 = y2*bc1 - bc2 + this->chbv();
              bc2 = ac3;
            }
          bc1 = bc1 - ac3*ttm; 
          ac3 = ac1;
          ac1 = x2*ac1 - ac2 + bc1;
          ac2 = ac3;
        }
      cfa[ipt3] = ac1 - ac3*tbm;
      cfa[ipt4] = bc1;
      cfa[ipt5] = ac3 - (x2*ac3 + bc1 - ac1)*tbm;
    }
#ifdef DEBUG_OUTPUT_
  //cout << " ipt1=" << ipt1 << " ihl=" << ihl << endl;
#endif
  ipt2 = (ipt2-3)/2 + 1;
  cf[(1-1) + (ipt2-1)*3] = cf[(3-1) + (ipt2-1)*3];
  cf[(2-1) + (ipt2-1)*3] = cf[(3-1) + (ipt2+1-1)*3];
  cf[(3-1) + (ipt2-1)*3] = cf[(3-1) + (ipt2+2-1)*3];

  double gam = cf[(k-1) + (ipt2-1)*3];
  double ax, ay, az;
  if( k == 1 )
    {
      ay = cf[(2-1) + (ipt2-1)*3];
      az = cf[(3-1) + (ipt2-1)*3];
      ax = sqrt( 1.0 - ay*ay - az*az );
    }
  else if( k == 2 )
    {
      ax = cf[(1-1) + (ipt2-1)*3];
      az = cf[(3-1) + (ipt2-1)*3];
      ay = sqrt( 1.0 - ax*ax - az*az );
    }
  else
    {
      ax = cf[(1-1) + (ipt2-1)*3];
      ay = cf[(2-1) + (ipt2-1)*3];
      az = sqrt( 1.0 - ax*ax - ay*ay );
    }

  double cosg, sing;
  if( az != 1.0 )
    { 
      cosg =  ax / sqrt( 1.0 - az*az );
      sing = -ay / sqrt( 1.0 - az*az );
    }
  else
    {
      cosg = 0.0;
      sing = -1.0;
    }

  double cosb = az;
  double sinb = -sqrt( 1.0 - cosb*cosb );
  double cosa = cos( +gam ) * cosg + sin( +gam ) * sing;
  double sina = sin( +gam ) * cosg - cos( +gam ) * sing;

  double xx, yy, zz;
  xx = ((cosa*cosb*cosg - sina*sing) * x[0]
        - (cosa*cosb*sing + sina*cosg) * x[1]
        + cosa*sinb*x[2]);
  yy = ((sina*cosb*cosg + cosa*sing) * x[0]
        - (sina*cosb*sing - cosa*cosg) * x[1]
        + sina*sinb*x[2]);
  zz = (-sinb*cosg * x[0]
        + sinb*sing*x[1]
        + cosb*x[2]);

  double fi;
  if( fabs(xx) > 1.0e-12 )
    {
      fi = atan( yy/xx );
      if( xx < 0.0 ) fi += dpi;
    }
  else if( yy >= 0.0 )
    {
      fi = dpi / 2.0;
    }
  else
    {
      fi = 1.5 * dpi;
    }

  double rr = sqrt( xx*xx + yy*yy + zz*zz );
  zz = zz / rr;
  if( zz >  1.0 ) zz =  1.0;
  if( zz < -1.0 ) zz = -1.0;

  // CALVTB (1.4T, 2.5T or 4.5T)
  double fact;
  if( _fieldRange <= 14 )
    fact = bm / 1.4;
  else if( _fieldRange <= 25 )
    fact = bm / 2.5;
  else
    fact = bm / 4.5;

  // Ylm => cnzn
  int ipta = 0;
  int iptb = 0;
  double fac1, fac2;
  for( m=0; m<=nord; ++m )
    {
      // Fill ar1,leg
      if( m == 0 )
        {
          ar1[1][1] = 1.0;
          ar1[2][2] = sqrt( 3.0 );
          leg[1][1] = 1.0;
          leg[2][1] = sqrt( 3.0 ) * zz;
          v3[1]     = 1.0;
          v3[2]     = zz;
          for( i=3; i<=nord+1; ++i )
            {
              fac1  = sqrt( (2.0*i - 1.0)*(2.0*i - 3.0) ) / (i-1.0);
              fac2  = (i-2.0) / (i-1.0) * sqrt( (2.0*i - 1.0)/(2.0*i - 5.0) );
              v3[i] = v3[i-1] * zz;
              ar1[i][i] = 0.0;
              leg[i][1] = 0.0;
              int j;
              for( j=i-1; j>=1; j=j-2 )
                {
                  ar1[i][j+1] += fac1 * ar1[i-1][j];
                  leg[i][1]   += ar1[i][j+1] * v3[j+1];
                  if( j > 1 ) ar1[i][j-1] = -fac2 * ar1[i-2][j-1]; 
                }
              if( j == 0 ) leg[i][1] += ar1[i][1];
            }
        }
      else
        {
          int j, kj;
          for( j=m+1; j<=8; ++j )
            {
              leg[j][m+1] = 0.0;
              fac1 = -sqrt( 1.0/(double)(j-m)/(double)(j+m-1) );
              for( kj=j-m; kj>=1; kj=kj-2 )
                { 
                  ar1[j][kj]   = ar1[j][kj+1] * kj * fac1;
                  leg[j][m+1] += ar1[j][kj] * v3[kj];
                }
              leg[j][m+1] *= pow( sqrt(1.0 - zz*zz), m );
            }
        }

      if( m < 10 )
        {
          int l, i3, i4, nnn;
          for( l=1; l<=nord+1; ++l )
            {
              v1[l] = 0;
              v2[l] = 0;
            }

          for( l=m; l<=nord; ++l )
            {
              nnn = ind2[l+1] + m;
              if( (nnn < 33 && (iar4[ihl+1] & (1<<(nnn-1)))) ||
                  (nnn > 32 && (iar4[ihl] & (1<<(nnn-33)))) )
                {
                  ++ipta;
                  for( i3=l-m+1; i3>=1; i3=i3-2 )
                    {
                      v1[i3] += cf[(1-1) + (ipta-1)*3] * ar1[l+1][i3];
                    }
                }
            }

          // Scale
          for( i3=1; i3<=nord-m+1; ++i3 )
            {
              v1[i3] *= pow( fact, (i3+m-1) );
            }

          // Back to Ylm
          for( i3=nord-m+1; i3>=1; --i3 )
            {
              if( v1[i3] != 0 )
                {
                  v2[m+i3] = v1[i3] / ar1[m+i3][i3];
                  for( i4=i3; i4>=1; i4=i4-2 )
                    {
                      v1[i4] -= v2[m+i3]*ar1[m+i3][i4];
                    }
                }
            }

          for( l=m; l<=nord; ++l )
            {
              nnn = ind2[l+1] + m;
              if( (nnn < 33 && (iar4[ihl+1] & (1<<(nnn-1)))) ||
                  (nnn > 32 && (iar4[ihl] & (1<<(nnn-33)))) )
                {
                  ++iptb; 
                  cf[(1-1) + (iptb-1)*3] = v2[l+1];
                }
            }
        }
      else
        {
          int l, nnn;
          for( l=m; l<=nord; ++l )
            {
              nnn = ind2[l+1] + m;
              if( (nnn < 33 && (iar4[ihl+1] & (1<<(nnn-1)))) ||
                  (nnn > 32 && (iar4[ihl] & (1<<(nnn-33)))) )
                {
                  ++iptb;
                  cf[(1-1) + (iptb-1)*3] *= pow( fact, l );
                } 
            }
        }
    }

  for( int nnn=1; nnn<=ipt2-1; ++nnn )
    {
      cf[(1-1) + (nnn-1)*3] *= cf[(2-1) + (nnn-1)*3];
    }

  int ipt, m1, m2;
  ipt = 0;
  fvolt[k] = 0;
  for( m1=0; m1<=nord; ++m1 )
    {
      for( m=m1; m<=nord; ++m )
        {
          m2 = ind2[m+1] + m1;
          if( (m2 < 33 && (iar4[ihl+1] & (1<<(m2-1)))) ||
              (m2 > 32 && (iar4[ihl] & (1<<(m2-33)))) )
            {
              ++ipt;
              // ### The following statement is meaningless...
              //if( m == 0 ) cf[(1-1) + (ipt-1)*3] = cf[(1-1) + (ipt-1)*3];
              fvolt[k] = (fvolt[k] + leg[m+1][m1+1] *
                          cf[(1-1) + (ipt-1)*3] * pow( -1.0, m1 ) *
                          cos( m1 * (fi - cf[(3-1) + (ipt-1)*3]) ));
            }
        }
    }

  if( k == 1 ) f[k-1] = fvolt[k] - vv1;
  if( k == 2 ) f[k-1] = fvolt[k] - vv2;
  if( k == 3 ) f[k-1] = fvolt[k] - vv3;
}

// ----------------------------------------------------------------------------

double CalibCalc::chbv()
{
  // Array iar1[] contains 32-bit/4-byte real numbers!
  float rel;
  char *relb = (char *) &rel;

  if( iar1[ipt1-1] != 0 )
    {
      int ik = 0;
      for( int i=4; i>=1; --i )
        {
          relb[ik] = iar1[ipt1+i-1-1];
          ++ik;
        }
      ipt1 += 4;
    }
  else
    {
      rel = 0.0;
      ++ipt1;
    }
#ifdef DEBUG_OUTPUT_
  //cout << "chbv=" << rel << endl;
#endif
  return( (double) rel );
}

// ----------------------------------------------------------------------------

unsigned char CalibCalc::crc8( unsigned char *id, int len )
{
  // Calculate CRC-8 value of 'len' bytes of 'id';
  // uses The CCITT-8 polynomial, expressed as X^8 + X^5 + X^4 + 1
  unsigned char crc = 0x00;
  int index, b;
  for( index=0; index<len; ++index )
    {
      // NB: the ID is stored 'MSByte' (=CRC byte) first
      unsigned char byt = id[len-1-index];
      for( b=0; b<8; ++b )
        {
          if( (byt^crc) & 0x01 )
            {
              crc ^= 0x18;
              crc >>= 1;
              crc |= 0x80;
            }
          else
            {
              crc >>= 1;
            }
          byt >>= 1;
        }
    }
  return crc;
}

// ----------------------------------------------------------------------------
