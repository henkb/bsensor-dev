#include <QDateTime>
#include <CANopen.h>
#include <CanInterface.h>
#include <CanMessage.h>
#include <CanNode.h>
#include "canreadthread.h"
#include "bscanimpl.h"

#define MAX_MSGS_AT_ONCE  50

// ----------------------------------------------------------------------------

CanReadThread::CanReadThread( BsCanImpl *bsys, QObject *parent )
  : QThread( parent ),
    _bsys( bsys ),
    _stop( false ),
    _paused( true )
{
  this->start();

  // The thread goes into a wait condition if _paused==true,
  // so acquire the resource of this initial '_sem.release()' in run()
  // (from then on used for synchronization purposes; see in disable() below)
  if( _paused ) _sem.acquire();
}

// ----------------------------------------------------------------------------

CanReadThread::~CanReadThread()
{
  // In case still running...
  this->stop();
}

// ----------------------------------------------------------------------------

void CanReadThread::stop()
{
  if( this->isRunning() )
    {
      _stop = true;
      _mutex.lock();
      _conditionSuspended.wakeAll();
      _conditionPaused.wakeAll();
      _mutex.unlock();
      this->wait(); // Wait until this thread (i.e. function run()) exits
    }
}

// ----------------------------------------------------------------------------

void CanReadThread::resume()
{
  if( !_paused ) return;
  _paused = false;

  _mutex.lock();
  _conditionPaused.wakeOne();
  _mutex.unlock();
}

// ----------------------------------------------------------------------------

void CanReadThread::pause()
{
  if( _paused ) return;
  _paused = true;

  // If run() is in its periodical suspension, take it out
  _mutex.lock();
  _conditionSuspended.wakeOne();
  _mutex.unlock();

  // Now synchronize: make sure the operations are halted before continuing;
  // this then allows the main thread to initiate synchronous message
  // write/read operations on the interface
  _sem.acquire();
}

// ----------------------------------------------------------------------------

void CanReadThread::run()
{
  uint32_t suspend_time = 1000;
  while( !_stop )
    {
      if( _paused )
        {
          // Lock includes _sem.release() or else resume() may be called
          // before run() enters conditionPaused.wait() below, so that
          // resume() does not wake up run()
          _mutex.lock();
          // Notify method pause() that readout has halted
          _sem.release();
          // Just wait to be resumed again (or to stop completely)...
          _conditionPaused.wait( &_mutex );
          _mutex.unlock();
        }
      else
        {
          // While enabled AND received messages available
          // process the messages, keeping the BscanImpl object up-to-date
          if( _bsys->_canIntf )
            {
              if( this->processMessages() == MAX_MSGS_AT_ONCE )
                {
                  // There may be more messages in the queue,
                  // so this time don't suspend for long
                  suspend_time = 1;
                }
              else
                {
                  // Suspend for somewhat longer
                  suspend_time = 10;
                  //suspend_time = 2000;
                }
            }
          // Suspend this thread: simply wait a little while...
          // unless a pause request has been issued
          if( !_paused )
            {
              _mutex.lock();
              _conditionSuspended.wait( &_mutex, suspend_time );
              _mutex.unlock();
            }
        }
    }
}

// ----------------------------------------------------------------------------

int CanReadThread::processMessages()
{
  int msgs_read = 0;
  CanInterface *intf = _bsys->_canIntf;
  CanMessage   *msg;
  // Process messages until there are no more or until a certain
  // number of messages has been handled (and it is time to yield)
  while( (msg = intf->receive()) != 0 && msgs_read < MAX_MSGS_AT_ONCE )
    {
      ++msgs_read;

      if( msg->isRemoteFrame() || msg->isErrFrame() )
        {
          if( msg->isErrFrame() )
            {
              // Indicate occurrence of Error Frames to the BscanImpl class
              // ### TODO?
            }
          // Don't handle these
          intf->msgHandled( msg );
          continue;
        }

      // Assume all are using the CANopen Predefined Connection Set
      int      object = msg->object();
      int      nodeid = msg->nodeId();
      int      dlc    = msg->dlc();
      uint8_t *bytes  = msg->data();

      // Get the index into _nodeInfo[] from the mapping array
      uint32_t node_i = _bsys->_nodeIdMap[nodeid];

      // Check if it points to an actually existing node in our list
      if( node_i > 127 )
        {
          // The node is not in our list, so ignore this message...
          intf->msgHandled( msg );
          continue;
        }

      // Process the message according to its CANopen object type
      switch( object )
        {
        case TPDO4_OBJ:
          // Transmit-PDO4 message containing B-sensor data
          this->processBsensorReading( node_i, dlc, bytes );
          break;

        case SDOTX_OBJ:
          // SDO server reply, they are for example
          // replies to B-sensor probe and reset requests
          // and then require actions on the _bsys object
          this->processSdo( node_i, msg );
          break;

        case EMERGENCY_OBJ: // ..or SYNC
          if( dlc == 8 )
            {
              // EMERGENCY message
              this->processEmergency( node_i, msg );
            }
          else if( dlc == 1 )
            {
              // SYNC message: ignore
            }
          break;

        case BOOTUP_OBJ: // ..or NODEGUARD_OBJ
          if( dlc == 1 && bytes[0] == 0 )
            {
              // Bootup message
              // Indicate occurrence of Bootup to the BscanImpl class
              // ### TODO
            }
          break;

        default:
          // Ignore this message
          break;
        }

      // Notify the interface object that the message has been handled,
      // it now can remove and delete it
      intf->msgHandled( msg );
    }
  return msgs_read;
}

// ----------------------------------------------------------------------------

void CanReadThread::processBsensorReading( uint32_t node_index,
                                           int      nbytes,
                                           uint8_t *databytes )
{
  // Check for a proper number of data bytes
  if( nbytes != 5 && nbytes != 6 ) return;

  int     b_remote_index, chan;
  int32_t val;
  if( nbytes == 5 )
    {
      // Must be data from a BATCAN node
      b_remote_index = 0;
      chan = databytes[0];
      val  = databytes[2] | (databytes[3]<<8) | (databytes[4]<<16);
    }
  else
    {
      // Data from a BATsCAN node
      // (or BATCAN node in BATsCAN-compatibility mode..)
      b_remote_index = databytes[0];
      chan = databytes[1];
      val  = databytes[3] | (databytes[4]<<8) | (databytes[5]<<16);
    }

  // Value is a 24-bit signed integer, so extend minus sign if necessary
  if( val & 0x800000 ) val |= 0xFF000000;

  // Update a B-sensor data item
  _bsys->bSensorDataItemUpdate( node_index, b_remote_index, chan, val );

#ifdef NO_BSENSORDATAITEMUPDATE_FUNCTION
  // Find location of this B-sensor in our local data structure
  bool found = false;
  int  str_no = b_remote_index / BSCAN_MAX_STRING_SZ;
  int  i      = b_remote_index - (str_no * BSCAN_MAX_STRING_SZ);
  // The B-sensor local index into _bsys->_bSensorInfo[]
  int bindex = _bsys->_nodeInfo[node_index].bStringIndex[str_no] + i;

  // Check if we've got the expected sensor, by checking the remote index
  if( _bsys->_bSensorInfo[bindex].remoteIndex == b_remote_index )
    found = true;

  if( found )
    {
      // Store just the 'raw' value
      // (calibrated values are calculated only on demand)
      if( chan <= 2 )
        _bsys->_bSensorData[bindex].hall[chan] = val;
      else if( chan == 3 )
        _bsys->_bSensorData[bindex].t = val;

      // All B-sensor data received from this node ?
      if( b_remote_index == _bsys->_nodeInfo[node_index].bRemoteIndexHi &&
          chan == 3 )
        // Yes, indicate it
        _bsys->_nodeInfo[node_index].bDataAvailable = true;
    }
#endif
}

// ----------------------------------------------------------------------------

void CanReadThread::processSdo( uint32_t    node_index,
                                CanMessage *msg )
{
  CanNode  *node = _bsys->_nodeInfo[node_index].pNode;
  SdoStatus stat = node->sdoReceived( msg );

  if( !(stat == SDO_STATUS_READ_COMPLETE ||
        stat == SDO_STATUS_WRITE_COMPLETE ||
        stat == SDO_STATUS_ERR_ABORT ) ) return;

  // Successful read or write, or abort
  int sdo_index = node->sdoIndex();
  switch( sdo_index )
    {
    case OD_BSENSOR_PROBE:
      if( stat == SDO_STATUS_ERR_ABORT )
        {
          _bsys->_errString << "Probe"
                            << ", Node " << node->nodeId()
                            << ": " << node->errString();
          _bsys->_nodeInfo[node_index].errStatus |= BSCAN_ERR_SENSOR;
        }
      _bsys->_nodeInfo[node_index].bProbeInProgress = false;
      break;

    case OD_BSENSOR_RESET:
    case OD_BSENSOR_RESET_ALL:
      if( stat == SDO_STATUS_ERR_ABORT )
        {
          _bsys->_errString << "Reset"
                            << ", Node " << node->nodeId()
                            << ": " << node->errString();
          _bsys->_nodeInfo[node_index].errStatus |= BSCAN_ERR_SENSOR;
        }
      else
        {
          // (A number of) B-sensor ADC Offset and Gain registers
          // contain newly determined values
          _bsys->_refreshBsensorOffsGains = true;
        }
      _bsys->_nodeInfo[node_index].bResetInProgress = false;
      break;

    case OD_BSENSOR_DESELECT:
      if( stat == SDO_STATUS_ERR_ABORT )
        {
          _bsys->_errString << "Deselect B-sensors"
                            << ", Node " << node->nodeId()
                            << ": " << node->errString();
          _bsys->_nodeInfo[node_index].errStatus |= BSCAN_ERR_SENSOR;
        }
      break;

    case OD_BSENSOR_SELECT:
      if( stat == SDO_STATUS_ERR_ABORT )
        {
          _bsys->_errString << "Select B-sensor"
                            << ", Node " << node->nodeId()
                            << " index " << node->sdoSubIndex()
                            << ": " << node->errString();
          _bsys->_nodeInfo[node_index].errStatus |= BSCAN_ERR_SENSOR;
        }
      break;

    default:
      break;
    }
}

// ----------------------------------------------------------------------------

void CanReadThread::processEmergency( uint32_t    node_index,
                                      CanMessage *msg )
{
  BsCanImpl::EmgInfo e_info;
  e_info.timeStamp = QDateTime::currentDateTime();

  // Filter out B-sensor related Emergencies for checking
  uint8_t *data = msg->data();
  uint32_t errcode = (static_cast<uint32_t> (data[0]) |
                      (static_cast<uint32_t> (data[1]) << 8));
  uint8_t *errbytes = &data[3];
  uint32_t errid    = static_cast<uint32_t> (errbytes[0]);
  if( errcode == 0x5000 && errid >= 0x50 && errid <= 0x5F )
    {
      // This is a B-sensor related Emergency:
      // if necessary declare the data-acquisition finished...
      BsCanImpl::NodeInfo *n_info = &_bsys->_nodeInfo[node_index];
      if( n_info->bTotal == 1 )
        {
          // There is a problem with the only B-sensor connected to this node;
          // try not to let it block data-acquisition
          n_info->bDataAvailable = true;
        }
      else
        {
          if( errid == 0x51 || errid == 0x52 || errid == 0x53 ||
              errid == 0x54 || errid == 0x56 || errid == 0x57 ||
              (errid == 0x58 && errbytes[2] != 0) )
            {
              int bindex = static_cast<int> (errbytes[1]);
              if( bindex == n_info->bRemoteIndexHi )
                {
                  // There is a problem with the B-sensor with the highest
                  // index connected to this node (used to detect
                  // the end of an acquisition)
                  n_info->bDataAvailable = true;
                }
            }
        }
      // Notify the BsCan system that it should update
      // its B-sensor (error) statuses
      _bsys->_refreshBsensorStatus = true;
    }

  // Copy the message and add to the list of Emergency messages
  CanMessage *emg_msg = new CanMessage( *msg );
  e_info.emgMsg = emg_msg;
  _bsys->_emg.push_back( e_info );

  // Set a limit to the number of Emergencies stored
  if( _bsys->_emg.size() == 100 )
    {
      // Remove the first (oldest) element in the list
      delete _bsys->_emg.front().emgMsg;
      _bsys->_emg.pop_front();
    }
}

// ----------------------------------------------------------------------------
