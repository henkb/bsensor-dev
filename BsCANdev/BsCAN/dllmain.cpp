#include <windows.h>
#include "bscan_c.h"

HMODULE g_Self;

int LockLibraryIntoProcessMem( HMODULE DllHandle, HMODULE *LocalDllHandle )
{
  if( NULL == LocalDllHandle ) return ERROR_INVALID_PARAMETER;
  *LocalDllHandle = NULL;
  TCHAR moduleName[1024];
  if( 0 == GetModuleFileName( DllHandle, moduleName,
                              sizeof(moduleName)/ sizeof(TCHAR) ) )
    return GetLastError();
  *LocalDllHandle = LoadLibrary( moduleName );
  if( NULL == *LocalDllHandle ) return GetLastError();
  return NO_ERROR;
}

BOOL WINAPI DllMain( HINSTANCE hinstDLL,    // handle to DLL module
                     DWORD     fdwReason,   // reason for calling function
                     LPVOID    lpReserved ) // reserved
{
  // Perform actions based on the reason for calling.
  switch( fdwReason ) 
    { 
    case DLL_PROCESS_ATTACH:
      // Initialize once for each new process.
      // Return FALSE to fail DLL load.

      // Refer to oneself to prevent DLL from being unloaded
      // (to solve crash issue with LabView when unloading this DLL)
      // (Eoin Butler, Henk B, 6 Aug 2012)
      LockLibraryIntoProcessMem( hinstDLL, &g_Self );
      break;

    case DLL_THREAD_ATTACH:
      // Do thread-specific initialization.
      break;

    case DLL_THREAD_DETACH:
      // Do thread-specific cleanup.
      break;

    case DLL_PROCESS_DETACH:
      // Perform any necessary cleanup.

      // ### Does not work, probably because threads should not stopped
      //     within DllMain (according to Microsoft documentation)
      // (Eoin Butler, Henk B, 5 Aug 2012)
      //BsCanClose();
      break;
    }
  return TRUE;
}
