#ifndef SYNCTHREAD_H
#define SYNCTHREAD_H

#include <QDateTime>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>

#include <string>

class CanInterface;

class SyncThread: public QThread
{
  Q_OBJECT

  public:
  SyncThread( CanInterface *intf = 0, QObject *parent = 0 );
    ~SyncThread();

    void stop         ();
    void run          ();
    void setEnabled   ( bool b );
    void setInterval  ( int ms );
    void takeTimeStamp();
    void setInterface ( CanInterface *intf ) { _canIntf = intf; };
    int  interval     () { return _intervalMs; }

    std::string dateTimeStampString();
    std::string timeStampString    ();
    long long   timeStampMsec      ();

  private:
    bool           _stop;
    bool           _enabled;
    int            _intervalMs;
    CanInterface  *_canIntf;
    QMutex         _mutex;
    QWaitCondition _condition;
    QDateTime      _timeStamp;
};

#endif // SYNCTHREAD_H
