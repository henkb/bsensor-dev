#ifndef BSCAN_H
#define BSCAN_H

#ifdef WIN32
 // Differentiate between building the DLL or using it
 #ifdef MY_LIB_EXPORT
 #define MY_LIB __declspec(dllexport)
 #else
 #define MY_LIB __declspec(dllimport)
 #endif
 #include "stdint.h"
#else
 #define MY_LIB
 #include </usr/include/stdint.h>
#endif // WIN32

#include <string>

class BsCanImpl;

const uint32_t BSCAN_ALL              = 9999;

// Error status bit definitions
const uint32_t BSCAN_ERR_CANINTF      = 0x01;
const uint32_t BSCAN_ERR_COMM         = 0x02;
const uint32_t BSCAN_ERR_COMM_TIMEOUT = 0x04;
const uint32_t BSCAN_ERR_SENSOR       = 0x08;
const uint32_t BSCAN_ERR_UNDEF        = 0x80;

class MY_LIB BsCan
{
 public:
  // C'tors, d'tor
  BsCan();
#ifdef WIN32
  BsCan( int portno, int interface_type = 0 );
#else
  BsCan( int portno, int interface_type = 3 );
#endif // WIN32
  ~BsCan();
  void stop();

  // Configuration
#ifdef WIN32
  bool        setCanPort               ( int portno,
                                         int interface_type = 0 );
#else
  bool        setCanPort               ( int portno,
                                         int interface_type = 3 );
#endif // WIN32
  bool        connected                ( );
  bool        getConfiguration         ( bool include_offsgains = false );
  bool        bSensorProbe             ( uint32_t node_i = BSCAN_ALL );
  bool        bSensorProbeInProgress   ( uint32_t node_i = BSCAN_ALL );
  bool        bSensorReset             ( uint32_t node_i = BSCAN_ALL );
  bool        bSensorResetInProgress   ( uint32_t node_i = BSCAN_ALL );
  bool        bSensorProbeAtPowerup    ( uint32_t node_i );
  bool        setBsensorProbeAtPowerup ( uint32_t node_i, bool enable );
  bool        triggerInputEnabled      ( uint32_t node_i,
                                         uint32_t trigger_input );
  bool        triggerInputRisingEdge   ( uint32_t node_i,
                                         uint32_t trigger_input );
  bool        setTriggerInputEnabled   ( uint32_t node_i,
                                         uint32_t trigger_input,
                                         bool     enable,
                                         bool     rising_edge = true );
  uint32_t    nodeCount                ( );
  uint32_t    bSensorCount             ( uint32_t node_i = BSCAN_ALL );
  uint32_t    bSensorStringCount       ( uint32_t node_i, uint32_t str_no );
  int         bSensorFirstIndex        ( uint32_t node_i );
  int         nodeId                   ( uint32_t node_i );
  std::string nodeFirmwareVersion      ( uint32_t node_i );
  int         bSensorNodeId            ( uint32_t i );
  int         bSensorRemoteIndex       ( uint32_t i );
  uint32_t    bSensorIdHi              ( uint32_t i );
  uint32_t    bSensorIdLo              ( uint32_t i );
  uint64_t    bSensorId                ( uint32_t i );
  bool        bSensorId                ( uint32_t i, uint8_t *id );
  std::string bSensorIdString          ( uint32_t i );
  int         bSensorIdAlias           ( uint32_t i );
  int         bSensorOrderedIndex      ( uint32_t i );
  bool        setBsensorOrderedIndex   ( uint32_t i, uint64_t id,
                                         int      alias = -1 );
  bool        bSensorOffsets           ( uint32_t i, uint32_t *offs );
  bool        bSensorGains             ( uint32_t i, uint32_t *gain );
  uint32_t    bSensorErrStatus         ( uint32_t i );
  bool        bSensorError             ( );
  bool        getBsensorErrStatus      ( uint32_t i = BSCAN_ALL );
  bool        getBsensorOffsGains      ( uint32_t i = BSCAN_ALL );
  std::string configSummaryString      ( bool ordered = false );
  void        setCalibConstFile        ( std::string filename );
  std::string calibConstFile           ( );
  void        setBsensorsFieldRange    ( double tesla );
  void        setBsensorsPre2016Type   ( bool b );

  // Data-acquisition
  void        setReadoutTriggerSec     ( int secs );
  void        setReadoutTriggerMsec    ( int msecs );
  int         readoutTriggerMsec       ( );
  void        triggerSingleReadout     ( );
  bool        bSensorDataAvailable     ( uint32_t node_i = BSCAN_ALL );
  bool        bSensorWaitDataAvailable ( uint32_t timeout_ms = 0xFFFFFFFF );
  void        bSensorWaitDataAbort     ( );
  uint32_t    bSensorDataRaw           ( int32_t *data, uint32_t i=BSCAN_ALL );
  uint32_t    bSensorDataCalibrated    ( double  *data,
                                         int32_t *err,
                                         uint32_t i=BSCAN_ALL );
  uint32_t    bSensorDataHallRaw       ( int32_t *data );
  uint32_t    bSensorDataTemperature   ( int32_t *data );
  void        bSensorDataRelease       ( );
  std::string dataTimeStampString      ( bool include_date = false );
  long long   dataTimeStampMsec        ( );

  // Various:
  // Multiple B-sensor ADC reset/calibrate sequences plus
  // average offset/gain register contents calculations with
  // an upper limit on the allowed spread in values
  bool        bSensorAdcCalibrate      ( uint32_t i,
                                         uint32_t calib_cnt,
                                         double   max_sigma,
                                         double  *sigma = 0,
                                         bool     include_tsensor = true );
  // Explicit B-sensor selection and deselection
  bool        bSensorSelect            ( uint32_t i );
  bool        bSensorDeselect          ( uint32_t i = BSCAN_ALL );
  // CAN node error status
  uint32_t    nodeErrStatus            ( uint32_t node_i );
  // System error status
  uint32_t    errStatus                ( );
  // Clear system error status
  void        clearErrStatus           ( );
  // System error string
  std::string errString                ( );
  // Emergency messages stuff
  uint32_t    emgCount                 ( );
  bool        emgClear                 ( uint32_t i = BSCAN_ALL );
  std::string emgTimeStampString       ( uint32_t i, bool include_date );
  std::string emgDataString            ( uint32_t i );
  std::string emgDescription           ( uint32_t i );
  // Class version string
  std::string version                  ( );

 private:
  BsCanImpl *_bscanImpl;
};

#endif // BSCAN_H
