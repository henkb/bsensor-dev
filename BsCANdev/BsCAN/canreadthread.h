#ifndef CANREADTHREAD_H
#define CANREADTHREAD_H

#include <QMutex>
#include <QSemaphore>
#include <QThread>
#include <QWaitCondition>

#ifdef WIN32
#include "stdint.h"
#else
#include </usr/include/stdint.h>
#endif // WIN32

class BsCanImpl;
class CanMessage;

class CanReadThread: public QThread
{
  Q_OBJECT

  public:
    CanReadThread( BsCanImpl *bsys, QObject *parent = 0 );
    ~CanReadThread();

    void stop                 ();
    void pause                ();
    void resume               ();

    void run                  ();
    int  processMessages      ();
    void processBsensorReading( uint32_t    node_index,
                                int         nbytes,
                                uint8_t    *databytes );
  void processSdo           ( uint32_t    node_index,
                              CanMessage *msg );
  void processEmergency     ( uint32_t    node_index,
                              CanMessage *msg );

  private:
    BsCanImpl     *_bsys;
    bool           _stop;
    bool           _paused;
    QMutex         _mutex;
    QWaitCondition _conditionSuspended;
    QWaitCondition _conditionPaused;
    QSemaphore     _sem;
};

#endif // CANREADTHREAD_H
