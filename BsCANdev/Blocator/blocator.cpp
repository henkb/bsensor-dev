/* B-LOCATOR:
   program to help locating where a particular B-sensor module in a BsCAN3
   system is located by selecting it, which switches on its selection LED;
   click on a listed B-sensor module to select it, or use up- and down-arrow
   keys to select the preceding or next module in the list.

   Version 1.0, 21-APR-2012
   - First version, tested on Windows with Kvaser module,
     on Linux not tested yet.
   Version 1.3, 30-NOV-2012
   - Display BsCan library version used.
   - Display info about nodes found: number of B-sensors, firmware version.
*/

//#define USE_SYSTEC

#include <CanInterface.h>
#include <bscan.h>

#include <QTime>
#include <QTimer>

#include "blocator.h"

const QString VERSION( "v1.3  30-Nov-2012" );
//const QString VERSION( "v1.2  29-AUG-2012" );

// ----------------------------------------------------------------------------

Blocator::Blocator()
  : QDialog(),
    _bSelected(-1)
{
  setupUi( this );

  this->setWindowFlags( Qt::WindowMinimizeButtonHint |
                        Qt::WindowMaximizeButtonHint |
                        Qt::WindowCloseButtonHint );

  // To get proper font size under Linux
  QApplication::setFont( QFont("Tahoma", 8) );

  this->labelVersion->setText( VERSION );

  this->addDiagnostic( QString("(Using lib ") +
                       QString::fromStdString( _bscan->version() ) +
                       QString(")") );

  // Fill the 'CAN port' combobox
  this->findCanPorts();

  // Connect buttons
  connect( this->pushButtonConnect, SIGNAL( clicked() ),
           this, SLOT( connectBsCan() ) );
  connect( this->pushButtonClear, SIGNAL( clicked() ),
           this, SLOT( clearDiagnostics() ) );

  // Connect the table widget
  connect( this->tableWidgetBsensors, SIGNAL( cellClicked(int,int) ),
           this, SLOT( selectBsensor(int) ) );

  // Instantiate a BsCAN3 system object (without connecting to a real one yet)
  _bscan = new BsCan;

  // Brushes for selected/deselected color indications
  _qbDeselect = this->palette().brush( QPalette::Base );
  _qbSelect   = QBrush( QColor("yellow") );
  //_qpDeselect = this->palette();
  //_qpSelect  = _qpDeselect;
  //_qpSelect.setColor( QPalette::Base, QColor("green") );

  // Initialize the table widget which will contain the B-sensor info
  QTableWidget *tbl = this->tableWidgetBsensors;
  tbl->setSelectionMode( QAbstractItemView::NoSelection );
  tbl->setColumnCount( 4 );
  QStringList labels;
  labels << " Index " << " NodeId " << " Rindex " << " B-sensor ID ";
  tbl->setHorizontalHeaderLabels( labels );
  tbl->resizeColumnsToContents();
  tbl->resizeRowsToContents();

  // Modify the horizontal table headers to stand out a bit more...
  QTableWidgetItem *hdr = tbl->horizontalHeaderItem( 0 );
  QFont qf = hdr->font();
  qf.setBold( true );
  for( int i=0; i<tbl->columnCount(); ++i )
    tbl->horizontalHeaderItem( i )->setFont( qf );
}

// ----------------------------------------------------------------------------

Blocator::~Blocator()
{
  if( _bscan ) delete _bscan;
}

// ----------------------------------------------------------------------------

void Blocator::closeEvent( QCloseEvent * )
{
  // When quitting the application deselect any B-sensor modules selected
  // and disconnect the CAN port
  if( _bscan->connected() ) this->connectBsCan();
}

// ----------------------------------------------------------------------------

void Blocator::keyPressEvent( QKeyEvent *ke )
{
  // Using up and down arrow keys select another B-sensor module
  // (key 'up' decreases, key 'down' increases the index !),
  // and the ESCAPE key deselects any selected B-sensor module
  int selected = _bSelected;
  if( selected < 0 ) return;

  int key = ke->key();
  if( key == Qt::Key_Down )
    {
      ++selected;
      // Wrap-around ?
      if( selected == (int) _bscan->bSensorCount() ) selected = 0;
      this->selectBsensor( selected );
      // Keep the user-interface up-to-date
      if( ke->isAutoRepeat() ) this->repaint();
    }
  else if( key == Qt::Key_Up )
    {
      --selected;
      // Wrap-around ?
      if( selected == -1 ) selected = _bscan->bSensorCount() - 1;
      this->selectBsensor( selected );
      // Keep the user-interface up-to-date
      if( ke->isAutoRepeat() ) this->repaint();
    }
  else if( key == Qt::Key_Escape )
    {
      // Deselect B-sensors
      this->selectBsensor( -1 );
    }
  else
    {
      // Forward the event to the base class
      QDialog::keyPressEvent( ke );
    }
}

// ----------------------------------------------------------------------------

void Blocator::findCanPorts()
{
  this->comboBoxPortNr->clear();

  int portcnt;
  int *portnumbers = 0;
#ifdef WIN32
  #ifdef USE_NICAN
  int intf_type = NICAN_INTF_TYPE;
  #else
  #ifdef USE_SYSTEC
  int intf_type = SYSTEC_INTF_TYPE;
  #else
  int intf_type = KVASER_INTF_TYPE;
  #endif
  #endif
#else
  #ifdef USE_KVASER
  int intf_type = KVASER_INTF_TYPE;
  #else
  int intf_type = SOCKET_INTF_TYPE;
  #endif
#endif
  portcnt = CanInterface::canInterfacePorts( intf_type, &portnumbers );

  for( int i=0; i<portcnt; ++i )
    this->comboBoxPortNr->addItem( QString::number(portnumbers[i]), i );

  // Change widgets' status and/or appearance
#ifdef WIN32
  #ifdef USE_NICAN
  QString qs( "NI-CAN" );
  #else
  #ifdef USE_SYSTEC
  QString qs( "SYSTEC" );
  #else
  QString qs( "KVASER" );
  #endif
  #endif
#else
  #ifdef USE_KVASER
  QString qs( "KVASER" );
  #else
  QString qs( "SocketCAN" );
  #endif
#endif // WIN32
  this->addDiagnostic( QString("Found %1 %2 CAN-ports").arg(portcnt).arg(qs) );
  this->groupBoxCanPort->setTitle( QString("CAN port %1").arg(qs) );

  if( portcnt > 0 )
    this->pushButtonConnect->setEnabled( true );
  else
    this->pushButtonConnect->setEnabled( false );
}

// ----------------------------------------------------------------------------

void Blocator::connectBsCan()
{
  // Connect to or disconnect from a BsCAN3 system on the selected CAN port
  if( _bscan->connected() )
    {
      // Deselect any selected B-sensor modules
      _bscan->bSensorDeselect();

      // Disconnect
      _bscan->setCanPort( -1 );
      _bSelected = -1;

      // Change widgets' status and/or appearance
      this->pushButtonConnect->setText( "Connect" );
      this->comboBoxPortNr->setEnabled( true );

      this->displayConfig();
    }
  else
    {
      // Change widgets' status and/or appearance
      // (and show hourglass while busy...)
      this->pushButtonConnect->setText( "Disconnect" );
      this->comboBoxPortNr->setEnabled( false );
      QApplication::setOverrideCursor( Qt::WaitCursor );
      qApp->processEvents();

      // Connect to the BsCAN3 system on the requested CAN port
      int port_no = this->comboBoxPortNr->currentText().toInt();

      // Connect (Windows: to Kvaser, Linux: to SocketCAN)
      _bscan->setCanPort( port_no );

      // Display errors
      if( !_bscan->errString().empty() )
        this->addDiagnostic( QString("### CAN port %1: %2").arg( port_no ).
                             arg( _bscan->errString().c_str() ) );

      if( _bscan->connected() )
        {
          // Show how many we found
          this->addDiagnostic( QString("=>CAN port %1 connected: "
                                       "found %2 nodes").
                               arg( port_no ).
                               arg( _bscan->nodeCount() ) );
          this->addDiagnostic( QString("  with %1 B-sensors in total").
                               arg( _bscan->bSensorCount() ) );
          for( unsigned int i=0; i<_bscan->nodeCount(); ++i )
            {
              QString version =
                QString::fromStdString( _bscan->nodeFirmwareVersion( i ) );
              this->addDiagnostic( QString(" Node %1: %2 B-sensor(s), "
                                           "firmw version \"%3\"").
                                   arg( _bscan->nodeId(i), 3 ).
                                   arg( _bscan->bSensorCount(i), 2 ).
                                   arg( version ) );
            }

          // Show the B-sensor configuration found on this CAN port
          this->displayConfig();

          QTimer::singleShot( 0, this, SLOT( checkForError() ) );
        }
      else
        {
          // Change widgets' status and/or appearance
          this->pushButtonConnect->setText( "Connect" );
          this->comboBoxPortNr->setEnabled( true );
        }
      QApplication::restoreOverrideCursor();
    }
}

// ----------------------------------------------------------------------------

void Blocator::displayConfig()
{
  QTableWidget *tbl = this->tableWidgetBsensors;
  tbl->setRowCount( _bscan->bSensorCount() );

  // Fill the table with a list of B-sensor module information, and display
  QTableWidgetItem *item;
  QFont qf;
  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
    {
      // Index
      item = new QTableWidgetItem( QString::number(i+1) );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      tbl->setItem( i, 0, item );

      // Node Identifier
      item = new QTableWidgetItem( QString::number(_bscan->bSensorNodeId(i)) );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      tbl->setItem( i, 1, item );

      // Remote Index
      item = new QTableWidgetItem( QString::number(_bscan->
                                                   bSensorRemoteIndex(i)) );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      tbl->setItem( i, 2, item );

      // ID
      item = new QTableWidgetItem( QString(_bscan->
                                           bSensorIdString(i).c_str()) );
      if( i == 0 )
        {
          // Initialize qf
          qf = item->font();
          qf.setFamily( "Courier" );
        }
      item->setFont( qf );
      item->setTextAlignment( Qt::AlignCenter );
      tbl->setItem( i, 3, item );
    }

  tbl->resizeColumnsToContents();
  tbl->resizeRowsToContents();
  tbl->setColumnWidth( 3, 130 );
}

// ----------------------------------------------------------------------------

void Blocator::selectBsensor( int row )
{
  // Select the B-sensor listed in row 'row' in the table widget

  // Already selected ?
  if( row == _bSelected ) return;

  if( _bSelected < 0 )
    {
      QApplication::setOverrideCursor( Qt::WaitCursor ); // Might take a while
      qApp->processEvents();
    }

  // Deselect currently selected B-sensor module (30 Nov 2012)
  // or any selected B-sensor module (when _bSelected = -1)
  _bscan->bSensorDeselect( _bSelected );

  if( _bSelected >= 0 )
    {
      // Remove the currently highlighted table row
      for( int col=0; col<this->tableWidgetBsensors->columnCount(); ++col )
        this->tableWidgetBsensors->item( _bSelected, col )->
          setBackground( _qbDeselect );

      _bSelected = -1;
    }

  // No new B-sensor to select
  if( row < 0 ) return;

  // Select the requested B-sensor
  if( _bscan->bSensorSelect( row ) )
    {
      _bSelected = row;

      // Highlight the table row of the selected B-sensor
      for( int col=0; col<this->tableWidgetBsensors->columnCount(); ++col )
        this->tableWidgetBsensors->item( row, col )->
          setBackground( _qbSelect );

      // Make sure the high-lighted table row is visible
      this->tableWidgetBsensors->
        scrollToItem( this->tableWidgetBsensors->item( row, 0 ) );
    }

  QApplication::restoreOverrideCursor();
}

// ----------------------------------------------------------------------------

void Blocator::checkForError()
{
  // Display BsCAN3 system errors
  if( _bscan->errStatus() )
    {
      this->addDiagnostic( QString("### %1").
                           arg( _bscan->errString().c_str() ) );
      _bscan->clearErrStatus();
    }

  // Display any Emergency messages (and then clear them..)
  uint32_t ecnt = _bscan->emgCount();
  for( uint32_t i=0; i<ecnt; ++i )
    {
      this->addDiagnostic( QString("### %1").
                           arg( _bscan->emgDescription(0).c_str() ) );
      _bscan->emgClear( 0 );
    }

  // Repeat, if appropriate
  if( _bscan->connected() )
    QTimer::singleShot( 500, this, SLOT( checkForError() ) );
}

// ----------------------------------------------------------------------------

void Blocator::addDiagnostic( const QString &qs )
{
  QString p;
  p =  QTime::currentTime().toString();
  p += QString( "  " );
  p += qs;
  this->plainTextEditDiagnostic->appendPlainText( p );
}

// ----------------------------------------------------------------------------

void Blocator::addDiagnostic( const char *str )
{
  this->addDiagnostic( QString(str) );
}

// ----------------------------------------------------------------------------

void Blocator::clearDiagnostics()
{
  this->plainTextEditDiagnostic->clear();
}

// ----------------------------------------------------------------------------
