#
# Project file for the Blocator tool
#
# To generate a Visual Studio project:
#   qmake -t vcapp Blocator.pro
# To generate a Makefile:
#   qmake Blocator.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = app
TARGET   = Blocator

# Create a Qt app
CONFIG += qt thread warn_on exceptions debug_and_release
contains(QT_MAJOR_VERSION,5) {
  QT += widgets
}

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  LIBS       += -L../Release
}

LIBS += -lBsCAN
LIBS += -lCANopen

INCLUDEPATH += ../CAN/CANopen
INCLUDEPATH += ../BsCAN

win32 {
  RC_FILE = blocator.rc
}

FORMS     += blocator.ui
RESOURCES += blocator.qrc 

SOURCES   += main.cpp
SOURCES   += blocator.cpp
HEADERS   += blocator.h
