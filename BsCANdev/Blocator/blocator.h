#ifndef BLOCATOR_H
#define BLOCATOR_H

#include "ui_blocator.h"

class BsCan;

class Blocator: public QDialog, private Ui_Blocator
{
  Q_OBJECT

 public:
  Blocator();
  ~Blocator();

 private slots:
  void findCanPorts();
  void connectBsCan();
  void selectBsensor( int row );

  void checkForError();

  void clearDiagnostics();

 private:
  void closeEvent( QCloseEvent *ce );
  void keyPressEvent( QKeyEvent *ke );

  void displayConfig();

  void addDiagnostic( const QString &qs );
  void addDiagnostic( const char *str );

 private:
  BsCan *_bscan;

  // Index of currently selected B-sensor module
  int    _bSelected;

  // Palettes and brushes for select/deselect color indications
  QBrush _qbDeselect, _qbSelect;
};

#endif // BLOCATOR_H
