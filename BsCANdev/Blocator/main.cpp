#include <QApplication>

#include "blocator.h"

int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );
  Blocator blocator;
  blocator.show();
  return app.exec();
}
