#include <iostream>
#include <iomanip>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
//#include "stdint.h"
#include "wingetopt.h"
#else
//#include </usr/include/stdint.h>
#include <unistd.h>
#endif

#include "calibcalc.h"

void usage();
void arg_error( char opt );
void arg_range( char opt, int min, int max );

// Version identifier: year, month, day, release number

// Indicate 'doubles' in ID list;
// by default list each ID plus size of constants block
const int VERSION_ID   = 0x17041300;
// Initial version
//const int VERSION_ID = 0x16121100;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int    opt;
  bool   long_list = true;
  bool   no_spaces = true;
  string filename;

  // Parse the options
  char opts[] = "hsSv";
  while( (opt = getopt(argc, argv, opts)) != -1 )
    {
      switch( opt )
        {
        case 'h':
          usage();
          return 0;
        case 's':
          long_list = false;
          break;
        case 'S':
          no_spaces = false;
          break;
        case 'v':
          cout << "Version " << hex << VERSION_ID << dec << endl;
          return 0;
        default: // '?'
          usage();
          return 0;
        }
    }

  // Name of file with B-sensor calibration constants
  if( optind < argc )
    filename = string( argv[optind] );
  if( filename.empty() )
    {
      cout << "### Missing calibration constants file name" << endl;
      return 0;
    }

  CalibCalc ccalc;
  ccalc.setCalibConstFile( filename );
  ccalc.listBsensorIds( long_list, no_spaces );

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "bcaliblist version " << hex << VERSION_ID << dec << endl <<
    "List information about a B-sensor calibration constants file.\n"
    "Usage: bcaliblist [-h|v] [-l] <filename>\n"
    "  -h          : Show this help text.\n"
    "  -v          : Show version.\n"
    "  -s          : 'Short' listing: display total number of B-sensor IDs in "
    "the file.\n"
    "                Default: list each B-sensor ID in the file.\n"
    " <filename>   : Name of file containing B-sensor calibration constants.\n";
}

// ----------------------------------------------------------------------------

void arg_error( char opt )
{
  cout << "### -" << opt << ": error in argument" << endl;
  usage();
  exit( 0 );
}

// ----------------------------------------------------------------------------

void arg_range( char opt, int min, int max )
{
  cout << "### -" << opt << ": argument not in range ("
       << min << ".." << max << ")" << endl;
  exit( 0 );
}

// ----------------------------------------------------------------------------
