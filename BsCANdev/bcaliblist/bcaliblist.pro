TEMPLATE = app
TARGET   = bcaliblist

CONFIG += console thread warn_on exceptions debug_and_release

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  DESTDIR     = ../Debug
  LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  DESTDIR     = ../Release
  LIBS       += -L../Release
}

INCLUDEPATH += ../BsCAN

SOURCES   += ../BsCAN/calibcalc.cpp
HEADERS   += ../BsCAN/calibcalc.h
win32 {
  SOURCES += wingetopt.c
  HEADERS += wingetopt.h
}

SOURCES   += bcaliblist.cpp
