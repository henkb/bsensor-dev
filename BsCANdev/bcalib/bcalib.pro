TEMPLATE = app
TARGET   = bcalib

CONFIG += console thread warn_on exceptions debug_and_release

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  DESTDIR     = ../Debug
  LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  DESTDIR     = ../Release
  LIBS       += -L../Release
}

INCLUDEPATH += ../BsCAN
INCLUDEPATH += ../bcaliblist

SOURCES   += ../BsCAN/calibcalc.cpp
HEADERS   += ../BsCAN/calibcalc.h
win32 {
  SOURCES += ../bcaliblist/wingetopt.c
  HEADERS += ../bcaliblist/wingetopt.h
}

SOURCES   += bcalib.cpp
