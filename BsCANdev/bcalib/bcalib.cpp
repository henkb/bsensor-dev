#include <iostream>
#include <iomanip>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#ifdef WIN32
//#include "stdint.h"
#include "wingetopt.h"
#else
//#include </usr/include/stdint.h>
#include <unistd.h>
#endif

#include "calibcalc.h"

void usage();
void arg_error( char opt );
void arg_range( char opt, int min, int max );

// Version identifier: year, month, day, release number

// Initial version
const int   VERSION_ID = 0x17070700; // Some comments; extend usage text
//const int VERSION_ID = 0x17070100;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int    opt;
  bool   gauss = true;
  int    precision = 1;
  string idstr, calibfile, datafile;
  int    b1, b2, b3, t;
  double b1cal, b2cal, b3cal;
  unsigned char id[8];

  // Parse the options
  char opts[] = "hgv23";
  while( (opt = getopt(argc, argv, opts)) != -1 )
    {
      switch( opt )
        {
        case 'h':
          usage();
          return 0;
        case '2':
          precision = 2;
          break;
        case '3':
          precision = 3;
          break;
        case 'g':
          gauss = false;
          break;
        case 'v':
          cout << "Version " << hex << VERSION_ID << dec << endl;
          return 0;
        default: // '?'
          usage();
          return 0;
        }
    }

  // Go through the other arguments
  if( !(/*argc == 4 ||*/ argc == optind-1 + 7) )
    {
      cout << "### Incomplete number of arguments" << endl;
      usage();
      return 0;
    }

  // Name of file with B-sensor calibration constants
  calibfile = string( argv[optind+0] );
  ifstream ifs( calibfile );
  if( !ifs.is_open() )
    {
      cout << "### Failed to open file: " << calibfile << endl;
      return 0;
    }
  ifs.close();

  // B-sensor ID
  idstr = string( argv[optind+1] );
  if( idstr.length() != 16 ) // Expect exactly 16 chars
    {
      cout << "### Illegal ID: " << idstr << endl;
      return 0;
    }
  for( int i=0; i<8; ++i )
    id[i] = 0;
  for( unsigned int i=0; i<idstr.length(); ++i )
    {
      int  nibble = 0;
      char ch = idstr[i];
      if( !((ch >= '0' && ch <= '9') ||
            (ch >= 'a' && ch <= 'f') ||
            (ch >= 'A' && ch <= 'F')) )
        {
          cout << "### Illegal ID (char " << i << "): " << idstr << endl;
          return 0;
        }

      if( ch >= '0' && ch <= '9' )
        nibble = (int) ch - '0';
      else if( ch >= 'a' && ch <= 'f' )
        nibble = (int) ch - 'a' + 0xA;
      else if( ch >= 'A' && ch <= 'F' )
        nibble = (int) ch - 'A' + 0xA;

      id[i/2] <<= 4;
      id[i/2] |= (unsigned char) (nibble & 0xF);
    }
  /*
  // DEBUG:
  cout << hex << setfill( '0' );
  for( int i=0; i<8; ++i )
    cout << setw( 2) << (unsigned int) id[i] << " ";
  cout << endl;
  */

  // Data b1, b2, b3 and t
  if( sscanf( argv[optind+2], "%d", &b1 ) != 1 ||
      sscanf( argv[optind+3], "%d", &b2 ) != 1 ||
      sscanf( argv[optind+4], "%d", &b3 ) != 1 ||
      sscanf( argv[optind+5], "%d", &t ) != 1 )
    {
      cout << "### Error in data arguments" << endl;
      return 0;
    }
  // Temperature value in millidegrees -> degrees
  double temp = (double) t / (double) 1000.0;

  CalibCalc ccalc;
  ccalc.setCalibConstFile( calibfile );
  //ccalc.setFieldRange( 25 );
  //ccalc.setPre2016Type( true );

  int info = 0;
  ccalc.calvtb( (char *) id, b1, b2, b3, temp,
                b1cal, b2cal, b3cal, info );

  // Extract the status/error bits
  info &= 0x0F;
  if( !(info == 1 || info == 2 || info == 3) )
    cout << "### Conversion error: " << info << endl;
  //else
  //  cout << "Info " << info << endl;

  // Display converted values, in Gauss or Tesla
  if( gauss )
    {
      b1cal *= 10000.0;
      b2cal *= 10000.0;
      b3cal *= 10000.0;
    }
  else
    {
      precision = 8;
    }
  cout << fixed << setprecision( precision );
  cout << b1cal << "  " << b2cal << "  " << b3cal << endl;

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "bcalib version " << hex << VERSION_ID << dec << endl <<
    "Convert a B-sensor reading to physical values, Gauss or Tesla.\n"
    "Usage: bcalib [-h|V] [-g] [-2|3] <calibfile> <ID> <b1> <b2> <b3> <T>\n"
    "  -h         : Show this help text.\n"
    "  -v         : Show version.\n"
    "  -g         : Display converted values in Tesla (default: Gauss).\n"
    "  -2|3       : Display Gauss values in 2 or 3 decimals (default: 1).\n"
    " <calibfile> : Name of file containing B-sensor calibration constants.\n"
    "  <ID>       : B-sensor ID (16-char hexadecimal number).\n"
    "  <b1>       : First Hall sensor reading.\n"
    "  <b2>       : Second Hall sensor reading.\n"
    "  <b3>       : Third Hall sensor reading.\n"
    "  <t>        : Temperature sensor reading, in millidegrees.\n";
}

// ----------------------------------------------------------------------------

void arg_error( char opt )
{
  cout << "### -" << opt << ": error in argument" << endl;
  usage();
  exit( 0 );
}

// ----------------------------------------------------------------------------

void arg_range( char opt, int min, int max )
{
  cout << "### -" << opt << ": argument not in range ("
       << min << ".." << max << ")" << endl;
  exit( 0 );
}

// ----------------------------------------------------------------------------
