#include <QApplication>

#include "bscan3ui.h"

int main( int argc, char *argv[] )
{
  QApplication app( argc, argv );
  BsCan3Ui bscan3ui;
  bscan3ui.show();
  return app.exec();
}
