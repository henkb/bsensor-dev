/* BsCAN3ui:
   program for reading out a BsCAN3 system, displaying the data (raw or
   calibrated) and optionally saving the data (raw or calibrated) to file.
   Similar to the 'old' BsCAN3ui program (made with VS6 and Qt v2.3.0),
   but completely rewritten, based on the BsCan class and using Qt v4.7.x
   and VS2008.

   Version 3.0, 14-MAY-2012 (Version 2.x.x was the old BsCAN3ui program)
   - First of version 3, tested on Windows with Kvaser module,
     on Linux not tested yet.

   Version 3.1, 29-AUG-2012
   - Using latest CANopen library (got rid of NewInterface.h).

   Version 3.2, 30-NOV-2012
   - Display BsCan library version used.
   - Display info about nodes found: number of B-sensors, firmware version.
   - More diagnostic output in case of node error.
   - Fix bug in Emergency display/clear.

   Version 3.3, 01-SEP-2014
   - Some updates in CAN libs; cosmetic changes in bscan3ui.cpp.
   - Added a spinbox for 'number of sensors per line' (in logfile).
   - Added BsCAN control groupbox:
     'Reset' and 'Probe' buttons plus their functionality.
   - Added 'include raw data' checkbox to 'logging' groupbox:
     when calibrated data is logged, (an) additional line(s) are added
     containing the raw B-sensor data values.
   - Added read- and writeAppSettings().

   Version 4.0, 25-NOV-2016
   - Introduce B-sensor ordering and 'alias' identifier, read from a file.
     (ordering plus alias-identifier implemented in the BsCan library,
      by calls setBsensorOrderedIndex() and bSensorOrderedIndex().)
   - Added support for SYSTEC interface (under Windows).

   Version 4.1, 13-DEC-2017
   - Added _bscan->getConfiguration() to actions after a probe or reset
     operation, in function processDataAndErrors().
*/

//#define USE_SYSTEC

#include <math.h>

#include <CanInterface.h>
#include <bscan.h>

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QSettings>
#include <QTime>
#include <QTimer>
#include <QTextStream>

#include "bscan3ui.h"

const QString VERSION( "v4.1  13-Dec-2017" );
//const QString VERSION( "v4.0  26-Apr-2017" );
//const QString VERSION( "v4.0  25-Nov-2016" );
//const QString VERSION( "v3.3  08-Sep-2014" );

// Table column indices
const int COL_I    = 0;
const int COL_AID  = 1;
const int COL_X    = 2;
const int COL_Y    = 3;
const int COL_Z    = 4;
const int COL_B    = 5;
const int COL_T    = 6;
const int COL_BID  = 7;
const int COL_NID  = 8;

// ----------------------------------------------------------------------------

BsCan3Ui::BsCan3Ui()
  : QDialog(),
    _bscan( 0 ),
    _interfaceType( KVASER_INTF_TYPE ),
    _calibConstDir( "." ),
    _orderFileDir( "." ),
    _resetInProgress( false ),
    _probeInProgress( false ),
    _inProgressCntr( 0 ),
    _loggedSamples( 0 ),
    _samplesToLog( 0 ),
    _configLogged( false )
{
  setupUi( this );

  this->setWindowFlags( Qt::WindowMinimizeButtonHint |
                        Qt::WindowMaximizeButtonHint |
                        Qt::WindowCloseButtonHint );

  // To get proper font size under Linux
  QApplication::setFont( QFont("Tahoma", 8) );

  _labelVersion->setText( VERSION );

  // Instantiate a BsCAN3 system object
  // (without connecting to any BsCAN3 system yet)
  _bscan = new BsCan;
  this->addDiagnostic( QString("(Using lib ") +
                       QString::fromStdString( _bscan->version() ) +
                       QString(")") );

  // Fill the 'CAN port' combobox
  this->findCanPorts();

  // Read settings remembered from this application's previous use
  this->readAppSettings();

  // Connect buttons
  connect( _pushButtonConnect, SIGNAL( clicked() ),
           this, SLOT( connectOrDisconnect() ) );
  connect( _pushButtonReset, SIGNAL( clicked() ),
           this, SLOT( reset() ) );
  connect( _pushButtonProbe, SIGNAL( clicked() ),
           this, SLOT( probe() ) );
  connect( _pushButtonStartStopReadout, SIGNAL( clicked() ),
           this, SLOT( startStopReadout() ) );
  connect( _pushButtonSelectCalibConstFile, SIGNAL( clicked() ),
           this, SLOT( selectCalibConstFile() ) );
  connect( _pushButtonStartStopLog, SIGNAL( clicked() ),
           this, SLOT( startStopLogging() ) );
  connect( _pushButtonSetToLog, SIGNAL( clicked() ),
           this, SLOT( setSamplesToLog() ) );
  connect( _pushButtonOrderFile, SIGNAL( clicked() ),
           this, SLOT( selectOrderFile() ) );
  connect( _pushButtonClear, SIGNAL( clicked() ),
           this, SLOT( clearDiagnostics() ) );
  connect( _pushButtonExit, SIGNAL( clicked() ),
           this, SLOT( close() ) );

  // Connect checkboxes
  connect( _checkBoxCalibrated, SIGNAL( clicked() ),
           this, SLOT( calibratedChanged() ) );

  // Connect line edits
  connect( _lineEditToLog, SIGNAL( returnPressed() ),
           this, SLOT( setSamplesToLog() ) );

  // Connect spinboxes
  //connect( _spinBoxSensorsPerLine, SIGNAL( valueChanged(int) ),
  //         this, SLOT( setSensorsPerLine(int) ) );

  _intValidator = new QIntValidator( 0, 0x7FFFFFFF, this );
  _lineEditToLog->setValidator( _intValidator );

  // Brushes for selected/deselected color indications
  _qbOkay  = this->palette().brush( QPalette::Base );
  _qbError = QBrush( QColor("yellow") );

  // Initialize the table widget which will contain the B-sensor info
  QTableWidget *tbl = _tableWidgetBsensors;
  tbl->setSelectionMode( QAbstractItemView::NoSelection );
  tbl->setColumnCount( 9 );
  QStringList labels;
  labels << " # " << " Alias " << "  Bx  " << "  By  " << "  Bz  " << " |B| "
         << "  Temp [C]  " << " ID " << " NodeId ";
  tbl->setHorizontalHeaderLabels( labels );
  tbl->resizeColumnsToContents();
  tbl->resizeRowsToContents();
  //tbl->setColumnHidden( COL_AID, true );
  tbl->setColumnHidden( COL_B, true );

  // Modify the horizontal table headers to stand out a bit more...
  QTableWidgetItem *hdr = tbl->horizontalHeaderItem( 0 );
  QFont qf = hdr->font();
  qf.setBold( true );
  for( int i=0; i<tbl->columnCount(); ++i )
    tbl->horizontalHeaderItem( i )->setFont( qf );

  _checkBoxIncludeRaw->hide();
  _lineEditDataLed->hide();
  _lineEditErrLed->hide();
}

// ----------------------------------------------------------------------------

BsCan3Ui::~BsCan3Ui()
{
  if( _bscan ) delete _bscan;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::closeEvent( QCloseEvent *event )
{
  // When quitting the application deselect any B-sensor modules selected
  // and disconnect the CAN port
  if( _bscan->connected() ) this->connectOrDisconnect();

  // When quitting the application save some of the current settings
  this->writeAppSettings();

  event->accept();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::readAppSettings()
{
  QSettings settings( "CERN", "BsCAN3ui" );

  int port = settings.value( "portIndex", 0 ).toInt();
  if( port != -1 && port < _comboBoxPortNr->count() )
    _comboBoxPortNr->setCurrentIndex( port );

  int interval = settings.value( "readoutInterval", 5 ).toInt();
  _spinBoxInterval->setValue( interval );

  int sensors = settings.value( "sensorsPerLine", 32 ).toInt();
  _spinBoxSensorsPerLine->setValue( sensors );

  int samples = settings.value( "samplesToLog", 0 ).toInt();
  _lineEditToLog->setText( QString::number(samples) );

  _calibConstDir = settings.value( "calibConstDir", "." ).toString();

  _orderFileDir = settings.value( "orderFileDir", "." ).toString();

  _logDir = settings.value( "logDir", "." ).toString();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::writeAppSettings()
{
  QSettings settings( "CERN", "BsCAN3ui" );
  if( _comboBoxPortNr->currentIndex() != -1 ) 
    settings.setValue( "portIndex", _comboBoxPortNr->currentIndex() );
  settings.setValue( "readoutInterval", _spinBoxInterval->value() );
  settings.setValue( "sensorsPerLine", _spinBoxSensorsPerLine->value() );
  settings.setValue( "samplesToLog", _lineEditToLog->text().toInt() );
  settings.setValue( "calibConstDir", _calibConstDir );
  settings.setValue( "orderFileDir", _orderFileDir );
  settings.setValue( "logDir", _logDir );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::findCanPorts()
{
  _comboBoxPortNr->clear();

  int portcnt;
  int *portnumbers = 0;
#ifdef WIN32
  #ifdef USE_NICAN
  _interfaceType = NICAN_INTF_TYPE;
  #else
  #ifdef USE_SYSTEC
  _interfaceType = SYSTEC_INTF_TYPE;
  #else
  _interfaceType = KVASER_INTF_TYPE;
  #endif
  #endif
#else
  #ifdef USE_KVASER
  _interfaceType = KVASER_INTF_TYPE;
  #else
  _interfaceType = SOCKET_INTF_TYPE;
  #endif
#endif // WIN32
  portcnt = CanInterface::canInterfacePorts( _interfaceType, &portnumbers );

  for( int i=0; i<portcnt; ++i )
    _comboBoxPortNr->addItem( QString::number(portnumbers[i]), i );

  // Change widgets' status and/or appearance
#ifdef WIN32
  #ifdef USE_NICAN
  QString qs( "NI-CAN" );
  #else
  #ifdef USE_SYSTEC
  QString qs( "SYSTEC" );
  #else
  QString qs( "KVASER" );
  #endif
  #endif
#else
  #ifdef USE_KVASER
  QString qs( "KVASER" );
  #else
  QString qs( "SocketCAN" );
  #endif
#endif // WIN32
  this->addDiagnostic( QString("Found %1 %2 CAN-ports").arg(portcnt).arg(qs) );
  _groupBoxCanPort->setTitle( QString("CAN port %1").arg(qs) );

  if( portcnt > 0 )
    _pushButtonConnect->setEnabled( true );
  else
    _pushButtonConnect->setEnabled( false );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::connectOrDisconnect()
{
  // Connect to or disconnect from a BsCAN3 system on the selected CAN port
  if( _bscan->connected() )
    {
      // Deselect any selected B-sensor modules
      //_bscan->bSensorDeselect();

      // Disconnect (stops read-out)
      _bscan->setCanPort( -1 );

      // Change widgets' status and/or appearance
      _pushButtonConnect->setText( "Connect" );
      _comboBoxPortNr->setEnabled( true );
      _pushButtonStartStopReadout->setText( "Start read" );
      _pushButtonStartStopReadout->setEnabled( false );
      _pushButtonStartStopLog->setEnabled( false );
      _pushButtonOrderFile->setEnabled( false );
      _pushButtonReset->setEnabled( false );
      _pushButtonProbe->setEnabled( false );
      _pushButtonReset->setChecked( false );
      _pushButtonProbe->setChecked( false );
      _spinBoxInterval->setEnabled( true );

      _configLogged = false;

      _resetInProgress = false;
      _probeInProgress = false;
    }
  else
    {
      // Connect to the BsCAN3 system on the requested CAN port
      int port_nr = _comboBoxPortNr->currentText().toInt();

      // Change widgets' status and/or appearance
      // (and show hourglass while busy...)
      _pushButtonConnect->setText( "Disconnect" );
      _comboBoxPortNr->setEnabled( false );
      QApplication::setOverrideCursor( Qt::WaitCursor );
      QApplication::processEvents();

      // Connect (Windows: to Kvaser, NICAN or SysTec, Linux: to SocketCAN)
      _bscan->setCanPort( port_nr, _interfaceType );

      // Display errors
      if( !_bscan->errString().empty() )
        this->addDiagnostic(QString("### CAN port %1: %2").arg( port_nr ).
                            arg(QString::fromStdString(_bscan->errString())) );

      if( _bscan->connected() )
        {
          // Change widgets' status and/or appearance
          _pushButtonStartStopReadout->setEnabled( true );
          _pushButtonStartStopLog->setEnabled( true );
          _pushButtonOrderFile->setEnabled( true );
          _pushButtonReset->setEnabled( true );
          _pushButtonProbe->setEnabled( true );
          _lineEditErrLed->hide();

          // Display in the diagnostic window how many we found
          this->displayConfig();

          // Update the table showing the B-sensor configuration
          // found on this CAN port
          this->updateConfig();

          // Write (new?) configuration to logfile if required and possible
          this->logConfig();

          QTimer::singleShot( 0, this, SLOT( processDataAndErrors() ) );
        }
      else
        {
          // Change widgets' status and/or appearance
          _pushButtonConnect->setText( "Connect" );
          _pushButtonConnect->setChecked( false );
          _comboBoxPortNr->setEnabled( true );
        }
      QApplication::restoreOverrideCursor();
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::reset()
{
  if( !_bscan->connected() )
    {
      QMessageBox::warning( this, "Reset B-sensors",
                            "There's nothing to reset: not connected" );
      _pushButtonReset->setChecked( false );
      return;
    }
  if( _bscan->bSensorCount() == 0 )
    {
      QMessageBox::warning( this, "Reset B-sensors",
                            "There are no B-sensors to reset..." );
      _pushButtonReset->setChecked( false );
      return;
    }

  if( !_bscan->bSensorReset() )
    {
      _pushButtonReset->setChecked( false );
      this->addDiagnostic( QString("BsCAN3 reset operation error: ") +
                           QString::fromStdString(_bscan->errString()) );
      return;
    }
  this->addDiagnostic( "Reset command issued" );

  _resetInProgress = true;
  _inProgressCntr = 0;

  // Buttons are re-enabled when operation completes, in processDataAndErrors()
  _pushButtonReset->setEnabled( false );
  _pushButtonStartStopReadout->setEnabled( false );

  // Re-log the configuration when appropriate
  _configLogged = false;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::probe()
{
  // Re-connect
  int port_nr = _comboBoxPortNr->currentText().toInt();
  _bscan->setCanPort( port_nr, _interfaceType );

  this->addDiagnostic( "Reconnected CAN port" );

  if( !_bscan->connected() )
    {
      QMessageBox::warning( this, "Probe for B-sensors",
                            "There's nothing to probe for:\nnot connected" );
      _pushButtonProbe->setChecked( false );
      return;
    }
  if( _bscan->nodeCount() == 0 )
    {
      QMessageBox::warning( this, "Probe for B-sensors",
                            "There's nothing to probe for:\n"
                            "no CAN nodes found\n"
                            "(consider Disconnect/Connect)" );
      _pushButtonProbe->setChecked( false );
      return;
    }

  if( !_bscan->bSensorProbe() )
    {
      _pushButtonProbe->setChecked( false );
      this->addDiagnostic( QString("BsCAN3 probe operation error: ") +
                           QString::fromStdString(_bscan->errString()) );
      return;
    }
  this->addDiagnostic( "Probe command issued" );

  _probeInProgress = true;
  _inProgressCntr = 0;

  // Buttons are re-enabled when operation completes, in processDataAndErrors()
  _pushButtonProbe->setEnabled( false );
  _pushButtonStartStopReadout->setEnabled( false );

  // Re-log the configuration when appropriate
  _configLogged = false;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startStopReadout()
{
  //if( _bscan->readoutTriggerMsec() == 0 )
  if( _spinBoxInterval->isEnabled() )
    this->startReadout();
  else
    this->stopReadout();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startReadout()
{
  // Change widgets' status and/or appearance
  _pushButtonStartStopReadout->setText( "Stop read" );
  _spinBoxInterval->setEnabled( false );
  _pushButtonStartStopLog->setEnabled( false );
  _pushButtonOrderFile->setEnabled( false );
  _pushButtonReset->setEnabled( false );
  _pushButtonProbe->setEnabled( false );

  if( _logFile.isOpen() )
    {
      // Simply add an empty line to indicate new start
      QTextStream stream( &_logFile );
      stream << QChar( '\n' );

      _lineEditToLog->setText( QString::number(_samplesToLog) );

      // Don't do this here, may take a long time...
      //this->logConfig();
    }
  else
    {
      _loggedSamples = 0;
      _lineEditLogged->setText( "0" );
      _lineEditToLog->setText( QString::number(_samplesToLog) );
    }

  _bscan->setReadoutTriggerSec( _spinBoxInterval->value() );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::stopReadout()
{
  _bscan->setReadoutTriggerSec( 0 );
  _bscan->bSensorDataRelease();

  // Change widgets' status and/or appearance
  _pushButtonStartStopReadout->setText( "Start read" );
  _spinBoxInterval->setEnabled( true );
  if( _bscan->connected() )
    {
      _pushButtonStartStopLog->setEnabled( true );
      _pushButtonOrderFile->setEnabled( true );
      _pushButtonReset->setEnabled( true );
      _pushButtonProbe->setEnabled( true );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startStopLogging()
{
  if( _logFile.isOpen() )
    this->stopLogging();
  else
    this->startLogging();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startLogging()
{
  QString filename =
    QFileDialog::getSaveFileName( this, "Open file for data logging", _logDir,
                                  "All files (*.*)", 0,
                                  QFileDialog::DontConfirmOverwrite );
  if ( filename.isEmpty() )
    {
      _pushButtonStartStopLog->setChecked( false );
      return;
    }

  _logFile.setFileName( filename );

  bool file_opened = false;
  if( _logFile.exists() )
    {
      // Ask: overwrite, append or cancel
      int result;
      result = QMessageBox::warning( this, "Data file",
                                     "File exists, what would you like to do?",
                                     "Overwrite", "Append", "Cancel", 1 );
      switch( result )
        {
        case 0:
          file_opened = _logFile.open( QIODevice::WriteOnly ) ;
          break;
        case 1:
          file_opened = _logFile.open( QIODevice::WriteOnly |
                                       QIODevice::Append ) ;
          break;
        case 2:
          _pushButtonStartStopLog->setChecked( false );
          return;
        }
    }
  else
    {
      file_opened = _logFile.open( QIODevice::WriteOnly ) ;
    }

  if( !file_opened )
    {
      _pushButtonStartStopLog->setChecked( false );
      QMessageBox::critical( this, "Opening data file",
                             "Failed to open file" );
      return;
    }

  // Remember the path for next time
  QFileInfo f_info( filename );
  _logDir = f_info.absolutePath();

  QString qs( "Opened data file: " );
  qs += filename;
  this->addDiagnostic( qs );

  _lineEditLogfile->setText( _logFile.fileName() );
  _lineEditLogfile->setEnabled( true );
  _pushButtonStartStopLog->setText( QString("Stop") );

  _loggedSamples = 0;
  _lineEditLogged->setText( "0" );
  _lineEditToLog->setText( QString::number(_samplesToLog) );

  // Write the configuration to logfile if required
  this->logConfig();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::stopLogging()
{
  // Close log file
  if( _logFile.isOpen() )
    {
      _logFile.flush();
      _logFile.close();

      QString qs( "Closed data file: " );
      qs += _logFile.fileName();
      this->addDiagnostic( qs );
    }

  //_lineEditLogfile->setText( QString( "" ) );
  _lineEditLogfile->setEnabled( false );
  _pushButtonStartStopLog->setText( QString( "Start..." ) );

  _configLogged = false;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::setSamplesToLog()
{
  if( _lineEditToLog->text().isEmpty() )
    {
      _lineEditToLog->setText( QString::number(_samplesToLog) );
    }
  else
    {
      _samplesToLog = _lineEditToLog->text().toInt();
      this->addDiagnostic( QString("Set number of samples to log to %1").
                           arg( _samplesToLog ) );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::selectCalibConstFile()
{
  QString filename =
    QFileDialog::getOpenFileName( this, "Select a calibration constants file",
                                  _calibConstDir,
                                  "DAT files (*.dat);;All files (*.*)" );
  if( filename.isEmpty() ) return;

  _bscan->setCalibConstFile( filename.toStdString() );

  this->addDiagnostic( "Calibration constants file set to: " );
  this->addDiagnostic( filename );

  // Remember the path for next time
  QFileInfo f_info( filename );
  _calibConstDir = f_info.absolutePath();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::selectOrderFile()
{
  QString filename =
    QFileDialog::getOpenFileName( this, "Select a B-sensor ordering file",
                                  _orderFileDir, "All files (*.*)" );
  if( filename.isEmpty() ) return;

  // Open file
  QFile file( filename );
  if( !file.open( QIODevice::ReadOnly ) )
    {
      QMessageBox::critical( this, "Opening ordering file",
                             "Failed to open file" );
      return;
    }

  // Clear the current ordering vector
  _bmap.clear();

  // Read file contents (ASCII)
  QTextStream in( &file );
  QString     line = in.readLine();
  int         line_nr = 0, defs = 0;
  QStringList list;
  uint64_t    id;
  int         alias;
  bool        ok;
  while( !line.isNull() )
    {
      ++line_nr;
      line = line.trimmed(); // Remove white-space at start and end
      if( line.startsWith( QChar('#') ) ) // Line with comment?
        {
          line = in.readLine();
          continue;
        }

      list = line.split( QRegExp("\\W+"), QString::SkipEmptyParts );
      if( list.size() < 1 || list.size() > 2 )
        {
          QMessageBox::critical( this, "Ordering file",
                                 QString("Line %1: unexpected number "
                                         "of parameters").arg( line_nr ) );
          break;
        }

      // First parameter should be the B-sensor ID (hexadecimal)
      id = list[0].toULongLong( &ok, 16 );
      if( !ok )
        {
          QMessageBox::critical( this, "Ordering file",
                                 QString("Line %1: invalid ID=%1").
                                 arg( list[0] ) );
          break;
        }

      // Second parameter, if any, should be the B-sensor alias
      alias = -1;
      if( list.size() == 2 )
        {
          alias = list[1].toInt( &ok );
          if( !ok )
            {
              QMessageBox::critical( this, "Ordering file",
                                     QString("Line %1: invalid alias=%1").
                                     arg( list[1] ) );
              break;
            }
        }
      ++defs;

      // Store definition in the order list, to be applied in updateConfig()
      _bmap.push_back( std::pair<uint64_t,int>( id, alias ) );
      //_bscan->setBsensorOrderedIndex( defs-1, id, alias );

      line = in.readLine();
    }

  QString qs;
  this->addDiagnostic( "B-sensor Order File:" );
  qs = QString( "%1: %2 lines, %3 definitions" ).
    arg( filename ).arg( line_nr ).arg( defs );
  this->addDiagnostic( qs );

  if( defs > 0 )
    {
      this->updateConfig();
      // Write the (new) configuration to logfile
      _configLogged = false;
      this->logConfig();
    }

  // Remember the path for next time
  QFileInfo f_info( filename );
  _orderFileDir = f_info.absolutePath();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::calibratedChanged()
{
  if( _checkBoxCalibrated->isChecked() )
    {
      // Only show the |B| column if calibrated values are shown
      _tableWidgetBsensors->setColumnHidden( COL_B, false );
      _checkBoxGauss->setEnabled( true );

      // Let the user know which calibration constants file will be used
      // (and if it exists at all..)
      QString filename = QString::fromStdString( _bscan->calibConstFile() );
      this->addDiagnostic( "Calib constants filename: " );
      this->addDiagnostic( filename );
      QFileInfo f_info( filename );
      if( !f_info.exists() )
        this->addDiagnostic( "WARNING: file does not exist" );

      // Show the 'include raw data' checkbox
      _checkBoxIncludeRaw->show();
    }
  else
    {
      // Hide the |B| column
      _tableWidgetBsensors->setColumnHidden( COL_B, true );
      _checkBoxGauss->setEnabled( false );

      // Hide the 'include raw data' checkbox
      _checkBoxIncludeRaw->hide();
      _checkBoxIncludeRaw->setChecked( false );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::processDataAndErrors()
{
  if( !_bscan->connected() ) return;

  // B-sensor data
  if( _bscan->bSensorDataAvailable() )
    {
      // Take into account that there might be just one B-sensor in total
      // that is in error...
      if( !(_bscan->bSensorCount() == 1 && _bscan->bSensorErrStatus(0)) )
        {
          // Flash the green 'Data LED'
          _lineEditDataLed->show();
          QTimer::singleShot( 500, this, SLOT( hideDataLed() ) );
        }

      this->updateBsensorData( _checkBoxCalibrated->isChecked(),
                               _checkBoxGauss->isChecked() );

      // Node error status
      int err = _bscan->errStatus();
      if( err )
        {
          for( unsigned int i=0; i<_bscan->nodeCount(); ++i )
            if( _bscan->nodeErrStatus( i ) )
              this->addDiagnostic( QString("BsCAN3 errstat CAN node %1: %2").
                                   arg( _bscan->nodeId(i) ).
                                   arg( _bscan->nodeErrStatus(i) ) );
          this->addDiagnostic( QString("BsCAN3 last err descr: ") +
                               QString::fromStdString(_bscan->errString()) );

          _bscan->clearErrStatus();
        }

      // B-sensor error status
      if( _bscan->bSensorError() )
        {
          // Highlight 'bad' B-sensors on the display
          for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
            {
              int order_i = _bscan->bSensorOrderedIndex( i );
              if( _bscan->bSensorErrStatus(order_i) )
                {
                  // Highlight the table row showing this B-sensor's data
                  for( int col=0; col<_tableWidgetBsensors->columnCount();
                       ++col )
                    _tableWidgetBsensors->item( i, col )->
                      setBackground( _qbError );
                  _lineEditErrLed->show();
                }
            }
        }

      this->logData();

      // Data processed, ready for the next set of sampled data
      _bscan->bSensorDataRelease();
    }

  // ### Don't use processEvents() in this timer-called function:
  // this function then seems to get called multiple times...(?)
  //QApplication::processEvents();

  // CANopen Emergencies
  int cnt = _bscan->emgCount();
  if( cnt )
    {
      //this->addDiagnostic( QString("EMG cnt=%1").arg( cnt ) );
      for( int i=0; i<cnt; ++i )
        {
          QString qs;
          qs += QString( "EMG " );
          qs += QString::fromStdString( _bscan->emgDescription(i) );
          this->addDiagnostic( qs );
        }
      // Don't read and clear in the same loop, so do clearing here
      // (use only index '0', since we delete one at a time)
      for( int i=0; i<cnt; ++i ) _bscan->emgClear( 0 );
    }

  // Reset and probe operations
  if( _resetInProgress )
    {
      if( _bscan->bSensorResetInProgress() )
        {
          ++_inProgressCntr;
          // Progress message every second...
          if( _inProgressCntr == 10 )
            {
              this->addDiagnostic( "BsCAN3 reset in-progress.." );
              _inProgressCntr = 0;
            }
        }
      else
        {
          _resetInProgress = false;
          this->addDiagnostic( "Reset completed" );

          // Reread and update the BsCAN3 B-sensor configuration shown
          // (and log it if necessary)
          _bscan->getConfiguration();
          this->displayConfig();
          this->updateConfig();
          this->logConfig();
          _pushButtonReset->setChecked( false );
          _pushButtonReset->setEnabled( true );
          _pushButtonStartStopReadout->setEnabled( true );
        }
    }
  if( _probeInProgress )
    {
      if( _bscan->bSensorProbeInProgress() )
        {
          ++_inProgressCntr;
          // Progress message every second...
          if( _inProgressCntr == 10 )
            {
              this->addDiagnostic( "BsCAN3 probe in-progress.." );
              _inProgressCntr = 0;
            }
        }
      else
        {
          _probeInProgress = false;
          this->addDiagnostic( "Probe completed" );

          // Reread and update the BsCAN3 B-sensor configuration shown
          // (and log it if necessary)
          _bscan->getConfiguration();
          this->displayConfig();
          this->updateConfig();
          this->logConfig();
          _pushButtonProbe->setChecked( false );
          _pushButtonProbe->setEnabled( true );
          _pushButtonStartStopReadout->setEnabled( true );
        }
    }

  QTimer::singleShot( 100, this, SLOT( processDataAndErrors() ) );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::displayConfig()
{
  int port_nr = _comboBoxPortNr->currentText().toInt();
  this->addDiagnostic( QString("CAN port %1: "
                               "found %2 B-sensors on %3 nodes").
                       arg( port_nr ).
                       arg( _bscan->bSensorCount() ).
                       arg( _bscan->nodeCount() ) );
  for( unsigned int i=0; i<_bscan->nodeCount(); ++i )
    {
      QString version =
        QString::fromStdString( _bscan->nodeFirmwareVersion( i ) );
      this->addDiagnostic( QString("  Node %1: %2 B-sensors, "
                                   "firmw version \"%3\"").
                           arg( _bscan->nodeId(i), 3 ).
                           arg( _bscan->bSensorCount(i), 2 ).
                           arg( version ) );
    }
  // Alternatively:
  // addDiagnostic( QString::fromStdString(_bscan->configSummaryString()) );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::updateConfig()
{
  QTableWidget *table = _tableWidgetBsensors;
  table->clearContents();
  table->setRowCount( _bscan->bSensorCount() );

  // Apply mapping when available
  if( _bmap.size() > 0 )
    {
      int index = 0;
      for( unsigned int i=0; i<_bmap.size(); ++i )
        {
          // An ID that is not found is skipped when ordering
          if( _bscan->setBsensorOrderedIndex( index, _bmap[i].first,
                                              _bmap[i].second ) )
            {
              /*QString qs;
              qs = QString( "%1 ID=%2 alias=%3" ).arg( index ).
                arg( _bmap[i].first, 16, 16, QChar('0') ).
                arg( _bmap[i].second );
                this->addDiagnostic( qs );*/
              ++index;
            }
          else if( _bscan->connected() )
            {
              QString qs;
              qs = QString( "B-sensor order: ID=%1 not found, skipped" ).
                arg( _bmap[i].first, 16, 16, QChar('0') );
              this->addDiagnostic( qs );
            }
        }
    }

  // Fill the table with a list of B-sensor module information,
  // and update the display
  QTableWidgetItem *item;
  QFont qf;
  int order_i;
  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
    {
      order_i = _bscan->bSensorOrderedIndex( i );

      // Index
      item = new QTableWidgetItem( QString::number(i+1) );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      table->setItem( i, COL_I, item );

      // Alias
      QString alias( "--" );
      if( _bscan->bSensorIdAlias(order_i) >= 0 )
        alias = QString::number( _bscan->bSensorIdAlias(order_i) );
      item = new QTableWidgetItem( alias );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      table->setItem( i, COL_AID, item );

      // Node Identifier
      item =
        new QTableWidgetItem( QString::number(_bscan->bSensorNodeId(order_i)) );
      item->setTextAlignment( Qt::AlignCenter );
      item->setFlags( item->flags() & ~Qt::ItemIsEditable );
      table->setItem( i, COL_NID, item );

      // ID
      item = new QTableWidgetItem( QString::fromStdString(_bscan->
                                   bSensorIdString(order_i)) );
      item->setTextAlignment( Qt::AlignCenter );
      if( i == 0 )
        {
          // Initialize qf
          qf = item->font();
          qf.setFamily( "Courier" );
          //qf.setPointSize( qf.pointSize()-1 );
        }
      item->setFont( qf );
      table->setItem( i, COL_BID, item );

      // X, Y, Z, T
      for( int j=COL_X; j<=COL_T; ++j )
        {
          item = new QTableWidgetItem( QString("") );
          if( j == COL_T )
            item->setTextAlignment( Qt::AlignCenter );
          else
            item->setTextAlignment( Qt::AlignRight|Qt::AlignVCenter );
          item->setFlags( item->flags() & ~Qt::ItemIsEditable );
          table->setItem( i, j, item );
        }
    }

  //tbl->resizeColumnsToContents();
  //table->setColumnWidth( COL_AID, 60 );
  table->setColumnWidth( COL_X, 80 );
  table->setColumnWidth( COL_Y, 80 );
  table->setColumnWidth( COL_Z, 80 );
  table->setColumnWidth( COL_B, 80 );
  table->setColumnWidth( COL_T, 70 );
  table->setColumnWidth( COL_BID, 150 );
  table->setColumnWidth( COL_NID, 60 );
  table->resizeRowsToContents();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::updateBsensorData( bool calibrated, bool gauss )
{
  QTableWidget *table = _tableWidgetBsensors;
  QTableWidgetItem *item_x, *item_y, *item_z, *item_b, *item_t;
  int order_i;
  if( calibrated )
    {
      int p; // For precision
      if( gauss )
        p = 1;
      else
        p = 6;
      for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
        {
          order_i = _bscan->bSensorOrderedIndex( i );

          double data[4];
          int    err;
          _bscan->bSensorDataCalibrated( data, &err, order_i );

          item_x = table->item( i, COL_X );
          item_y = table->item( i, COL_Y );
          item_z = table->item( i, COL_Z );
          item_b = table->item( i, COL_B );
          item_t = table->item( i, COL_T );

          if( err )
            {
              QString qs = QString("###Err%1").arg( err );
              item_x->setText( qs );
              item_y->setText( qs );
              item_z->setText( qs );
              item_b->setText( QString("") );
            }
          else
            {
              if( gauss )
                {
                  data[0] *= 10000.0;
                  data[1] *= 10000.0;
                  data[2] *= 10000.0;
                }
              item_x->setText( QString("%1").arg( data[0], 0, 'f', p ) );
              item_y->setText( QString("%1").arg( data[1], 0, 'f', p ) );
              item_z->setText( QString("%1").arg( data[2], 0, 'f', p ) );
              double b = sqrt( data[0]*data[0] +
                               data[1]*data[1] + data[2]*data[2] );
              item_b->setText( QString("%1").arg( b, 0, 'f', p ) );
            }
          // Temperature
          item_t->setText( QString("%1").arg( data[3], 5, 'f', 2 ) );
        }
    }
  else
    {
      for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
        {
          order_i = _bscan->bSensorOrderedIndex( i );

          int32_t data[4];
          _bscan->bSensorDataRaw( data, order_i );

          item_x = table->item( i, COL_X );
          item_y = table->item( i, COL_Y );
          item_z = table->item( i, COL_Z );
          item_t = table->item( i, COL_T );

          // B-field
          item_x->setText( QString("%1").arg( data[0] ) );
          item_y->setText( QString("%1").arg( data[1] ) );
          item_z->setText( QString("%1").arg( data[2] ) );

          // Temperature
          item_t->setText( QString("%1").
                           arg( (float) data[3]/1000.0, 5, 'f', 2 ) );
        }
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::logConfig()
{
  if( !_bscan->connected() ) return;
  if( !_logFile.isOpen() ) return;

  // Write the current B-sensor configuration to the logfile
  // only if not done already
  if( _configLogged ) return;

  // Reading offsets/gains may take a while...
  this->addDiagnostic( "Acquiring B-sensor config data for logfile..." );
  QApplication::processEvents();
  QApplication::setOverrideCursor( Qt::WaitCursor );

  // Add an empty line to indicate a possibly new log start
  QTextStream stream( &_logFile );
  stream << QChar( '\n' );

  // Write the current configuration to the logfile
  QString      qs;
  int          order_i, val;
  unsigned int uval, regs[4];
  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
    {
      order_i = _bscan->bSensorOrderedIndex( i );
      qs.clear();
      qs += QString( "CONFIG %1" ).arg( i+1, 3 );
      val = _bscan->bSensorIdAlias( order_i );
      qs += QString( " Alias %1" ).arg( val, 3 );
      val = _bscan->bSensorNodeId( order_i );
      qs += QString( " Node %1" ).arg( val, 3 );
      val = _bscan->bSensorRemoteIndex( order_i );
      qs += QString( " String %1" ).arg( val/32 );
      qs += QString( " Rindex %1 : " ).arg( val, 3 );

      // B-sensor ID
      uval = _bscan->bSensorIdHi( order_i );
      qs += QString::number( uval, 16 ).toUpper().rightJustified( 8, '0' );
      uval = _bscan->bSensorIdLo( order_i );
      qs += QString::number( uval, 16 ).toUpper().rightJustified( 8, '0' );

      // ADC Offset register values
      qs += QString( "  Offset " );
      _bscan->bSensorOffsets( order_i, regs );
      for( int chan=0; chan<4; ++chan )
        qs += QString( "%1 " ).
          arg( QString::number( regs[chan],16 ).toUpper(), 7 );

      // ADC Gain register values
      qs += QString( " Gain " );
      _bscan->bSensorGains( order_i, regs );
      for( int chan=0; chan<4; ++chan )
        qs += QString( "%1 " ).
          arg( QString::number( regs[chan],16 ).toUpper(), 7 );

      qs += '\n';
      // Write the line to file
      stream << qs;
    }
  stream << flush;

  this->addDiagnostic( "Write B-sensor configuration to logfile: done" );

  QApplication::restoreOverrideCursor();

  _configLogged = true;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::logData()
{
  if( !_bscan->connected() ) return;
  if( !_logFile.isOpen() ) return;

  // Also include the raw data in the logfile ?
  bool include_raw = (_checkBoxCalibrated->isChecked() &&
                      _checkBoxIncludeRaw->isChecked());
  // At most the data of 'n' B-sensor modules per logfile line
  int  sensors_per_line = _spinBoxSensorsPerLine->value();

  QString timestamp = QString::fromStdString( _bscan->
                                              dataTimeStampString( true ) );

  QTextStream stream( &_logFile );

  QString line, lineraw, id;
  QTableWidget *table = _tableWidgetBsensors;
  QTableWidgetItem *item_x, *item_y, *item_z, *item_t;
  unsigned int i;
  int order_i, alias;
  for( i=0; i<_bscan->bSensorCount(); ++i )
    {
      if( (i % sensors_per_line) == 0 )
        {
          // Start of a new line
          line.clear();
          line += ' ';
          line += timestamp;
          if( include_raw )
            {
              lineraw.clear();
              lineraw += '*';
              lineraw += timestamp;
            }
        }

      order_i = _bscan->bSensorOrderedIndex( i );
      alias   = _bscan->bSensorIdAlias( order_i );
      if( alias < 0 )
        //id = QString("%1").arg( i+1, 6 );
        id = QString("%1").arg( order_i+1, 6 );
      else
        id = QString("%1").arg( alias, 6 );
      line += id;
      if( include_raw ) lineraw += id;

      item_x = table->item( i, COL_X );
      item_y = table->item( i, COL_Y );
      item_z = table->item( i, COL_Z );
      item_t = table->item( i, COL_T );
      line += item_x->text().rightJustified( 10 );
      line += item_y->text().rightJustified( 10 );
      line += item_z->text().rightJustified( 10 );
      line += item_t->text().rightJustified( 10 );

      if( include_raw )
        {
          int32_t data[4], j;
          _bscan->bSensorDataRaw( data, i );
          for( j=0; j<4; ++j )
            lineraw += QString("%1").arg( data[j], 10 );
        }

      // Write the line to file if the number of B-sensors is reached
      if( ((i+1) % sensors_per_line) == 0 )
        {
          line += '\n';
          stream << line;
          if( include_raw )
            {
              lineraw += '\n';
              stream << lineraw;
            }
        }
    }
  // Write the last line to file if not yet done
  if( (i % sensors_per_line) != 0 )
    {
      line += '\n';
      stream << line;
      if( include_raw )
        {
          lineraw += '\n';
          stream << lineraw;
        }
    }
  stream << flush;

  ++_loggedSamples;
  _lineEditLogged->setText( QString::number(_loggedSamples) );
  // Automatically stop data-acquisition if the requested number
  // of samples has been reached
  if( _samplesToLog > 0 && _loggedSamples >= _samplesToLog )
    {
      this->stopLogging();
      _pushButtonStartStopLog->setChecked( false );
      this->stopReadout();
      _pushButtonStartStopReadout->setChecked( false );
      QMessageBox::information( _pushButtonStartStopLog,
                                "Log file closed / readout stopped",
                                "Requested number of samples reached" );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::addDiagnostic( const QString &qs )
{
  QString p;
  QTime time = QTime::currentTime();
  //p =  QTime::currentTime().toString();
  p =  time.toString();
  p += QString( ":%1" ).arg( time.msec(), 3, 10, QChar('0') );
  p += "  ";
  p += qs;
  _plainTextEditDiagnostic->appendPlainText( p );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::addDiagnostic( const char *str )
{
  this->addDiagnostic( QString(str) );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::clearDiagnostics()
{
  _plainTextEditDiagnostic->clear();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::hideDataLed()
{
  _lineEditDataLed->hide();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::hideErrLed()
{
  _lineEditErrLed->hide();
}

// ----------------------------------------------------------------------------
