/* ------------------------------------------------------------------------
File   : SocketCanInterface.cpp

Descr  : SocketCAN-based CAN-port/interface class implementation.

History: 12DEC.11; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>    // For 'struct ifreq' used below
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <string.h>

// ###Need root privilege to be able to make libsocketcan calls...
//#define USE_LIBSOCKETCAN
#ifdef USE_LIBSOCKETCAN
// Needed to add "extern C {}" to libsocketcan.h
// to be able to use the calls in a C++ program
#include "libsocketcan.h"
#endif // USE_LIBSOCKETCAN

#include "CANopen.h"
#include "SocketCanInterface.h"
#include "CanMessage.h"

const int    SocketCanInterface::CAN_INTF_DFLT    = 0;
const int    SocketCanInterface::CAN_BITRATE_DFLT = 125;
const string SocketCanInterface::MANUFACTURER_STR = string("SocketCAN");

// Some definitions for the _opStatus word
#define CAN_MSG_TIMEOUT 0x1000
#define CAN_MSG_NOTSENT 0x2000
#define CAN_ERR_ERRNO   0x8000

/* ------------------------------------------------------------------------ */

SocketCanInterface::SocketCanInterface( int  port_nr,
					int  kbitrate,
					bool open_now )
  : CanInterface( MANUFACTURER_STR, SOCKET_INTF_TYPE, port_nr, kbitrate ),
    _errNo( 0 ), _sockCanErrFrame(0)
{
  if( !this->setBusSpeed( kbitrate ) ) _kbitRate = 125;

  if( open_now ) this->open();

  // Start time, used for time-stamping received messages
  struct timeval tval;
  gettimeofday( &tval, 0 );
  _startTimeSec = tval.tv_sec;
  //_startTimeSec = 0;
}

/* ------------------------------------------------------------------------ */

SocketCanInterface::~SocketCanInterface()
{
  this->close();
  if( _sock ) ::close( _sock );
}

/* ------------------------------------------------------------------------ */

vector<int> SocketCanInterface::ports()
{
  // Create a list of available port numbers
  vector<int> portnumbers;

  // Open a socket for subsequent operations
  int s = socket( PF_CAN, SOCK_RAW, CAN_RAW );
  if( s < 0 ) return portnumbers;

  // Count ports by trying to read their interface index
  ostringstream oss;
  for( int portnr=0; portnr<64; ++portnr )
    {
      oss << "can" << portnr;
      struct ifreq ifr;
      strncpy( ifr.ifr_name, oss.str().c_str(), IFNAMSIZ );
      if( ioctl( s, SIOCGIFINDEX, &ifr ) != -1 )
	portnumbers.push_back( portnr );
      oss.str( "" ); // Clear string
    }
  ::close( s );
  return portnumbers;
}

/* ------------------------------------------------------------------------ */

bool SocketCanInterface::setBusSpeed( int kbitrate )
{
  // Change of bus speed only allowed when the port is not open
  if( this->isOpen() ) return false;

  // Only certain baudrates are allowed
  if( !(kbitrate == 1000 || kbitrate == 800 ||
	kbitrate == 500  || kbitrate == 250 ||
	kbitrate == 125  || kbitrate == 50 ||
	kbitrate == 20   || kbitrate == 10) )
    return false;

  _kbitRate = kbitrate;
  return true;
}

/* ------------------------------------------------------------------------ */

bool SocketCanInterface::open()
{
  _opStatus = 0;

  // Open a socket
  _sock = socket( PF_CAN, SOCK_RAW, CAN_RAW );
  if( _sock < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed to open socket: " );
      _initResultString += this->errString();
      return false;
    }

  int bitrate = _kbitRate * 1000;

#ifdef USE_LIBSOCKETCAN
  // Initialize the CAN-channel, using iproute2 "ip" utility
#else
  // Initialize the CAN-channel, using libsocketcan calls
#endif // USE_LIBSOCKETCAN
  ostringstream oss;

#ifdef USE_LIBSOCKETCAN
  oss << "can" << _portNumber;
  if( can_set_bitrate( oss.str().c_str(), bitrate ) != 0 )
    {
      _errNo = errno;
      _initResultString = string( "Failed: can_set_bitrate(); " );
      _initResultString += this->errString();
      _opStatus = CAN_ERR_ERRNO;
      return false;
    }
  oss.str( "" ); // Clear string
#else
  // To be able to set the bitrate the interface must be 'DOWN'
  // (so if it already 'up' and in use, this call does not change the bitrate)
  oss << "sudo /sbin/ip link set can" << _portNumber
      << " type can bitrate " << bitrate;
  if( system( oss.str().c_str() ) != 0 )
    {
      _errNo = errno;
      _initResultString = string( "Failed: " );
      _initResultString += oss.str() + string( "; " );
      _initResultString += this->errString();
      // ### For PEAK interface 'ip set bitrate' does not work,
      //     (do it -for 125kbit/s- using: echo "i 0x031C" >/dev/pcanusb0)
      //     so don't exit here for now..
      //_opStatus = CAN_ERR_ERRNO;
      //return false;
    }
  oss.str( "" ); // Clear string
#endif // USE_LIBSOCKETCAN

//#define MY_SET_RESTART_MS
#ifdef MY_SET_RESTART_MS
  // To be able to set 'restart-ms' the interface must be 'DOWN'.
  // From SocketCAN documentation:
  // A device may enter the "bus-off" state if too much errors occurred on
  // the CAN bus. Then no more messages are received or sent. An automatic
  // bus-off recovery can be enabled by setting the "restart-ms" to a
  // non-zero value, e.g.:
  oss << "sudo /sbin/ip link set can" << _portNumber
      << " type can restart-ms 500";
  if( system( oss.str().c_str() ) != 0 )
    {
      _errNo = errno;
      _initResultString = string( "Failed: " );
      _initResultString += oss.str() + string( "; " );
      _initResultString += this->errString();
      // ### Some interfaces may not support this setting?
      //     so don't exit here for now..
      //_opStatus = CAN_ERR_ERRNO;
      //return false;
    }
  oss.str( "" ); // Clear string
#endif // MY_SET_RESTART_MS

  // Set the link to 'up', if not up already
  // (if already 'up' system() call results in message:
  // "RTNETLINK answers: Device or resource busy"
  oss << "sudo /sbin/ip link set can" << _portNumber << " up";
  if( system( oss.str().c_str() ) != 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed: " );
      _initResultString += oss.str() + string( "; " );
      _initResultString += this->errString();
      return false;
    }
  oss.str( "" ); // Clear string

  oss << "can" << _portNumber;
  struct ifreq ifr;
  strncpy( ifr.ifr_name, oss.str().c_str(), IFNAMSIZ );
  if( ioctl( _sock, SIOCGIFINDEX, &ifr ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed to get CAN port index " );
      _initResultString += oss.str();
      _initResultString += string( " by ioctl(SIOCGIFINDEX); " );
      _initResultString += this->errString();
      return false;
    }

  // Bind the interface index found to the socket
  struct sockaddr_can addr;
  addr.can_family  = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;
  if( bind( _sock, (struct sockaddr *) &addr, sizeof(addr) ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed to bind socket " );
      _initResultString += oss.str() + string( ": " );
      _initResultString += this->errString();
      return false;
    }

  // Enable reception of errors (all possible => CAN_ERR_MASK)
  //can_err_mask_t err_mask = CAN_ERR_MASK;
  can_err_mask_t err_mask = (CAN_ERR_TX_TIMEOUT | CAN_ERR_CRTL |
			     CAN_ERR_PROT | CAN_ERR_TRX |
			     CAN_ERR_BUSOFF | CAN_ERR_BUSERROR);
  if( setsockopt( _sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER,
		  &err_mask, sizeof(err_mask) ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString =
	string( "Failed in setsockopt(CAN_RAW_ERR_FILTER) " );
      _initResultString += oss.str() + string( ": " );
      _initResultString += this->errString();
      return false;
    }

  // Do not allow the interface to be used by more than one application
  // ###DOES NOT WORK? WHY NOT?
  int reuseaddr = 0;
  if( setsockopt( _sock, SOL_SOCKET, SO_REUSEADDR,
		  &reuseaddr, sizeof(reuseaddr) ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed in setsockopt(SO_REUSEADDR) " );
      _initResultString += oss.str() + string( ": " );
      _initResultString += this->errString();
      return false;
    }

//#define MY_SET_SNDBUF_SIZE
#ifdef MY_SET_SNDBUF_SIZE
  // Increase send buffer size
  // ### Rather forget about this because:
  // ### SYSTEC interface: if we do this, we hang on buffer full.
  // ### PEAK interface: if we do this, even with bufsz=0 the outbuffer
  //          does not overflow, if we don't do this it overflows
  //          ("errno=105: No buffer space available").
  //int bufsz = 0x10000; // 64Kbyte ###NO GOOD ?
  //int bufsz = 0x8000; // 32Kbyte
  int bufsz = 0; // n bytes or n CAN frames ?
  if( setsockopt( _sock, SOL_SOCKET, SO_SNDBUF,
		  &bufsz, sizeof(bufsz) ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString =
	string( "Failed in setsockopt(SO_SNDBUF) " );
      _initResultString += oss.str() + string( ": " );
      _initResultString += this->errString();
      return false;
    }
#endif

//#define MY_ENABLE_TIMESTAMPS
#ifdef MY_ENABLE_TIMESTAMPS
  // Enable time-stamps
  // (to be read out as a control message using recvmsg())
  // ### No need if we use ioctl(): ioctl( _sock, SIOCGSTAMP, &tval );
  int timestamp_on = 1;
  if( setsockopt( _sock, SOL_SOCKET, SO_TIMESTAMP,
		  &timestamp_on, sizeof(timestamp_on) ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      _initResultString = string( "Failed in setsockopt(SO_TIMESTAMP) " );
      _initResultString += oss.str() + string( ": " );
      _initResultString += this->errString();
      return false;
    }
#endif

  _isOpen = true;
  return true;
}

/* ------------------------------------------------------------------------ */

bool SocketCanInterface::close()
{
  bool result = true;
  this->awaitTransmission( 100 ); // In case of any remaining messages to send

  _opStatus = 0;
  _isOpen = false;

  /* Not supported?:
  if( shutdown( _sock, SHUT_RDWR ) != 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      result = false;
    }
  */
  if( ::close( _sock ) != 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      result = false;
    }

  // Set the link 'down': is that a good idea? what about other active apps?
  /*
  ostringstream oss;
  oss << "sudo /sbin/ip link set can" << _portNumber << " down";
  if( system( oss.str().c_str() ) != 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      return false;
    }
  */
  return result;
}

/* ------------------------------------------------------------------------ */

bool SocketCanInterface::send( CanMessage &can_msg )
{
  _opStatus = 0;

  struct can_frame frame;
  frame.can_id = can_msg.cobId();
  if( can_msg.isRemoteFrame() ) frame.can_id |= CAN_RTR_FLAG;
  // ### I'm afraid CAN_ERR_FLAG does NOT indicate a CAN Error Frame
  //     in the CAN-sense of the word, but is an 8-byte message frame
  //     containing bytes/bits indicating various interface and protocol errors
  //     (and thus is *not* something that can be sent...)
  if( can_msg.isErrFrame() ) frame.can_id |= CAN_ERR_FLAG | CAN_ERR_BUSERROR;
  frame.can_dlc = can_msg.dlc();
  memcpy( frame.data, can_msg.data(), can_msg.dlc() );

  bool result = true;
  int nbytes = write( _sock, &frame, sizeof(struct can_frame) );
  if( nbytes < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
      result = false;
    }
  else if( nbytes < (int) sizeof(struct can_frame) )
    {
      _errNo = errno;
      _opStatus = CAN_MSG_NOTSENT;
      result = false;
    }

  return result;
}

/* ------------------------------------------------------------------------ */

CanMessage* SocketCanInterface::receive( unsigned int timeout_ms )
{
  // Read the next available message;
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  if( !_canMsgs.empty() )
    {
      // Get the first message from the list
      pMsg = *(_canMsgs.begin());

      // Remove the message pointer from the list
      _canMsgs.pop_front();

      // And return it
      return pMsg;
    }

  if( timeout_ms != 0 )
    {
      _opStatus = 0;

      // Set the requested time-out on the socket
      struct timeval tval;
      tval.tv_sec  = timeout_ms/1000;
      tval.tv_usec = (timeout_ms % 1000) * 1000;
      if( setsockopt( _sock, SOL_SOCKET, SO_RCVTIMEO,
		      static_cast<void *> (&tval),
		      sizeof(struct timeval) ) != 0 )
	{
	  _errNo = errno;
	  _opStatus = CAN_ERR_ERRNO;
	  return 0;
	}

      // Receive a message or time-out
      struct can_frame frame;
      int nbytes = read( _sock, &frame, sizeof(struct can_frame) );
      if( nbytes < 0 )
	{
	  if( errno != EAGAIN )
	    {
	      _errNo = errno; // Not a time-out!
	      _opStatus = CAN_ERR_ERRNO;
	    }
	  else
	    {
	      _opStatus = CAN_MSG_TIMEOUT; // Time-out
	    }
	  return pMsg;
	}
      else
	{
	  // Check for a SocketCAN error frame... (a special 8-byte message)
	  if( frame.can_id & CAN_ERR_FLAG )
	    {
	      // Fill 1st and 2nd byte
	      _sockCanErrFrame = (frame.can_id & CAN_ERR_MASK) & 0xFFFF;
	      // 3rd byte
	      if( frame.can_id & CAN_ERR_CRTL )
		{
		  // According to can/error.h more info in data[1]
		  _sockCanErrFrame |= (((unsigned long) (frame.data[1]) &
					0xFF) << 16);
		}
	      // 4th byte
	      if( frame.can_id & CAN_ERR_TRX )
		{
		  // According to can/error.h more info in data[4]
		  _sockCanErrFrame |= (((unsigned long) (frame.data[4]) &
					0xFF) << 24);
		}
	      return 0;
	    }
	  // Add the time: 'candump' tool uses 'socket ancillary data'
	  // using recvmsg() (see 'man recvmgs' and 'man cmsg'),
	  // but seems to me to be too complicated...
	  // so for the time being use the system time
	  //gettimeofday( &tval, 0 ); // Not very precise!
	  ioctl( _sock, SIOCGSTAMP, &tval );
	  unsigned long t_ms = ((tval.tv_sec - _startTimeSec)*1000 +
				tval.tv_usec/1000);
	  bool rtr = ((frame.can_id & CAN_RTR_FLAG) != 0);
	  pMsg = new CanMessage( frame.can_id & CAN_SFF_MASK, rtr,
				 frame.can_dlc, frame.data, t_ms );
	}
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

CanMessage* SocketCanInterface::receiveSpecific( int          cob_id,
						 unsigned int timeout_ms )
{
  // Read the next available message having COB-ID 'cob_id';
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  // Is this message perhaps already available in the currently held list ?
  pMsg = this->getMsg( cob_id );

  // Message found ?
  if( pMsg ) return pMsg;

  if( timeout_ms != 0 )
    {
      _opStatus = 0;

      // Set the requested time-out on the socket
      struct timeval tval;
      tval.tv_sec  = timeout_ms/1000;
      tval.tv_usec = (timeout_ms % 1000) * 1000;
      if( setsockopt( _sock, SOL_SOCKET, SO_RCVTIMEO,
		      static_cast<void *> (&tval),
		      sizeof(struct timeval) ) != 0 )
	{
	  _errNo = errno;
	  _opStatus = CAN_ERR_ERRNO;
	  return 0;
	}

      struct can_frame frame;
      int              nbytes;
      unsigned long    t_ms;
      bool             rtr;
      while( !pMsg )
	{
	  // Receive a message or time-out
	  nbytes = read( _sock, &frame, sizeof(struct can_frame) );
	  if( nbytes < 0 )
	    {
	      if( errno != EAGAIN )
		{
		  _errNo = errno; // Not a time-out!
		  _opStatus = CAN_ERR_ERRNO;
		}
	      else
		{
		  _opStatus = CAN_MSG_TIMEOUT; // Time-out
		}

	      // Interrupt the while-loop
	      //break;
	      return 0;
	    }
	  else
	    {
	      // Check if it is a SocketCAN error frame...
	      if( frame.can_id & CAN_ERR_FLAG )
		{
		  // Fill 1st and 2nd byte
		  _sockCanErrFrame = (frame.can_id & CAN_ERR_MASK) & 0xFFFF;
		  // 3rd byte
		  if( frame.can_id & CAN_ERR_CRTL )
		    {
		      // According to can/error.h more info in data[1]
		      _sockCanErrFrame |= (((int) (frame.data[1]) & 0xFF) <<
					   16);
		    }
		  // 4th byte
		  if( frame.can_id & CAN_ERR_TRX )
		    {
		      // According to can/error.h more info in data[4]
		      _sockCanErrFrame |= (((int) (frame.data[4]) & 0xFF) <<
					   24);
		    }
		  return 0; // Make sure the user will check for errors
		}

	      // Add the time: 'candump' tool uses 'socket ancillary data'
	      // using recvmsg() (see 'man recvmgs' and 'man cmsg'),
	      // but seems to me to be too complicated...
	      //gettimeofday( &tval, 0 ); // Not very precise!
	      ioctl( _sock, SIOCGSTAMP, &tval );
	      t_ms = ((tval.tv_sec - _startTimeSec)*1000 + tval.tv_usec/1000);
	      rtr = ((frame.can_id & CAN_RTR_FLAG) != 0);
	      pMsg = new CanMessage( frame.can_id & CAN_SFF_MASK, rtr,
				     frame.can_dlc, frame.data, t_ms );

	      // Is this a message we are looking for ?
	      if( (int) frame.can_id != cob_id )
		{
		  // No, so append (a pointer to) this message to the list
		  // of unhandled messages
		  _canMsgs.push_back( pMsg );

		  // Reset the pointer
		  pMsg = 0;
		}
	    }
	}
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

void SocketCanInterface::bufferReceivedMsgs()
{
  _opStatus = 0;

  // Get all available received CAN messages from the interface
  // and store them in the list

  fd_set read_fds;
  // Add the socket file descriptor to the 'fd_set' for the call to 'select'
  FD_ZERO( &read_fds );
  FD_SET( _sock, &read_fds );

  // Going to poll the socket, so set time-out to 0
  struct timeval tval, tval0;
  tval0.tv_sec = 0; tval0.tv_usec = 0;

  // Poll the socket
  CanMessage      *pMsg;
  struct can_frame frame;
  int              nbytes;
  int              result;
  while( (result = select( _sock+1, &read_fds, 0, 0, &tval0 )) > 0 )
    {
      // Receive a message or time-out
      nbytes = read( _sock, &frame, sizeof(struct can_frame) );
      if( nbytes < 0 )
	{
	  if( errno != EAGAIN )
	    {
	      _errNo = errno; // Not a time-out!
	      _opStatus = CAN_ERR_ERRNO;
	    }
	  else
	    {
	      _opStatus = CAN_MSG_TIMEOUT; // Time-out !?
	    }

	  // Interrupt the while-loop
	  break;
	}
      else
	{
	  // Check if it is a SocketCAN error frame...
	  if( frame.can_id & CAN_ERR_FLAG )
	    {
	      // Fill 1st and 2nd byte
	      _sockCanErrFrame = (frame.can_id & CAN_ERR_MASK) & 0xFFFF;
	      // 3rd byte
	      if( frame.can_id & CAN_ERR_CRTL )
		{
		  // According to can/error.h more info in data[1]
		  _sockCanErrFrame |= (((unsigned long) (frame.data[1]) &
					0xFF) << 16);
		}
	      // 4th byte
	      if( frame.can_id & CAN_ERR_TRX )
		{
		  // According to can/error.h more info in data[4]
		  _sockCanErrFrame |= (((unsigned long) (frame.data[4]) &
					0xFF) << 24);
		}
	    }
	  else
	    {
	      // Add the time: 'candump' tool uses 'socket ancillary data'
	      // using recvmsg() (see 'man recvmgs' and 'man cmsg'),
	      // but seems to me to be too complicated...
	      // so for the time being use the system time
	      unsigned long t_ms;
	      //gettimeofday( &tval, 0 ); // Not very precise!
	      // Alternatively, use ioctl():
	      ioctl( _sock, SIOCGSTAMP, &tval );
	      t_ms = ((tval.tv_sec - _startTimeSec)*1000 + tval.tv_usec/1000);
	      bool rtr = ((frame.can_id & CAN_RTR_FLAG) != 0);
	      pMsg = new CanMessage( frame.can_id & CAN_SFF_MASK, rtr,
				     frame.can_dlc, frame.data, t_ms );

	      // Append (a pointer to) the message to the list
	      _canMsgs.push_back( pMsg );
	    }
	}
    }
  if( result < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
    }
}

/* ------------------------------------------------------------------------ */

bool SocketCanInterface::awaitTransmission( unsigned int timeout_ms )
{
  _opStatus = 0;

  // ###NB: the following does not work when it comes to waiting
  //        for the CAN messages to be sent *on the actual CAN bus*;
  //        it only means the socketcan driver can accept more messages 
  fd_set write_fds;
  // Add the socket file descriptor to the 'fd_set' for the call to 'select'
  FD_ZERO( &write_fds );
  FD_SET( _sock, &write_fds );
  // Set time-out value
  struct timeval tval;
  tval.tv_sec  = timeout_ms/1000;
  tval.tv_usec = (timeout_ms - (tval.tv_sec*1000))*1000;
  select( _sock+1, 0, &write_fds, 0, &tval );

  // Wait for all messages in the queue to be transmitted on the bus: how?
  // ....
  usleep( timeout_ms*1000 ); // For now, simply wait for a while
  return true;
}

/* ------------------------------------------------------------------------ */

void SocketCanInterface::flush()
{
  // Flush transmit and (then) receive buffers
  _opStatus = 0;

  // Transmit buffers: new feature added by SYSTEC at ATLAS-DCS request;
  // go to 'flush mode'
  //struct ifreq ifr;
  int dummy;
  if( ioctl( _sock, SIOCDEVPRIVATE, &dummy ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
    }

  // Receive buffers: simply read out any messages available
  // (this in fact makes the message list clear below redundant)
  CanMessage *msg;
  while( (msg = this->receive()) != 0 ) delete msg;

  // Clear the received messages list:
  // delete the allocated CAN messages, then clear the (pointer) list
  list<CanMessage *>::iterator it;
  for( it=_canMsgs.begin(); it!=_canMsgs.end(); ++it )
    delete (*it);
  _canMsgs.clear();

  // Transmit buffer flush: exit 'flush mode'
  if( ioctl( _sock, SIOCDEVPRIVATE+1, &dummy ) < 0 )
    {
      _errNo = errno;
      _opStatus = CAN_ERR_ERRNO;
    }
}

/* ------------------------------------------------------------------------ */

unsigned long SocketCanInterface::state()
{
  // Return the state of the interface, i.e. for SocketCAN we return
  // the information returned by a socalled 'error frame'
  return _sockCanErrFrame;
}

/* ------------------------------------------------------------------------ */

string SocketCanInterface::stateString( unsigned long state )
{
  // Return a string describing the state of the interface/port;
  // in fact _sockCanErrFrame contains the contents of the 'error frame'
  // (HenkB: this is a misnomer in the SocketCAN library,
  //  denoting a 'struct can_frame' containing error information;
  //  has nothing to do with a CAN Error Frame so it seems!)
  //
  // See /usr/include/linux/can/error.h:
  // /* error class (mask) in can_id */
  // CAN_ERR_TX_TIMEOUT   0x00000001U /* TX timeout (by netdevice driver) */
  // CAN_ERR_LOSTARB      0x00000002U /* lost arbitration    / data[0]    */
  // CAN_ERR_CRTL         0x00000004U /* controller problems / data[1]    */
  // CAN_ERR_PROT         0x00000008U /* protocol violations / data[2..3] */
  // CAN_ERR_TRX          0x00000010U /* transceiver status  / data[4]    */
  // CAN_ERR_ACK          0x00000020U /* received no ACK on transmission */
  // CAN_ERR_BUSOFF       0x00000040U /* bus off */
  // CAN_ERR_BUSERROR     0x00000080U /* bus error (may flood!) */
  // CAN_ERR_RESTARTED    0x00000100U /* controller restarted */
  // /* arbitration lost in bit ... / data[0] */
  // /* error status of CAN-controller / data[1] */
  // /* error in CAN protocol (type) / data[2] */
  // /* error in CAN protocol (location) / data[3] */
  // /* error status of CAN-transceiver / data[4] */
  // /* controller specific additional information / data[5..7] */
  ostringstream oss;
  if( state & CAN_ERR_TX_TIMEOUT ) oss << "ERR_TX_TIMEOUT ";
  if( state & CAN_ERR_LOSTARB    ) oss << "ERR_LOSTARB ";
  if( state & CAN_ERR_CRTL )       oss << "ERR_CRTL (see byte 2) ";
  if( state & CAN_ERR_PROT )       oss << "ERR_PROT ";
  if( state & CAN_ERR_TRX )        oss << "ERR_TRX (see byte 3) ";
  if( state & CAN_ERR_ACK )        oss << "ERR_ACK ";
  if( state & CAN_ERR_BUSOFF )     oss << "ERR_BUSOFF ";
  if( state & CAN_ERR_BUSERROR )   oss << "ERR_BUSERR ";
  if( state & CAN_ERR_RESTARTED )  oss << "ERR_RESTARTED ";
  return oss.str();
}

/* ------------------------------------------------------------------------ */

string SocketCanInterface::errString()
{
  // Returns a string describing the result of the last CAN interface operation
  ostringstream oss;
  if( _opStatus != 0 )
    {
      if( _opStatus & CAN_MSG_TIMEOUT ) oss << "MY_MSG_TIMEOUT ";
      if( _opStatus & CAN_MSG_NOTSENT ) oss << "MY_MSG_NOTSENT ";
      if( _opStatus & CAN_ERR_ERRNO )   oss << "MY_ERR_ERRNO ";
      if( _errNo != 0 )
	oss << "errno=" << _errNo << ": " << string( strerror(_errNo) );
    }
  else
    {
      oss << "No error";
    }
  return oss.str();
}

/* ------------------------------------------------------------------------ */

string SocketCanInterface::errString( const char *context )
{
  // Returns a string describing the result of the last CAN interface operation
  // preceded by a 'context' string
  return( string( context ) + ": " + this->errString() );
}

/* ------------------------------------------------------------------------ */

#ifdef WIN32
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

extern "C" MY_EXPORT CanInterface *newSocketCanInterface( int  port_nr,
							  int  kbitrate,
							  bool open_now )
{
  return new SocketCanInterface( port_nr, kbitrate, open_now );
}

/* ------------------------------------------------------------------------ */

extern "C" MY_EXPORT int socketCanInterfacePorts( int *port_numbers )
{
  vector<int> ports = SocketCanInterface::ports();
  for( unsigned int i=0; i<ports.size(); ++i ) port_numbers[i] = ports[i];
  return ports.size();
}

/* ------------------------------------------------------------------------ */
