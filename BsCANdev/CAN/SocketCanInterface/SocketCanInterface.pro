#
# Project file for the SocketCanInterface library
# (Windows only)
#
# To generate a Visual Studio project:
#   qmake -t vclib SocketCanInterface.pro
# To generate a Makefile:
#   qmake SocketCanInterface.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = SocketCanInterface

# Create a shared library
CONFIG += shared qt thread warn_on exceptions debug_and_release

QT -= gui
QT += core

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

LIBS += -lCANopen

INCLUDEPATH += ../CANopen

# Using libsocketcan...
#DEFINES += USE_LIBSOCKETCAN
#INCLUDEPATH += ../../../libsocketcan-0.0.8/include
#LIBS += -L../../../libsocketcan-0.0.8/src/.libs
#LIBS += -lsocketcan

SOURCES += SocketCanInterface.cpp
HEADERS += SocketCanInterface.h
HEADERS += ../CANopen/CanInterface.h
HEADERS += ../CANopen/CanMessage.h
HEADERS += ../CANopen/CANopen.h

