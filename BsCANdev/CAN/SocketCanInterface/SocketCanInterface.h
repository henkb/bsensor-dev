/* ------------------------------------------------------------------------
File   : SocketCanInterface.h

Descr  : CAN-port/interface class based on SocketCAN drivers.

History: 12DEC.11; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef SOCKETCANINTERFACE_H
#define SOCKETCANINTERFACE_H

#include "CanInterface.h"

/* ------------------------------------------------------------------------ */

class SocketCanInterface: public CanInterface
{
 public:
  SocketCanInterface( int  port_no  = CAN_INTF_DFLT,
		      int  bitrate  = CAN_BITRATE_DFLT,
		      bool open_now = true );

  ~SocketCanInterface();

  static std::vector<int> ports();

  bool          setBusSpeed( int kbitrate );

  bool          open();

  bool          close();

  bool          send( CanMessage &can_msg );

  CanMessage*   receive( unsigned int timeout_ms = 0 );

  CanMessage*   receiveSpecific( int cob_id, unsigned int timeout_ms = 0 );

  bool          awaitTransmission( unsigned int timeout_ms );

  void          flush();

  unsigned long state();
  std::string   stateString( unsigned long state );

  std::string   errString();
  std::string   errString( const char *context );

 private:
  void          bufferReceivedMsgs();

  // Handle to the actual device, which in this case is a socket descriptor
  int _sock;

  // Storage for Linux errno
  int _errNo;

  // Storage for SocketCAN 'error frame' contents
  // (which is *not* a CAN-bus err frame), returned by state()/stateString()
  unsigned long _sockCanErrFrame;

  // Start time, in seconds
  unsigned long _startTimeSec;

  // Some convenient defaults
  static const int         CAN_INTF_DFLT;
  static const int         CAN_BITRATE_DFLT;
  static const std::string MANUFACTURER_STR;
};

/* ------------------------------------------------------------------------ */
#endif // SOCKETCANINTERFACE_H
