/* ------------------------------------------------------------------------
File   : SystecInterface.cpp

Descr  : Kvaser CAN-port/interface class implementation.

History: 06NOV.11; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#ifdef WIN32
#include <Windows.h>
#endif

#include <algorithm>
#include <sstream>
#include <iomanip>
using namespace std;

#include "CANopen.h"
#include "SystecInterface.h"
#include "CanMessage.h"

bool SystecInterface::_portInUse[USBCAN_MAX_MODULES*2] =
  { false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false };

tUcanHandle SystecInterface::_handleInUse[USBCAN_MAX_MODULES];

const int    SystecInterface::CAN_INTF_DFLT    = 0;
const int    SystecInterface::CAN_BITRATE_DFLT = 125;
const string SystecInterface::MANUFACTURER_STR = string("SYSTEC");

/* Indices for bit rates */
#define KB1000  0
#define KB800   1
#define KB500   2
#define KB250   3
#define KB125   4
#define KB50    5
#define KB20    6
#define KB10    7

// From Usbcan32.h, using the 85%/87.5% sampling points:
// "pre-defined baudrate values for Multiport 3004006, USB-CANmodul1 3204000 or
//  USB-CANmodul2 3204002 (use function UcanInitCanEx or UcanSetBaudrateEx)"
const unsigned long BAUD_REG[] = { USBCAN_BAUDEX_SP2_1MBit,
				   USBCAN_BAUDEX_SP2_800kBit,
				   USBCAN_BAUDEX_SP2_500kBit,
				   USBCAN_BAUDEX_SP2_250kBit,
				   USBCAN_BAUDEX_SP2_125kBit,
				   USBCAN_BAUDEX_SP2_50kBit,
				   USBCAN_BAUDEX_SP2_20kBit,
				   USBCAN_BAUDEX_SP2_10kBit };

/* ------------------------------------------------------------------------ */

SystecInterface::SystecInterface( int  port_nr,
				  int  kbitrate,
				  bool open_now )
  : CanInterface( MANUFACTURER_STR, SYSTEC_INTF_TYPE, port_nr, kbitrate )
{
  // Port number consists of the module number times 2 plus
  // the CAN channel number (only channels 0 or 1 are possible).
  // Port count in the software starts at 0
  _modNumber  = port_nr / 2;
  _chanNumber = port_nr & 1;
  if( _modNumber >= USBCAN_MAX_MODULES || _modNumber < 0 ||
      !(_chanNumber == 0 || _chanNumber == 1) )
    {
      if( _modNumber >= USBCAN_MAX_MODULES )
	_opStatus |= USBCAN_ERR_MAXMODULES;
      if( !(_chanNumber == 0 || _chanNumber == 1) )
	_opStatus |= USBCAN_ERR_ILLCHANNEL;

      _initResultString = this->errString( "CAN port parameter" );
      return;
    }

  // Is the other channel already in use ?
  bool in_use;
  if( _chanNumber == 0 )
    in_use = _portInUse[_modNumber*2+1];
  else
    in_use = _portInUse[_modNumber*2+0];

  // If not yet in use, initialize the hardware
  UCANRET stat;
  if( !in_use )
    {
      stat = UcanInitHardwareEx( &_handle, _modNumber, 0, 0 );
      if( stat != USBCAN_SUCCESSFUL )
	{
	  _opStatus = stat;

	  _initResultString = this->errString( "uCanInitHardwareEx" );
	  return;
	}
      _handleInUse[_modNumber] = _handle;
    }
  else
    {
      _handle = _handleInUse[_modNumber];
    }
  _portInUse[_modNumber*2 + _chanNumber] = true;

  if( !this->setBusSpeed( kbitrate ) )
    {
      _kbitRate = 125;
      _kbIndex  = KB125;
    }

  if( open_now )
    {
      // Initialize the CAN-channel
      tUcanInitCanParam params;
      memset( &params, 0, sizeof(params) );
      params.m_dwSize               = sizeof( params );
      params.m_bMode                = kUcanModeNormal;
      params.m_bBTR0                = (BYTE) ((USBCAN_BAUD_USE_BTREX &
					       0xFF00) >> 8);
      params.m_bBTR1                = (BYTE) ((USBCAN_BAUD_USE_BTREX &
					       0x00FF) >> 0);
      params.m_bOCR                 = USBCAN_OCR_DEFAULT;
      params.m_dwAMR                = USBCAN_AMR_ALL;
      params.m_dwACR                = USBCAN_ACR_ALL;
      // Match selected bitrate
      params.m_dwBaudrate           = BAUD_REG[_kbIndex];
      params.m_wNrOfRxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
      params.m_wNrOfTxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
      stat = UcanInitCanEx2( _handle, _chanNumber, &params );
      if( stat != USBCAN_SUCCESSFUL )
	{
	  _opStatus = stat;

	  _initResultString = this->errString( "uCanInitCanEx2" );

	  if( !in_use ) UcanDeinitHardware( _handle );

	  _portInUse[_modNumber*2 + _chanNumber] = false;

	  return;
	}
      _isOpen = true;
    }
}

/* ------------------------------------------------------------------------ */

SystecInterface::~SystecInterface()
{
  if( !_portInUse[_modNumber*2 + _chanNumber] ) return;

  this->close();

  // If the other channel is not in use, 'deinit' the hardware
  if( _chanNumber == 0 )
    {
      _portInUse[_modNumber*2+0] = false;
      if( !_portInUse[_modNumber*2+1] ) UcanDeinitHardware( _handle );
    }
  else
    {
      _portInUse[_modNumber*2+1] = false;
      if( !_portInUse[_modNumber*2+0] ) UcanDeinitHardware( _handle );
    }
}

/* ------------------------------------------------------------------------ */

vector<int> SystecInterface::ports()
{
  // Create a list of available port numbers
  vector<int>         portnumbers;
  vector<tUcanHandle> handles;
  UCANRET             stat;
  tUcanHandle         handle;
  tUcanHardwareInfoEx hwinfo;
  int                 modnr;

  // Visit@SYSTEC tip: use UcanInitHardware() using modnr=255, to get
  // the first available module, keep the handle and call UcanInitHardware()
  // again using modnr=255, and repeat until no modules available,
  // that's when all available modules have been found.
  // Use UcanGetHardwareInfoEx2() to find out the module number etc.
  bool available = true;
  while( available )
    {
      stat = UcanInitHardware( &handle, USBCAN_ANY_MODULE, 0 );
      if( stat == USBCAN_SUCCESSFUL )
	{
	  // Keep the handle (and the module in use..)
	  handles.push_back( handle );

	  memset( &hwinfo, 0, sizeof(hwinfo) );
	  hwinfo.m_dwSize = sizeof( hwinfo );
	  // Get the extended hardware information
	  stat = UcanGetHardwareInfoEx2( handle, &hwinfo, 0, 0 );
	  if( stat == USBCAN_SUCCESSFUL )
	    {
	      modnr = hwinfo.m_bDeviceNr;
	      // NB: the ports on a module are numbered 0 and 1
	      portnumbers.push_back( 2*modnr + 0 );
	      // Check whether two CAN-channels are supported;
	      // if yes, add a port to our list
	      if( USBCAN_CHECK_SUPPORT_TWO_CHANNEL( &hwinfo ) )
		portnumbers.push_back( modnr*2 + 1 );
	    }
	}
      else
	{
	  available = false;
	}
    }
  // Close again all modules that we opened in this function
  for( unsigned int i=0; i<handles.size(); ++i )
    stat = UcanDeinitHardware( handles[i] );

  // Also add to the list ports not-in-use from any currently opened module
  // (which are not found by the procedure above)
  for( modnr=0; modnr<USBCAN_MAX_MODULES; ++modnr )
    {
      if( _portInUse[modnr*2+0] && !_portInUse[modnr*2+1] )
	portnumbers.push_back( modnr*2 + 1 );
      else if( !_portInUse[modnr*2+0] && _portInUse[modnr*2+1] )
	portnumbers.push_back( modnr*2 + 0 );
    }

  return portnumbers;
}

/* ------------------------------------------------------------------------ */

bool SystecInterface::setBusSpeed( int kbitrate )
{
  // Determine the index for the bit-timing parameters
  // dependent on the baudrate selected from a predefined set of baudrates

  // Change of bus speed only allowed when the port is not open
  if( this->isOpen() ) return false;

  // Only certain baudrates are allowed
  bool result = true;
  if( kbitrate == 1000 )     _kbIndex = KB1000;
  else if( kbitrate == 800 ) _kbIndex = KB800;
  else if( kbitrate == 500 ) _kbIndex = KB500;
  else if( kbitrate == 250 ) _kbIndex = KB250;
  else if( kbitrate == 125 ) _kbIndex = KB125;
  else if( kbitrate == 50 )  _kbIndex = KB50;
  else if( kbitrate == 20 )  _kbIndex = KB20;
  else if( kbitrate == 10 )  _kbIndex = KB10;
  else result = false;

  if( result ) _kbitRate = kbitrate;
  return result;
}

/* ------------------------------------------------------------------------ */

bool SystecInterface::open()
{
  _opStatus = 0;

  // Initialize the CAN-channel
  tUcanInitCanParam params;
  memset( &params, 0, sizeof(params) );
  params.m_dwSize               = sizeof( params );
  params.m_bMode                = kUcanModeNormal;
  params.m_bBTR0                = (BYTE)((USBCAN_BAUD_USE_BTREX&0xFF00)>>8);
  params.m_bBTR1                = (BYTE)((USBCAN_BAUD_USE_BTREX&0x00FF)>>0);
  params.m_bOCR                 = USBCAN_OCR_DEFAULT;
  params.m_dwAMR                = USBCAN_AMR_ALL;
  params.m_dwACR                = USBCAN_ACR_ALL;
  params.m_dwBaudrate           = BAUD_REG[_kbIndex]; // Match selected bitrate
  params.m_wNrOfRxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
  params.m_wNrOfTxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
  UCANRET stat = UcanInitCanEx2( _handle, _chanNumber, &params );
  if( stat != USBCAN_SUCCESSFUL )
    {
      _opStatus = stat;
      return false;
    }
  _isOpen = true;
  return true;
}

/* ------------------------------------------------------------------------ */

bool SystecInterface::close()
{
  bool result = true;
  this->awaitTransmission( 100 ); // In case of any remaining messages to send

  _opStatus = 0;
  _isOpen = false;

  UCANRET stat;
  stat = UcanDeinitCanEx( _handle, _chanNumber );
  if( stat != USBCAN_SUCCESSFUL )
    {
      _opStatus = stat;
      result = false;
    }

  // Flush hardware and software buffers
  this->flush();

  return result;
}

/* ------------------------------------------------------------------------ */

bool SystecInterface::send( CanMessage &can_msg )
{
  _opStatus = 0;

  tCanMsgStruct frame;
  frame.m_dwID = can_msg.cobId();
  frame.m_bFF  = USBCAN_MSG_FF_STD;
  if( can_msg.isRemoteFrame() ) frame.m_bFF |= USBCAN_MSG_FF_RTR;
  //if( can_msg.isErrFrame() ) ????
  frame.m_bDLC = can_msg.dlc();
  memcpy( frame.m_bData, can_msg.data(), can_msg.dlc() );

  UCANRET stat;
  stat = UcanWriteCanMsgEx( _handle, _chanNumber, &frame, 0 );
  if( stat != USBCAN_SUCCESSFUL )
    {
      if( stat == USBCAN_WARN_FW_TXOVERRUN )
	{
	  // Message was successfully sent, but there was an overrun
	  // in the transmit queue of the firmware
	  // (### Henk B: so what does that mean and what to do!?)
	  Sleep( 10 );
	}
      else
	{
	  _opStatus = stat;
	  return false;
	}
    }

  return true;
}

/* ------------------------------------------------------------------------ */

CanMessage* SystecInterface::receive( unsigned int timeout_ms )
{
  // Read the next available message;
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  if( !_canMsgs.empty() )
    {
      // Get the first message from the list
      pMsg = *(_canMsgs.begin());

      // Remove the message pointer from the list
      _canMsgs.pop_front();

      // And return it
      return pMsg;
    }

  if( timeout_ms != 0 )
    {
      int cntr = timeout_ms/10;
      if( cntr == 0 ) cntr = 1;
      while( cntr > 0 )
	{
	  // Should wait, and then read again...
	  Sleep( 10 );
	  this->bufferReceivedMsgs();
	  if( !_canMsgs.empty() )
	    {
	      // Get the first message from the list
	      pMsg = *(_canMsgs.begin());
	      // Remove the message pointer from the list
	      _canMsgs.pop_front();
	      return pMsg;
	    }
	  --cntr;
	}
      // After time-out still no message?
      // Indicate it by using a SYSTEC defined error value
      _opStatus = USBCAN_ERR_TIMEOUT;
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

CanMessage* SystecInterface::receiveSpecific( int          cob_id,
					      unsigned int timeout_ms )
{
  // Read the next available message having COB-ID 'cob_id';
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  // Is this message perhaps already available in the currently held list ?
  pMsg = this->getMsg( cob_id );

  // Message found ?
  if( pMsg ) return pMsg;

  if( timeout_ms != 0 )
    {
      int cntr = (timeout_ms+9)/10; // Minimum counter is 1
      while( cntr > 0 )
	{
	  // Should wait, and then read again...
	  Sleep( 10 );
	  this->bufferReceivedMsgs();
	  pMsg = this->getMsg( cob_id );
	  // Message found ?
	  if( pMsg ) return pMsg;
	  --cntr;
	}
      // After time-out still no message?
      // Indicate it by using a SYSTEC defined error value
      _opStatus = USBCAN_ERR_TIMEOUT;
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

void SystecInterface::bufferReceivedMsgs()
{
  // Get all available received CAN messages from the interface
  // and store them in the list
  UCANRET       stat;
  tCanMsgStruct frame;
  CanMessage    *pMsg = 0;
  BYTE          chan = (BYTE) (_chanNumber & 0xFF);
  bool          rtr;
 
  _opStatus = 0;

  // Check for and read any CAN messages received
  stat = UcanReadCanMsgEx( _handle, &chan, &frame, 0 );
  while( USBCAN_CHECK_VALID_RXCANMSG(stat) )
    {
      // (### No indication of an Error Frame available ?)
      rtr = false;
      if( frame.m_bFF & USBCAN_MSG_FF_RTR ) rtr = true;
      pMsg = new CanMessage( frame.m_dwID, rtr,
			     frame.m_bDLC, frame.m_bData,
			     // Timestamp should be 32-bits according to
			     // Usbcan32.h, but it seems to be 24-bits plus
			     // some unknown byte value...
			     //frame.m_dwTime );
			     (frame.m_dwTime & 0x00FFFFFF) );

      // Append (a pointer to) the message to the list
      _canMsgs.push_back( pMsg );

      stat = UcanReadCanMsgEx( _handle, &chan, &frame, 0 );
    }

  // Report problems (but 'no data' is of course not a problem..)
  if( stat != USBCAN_WARN_NODATA ) _opStatus = stat;
}

/* ------------------------------------------------------------------------ */

bool SystecInterface::awaitTransmission( unsigned int timeout_ms )
{
  _opStatus = 0;

  // Wait for all messages in the queue to be transmitted on the bus
  UCANRET stat;
  unsigned long n_msgs;
  stat = UcanGetMsgPending( _handle, _chanNumber,
			    USBCAN_PENDING_FLAG_TX_DLL |
			    USBCAN_PENDING_FLAG_TX_FW, &n_msgs );
  if( stat != USBCAN_SUCCESSFUL )
    {
      _opStatus = stat;
      return false;
    }
  else
    {
      int cntr = timeout_ms/10;
      if( cntr == 0 ) cntr = 1;
      while( n_msgs > 0 && cntr > 0 )
	{
	  // Should wait, and then check again...
	  Sleep( 10 );
	  stat = UcanGetMsgPending( _handle, _chanNumber,
				    USBCAN_PENDING_FLAG_TX_DLL |
				    USBCAN_PENDING_FLAG_TX_FW, &n_msgs );
	  if( stat != USBCAN_SUCCESSFUL )
	    {
	      _opStatus = stat;
	      return false;
	    }
	  --cntr;
	}
      if( n_msgs > 0 )
	{
	  // After time-out still messages in the queue:
	  // indicate it by using a SYSTEC defined error value
	  _opStatus = USBCAN_ERR_TIMEOUT;
	  return false;
	}
    }
  return true;
}

/* ------------------------------------------------------------------------ */

void SystecInterface::flush()
{
  UCANRET stat;

  // Flush transmit and then receive buffers
  _opStatus = 0;
  stat = UcanResetCanEx( _handle, (BYTE) _chanNumber,
			 USBCAN_RESET_ONLY_TX_BUFF );
  if( stat != USBCAN_SUCCESSFUL )
    _opStatus = stat;
  stat = UcanResetCanEx( _handle, (BYTE) _chanNumber,
			 USBCAN_RESET_ONLY_RX_BUFF );
  if( stat != USBCAN_SUCCESSFUL )
    _opStatus = stat;

  // Clear the received messages list:
  // delete the allocated CAN messages, then clear the list
  list<CanMessage *>::iterator it;
  for( it=_canMsgs.begin(); it!=_canMsgs.end(); ++it )
    delete (*it);
  _canMsgs.clear();
}

/* ------------------------------------------------------------------------ */

unsigned long SystecInterface::state()
{
  UCANRET       stat;
  tStatusStruct status_struct;
  unsigned long state = 0;

  _opStatus = 0;
  stat = UcanGetStatusEx( _handle, (BYTE) _chanNumber, &status_struct );
  if( stat != USBCAN_SUCCESSFUL )
    _opStatus = stat;
  else
    //state = status_struct.m_wUsbStatus;
    state = status_struct.m_wCanStatus;
  
  return state;
}

/* ------------------------------------------------------------------------ */

string SystecInterface::stateString( unsigned long state )
{
  // Return a string describing the state
  string str;
  unsigned long known_mask = (USBCAN_CANERR_XMTFULL |
			      USBCAN_CANERR_OVERRUN |
			      USBCAN_CANERR_BUSLIGHT |
			      USBCAN_CANERR_BUSHEAVY |
			      USBCAN_CANERR_BUSOFF |
			      USBCAN_CANERR_QRCVEMPTY |
			      USBCAN_CANERR_QOVERRUN |
			      USBCAN_CANERR_QXMTFULL |
			      USBCAN_CANERR_REGTEST |
			      USBCAN_CANERR_MEMTEST |
			      USBCAN_CANERR_TXMSGLOST);

  // Check for known state bits
  if( state & USBCAN_CANERR_XMTFULL )   str += "ERR_XMTFULL ";
  if( state & USBCAN_CANERR_OVERRUN )   str += "ERR_OVERRUN ";
  if( state & USBCAN_CANERR_BUSLIGHT )  str += "ERR_BUSLIGHT ";
  if( state & USBCAN_CANERR_BUSHEAVY )  str += "ERR_BUSHEAVY ";
  if( state & USBCAN_CANERR_BUSOFF )    str += "ERR_BUSOFF ";
  if( state & USBCAN_CANERR_QRCVEMPTY ) str += "ERR_QRCVEMPTY ";
  if( state & USBCAN_CANERR_QOVERRUN )  str += "ERR_QOVERRUN ";
  if( state & USBCAN_CANERR_QXMTFULL )  str += "ERR_QXMTFULL ";
  if( state & USBCAN_CANERR_REGTEST )   str += "ERR_REGTEST ";
  if( state & USBCAN_CANERR_MEMTEST )   str += "ERR_MEMTEST ";
  if( state & USBCAN_CANERR_TXMSGLOST ) str += "ERR_TXMSGLOST ";

  // Any of the unknown bits set ?
  if( state & (~known_mask) )
    { 
      str += "???? ";
      ostringstream oss;
      oss << " (" << hex << setw(8) << setfill('0')
	  << (state & (~known_mask)) << ")";
      str += oss.str();
    }

  return str;
}

/* ------------------------------------------------------------------------ */

string SystecInterface::errString()
{
  // Returns a string describing the result of the last CAN interface operation
  string str;

  // Error messages, that can occur in the library
  // USBCAN_SUCCESSFUL: no error
  if     ( _opStatus == USBCAN_SUCCESSFUL )       str += "No error ";
  else if( _opStatus == USBCAN_ERR_RESOURCE )     str += "ERR_RESOURCE ";
  else if( _opStatus == USBCAN_ERR_MAXMODULES )   str += "ERR_MAXMODULES ";
  else if( _opStatus == USBCAN_ERR_HWINUSE )      str += "ERR_HWINUSE ";
  else if( _opStatus == USBCAN_ERR_ILLVERSION )   str += "ERR_ILLVERSION ";
  else if( _opStatus == USBCAN_ERR_ILLHW )        str += "ERR_ILLHW ";
  else if( _opStatus == USBCAN_ERR_ILLHANDLE )    str += "ERR_ILLHANDLE ";
  else if( _opStatus == USBCAN_ERR_ILLPARAM )     str += "ERR_ILLPARAM ";
  else if( _opStatus == USBCAN_ERR_BUSY )         str += "ERR_BUSY ";
  else if( _opStatus == USBCAN_ERR_TIMEOUT )      str += "ERR_TIMEOUT ";
  else if( _opStatus == USBCAN_ERR_IOFAILED )     str += "ERR_IOFAILED ";
  else if( _opStatus == USBCAN_ERR_DLL_TXFULL )   str += "ERR_DLL_TXFULL ";
  else if( _opStatus == USBCAN_ERR_MAXINSTANCES ) str += "ERR_MAXINSTANCES ";
  else if( _opStatus == USBCAN_ERR_CANNOTINIT )   str += "ERR_CANNOTINIT ";
  else if( _opStatus == USBCAN_ERR_DISCONNECT )   str += "ERR_DISCONNECT ";
  else if( _opStatus == USBCAN_ERR_DISCONECT )    str += "ERR_DISCONECT ";
  else if( _opStatus == USBCAN_ERR_NOHWCLASS )    str += "ERR_NOHWCLASS ";
  else if( _opStatus == USBCAN_ERR_ILLCHANNEL )   str += "ERR_ILLCHANNEL ";
  else if( _opStatus == USBCAN_ERR_RESERVED1 )    str += "ERR_RESERVED1 ";
  else if( _opStatus == USBCAN_ERR_ILLHWTYPE )    str += "ERR_ILLHWTYPE ";

  // Error messages, that the module returns during the command sequence
  else if( _opStatus == USBCAN_ERRCMD_NOTEQU )      str += "ERRCMD_NOTEQU ";
  else if( _opStatus == USBCAN_ERRCMD_REGTST )      str += "ERRCMD_REGTST ";
  else if( _opStatus == USBCAN_ERRCMD_ILLCMD )      str += "ERRCMD_ILLCMD ";
  else if( _opStatus == USBCAN_ERRCMD_EEPROM )      str += "ERRCMD_EEPROM ";
  else if( _opStatus == USBCAN_ERRCMD_RESERVED1 )   str += "ERRCMD_RESERVED1 ";
  else if( _opStatus == USBCAN_ERRCMD_RESERVED2 )   str += "ERRCMD_RESERVED2 ";
  else if( _opStatus == USBCAN_ERRCMD_RESERVED3 )   str += "ERRCMD_RESERVED3 ";
  else if( _opStatus == USBCAN_ERRCMD_ILLBDR )      str += "ERRCMD_ILLBDR ";
  else if( _opStatus == USBCAN_ERRCMD_NOTINIT )     str += "ERRCMD_NOTINIT ";
  else if( _opStatus == USBCAN_ERRCMD_ALREADYINIT ) str += "ERRCMD_ALREADYINIT ";
  else if( _opStatus == USBCAN_ERRCMD_ILLSUBCMD )   str += "ERRCMD_ILLSUBCMD ";
  else if( _opStatus == USBCAN_ERRCMD_ILLIDX )      str += "ERRCMD_ILLIDX ";
  else if( _opStatus == USBCAN_ERRCMD_RUNNING )     str += "ERRCMD_RUNNING ";

  // Warning messages that can occur in library
  // NOTE: These messages are only warnings.
  // The function has been executed anyway.
  else if( _opStatus == USBCAN_WARN_NODATA )        str += "WARN_NODATA ";
  else if( _opStatus == USBCAN_WARN_SYS_RXOVERRUN ) str += "WARN_SYS_RXOVERRUN ";
  else if( _opStatus == USBCAN_WARN_DLL_RXOVERRUN ) str += "WARN_DLL_RXOVERRUN ";
  else if( _opStatus == USBCAN_WARN_RESERVED1 )     str += "WARN_RESERVED1 ";
  else if( _opStatus == USBCAN_WARN_RESERVED2 )     str += "WARN_RESERVED2 ";
  else if( _opStatus == USBCAN_WARN_FW_TXOVERRUN )  str += "WARN_FW_TXOVERRUN ";
  else if( _opStatus == USBCAN_WARN_FW_RXOVERRUN )  str += "WARN_FW_RXOVERRUN ";
  else if( _opStatus == USBCAN_WARN_FW_TXMSGLOST )  str += "WARN_FW_TXMSGLOST ";
  else if( _opStatus == USBCAN_WARN_NULL_PTR )      str += "WARN_NULL_PTR ";
  else if( _opStatus == USBCAN_WARN_TXLIMIT )       str += "WARN_TXLIMIT ";
  else if( _opStatus == USBCAN_WARN_BUSY )          str += "WARN_BUSY";

  return str;
}

/* ------------------------------------------------------------------------ */

string SystecInterface::errString( const char *context )
{
  // Returns a string describing the result of the last CAN interface operation
  // preceeded by a 'context' string
  return( string( context ) + ": " + this->errString() );
}

/* ------------------------------------------------------------------------ */

#ifdef WIN32
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

extern "C" MY_EXPORT CanInterface *newSystecInterface( int  port_nr,
						       int  kbitrate,
						       bool open_now )
{
  return new SystecInterface( port_nr, kbitrate, open_now );
}

/* ------------------------------------------------------------------------ */

extern "C" MY_EXPORT int systecInterfacePorts( int *port_numbers )
{
  vector<int> ports = SystecInterface::ports();
  sort( ports.begin(), ports.end() );
  for( unsigned int i=0; i<ports.size(); ++i ) port_numbers[i] = ports[i];
  return ports.size();
}

/* ------------------------------------------------------------------------ */
