#
# Project file for the SystecInterface library
#
# To generate a Visual Studio project:
#   qmake -t vclib SystecInterface.pro
# To generate a Makefile:
#   qmake SystecInterface.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = SystecInterface

# Create a shared library
CONFIG += shared qt thread warn_on exceptions debug_and_release

QT -= gui
QT += core

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

win32 {
  #INCLUDEPATH += "C:/Program Files/SYSTEC-electronic/USB-CANmodul Utility Disk/Include"
  INCLUDEPATH += "C:/Program Files (x86)\SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Include"
  # Need to add the following lib when creating the shared lib
  #LIBS += "-LC:/Program Files/SYSTEC-electronic/USB-CANmodul Utility Disk/Lib"
  LIBS += "-LC:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib"
  LIBS += -lUSBCAN64
}
LIBS += -lCANopen

INCLUDEPATH += ../CANopen

SOURCES += SystecInterface.cpp
HEADERS += SystecInterface.h
HEADERS += ../CANopen/CanInterface.h
HEADERS += ../CANopen/CanMessage.h
HEADERS += ../CANopen/CANopen.h

