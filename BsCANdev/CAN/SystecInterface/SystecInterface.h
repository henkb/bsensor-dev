/* ------------------------------------------------------------------------
File   : SystecInterface.h

Descr  : SYSTEC CAN-port/interface class.

History: 06NOV.11; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef SYSTECINTERFACE_H
#define SYSTECINTERFACE_H

// From WinDef.h
typedef unsigned long   DWORD;
typedef int             BOOL;
typedef unsigned char   BYTE;
typedef unsigned short  WORD;
typedef char            _TCHAR;
#include "usbcan32.h"

#include "CanInterface.h"

/* ------------------------------------------------------------------------ */

class SystecInterface: public CanInterface
{
 public:
  SystecInterface( int port_no   = CAN_INTF_DFLT,
		   int bitrate   = CAN_BITRATE_DFLT,
		   bool open_now = true );

  ~SystecInterface();

  static std::vector<int> ports();

  bool          setBusSpeed( int kbitrate );

  bool          open();

  bool          close();

  bool          send( CanMessage &can_msg );

  CanMessage*   receive( unsigned int timeout_ms = 0 );

  CanMessage*   receiveSpecific( int cob_id, unsigned int timeout_ms = 0 );

  bool          awaitTransmission( unsigned int timeout_ms );

  void          flush();

  unsigned long state();
  std::string   stateString( unsigned long state );

  std::string   errString();
  std::string   errString( const char *context );

 private:
  void          bufferReceivedMsgs();

  // Handle to the actual device
  tUcanHandle _handle;

  // Need to keep track of a module number and a channel number
  int         _modNumber, _chanNumber;

  // Index for selected bitrate
  int         _kbIndex;

  // Have to keep track of ports in use, so as not to 'deinit'
  // modules which still have an open channel, as well as their handles
  static bool _portInUse[USBCAN_MAX_MODULES*2];
  static tUcanHandle _handleInUse[USBCAN_MAX_MODULES];

  // Some convenient defaults
  static const int         CAN_INTF_DFLT;
  static const int         CAN_BITRATE_DFLT;
  static const std::string MANUFACTURER_STR;
};

/* ------------------------------------------------------------------------ */
#endif // SYSTECINTERFACE_H
