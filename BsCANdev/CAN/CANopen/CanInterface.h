/* ------------------------------------------------------------------------
File   : CanInterface.h

Descr  : Definition of a virtual base class for CANopen CAN-interface classes.

History: --JUL.01; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef CANINTERFACE_H
#define CANINTERFACE_H

#include <string>
#include <vector>
#include <list>

class CanMessage;

// Supported interface types
enum {
  KVASER_INTF_TYPE = 0,
  NICAN_INTF_TYPE  = 1,
  SYSTEC_INTF_TYPE = 2,
  SOCKET_INTF_TYPE = 3,
  PEAK_INTF_TYPE   = 4
};

#ifdef WIN32
 // Differentiate between building the DLL or using it
 #ifdef MY_LIB_EXPORT
 #define MY_LIB __declspec(dllexport)
 #else
 #define MY_LIB __declspec(dllimport)
 #endif
#else
 #define MY_LIB
#endif // WIN32

/* ------------------------------------------------------------------------ */

class MY_LIB CanInterface
{
 public:
  CanInterface( std::string man_name,
                int         intf_type,
                int         port_nr,
                int         kbitrate );

  virtual ~CanInterface() {};

  virtual bool          open() = 0;

  virtual bool          close() = 0;

  virtual bool          send( CanMessage &can_msg ) = 0;

  virtual CanMessage*   receive( unsigned int timeout_ms = 0 ) = 0;

  virtual CanMessage*   receiveSpecific( int cob_id,
                                         unsigned int timeout_ms = 0 ) = 0;

  void                  msgHandled( CanMessage *msg );

  virtual bool          awaitTransmission( unsigned int timeout_ms ) = 0;

  bool                  nmt( int command );

  bool                  sendSync();

  int                   scanBusSynchronized( bool get_serialnr = true );
  int                   scanBus( unsigned int timeout_ms = 500,
                                 bool get_serialnr = true );
  int                   scannedNodeId( unsigned int index );
  std::string           scannedSwVersion( unsigned int index );
  int                   scannedSerialNr( unsigned int index );

  virtual void          flush() = 0;

  virtual unsigned long state() = 0;
  virtual std::string   stateString( unsigned long state ) = 0;

  // Return the result from the last CAN port operation
  int                   opStatus() { return _opStatus; }

  std::string           initResultString() { return _initResultString; }

  void                  setSkipMessages( bool skip ) { _skipMessages = skip; }

  virtual std::string   errString() = 0;
  virtual std::string   errString( const char *context ) = 0;

  bool                  isOpen()           { return _isOpen; }
  std::string           manufacturerName() { return _manufacturerName; }
  int                   interfaceType()    { return _interfaceType; }
  int                   portNumber()       { return _portNumber; }
  int                   kbitRate()         { return _kbitRate; }
  int                   busSpeed()         { return _kbitRate; }
  virtual bool          setBusSpeed( int kbitrate ) = 0;

  // Static methods using dynamic library loading:
  // - create a new interface of the requested type
  static CanInterface  *newCanInterface( int  intf_type,
                                         int  port_nr,
                                         int  kbitrate,
                                         bool open_now = true );
  // - return the available port numbers of the requested interface type
  static int canInterfacePorts( int intf_type, int **port_numbers );

 protected:
  std::string _manufacturerName;
  std::string _initResultString;
  int         _interfaceType;
  int         _portNumber;
  int         _kbitRate;
  bool        _isOpen;
  int         _opStatus;     // Return status of last operation
  int         _msgErrFlags;  // Error flags during last receive op
  bool        _skipMessages; // Whether to trash messages while
                             // looking for a specific one

  virtual void  bufferReceivedMsgs() = 0;
  CanMessage*   getMsg( int cob_id );

  // Buffer for CAN messages still to handle
  std::list<CanMessage *> _canMsgs;

  // Storage for CAN bus scan results
  typedef struct node_info {
    int         id;
    std::string sw_version;
    int         serial_nr;
  } NodeInfo;
  std::vector<NodeInfo> _scannedNodeInfo;
};

/* ------------------------------------------------------------------------ */
#endif // CANINTERFACE_H
