/* ------------------------------------------------------------------------
File   : CanMessage.cpp

Descr  : Implementation of a CAN message class with CANopen features.

History: --JUL.01; Henk B&B; First definition.
--------------------------------------------------------------------------- */
#include <sstream>
#include <iomanip>
using namespace std;

#include "CanMessage.h"
#include "CANopen.h"

/* ------------------------------------------------------------------------ */

CanMessage::CanMessage()
  :  _isErrFrame(true),
     _isRtrFrame(false),
     _cobId(0x77F),
     _dlc(0),
     _timeStamp(0)
{
}

/* ------------------------------------------------------------------------ */

CanMessage::CanMessage( CanMessage &msg )
{
  _isErrFrame = msg._isErrFrame;
  _isRtrFrame = msg._isRtrFrame;
  _cobId      = msg._cobId;
  _dlc        = msg._dlc;
  _timeStamp  = msg._timeStamp;
  for( int i=0; i<8; ++i )
    _data[i] = msg._data[i];
}

/* ------------------------------------------------------------------------ */

CanMessage::CanMessage( int            cobid,
                        bool           remote,
                        int            dlc,
                        unsigned char *data,
                        unsigned long  time )
  :  _isErrFrame(false),
     _isRtrFrame(remote),
     _cobId(cobid & COBID_MASK),
     _timeStamp(time)
{
  if( dlc > 8 ) dlc = 8;
  if( dlc < 0 ) dlc = 0;
  _dlc = dlc;
  if( !remote && data )
    for( int i=0; i<dlc; ++i )
      _data[i] = data[i];
  //memcpy( (void *) _data, (const void *) data, dlc );
}

/* ------------------------------------------------------------------------ */

CanMessage::~CanMessage()
{
}

/* ------------------------------------------------------------------------ */

int CanMessage::cobId()
{
  return _cobId;
}

/* ------------------------------------------------------------------------ */

void CanMessage::setCobId( int cobid )
{
  _cobId = cobid & COBID_MASK;
}

/* ------------------------------------------------------------------------ */

int CanMessage::nodeId()
{
  // The CANopen node identifier/address according to
  // the CANopen Predefined Connection Set
  return( _cobId & NODEID_MASK );
}

/* ------------------------------------------------------------------------ */

void CanMessage::setNodeId( int nodeid )
{
  // Set the node identifier/address according to
  // the CANopen Predefined Connection Set
  _cobId = (_cobId & ~NODEID_MASK) | (nodeid & NODEID_MASK);
}

/* ------------------------------------------------------------------------ */

int CanMessage::object()
{
  // The CANopen object (without node identifier) according to
  // the CANopen Predefined Connection Set
  return( _cobId & OBJECT_MASK );
}

/* ------------------------------------------------------------------------ */

void CanMessage::setObject( int object )
{
  // Set the CANopen object according to
  // the CANopen Predefined Connection Set
  _cobId = (_cobId & ~OBJECT_MASK) | (object & OBJECT_MASK);
}

/* ------------------------------------------------------------------------ */

int CanMessage::dlc()
{
  return _dlc;
}

/* ------------------------------------------------------------------------ */

void CanMessage::setDlc( int dlc )
{
  if( dlc > 8 ) dlc = 8;
  if( dlc < 0 ) dlc = 0;
  _dlc = dlc;
}

/* ------------------------------------------------------------------------ */

unsigned char *CanMessage::data()
{
  return _data;
}

/* ------------------------------------------------------------------------ */

void CanMessage::setData( int dlc, unsigned char *data )
{
  if( dlc > 8 ) dlc = 8;
  _dlc = dlc;
  for( int i=0; i<dlc; ++i ) _data[i] = data[i];
}

/* ------------------------------------------------------------------------ */

unsigned long CanMessage::timeStamp()
{
  return _timeStamp;
}

/* ------------------------------------------------------------------------ */

void CanMessage::setTimeStamp( unsigned long timestamp )
{
  _timeStamp = timestamp;
}

/* ------------------------------------------------------------------------ */

bool CanMessage::isErrFrame()
{
  return _isErrFrame;
}

/* ------------------------------------------------------------------------ */

bool CanMessage::isRemoteFrame()
{
  return _isRtrFrame;
}

/* ------------------------------------------------------------------------ */

int CanMessage::sdoCmdSpecifier()
{
  return( _data[0] & SDO_COMMAND_SPECIFIER_MASK );
}

/* ------------------------------------------------------------------------ */

const char *SDO_ERR_CLASS[] = { "???", "???", "???", "???", "???",
                                "service", "access", "???", "other" };
const char *SDO_ERR_CODE[]  = { "???",          "access",
                                "non-existent", "par inconsistent",
                                "par illegal",  "???",
                                "hardware",     "type conflict",
                                "???",          "attribute" };

const char *OBJCOLOR  = "<b><font color=\"blue\">";
const char *EMGCOLOR  = "<b><font color=\"red\">";
const char *BOOTCOLOR = "<b><font color=\"orange\">";
const char *NMTCOLOR  = "<b><font color=\"magenta\">";
const char *NGCOLOR   = "<b><font color=\"lime\">";
const char *SYNCCOLOR = "<b><font color=\"maroon\">";
const char *SRVCOLOR  = "<b><font color=\"cyan\">";
const char *ENDCOLOR  = "</font></b>";

// '99' means: channel not related to a mezzanine:
const int   CSM_ADC_MEZZ[] = {
  16, 16, 16, 99, 15, 15, 15, 99,
  17, 17, 17, 99, 99, 99, 99, 99,
  6,  6,  6,  99, 5,  5,  5,  7,
  8,  8,  8,  7,  9,  9,  9,  7,
  10, 10, 10, 99, 12, 12, 12, 11,
  14, 14, 14, 11, 13, 13, 13, 11,
  0,  0,  0,  99, 2,  2,  2,  1,
  4,  4,  4,  1,  3,  3,  3,  1
};

std::string CanMessage::description( int  mode,
                                     bool show_time,
                                     bool show_time_raw )
{
  // Return a string describing the CANopen message, based on the
  // CANopen Predefined Connection Set;
  // the 'mode' parameter is used to create more or less verbose descriptions:
  // mode 0 and 1 used by the CANhost tool (historically),
  // mode > 1 used by the WINhost tool:
  // 2: plain CAN bytes display,
  // 3: messages interpreted according to the CANopen Predefined Connection Set,
  // 4: some messages interpreted as ELMBio firmware specific
  // 5: some messages interpreted as MDT-DCS Module firmware specific
  ostringstream oss;
  oss << uppercase; // Any hex output in uppercase

  if( show_time )
    {
      // Assume the timestamp is in milliseconds
      if( mode <= 1 )
        {
          unsigned long ts_s = _timeStamp/1000;
          unsigned long hrs  = ((ts_s/60) / 60) % 100; // Up to 99 hrs
          unsigned long mins = (ts_s/60) % 60;
          unsigned long secs = ts_s % 60;
          unsigned long ms   = ((_timeStamp)) % 1000;
          oss << setfill('0');
          oss << "T=";
          oss << setw(2) << hrs << ":"
              << setw(2) << mins << ":"
              << setw(2) << secs << ":"
              << setw(3) << ms << "  ";
          if( show_time_raw ) oss << "(" << _timeStamp << ") ";
        }
      else
        {
          oss << "<font color=\"blue\">";
          unsigned long ts_s = _timeStamp/1000;
          unsigned long ms   = _timeStamp - ts_s*1000;
          oss << "T=" << setw(6) << ts_s
              << "." << setw(3) << setfill('0') << ms << "  ";
          oss << "</font>";
        }
    }

  if( _isErrFrame )
    {
      oss << "Error Frame";
      if( mode <= 1 ) oss << endl;
      return oss.str();
    }

  int obj = this->object();
  if( mode <= 1 )
    {
      // For 'CANhost'
      oss << "N=" << setw(3) << setfill(' ') << this->nodeId()
          << "  COB=" << hex << setw(3) << obj << "h" << dec
          << "  DLC=" << _dlc << setfill('0');
    }
  else
    {
      // For 'WINhost'
      if( mode == 2 )
        {
          oss << "COBID = " << hex << setfill('0') << setw(3)
              << this->cobId() << dec
              << "  DLC = " << _dlc;
        }
      else
        {
          oss << "Node=" << setw(3) << setfill(' ') << this->nodeId()
            //<< "  COB=" << hex << setw(3) << obj << "h" << dec
              << "  DLC=" << _dlc;
        }
    }

  if( _isRtrFrame )
    {
      oss << "  RTR";
    }
  else
    {
      if( _dlc > 0 && mode <= 2 )
        {
          oss << "  Data:" << hex << setfill('0');
          for( int i=0; i<_dlc; ++i )
            oss << " " << setw(2) << (int) _data[i];
          oss << dec;
        }
    }
  if( mode <= 1 ) oss << endl;

  // If the short version is required return from here...
  if( mode == 0 || mode == 2 )
    return oss.str();

  // If mode==1 or mode==3 describe the message in CANopen terms,
  // if mode==4 describe the message also in 'ELMB' terms (where appropriate)
  if( mode <= 1 )
    oss << "  COB=";
  else
    oss << "  ";
  switch( obj )
    {
    case NMT_OBJ:
      if( mode > 2 ) oss << NMTCOLOR << "NMT " << ENDCOLOR;
      else oss << "NMT ";
      if( _dlc < 2 )
        {
          oss << "### Error: DLC<2";
        }
      else
        {
          switch( _data[0] )
            {
            case NMT_START_REMOTE_NODE:
              oss << "Start-Remote-Node";
              break;
            case NMT_STOP_REMOTE_NODE:
              oss << "Stop-Remote-Node";
              break;
            case NMT_ENTER_PREOPERATIONAL_STATE:
              oss << "Enter-Preoperational-State";
              break;
            case NMT_RESET_NODE:
              oss << "Reset-Node";
              break;
            case NMT_RESET_COMMUNICATION:
              oss << "Reset-Communication";
              break;
            default:
              oss << "???";
              break;
            }
          if( _data[1] == 0 )
            oss << " NodeId=ALL";
          else
            oss << " NodeId=" << (int) _data[1];
        }
      break;

    case SYNC_OBJ: // also equal to EMERGENCY_OBJ
      if( _dlc == 0 )
        {
          if( mode > 2 ) oss << SYNCCOLOR << "SYNC" << ENDCOLOR;
          else oss << "SYNC";
        }
      else
        {
          if( mode > 2 ) oss << EMGCOLOR << "EMERGENCY" << ENDCOLOR;
          else oss << "EMERGENCY";
          oss << " ErrCode = " << hex << setfill('0') << setw(4)
              << (int)_data[0] + ((int)_data[1]<<8)
              << "h, ErrReg = " << setw(2) << (int)_data[2] << "h";
          if( mode != 4 )
            {
              oss << " Manufctr-spec:";
              for( int i=3; i<_dlc; ++i )
                oss << " " << setw(2) << (int) _data[i];
              oss << dec;
            }
          else
            {
              // Display Emergency as ELMBio firmware specific
              // ## TO BE DONE...
              oss << " Manufctr-spec:";
              for( int i=3; i<_dlc; ++i )
                oss << " " << setw(2) << (int) _data[i];
              oss << dec;
            }
        }
      break;

    case TIMESTAMP_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "TIMESTAMP" << ENDCOLOR;
      else oss << "TIMESTAMP";
      break;

    case TPDO1_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "TPDO1" << ENDCOLOR;
      else oss << "TPDO1";
      break;

    case RPDO1_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "RPDO1" << ENDCOLOR;
      else oss << "RPDO1";
      break;

    case TPDO2_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "TPDO2" << ENDCOLOR;
      else oss << "TPDO2";
      break;

    case RPDO2_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "RPDO2" << ENDCOLOR;
      else oss << "RPDO2";
      break;

    case TPDO3_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "TPDO3" << ENDCOLOR;
      else oss << "TPDO3";
      break;

    case RPDO3_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "RPDO3" << ENDCOLOR;
      else oss << "RPDO3";
      break;

    case TPDO4_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "TPDO4" << ENDCOLOR;
      else oss << "TPDO4";
      break;

    case RPDO4_OBJ:
      if( mode > 2 ) oss << OBJCOLOR << "RPDO4" << ENDCOLOR;
      else oss << "RPDO4";
      break;

    case NODEGUARD_OBJ: // also equal to BOOTUP_OBJ
      if( !_isRtrFrame && _data[0] == 0 )
        {
          if( mode > 2 ) oss << BOOTCOLOR << "BOOTUP" << ENDCOLOR;
          else oss << "BOOTUP";
        }
      else
        {
          if( mode > 2 ) oss << NGCOLOR << "NODEGUARD " << ENDCOLOR;
          else oss << "NODEGUARD ";
          if( _isRtrFrame )
            {
              oss << "Request";
            }
          else
            {
              oss << "Reply, State=";
              switch( _data[0] & NMT_STATE_MASK )
                {
                case NMT_INITIALISING:
                  oss << "Initialising";
                  break;
                case NMT_DISCONNECTED:
                  oss << "Disconnected";
                  break;
                case NMT_CONNECTING:
                  oss << "Connecting";
                  break;
                case NMT_PREPARING:
                  oss << "Preparing";
                  break;
                case NMT_STOPPED:
                  oss << "Stopped";
                  break;
                case NMT_OPERATIONAL:
                  oss << "Operational";
                  break;
                case NMT_PREOPERATIONAL:
                  oss << "Preoperational";
                  break;
                default:
                  oss << "???";
                  break;
                }
              oss << " Toggle=" << ((_data[0] & NMT_TOGGLE_MASK) ? 1 : 0);
            }
        }
      break;

    case SDOTX_OBJ:
      {
        if( mode > 2 ) oss << SRVCOLOR << "SDO-server" << ENDCOLOR;
        else oss << "SDO-server";

        // Display contents of SDO-server message more verbose
        int sdo_mode, cs, index, subindex, nbytes, i;
        unsigned long data;

        // Modifier bits in first message byte...
        sdo_mode = (int) _data[0];

        // Determine the command specifier (cs)
        cs = sdo_mode & SDO_COMMAND_SPECIFIER_MASK;

        if( mode == 1 )
          oss << "  cmd-specifier: ";
        else
          oss << ": ";
        switch( cs )
          {
          case SDO_INITIATE_DOWNLOAD_RESP:
            oss << "Init-Download";
            break;
          case SDO_DOWNLOAD_SEGMENT_RESP:
            oss << "Segment-Download";
            oss << "  Toggle: " << ((sdo_mode & SDO_TOGGLE_BIT) ? 1:0);
            break;
          case SDO_INITIATE_UPLOAD_RESP:
            oss << "Init-Upload";
            if( sdo_mode & SDO_EXPEDITED )
              {
                if( mode > 2 )
                  oss << "-Exp";
                else
                  oss << "-Expedited";
              }
            else
              {
                oss << "-Segmented";
              }
            break;
          case SDO_UPLOAD_SEGMENT_RESP:
            oss << "Segment-Upload";
            if( sdo_mode & SDO_LAST_SEGMENT ) oss << " (last segment)";
            oss << "  Toggle: " << ((sdo_mode & SDO_TOGGLE_BIT) ? 1:0);
            break;
          case SDO_ABORT_TRANSFER:
            if( mode > 2 ) oss << EMGCOLOR << "Abort-Transfer" << ENDCOLOR;
            else oss << "Abort-Transfer";
            break;
          default:
            oss << "????";
            break;
          }
        if( mode == 1 ) oss << endl;

        if( cs == SDO_INITIATE_DOWNLOAD_RESP ||
            cs == SDO_INITIATE_UPLOAD_RESP ||
            cs == SDO_ABORT_TRANSFER )
          {
            index    = (int) _data[1] + ((int) _data[2] << 8);
            subindex = (int) _data[3];

            if( mode == 1 )
              {
                oss << hex << "  OD-index=" << index << "h";
                oss << dec << ", OD-subindex=" << subindex;
              }
            else
              {
                oss << hex << " Index=" << index << "h";
                oss << dec << ",Sub=" << subindex;
              }
          }

        if( cs == SDO_INITIATE_UPLOAD_RESP )
          {
            if( sdo_mode & SDO_SEGMENT_SIZE_INDICATED )
              {
                if( sdo_mode & SDO_EXPEDITED )
                  {
                    // Expedited transfer
                    nbytes = 4 - ((sdo_mode & SDO_DATA_SIZE_MASK) >>
                                  SDO_DATA_SIZE_SHIFT);
                    if( mode == 1 )
                      oss << "  #databytes " << nbytes << ": ";
                    else
                      oss << "  Data: ";
                    data = 0;
                    oss << hex << setfill('0');
                    for( i=0; i<nbytes; ++i )
                      {
                        data |= (_data[4+i] << (8*i));
                        oss << setw(2) << (int) _data[4+i] << " ";
                      }
                    oss << dec;
                    if( index == 0x1008 ||
                        index == 0x1009 ||
                        index == 0x100A )
                      {
                        // Manufacturer device name,
                        // manufacturer hardware version or
                        // manufacturer software version:
                        // the data can be displayed as a string
                        oss << "(\"";
                        for( i=0; i<nbytes; ++i )
                          oss << (char) _data[4+i];
                        oss << "\")";
                      }
                    else
                      {
                        oss << hex << "(";
                        if( nbytes == 1 )      oss << setw(2) << data;
                        else if( nbytes == 2 ) oss << setw(4) << data;
                        else if( nbytes == 3 ) oss << setw(6) << data;
                        else if( nbytes == 4 ) oss << setw(8) << data;
                        oss << "h, " << dec << data << ")";
                      }
                  }
                else
                  {
                    // Segmented transfer: byte counter present
                    nbytes = 4;
                    if( mode == 1 )
                      oss << "  #databytes " << nbytes << ": ";
                    else
                      oss << "  Data: ";
                    oss << "bytecntr = ";
                    data = 0;
                    for( i=0; i<nbytes; ++i )
                      data |= (_data[4+i] << (8*i));
                    oss << dec << data << " (" << hex << setfill('0')
                        << setw(8) << data << "h)" << dec;
                  }
              }
          }

        if( cs == SDO_UPLOAD_SEGMENT_RESP )
          {
            nbytes = 7 - ((sdo_mode & SDO_SEGMENT_SIZE_MASK) >>
                          SDO_SEGMENT_SIZE_SHIFT);
            if( mode == 1 )
              oss << "  #databytes " << nbytes << ": ";
            else
              oss << "  Data: ";
            oss << hex << setfill('0');
            for( i=0; i<nbytes; ++i )
              oss << setw(2) << (int) _data[1+i] << " ";
            oss << dec;
          }
        if( mode == 1 ) oss << endl;

        if( cs == SDO_ABORT_TRANSFER )
          {
            int error_class, error_code, additional_code;

            error_class     = (int) _data[7];
            error_code      = (int) _data[6];
            additional_code = ((int) _data[5]<<8) | (int) _data[4];

            //oss << "   ###err: class=%X, code=%X, additional=%X\n",
            //    error_class, error_code, additional_code );

            oss << hex << "  ###err: class=" << error_class << " (";
            if( error_class <= SDO_ECLASS_OTHER )
              oss << SDO_ERR_CLASS[error_class];
            else
              oss << "???";

            oss << "), code=" << error_code << " (";
            if( error_code <= SDO_ECODE_ATTRIBUTE )
              oss << SDO_ERR_CODE[error_code];
            else
              oss << "???";

            oss << "), additional=" << additional_code;
            if( mode == 1 ) oss << endl;
          }

        break;
      }

    case SDORX_OBJ:
      {
        if( mode > 2 ) oss << OBJCOLOR << "SDO-client" << ENDCOLOR;
        else oss << "SDO-client";

        // Display contents of SDO-client message more verbose
        int sdo_mode, cs, index, subindex, nbytes, i;
        unsigned long data;

        // Modifier bits in first message byte...
        sdo_mode = (int) _data[0];

        // Determine the command specifier (cs)
        cs = sdo_mode & SDO_COMMAND_SPECIFIER_MASK;

        if( mode == 1 )
          oss << "  cmd-specifier: ";
        else
          oss << ": ";
        switch( cs )
          {
          case SDO_INITIATE_DOWNLOAD_REQ:
            oss << "Init-Download";
            break;
          case SDO_DOWNLOAD_SEGMENT_REQ:
            oss << "Segment-Download";
            if( sdo_mode & SDO_LAST_SEGMENT )
              oss << " (last segment)";
            oss << "  Toggle: " << ((sdo_mode & SDO_TOGGLE_BIT) ? 1:0);
            break;
          case SDO_INITIATE_UPLOAD_REQ:
            oss << "Init-Upload";
            break;
          case SDO_UPLOAD_SEGMENT_REQ:
            oss << "Segment-Upload";
            break;
          case SDO_ABORT_TRANSFER:
            if( mode > 2 ) oss << EMGCOLOR << "Abort-Transfer" << ENDCOLOR;
            else oss << "Abort-Transfer";
            break;
          default:
            oss << "????";
            break;
          }
        if( mode == 1 ) oss << endl;

        if( cs == SDO_INITIATE_DOWNLOAD_REQ ||
            cs == SDO_INITIATE_UPLOAD_REQ ||
            cs == SDO_ABORT_TRANSFER )
          {
            index    = (int) _data[1] + ((int) _data[2] << 8);
            subindex = (int) _data[3];

            if( mode == 1 )
              {
                oss << hex << "  OD-index=" << index << "h";
                oss << dec << ", OD-subindex=" << subindex;
              }
            else
              {
                oss << hex << " Index=" << index << "h";
                oss << dec << ",Sub=" << subindex;
              }
          }

        if( cs == SDO_INITIATE_DOWNLOAD_REQ )
          {
            if( sdo_mode & SDO_SEGMENT_SIZE_INDICATED )
              {
                if( sdo_mode & SDO_EXPEDITED )
                  {
                    /* Expedited transfer */
                    nbytes = 4 - ((sdo_mode & SDO_DATA_SIZE_MASK) >>
                                  SDO_DATA_SIZE_SHIFT);
                    if( mode == 1 )
                      oss << "  #databytes " << nbytes << ": ";
                    else
                      oss << "  Data: ";
                    data = 0;
                    oss << hex << setfill('0');
                    for( i=0; i<nbytes; ++i )
                      {
                        data |= (_data[4+i] << (8*i));
                        oss << setw(2) << (int) _data[4+i] << " ";
                      }
                    oss << "(";
                    if( nbytes == 1 )      oss << setw(2) << data;
                    else if( nbytes == 2 ) oss << setw(4) << data;
                    else if( nbytes == 3 ) oss << setw(6) << data;
                    else if( nbytes == 4 ) oss << setw(8) << data;
                    oss << "h, " << dec << data << ")";
                  }
                else
                  {
                    /* Segmented transfer: byte counter present */
                    nbytes = 4;
                    if( mode == 1 )
                      oss << "  #databytes " << nbytes << ": ";
                    else
                      oss << "  Data: ";
                    oss << "byte-counter = ";
                    data = 0;
                    oss << hex << setfill('0');
                    for( i=0; i<nbytes; ++i )
                      {
                        data |= (_data[4+i] << (8*i));
                        oss << setw(2) << (int) _data[4+i] << " ";
                      }
                    oss << "= " << dec << data << " ("
                        << hex << setfill('0') << setw(8) << data << "h)";
                  }
              }
          }

        if( cs == SDO_DOWNLOAD_SEGMENT_REQ )
          {
            nbytes = 7 - ((sdo_mode & SDO_SEGMENT_SIZE_MASK) >>
                          SDO_SEGMENT_SIZE_SHIFT);
            if( mode == 1 )
              oss << "  #databytes " << nbytes << ": ";
            else
              oss << "  Data: ";
            oss << hex << setfill('0');
            for( i=0; i<nbytes; ++i )
              oss << setw(2) << (int) _data[1+i] << " ";
          }
        oss << dec;

        if( cs == SDO_ABORT_TRANSFER )
          {
            int error_class, error_code, additional_code;
            error_class     = (int) _data[7];
            error_code      = (int) _data[6];
            additional_code = ((int) _data[5]<<8) | (int) _data[4];
            oss << hex << "  ###err: class=" << error_class
                << ", code=" << error_code
                << ", add=" << additional_code;
          }

        if( mode == 1 ) oss << endl;
        break;
      }
    default:
      break;
    }
  if( mode == 1 )
    {
      oss << endl;
      return oss.str();
    }

  // For mode > 1:
  if( _dlc > 0 &&
      obj != NMT_OBJ &&
      obj != SDORX_OBJ &&
      obj != SDOTX_OBJ &&
      obj != EMERGENCY_OBJ &&
      !_isRtrFrame )
    {
      if( mode != 4 && mode != 5 )
        {
          oss << "  Data:" << hex << setfill('0');
          for( int i=0; i<_dlc; ++i )
            oss << " " << setw(2) << (int) _data[i];
          oss << dec;
        }
      else if( mode == 4 )
        {
          // Display some data interpreted as ELMBio firmware specific,
          // in particular the PDOs
          if( obj == TPDO2_OBJ )
            {
              int adc_cnt = ((int) _data[2] | ((int) _data[3] << 8));
              // Take unipolar/bipolar setting into account
              if( (_data[1] & 1) == 0 )
                // Bipolar, so can be negative...
                if( adc_cnt & 0x8000 ) adc_cnt |= 0xFFFF0000;
              oss << "  ADC chan " << setw(2) << (int) _data[0]
                  << ":" << setw(6) << adc_cnt;
              // Conversion error ?
              if( _data[1] & 0x80 )
                oss << EMGCOLOR << " (Err)" << ENDCOLOR;
            }
          else if( obj == TPDO3_OBJ )
            {
              // ADC input reading in microvolts
              int uv = (int) _data[2] | ((int) _data[3] << 8) |
                ((int) _data[4] << 16) | ((int) _data[5] << 24);
              int v = uv/1000; // Positive or negative or zero
              bool negative = false;
              if( uv < 0 )
                {
                  negative = true;
                  // Positive only, to prevent '-' in front of mV below
                  uv = -uv;
                }
              oss << "  ADC chan " << setw(2) << (int) _data[0] << ": ";
              if( negative && v == 0 )
                oss << "   -0";
              else
                oss << setw(5) << v;
              oss << "." << setw(3) << setfill('0')
                  << (uv - (uv/1000)*1000) << " mV";
              // Conversion error ?
              if( _data[1] & 0x80 )
                oss << EMGCOLOR << " (Err)" << ENDCOLOR;
            }
          else if( obj == TPDO1_OBJ )
            {
              oss << hex << setfill('0') << "  DigIn:";
              int byte, val, bitmask;
              for( byte=0; byte<3; ++byte )
                if( _dlc > byte )
                  {
                    val = (int) _data[byte];
                    bitmask = 0x80;
                    if( byte == 0 )
                      oss << " PortF7-0:";
                    else if( byte == 1 )
                      oss << " PortA7-0:";
                    else
                      oss << " PortC7-0:";
                    for( int i=0; i<8; ++i, bitmask>>=1 )
                      {
                        if( val & bitmask )
                          oss << "1";
                        else
                          oss << "0";
                      }
                  }
            }
          else if( obj == RPDO1_OBJ )
            {
              oss << hex << setfill('0') << "  DigOut:";
              int byte, val, bitmask;
              for( byte=0; byte<3; ++byte )
                if( _dlc > byte )
                  {
                    val = (int) _data[byte];
                    bitmask = 0x80;
                    if( byte == 0 )
                      oss << " PortC7-0:";
                    else if( byte == 1 )
                      oss << " PortA7-0:";
                    else
                      oss << " PortF7-0:";
                    for( int i=0; i<8; ++i, bitmask>>=1 )
                      {
                        if( val & bitmask )
                          oss << "1";
                        else
                          oss << "0";
                      }
                  }
              //if( _dlc > 0 )
              //  oss << " PortC=0x" << setw(2) << (int) _data[0];
              //if( _dlc > 1 )
              //  oss << " PortA=0x" << setw(2) << (int) _data[1];
              //if( _dlc > 2 )
              //  oss << " PortF=0x" << setw(2) << (int) _data[2];
            }
          else
            {
              oss << "  Data:" << hex << setfill('0');
              for( int i=0; i<_dlc; ++i )
                oss << " " << setw(2) << (int) _data[i];
              oss << dec;
            }
        }
      else if( mode == 5 )
        {
          // Display some data interpreted as MDT-DCS firmware-specific,
          // in particular the PDOs
          if( obj == TPDO2_OBJ )
            {
              // T-sensors
              int mdegrees = ((int) _data[1] | ((int) _data[2] << 8) |
                              ((int) _data[3] << 16));
              oss << "  NTC chan " << setw(2) << (int) _data[0]
                  << ":" << setw(3) << mdegrees/1000 << "."
                  << setfill('0') << setw(3) << (mdegrees % 1000)
                  << " C";
            }
          else if( obj == TPDO3_OBJ )
            {
              // CSM-ADC monitoring: temperature, analog and
              // digital voltages, power and reference voltages
              int adc_cnt = ((int) _data[2] | ((int) _data[3] << 8));
              // Take unipolar/bipolar setting into account
              if( (_data[1] & 1) == 0 )
                // Bipolar, so can be negative...
                if( adc_cnt & 0x8000 ) adc_cnt |= 0xFFFF0000;

              int chan = (int) _data[0];
              oss << "  CSM chan " << setw(2) << chan;

              // Convert to physical value
              double f;
              if( chan == 0 || chan == 4 ||
                  chan == 8 || chan == 13 ||
                  chan == 16 || chan == 20 ||
                  chan == 23 || chan == 24 ||
                  chan == 28 || chan == 32 ||
                  chan == 36 || chan == 40 ||
                  chan == 44 || chan == 48 ||
                  chan == 52 || chan == 55 ||
                  chan == 56 || chan == 60 )
                {
                  // T-sensor
                  f = ((double) adc_cnt * 0.01526) - 50.0;
                  if( chan == 13 )
                    oss << ", CSM temp";
                  else
                    oss << ", Mezz temp " << setw(2) << CSM_ADC_MEZZ[chan];
                  oss << fixed << setprecision(1)
                      << ":" << setw(5) << f << " C";
                }
              else
                {
                  // CSM or mezzanines voltages
                  f = (double) adc_cnt * 5.0 / (float) 0x7FFF;
                  if( chan == 3 )
                    oss << " (2.5V)    ";
                  else if( chan == 7 )
                    oss << " (1.5V)    ";
                  else if( chan == 11 || chan == 15 )
                    oss << " (2.5V ref)";
                  else if( chan == 12 )
                    oss << " (+5Vcc/2) ";
                  else if( chan == 14 )
                    oss << " (-5Vee/2) ";
                  else if( chan == 19 )
                    oss << " (3.3V)    ";
                  else if( chan == 35 )
                    oss << " (1.8V)    ";
                  else if( chan == 51 )
                    oss << " (Vcc)     ";
                  else
                    oss << " (Mezz " << setw(2)
                        << CSM_ADC_MEZZ[chan] << ")";
                  oss << fixed << setprecision(3)
                      << ":" << setw(6) << f;
                }

              // Conversion error ?
              if( _data[1] & 0x80 )
                oss << EMGCOLOR << " (Err)" << ENDCOLOR;
            }
          else if( obj == TPDO4_OBJ )
            {
              // B-sensors
              int adc_cnt = ((int) _data[2] | ((int) _data[3] << 8) |
                             ((int) _data[4] << 16));
              // Take unipolar/bipolar setting into account
              if( (_data[1] & 1) == 0 )
                // Bipolar, so can be negative...
                if( adc_cnt & 0x800000 ) adc_cnt |= 0xFF000000;
              oss << "  B-sensor chan " << setw(2) << (int) _data[0]
                  << ":" << setw(6) << adc_cnt;
            }
          else
            {
              oss << "  Data:" << hex << setfill('0');
              for( int i=0; i<_dlc; ++i )
                oss << " " << setw(2) << (int) _data[i];
              oss << dec;
            }
        }
    }

  return oss.str();
}

/* ------------------------------------------------------------------------ */
