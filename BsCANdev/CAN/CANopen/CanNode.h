/* ------------------------------------------------------------------------
File   : CanNode.h

Descr  : Definition of a CANopen node class.

History: --DEC.09; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef CANNODE_H
#define CANNODE_H

#include <string>

enum NgStatus {
  NG_STATUS_OKAY            = 0,
  NG_STATUS_ERR_TOGGLE      = 0xFC,
  NG_STATUS_ERR_TIMEOUT     = 0xFE,
  NG_STATUS_ERR_SEND        = 0xFF
};

enum SdoStatus {
  SDO_STATUS_OKAY           = 0,
  SDO_STATUS_WRITE_COMPLETE = 0xF1,
  SDO_STATUS_READ_COMPLETE  = 0xF2,

  SDO_STATUS_ERR_ABORT      = 0xF8, //SDO_ABORT_TRANSFER
  SDO_STATUS_ERR_INDEX      = 0xF9,
  SDO_STATUS_ERR_SIZE       = 0xFA,
  SDO_STATUS_ERR_BUSY       = 0xFB,
  SDO_STATUS_ERR_TOGGLE     = 0xFC,
  SDO_STATUS_ERR_PROTOCOL   = 0xFD,
  SDO_STATUS_ERR_TIMEOUT    = 0xFE,
  SDO_STATUS_ERR_SEND       = 0xFF
};

class CanInterface;
class CanMessage;
#ifndef NOT_USING_QT
class NodeTimer;
class QSemaphore;
class QMutex;
#endif // NOT_USING_QT

#ifdef WIN32
 // Differentiate between building the DLL or using it
 #ifdef MY_LIB_EXPORT
 #define MY_LIB __declspec(dllexport)
 #else
 #define MY_LIB __declspec(dllimport)
 #endif
#else
 #define MY_LIB
#endif // WIN32

/* ------------------------------------------------------------------------ */

class MY_LIB CanNode
{
 public:
  CanNode( CanInterface *can_intf,
           int           node_id );

  virtual ~CanNode();

  void stop             ();

  int  nodeId           ();
  bool setNodeId        ( int node_id );

  bool nmt              ( int command );
  bool waitForBootup    ( unsigned int timeout_ms );
  bool getEmergency     ( int *code, unsigned char data[8] );
  void emergencyReceived( CanMessage *msg );

  void timedOut         ( int timer_id );

  // Nodeguarding (synchronous: request + reply)
  bool nodeGuard        ( int *status, bool *toggle,
                          unsigned int timeout_ms = 10 );
  // Nodeguarding request
  bool nodeGuardRequest ( unsigned int timeout_ms = 10 );
  // Asynchronously received Nodeguard message processing
  NgStatus nodeGuardReceived( CanMessage *msg );
  void     nodeGuardTimedOut();
  bool     nodeGuardTimeout() { return _ngTimeout; }

  int nmtState() { return _nmtState; }

  // SDO Expedited Write operation (synchronous: request + reply)
  bool sdoWriteExpedited( int           index,
                          int           subindex,
                          int           no_of_bytes,
                          unsigned char databytes[4],
                          unsigned int  timeout_ms );
  bool sdoWriteExpedited( int           index,
                          int           subindex,
                          int           no_of_bytes,
                          int           data,
                          unsigned int  timeout_ms );

  // SDO Segmented Write operation (synchronous: requests + replies)
  bool sdoWriteSegmented( int            index,
                          int            subindex,
                          int            no_of_bytes,
                          unsigned char *databytes,
                          unsigned int   timeout_ms );

  // SDO Expedited or Segmented Read operation (synchronous: requests+replies)
  bool sdoRead          ( int            index,
                          int            subindex,
                          int           *no_of_bytes,
                          unsigned char *databytes,
                          unsigned int   timeout_ms,
                          int            max_bytes = 4 );

  // SDO Expedited Read operation (synchronous: request + reply)
  bool sdoReadExpedited ( int           index,
                          int           subindex,
                          int          *no_of_bytes,
                          int          *data,
                          unsigned int  timeout_ms );

  // Asynchronous SDO operation requests
  bool sdoReadRequest          ( int          index,
                                 int          subindex,
                                 unsigned int timeout_ms );
  bool sdoReadDummyRequest     ( int          index,
                                 int          subindex,
                                 unsigned int timeout_ms );
  bool sdoWriteExpeditedRequest( int           index,
                                 int           subindex,
                                 int           no_of_bytes,
                                 unsigned char databytes[4],
                                 unsigned int  timeout_ms );
  bool sdoWriteExpeditedRequest( int           index,
                                 int           subindex,
                                 int           no_of_bytes,
                                 int           data,
                                 unsigned int  timeout_ms );
  bool sdoWriteSegmentedRequest( int            index,
                                 int            subindex,
                                 int            no_of_bytes,
                                 unsigned char *databytes,
                                 unsigned int   timeout_ms );
  bool sdoWriteDummyRequest    ( int           index,
                                 int           subindex,
                                 int           no_of_bytes,
                                 unsigned int  timeout_ms,
                                 bool          expedited = true );

  // Asynchronously received SDO message processing
  SdoStatus      sdoReceived( CanMessage *msg );
  void           sdoCancel();
  void           sdoTimedOut();

  // SDO status and results
  // (inlined functions moved to CanNode.cpp, as it was noticed these sometimes
  // lead to problems due to compiler optimizations, 25 Jul 2013)
  SdoStatus      sdoStatus();
  int            sdoIndex();
  int            sdoSubIndex();
  int            sdoSize();
  int            sdoMaxSize();
  unsigned char *sdoData();
  bool           sdoTimeout();
  std::string    sdoStatusString();

  bool cobRead( int           cob_id,
                int          *no_of_bytes,
                unsigned char data[8] );

  bool cobRead( int            cob_id,
                int           *no_of_bytes,
                unsigned char  data[8],
                unsigned int   timeout_ms );

  bool getSerialNumber   ( int *serialno );
  bool getFirmwareVersion( char *version );
  bool getFirmwareVersion( std::string &version_str );

  bool saveConfig( unsigned int timeout_ms = 500 );
  bool eraseConfig( unsigned int timeout_ms = 500 );

  std::string errString();

  std::string sdoErrCodeString( int code );

 protected:
  // Send an SDO Abort Transfer message
  bool sdoAbortTransfer();

  // Write the next segment in an SDO Segmented Write operation
  SdoStatus sdoWriteNextSegment();

  // Request the next segment in an SDO Segmented Read operation
  SdoStatus sdoReadNextSegmentRequest();

 protected:
  CanInterface*  _canIntf; // The CAN-interface to use to access this node
  int            _nodeId;
  std::string    _errString;

  // Nodeguard stuff
  int            _nmtState;
  NgStatus       _ngStatus;
  unsigned char  _ngToggleBit;
  bool           _ngTimeout;
#ifndef NOT_USING_QT
  NodeTimer     *_ngTimer;
#endif // NOT_USING_QT

  // SDO stuff

  // Allow up to 8 kBytes in a segmented transfer
  static const int SDO_MAX_SIZE = 8192;
  // Private SDO command specifier value, indicating
  // that no SDO operation is in progress
  static const unsigned char SDO_IDLE = (unsigned char) 0xFF;
  // Various variables to keep track of the current SDO operation's status
  SdoStatus      _sdoStatus;
  unsigned char  _sdoOdIndexLo, _sdoOdIndexHi, _sdoOdSubIndex;
  unsigned char  _sdoCsExpected, _sdoToggleBit;
  bool           _sdoTimeout;
  int            _sdoSize;
  int            _sdoRdWrIndex;
  unsigned int   _sdoErrClass, _sdoErrCode;
  // Storage space for data transferred in an SDO operation
  unsigned char  _sdoData[SDO_MAX_SIZE];
#ifndef NOT_USING_QT
  QSemaphore    *_sdoSemaphore;
  QMutex        *_sdoMutex;
  NodeTimer     *_sdoTimer;
#endif // NOT_USING_QT

  // Emergency stuff (just store the last Emergency message data, for now)
  unsigned char  _emergencyBytes[8];
};

/* ------------------------------------------------------------------------ */
#endif // CANNODE_H
