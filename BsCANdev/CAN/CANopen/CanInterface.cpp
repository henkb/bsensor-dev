/* ------------------------------------------------------------------------
File   : CanInterface.cpp

Descr  : CAN interface (abstract) base class implementation,
         for CANopen-based CAN networks. 

History: --FEB.04; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#include <sstream>

#include "CANopen.h"
#include "CanInterface.h"
#include "CanNode.h"
#include "CanMessage.h"

/* ------------------------------------------------------------------------ */

CanInterface::CanInterface( std::string man_name,
                            int         intf_type,
                            int         port_nr,
                            int         kbitrate )
  : _manufacturerName(man_name), _interfaceType(intf_type),
    _portNumber(port_nr), _kbitRate(kbitrate), _isOpen(false),
    _opStatus(0), _msgErrFlags(0), _skipMessages(false)
{
  // Default initialization information string, which will be overwritten
  // if anything fails in the interface-specific initialization
  std::ostringstream oss;
  oss << man_name
      << " CAN-port initialized, port " << _portNumber
      << ", " << kbitrate << " kbit/s";
  _initResultString = oss.str();
}

/* ------------------------------------------------------------------------ */

CanMessage* CanInterface::getMsg( int cob_id )
{
  CanMessage *pMsg = 0;

  // Get a (particular) CAN-message from the list
  if( !_canMsgs.empty() )
    {
      std::list<CanMessage *>::iterator it;
      for( it=_canMsgs.begin(); it!=_canMsgs.end(); )
        {
          if( (*it)->cobId() == cob_id )
            {
              // A match: get the message from the list
              pMsg = *it;

              // Remove the message pointer from the list
              _canMsgs.erase( it );

              // Interrupt the for-loop
              break;
            }
          else
            {
              if( _skipMessages )
                {
                  // Remove the message and the message pointer from the list
                  delete (*it);
                  it = _canMsgs.erase( it );
                }
              else
                {
                  ++it;
                }
            }
        }
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

void CanInterface:: msgHandled( CanMessage *msg )
{
  delete msg;
}

/* ------------------------------------------------------------------------ */

bool CanInterface::nmt( int command )
{
  // Send NMT message to all nodes on the bus

  // (effectively using a CanNode object with node identifier equal to 0)
  // (Replaced by code below; a bit too much overhead ? Henk 13 May 2012)
  //CanNode cn( this, 0 );
  //return cn.nmt( command );

  unsigned char data[2] = { (unsigned char) (command & 0xFF), 00 };
  CanMessage nmt_msg( NMT_OBJ, false, 2, data, 0 );
  // ...and send it
  return( this->send( nmt_msg ) );
}

/* ------------------------------------------------------------------------ */

bool CanInterface::sendSync()
{
  // Create a CANopen SYNC message
  CanMessage sync_msg( SYNC_OBJ, false, 0, 0, 0 );
  // ...and send it
  return( this->send( sync_msg ) );
}

/* ------------------------------------------------------------------------ */

int CanInterface::scanBusSynchronized( bool get_serialnr )
{
  // Clear the currently held list of node information
  _scannedNodeInfo.clear();

  // Scan the bus for CAN(open) nodes
  int      id, cnt = 0;
  int      no_of_bytes, data;
  char     version_str[10];
  NodeInfo n_info;
  CanNode  node( this, 0 );
  for( id=1; id<128; ++id )
    {
      node.setNodeId( id );

      // Is this node present (does it reply to a request of Object 1000h,
      // an object available on *any* CANopen node) ?
      if( node.sdoReadExpedited( OD_DEVICE_TYPE, 0x00,
                                 &no_of_bytes, &data, 15 ) )
        {
          ++cnt;

          // Store the Node Identifier
          n_info.id = id;

          std::string str;
          if( node.getFirmwareVersion( version_str ) )
            {
              str = version_str;
              if( version_str[4] == '\0' )
                str += ".----";
            }
          else
            {
              str = "----.----";
            }
          // Store the software version string
          n_info.sw_version = str;

          int sn = 0;
          if( get_serialnr ) node.getSerialNumber( &sn );
          // Store the serial number
          n_info.serial_nr = sn;

          // Store this node's info
          _scannedNodeInfo.push_back( n_info );
        }
    }
  node.stop();
  return cnt;
}

/* ------------------------------------------------------------------------ */

int CanInterface::scanBus( unsigned int timeout_ms,
                           bool         get_serialnr )
{
  // Scan the bus for CAN(open) nodes
  // by issuing SDO reads for Object 1000h

  // Clear the currently held list of node information
  _scannedNodeInfo.clear();

  // Create a CANopen SDO Read message for Object 1000h, subindex 0
  int index    = OD_DEVICE_TYPE;
  int subindex = 0x00;
  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char can_data[8];
  can_data[0] = SDO_INITIATE_UPLOAD_REQ;
  can_data[1] = index_lo;
  can_data[2] = index_hi;
  can_data[3] = subindex;
  for( int i=0; i<4; ++i ) can_data[4+i] = 0x00;
  CanMessage sdo_msg( SDORX_OBJ, false, 8, can_data, 0 );

  // Send it to all possible nodes
  int id;
  for( id=1; id<128; ++id )
    {
      sdo_msg.setNodeId( id );
      if( this->send( sdo_msg ) == false ) return 0;
    }
  // Need to wait for all messages to be transmitted
  // or subsequent calls node.cobRead() are done too soon...
  this->awaitTransmission( timeout_ms );

  // Check for replies, observing the set timeout at least once
  int      cnt = 0, timeout = 100;
  char     version_str[10];
  int      no_of_bytes;
  bool     found;
  NodeInfo n_info;
  CanNode  node( this, 0 );
  for( id=1; id<128; ++id )
    {
      node.setNodeId( id );

      // Observe the 'timeout' at least for the first NodeID *not* found
      if( cnt == id-1 )
        found = node.cobRead( SDOTX_OBJ + id, &no_of_bytes,
                              can_data, timeout );
      else
        found = node.cobRead( SDOTX_OBJ + id, &no_of_bytes, can_data );

      if( found )
        {
          ++cnt;

          // Store the Node Identifier
          n_info.id = id;

          // Store the Node Identifier
          _scannedNodeInfo.push_back( n_info );
        }
    }

  // Get the firmware version from the nodes found
  for( unsigned int i=0; i<_scannedNodeInfo.size(); ++i )
    {
      node.setNodeId( _scannedNodeInfo[i].id );

      std::string str;
      if( node.getFirmwareVersion( version_str ) )
        {
          str = version_str;
          if( version_str[4] == '\0' )
            str += ".----";
        }
      else
        {
          str = "----.----";
        }
      // Store the software version string
      _scannedNodeInfo[i].sw_version = str;

      int sn = 0;
      if( get_serialnr ) node.getSerialNumber( &sn );
      // Store the serial number
      _scannedNodeInfo[i].serial_nr = sn;
    }
  node.stop();

  // Return the number of nodes found
  return cnt;
}

/* ------------------------------------------------------------------------ */

int CanInterface::scannedNodeId( unsigned int index )
{
  if( _scannedNodeInfo.size() > index )
    return _scannedNodeInfo[index].id;
  return -1;
}

/* ------------------------------------------------------------------------ */

std::string CanInterface::scannedSwVersion( unsigned int index )
{
  if( _scannedNodeInfo.size() > index )
    return _scannedNodeInfo[index].sw_version;
  return std::string("");
}

/* ------------------------------------------------------------------------ */

int CanInterface::scannedSerialNr( unsigned int index )
{
  if( _scannedNodeInfo.size() > index )
    return _scannedNodeInfo[index].serial_nr;
  return -1;
}

// ----------------------------------------------------------------------------

#include <QLibrary>

// Prototype function pointers for the dynamically loaded libraries
// used in the (static) functions below
typedef CanInterface* (*NewIntf)  ( int port_nr, int kbitrate, bool open_now );
typedef int           (*PortsList)( int *port_numbers );

// ----------------------------------------------------------------------------

CanInterface *CanInterface::newCanInterface( int  intf_type,
                                             int  port_nr,
                                             int  kbitrate,
                                             bool open_now )
{
  // Factory method for new CAN interface port objects,
  // dynamically loading the appropriate library/DLL
  CanInterface *intf = 0;

  // Function pointer to the function that will return
  // a CAN port object of the requested type
  NewIntf new_intf = 0;

  switch( intf_type )
    {
    case KVASER_INTF_TYPE:
      new_intf = (NewIntf) QLibrary::resolve( "KvaserInterface",
                                              "newKvaserInterface" );
      if( new_intf )
        intf = new_intf( port_nr, kbitrate, open_now );
      break;

#ifdef WIN32
    case SYSTEC_INTF_TYPE:
      new_intf = (NewIntf) QLibrary::resolve( "SystecInterface",
                                              "newSystecInterface" );
      if( new_intf )
        intf = new_intf( port_nr, kbitrate, open_now );
      break;
#else
    case SOCKET_INTF_TYPE:
      new_intf = (NewIntf) QLibrary::resolve( "SocketCanInterface",
                                              "newSocketCanInterface" );
      if( new_intf )
        intf = new_intf( port_nr, kbitrate, open_now );
      break;
#endif // WIN32

#ifdef WIN32
    case PEAK_INTF_TYPE:
      new_intf = (NewIntf) QLibrary::resolve( "PeakInterface",
                                              "newPeakInterface" );
      if( new_intf )
        intf = new_intf( port_nr, kbitrate, open_now );
      break;

    case NICAN_INTF_TYPE:
      new_intf = (NewIntf) QLibrary::resolve( "NiCanInterface",
                                              "newNiCanInterface" );
      if( new_intf )
        intf = new_intf( port_nr, kbitrate, open_now );
      break;
#endif // WIN32
    default:
      break;
    }
  return intf;
}

// ----------------------------------------------------------------------------

int CanInterface::canInterfacePorts( int intf_type, int **port_numbers )
{
  // Determine the available number of ports of a particular interface type,
  // dynamically loading the appropriate library/DLL.
  // Returns a pointer to the list of port numbers to the user
  static int PortNumbers[128];
  *port_numbers = PortNumbers;

  // Function pointer to the function that will give us
  // the list of available CAN ports of the requested type
  PortsList ports_list = 0;

  switch( intf_type )
    {
    case KVASER_INTF_TYPE:
      ports_list = (PortsList) QLibrary::resolve( "KvaserInterface",
                                                  "kvaserInterfacePorts" );
      break;

#ifdef WIN32
    case SYSTEC_INTF_TYPE:
      ports_list = (PortsList) QLibrary::resolve( "SystecInterface",
                                                  "systecInterfacePorts" );
      break;
#else
    case SOCKET_INTF_TYPE:
      ports_list = (PortsList) QLibrary::resolve( "SocketCanInterface",
                                                  "socketCanInterfacePorts" );
      break;
#endif // WIN32

#ifdef WIN32
    case PEAK_INTF_TYPE:
      ports_list = (PortsList) QLibrary::resolve( "PeakInterface",
                                                  "peakInterfacePorts" );
      break;

    case NICAN_INTF_TYPE:
      ports_list = (PortsList) QLibrary::resolve( "NiCanInterface",
                                                  "niCanInterfacePorts" );
      break;
#endif // WIN32

    default:
      break;
    }

  int portcount = 0;
  // The 'PortNumbers[]' array is filled by function 'ports_list'
  if( ports_list ) portcount = ports_list( PortNumbers );
  return portcount;
}

/* ------------------------------------------------------------------------ */
