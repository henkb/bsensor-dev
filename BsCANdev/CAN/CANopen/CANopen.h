/* ------------------------------------------------------------------------
File   : CANopen.h

Descr  : Definitions for CANopen.

History: 29.06.98; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef CANOPEN_H
#define CANOPEN_H

/* ------------------------------------------------------------------------ */
// Communication Objects for the CANopen Predefined Connection Set

#define COBID_MASK                        0x7FF
#define OBJECT_MASK                       0x780
#define NODEID_MASK                       0x07F

#define NMT_OBJ                           0x000
#define SYNC_OBJ                          0x080
#define EMERGENCY_OBJ                     SYNC_OBJ
#define TIMESTAMP_OBJ                     0x100
#define TPDO1_OBJ                         0x180
#define RPDO1_OBJ                         0x200
#define TPDO2_OBJ                         0x280
#define RPDO2_OBJ                         0x300
#define TPDO3_OBJ                         0x380
#define RPDO3_OBJ                         0x400
#define TPDO4_OBJ                         0x480
#define RPDO4_OBJ                         0x500
#define SDOTX_OBJ                         0x580
#define SDORX_OBJ                         0x600
#define NODEGUARD_OBJ                     0x700
#define BOOTUP_OBJ                        NODEGUARD_OBJ
#define LSSREQ_OBJ                        0x7E5
#define LSSREPLY_OBJ                      0x7E4

/* ------------------------------------------------------------------------ */
// NMT Start/Stop Service command specifiers

#define NMT_START_REMOTE_NODE             1
#define NMT_STOP_REMOTE_NODE              2
#define NMT_ENTER_PREOPERATIONAL_STATE    0x80
#define NMT_RESET_NODE                    0x81
#define NMT_RESET_COMMUNICATION           0x82

/* ------------------------------------------------------------------------ */
// NMT Slave state

#define NMT_INITIALISING                  0
#define NMT_DISCONNECTED                  1
#define NMT_CONNECTING                    2
#define NMT_PREPARING                     3
#define NMT_STOPPED                       4
#define NMT_OPERATIONAL                   5
#define NMT_PREOPERATIONAL                127

#define NMT_STATE_MASK                    0x7F
#define NMT_TOGGLE_MASK                   0x80

/* ------------------------------------------------------------------------ */
// SDO command specifiers and other bits

#define SDO_COMMAND_SPECIFIER_MASK        (7<<5)

// Client command specifiers (0x20, 0x00, 0x40, 0x60)
#define SDO_INITIATE_DOWNLOAD_REQ         (1<<5)
#define SDO_DOWNLOAD_SEGMENT_REQ          (0<<5)
#define SDO_INITIATE_UPLOAD_REQ           (2<<5)
#define SDO_UPLOAD_SEGMENT_REQ            (3<<5)

// Server command specifiers (0x60, 0x20, 0x40, 0x00)
#define SDO_INITIATE_DOWNLOAD_RESP        (3<<5)
#define SDO_DOWNLOAD_SEGMENT_RESP         (1<<5)
#define SDO_INITIATE_UPLOAD_RESP          (2<<5)
#define SDO_UPLOAD_SEGMENT_RESP           (0<<5)

// Client or Server command specifiers (0x80)
#define SDO_ABORT_TRANSFER                (4<<5)

// Expedited- and Segmented-SDO related bits (0x02, 0x10, 0x01(3x), 0x0E, 0x0C)
#define SDO_EXPEDITED                     (1<<1)
#define SDO_TOGGLE_BIT                    (1<<4)
#define SDO_LAST_SEGMENT                  (1<<0)
#define SDO_SEGMENT_SIZE_INDICATED        (1<<0)
#define SDO_DATA_SIZE_INDICATED           (1<<0)
#define SDO_SEGMENT_SIZE_MASK             (0x7<<1)
#define SDO_DATA_SIZE_MASK                (0x3<<2)
#define SDO_SEGMENT_SIZE_SHIFT            1
#define SDO_DATA_SIZE_SHIFT               2

/* ------------------------------------------------------------------------ */
// SDO Abort Domain Transfer protocol: abort codes

// Error classes (MSB)
#define SDO_ECLASS_SERVICE                5
#define SDO_ECLASS_ACCESS                 6
#define SDO_ECLASS_OTHER                  8

// Error codes (MSB-1)
#define SDO_ECODE_PAR_INCONSISTENT        3
#define SDO_ECODE_PAR_ILLEGAL             4
#define SDO_ECODE_ACCESS                  1
#define SDO_ECODE_NONEXISTENT             2
#define SDO_ECODE_HARDWARE                6
#define SDO_ECODE_TYPE_CONFLICT           7
#define SDO_ECODE_ATTRIBUTE               9
#define SDO_ECODE_OKAY                    0

/* ------------------------------------------------------------------------ */
// CANopen Object Dictionary indices and sub-indices

#define OD_DEVICE_TYPE                    0x1000
#define OD_ERROR_REG                      0x1001
#define OD_STATUS_REG                     0x1002
#define OD_DEVICE_NAME                    0x1008
#define OD_HW_VERSION                     0x1009
#define OD_SW_VERSION                     0x100A
#define OD_GUARDTIME                      0x100C
#define OD_LIFETIME_FACTOR                0x100D
#define OD_STORE_PARAMETERS               0x1010
#define OD_DFLT_PARAMETERS                0x1011
#define OD_HEARTBEAT_TIME                 0x1017
#define OD_IDENTITY                       0x1018
#define OD_STORE_ALL                      1
#define OD_STORE_COMM_PARS                2
#define OD_STORE_APP_PARS                 3
// Often subindex 0 provides access to the number of entries in the object
#define OD_OBJECT_ENTRIES                 0

// Receive PDOs
#define OD_RPDO1_PAR                      0x1400
#define OD_RPDO1_MAP                      0x1600

// Transmit PDOs
#define OD_TPDO1_PAR                      0x1800
#define OD_TPDO2_PAR                      0x1801
#define OD_TPDO3_PAR                      0x1802
#define OD_TPDO4_PAR                      0x1803
#define OD_PDO_COBID                      1
#define OD_PDO_TRANSMTYPE                 2
#define OD_PDO_INHIBITTIME                3
#define OD_PDO_DUMMY_ENTRY                4
#define OD_PDO_EVENT_TIMER                5

// PDO mapping
#define OD_TPDO1_MAP                      0x1A00
#define OD_TPDO2_MAP                      0x1A01
#define OD_TPDO3_MAP                      0x1A02
#define OD_TPDO4_MAP                      0x1A03

// Manufacturer-specific objects: ELMB

// Serial Number
#define OD_SERIAL_NO                      0x3100
#define OD_SN_WRITE_ENA                   0x3101
// CAN-controller configuration
#define OD_CAN_CONFIG                     0x3200
// CANopen Node-ID configuration
#define OD_NODEID_CONFIG                  0x3300
#define OD_NODEID_WRITE_ENA               0x3301
// Other
#define OD_SWITCH_TO_LOADER               0x5E00

// Some standardised Device Profile objects

// Digital inputs
#define OD_DIGITAL_IN_8                   0x6000
#define OD_DIGIN_INTRPT_ENA               0x6005
#define OD_DIGIN_INTRPT_MSK               0x6006

// Digital outputs
#define OD_DIGITAL_OUT_8                  0x6200
#define OD_DIGITAL_OUT_8_MSK              0x6208
#define OD_DIGITAL_OUT_1                  0x6220

// Analog inputs
#define OD_ANALOG_IN                      0x6404

/* ------------------------------------------------------------------------ */
#endif // CANOPEN_H
