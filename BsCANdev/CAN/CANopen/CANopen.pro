#
# Project file for the CANopen library
#
# To generate a Visual Studio project:
#   qmake -t vclib CANopen.pro
# To generate a Makefile:
#   qmake CANopen.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = CANopen

# Create a shared library, uses Qt
CONFIG += shared qt thread warn_on exceptions debug_and_release 

QT -= gui
QT += core

DEFINES += MY_LIB_EXPORT

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
}

SOURCES += CanInterface.cpp
SOURCES += CanMessage.cpp
SOURCES += CanNode.cpp
SOURCES += NodeTimer.cpp

HEADERS += CanInterface.h
HEADERS += CanMessage.h
HEADERS += CanNode.h
HEADERS += CANopen.h
HEADERS += NodeTimer.h
