#include "NodeTimer.h"
#include "CanNode.h"

// ----------------------------------------------------------------------------

NodeTimer::NodeTimer( CanNode *node, int timer_id, QObject *parent )
  : QThread( parent ),
    _canNode( node ),
    _timerId( timer_id ),
    _stop( false ),
    _timeout( true ),
    _timeoutMs( 0 )
{
  this->start();
}

// ----------------------------------------------------------------------------

NodeTimer::~NodeTimer()
{
  // In case still running...
  this->stop();
}

// ----------------------------------------------------------------------------

void NodeTimer::stop()
{
  if( this->isRunning() )
    {
      // Run() may be in front of _sem1.acquire(),
      // so we first need to start a time-out operation,
      // which we cancel immediately, to be able to exit
      this->setTimeout( 1000 );
      _stop = true;
      this->cancelTimeout();
      this->wait(); // Wait until this thread (i.e. function run()) exits
    }
}

// ----------------------------------------------------------------------------

void NodeTimer::run()
{
  bool ret;
  while( !_stop )
    {
      // Wait until setTimeout() instructs us to proceed
      // and start a time-out operation
      _sem1.acquire();

      _timeout = false;

      // Synchronize with setTimeout(), to start the actual time-out operation
      // by means of a wait-condition (so can be canceled!)
      // with the requested time-out period
      _mutex.lock();
      _sem2.release();
      ret = _condition.wait( &_mutex, _timeoutMs );
      // Check if the condition timed out and indicate it if so
      if( ret == false )
        {
          _timeout = true;
          // Depending on _timerId the node takes action
          if( _canNode ) _canNode->timedOut( _timerId );
        }
      _mutex.unlock();
    }
}

// ----------------------------------------------------------------------------

void NodeTimer::setTimeout( int ms )
{
  // If a time-out operation is in progress, cancel it
  // (if it isn't (anymore) the wakeOne() call does nothing)
  if( _timeout == false ) _condition.wakeOne();

  // Synchronize with run() using the semaphore,
  // but first set the requested time-out period !
  _timeoutMs = ms;
  _sem1.release();

  // Wait for run() to enter the wait condition
  // using semaphore _sem2 in combination with _mutex:
  // the semaphore acquire() guarantees run() is in its mutex part of code
  // and the _mutex.lock() below guarantees the wait-condition in run()
  // has been entered before exiting this method
  // (and calling it again immediately, for example)
  _sem2.acquire();
  _mutex.lock();
  _mutex.unlock();
}

// ----------------------------------------------------------------------------

void NodeTimer::cancelTimeout()
{
  // If a time-out operation is in progress, cancel it
  // (if it isn't (anymore) the wakeOne() call does nothing)
  // ### NB: don't use _mutex here: in _canNode there is a mutex as well
  //     (at least for the SDO timer), and this may cause a deadlock
  //     in combination with _canNode->timedOut() above.
  //_mutex.lock();
  if( _timeout == false ) _condition.wakeOne();
  //_mutex.unlock();
}

// ----------------------------------------------------------------------------
