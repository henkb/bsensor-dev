/* ------------------------------------------------------------------------
File   : CanNode.h

Descr  : Implementation of a CANopen node class.

History: --DEC.09; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#include <time.h>

#include "CANopen.h"
#include "CanInterface.h"
#include "CanMessage.h"
#include "CanNode.h"

#ifndef NOT_USING_QT
#include <QMutex>
#include <QMutexLocker>
#include <QSemaphore>
#include "NodeTimer.h"
#endif // NOT_USING_QT

/* ------------------------------------------------------------------------ */

CanNode::CanNode( CanInterface *can_intf,
                  int           node_id )
  : _canIntf(can_intf)
{
#ifndef NOT_USING_QT
  _sdoSemaphore = new QSemaphore( 1 );
  _sdoMutex     = new QMutex();
  _sdoTimer     = new NodeTimer( this, 1 );
  _ngTimer      = new NodeTimer( this, 2 );
#endif // NOT_USING_QT

  this->setNodeId( node_id );
}

/* ------------------------------------------------------------------------ */

CanNode::~CanNode()
{
#ifndef NOT_USING_QT
  // ### Stop() doesn't work nicely in this d'tor: thread is already gone
  //     before NodeTimer::stop() can finish doing its business !?
  //     an application crash results...
  //_sdoTimer->stop();

  // ### Under Linux we hang if we do the next statements here ?
  // ### So moved them to new function stop() to be called before ~CanNode
  //_sdoTimer->terminate();
  //delete _sdoTimer;
  //delete _sdoMutex;
  //delete _sdoSemaphore;
#endif // NOT_USING_QT
}

/* ------------------------------------------------------------------------ */

void CanNode::stop()
{
#ifndef NOT_USING_QT
  _sdoTimer->stop();
  _ngTimer->stop();
  delete _sdoTimer;
  delete _ngTimer;
  delete _sdoMutex;
  delete _sdoSemaphore;
#endif // NOT_USING_QT
}

/* ------------------------------------------------------------------------ */

int CanNode::nodeId()
{
  return _nodeId;
}

/* ------------------------------------------------------------------------ */

bool CanNode::setNodeId( int node_id )
{
  // For convenience allow a node identifier of 0 too; this is for example
  // used in the firmware-upgrade-via-CAN-bus procedure where SDO messages
  // are 'broadcast'
  if( node_id < 0 || node_id > 127 ) return false;

  // Changing this node's ID means we aren't interested anymore
  // in the ongoing SDO and/or Nodeguarding operations
  this->sdoCancel();

  _nodeId = node_id;
  // Initialize
  _nmtState      = NMT_DISCONNECTED;
  _ngStatus      = NG_STATUS_OKAY;
  // Defining _ngToggleBit as unknown sofar
  _ngToggleBit   = (unsigned char) ((~NMT_TOGGLE_MASK) & 0xFF);
  _ngTimeout     = false;
  _sdoStatus     = SDO_STATUS_OKAY;
  _sdoCsExpected = SDO_IDLE;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoTimeout    = false;
  _sdoSize       = 0;
  _sdoRdWrIndex  = 0;
  _sdoErrClass   = 0;
  _sdoErrCode    = 0;

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::nmt( int command )
{
  unsigned char can_data[2];
  bool          result = false;

  if( command == NMT_START_REMOTE_NODE          ||
      command == NMT_STOP_REMOTE_NODE           ||
      command == NMT_ENTER_PREOPERATIONAL_STATE ||
      command == NMT_RESET_NODE                 ||
      command == NMT_RESET_COMMUNICATION )
    {
      can_data[0] = command;
      can_data[1] = _nodeId;

      // Create a CANopen NMT message
      CanMessage nmt_msg( NMT_OBJ, false, 2, can_data, 0 );

      // Send it...
      if( _canIntf->send( nmt_msg ) )
        result = true;
      else
        _errString = _canIntf->errString();
    }
  else
    {
      _errString = "NMT illegal command";
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::waitForBootup( unsigned int timeout_ms )
{
  bool result = false;
  CanMessage *pbootup = _canIntf->receiveSpecific( BOOTUP_OBJ | _nodeId,
                                                   timeout_ms );
  if( pbootup )
    {
      // Check if the reply contains the proper databyte (=0)
      // (otherwise it is a NodeGuarding/Heartbeat message!)
      unsigned char *pdata = pbootup->data();
      if( pdata[0] == 0 )
        {
          result = true;
          // Initialize local stuff by calling setNodeId()
          this->setNodeId( _nodeId );
        }
      else
        {
          _errString = "BOOTUP has data != 0, i.e. is Nodeguard reply";
        }
      _canIntf->msgHandled( pbootup );
    }
  else
    {
      _errString = _canIntf->errString();
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::getEmergency( int *code, unsigned char data[8] )
{
  // Check for and get the first Emergency message in the message list
  bool result = false;
  CanMessage *pemg = _canIntf->receiveSpecific( EMERGENCY_OBJ | _nodeId, 0 );
  if( pemg )
    {
      if( pemg->dlc() == 8 )
        {
          unsigned char *pdata = pemg->data();
          *code = (((int) pdata[1]) << 8) | (int) pdata[0];
          for( int i=0; i<8; ++i ) data[i] = pdata[i];
          result = true;
        }
      else
        {
          _errString = "Emergency has DLC != 8";
        }
      _canIntf->msgHandled( pemg );
    }
  return result;
}

/* ------------------------------------------------------------------------ */

void CanNode::emergencyReceived( CanMessage *msg_in )
{
  if( !msg_in ) return;

  // For now, simply copy the Emergency data bytes
  unsigned char *pdata = msg_in->data();
  for( int i=0; i<8; ++i ) _emergencyBytes[i] = pdata[i];
}

/* ------------------------------------------------------------------------ */

void CanNode::timedOut( int timer_id )
{
  // Take action based on the ID of the timer that timed out
  switch( timer_id )
    {
    case 1:
      this->sdoTimedOut();
      break;
    case 2:
      this->nodeGuardTimedOut();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

bool CanNode::nodeGuard( int *state, bool *toggle, unsigned int timeout_ms )
{
  // Send a Nodeguard request and wait for and receive the reply
  if( this->nodeGuardRequest( timeout_ms ) == false )
    return false;

#ifndef NOT_USING_QT
  _ngTimer->cancelTimeout(); // Here time-out handled locally
#endif // NOT_USING_QT

  // Receive the Nodeguard reply message
  bool result = false;
  CanMessage *png = _canIntf->receiveSpecific( NODEGUARD_OBJ | _nodeId,
                                               timeout_ms );
  if( png )
    {
      // Process the received Nodeguard message
      //NgStatus ngstat = nodeGuardReceived( png );
      //if( ngstat == NG_STATUS_OKAY )
      //result = true;

      if( png->dlc() == 1 )
        {
          // Get the status and toggle bit from the data byte in the reply
          unsigned char *pdata = png->data();
          unsigned char toggle_bit = pdata[0] & NMT_TOGGLE_MASK;
          _nmtState  = pdata[0] & NMT_STATE_MASK;
          *state  = _nmtState;
          *toggle = (toggle_bit == NMT_TOGGLE_MASK);

          // Check toggle bit
          if( _ngToggleBit != (unsigned char) ((~NMT_TOGGLE_MASK) & 0xFF) &&
              toggle_bit != _ngToggleBit )
            _ngStatus = NG_STATUS_ERR_TOGGLE;

          // Expected toggle bit next time
          _ngToggleBit = toggle_bit ^ NMT_TOGGLE_MASK;

          result = true;
        }
      else
        {
          _errString = "Nodeguard has DLC != 1"; 
          _ngStatus = NG_STATUS_ERR_TIMEOUT;
        }

      _canIntf->msgHandled( png );
    }
  else
    {
      _errString = _canIntf->errString();
      this->nodeGuardTimedOut();
    }

  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::nodeGuardRequest( unsigned int timeout_ms )
{
  // Send a Nodeguard request, and don't wait for the reply
  // but do set a timeout on the expected reply

  _ngStatus = NG_STATUS_OKAY;

  // Create a CANopen Nodeguarding Remote Frame message
  CanMessage ng_msg( NODEGUARD_OBJ | _nodeId, true, 1, 0, 0 );

  // Send it...
  if( _canIntf->send( ng_msg ) == false )
    {
      _errString = _canIntf->errString();
      _ngStatus = NG_STATUS_ERR_SEND;
      return false;
    }

  // Nodeguard administration
  _ngTimeout = false;

#ifndef NOT_USING_QT
  // Start this node's Nodeguard time-out timer
  _ngTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

NgStatus CanNode::nodeGuardReceived( CanMessage *msg_in )
{
  if( !msg_in ) return NG_STATUS_OKAY;

  // Is this actually a Nodeguard message for me ?
  if( msg_in->dlc() != 1 || msg_in->cobId() != (NODEGUARD_OBJ | _nodeId) )
    return NG_STATUS_OKAY;  // Ignore

  _ngStatus = NG_STATUS_OKAY;

  // Get the Nodeguard data byte: toggle bit and the node's NMT state */
  unsigned char *pdata = msg_in->data();
  unsigned char toggle_bit = pdata[0] & NMT_TOGGLE_MASK;
  _nmtState = pdata[0] & NMT_STATE_MASK;

  // Check toggle bit
  if( _ngToggleBit != (unsigned char) ((~NMT_TOGGLE_MASK) & 0xFF) &&
      toggle_bit != _ngToggleBit )
    {
      _ngStatus = NG_STATUS_ERR_TOGGLE;
      _errString = std::string( "NG_STAT_ERR_TOGGLE" );
    }

  // Expected toggle bit next time
  _ngToggleBit = toggle_bit ^ NMT_TOGGLE_MASK;

#ifndef NOT_USING_QT
  _ngTimer->cancelTimeout();
#endif // NOT_USING_QT

  return _ngStatus;
}

/* ------------------------------------------------------------------------ */

void CanNode::nodeGuardTimedOut()
{
  _ngStatus  = NG_STATUS_ERR_TIMEOUT;
  _ngTimeout = true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteExpedited( int           index,
                                 int           subindex,
                                 int           no_of_bytes,
                                 unsigned char databytes[4],
                                 unsigned int  timeout_ms )
{
  // Expects to execute an SDO *Expedited* Write operation

  // Send the request message...
  if( this->sdoWriteExpeditedRequest( index, subindex, no_of_bytes,
                                      databytes, timeout_ms ) == false )
    return false;

#ifndef NOT_USING_QT
  _sdoTimer->cancelTimeout(); // Here time-out handled locally
#endif // NOT_USING_QT

  // ..and get the reply message
  CanMessage *preply;
  bool        sdo_in_progress = true;
  bool        result = false;
  while( sdo_in_progress )
    {
      preply = _canIntf->receiveSpecific( SDOTX_OBJ | _nodeId, timeout_ms );
      if( preply )
        {
          // Process the received SDO message
          SdoStatus sdostat = sdoReceived( preply );

          // Done ?
          if( sdostat == SDO_STATUS_WRITE_COMPLETE )
            result = true;

          // Retry only if message contained the wrong (sub)index
          if( sdostat != SDO_STATUS_ERR_INDEX )
            sdo_in_progress = false;

          _canIntf->msgHandled( preply );
        }
      else
        {
          // Timeout
          sdo_in_progress = false;
          _errString = _canIntf->errString();
          this->sdoTimedOut();
        }
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteExpedited( int          index,
                                 int          subindex,
                                 int          no_of_bytes,
                                 int          data,
                                 unsigned int timeout_ms )
{
  // Expects to execute an SDO *Expedited* Write operation

  // Separate 'data' into individual bytes
  unsigned char databytes[4];
  for( int i=0; i<4; ++i, data>>=8 )
    databytes[i] = (unsigned char) (data & 0xFF);

  return( this->sdoWriteExpedited( index, subindex, no_of_bytes,
                                   databytes, timeout_ms ) );
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteSegmented( int            index,
                                 int            subindex,
                                 int            no_of_bytes,
                                 unsigned char *databytes,
                                 unsigned int   timeout_ms )
{
  // Send the request message...
  if( this->sdoWriteSegmentedRequest( index, subindex, no_of_bytes,
                                      databytes, timeout_ms ) == false )
    return false;

#ifndef NOT_USING_QT
  _sdoTimer->cancelTimeout(); // Here time-out handled locally
#endif // NOT_USING_QT

  CanMessage *preply;
  bool        result = false;
  bool        sdo_in_progress = true;
  while( sdo_in_progress )
    {
      preply = _canIntf->receiveSpecific( SDOTX_OBJ | _nodeId, timeout_ms );
      if( preply )
        {
          // Process the received SDO message
          SdoStatus sdostat = sdoReceived( preply );

          // Done ?
          if( sdostat == SDO_STATUS_WRITE_COMPLETE )
            result = true;

          // Continue until the SDO Write operation has finished
          // and also if a message contains the wrong (sub)index
          if( sdostat != SDO_STATUS_OKAY && sdostat != SDO_STATUS_ERR_INDEX )
            sdo_in_progress = false;

          _canIntf->msgHandled( preply );
        }
      else
        {
          // Timeout
          sdo_in_progress = false;
          _errString = _canIntf->errString();
          this->sdoTimedOut();
        }
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoRead( int            index,
                       int            subindex,
                       int           *no_of_bytes,
                       unsigned char *databytes,
                       unsigned int   timeout_ms,
                       int            max_bytes )
{
  // Perform an SDO Read operation, either Expedited or Segmented
  // of up to max_bytes data bytes

  // Send the (first) SDO Read request message...
  if( this->sdoReadRequest( index, subindex, timeout_ms ) == false )
    return false;

#ifndef NOT_USING_QT
  _sdoTimer->cancelTimeout(); // Here time-out handled locally
#endif // NOT_USING_QT

  CanMessage *preply;
  bool        result = false;
  bool        sdo_in_progress = true;
  while( sdo_in_progress )
    {
      preply = _canIntf->receiveSpecific( SDOTX_OBJ | _nodeId, timeout_ms );
      if( preply )
        {
          // Process the received SDO message
          SdoStatus sdostat = sdoReceived( preply );

          // Done ?
          if( sdostat == SDO_STATUS_READ_COMPLETE )
            {
              // SDO Read operation completed: set the number of bytes
              // and copy the data bytes from local storage to the caller
              *no_of_bytes = _sdoSize;
              if( _sdoSize < max_bytes )
                max_bytes = _sdoSize;
              for( int i=0; i<max_bytes; ++i ) databytes[i] = _sdoData[i];
              result = true;
            }

          // Continue until the SDO Read operation is finished
          // and also if a message contains the wrong (sub)index
          if( sdostat != SDO_STATUS_OKAY && sdostat != SDO_STATUS_ERR_INDEX )
            sdo_in_progress = false;

          _canIntf->msgHandled( preply );
        }
      else
        {
          // Timeout
          sdo_in_progress = false;
          _errString = _canIntf->errString();
          this->sdoTimedOut();
        }
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoReadExpedited( int           index,
                                int           subindex,
                                int          *no_of_bytes,
                                int          *data,
                                unsigned int  timeout_ms )
{
  // Expects to execute an SDO *Expedited* Read operation,
  // i.e. up to 4 data bytes, any additional bytes are lost !
  unsigned char bytes[4];
  if( this->sdoRead( index, subindex, no_of_bytes,
                     bytes, timeout_ms, 4 ) == false )
    return false;

  // Convert the data bytes into an integer, as requested by this call
  *data = 0;
  for( int i=0; i<(*no_of_bytes); ++i )
    *data |= (((int) bytes[i]) << (i*8));

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoReadRequest( int          index,
                              int          subindex,
                              unsigned int timeout_ms )
{
#ifndef NOT_USING_QT
  // Only one outstanding SDO request at a time allowed
  _sdoSemaphore->acquire();
#endif // NOT_USING_QT

  // SDO administration
  if( _sdoCsExpected != SDO_IDLE )
    {
      // Some SDO operation is in progress
      // (shouldn't occur when the semaphore is used)
      _sdoStatus = SDO_STATUS_ERR_BUSY;
      _errString = this->sdoStatusString();
      return false;
    }
  _sdoStatus   = SDO_STATUS_OKAY;
  _sdoErrClass = 0;
  _sdoErrCode  = 0;

  // Send the SDO Read message, don't wait for any reply
  unsigned char can_data[8];
  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char sub      = (unsigned char) subindex;

  can_data[0] = SDO_INITIATE_UPLOAD_REQ;
  can_data[1] = index_lo;
  can_data[2] = index_hi;
  can_data[3] = sub;
  for( int i=4; i<8; ++i ) can_data[i] = 0x00;

  // Create the CANopen SDO message object
  CanMessage sdo_msg( SDORX_OBJ | _nodeId, false, 8, can_data, 0 );

  // Send the message...
  if( _canIntf->send( sdo_msg ) == false )
    {
      _errString = _canIntf->errString();
      _sdoStatus = SDO_STATUS_ERR_SEND;
      return false;
    }

  // SDO administration
  _sdoCsExpected = SDO_INITIATE_UPLOAD_RESP;
  _sdoOdIndexLo  = index_lo;
  _sdoOdIndexHi  = index_hi;
  _sdoOdSubIndex = sub;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoSize       = 0;
  _sdoRdWrIndex  = 0;
  _sdoTimeout    = false;

#ifndef NOT_USING_QT
  // Start this node's SDO time-out timer
  _sdoTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoReadDummyRequest( int          index,
                                   int          subindex,
                                   unsigned int timeout_ms )
{
#ifndef NOT_USING_QT
  // Only one outstanding SDO request at a time allowed
  _sdoSemaphore->acquire();
#endif // NOT_USING_QT

  // SDO administration
  if( _sdoCsExpected != SDO_IDLE )
    {
      // Some SDO operation is in progress
      // (shouldn't occur when the semaphore is used)
      _sdoStatus = SDO_STATUS_ERR_BUSY;
      _errString = this->sdoStatusString();
      return false;
    }
  _sdoStatus   = SDO_STATUS_OKAY;
  _sdoErrClass = 0;
  _sdoErrCode  = 0;

  // Dummy: no (SDO) message is sent...

  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char sub      = (unsigned char) subindex;

  // SDO administration
  _sdoCsExpected = SDO_INITIATE_UPLOAD_RESP;
  _sdoOdIndexLo  = index_lo;
  _sdoOdIndexHi  = index_hi;
  _sdoOdSubIndex = sub;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoSize       = 0;
  _sdoRdWrIndex  = 0;
  _sdoTimeout    = false;

#ifndef NOT_USING_QT
  // Start this node's SDO time-out timer
  _sdoTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteExpeditedRequest( int           index,
                                        int           subindex,
                                        int           no_of_bytes,
                                        unsigned char databytes[4],
                                        unsigned int  timeout_ms )
{
#ifndef NOT_USING_QT
  // Only one outstanding SDO request at a time allowed
  _sdoSemaphore->acquire();
#endif // NOT_USING_QT

  // SDO administration
  if( _sdoCsExpected != SDO_IDLE )
    {
      // Some SDO operation is in progress
      // (shouldn't occur when the semaphore is used)
      _sdoStatus = SDO_STATUS_ERR_BUSY;
      _errString = this->sdoStatusString();
      return false;
    }
  _sdoStatus   = SDO_STATUS_OKAY;
  _sdoErrClass = 0;
  _sdoErrCode  = 0;

  // Send the SDO Expedited Write message, don't wait for any reply
  unsigned char can_data[8];
  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char sub      = (unsigned char) subindex;

  if( no_of_bytes > 4 ) no_of_bytes = 4;

  // Expedited transfer: set data size in command specifier byte
  int invalid_bytes = 4-no_of_bytes;
  can_data[0] = (SDO_INITIATE_DOWNLOAD_REQ | SDO_EXPEDITED |
                 SDO_DATA_SIZE_INDICATED |
                 (invalid_bytes << SDO_DATA_SIZE_SHIFT));
  can_data[1] = index_lo;
  can_data[2] = index_hi;
  can_data[3] = sub;

  // Put data directly in message, lower-significant byte first
  int i;
  for( i=0; i<no_of_bytes; ++i ) can_data[4+i] = databytes[i];
  for( i=no_of_bytes; i<4; ++i ) can_data[4+i] = 0x00;

  // Create the CANopen SDO message object
  CanMessage sdo_msg( SDORX_OBJ | _nodeId, false, 8, can_data, 0 );

  // Send the message...
  if( _canIntf->send( sdo_msg ) == false )
    {
      _errString = _canIntf->errString();
      _sdoStatus = SDO_STATUS_ERR_SEND;
      return false;
    }

  // SDO administration
  _sdoCsExpected = SDO_INITIATE_DOWNLOAD_RESP;
  _sdoOdIndexLo  = index_lo;
  _sdoOdIndexHi  = index_hi;
  _sdoOdSubIndex = sub;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoSize       = no_of_bytes;
  _sdoRdWrIndex  = no_of_bytes; // Data is already written with the message
  _sdoTimeout    = false;

#ifndef NOT_USING_QT
  // Start this node's SDO time-out timer
  _sdoTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteExpeditedRequest( int          index,
                                        int          subindex,
                                        int          no_of_bytes,
                                        int          data,
                                        unsigned int timeout_ms )
{
  // Starts an asynchronous SDO Expedited Write operation

  // Separate 'data' into individual bytes
  unsigned char databytes[4];
  for( int i=0; i<4; ++i, data>>=8 )
    databytes[i] = (unsigned char) (data & 0xFF);

  return( this->sdoWriteExpeditedRequest( index, subindex, no_of_bytes,
                                          databytes, timeout_ms ) );
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteSegmentedRequest( int            index,
                                        int            subindex,
                                        int            no_of_bytes,
                                        unsigned char *databytes,
                                        unsigned int   timeout_ms )
{
#ifndef NOT_USING_QT
  // Only one outstanding SDO request at a time allowed
  _sdoSemaphore->acquire();
#endif // NOT_USING_QT

  // SDO administration
  if( _sdoCsExpected != SDO_IDLE )
    {
      // Some SDO operation is in progress
      // (shouldn't occur when the semaphore is used)
      _sdoStatus = SDO_STATUS_ERR_BUSY;
      _errString = this->sdoStatusString();
      return false;
    }
  _sdoStatus   = SDO_STATUS_OKAY;
  _sdoErrClass = 0;
  _sdoErrCode  = 0;

  // The number of bytes that can be sent is limited (that's not CANopen..)
  if( no_of_bytes > SDO_MAX_SIZE )
    {
      _sdoStatus = SDO_STATUS_ERR_SIZE;
      _errString = this->sdoStatusString();
      return false;
    }

  unsigned char can_data[8];
  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char sub      = (unsigned char) subindex;

  // Segmented transfer: send total number of bytes in the first message
  int nbytes        = 4;
  int invalid_bytes = (4-nbytes);
  can_data[0] = (SDO_INITIATE_DOWNLOAD_REQ | SDO_DATA_SIZE_INDICATED |
                 (invalid_bytes << SDO_DATA_SIZE_SHIFT));
  can_data[1] = index_lo;
  can_data[2] = index_hi;
  can_data[3] = sub;
  can_data[4] = (unsigned char) ((no_of_bytes & 0x00FF) >> 0);
  can_data[5] = (unsigned char) ((no_of_bytes & 0xFF00) >> 8);
  can_data[6] = 0x00;
  can_data[7] = 0x00;

  // Create a CANopen SDO message
  CanMessage sdo_msg( SDORX_OBJ | _nodeId, false, 8, can_data, 0 );

  // Send the first message...
  if( _canIntf->send( sdo_msg ) == false )
    {
      _errString = _canIntf->errString();
      _sdoStatus = SDO_STATUS_ERR_SEND;
      return false;
    }

  // SDO administration
  _sdoCsExpected = SDO_INITIATE_DOWNLOAD_RESP;
  _sdoOdIndexLo  = index_lo;
  _sdoOdIndexHi  = index_hi;
  _sdoOdSubIndex = sub;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoSize       = no_of_bytes;
  _sdoRdWrIndex  = 0;
  _sdoTimeout    = false;
  // Copy the data bytes from the caller to local storage
  for( int i=0; i<no_of_bytes; ++i ) _sdoData[i] = databytes[i];

#ifndef NOT_USING_QT
  // Start this node's SDO time-out timer
  _sdoTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoWriteDummyRequest( int           index,
                                    int           subindex,
                                    int           no_of_bytes,
                                    unsigned int  timeout_ms,
                                    bool          expedited )
{
  // This function can be used to make this CanNode believe
  // it has an outstanding SDO request, without actually sending
  // any request, because that has already been done...;
  // it is used to implement the SDO 'broadcast' mechanism,
  // used by the Bootloader (in which a single SDO write request
  // is received and replied to by all active Bootloaders)
#ifndef NOT_USING_QT
  // Only one outstanding SDO request at a time allowed
  _sdoSemaphore->acquire();
#endif // NOT_USING_QT

  // SDO administration
  if( _sdoCsExpected != SDO_IDLE )
    {
      // Some SDO operation is in progress
      // (shouldn't occur when the semaphore is used)
      _sdoStatus = SDO_STATUS_ERR_BUSY;
      _errString = this->sdoStatusString();
      return false;
    }
  _sdoStatus   = SDO_STATUS_OKAY;
  _sdoErrClass = 0;
  _sdoErrCode  = 0;

  // Dummy: no (SDO) message is sent...

  unsigned char index_hi = (unsigned char) ((index & 0xFF00) >> 8);
  unsigned char index_lo = (unsigned char) (index & 0xFF);
  unsigned char sub      = (unsigned char) subindex;

  // SDO administration
  _sdoCsExpected = SDO_INITIATE_DOWNLOAD_RESP;
  _sdoOdIndexLo  = index_lo;
  _sdoOdIndexHi  = index_hi;
  _sdoOdSubIndex = sub;
  _sdoToggleBit  = SDO_TOGGLE_BIT;
  _sdoSize       = no_of_bytes;
  if( expedited )
    _sdoRdWrIndex= no_of_bytes; // Data is already written with the message
  else
    _sdoRdWrIndex= 0;
  _sdoTimeout    = false;

#ifndef NOT_USING_QT
  // Start this node's SDO time-out timer
  _sdoTimer->setTimeout( timeout_ms );
#endif // NOT_USING_QT

  return true;
}

/* ------------------------------------------------------------------------ */

SdoStatus CanNode::sdoReceived( CanMessage *msg_in )
{
  if( !msg_in ) return SDO_STATUS_OKAY;

  // Simply ignore (SDO) messages received when no SDO operation is active
  if( _sdoCsExpected == SDO_IDLE ) return SDO_STATUS_OKAY;

  // Is this actually a proper SDO message ?
  if( msg_in->dlc() != 8 || msg_in->cobId() != (SDOTX_OBJ | _nodeId) )
    return SDO_STATUS_OKAY;  // Ignore

#ifndef NOT_USING_QT
  QMutexLocker locker( _sdoMutex ); // Unlocks at return
#endif // NOT_USING_QT

  // Get the command specifier (cs) */
  unsigned char *pdata   = msg_in->data();
  unsigned char sdo_mode = pdata[0];
  unsigned char cs       = sdo_mode & SDO_COMMAND_SPECIFIER_MASK;
  switch( cs )
    {
    case SDO_INITIATE_DOWNLOAD_RESP:
      switch( _sdoCsExpected )
        {
        case SDO_INITIATE_DOWNLOAD_RESP:
          // SDO Write operation

          // Check OD index and subindex: if not the expected (sub)index,
          // return an error without changing _sdoCsExpected
          if( !(pdata[1] == _sdoOdIndexLo && pdata[2] == _sdoOdIndexHi &&
                pdata[3] == _sdoOdSubIndex) )
            {
              return SDO_STATUS_ERR_INDEX;
            }

          // Check if all bytes have been written
          if( _sdoRdWrIndex != _sdoSize )
            {
              // Start of a Segmented SDO Write
              _sdoStatus = this->sdoWriteNextSegment();
              if( _sdoStatus == SDO_STATUS_OKAY )
                _sdoCsExpected = SDO_DOWNLOAD_SEGMENT_RESP;
            }
          else
            {
              // Properly concluded SDO Expedited Write
              _sdoStatus = SDO_STATUS_WRITE_COMPLETE;
            }
          break;

        case SDO_INITIATE_UPLOAD_RESP:
          // Unexpected command specifier SDO_INITIATE_DOWNLOAD_RESP
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        case SDO_DOWNLOAD_SEGMENT_RESP:
        case SDO_UPLOAD_SEGMENT_RESP:
          // Segmented SDO Write or Segmented SDO Read in progress
          // so command specifier SDO_INITIATE_DOWNLOAD_RESP is unexpected:
          // send ABORT TRANSFER message
          this->sdoAbortTransfer();
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        default:
          // No SDO operation in progress, ignore
          _sdoStatus = SDO_STATUS_OKAY;
          break;
        }
      break;

    case SDO_INITIATE_UPLOAD_RESP:
      switch( _sdoCsExpected )
        {
        case SDO_INITIATE_UPLOAD_RESP:
          // SDO Read operation

          // Check OD index and subindex: if not the expected (sub)index,
          // return an error without changing _sdoCsExpected
          if( !(pdata[1] == _sdoOdIndexLo && pdata[2] == _sdoOdIndexHi &&
                pdata[3] == _sdoOdSubIndex) )
            {
              return SDO_STATUS_ERR_INDEX;
            }

          {
            int nbytes;
            if( sdo_mode & SDO_EXPEDITED )
              {
                // SDO Expedited Read
                if( sdo_mode & SDO_DATA_SIZE_INDICATED )
                  nbytes = 4 - ((sdo_mode & SDO_DATA_SIZE_MASK) >>
                                SDO_DATA_SIZE_SHIFT);
                else
                  nbytes = 4;

                for( int i=0; i<nbytes; ++i ) _sdoData[i] = pdata[4+i];
                _sdoSize = nbytes;

                // Properly concluded SDO Expedited Read
                _sdoStatus = SDO_STATUS_READ_COMPLETE;
              }
            else
              {
                // Start of a Segmented SDO Read

                // Get expected data size (in bytes), if available
                if( sdo_mode & SDO_SEGMENT_SIZE_INDICATED )
                  {
                    nbytes  = ((int) pdata[4]) << 0;
                    nbytes |= ((int) pdata[5]) << 8;
                    nbytes |= ((int) pdata[6]) << 16;
                    nbytes |= ((int) pdata[7]) << 24;
                  }
                else
                  {
                    nbytes = SDO_MAX_SIZE;
                  }

                // The size of the segment expected
                // (if equal to SDO_MAX_SIZE, assume the size was not indicated
                //  and take that into account later on)
                _sdoSize = nbytes;

                // Request the first segment
                _sdoStatus = this->sdoReadNextSegmentRequest();
                if( _sdoStatus == SDO_STATUS_OKAY )
                  _sdoCsExpected = SDO_UPLOAD_SEGMENT_RESP;
              }
          }
          break;

        case SDO_INITIATE_DOWNLOAD_RESP:
          // Unexpected command specifier SDO_INITIATE_UPLOAD_RESP
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        case SDO_DOWNLOAD_SEGMENT_RESP:
        case SDO_UPLOAD_SEGMENT_RESP:
          // Segmented SDO Write or Segmented SDO Read in progress
          // so command specifier SDO_INITIATE_UPLOAD_RESP is unexpected:
          // send ABORT TRANSFER message
          this->sdoAbortTransfer();
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        default:
          // No SDO operation in progress, ignore
          _sdoStatus = SDO_STATUS_OKAY;
          break;
        }
      break;

    case SDO_DOWNLOAD_SEGMENT_RESP:
      switch( _sdoCsExpected )
        {
        case SDO_DOWNLOAD_SEGMENT_RESP:
          // Write next segment in a Segmented SDO Write operation
          // or conclude that it has completed
          _sdoStatus = this->sdoWriteNextSegment();
          break;

        case SDO_INITIATE_DOWNLOAD_RESP:
          if( _sdoRdWrIndex != _sdoSize )
            {
              // Segmented SDO Write already in progress so command specifier
              // SDO_DOWNLOAD_SEGMENT_RESP is unexpected:
              // send ABORT TRANSFER message
              this->sdoAbortTransfer();
            }
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        case SDO_UPLOAD_SEGMENT_RESP:
          // Segmented SDO Read in progress so command specifier
          // SDO_DOWNLOAD_SEGMENT_RESP is unexpected:
          // send ABORT TRANSFER message
          this->sdoAbortTransfer();
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        case SDO_INITIATE_UPLOAD_RESP:
          // SDO Read reply expected
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        default:
          // No SDO operation in progress, ignore
          _sdoStatus = SDO_STATUS_OKAY;
          break;
        }
      break;

    case SDO_UPLOAD_SEGMENT_RESP:
      switch( _sdoCsExpected )
        {
        case SDO_UPLOAD_SEGMENT_RESP:
          // Receive next segment in a Segmented SDO Read operation
          if( (sdo_mode & SDO_TOGGLE_BIT) == _sdoToggleBit )
            {
              unsigned char nbytes_in_segment, invalid_bytes;
              bool          last_segment;
              if( sdo_mode & SDO_LAST_SEGMENT )
                last_segment = true;
              else
                last_segment = false;
              invalid_bytes = ((sdo_mode & SDO_SEGMENT_SIZE_MASK) >>
                               SDO_SEGMENT_SIZE_SHIFT);
              nbytes_in_segment = 7 - invalid_bytes;

              // More bytes than expected?
              if( _sdoRdWrIndex + nbytes_in_segment > _sdoSize )
                {
                  // Send ABORT-TRANSFER message
                  this->sdoAbortTransfer();
                  _errString = "SDO SegRead, more bytes than expected";
                  _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
                }
              else
                {
                  // Copy segment data to data array
                  for( int i=0; i<nbytes_in_segment; ++i, ++_sdoRdWrIndex )
                    _sdoData[_sdoRdWrIndex] = pdata[1+i];

                  // Is this the last segment ?
                  if( last_segment )
                    {
                      // No good, if we have less bytes than expected
                      // (unless size was not indicated, which is indicated
                      //  here by an _sdoSize of SDO_MAX_SIZE)
                      if( _sdoRdWrIndex != _sdoSize &&
                          _sdoSize != SDO_MAX_SIZE )
                        {
                          _errString = "SDO SegRead, "
                            "less bytes than expected";
                          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
                        }
                      else
                        {
                          // Properly concluded SDO Segmented Read
                          _sdoStatus = SDO_STATUS_READ_COMPLETE;
                        }
                    }
                  else
                    {
                      // Request the next segment
                      _sdoStatus = this->sdoReadNextSegmentRequest();
                      if( _sdoStatus == SDO_STATUS_OKAY )
                        _sdoCsExpected = SDO_UPLOAD_SEGMENT_RESP;
                    }
                }
            }
          else
            {
              // Wrong toggle bit !?: send ABORT-TRANSFER message
              this->sdoAbortTransfer();
              _errString = "SDO SegRead, toggle bit";
              _sdoStatus = SDO_STATUS_ERR_TOGGLE;
            }
          break;

        case SDO_DOWNLOAD_SEGMENT_RESP:
        case SDO_INITIATE_DOWNLOAD_RESP:
          if( _sdoRdWrIndex != _sdoSize )
            {
              // Segmented SDO Write in progress so command specifier
              // SDO_UPLOAD_SEGMENT_RESP is unexpected:
              // send ABORT TRANSFER message
              this->sdoAbortTransfer();
            }
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        case SDO_INITIATE_UPLOAD_RESP:
          // SDO Read reply expected
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
          break;

        default:
          // No SDO operation in progress, ignore
          _sdoStatus = SDO_STATUS_OKAY;
          break;
        }
      break;

    case SDO_ABORT_TRANSFER:
      // Received an SDO Abort Transfer message
      _sdoErrClass = pdata[7];
      _sdoErrCode  = pdata[6];
      _errString   = "SDO AbortTransfer, " +
        this->sdoErrCodeString( _sdoErrCode );
      _sdoStatus = SDO_STATUS_ERR_ABORT;
      break;

    default:
      if( _sdoCsExpected == SDO_INITIATE_UPLOAD_RESP ||
          _sdoCsExpected == SDO_UPLOAD_SEGMENT_RESP ||
          _sdoCsExpected == SDO_INITIATE_DOWNLOAD_RESP ||
          _sdoCsExpected == SDO_DOWNLOAD_SEGMENT_RESP )
        {
          if( _sdoRdWrIndex != _sdoSize )
            {
              // Segmented SDO in progress so this (unknown) command specifier
              // is unexpected: send ABORT TRANSFER message
              this->sdoAbortTransfer();
            }
          _sdoStatus = SDO_STATUS_ERR_PROTOCOL;
          _errString = this->sdoStatusString();
        }
      else
        {
          // Ignore
          _sdoStatus = SDO_STATUS_OKAY;
        }
      break;
    }

  // Properly concluded SDO read or write, so we're done,
  // or if any error occurred, we're also done...
  if( _sdoStatus != SDO_STATUS_OKAY )
    {
      _sdoCsExpected = SDO_IDLE;
#ifndef NOT_USING_QT
      _sdoTimer->cancelTimeout();
      // A next SDO operation is allowed
      if( !_sdoSemaphore->available() ) _sdoSemaphore->release();
#endif // NOT_USING_QT
    }

  return _sdoStatus;
}

/* ------------------------------------------------------------------------ */

void CanNode::sdoCancel()
{
#ifndef NOT_USING_QT
  QMutexLocker locker( _sdoMutex ); // Unlocks at return
  _sdoTimer->cancelTimeout();
#endif // NOT_USING_QT

  // Simply cancel the ongoing operation (from the processing point-of-view);
  // a still arriving reply will be ignored
  _sdoCsExpected = SDO_IDLE;
  _sdoTimeout    = false;

#ifndef NOT_USING_QT
  // A next SDO operation is allowed
  if( !_sdoSemaphore->available() ) _sdoSemaphore->release();
#endif // NOT_USING_QT
}

/* ------------------------------------------------------------------------ */

void CanNode::sdoTimedOut()
{
#ifndef NOT_USING_QT
  QMutexLocker locker( _sdoMutex ); // Unlocks at return
#endif // NOT_USING_QT

  // Check expected command-specifier as the SDO operation may have been
  // canceled just before this call by sdoCancel() above
  if( _sdoCsExpected != SDO_IDLE )
    {
      _sdoStatus     = SDO_STATUS_ERR_TIMEOUT;
      _sdoCsExpected = SDO_IDLE;
      _sdoTimeout    = true;
    }

#ifndef NOT_USING_QT
  // A next SDO operation is allowed
  if( !_sdoSemaphore->available() ) _sdoSemaphore->release();
#endif // NOT_USING_QT
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoTimeout()
{
  // NB: reading also resets the time-out status !
  //     (to prevent repeated reporting of the same occurrence)
#ifndef NOT_USING_QT
  QMutexLocker locker( _sdoMutex ); // Unlocks at return
#endif // NOT_USING_QT

  bool sdo_timeout = _sdoTimeout;

  _sdoTimeout = false;
  if( _sdoStatus == SDO_STATUS_ERR_TIMEOUT )
    _sdoStatus = SDO_STATUS_OKAY;

  return sdo_timeout;
}

/* ------------------------------------------------------------------------ */

std::string CanNode::sdoStatusString()
{
  switch( _sdoStatus )
    {
    case SDO_STATUS_OKAY:
      return std::string( "SDO_STAT_OKAY" );
    case SDO_STATUS_WRITE_COMPLETE:
      return std::string( "SDO_STAT_WRITE_COMPLETE" );
    case SDO_STATUS_READ_COMPLETE:
      return std::string( "SDO_STAT_READ_COMPLETE" );
    case SDO_STATUS_ERR_ABORT:
      return std::string( "SDO_STAT_ERR_ABORT" );
    case SDO_STATUS_ERR_INDEX:
      return std::string( "SDO_STAT_ERR_INDEX" );
    case SDO_STATUS_ERR_SIZE:
      return std::string( "SDO_STAT_ERR_SIZE" );
    case SDO_STATUS_ERR_BUSY:
      return std::string( "SDO_STAT_ERR_BUSY" );
    case SDO_STATUS_ERR_PROTOCOL:
      return std::string( "SDO_STAT_ERR_PROTOCOL" );
    case SDO_STATUS_ERR_TIMEOUT:
      return std::string( "SDO_STAT_ERR_TIMEOUT" );
    case SDO_STATUS_ERR_SEND:
      return std::string( "SDO_STAT_ERR_SEND" );
    default:
      return std::string( "????" );
      break;
    }
}

/* ------------------------------------------------------------------------ */

SdoStatus CanNode::sdoStatus()
{
  return _sdoStatus;
}

/* ------------------------------------------------------------------------ */

int CanNode::sdoIndex()
{
  return( (((int) _sdoOdIndexHi) << 8 | (int) _sdoOdIndexLo) );
}

/* ------------------------------------------------------------------------ */

int CanNode::sdoSubIndex()
{
  return( (int) _sdoOdSubIndex );
}

/* ------------------------------------------------------------------------ */

int CanNode::sdoSize()
{
  return _sdoSize;
}

/* ------------------------------------------------------------------------ */

int CanNode::sdoMaxSize()
{
  return SDO_MAX_SIZE;
}

/* ------------------------------------------------------------------------ */

unsigned char *CanNode::sdoData()
{
  return _sdoData;
}

/* ------------------------------------------------------------------------ */

bool CanNode::sdoAbortTransfer()
{
  // Send an SDO Abort Transfer message
  unsigned char can_data[8];
  CanMessage    msg_out( SDORX_OBJ | _nodeId, false, 0, 0, 0 );

  // Put bytes in message
  can_data[0] = SDO_ABORT_TRANSFER;
  for( int i=1; i<8; ++i ) can_data[i] = 0;
  msg_out.setData( 8, can_data );

  // Send message...
  if( _canIntf->send( msg_out ) == false )
    {
      _errString = _canIntf->errString();
      return false;
    }
  return true;
}

/* ------------------------------------------------------------------------ */

SdoStatus CanNode::sdoWriteNextSegment()
{
  // Send the next segment (of up to 7 bytes) in
  // an SDO Segmented Write operation
  SdoStatus     stat = SDO_STATUS_OKAY;
  unsigned char nbytes, invalid_bytes;
  unsigned char can_data[8];
  CanMessage    msg_out( SDORX_OBJ | _nodeId, false, 0, 0, 0 );

  if( _sdoRdWrIndex == _sdoSize )
    // Properly concluded SDO Segmented Write
    return SDO_STATUS_WRITE_COMPLETE;

  // Determine the number of bytes to send in this segment
  if( _sdoSize - _sdoRdWrIndex > 6 )
    // As many as fit in a Segmented SDO message
    nbytes = 7;
  else
    // The last bytes
    nbytes = _sdoSize - _sdoRdWrIndex;

  invalid_bytes = 7-nbytes;

  // Toggle the toggle bit
  _sdoToggleBit ^= SDO_TOGGLE_BIT;

  // The command specifier
  can_data[0] = (SDO_DOWNLOAD_SEGMENT_REQ |
                 (invalid_bytes << SDO_SEGMENT_SIZE_SHIFT) | _sdoToggleBit);

  // Add (up to 7) data bytes
  for( int i=1; i<=nbytes; ++i, ++_sdoRdWrIndex )
    can_data[i] = _sdoData[_sdoRdWrIndex];

  // Is this the last segment ?
  if( _sdoRdWrIndex == _sdoSize )
    {
      // Indicate last segment in the command specifier
      can_data[0] |= SDO_LAST_SEGMENT;

      // Set non-significant bytes to zero
      for( int i=nbytes+1; i<8; ++i ) can_data[i] = 0x00;
    }

  // Put bytes in message
  msg_out.setData( 8, can_data );

  // Send this segment...
  if( _canIntf->send( msg_out ) == false )
    {
      _errString = _canIntf->errString();
      stat = SDO_STATUS_ERR_SEND;
    }

  return stat;
}

/* ------------------------------------------------------------------------ */

SdoStatus CanNode::sdoReadNextSegmentRequest()
{
  // Request the next segment in an SDO Segmented Read operation
  SdoStatus     stat = SDO_STATUS_OKAY;
  unsigned char can_data[8];
  CanMessage    msg_out( SDORX_OBJ | _nodeId, false, 8, can_data, 0 );

  // Toggle the toggle bit
  _sdoToggleBit ^= SDO_TOGGLE_BIT;

  can_data[0] = SDO_UPLOAD_SEGMENT_REQ | _sdoToggleBit;
  for( int i=1; i<8; ++i ) can_data[i] = 0x00;

  // Put bytes in message
  msg_out.setData( 8, can_data );

  // Request next segment...
  if( _canIntf->send( msg_out ) == false )
    {
      _errString = _canIntf->errString();
      stat = SDO_STATUS_ERR_SEND;
    }

  return stat;
}

/* ------------------------------------------------------------------------ */

bool CanNode::cobRead( int            cob_id,
                       int           *no_of_bytes,
                       unsigned char  data[8] )
{
  bool result = false;

  CanMessage *pmsg = _canIntf->receiveSpecific( cob_id | _nodeId );
  if( pmsg )
    {
      // There is a message from this CanNode with the requested COB-ID
      *no_of_bytes = pmsg->dlc();
      unsigned char *pdata = pmsg->data();
      for( int i=0; i<*no_of_bytes; ++i, ++pdata ) data[i] = *pdata;
      _canIntf->msgHandled( pmsg );
      result = true;
    }
  else
    {
      _errString = _canIntf->errString();
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::cobRead( int            cob_id,
                       int           *no_of_bytes,
                       unsigned char  data[8],
                       unsigned int   timeout_ms )
{
  bool result = false;

  CanMessage *pmsg = _canIntf->receiveSpecific( cob_id | _nodeId, timeout_ms );
  if( pmsg )
    {
      // There is a message from this CanNode with the requested COB-ID
      *no_of_bytes = pmsg->dlc();
      unsigned char *pdata = pmsg->data();
      for( int i=0; i<*no_of_bytes; ++i, ++pdata ) data[i] = *pdata;
      _canIntf->msgHandled( pmsg );
      result = true;
    }
  else
    {
      _errString = _canIntf->errString();
    }
  return result;
}

/* ------------------------------------------------------------------------ */

bool CanNode::getSerialNumber( int *serialno )
{
  int no_of_bytes;
  unsigned int timeout_ms = 100;
  //*serialno = 0xFFFFFFFF;
  *serialno = 0;
  return( this->sdoReadExpedited( OD_SERIAL_NO, 0,
                                  &no_of_bytes, serialno, timeout_ms ) );
}

/* ------------------------------------------------------------------------ */

bool CanNode::getFirmwareVersion( char *version )
{
  int no_of_bytes, data;
  unsigned int timeout_ms = 100;
  int i;

  version[0] = '\0';

  // Read the version number
  if( this->sdoReadExpedited( OD_SW_VERSION, 0,
                              &no_of_bytes, &data, timeout_ms ) )
    {
      for( i=0; i<4; ++i )
        {
          version[i] = (char) (data & 0xFF);
          data >>= 8;
        }

      // Read the minor version number, if any
      if( this->sdoReadExpedited( OD_SW_VERSION, 1,
                                  &no_of_bytes, &data, timeout_ms ) )
        {
          version[i++] = '.';
          for( ;i<9; ++i )
            {
              version[i] = (char) (data & 0xFF);
              data >>= 8;
            }
        }
      version[i] ='\0';
      return true;
    }
  return false;
}

/* ------------------------------------------------------------------------ */

bool CanNode::getFirmwareVersion( std::string &version_str )
{
  char version[32];
  if( this->getFirmwareVersion( version ) )
    {
      version_str = std::string( version );
      return true;
    }
  version_str = std::string( "" );
  return false;
}

/* ------------------------------------------------------------------------ */

bool CanNode::saveConfig( unsigned int timeout_ms )
{
  int data = ((((int) 's')<<0) | (((int) 'a')<<8) |
              (((int) 'v')<<16) | (((int) 'e')<<24));

  if( this->sdoWriteExpedited( OD_STORE_PARAMETERS, OD_STORE_ALL,
                               4, data, timeout_ms ) )
    return true;
  else
    return false;
}

/* ------------------------------------------------------------------------ */

bool CanNode::eraseConfig( unsigned int timeout_ms )
{
  int data = ((((int) 'l')<<0) | (((int) 'o')<<8) |
              (((int) 'a')<<16) | (((int) 'd')<<24));

  if( this->sdoWriteExpedited( OD_DFLT_PARAMETERS, OD_STORE_ALL,
                               4, data, timeout_ms ) )
    return true;
  else
    return false;
}

/* ------------------------------------------------------------------------ */

std::string CanNode::errString()
{
  std::string errstr = _errString;
  _errString = ""; // Clear the error string...
  return errstr;
}

/* ------------------------------------------------------------------------ */

std::string CanNode::sdoErrCodeString( int code )
{
  switch( code )
    {
    case SDO_ECODE_PAR_INCONSISTENT:
      return std::string( "ErrCode: parameter inconsistent" );
    case SDO_ECODE_PAR_ILLEGAL:
      return std::string( "ErrCode: parameter illegal" );
    case SDO_ECODE_ACCESS:
      return std::string( "ErrCode: access" );
    case SDO_ECODE_NONEXISTENT:
      return std::string( "ErrCode: non-existent" );
    case SDO_ECODE_HARDWARE:
      return std::string( "ErrCode: hardware" );
    case SDO_ECODE_TYPE_CONFLICT:
      return std::string( "ErrCode: type conflict" );
    case SDO_ECODE_ATTRIBUTE:
      return std::string( "ErrCode: attribute" );
    case SDO_ECODE_OKAY:
      return std::string( "ErrCode: OK" );
    default:
      return std::string( "ErrCode: ???" );
    }
}

/* ------------------------------------------------------------------------ */
