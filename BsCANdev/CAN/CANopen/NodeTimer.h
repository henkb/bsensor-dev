#ifndef NODETIMER_H
#define NODETIMER_H

#include <QMutex>
#include <QThread>
#include <QSemaphore>
#include <QWaitCondition>

class CanNode;

class NodeTimer: public QThread
{
  Q_OBJECT

  public:
    NodeTimer( CanNode *node, int timer_id, QObject *parent = 0 );
    ~NodeTimer();

    void stop         ();
    void run          ();
    void setTimeout   ( int ms );
    void cancelTimeout();
    bool timeout      () { return _timeout; }

  private:
    CanNode       *_canNode;
    int            _timerId;
    bool           _stop;
    bool           _timeout;
    int            _timeoutMs;

    QMutex         _mutex;
    QWaitCondition _condition;
    QSemaphore     _sem1, _sem2;
};

#endif // NODETIMER_H
