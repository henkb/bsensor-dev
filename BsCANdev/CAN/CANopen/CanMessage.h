/* ------------------------------------------------------------------------
File   : CanMessage.h

Descr  : CAN message class with CANopen features.

History: --JUL.01; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef CANMESSAGE_H
#define CANMESSAGE_H

#include <string>

#ifdef WIN32
 // Differentiate between building the DLL or using it
 #ifdef MY_LIB_EXPORT
 #define MY_LIB __declspec(dllexport)
 #else
 #define MY_LIB __declspec(dllimport)
 #endif
#else
 #define MY_LIB
#endif // WIN32

/* ------------------------------------------------------------------------ */

class MY_LIB CanMessage
{
 public:
  CanMessage();

  CanMessage( CanMessage &msg );

  CanMessage( int           can_identifier,
	      bool          remote,
	      int           dlc,
	      unsigned char *data,
	      unsigned long time );

  ~CanMessage();

  int            cobId    ();
  void           setCobId ( int cobid );
  int            nodeId   ();
  void           setNodeId( int nodeid );
  int            object   ();
  void           setObject( int object );
  int            dlc      ();
  void           setDlc   ( int dlc );
  unsigned char *data     ();
  void           setData  ( int dlc, unsigned char *data );
  unsigned long  timeStamp();
  void           setTimeStamp( unsigned long timestamp );

  bool           isErrFrame();
  bool           isRemoteFrame();

  int            sdoCmdSpecifier();

  std::string    description( int mode = 0,
			      bool show_time = true,
			      bool show_time_raw = false );

 private:
  bool          _isErrFrame;
  bool          _isRtrFrame;
  int           _cobId;
  int           _dlc;
  unsigned char _data[8];
  unsigned long _timeStamp;
};

/* ------------------------------------------------------------------------ */
#endif // CANMESSAGE_H
