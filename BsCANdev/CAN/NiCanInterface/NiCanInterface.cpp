/* ------------------------------------------------------------------------
File   : NiCanInterface.cpp

Descr  : National Instruments CAN-interface class implementation.

History: --JUL.01; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#include <sstream>
#include <iomanip>

#include "CANopen.h"
#include "NiCanInterface.h"
#include "CanMessage.h"
using namespace std;

const int    NiCanInterface::CAN_INTF_DFLT    = 0;
const int    NiCanInterface::CAN_BITRATE_DFLT = 125;
const string NiCanInterface::MANUFACTURER_STR = string("NICAN");

/* ------------------------------------------------------------------------ */

NiCanInterface::NiCanInterface( int  port_nr,
				int  kbitrate,
				bool open_now )
  : CanInterface( MANUFACTURER_STR, NICAN_INTF_TYPE, port_nr, kbitrate )
{
  if( !this->setBusSpeed( kbitrate ) ) _kbitRate = 125;

  if( open_now ) this->open();
}

/* ------------------------------------------------------------------------ */

NiCanInterface::~NiCanInterface()
{
  this->close();
}

/* ------------------------------------------------------------------------ */

vector<int> NiCanInterface::ports()
{
  // Create a list of available port numbers
  vector<int> portnumbers;
  int total_ports = 0;

  NCTYPE_STATUS status;
  // Get the number of cards in the system
  NCTYPE_UINT32 ncards;
  status = ncGetHardwareInfo( 1, 1, NC_ATTR_NUM_CARDS, 4, &ncards );
  if( status == NC_SUCCESS )
    {
      // Get the number of ports on each card (start card number from 1!)
      NCTYPE_UINT32 card_no, nports;
      for( card_no=1; card_no<=ncards; ++card_no )
	{
	  status = ncGetHardwareInfo( card_no, 1,
				      NC_ATTR_NUM_PORTS, 4, &nports );
	  if( status == NC_SUCCESS )
	    {
	      for( NCTYPE_UINT32 i=0; i<nports; ++i )
		{
		  portnumbers.push_back( total_ports );
		  ++total_ports;
		}
	    }
	}
    }

  return portnumbers;
}

/* ------------------------------------------------------------------------ */

bool NiCanInterface::setBusSpeed( int kbitrate )
{
  // Change of bus speed only allowed when the port is not open
  if( this->isOpen() ) return false;

  // Only certain baudrates are allowed
  if( !(kbitrate == 1000 || kbitrate == 800 ||
	kbitrate == 500  || kbitrate == 250 ||
	kbitrate == 125  || kbitrate == 50 ||
	kbitrate == 20   || kbitrate == 10) )
    return false;

  _kbitRate = kbitrate;
  return true;
}

/* ------------------------------------------------------------------------ */

bool NiCanInterface::open()
{
  NCTYPE_STATUS status;
  NCTYPE_UINT32 no_attr = 9;
  NCTYPE_ATTRID attr_id[9];
  NCTYPE_UINT32 attr_val[9];

  // Configure the CAN-interface object
  attr_id[0] = NC_ATTR_BAUD_RATE;     attr_val[0] = _kbitRate*1000;
  attr_id[1] = NC_ATTR_START_ON_OPEN; attr_val[1] = NC_TRUE;
  attr_id[2] = NC_ATTR_READ_Q_LEN;    attr_val[2] = 100;
  attr_id[3] = NC_ATTR_WRITE_Q_LEN;   attr_val[3] = 20;
  attr_id[4] = NC_ATTR_TIMESTAMPING;  attr_val[4] = NC_FALSE;
  attr_id[5] = NC_ATTR_CAN_COMP_STD;  attr_val[5] = 0;
  attr_id[6] = NC_ATTR_CAN_MASK_STD;  attr_val[6] = 0;
  attr_id[7] = NC_ATTR_CAN_COMP_XTD;  attr_val[7] = NC_CAN_ARBID_NONE;
  attr_id[8] = NC_ATTR_CAN_MASK_XTD;  attr_val[8] = 0;

  ostringstream oss;
  oss << "CAN" << _portNumber;
  string intf_name = oss.str();
  status = ncConfig( (char *) intf_name.c_str(), no_attr, attr_id, attr_val );
  if( status != NC_SUCCESS )
    {
      _opStatus = status;
      _initResultString = this->errString( "ncConfig" );
      return false;
    }

  // Open the CAN-interface object
  status = ncOpenObject( (char *) intf_name.c_str(), &_handle );
  if( status != NC_SUCCESS )
    {
      _opStatus = status;
      _initResultString = this->errString( "ncOpenObject" );
      return false;
    }

  status = ncAction( _handle, NC_OP_START, 0 );
  if( status != NC_SUCCESS )
    {
      _opStatus = status;
      _initResultString = this->errString( "ncAction" );
      return false;
    }

  _isOpen = true;
  return true;
}

/* ------------------------------------------------------------------------ */

bool NiCanInterface::close()
{
  // Close the CAN-interface object
  NCTYPE_STATUS status;

  // If necessary wait for the last messages to be sent
  ncWaitForState( _handle, NC_ST_WRITE_SUCCESS, 100, 0 );

  // Flush hardware and software buffers
  this->flush();

  _opStatus = 0;
  _isOpen = false;

  status = ncAction( _handle, NC_OP_STOP, 0 );
  if( status != NC_SUCCESS ) _opStatus = status;

  status = ncCloseObject( _handle );
  if( status != NC_SUCCESS )
    {
      _opStatus = status;
      return false;
    }

  return true;
}

/* ------------------------------------------------------------------------ */

bool NiCanInterface::send( CanMessage &can_msg )
{
  NCTYPE_STATUS    status;
  NCTYPE_STATE     state;
  NCTYPE_CAN_FRAME frame;
  unsigned char    *pch;

  _opStatus = 0;

  frame.IsRemote      = can_msg.isRemoteFrame();
  frame.ArbitrationId = can_msg.cobId();
  frame.DataLength    = can_msg.dlc();
  pch                 = can_msg.data();
  for( int i=0; i<frame.DataLength; ++i ) frame.Data[i] = pch[i];

  bool result = true;
  status = ncWrite( _handle, sizeof(NCTYPE_CAN_FRAME), &frame );
  if( status != NC_SUCCESS )
    {
      if( status == CanErrOverflowWrite )
	{
	  // If interface output buffer overflow, wait until (some) messages
	  // have been sent and try to resend the message
	  status = ncWaitForState( _handle, NC_ST_WRITE_SUCCESS, 100, &state );
	  //if( status == NC_SUCCESS )
	  status = ncWrite( _handle, sizeof(NCTYPE_CAN_FRAME), &frame );
	  if( status != NC_SUCCESS )
	    {
	      _opStatus = status;
	      result = false;
	    }
	}
      else
	{
	  _opStatus = status;
	  result = false;
	}
    }
  return result;
}

/* ------------------------------------------------------------------------ */

CanMessage* NiCanInterface::receive( unsigned int timeout_ms )
{
  // Read the next available message;
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  if( !_canMsgs.empty() )
    {
      // Get the first message from the list
      pMsg = *(_canMsgs.begin());

      // Remove the message pointer from the list
      _canMsgs.pop_front();

      return pMsg;
    }

  if( timeout_ms != 0 )
    {
      _opStatus = 0;

      NCTYPE_STATUS status;
      NCTYPE_STATE  state;

      status = ncWaitForState( _handle, NC_ST_READ_AVAIL,
			       timeout_ms, &state );
      if( status == NC_SUCCESS )
	{
	  // Read all available message(s) from the interface
	  // (This should be done in a separate thread?)
	  this->bufferReceivedMsgs();

	  if( !_canMsgs.empty() )
	    {
	      // Get the first message from the list
	      pMsg = *(_canMsgs.begin());

	      // Remove the message pointer from the list
	      _canMsgs.pop_front();
	    }
	}
      else
	{
	  // Report problems, including the timeout
	  //if( status != CanErrFunctionTimeout )
	  _opStatus = status;
	}
    }

  return pMsg;
}

/* ------------------------------------------------------------------------ */

CanMessage* NiCanInterface::receiveSpecific( int          cob_id,
					     unsigned int timeout_ms )
{
  // Read the next available message having COB-ID 'cob_id';
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  // Is this message perhaps already available in the currently held list ?
  pMsg = this->getMsg( cob_id );

  // Message found ?
  if( pMsg ) return pMsg;

  if( timeout_ms > 0 )
    {
      _opStatus = 0;

      while( !pMsg )
	{
	  NCTYPE_STATUS status;
	  NCTYPE_STATE  state;

	  status = ncWaitForState( _handle, NC_ST_READ_AVAIL,
				   timeout_ms, &state );
	  if( status == NC_SUCCESS )
	    {
	      // Read all available message(s) from the interface
	      // (This should be done in a separate thread?)
	      this->bufferReceivedMsgs();

	      // Get the required message, if available
	      pMsg = this->getMsg( cob_id );
	    }
	  else
	    {
	      // Report problems, including the timeout
	      //if( status != CanErrFunctionTimeout )
	      _opStatus = status;
	      // Interrupt the while-loop
	      break;
	    }
	}
    }

  return pMsg;
}

/* ------------------------------------------------------------------------ */

void NiCanInterface::bufferReceivedMsgs()
{
  // Get all available CAN messages from the interface
  // and store them in the list
  NCTYPE_CAN_STRUCT frame;
  NCTYPE_STATUS     status;
  CanMessage       *pMsg = 0;

  _opStatus = 0;
  _msgErrFlags = 0;

  while( (status = ncRead( _handle, sizeof(NCTYPE_CAN_STRUCT),
			   &frame )) == NC_SUCCESS )
    {
      bool rtr = false;
      // The frame's Timestamp contains a Windows FILETIME struct,
      // containing a 64-bit value representing the number of 100-nanosecond
      // intervals since January 1, 1601 (UTC).
      // Just use whatever fits in 32 bits, and downscaled to milliseconds.
      unsigned long time_in_ms;
      time_in_ms = ((frame.Timestamp.LowPart/10000) *
		    (frame.Timestamp.HighPart % 10000));
      switch( frame.FrameType )
	{
	case NC_FRMTYPE_REMOTE:
	  rtr = true;
	case NC_FRMTYPE_DATA:
	  pMsg = new CanMessage( frame.ArbitrationId, rtr,
				 frame.DataLength, frame.Data,
				 time_in_ms );
	  break;
	case NC_FRMTYPE_COMM_ERR:
	  _msgErrFlags = frame.ArbitrationId;
	  break;
	case NC_FRMTYPE_BUS_ERR:
	  pMsg = new CanMessage();
	  break;
	case NC_FRMTYPE_TRANSCEIVER_ERR:
	case NC_FRMTYPE_RTSI:
	case NC_FRMTYPE_TRIG_START:
	default:
	  break;
	}

      if( pMsg )
	// Append (a pointer to) the message to the list
	_canMsgs.push_back( pMsg );
    }

  // Report problems (but 'old data' is not a problem..)
  if( status != CanWarnOldData ) _opStatus = status;
}

/* ------------------------------------------------------------------------ */

bool NiCanInterface::awaitTransmission( unsigned int timeout_ms )
{
  _opStatus = 0;

  NCTYPE_STATUS status;
  NCTYPE_STATE  state;
  status = ncWaitForState( _handle, NC_ST_WRITE_SUCCESS, timeout_ms, &state );
  if( status != NC_SUCCESS )
    {
      _opStatus = status;
      return false;
    }
  return true;
}

/* ------------------------------------------------------------------------ */

void NiCanInterface::flush()
{
  _opStatus = 0;

  // Flush receive and transmit buffers
  NCTYPE_STATUS status;
  status = ncAction( _handle, NC_OP_RESET, 0 );
  if( status != NC_SUCCESS ) _opStatus = status;

  // Clear the received messages list:
  // delete the CAN messages, then clear the list
  list<CanMessage *>::iterator it;
  for( it=_canMsgs.begin(); it!=_canMsgs.end(); ) delete (*it);
  _canMsgs.clear();

  status = ncAction( _handle, NC_OP_START, 0 );
  if( status != NC_SUCCESS ) _opStatus = status;
}

/* ------------------------------------------------------------------------ */

unsigned long NiCanInterface::state()
{
  // Return state of the interface
  NCTYPE_STATUS status;
  NCTYPE_STATE  state;

  _opStatus = 0;

  status = ncGetAttribute( _handle, NC_ATTR_STATE,
			   sizeof(NCTYPE_STATE), &state );
  if( status != NC_SUCCESS ) _opStatus = status;

  return state;
}

/* ------------------------------------------------------------------------ */

string NiCanInterface::stateString( unsigned long state )
{
  // Return a string describing the state
  string str;
  unsigned long known_mask = (NC_ST_READ_AVAIL |
			      NC_ST_READ_MULT |
			      NC_ST_REMOTE_WAKEUP |
			      NC_ST_WRITE_MULT |
			      NC_ST_WRITE_SUCCESS);

  // Check for known state bits
  if( state & NC_ST_READ_AVAIL )    str += "READ_AVAIL ";
  if( state & NC_ST_READ_MULT )     str += "READ_MULT ";
  if( state & NC_ST_REMOTE_WAKEUP ) str += "REMOTE_WAKEUP ";
  if( state & NC_ST_WRITE_MULT )    str += "WRITE_MULT ";
  if( state & NC_ST_WRITE_SUCCESS ) str += "WRITE_SUCCESS ";

  // Any of the unknown bits set ?
  if( state & (~known_mask) )
    { 
      str += "???? ";
      ostringstream oss;
      oss << " (" << hex << setw(8) << setfill('0')
	  << (state & (~known_mask)) << ")";
      str += oss.str();
    }

  return str;
}

/* ------------------------------------------------------------------------ */

string NiCanInterface::errString()
{
  char tmp[300] = "No error";
  ncStatusToString( _opStatus, sizeof(tmp), tmp );
  return( string( tmp ) );
}

/* ------------------------------------------------------------------------ */

string NiCanInterface::errString( const char *context )
{
  // Returns a string describing the result of the last CAN interface operation
  // preceeded by a 'context' string
  return( string( context ) + ": " + this->errString() );
}

/* ------------------------------------------------------------------------ */

#ifdef WIN32
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

extern "C" MY_EXPORT CanInterface *newNiCanInterface( int  port_nr,
						      int  kbitrate,
						      bool open_now )
{
  return new NiCanInterface( port_nr, kbitrate, open_now );
}

/* ------------------------------------------------------------------------ */

extern "C" MY_EXPORT int niCanInterfacePorts( int *port_numbers )
{
  vector<int> ports = NiCanInterface::ports();
  for( unsigned int i=0; i<ports.size(); ++i ) port_numbers[i] = ports[i];
  return ports.size();
}

/* ------------------------------------------------------------------------ */
