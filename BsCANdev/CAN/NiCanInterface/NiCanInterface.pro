#
# Project file for the NiCanInterface library
# (Windows only)
#
# To generate a Visual Studio project:
#   qmake -t vclib NiCanInterface.pro
# To generate a Makefile:
#   qmake NiCanInterface.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = NiCanInterface

# Create a shared library
CONFIG += shared qt thread warn_on exceptions debug_and_release

QT -= gui
QT += core

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

win32 {
  INCLUDEPATH += "C:/Program Files/National Instruments/NI-CAN/MS Visual C"
  INCLUDEPATH += "C:/Program Files (x86)/National Instruments/NI-CAN/MS Visual C"
  # Need to add the following lib when creating the shared lib
  LIBS += "-LC:/Program Files/National Instruments/NI-CAN/MS Visual C"
  LIBS += "-LC:/Program Files (x86)/National Instruments/NI-CAN/MS Visual C"
  LIBS += -lnicanmsc
}
LIBS += -lCANopen

INCLUDEPATH += ../CANopen

SOURCES += NiCanInterface.cpp
HEADERS += NiCanInterface.h
HEADERS += ../CANopen/CanInterface.h
HEADERS += ../CANopen/CanMessage.h
HEADERS += ../CANopen/CANopen.h

