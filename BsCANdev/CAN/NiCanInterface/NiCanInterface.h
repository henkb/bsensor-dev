/* ------------------------------------------------------------------------
File   : NiCanInterface.h

Descr  : National Instruments CAN-interface class.

History: --JUL.01; Henk B&B; First definition.
--------------------------------------------------------------------------- */

#ifndef NICANINTERFACE_H
#define NICANINTERFACE_H

#include "nican.h"

#define NC_SUCCESS  CanSuccess

#include "CanInterface.h"

/* ------------------------------------------------------------------------ */

class NiCanInterface: public CanInterface
{
 public:
  NiCanInterface( int port_no   = CAN_INTF_DFLT,
		  int bitrate   = CAN_BITRATE_DFLT,
		  bool open_now = true );

  ~NiCanInterface();

  static std::vector<int> ports();

  bool          setBusSpeed( int kbitrate );

  bool          open();

  bool          close();

  bool          send( CanMessage &CanMsg );

  CanMessage*   receive( unsigned int timeout_ms );

  CanMessage*   receiveSpecific( int cob_id, unsigned int timeout_ms = 0 );

  bool          awaitTransmission( unsigned int timeout_ms );

  void          flush();

  unsigned long state();
  std::string   stateString( unsigned long state );

  std::string   errString();
  std::string   errString( const char *context );

 private:
  void          bufferReceivedMsgs();

  // Handle to the actual device
  NCTYPE_OBJH _handle;

  static const int         CAN_INTF_DFLT;
  static const int         CAN_BITRATE_DFLT;
  static const std::string MANUFACTURER_STR;
};

/* ------------------------------------------------------------------------ */
#endif // NICANINTERFACE_H
