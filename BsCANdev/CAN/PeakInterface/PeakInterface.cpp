/* ------------------------------------------------------------------------
File   : PeakInterface.cpp

Descr  : PEAK CAN-interface class implementation.

History: 05NOV.12; Henk B&B; Created.
--------------------------------------------------------------------------- */

#ifdef WIN32
#include <Windows.h>
#endif

#include <algorithm>
#include <sstream>
#include <iomanip>
using namespace std;

#include "CANopen.h"
#include "PeakInterface.h"
#include "CanMessage.h"

const int    PeakInterface::CAN_INTF_DFLT    = 0;
const int    PeakInterface::CAN_BITRATE_DFLT = 125;
const string PeakInterface::MANUFACTURER_STR = string("PEAK");

/* Indices for bit rates */
#define KB1000  0
#define KB800   1
#define KB500   2
#define KB250   3
#define KB125   4
#define KB50    5
#define KB20    6
#define KB10    7

const unsigned long BAUD_REG[] = { PCAN_BAUD_1M,
				   PCAN_BAUD_800K,
				   PCAN_BAUD_500K,
				   PCAN_BAUD_250K,
				   PCAN_BAUD_125K,
				   PCAN_BAUD_50K,
				   PCAN_BAUD_20K,
				   PCAN_BAUD_10K };

// PEAK driver can handle up to 8 PCAN-USB interfaces
// (which is the only type we will support for the time being..)
const TPCANHandle PEAK_USBCAN_HANDLE[] = { PCAN_USBBUS1,
					   PCAN_USBBUS2,
					   PCAN_USBBUS3,
					   PCAN_USBBUS4,
					   PCAN_USBBUS5,
					   PCAN_USBBUS6,
					   PCAN_USBBUS7,
					   PCAN_USBBUS8 };

// Note that it is possible to open PEAK CAN ports on the basis of
// their 'PCAN_DEVICE_NUMBER', a number stored onboard,
// allowing us to open an interface independent of its
// position or order of plug-in, but based on the stored number.
// The port number could be any number between 0 and 255.
// See below in c'tor and ports() for the implementation.
// Uncomment the following line to use this type of CAN port numbering here.
//#define USE_ONBOARD_DEVICE_NUMBER

/* ------------------------------------------------------------------------ */

PeakInterface::PeakInterface( int  port_nr,
			      int  kbitrate,
			      bool open_now )
  : CanInterface( MANUFACTURER_STR, PEAK_INTF_TYPE, port_nr, kbitrate )
{
#ifndef USE_ONBOARD_DEVICE_NUMER
  // Check for a valid port number
  if( port_nr < 0 || port_nr > 7 )
    {
      _opStatus |= PCAN_ERROR_ILLHW;

      _initResultString = this->errString( "CAN port number" );
      return;
    }
  _portIndex = port_nr;

#else

  // Alternatively, use the onboard 'device number' as 'port number'
  _portIndex = -1;
  int val;
  for( int index=0; index<8; ++index )
    {
      if( CAN_GetValue( PEAK_USBCAN_HANDLE[index],
			PCAN_CHANNEL_CONDITION,
			(void *) &val, sizeof(int) ) == PCAN_ERROR_OK )
	{
	  if( val == PCAN_CHANNEL_AVAILABLE )
	    {
	      // Unfortunately we need to initialize before we can read
	      // this parameter... which is silly of course!
	      // Therefor set it to listen-only to prevent interference
	      // NB: can't get device number from 'occupied' channels
	      int on = PCAN_PARAMETER_ON;
	      CAN_SetValue( PEAK_USBCAN_HANDLE[index],
			    PCAN_LISTEN_ONLY, &on, sizeof(int) );
	      CAN_Initialize( PEAK_USBCAN_HANDLE[index], PCAN_BAUD_125K );
	      if( CAN_GetValue( PEAK_USBCAN_HANDLE[index],
				PCAN_DEVICE_NUMBER,
				(void *) &val, sizeof(int) ) == PCAN_ERROR_OK )
		// Is 'val' equal to the requested port number ?
		if( val == port_nr )
		  _portIndex = index; // Yes, so select it
	      CAN_Uninitialize( PEAK_USBCAN_HANDLE[index] );
	      if( _portIndex > -1 )
		break; // Port has been selected, break out of for-loop
	    }
	}
    }
  if( _portIndex < 0 )
    {
      _opStatus |= PCAN_ERROR_ILLHW;

      _initResultString = this->errString( "CAN port number" );
      return;
    }
#endif // USE_ONBOARD_DEVICE_NUMBER

  if( !this->setBusSpeed( kbitrate ) )
    {
      _kbitRate = 125;
      _kbIndex  = KB125;
    }

  TPCANStatus stat;
  if( open_now )
    {
      // Initialize the CAN port
      stat = CAN_Initialize( PEAK_USBCAN_HANDLE[_portIndex],
			     BAUD_REG[_kbIndex] );
      if( stat != PCAN_ERROR_OK )
	{
	  _opStatus = stat;
	  _initResultString = this->errString( "CAN_Initialize" );
	}
      else
	{
	  // Set port to auto-recover from a bus-off state
	  int on = PCAN_PARAMETER_ON;
	  CAN_SetValue( PEAK_USBCAN_HANDLE[_portIndex],
			PCAN_BUSOFF_AUTORESET, &on, sizeof(int) );
	  _isOpen = true;
	}
    }
}

/* ------------------------------------------------------------------------ */

PeakInterface::~PeakInterface()
{
  this->close();
}

/* ------------------------------------------------------------------------ */

vector<int> PeakInterface::ports()
{
  // Create a list of available port numbers
  vector<int> portnumbers;

  int val;
  for( int index=0; index<8; ++index )
    {
      if( CAN_GetValue( PEAK_USBCAN_HANDLE[index],
			PCAN_CHANNEL_CONDITION,
			(void *) &val, sizeof(int) ) == PCAN_ERROR_OK )
	{
	  if( val == PCAN_CHANNEL_AVAILABLE ||
	      val == PCAN_CHANNEL_OCCUPIED )
	    {
#ifndef USE_ONBOARD_DEVICE_NUMBER
	      // Port exists, so add it to our list
	      portnumbers.push_back( index );
#else
	      // Alternatively, get and use its 'device number' as 'port';
	      // Unfortunately we need to initialize before we can read
	      // this parameter... which is silly of course!
	      // Therefor set it to listen-only to prevent interference
	      // NB: can't get device number from 'occupied' channels
	      //     so channels in-use will not show up in our list
	      int on = PCAN_PARAMETER_ON;
	      CAN_SetValue( PEAK_USBCAN_HANDLE[index],
			    PCAN_LISTEN_ONLY, &on, sizeof(int) );
	      CAN_Initialize( PEAK_USBCAN_HANDLE[index], PCAN_BAUD_125K );
	      if( CAN_GetValue( PEAK_USBCAN_HANDLE[index],
				PCAN_DEVICE_NUMBER,
				(void *) &val, sizeof(int) ) == PCAN_ERROR_OK )
		 portnumbers.push_back( val );
	      CAN_Uninitialize( PEAK_USBCAN_HANDLE[index] );
#endif // USE_ONBOARD_DEVICE_NUMBER
	    }
	}
    }

  return portnumbers;
}

/* ------------------------------------------------------------------------ */

bool PeakInterface::setBusSpeed( int kbitrate )
{
  // Determine the index for the bit-timing parameters
  // dependent on the baudrate selected from a predefined set of baudrates

  // Change of bus speed only allowed when the port is not open
  if( this->isOpen() ) return false;

  // Only certain baudrates are allowed
  bool result = true;
  if( kbitrate == 1000 )     _kbIndex = KB1000;
  else if( kbitrate == 800 ) _kbIndex = KB800;
  else if( kbitrate == 500 ) _kbIndex = KB500;
  else if( kbitrate == 250 ) _kbIndex = KB250;
  else if( kbitrate == 125 ) _kbIndex = KB125;
  else if( kbitrate == 50 )  _kbIndex = KB50;
  else if( kbitrate == 20 )  _kbIndex = KB20;
  else if( kbitrate == 10 )  _kbIndex = KB10;
  else result = false;

  if( result ) _kbitRate = kbitrate;
  return result;
}

/* ------------------------------------------------------------------------ */

bool PeakInterface::open()
{
  _opStatus = 0;

  // Initialize the CAN port
  TPCANStatus stat = CAN_Initialize( PEAK_USBCAN_HANDLE[_portIndex],
				     BAUD_REG[_kbIndex] );
  if( stat != PCAN_ERROR_OK )
    {
      _opStatus = stat;
      return false;
    }
  // Set port to auto-recover from a bus-off state
  int on = PCAN_PARAMETER_ON;
  CAN_SetValue( PEAK_USBCAN_HANDLE[_portIndex],
		PCAN_BUSOFF_AUTORESET, &on, sizeof(int) );
  _isOpen = true;
  return true;
}

/* ------------------------------------------------------------------------ */

bool PeakInterface::close()
{
  bool result = true;
  this->awaitTransmission( 100 ); // In case of any remaining messages to send

  // Flush hardware and/or software buffers
  this->flush();

  _opStatus = 0;
  _isOpen = false;

  TPCANStatus stat = CAN_Uninitialize( PEAK_USBCAN_HANDLE[_portIndex] );
  if( stat != PCAN_ERROR_OK )
    {
      _opStatus = stat;
      result = false;
    }

  return result;
}

/* ------------------------------------------------------------------------ */

bool PeakInterface::send( CanMessage &can_msg )
{
  _opStatus = 0;

  TPCANMsg pcan_msg;
  pcan_msg.MSGTYPE = PCAN_MESSAGE_STANDARD;
  if( can_msg.isRemoteFrame() ) pcan_msg.MSGTYPE = PCAN_MESSAGE_RTR;
  // if( can_msg.isErrFrame() ) ???? // Can't handle this
  pcan_msg.LEN = can_msg.dlc();
  pcan_msg.ID  = can_msg.cobId();
  memcpy( pcan_msg.DATA, can_msg.data(), can_msg.dlc() );

  TPCANStatus stat;
  bool result = true;
  stat = CAN_Write( PEAK_USBCAN_HANDLE[_portIndex], &pcan_msg );
  if( stat != PCAN_ERROR_OK )
    {
      if( stat == PCAN_ERROR_QXMTFULL )
	{
	  // If interface output buffer overflow, wait until (some) messages
	  // have been sent and try to resend the message
	  Sleep( 10 );
	  stat = CAN_Write( PEAK_USBCAN_HANDLE[_portIndex], &pcan_msg );
	  if( stat != PCAN_ERROR_OK )
	    {
	      _opStatus = stat;
	      result = false;
	    }
	}
      else
	{
	  _opStatus = stat;
	  result = false;
	}
    }

  return result;
}

/* ------------------------------------------------------------------------ */

CanMessage* PeakInterface::receive( unsigned int timeout_ms )
{
  // Read the next available message;
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This could be done in a separate thread)
  this->bufferReceivedMsgs();

  if( !_canMsgs.empty() )
    {
      // Get the first message from the list
      pMsg = *(_canMsgs.begin());

      // Remove the message pointer from the list
      _canMsgs.pop_front();

      return pMsg;
    }

  if( timeout_ms != 0 )
    {
      int cntr = timeout_ms/10;
      if( cntr == 0 ) cntr = 1;
      while( cntr > 0 )
        {
          // Should wait, and then read again...
          Sleep( 10 );
          this->bufferReceivedMsgs();
          if( !_canMsgs.empty() )
            {
              // Get the first message from the list
              pMsg = *(_canMsgs.begin());
              // Remove the message pointer from the list
              _canMsgs.pop_front();
              return pMsg;
            }
          --cntr;
        }
      // After time-out still no message?
      // Indicate it by using a PEAK defined error value
      _opStatus = PCAN_ERROR_QRCVEMPTY;
    }

  return pMsg;
}

/* ------------------------------------------------------------------------ */

CanMessage* PeakInterface::receiveSpecific( int          cob_id,
					    unsigned int timeout_ms )
{
  // Read the next available message having COB-ID 'cob_id';
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This could be done in a separate thread)
  this->bufferReceivedMsgs();

  // Is this message perhaps already available in the currently held list ?
  pMsg = this->getMsg( cob_id );

  // Message found ?
  if( pMsg ) return pMsg;

  if( timeout_ms != 0 )
    {
      int cntr = (timeout_ms+9)/10; // Minimum counter is 1
      while( cntr > 0 )
        {
          // Should wait, and then read again...
          Sleep( 10 );
          this->bufferReceivedMsgs();
          pMsg = this->getMsg( cob_id );
          // Message found ?
          if( pMsg ) return pMsg;
          --cntr;
        }
      // After time-out still no message?
      // Indicate it by using a PEAK defined error value
      _opStatus = PCAN_ERROR_QRCVEMPTY;
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

void PeakInterface::bufferReceivedMsgs()
{
  // Get all available received CAN messages from the interface
  // and store them in the list; limit the number of received Error Frames!
  TPCANStatus    stat;
  TPCANMsg       pcan_msg;
  TPCANTimestamp pcan_timestamp;
  CanMessage    *pMsg = 0;
  bool           rtr;
 
  _opStatus = 0;
  _msgErrFlags = 0;

  // Check for and read any CAN messages received
  while( (stat = CAN_Read( PEAK_USBCAN_HANDLE[_portIndex],
			   &pcan_msg, &pcan_timestamp )) == PCAN_ERROR_OK )
    {
      // ## No Error Frames
      rtr = false;
      if( pcan_msg.MSGTYPE == PCAN_MESSAGE_RTR ) rtr = true;

      pMsg = new CanMessage( pcan_msg.ID, rtr,
			     pcan_msg.LEN, pcan_msg.DATA,
			     pcan_timestamp.millis );

      // Append (a pointer to) the message to the list
      _canMsgs.push_back( pMsg );
    }

  // Report problems (but 'no data' is of course not a problem..)
  if( stat != PCAN_ERROR_QRCVEMPTY ) _opStatus = stat;
}

/* ------------------------------------------------------------------------ */

bool PeakInterface::awaitTransmission( unsigned int timeout_ms )
{
  _opStatus = 0;

  // Wait for all messages in the queue to be transmitted on the bus
  // or until timeout_ms milliseconds have passed:
  // for PEAK we have no way of knowing whether all messages are sent,
  // just that there are no full buffers (CAN controller or transmit queue)
  // (so indeed the following code doesn't work properly;
  // e.g. for CanInterface::scanBus() increased the time-out for the first
  // non-existent node from 20 to 100 ms, allowing the request messages
  // to be sent while waiting for this time-out)
  int cntr = timeout_ms/10;
  if( cntr == 0 ) cntr = 1;
  TPCANStatus stat = CAN_GetStatus( PEAK_USBCAN_HANDLE[_portIndex] );
  while( (stat & (PCAN_ERROR_XMTFULL | PCAN_ERROR_QXMTFULL)) != 0 &&
	 cntr > 0 )
    {
      // Should wait, then check again...
      Sleep( 10 );
      stat = CAN_GetStatus( PEAK_USBCAN_HANDLE[_portIndex] );
      --cntr;
    }
  // After time-out still messages in the queue:
  // indicate it by using a PEAK defined error value
  if( (stat & (PCAN_ERROR_XMTFULL | PCAN_ERROR_QXMTFULL)) != 0 )
    {
      _opStatus = stat;
      return false;
    }
  return true;
}

/* ------------------------------------------------------------------------ */

void PeakInterface::flush()
{
  _opStatus = 0;

  // Reset transmit and receive buffers
  TPCANStatus stat = CAN_Reset( PEAK_USBCAN_HANDLE[_portIndex] );
  if( stat != PCAN_ERROR_OK ) _opStatus = stat;

  // Clear the received messages list:
  // delete the allocated CAN messages, then clear the list
  list<CanMessage *>::iterator it;
  for( it=_canMsgs.begin(); it!=_canMsgs.end(); ++it )
    delete (*it);
  _canMsgs.clear();
}

/* ------------------------------------------------------------------------ */

unsigned long PeakInterface::state()
{
  // Return the state/status of the interface
  _opStatus = 0;

  TPCANStatus stat = CAN_GetStatus( PEAK_USBCAN_HANDLE[_portIndex] );

  return stat;
}

/* ------------------------------------------------------------------------ */

string PeakInterface::stateString( unsigned long state )
{
  // Return a string describing the state of the interface
  string str;
  unsigned long known_mask = ( PCAN_ERROR_XMTFULL |
			       PCAN_ERROR_OVERRUN |
			       PCAN_ERROR_BUSLIGHT |
			       PCAN_ERROR_BUSHEAVY |
			       PCAN_ERROR_BUSOFF |
			       PCAN_ERROR_QRCVEMPTY |
			       PCAN_ERROR_QOVERRUN |
			       PCAN_ERROR_QXMTFULL |
			       PCAN_ERROR_REGTEST |
			       PCAN_ERROR_NODRIVER |
			       PCAN_ERROR_HWINUSE |
			       PCAN_ERROR_NETINUSE |
			       PCAN_ERROR_ILLHANDLE |
			       PCAN_ERROR_RESOURCE |
			       PCAN_ERROR_ILLPARAMTYPE |
			       PCAN_ERROR_ILLPARAMVAL |
			       PCAN_ERROR_UNKNOWN |
			       PCAN_ERROR_INITIALIZE );

  // Check for known state bits
  if( state & PCAN_ERROR_XMTFULL )      str += "XMTFULL ";
  if( state & PCAN_ERROR_OVERRUN )      str += "OVERRUN ";
  if( state & PCAN_ERROR_BUSLIGHT )     str += "BUSLIGHT ";
  if( state & PCAN_ERROR_BUSHEAVY )     str += "BUSHEAVY ";
  if( state & PCAN_ERROR_BUSOFF )       str += "BUSOFF ";
  if( state & PCAN_ERROR_QRCVEMPTY )    str += "QRCVEMPTY ";
  if( state & PCAN_ERROR_QOVERRUN )     str += "QOVERRUN ";
  if( state & PCAN_ERROR_QXMTFULL )     str += "QXMTFULL ";
  if( state & PCAN_ERROR_REGTEST )      str += "REGTEST ";
  if( state & PCAN_ERROR_NODRIVER )     str += "NODRIVER ";
  if( state & PCAN_ERROR_HWINUSE )      str += "HWINUSE ";
  if( state & PCAN_ERROR_NETINUSE )     str += "NETINUSE ";
  if( state & PCAN_ERROR_RESOURCE )     str += "RESOURCE ";
  if( state & PCAN_ERROR_ILLPARAMTYPE ) str += "ILLPARAMTYPE ";
  if( state & PCAN_ERROR_ILLPARAMVAL )  str += "ILLPARAMVAL ";
  if( state & PCAN_ERROR_UNKNOWN )      str += "UNKNOWN ";
  if( state & PCAN_ERROR_INITIALIZE )   str += "INITIALIZE ";
  if( (state & PCAN_ERROR_ILLHANDLE) == PCAN_ERROR_ILLHANDLE )
    str += "ILLHANDLE ";
  else if( (state & PCAN_ERROR_ILLHW) == PCAN_ERROR_ILLHW )
    str += "ILLHW ";
  else if( (state & PCAN_ERROR_ILLNET) == PCAN_ERROR_ILLNET )
    str += "ILLNET ";

  // Any of the unknown bits set ?
  if( state & (~known_mask) )
    { 
      str += "???? ";
      ostringstream oss;
      oss << " (" << hex << setw(8) << setfill('0')
	  << (state & (~known_mask)) << ")";
      str += oss.str();
    }

  return str;
}

/* ------------------------------------------------------------------------ */

string PeakInterface::errString()
{
  // Returns a string describing the result of the last CAN interface operation
  char str[300];
  TPCANStatus stat;
  LPSTR       lpstr = str;
  stat = CAN_GetErrorText( (TPCANStatus) _opStatus, 0, lpstr );
  return string( str );
}

/* ------------------------------------------------------------------------ */

string PeakInterface::errString( const char *context )
{
  // Returns a string describing the result of the last CAN interface operation
  // preceeded by a 'context' string
  return( string( context ) + ": " + this->errString() );
}

/* ------------------------------------------------------------------------ */

#ifdef WIN32
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

extern "C" MY_EXPORT CanInterface *newPeakInterface( int  port_nr,
						     int  kbitrate,
						     bool open_now )
{
  return new PeakInterface( port_nr, kbitrate, open_now );
}

/* ------------------------------------------------------------------------ */

extern "C" MY_EXPORT int peakInterfacePorts( int *port_numbers )
{
  vector<int> ports = PeakInterface::ports();
  sort( ports.begin(), ports.end() );
  for( unsigned int i=0; i<ports.size(); ++i ) port_numbers[i] = ports[i];
  return ports.size();
}

/* ------------------------------------------------------------------------ */
