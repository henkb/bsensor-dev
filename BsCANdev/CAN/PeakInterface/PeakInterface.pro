#
# Project file for the PeakInterface library
# (Windows only)
#
# To generate a Visual Studio project:
#   qmake -t vclib PeakInterface.pro
# To generate a Makefile:
#   qmake PeakInterface.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = PeakInterface

# Create a shared library
CONFIG += shared qt thread warn_on exceptions debug_and_release

QT -= gui
QT += core

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

win32 {
  INCLUDEPATH += "C:/Program Files/PCAN/PCAN-Basic API/Include"
  INCLUDEPATH += "C:/Program Files (x86)/PCAN/PCAN-Basic API/Include"
  # Need to add the following lib when creating the shared lib
  LIBS += "-LC:/Program Files/PCAN/PCAN-Basic API/Win32/VC_LIB"
  LIBS += "-LC:/Program Files (x86)/PCAN/PCAN-Basic API/Win32/VC_LIB"
  LIBS += -lPCANBasic
}
LIBS += -lCANopen

INCLUDEPATH += ../CANopen

SOURCES += PeakInterface.cpp
HEADERS += PeakInterface.h
HEADERS += ../CANopen/CanInterface.h
HEADERS += ../CANopen/CanMessage.h
HEADERS += ../CANopen/CANopen.h

