/* ------------------------------------------------------------------------
File   : PeakInterface.h

Descr  : PEAK CAN-port/interface class.

History: 05NOV.12; Henk B&B; Creation.
--------------------------------------------------------------------------- */

#ifndef PEAKINTERFACE_H
#define PEAKINTERFACE_H

#include "PCANBasic.h"

#include "CanInterface.h"

/* ------------------------------------------------------------------------ */

class PeakInterface: public CanInterface
{
 public:
  PeakInterface( int port_no   = CAN_INTF_DFLT,
		 int bitrate   = CAN_BITRATE_DFLT,
		 bool open_now = true );

  ~PeakInterface();

  static std::vector<int> ports();

  bool          setBusSpeed( int kbitrate );

  bool          open();

  bool          close();

  bool          send( CanMessage &can_msg );

  CanMessage*   receive( unsigned int timeout_ms = 0 );

  CanMessage*   receiveSpecific( int cob_id, unsigned int timeout_ms = 0 );

  bool          awaitTransmission( unsigned int timeout_ms );

  void          flush();

  unsigned long state();
  std::string   stateString( unsigned long state );

  std::string   errString();
  std::string   errString( const char *context );

 private:
  void          bufferReceivedMsgs();

  // Index for selected port number
  int         _portIndex;

  // Index for selected bitrate
  int         _kbIndex;

  // Some convenient defaults
  static const int         CAN_INTF_DFLT;
  static const int         CAN_BITRATE_DFLT;
  static const std::string MANUFACTURER_STR;
};

/* ------------------------------------------------------------------------ */
#endif // PEAKINTERFACE_H
