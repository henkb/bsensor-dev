/* ------------------------------------------------------------------------
File   : KvaserInterface.cpp

Descr  : Kvaser CAN-interface class implementation.

History: --FEB.04; Henk B&B; Start of development.
         10FEB.10; Henk B&B; Version for Linux: Kvaser canlib does not have
                             the canReadSpecific..() calls, so have to rely
                             on local buffering of messages in memory,
                             like in class NiCanInterface.
         07SEP.11; Henk B&B; Incorporated Linux version differences using
                             #ifdef/#ifndef _LOCAL_MSG_BUFFER.
         24OCT.11; Henk B&B; Work with a local message buffer: remove Linux/
                             Windows differences, i.e. no longer using
                             canReadSpecific..() calls.
--------------------------------------------------------------------------- */

#include <sstream>
#include <iomanip>
using namespace std;

#include "CANopen.h"
#include "KvaserInterface.h"
#include "CanMessage.h"

const int    KvaserInterface::CAN_INTF_DFLT    = 1;
const int    KvaserInterface::CAN_BITRATE_DFLT = 125;
const string KvaserInterface::MANUFACTURER_STR = string("KVASER");

/* Next values according to CANopen specification, for a 16 MHz 8xC592:
   bit rates           1000, 800, 500, 250, 125, 50, 20, 10 kbit/s: */
const char SJW[]   = {    1,   1,   1,   1,   1,  1,  1,  1 };
const char SAM[]   = {    0,   0,   0,   0,   0,  0,  0,  0 };
const char PRESC[] = {    1,   1,   1,   2,   4, 10, 25, 50 };
const char TSEG1[] = {    5,   7,  13,  13,  13, 13, 13, 13 };
const char TSEG2[] = {    2,   2,   2,   2,   2,  2,  2,  2 };

/* Indices for bit rates */
#define KB1000  0
#define KB800   1
#define KB500   2
#define KB250   3
#define KB125   4
#define KB50    5
#define KB20    6
#define KB10    7

/* ------------------------------------------------------------------------ */

KvaserInterface::KvaserInterface( int  port_nr,
				  int  kbitrate,
				  bool open_now )
  : CanInterface( MANUFACTURER_STR, KVASER_INTF_TYPE, port_nr, kbitrate )
{
  canInitializeLibrary();

  // Port count in the software starts at 0, for the Kvaser user at 1;
  // if port_no==0 (actually -1) the first available port is opened
  --port_nr;
  _handle = canOpenChannel( port_nr, canOPEN_EXCLUSIVE );
  //_handle = canOpenChannel( port_nr, 0 ); // Not exclusive

  if( _handle < 0 )
    {
      _opStatus = _handle;
      _initResultString = this->errString( "canOpenChannel" );
      return;
    }

  if( !this->setBusSpeed( kbitrate ) )
    {
      _kbitRate = 125;
      _kbIndex  = KB125;
    }

  // The bit-timing parameters
  int sjw, tseg1, tseg2, sam;
  tseg1 = TSEG1[_kbIndex];
  tseg2 = TSEG2[_kbIndex];
  sjw   = SJW[_kbIndex];
  sam   = SAM[_kbIndex];

  canStatus stat;
  stat = canSetBusParams( _handle, kbitrate*1000, tseg1, tseg2, sjw, sam, 0 );
  if( stat != canOK )
    {
      _opStatus = stat;
      _initResultString = this->errString( "canSetBusParams" );
    }

  stat = canSetBusOutputControl( _handle, canDRIVER_NORMAL );
  if( stat != canOK )
    {
      _opStatus = stat;
      _initResultString = this->errString( "canSetBusOutputControl" );
    }

  if( open_now )
    {
      _isOpen = true;
      stat = canBusOn( _handle );
      if( stat != canOK )
	{
	  _opStatus = stat;
	  _initResultString = this->errString( "canBusOn" );
	  _isOpen = false;
	}
    }
}

/* ------------------------------------------------------------------------ */

KvaserInterface::~KvaserInterface()
{
  if( _handle < 0 ) return;
  this->close();
  canClose( _handle );
}

/* ------------------------------------------------------------------------ */

vector<int> KvaserInterface::ports()
{
  // Create a list of available port numbers
  vector<int> portnumbers;

  // In case we call this (static) method 'standalone'
  canInitializeLibrary();

  int channel_cnt;
  if( canGetNumberOfChannels( &channel_cnt ) != canOK ) channel_cnt = 0;

  // Don't count virtual channels;
  // there are 2 of them at most, right after the physical channels
  if( channel_cnt >= 2 )
    {
      // Don't assume anymore there are virtual channels ! Check them...
      //channel_cnt -= 2;
      unsigned int hwtype;
      int          chan = channel_cnt - 1;
      if( canGetChannelData( chan, canCHANNELDATA_CARD_TYPE,
			     &hwtype, sizeof(hwtype) ) == canOK )
	if( hwtype == canHWTYPE_VIRTUAL ) --channel_cnt;
      if( canGetChannelData( chan-1, canCHANNELDATA_CARD_TYPE,
			     &hwtype, sizeof(hwtype) ) == canOK )
	if( hwtype == canHWTYPE_VIRTUAL ) --channel_cnt;
    }

  // KVASER interface: start numbering from 1
  for( int i=0; i<channel_cnt; ++i ) portnumbers.push_back( i+1 );

  return portnumbers;
}

/* ------------------------------------------------------------------------ */

bool KvaserInterface::setBusSpeed( int kbitrate )
{
  // Determine the index for the bit-timing parameters
  // dependent on the baudrate selected from a predefined set of baudrates

  // Change of bus speed only allowed when the port is not open
  if( this->isOpen() ) return false;

  // Only certain baudrates are allowed
  bool result = true;
  if( kbitrate == 1000 )     _kbIndex = KB1000;
  else if( kbitrate == 800 ) _kbIndex = KB800;
  else if( kbitrate == 500 ) _kbIndex = KB500;
  else if( kbitrate == 250 ) _kbIndex = KB250;
  else if( kbitrate == 125 ) _kbIndex = KB125;
  else if( kbitrate == 50 )  _kbIndex = KB50;
  else if( kbitrate == 20 )  _kbIndex = KB20;
  else if( kbitrate == 10 )  _kbIndex = KB10;
  else result = false;

  if( result ) _kbitRate = kbitrate;
  return result;
}

/* ------------------------------------------------------------------------ */

bool KvaserInterface::open()
{
  _opStatus = 0;

  // The bit-timing parameters
  int sjw, tseg1, tseg2, sam;
  tseg1 = TSEG1[_kbIndex];
  tseg2 = TSEG2[_kbIndex];
  sjw   = SJW[_kbIndex];
  sam   = SAM[_kbIndex];
  canStatus stat;
  stat = canSetBusParams( _handle, _kbitRate*1000, tseg1, tseg2, sjw, sam, 0 );
  if( stat != canOK )
    {
      _opStatus = stat;
      return false;
    }

  stat = canBusOn( _handle );
  if( stat != canOK )
    {
      _opStatus = stat;
      return false;
    }

  _isOpen = true;
  return true;
}

/* ------------------------------------------------------------------------ */

bool KvaserInterface::close()
{
  bool result = true;
  this->awaitTransmission( 100 ); // In case of any remaining messages to send

  _opStatus = 0;
  _isOpen = false;

  canStatus stat = canBusOff( _handle );
  if( stat != canOK )
    {
      _opStatus = stat;
      result = false;
    }

  // Flush hardware and software buffers
  this->flush();

  return result;
}

/* ------------------------------------------------------------------------ */

bool KvaserInterface::send( CanMessage &can_msg )
{
  int stat;
  unsigned int flags = 0;

  _opStatus = 0;

  if( can_msg.isRemoteFrame() ) flags |= canMSG_RTR;
  if( can_msg.isErrFrame() )    flags |= canMSG_ERROR_FRAME;

  bool result = true;
  stat = canWrite( _handle, can_msg.cobId(),
		   can_msg.data(), can_msg.dlc(), flags );
  if( stat != canOK )
    {
      if( stat == canERR_TXBUFOFL )
	{
	  // If interface output buffer overflow, wait until (some) messages
	  // have been sent and try to resend the message
	  stat = canWriteSync( _handle, 100 ); 
	  //if( stat == canOK )
	  stat = canWrite( _handle, can_msg.cobId(),
			   can_msg.data(), can_msg.dlc(), flags );
	  if( stat != canOK )
	    {
	      _opStatus = stat;
	      result = false;
	    }
	}
      else
	{
	  _opStatus = stat;
	  result = false;
	}
    }

  return result;
}

/* ------------------------------------------------------------------------ */

CanMessage* KvaserInterface::receive( unsigned int timeout_ms )
{
  // Read the next available message;
  // if required wait 'timeout_ms' milliseconds
  CanMessage *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  if( !_canMsgs.empty() )
    {
      // Get the first message from the list
      pMsg = *(_canMsgs.begin());

      // Remove the message pointer from the list
      _canMsgs.pop_front();

      return pMsg;
    }

  if( timeout_ms != 0 )
    {
      canStatus     stat;
      unsigned char can_data[8];
      long          identifier;
      unsigned int  dlc;
      unsigned int  flags;
      unsigned long time;

      _opStatus = 0;
      _msgErrFlags = 0;

      identifier = 0;
      stat = canReadWait( _handle, &identifier,
			  can_data, &dlc, &flags, &time, timeout_ms );
      if( stat == canOK )
	{
	  // Indicate Error Frame or Remote Frame
	  if( flags & canMSG_ERROR_FRAME )
	    {
	      pMsg = new CanMessage();
	      pMsg->setTimeStamp( time );
	    }
	  else
	    {
	      pMsg = new CanMessage( identifier, flags & canMSG_RTR,
				     dlc, can_data, time );
	    }
	  _msgErrFlags = flags & canMSGERR_MASK;
	}
      else
	{
	  // Report problems ('no data' means: timeout occurred)
	  if( stat != canERR_NOMSG )
	    _opStatus = stat;
	  else
	    _opStatus = canERR_TIMEOUT;
	}
    }

  return pMsg;
}

/* ------------------------------------------------------------------------ */

CanMessage* KvaserInterface::receiveSpecific( int          cob_id,
					      unsigned int timeout_ms )
{
  // Read the next available message having COB-ID 'cob_id';
  // if required wait 'timeout_ms' milliseconds
  CanMessage    *pMsg = 0;

  // Read all received messages from the interface
  // (This should be done in a separate thread?)
  this->bufferReceivedMsgs();

  // Is this message perhaps already available in the currently held list ?
  pMsg = this->getMsg( cob_id );

  // Message found ?
  if( pMsg ) return pMsg;

  if( timeout_ms != 0 )
    {
      canStatus     stat;
      unsigned char can_data[8];
      long          identifier;
      unsigned int  dlc;
      unsigned int  flags;
      unsigned long time;
      int           errframe_cnt = 0;

      _opStatus = 0;
      _msgErrFlags = 0;

      while( !pMsg )
	{
	  identifier = 0;
	  stat = canReadWait( _handle, &identifier,
			      can_data, &dlc, &flags, &time, timeout_ms );
	  if( stat == canOK )
	    {
	      // Indicate Error Frame or Remote Frame
	      if( flags & canMSG_ERROR_FRAME )
		{
		  pMsg = new CanMessage();
		  ++errframe_cnt;
		  if( errframe_cnt == 11 )
		    {
		      // Received 10 Error Frames in a row: quit..
		      delete pMsg;
		      pMsg = 0;
		      _opStatus = canERR_TIMEOUT; // Consider it a time-out..
		      break; // Break out of while-loop
		    }
		}
	      else
		{
		  pMsg = new CanMessage( identifier, flags & canMSG_RTR,
					 dlc, can_data, time );
		}
	      _msgErrFlags = flags & canMSGERR_MASK;

	      // Is this a message we are looking for ?
	      if( identifier != cob_id )
		{
		  // No, so append (a pointer to) this message to the list
		  // of unhandled messages
		  _canMsgs.push_back( pMsg );

		  if( !pMsg->isErrFrame() ) errframe_cnt = 0;

		  // Reset the pointer
		  pMsg = 0;
		}
	    }
	  else
	    {
	      // Report problems ('no data' means: timeout occurred)
	      if( stat != canERR_NOMSG )
		_opStatus = stat;
	      else
		_opStatus = canERR_TIMEOUT;

	      // Interrupt the while-loop
	      break;
	    }
	}
    }
  return pMsg;
}

/* ------------------------------------------------------------------------ */

void KvaserInterface::bufferReceivedMsgs()
{
  // Get all available received CAN messages from the interface
  // and store them in the list; limit the number of received Error Frames!
  canStatus     stat;
  long          cob_id;
  unsigned char can_data[8];
  unsigned int  dlc;
  unsigned int  flags;
  unsigned long time;
  int           errframe_cnt = 0;
  CanMessage    *pMsg = 0;
 
  _opStatus = 0;
  _msgErrFlags = 0;

  // Check for and read any CAN messages received
  while( (stat = canRead( _handle, &cob_id,
			  can_data, &dlc, &flags, &time )) == canOK )
    {
      // Indicate Error Frame or Remote Frame
      if( flags & canMSG_ERROR_FRAME )
	{
	  pMsg = new CanMessage();
	  ++errframe_cnt;
	  if( errframe_cnt == 11 )
	    {
	      // Received 10 Error Frames in a row: quit..
	      delete pMsg;
	      break; // Break out of while-loop
	    }
	}
      else
	{
	  pMsg = new CanMessage( cob_id, flags & canMSG_RTR,
				 dlc, can_data, time );
	  errframe_cnt = 0;
	}
      _msgErrFlags = flags & canMSGERR_MASK;

      // Append (a pointer to) the message to the list
      _canMsgs.push_back( pMsg );
    }

  // Report problems (but 'no data' is of course not a problem..)
  if( stat != canERR_NOMSG ) _opStatus = stat;
}

/* ------------------------------------------------------------------------ */

bool KvaserInterface::awaitTransmission( unsigned int timeout_ms )
{
  _opStatus = 0;

  // Wait for all messages in the queue to be transmitted on the bus
  // or until timeout_ms milliseconds have passed
  canStatus stat = canWriteSync( _handle, timeout_ms );
  if( stat != canOK )
    {
      _opStatus = stat;
      return false;
    }
  return true;
}

/* ------------------------------------------------------------------------ */

void KvaserInterface::flush()
{
  canStatus    stat;
  int         *buf = 0;    // Dummy...
  unsigned int buflen = 0; // Dummy...

  _opStatus = 0;

  // Flush transmit and then receive buffers
  // (if done at the same time there may still be Error Frames received
  //  due to being disconnected from the bus and trying to send;
  //  found by testing, 28 Oct 2011)
  stat = canIoCtl( _handle, canIOCTL_FLUSH_TX_BUFFER, buf, buflen );
  if( stat != canOK ) _opStatus = stat;
  stat = canIoCtl( _handle, canIOCTL_FLUSH_RX_BUFFER, buf, buflen );
  if( stat != canOK ) _opStatus = stat;

  // Clear the received messages list:
  // delete the allocated CAN messages, then clear the list
  list<CanMessage *>::iterator it;
  for( it=_canMsgs.begin(); it!=_canMsgs.end(); ++it )
    delete (*it);
  _canMsgs.clear();
}

/* ------------------------------------------------------------------------ */

unsigned long KvaserInterface::state()
{
  // Return the state of the interface
  unsigned long flags;

  _opStatus = 0;

  canStatus stat = canReadStatus( _handle, &flags );
  if( stat != canOK ) _opStatus = stat;

  return flags;
}

/* ------------------------------------------------------------------------ */

string KvaserInterface::stateString( unsigned long state )
{
  // Return a string describing the state of the interface
  string str;
  unsigned long known_mask = (canSTAT_ERROR_PASSIVE |
			      canSTAT_BUS_OFF |
			      canSTAT_ERROR_WARNING |
			      canSTAT_ERROR_ACTIVE |
			      canSTAT_TX_PENDING |
			      canSTAT_RX_PENDING |
			      canSTAT_RESERVED_1 |
			      canSTAT_TXERR |
			      canSTAT_RXERR |
			      canSTAT_HW_OVERRUN |
			      canSTAT_SW_OVERRUN);

  // Check for known state bits
  if( state & canSTAT_ERROR_PASSIVE ) str += "ERR_PASSIVE ";
  if( state & canSTAT_BUS_OFF )       str += "BUS_OFF ";
  if( state & canSTAT_ERROR_WARNING ) str += "ERR_WARNING ";
  if( state & canSTAT_ERROR_ACTIVE )  str += "ERR_ACTIVE ";
  if( state & canSTAT_TX_PENDING )    str += "TX_PENDINIG ";
  if( state & canSTAT_RX_PENDING )    str += "RX_PENDING ";
  if( state & canSTAT_RESERVED_1 )    str += "RESERVED_1 ";
  if( state & canSTAT_TXERR )         str += "TXERR ";
  if( state & canSTAT_RXERR )         str += "RXERR ";
  if( state & canSTAT_HW_OVERRUN )    str += "HW_OVERRUN ";
  if( state & canSTAT_SW_OVERRUN )    str += "SW_OVERRUN ";

  // Any of the unknown bits set ?
  if( state & (~known_mask) )
    { 
      str += "???? ";
      ostringstream oss;
      oss << " (" << hex << setw(8) << setfill('0')
	  << (state & (~known_mask)) << ")";
      str += oss.str();
    }

  return str;
}

/* ------------------------------------------------------------------------ */

string KvaserInterface::errString()
{
  // Returns a string describing the result of the last CAN interface operation
  char str[300];
  canGetErrorText( (canStatus) _opStatus, str, sizeof(str) );
  return string( str );
}

/* ------------------------------------------------------------------------ */

string KvaserInterface::errString( const char *context )
{
  // Returns a string describing the result of the last CAN interface operation
  // preceeded by a 'context' string
  return( string( context ) + ": " + this->errString() );
}

/* ------------------------------------------------------------------------ */

#ifdef WIN32
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

extern "C" MY_EXPORT CanInterface *newKvaserInterface( int  port_nr,
						       int  kbitrate,
						       bool open_now )
{
  return new KvaserInterface( port_nr, kbitrate, open_now );
}

/* ------------------------------------------------------------------------ */

extern "C" MY_EXPORT int kvaserInterfacePorts( int *port_numbers )
{
  vector<int> ports = KvaserInterface::ports();
  for( unsigned int i=0; i<ports.size(); ++i ) port_numbers[i] = ports[i];
  return ports.size();
}

/* ------------------------------------------------------------------------ */
