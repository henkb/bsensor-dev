#
# Project file for the KvaserInterface library
#
# To generate a Visual Studio project:
#   qmake -t vclib KvaserInterface.pro
# To generate a Makefile:
#   qmake KvaserInterface.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = KvaserInterface

# Create a shared library
CONFIG += shared qt thread warn_on exceptions debug_and_release

QT -= gui
QT += core

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

win32 {
  INCLUDEPATH += "C:/Program Files (x86)/kvaser/canlib/inc"
  # Need to add the following lib when creating the shared lib
  #LIBS += "-LC:/Program Files (x86)/KVASER/Canlib/Lib/MS"
  LIBS += "-LC:/Program Files (x86)/KVASER/Canlib/Lib/x64"
  LIBS += -lcanlib32
}
unix {
  INCLUDEPATH += "../../../kvaser-linuxcan_v5/include"
  LIBS += -lcanlib
}
LIBS += -lCANopen

INCLUDEPATH += ../CANopen

SOURCES += KvaserInterface.cpp
HEADERS += KvaserInterface.h
HEADERS += ../CANopen/CanInterface.h
HEADERS += ../CANopen/CanMessage.h
HEADERS += ../CANopen/CANopen.h

