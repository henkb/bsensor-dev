/* ------------------------------------------------------------------------
File   : KvaserInterface.h

Descr  : Kvaser CAN-port/interface class.

History: --FEB.04; Henk B&B; First definition.
         10FEB.10; Henk B&B; Version for Linux: Kvaser canlib API does not have
	                     the canReadSpecific..() calls, so have to rely
			     on local buffering of messages, like in class
			     NiCanInterface;define "_LOCAL_MSG_BUFFER" for it.
--------------------------------------------------------------------------- */

#ifndef KVASERINTERFACE_H
#define KVASERINTERFACE_H

#include <canlib.h>

#include "CanInterface.h"

/* ------------------------------------------------------------------------ */

class KvaserInterface: public CanInterface
{
 public:
  KvaserInterface( int port_no   = CAN_INTF_DFLT,
		   int kbitrate  = CAN_BITRATE_DFLT,
		   bool open_now = true );

  ~KvaserInterface();

  static std::vector<int> ports();

  bool          setBusSpeed( int kbitrate );

  bool          open();

  bool          close();

  bool          send( CanMessage &can_msg );

  CanMessage*   receive( unsigned int timeout_ms = 0 );

  CanMessage*   receiveSpecific( int cob_id, unsigned int timeout_ms = 0 );

  bool          awaitTransmission( unsigned int timeout_ms );

  void          flush();

  unsigned long state();
  std::string   stateString( unsigned long state );

  std::string   errString();
  std::string   errString( const char *context );

 private:
  void          bufferReceivedMsgs();

  // Index for selected bitrate
  int       _kbIndex;

  // Handle to the actual device
  canHandle _handle;

  static const int         CAN_INTF_DFLT;
  static const int         CAN_BITRATE_DFLT;
  static const std::string MANUFACTURER_STR;
};

/* ------------------------------------------------------------------------ */
#endif // KVASERINTERFACE_H
