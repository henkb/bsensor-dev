#include <iostream>
#include <iomanip>
#include <windows.h>
#include "bscanimpl.h"

int main( int argc, char **argv )
{
  cout << BsCanImpl::version() << endl;

  // A B-sensor system
  BsCanImpl bsystem;

  // Connected to CAN port 2 (Kvaser)
  cout << "CAN port 2" << endl;
  bsystem.setCanPort( 2 );

  cout << "Status = " << hex << bsystem.errStatus();
  if(  !bsystem.errString().empty() )
    cout << ": " << bsystem.errString() << endl;
  else
    cout << endl;

  cout << bsystem.configSummaryString() << endl;

  char ch;
  cin >> ch;

#ifdef PROBE_TEST
  cout << "bSensorProbe: " << bsystem.bSensorProbe() << endl;

  while( bsystem.bSensorProbeInProgress() ) Sleep( 1000 );

  cout << "==> Probe done..." << endl;

  cout << "readConfig:" << bsystem.readConfiguration() << endl;

  cout << bsystem.configSummaryString() << endl;

  cin >> ch;

  bsystem.triggerSingleReadout();

  while( !bsystem.bSensorDataAvailable() ) Sleep( 100 );

  cout << "==> Data available..." << endl;

  cin >> ch;
#endif

  //bsystem.setReadoutTriggerInterval( 5000 );

  while( 1 )
  for( int i=0; i<bsystem.bSensorCount(); ++i )
    {
      cout << i << endl;
      if( bsystem.bSensorSelect( i ) == false )
	cout << "  select: " << bsystem.errString() << endl;
      Sleep( 500 );
      if( bsystem.bSensorDeselect() == false )
	cout << "  deselect: " << bsystem.errString() << endl;
      Sleep( 100 );
    }

  return 0;
}
