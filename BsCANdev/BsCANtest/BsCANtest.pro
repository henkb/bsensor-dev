#
# Project file for BsCANtest
#
# To generate a Visual Studio project:
#   qmake -t vcapp BsCANtest.pro
# To generate a Makefile:
#   qmake BsCANtest.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = app
TARGET   = BsCANtest

# Create a console app
CONFIG += console qt warn_on exceptions debug_and_release

# Need only part of Qt..
QT += core
QT -= gui

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  LIBS       += -L../Release
}

INCLUDEPATH += ../CAN/CANopen
INCLUDEPATH += ../BsCAN

win32 {
  INCLUDEPATH += "C:/Program Files/kvaser/canlib/inc"
  INCLUDEPATH += "C:/Program Files (x86)/kvaser/canlib/inc"
  LIBS += "-LC:/Program Files/KVASER/Canlib/Lib/MS"
  LIBS += "-LC:/Program Files (x86)/KVASER/Canlib/Lib/MS"
  LIBS += -lcanlib32
}
unix {
  LIBS += -lcanlib
}
LIBS += -lBsCAN
LIBS += -lCANopen

SOURCES += bscantest.cpp
