#include <iostream>
#include <sstream>
#include <iomanip>
#include <windows.h>
#include "bscan.h"
#include "bscan_c.h"
using namespace std;

int main( int argc, char **argv )
{
  cout << "Starting..." << endl;

  // A B-sensor system
  BsCan bsystem;

  cout << bsystem.version() << ", " << bsystem.calibConstFile() << endl;
  // Connected to CAN port 2 (Kvaser)
  cout << "CAN port 2" << endl;
  if( bsystem.setCanPort( 2 ) == false )
    cout << "setCanPort(): " << bsystem.errString() << endl;

  cout << "Status = " << hex << bsystem.errStatus() << dec;
  if(  !bsystem.errString().empty() )
    cout << ": " << bsystem.errString() << endl;
  else
    cout << endl;

  cout << bsystem.configSummaryString() << endl;

  char ch;

  // ------------------------------------------------------------------
#ifdef PROBE_TEST
  cin >> ch;

  cout << "bSensorProbe: " << bsystem.bSensorProbe() << endl;

  while( bsystem.bSensorProbeInProgress() ) Sleep( 1000 );

  cout << "==> Probe done..." << endl;

  cout << "readConfig:" << bsystem.readConfiguration() << endl;

  cout << bsystem.configSummaryString() << endl;

  cin >> ch;

  bsystem.triggerSingleReadout();

  while( !bsystem.bSensorDataAvailable() ) Sleep( 100 );

  cout << "==> Data available..." << endl;

  cin >> ch;

  bsystem.setReadoutTriggerMsec( 5000 );

  for( uint32_t i=0; i<bsystem.bSensorCount(); ++i )
    {
      cout << i << endl;
      if( bsystem.bSensorSelect( i ) == false )
	cout << "  select: " << bsystem.errString() << endl;
      Sleep( 500 );
      if( bsystem.bSensorDeselect( i ) == false )
	cout << "  deselect: " << bsystem.errString() << endl;
      Sleep( 100 );
    }
#endif // PROBE_TEST
  // ------------------------------------------------------------------

  for( uint32_t i=0; i<bsystem.nodeCount(); ++i )
    {
      cout << "NodeErr " << i << hex << ": " << bsystem.nodeErrStatus(i)
	   << dec << endl;
    }
  if( bsystem.bSensorError() )
    {
      for( uint32_t i=0; i<bsystem.bSensorCount(); ++i )
	if( bsystem.bSensorErrStatus(i) )
	  cout << "BsensorErr " << i << hex << ": "
	       << bsystem.bSensorErrStatus(i) << dec << endl;
    }
  //bsystem.getBsensorErrStatus();

  // ------------------------------------------------------------------
#ifdef DAQ_TEST
  cin >> ch;

  bsystem.setReadoutTriggerMsec( 2000 );

  uint32_t offs[4];
  for( uint32_t loop=0; loop<4; ++loop )
    {
      // Just to test with activity inbetween triggers (=SYNCs)
      if( loop == 2 )
	{
	  Sleep( 3000 );
	  bsystem.bSensorOffsets( 0, offs );
	}

      while( !bsystem.bSensorDataAvailable() ) Sleep( 100 );

      cout << "==> Data available..." << endl;
      cout << "Timestamp: " << bsystem.dataTimeStampString( true ) << endl;
      int32_t data[4];
      //int32_t *data = new int32_t[bsystem.bSensorCount()*4];
      for( uint32_t i=0; i<bsystem.bSensorCount(); ++i )
	{
	  cout << setw(2) << i << ": ";
	  if( bsystem.bSensorDataRaw( data, i ) )
	    {
	      for( int j=0; j<4; ++j )
		cout << setw(6) << data[j] << "  ";
	    }
	  else
	    {
	      cout << "NONE";
	    }
	  cout << endl;
	}

      bsystem.bSensorDataRelease();
    }

  bsystem.setReadoutTriggerSec( 0 );
#endif // DAQ_TEST
  // ------------------------------------------------------------------

#ifdef EMG_TEST
  cout << endl;
  cout << "Emg count = " << bsystem.emgCount() << endl;
  for( uint32_t i=0; i<bsystem.emgCount(); ++i )
    {
      cout << i << ": "
	   << bsystem.emgTimeStampString( i, true ) << " "
	   << bsystem.emgDescription( i ) << endl;
    }

  bsystem.emgClear();
  cout << "Cleared, Emg count = " << bsystem.emgCount() << endl;
#endif // EMG_TEST

  uint32_t regs[4];
#ifdef OFFSGAINS_TEST
  cin >> ch;

  cout << "OFFS/GAINS #0" << endl;
  for( int loop=0; loop<3; ++loop )
    {
      bsystem.bSensorReset();
      while( bsystem.bSensorResetInProgress() ) Sleep( 100 );
      if( bsystem.bSensorOffsets( 0, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      if( bsystem.bSensorGains( 0, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      cout << endl;
    }
  cout << "OFFS/GAINS #1" << endl;
  for( int loop=0; loop<0; ++loop )
    {
      bsystem.bSensorReset();
      while( bsystem.bSensorResetInProgress() ) Sleep( 100 );
      if( bsystem.bSensorOffsets( 1, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      if( bsystem.bSensorGains( 1, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      cout << endl;
    }
#endif // OFFSGAINS_TEST

  int bsensor_i = 5;

#ifdef RESETSINGLE_TEST
  //for( uint32_t b=0; b<bsystem.bSensorCount(); ++b )
  cout << "OFFS/GAINS #" << bsensor_i << " (Single)" << endl;
  for( int loop=0; loop<3; ++loop )
    {
      if( bsystem.bSensorResetSingle( bsensor_i ) == false )
	cout << "###reset " << bsystem.errString() << endl;
      if( bsystem.bSensorOffsets( bsensor_i, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      else
	cout << "###offs " << bsystem.errString() << endl;
      if( bsystem.bSensorGains( bsensor_i, regs ) )
	for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
      else
	cout << "###gain " << bsystem.errString() << endl;
      cout << endl;
    }
#endif // RESETSINGLE_TEST

//#define ADCCALIBRATE_TEST
#ifdef ADCCALIBRATE_TEST
  cin >> ch;

  cout << "AdcCalibrate" << endl;
  double sigma[8], max_sigma = 100.0;
  //if( bsystem.bSensorAdcCalibrate( 0, 5, max_sigma ) == false )
  if( bsystem.bSensorAdcCalibrate( bsensor_i, 3, max_sigma, sigma ) == false )
    cout << "###AdcCalibrate: " << bsystem.errString() << endl;
  cout << "sigma: ";
  for( int i=0; i<8; ++i ) cout << sigma[i] << " ";
  cout << endl;

  cin >> ch;

  cout << "Offs/Gains read #" << bsensor_i << endl;
  if( bsystem.bSensorOffsets( bsensor_i, regs ) )
    for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
  else
    cout << "###offs " << bsystem.errString() << endl;
  if( bsystem.bSensorGains( bsensor_i, regs ) )
    for( int i=0; i<4; ++i ) cout << setw(10) << regs[i];
  else
    cout << "###gain " << bsystem.errString() << endl;
  cout << endl;

#endif // ADCCALIBRATE_TEST

#ifdef TRIGINPUT_TEST
  cin >> ch;

  if( bsystem.setTriggerInputEnabled( 0, 3, true, false ) == false )
    cout << "###setTriggerInputEnabled(): " << bsystem.errString() << endl;
  else
    cout << "setTriggerInputEnabled()" << endl;

  for( int i=0; i<4; ++i )
    {
      cout << i << ": " << bsystem.triggerInputEnabled(0, i) << " "
	   << bsystem.triggerInputRisingEdge(0, i) << endl;
      cout << i << ": " << bsystem.triggerInputEnabled(1, i) << " "
	   << bsystem.triggerInputRisingEdge(1, i) << endl;
    }
#endif // TRIGINPUT_TEST

//#define READOUT_TEST
#ifdef READOUT_TEST
  cin >> ch;

  bsystem.setReadoutTriggerMsec( 8000 );
  int cnt = 0;
  while( 1 ) 
    {
      //while( !bsystem.bSensorDataAvailable() ) Sleep( 100 );
      //cout << "==> Data available..." << endl;
      //cout << "Timestamp: " << bsystem.dataTimeStampString( true ) << endl;
      if( bsystem.bSensorWaitDataAvailable( 5000 ) )
	{
	  cout << "B-sensor data " << cnt << endl;
	  ++cnt;
	  bsystem.bSensorDataRelease();
	}
      else
	{
	  cout << "###Time-out" << endl;
	}
    }
#endif // READOUT_TEST 

  bsystem.setCanPort( -1 ); // Close CAN interface
  cin >> ch;

  // C-interface test

  const char *teststr = "BsCan C-API: ";
  cout << teststr << BsCanVersion() << ", " << BsCanCalibConstFile() << endl;
  if( !BsCanSetCanPort( 2 ) )
    {
      cout << "BsCanSetCanPort(): " << BsCanErrString() << endl;
    }

  cout << "BsCanNodeCount(): " << BsCanNodeCount() << endl;
  cout << "BsCanBsensorCount(): " << BsCanBsensorCount(BSCAN_ALL) << endl;
  uint32_t bi = 23;
  cout << "BsCanBsensorIdString(" << bi << "): "
       << BsCanBsensorIdString( bi ) << endl;
  //BsCanClose();

  //bsystem.bSensorProbe();
  //bsystem.triggerSingleReadout();
  //Sleep( 1000 );
  //bsystem.triggerSingleReadout();
  //Sleep( 1000 );
  //bsystem.triggerSingleReadout();
  //while( bsystem.bSensorProbeInProgress() ) Sleep( 100 );

  //bsystem.getConfiguration();
  //Sleep( 3000 );
  //bsystem.getConfiguration( true );

  return 0;
}
