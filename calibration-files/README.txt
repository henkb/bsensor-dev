This directory contains all known -to me, Henk- calibration data files
(plus some derived(?) text files).

There may be duplicate B-sensor calibration data in these files;
find out e.g. by using tool 'bcaliblist'.

Henk B, 17 March 2024
