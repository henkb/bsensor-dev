#ifndef BSCAN3UI_H
#define BSCAN3UI_H

#include <QFile>
#include <QFileInfo>

#include "ui_bscan3ui.h"

class BsCan;

class BsCan3Ui: public QDialog, private Ui_BsCan3Ui
{
  Q_OBJECT

 public:
  BsCan3Ui();
  ~BsCan3Ui();

 private slots:
  void findCanPorts();
  void connectOrDisconnect();
  void startStopReadout();
  void startStopLogging();
  void setSamplesToLog();
  void selectCalibConstFile();
  void calibratedChanged();
  void processDataAndErrors();
  void clearDiagnostics();
  void hideDataLed();
  void hideErrLed();

 private:
  void closeEvent( QCloseEvent *ce );
  void startReadout();
  void stopReadout();
  void startLogging();
  void stopLogging();
  void displayConfig();
  void displayBsensorData( bool calibrated, bool gauss );
  void logConfig();
  void logData();

  void addDiagnostic( const QString &qs );
  void addDiagnostic( const char *str );

 private:
  BsCan *_bscan;

  // Palettes and brushes for okay/error color indications
  QBrush _qbOkay, _qbError;

  // Data logging
  QFile            _logFile;
  QFileInfo        _logFileInfo;
  QString          _convStartTimeStr;
  int              _loggedSamples, _samplesToLog;
  bool             _configLogged;
  QIntValidator   *_intValidator;
};

#endif // BSCAN3UI_H
