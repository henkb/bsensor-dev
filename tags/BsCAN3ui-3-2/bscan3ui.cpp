/* BsCAN3ui:
   program for reading out a BsCAN3 system, displaying the data (raw or
   calibrated) and optionally saving the data (raw or calibrated) to file.
   Similar to the 'old' BsCAN3ui program (made with VS6 and Qt v2.3.0),
   but completely rewritten, based on the BsCan class and using Qt v4.7.x
   and VS2008.

   Version 3.0, 14-MAY-2012 (Version 2.x.x was the old BsCAN3ui program)
   - First of version 3, tested on Windows with Kvaser module,
     on Linux not tested yet.

   Version 3.1, 29-AUG-2012
   - Using latest CANopen library (got rid of NewInterface.h).

   Version 3.2, 30-NOV-2012
   - Display BsCan library version used.
   - Display info about nodes found: number of B-sensors, firmware version.
   - More diagnostic output in case of node error.
   - Fix bug in Emergency display/clear.
*/

#include <math.h>

#include <CanInterface.h>
#include <bscan.h>

#include <QFileDialog>
#include <QMessageBox>
#include <QTime>
#include <QTimer>
#include <QTextStream>

#include "bscan3ui.h"

const QString VERSION( "v3.2  30-Nov-2012" );

// Table column indices
const int COL_I    = 0;
const int COL_X    = 1;
const int COL_Y    = 2;
const int COL_Z    = 3;
const int COL_B    = 4;
const int COL_T    = 5;
const int COL_BID  = 6;
const int COL_NID  = 7;

// ----------------------------------------------------------------------------

BsCan3Ui::BsCan3Ui()
  : QDialog()
{
  setupUi( this );

  this->setWindowFlags( Qt::WindowMinimizeButtonHint |
			Qt::WindowMaximizeButtonHint |
			Qt::WindowCloseButtonHint );

  this->labelVersion->setText( VERSION );

  this->addDiagnostic( QString("(Using lib ") +
		       QString::fromStdString( _bscan->version() ) +
		       QString(")") );

  // Fill the 'CAN port' combobox
  this->findCanPorts();

  // Connect buttons
  connect( this->pushButtonConnect, SIGNAL( clicked() ),
	   this, SLOT( connectOrDisconnect() ) );
  connect( this->pushButtonStartStopReadout, SIGNAL( clicked() ),
	   this, SLOT( startStopReadout() ) );
  connect( this->pushButtonSelectCalibConstFile, SIGNAL( clicked() ),
	   this, SLOT( selectCalibConstFile() ) );
  connect( this->pushButtonStartStopLog, SIGNAL( clicked() ),
	   this, SLOT( startStopLogging() ) );
  connect( this->pushButtonSetToLog, SIGNAL( clicked() ),
	   this, SLOT( setSamplesToLog() ) );
  connect( this->pushButtonClear, SIGNAL( clicked() ),
	   this, SLOT( clearDiagnostics() ) );
  connect( this->pushButtonExit, SIGNAL( clicked() ),
	   this, SLOT( close() ) );

  // Connect checkboxes
  connect( this->checkBoxCalibrated, SIGNAL( clicked() ),
	   this, SLOT( calibratedChanged() ) );

  // Connect line edits
  connect( this->lineEditToLog, SIGNAL( returnPressed() ),
	   this, SLOT( setSamplesToLog() ) );
  _intValidator = new QIntValidator( 0, 0x7FFFFFFF, this );
  this->lineEditToLog->setValidator( _intValidator );
  _samplesToLog = 0;
  _configLogged = false;

  // Instantiate a BsCAN3 system object (without connecting to a system yet)
  _bscan = new BsCan;

  // Brushes for selected/deselected color indications
  _qbOkay  = this->palette().brush( QPalette::Base );
  _qbError = QBrush( QColor("yellow") );

  // Initialize the table widget which will contain the B-sensor info
  QTableWidget *tbl = this->tableWidgetBsensors;
  tbl->setSelectionMode( QAbstractItemView::NoSelection );
  tbl->setColumnCount( 8 );
  QStringList labels;
  labels << " # " << "  Bx  " << "  By  " << "  Bz  " << " |B| "
	 << "  Temp [C]  " << " ID " << " NodeId ";
  tbl->setHorizontalHeaderLabels( labels );
  tbl->resizeColumnsToContents();
  tbl->resizeRowsToContents();
  tbl->setColumnHidden( COL_B, true );

  // Modify the horizontal table headers to stand out a bit more...
  QTableWidgetItem *hdr = tbl->horizontalHeaderItem( 0 );
  QFont qf = hdr->font();
  qf.setBold( true );
  for( int i=0; i<tbl->columnCount(); ++i )
    tbl->horizontalHeaderItem( i )->setFont( qf );

  this->lineEditDataLed->hide();
  this->lineEditErrLed->hide();
}

// ----------------------------------------------------------------------------

BsCan3Ui::~BsCan3Ui()
{
  if( _bscan ) delete _bscan;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::closeEvent( QCloseEvent *event )
{
  // When quitting the application deselect any B-sensor modules selected
  // and disconnect the CAN port
  if( _bscan->connected() ) this->connectOrDisconnect();
  event->accept();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::findCanPorts()
{
  this->comboBoxPortNr->clear();

  int portcnt;
  int *portnumbers = 0;
#ifdef WIN32
#ifdef USE_NICAN
  portcnt = CanInterface::canInterfacePorts( NICAN_INTF_TYPE, &portnumbers );
#else
  portcnt = CanInterface::canInterfacePorts( KVASER_INTF_TYPE, &portnumbers );
#endif // USE_NICAN
#else
  portcnt = CanInterface::canInterfacePorts( SOCKET_INTF_TYPE, &portnumbers );
#endif

  for( int i=0; i<portcnt; ++i )
    this->comboBoxPortNr->addItem( QString::number(portnumbers[i]), i );

  // Change widgets' status and/or appearance
#ifdef WIN32
#ifdef USE_NICAN
  this->addDiagnostic( QString("Found %1 NICAN CAN-ports").arg(portcnt) );
  this->groupBoxCanPort->setTitle( "CAN port (NI-CAN)" );
#else
  this->addDiagnostic( QString("Found %1 KVASER CAN-ports").arg(portcnt) );
  this->groupBoxCanPort->setTitle( "CAN port (KVASER)" );
#endif // USE_NICAN
#else
  this->addDiagnostic( QString("Found %1 SocketCAN CAN-ports").arg(portcnt) );
  this->groupBoxCanPort->setTitle( "CAN port (SocketCAN)" );
#endif
  if( portcnt > 0 )
    this->pushButtonConnect->setEnabled( true );
  else
    this->pushButtonConnect->setEnabled( false );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::connectOrDisconnect()
{
  // Connect to or disconnect from a BsCAN3 system on the selected CAN port
  if( _bscan->connected() )
    {
      // Deselect any selected B-sensor modules
      //_bscan->bSensorDeselect();

      // Disconnect (stops read-out)
      _bscan->setCanPort( -1 );

      // Change widgets' status and/or appearance
      this->pushButtonConnect->setText( "Connect" );
      this->comboBoxPortNr->setEnabled( true );
      this->pushButtonStartStopReadout->setText( "Start readout" );
      this->pushButtonStartStopReadout->setEnabled( false );
      this->pushButtonStartStopLog->setEnabled( true );
      this->spinBoxInterval->setEnabled( true );

      _configLogged = false;
    }
  else
    {
      // Connect to the BsCAN3 system on the requested CAN port
      int port_no = this->comboBoxPortNr->currentText().toInt();

      // Change widgets' status and/or appearance
      // (and show hourglass while busy...)
      this->pushButtonConnect->setText( "Disconnect" );
      this->comboBoxPortNr->setEnabled( false );
      QApplication::setOverrideCursor( Qt::WaitCursor );
      qApp->processEvents();

      // Connect (Windows: to Kvaser, Linux: to SocketCAN)
#ifdef USE_NICAN
      _bscan->setCanPort( port_no, NICAN_INTF_TYPE );
#else
      _bscan->setCanPort( port_no );
#endif // USE_NICAN

      // Display errors
      if( !_bscan->errString().empty() )
	this->addDiagnostic( QString("### CAN port %1: %2").arg( port_no ).
			     arg( _bscan->errString().c_str() ) );

      if( _bscan->connected() )
	{
	  // Change widgets' status and/or appearance
	  this->pushButtonStartStopReadout->setEnabled( true );
	  this->lineEditErrLed->hide();

	  // Show how many we found
	  this->addDiagnostic( QString("CAN port %1: "
				       "found %2 B-sensors on %3 nodes").
			       arg( port_no ).
			       arg( _bscan->bSensorCount() ).
			       arg( _bscan->nodeCount() ) );
	  for( unsigned int i=0; i<_bscan->nodeCount(); ++i )
	    {
	      QString version =
		QString::fromStdString( _bscan->nodeFirmwareVersion( i ) );
	      this->addDiagnostic( QString(" Node %1: %2 B-sensor(s), "
					   "firmw version \"%3\"").
				   arg( _bscan->nodeId(i), 3 ).
				   arg( _bscan->bSensorCount(i), 2 ).
				   arg( version ) );
	    }

	  // Show the B-sensor configuration found on this CAN port
	  this->displayConfig();

	  // Write (new?) configuration to logfile if required and possible
	  this->logConfig();

	  QTimer::singleShot( 0, this, SLOT( processDataAndErrors() ) );
	}
      else
	{
	  // Change widgets' status and/or appearance
	  this->pushButtonConnect->setText( "Connect" );
	  this->pushButtonConnect->setChecked( false );
	  this->comboBoxPortNr->setEnabled( true );
	}
      QApplication::restoreOverrideCursor();
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startStopReadout()
{
  if( _bscan->readoutTriggerMsec() == 0 )
    this->startReadout();
  else
    this->stopReadout();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startReadout()
{
  // Change widgets' status and/or appearance
  this->pushButtonStartStopReadout->setText( "Stop readout" );
  this->spinBoxInterval->setEnabled( false );
  this->pushButtonStartStopLog->setEnabled( false );

  if( _logFile.isOpen() )
    {
      // Simply add an empty line to indicate new start
      QTextStream stream( &_logFile );
      stream << QChar( '\n' );

      this->lineEditToLog->setText( QString::number(_samplesToLog) );

      // Don't do this here, may take a long time...
      //this->logConfig();
    }
  else
    {
      _loggedSamples = 0;
      this->lineEditLogged->setText( "0" );
      this->lineEditToLog->setText( QString::number(_samplesToLog) );
    }

  _bscan->setReadoutTriggerSec( this->spinBoxInterval->value() );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::stopReadout()
{
  _bscan->setReadoutTriggerSec( 0 );
  _bscan->bSensorDataRelease();

  // Change widgets' status and/or appearance
  this->pushButtonStartStopReadout->setText( "Start readout" );
  this->spinBoxInterval->setEnabled( true );
  this->pushButtonStartStopLog->setEnabled( true );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startStopLogging()
{
  if( _logFile.isOpen() )
    this->stopLogging();
  else
    this->startLogging();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::startLogging()
{
  QString filename =
    QFileDialog::getSaveFileName( this, "Open file for data logging",
				  _logFileInfo.filePath(),
				  "All files (*.*)", 0,
				  QFileDialog::DontConfirmOverwrite );
  if ( filename.isEmpty() )
    {
      this->pushButtonStartStopLog->setChecked( false );
      return;
    }

  _logFile.setFileName( filename );

  bool file_opened;
  if( _logFile.exists() )
    {
      // Ask: overwrite, append or cancel
      int result;
      result = QMessageBox::warning( this, "Data file",
				     "File exists, what would you like to do?",
				     "Overwrite", "Append", "Cancel", 1 );
      switch( result )
	{
	case 0:
	  file_opened = _logFile.open( QIODevice::WriteOnly ) ;
	  break;
	case 1:
	  file_opened = _logFile.open( QIODevice::WriteOnly |
				       QIODevice::Append ) ;
	  break;
	case 2:
	  this->pushButtonStartStopLog->setChecked( false );
	  return;
	}
    }
  else
    {
      file_opened = _logFile.open( QIODevice::WriteOnly ) ;
    }

  if( !file_opened )
    {
      this->pushButtonStartStopLog->setChecked( false );
      QMessageBox::critical( this, "Opening data file",
			     "Failed to open file" );
      return;
    }

  _logFileInfo.setFile( filename );

  QString qs( "Opened data file: " );
  qs += filename;
  this->addDiagnostic( qs );

  this->lineEditLogfile->setText( _logFileInfo.fileName() );
  this->lineEditLogfile->setEnabled( true );
  this->pushButtonStartStopLog->setText( QString("Stop Log") );

  _loggedSamples = 0;
  this->lineEditLogged->setText( "0" );
  this->lineEditToLog->setText( QString::number(_samplesToLog) );

  // Write the configuration to logfile if required
  this->logConfig();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::stopLogging()
{
  // Close log file
  if( _logFile.isOpen() )
    {
      _logFile.flush();
      _logFile.close();

      QString qs( "Closed data file: " );
      qs += _logFile.fileName();
      this->addDiagnostic( qs );
    }

  //this->lineEditLogfile->setText( QString( "" ) );
  this->lineEditLogfile->setEnabled( false );
  this->pushButtonStartStopLog->setText( QString( "Start Log" ) );

  _configLogged = false;
}

/* ------------------------------------------------------------------------ */

void BsCan3Ui::setSamplesToLog()
{
  if( this->lineEditToLog->text().isEmpty() )
    {
      this->lineEditToLog->setText( QString::number(_samplesToLog) );
    }
  else
    {
      _samplesToLog = this->lineEditToLog->text().toInt();
      this->addDiagnostic( QString("Set number of samples to log to %1").
			   arg( _samplesToLog ) );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::selectCalibConstFile()
{
  QString filename =
    QFileDialog::getOpenFileName( this, "Select calibration constants file",
				  "", "DAT files (*.dat);;All files (*.*)" );
  if( filename.isEmpty() ) return;

  _bscan->setCalibConstFile( filename.toStdString() );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::calibratedChanged()
{
  if( this->checkBoxCalibrated->isChecked() )
    {
      // Only show the |B| column if calibrated values are shown
      this->tableWidgetBsensors->setColumnHidden( COL_B, false );
      this->checkBoxGauss->setEnabled( true );
      // Let the user know which calibration constants file will be used
      this->addDiagnostic( QString( "Calib const filename: " ) +
			   QString( _bscan->calibConstFile().c_str() ) );
    }
  else
    {
      // Hide the |B| column
      this->tableWidgetBsensors->setColumnHidden( COL_B, true );
      this->checkBoxGauss->setEnabled( false );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::processDataAndErrors()
{
  // B-sensor data
  if( _bscan->bSensorDataAvailable() )
    {
      // Take into account that there might be just one B-sensor in total
      // that is in error...
      if( !(_bscan->bSensorCount() == 1 && _bscan->bSensorErrStatus(0)) )
	{
	  // Flash the green 'Data LED'
	  this->lineEditDataLed->show();
	  QTimer::singleShot( 500, this, SLOT( hideDataLed() ) );
	}

      this->displayBsensorData( this->checkBoxCalibrated->isChecked(),
				this->checkBoxGauss->isChecked() );

      // Node error status
      int err = _bscan->errStatus();
      if( err )
	{
	  for( unsigned int i=0; i<_bscan->nodeCount(); ++i )
	    if( _bscan->nodeErrStatus( i ) )
	      this->addDiagnostic( QString("BsCAN3 errstat CAN node %1: %2").
				   arg( _bscan->nodeId(i) ).
				   arg( _bscan->nodeErrStatus(i) ) );
	  this->addDiagnostic( QString("BsCAN3 last err descr: ")+
			       QString::fromStdString(_bscan->errString()) );

	  _bscan->clearErrStatus();
	}

      // B-sensor error status
      if( _bscan->bSensorError() )
	{
	  // Highlight 'bad' B-sensors on the display
	  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
	    if( _bscan->bSensorErrStatus(i) )
	      {
		// Highlight the table row showing this B-sensor's data
		for( int col=0; col<this->tableWidgetBsensors->columnCount();
		     ++col )
		  this->tableWidgetBsensors->item( i, col )->
		    setBackground( _qbError );
		this->lineEditErrLed->show();
	      }
	}

      qApp->processEvents();

      this->logData();

      // Data processed, ready for the next set of sampled data
      _bscan->bSensorDataRelease();
    }

  // CANopen Emergencies
  int cnt = _bscan->emgCount();
  if( cnt )
    {
      //this->addDiagnostic( QString("EMG cnt=%1").arg( cnt ) );
      for( int i=0; i<cnt; ++i )
	{
	  QString qs;
	  qs += QString( "EMG " );
	  qs += QString( _bscan->emgDescription(i).c_str() );
	  this->addDiagnostic( qs );
	}
      // Don't read and clear in the same loop, so do clearing here
      // (use only index '0', since we delete one at a time)
      for( int i=0; i<cnt; ++i ) _bscan->emgClear( 0 );
    }

  QTimer::singleShot( 100, this, SLOT( processDataAndErrors() ) );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::displayConfig()
{
  QTableWidget *tbl = this->tableWidgetBsensors;
  tbl->setRowCount( _bscan->bSensorCount() );

  // Fill the table with a list of B-sensor module information, and display
  QTableWidgetItem *item;
  QFont qf;
  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
    {
      // Index
      item = new QTableWidgetItem( QString::number(i+1) );
      item->setTextAlignment( Qt::AlignCenter );
      tbl->setItem( i, COL_I, item );

      // Node Identifier
      item = new QTableWidgetItem( QString::number(_bscan->bSensorNodeId(i)) );
      item->setTextAlignment( Qt::AlignCenter );
      tbl->setItem( i, COL_NID, item );

      // ID
      item = new QTableWidgetItem( QString(_bscan->
					   bSensorIdString(i).c_str()) );
      item->setTextAlignment( Qt::AlignCenter );
      if( i == 0 )
	{
	  // Initialize qf
	  qf = item->font();
	  qf.setFamily( "Courier" );
	}
      item->setFont( qf );
      tbl->setItem( i, COL_BID, item );

      // X, Y, Z, T
      for( int j=COL_X; j<=COL_T; ++j )
	{
	  item = new QTableWidgetItem( QString("") );
	  if( j == COL_T )
	    item->setTextAlignment( Qt::AlignCenter );
	  else
	    item->setTextAlignment( Qt::AlignRight|Qt::AlignVCenter );
	  tbl->setItem( i, j, item );
	}
    }

  //tbl->resizeColumnsToContents();
  tbl->setColumnWidth( COL_X, 80 );
  tbl->setColumnWidth( COL_Y, 80 );
  tbl->setColumnWidth( COL_Z, 80 );
  tbl->setColumnWidth( COL_B, 80 );
  tbl->setColumnWidth( COL_T, 70 );
  tbl->setColumnWidth( COL_BID, 150 );
  tbl->setColumnWidth( COL_NID, 60 );
  tbl->resizeRowsToContents();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::displayBsensorData( bool calibrated, bool gauss )
{
  QTableWidgetItem *item_x, *item_y, *item_z, *item_b, *item_t;
  if( calibrated )
    {
      int p; // For precision
      if( gauss )
	p = 1;
      else
	p = 6;
      for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
	{
	  double data[4];
	  int    err;
	  _bscan->bSensorDataCalibrated( data, &err, i );

	  item_x = this->tableWidgetBsensors->item( i, COL_X );
	  item_y = this->tableWidgetBsensors->item( i, COL_Y );
	  item_z = this->tableWidgetBsensors->item( i, COL_Z );
	  item_b = this->tableWidgetBsensors->item( i, COL_B );
	  item_t = this->tableWidgetBsensors->item( i, COL_T );

	  if( err )
	    {
	      QString qs = QString("###Err%1").arg( err );
	      item_x->setText( qs );
	      item_y->setText( qs );
	      item_z->setText( qs );
	      item_b->setText( QString("") );
	    }
	  else
	    {
	      if( gauss )
		{
		  data[0] *= 10000.0;
		  data[1] *= 10000.0;
		  data[2] *= 10000.0;
		}
	      item_x->setText( QString("%1").arg( data[0], 0, 'f', p ) );
	      item_y->setText( QString("%1").arg( data[1], 0, 'f', p ) );
	      item_z->setText( QString("%1").arg( data[2], 0, 'f', p ) );
	      double b = sqrt( data[0]*data[0] +
			       data[1]*data[1] + data[2]*data[2] );
	      item_b->setText( QString("%1").arg( b, 0, 'f', p ) );
	    }
	  // Temperature
	  item_t->setText( QString("%1").arg( data[3], 5, 'f', 2 ) );
	}
    }
  else
    {
      for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
	{
	  int32_t data[4];
	  _bscan->bSensorDataRaw( data, i );

	  item_x = this->tableWidgetBsensors->item( i, COL_X );
	  item_y = this->tableWidgetBsensors->item( i, COL_Y );
	  item_z = this->tableWidgetBsensors->item( i, COL_Z );
	  item_t = this->tableWidgetBsensors->item( i, COL_T );

	  // B-field
	  item_x->setText( QString("%1").arg( data[0] ) );
	  item_y->setText( QString("%1").arg( data[1] ) );
	  item_z->setText( QString("%1").arg( data[2] ) );

	  // Temperature
	  item_t->setText( QString("%1").
			   arg( (float) data[3]/1000.0, 5, 'f', 2 ) );
	}
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::logConfig()
{
  if( !_bscan->connected() ) return;
  if( !_logFile.isOpen() ) return;

  // Write the current B-sensor configuration to the logfile
  // only if not done already
  if( _configLogged ) return;

  // Reading offsets/gains may take a while...
  this->addDiagnostic( "Acquiring B-sensor config data for logfile..." );
  qApp->processEvents();
  QApplication::setOverrideCursor( Qt::WaitCursor );

  // Add an empty line to indicate a possibly new log start
  QTextStream stream( &_logFile );
  stream << QChar( '\n' );

  // Write the current configuration to the logfile
  QString      qs;
  unsigned int val, regs[4];
  for( unsigned int i=0; i<_bscan->bSensorCount(); ++i )
    {
      qs.clear();
      qs += QString( "CONFIG %1" ).arg( i+1, 3 );
      val = _bscan->bSensorNodeId( i );
      qs += QString( " Node %1" ).arg( val, 3 );
      val = _bscan->bSensorRemoteIndex( i );
      qs += QString( " String %1" ).arg( val/32 );
      qs += QString( " Rindex %1 : " ).arg( val, 3 );

      // B-sensor ID
      val = _bscan->bSensorIdHi( i );
      qs += QString::number( val, 16 ).toUpper().rightJustified( 8, '0' );
      val = _bscan->bSensorIdLo( i );
      qs += QString::number( val, 16 ).toUpper().rightJustified( 8, '0' );

      // ADC Offset register values
      qs += QString( "  Offset " );
      _bscan->bSensorOffsets( i, regs );
      for( int chan=0; chan<4; ++chan )
	qs += QString( "%1 " ).
	  arg( QString::number( regs[chan],16 ).toUpper(), 7 );

      // ADC Gain register values
      qs += QString( " Gain " );
      _bscan->bSensorGains( i, regs );
      for( int chan=0; chan<4; ++chan )
	qs += QString( "%1 " ).
	  arg( QString::number( regs[chan],16 ).toUpper(), 7 );

      qs += '\n';
      // Write the line to file
      stream << qs;
    }
  stream << flush;

  this->addDiagnostic( "Write B-sensor config to logfile done" );

  QApplication::restoreOverrideCursor();

  _configLogged = true;
}

// ----------------------------------------------------------------------------

void BsCan3Ui::logData()
{
  if( !_bscan->connected() ) return;
  if( !_logFile.isOpen() ) return;

  QString timestamp = QString( _bscan->dataTimeStampString( true ).c_str() );

  QTextStream stream( &_logFile );

  // At most the data of 'n' B-sensor modules per line (power of 2)
  const int LINESIZE = 8-1; // 8 B-sensors per line
  QString line;
  QTableWidgetItem *item_x, *item_y, *item_z, *item_t;
  unsigned int i;
  for( i=0; i<_bscan->bSensorCount(); ++i )
    {
      if( (i & LINESIZE) == 0 )
	{
	  // Start of a new line
	  line.clear();
	  line += timestamp;
	}

      line += QString("%1").arg( i+1, 6 );

      item_x = this->tableWidgetBsensors->item( i, COL_X );
      item_y = this->tableWidgetBsensors->item( i, COL_Y );
      item_z = this->tableWidgetBsensors->item( i, COL_Z );
      item_t = this->tableWidgetBsensors->item( i, COL_T );
      line += item_x->text().rightJustified( 10 );
      line += item_y->text().rightJustified( 10 );
      line += item_z->text().rightJustified( 10 );
      line += item_t->text().rightJustified( 10 );

      // Write the line to file if the number of B-sensors is reached
      if( ((i+1) & LINESIZE) == 0 )
	{
	  line += '\n';
	  stream << line;
	}
    }
  // Write the last line to file if not yet done
  if( (i & LINESIZE) != 0 )
    {
      line += '\n';
      stream << line;
    }
  stream << flush;

  ++_loggedSamples;
  this->lineEditLogged->setText( QString::number(_loggedSamples) );
  // Automatically stop data-acquisition if the requested number
  // of samples has been reached
  if( _samplesToLog > 0 && _loggedSamples >= _samplesToLog )
    {
      this->stopLogging();
      this->pushButtonStartStopLog->setChecked( false );
      this->stopReadout();
      this->pushButtonStartStopReadout->setChecked( false );
      QMessageBox::information( this->pushButtonStartStopLog,
				"Log file closed / readout stopped",
				"Requested number of samples reached" );
    }
}

// ----------------------------------------------------------------------------

void BsCan3Ui::addDiagnostic( const QString &qs )
{
  QString p;
  p =  QTime::currentTime().toString();
  p += QString( "  " );
  p += qs;
  this->plainTextEditDiagnostic->appendPlainText( p );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::addDiagnostic( const char *str )
{
  this->addDiagnostic( QString(str) );
}

// ----------------------------------------------------------------------------

void BsCan3Ui::clearDiagnostics()
{
  this->plainTextEditDiagnostic->clear();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::hideDataLed()
{
  this->lineEditDataLed->hide();
}

// ----------------------------------------------------------------------------

void BsCan3Ui::hideErrLed()
{
  this->lineEditErrLed->hide();
}

// ----------------------------------------------------------------------------
