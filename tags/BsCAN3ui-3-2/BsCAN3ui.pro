#
# Project file for the BsCAN3ui tool
#
# To generate a Visual Studio project:
#   qmake -t vcapp BsCAN3ui.pro
# To generate a Makefile:
#   qmake BsCAN3ui.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = app
TARGET   = BsCAN3ui

# Create a Qt app
CONFIG += qt thread warn_on exceptions debug_and_release

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  LIBS       += -L../Release
}

LIBS += -lBsCAN
LIBS += -lCANopen

INCLUDEPATH += ../CAN/CANopen
INCLUDEPATH += ../BsCAN

win32 {
  RC_FILE = bscan3ui.rc
}

FORMS    += bscan3ui.ui
RESOURCES = bscan3ui.qrc

SOURCES  += main.cpp
SOURCES  += bscan3ui.cpp
HEADERS  += bscan3ui.h
