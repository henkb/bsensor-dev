/* -------------------------------------------------------------------------
FILE   : bscanimpl.cpp

DESCR  : Class for interfacing to a system of (strings of) B-sensor modules
         connected to multiple BATCAN and/or mBATCAN modules connected to
         a single CAN-bus.
         The mBATCAN modules must run the socalled 'BATsCAN3' firmware,
         and the BATCAN modules must run BATCAN firmware version 1.1 or later
         to provide BATsCAN3-compatibility.
         This class is the actual implementation for the user class which
         will work by means of the 'pointer to implementation' idiom, to hide
         any implementation-specific include files and members from the user
         (see bscan.h and bscan.cpp).

HISTORY:
29MAR12; v1.0.0: Under development.
  APR12; Ongoing development.
  MAY12; Ongoing development.
17MAY12; Version 1.0.0.
23MAY12; Version 1.1.0.
         - Added bSensorAdcCalibrate() (public) member function.
         - Added bSensorResetSingle() (private) member function.
         - Added setBsensorOffsGains() (private) member function.
         - Added "invalid Node/B-sensor index" error messages.
18JUL12; Version 1.2.0.
         - Revised use of 'success' bool overall (start with success=true).
         - Added setTriggerInputEnabled() (public) member function,
           as well as triggerInputEnabled() and triggerInputRisingEdge().
         - Added blocking wait-for-data function bSensorWaitDataAvailable()
           with a time-out, and function bSensorWaitDataAbort() to cancel
           the wait (from another thread).
         - Added bSensorDataItemUpdate(), called from the CAN reading thread,
           to update one B-sensor reading and if appropriate signal the
           wait-for-data function.
30NOV12; Version 1.2.1.
         - CanInterface::port() has changed to CanInterface::portNumber().
	 - Increased time-out when deselecting devices in prepareNodes()
           due to new B-sensors with the DS2413 device.
	 - Do not clear the SDO time-out error bit in nodeErrStatus(),
           let the user decide.
         - Replace some OD_BSENSOR_DESELECT for OD_BSENSOR_SELECT,
           due to timing issues with B-sensors with DS2413,
           or increase time-out considerably for OD_BSENSOR_DESELECT.
         - Add local B-sensor index in emgDescription() where appropriate.
---------------------------------------------------------------------------- */

#include <math.h>
#ifdef DEBUG_OUTPUT
#include <iostream> // In case cout needs to be used for DEBUG output
#endif // DEBUG_OUTPUT
#include <iomanip>

#include <CANopen.h>
#include <CanInterface.h>
#include <CanNode.h>
#include <CanMessage.h>

#include "bscanimpl.h"
#include "canreadthread.h"
#include "syncthread.h"

const   string BsCanImpl::VERSION_STR = string( "BsCan v1.2.1 2012-11-30" );
//const string BsCanImpl::VERSION_STR = string( "BsCan v1.2.0 2012-07-18" );
//const string BsCanImpl::VERSION_STR = string( "BsCan v1.1.0 2012-05-23" );
//const string BsCanImpl::VERSION_STR = string( "BsCan v1.0.0 2012-05-17" );

// ----------------------------------------------------------------------------
// Constructors / destructor
// ----------------------------------------------------------------------------

BsCanImpl::BsCanImpl()
  : _canIntf(0),
    _refreshBsensorStatus(false),
    _refreshBsensorOffsGains(true),
    _errStatus(0),
    _canMsgReader(0),
    _syncGenerator(0)
{
  _canMsgReader  = new CanReadThread( this );
  _syncGenerator = new SyncThread;
}

// ----------------------------------------------------------------------------

BsCanImpl::BsCanImpl( int portno, int interface_type )
  : _canIntf(0),
    _refreshBsensorStatus(false),
    _refreshBsensorOffsGains(true),
    _errStatus(0),
    _canMsgReader(0),
    _syncGenerator(0)
{
  _canMsgReader  = new CanReadThread( this );
  _syncGenerator = new SyncThread;

  this->setCanPort( portno, interface_type );
}

// ----------------------------------------------------------------------------

BsCanImpl::~BsCanImpl()
{
  this->stop();
}

// ----------------------------------------------------------------------------

void BsCanImpl::stop()
{
  this->clearConfig();

  if( _canMsgReader )
    {
      _canMsgReader->stop(); // Rather stop thread outside d'tor
      delete _canMsgReader;
      _canMsgReader = 0;
    }

  if( _syncGenerator )
    {
      _syncGenerator->stop(); // Rather stop thread outside d'tor
      delete _syncGenerator;
      _syncGenerator = 0;
    }

  if( _canIntf )
  {
    delete _canIntf;
    _canIntf = 0;
  }
}

// ----------------------------------------------------------------------------
// Configuration
// ----------------------------------------------------------------------------

bool BsCanImpl::setCanPort( int portno, int interface_type )
{
  // Disable parallel CAN readout temporarily
  // to allow synchronous (send+receive) SDO operations
  _canMsgReader->pause();

  // Stop SYNCs (if any at all configured)
  //_syncGenerator->setInterval( 0 );
  this->setReadoutTriggerMsec( 0 ); // Properly cancels ongoing operations

  // Clear all configuration and data held up to now
  this->clearConfig();

  // Get rid of the currently opened interface, if any
  if( _canIntf ) delete _canIntf;
  _canIntf = 0;

  // If portno<0 we assume any opened port was to be closed
  // and no new one to be opened
  if( portno < 0 ) return true;

  // Instantiate and open the requested CAN-port of the requested type
  // (always at a bus rate of 125 kbit/s)
  //_canIntf = CanInterface::NewInterface( interface_type, portno, 125 );
  _canIntf = CanInterface::newCanInterface( interface_type, portno, 125 );

  if( _canIntf )
    {
      // Check whether the interface was properly opened
      if( _canIntf->opStatus() )
	{
	  this->errStringClear();
	  _errString << _canIntf->initResultString();
	  _errStatus |= BSCAN_ERR_CANINTF;
	  delete _canIntf;
	  _canIntf = 0;
	  return false;
	}
      // The SYNC generator should use the new interface
      _syncGenerator->setInterface( _canIntf );
    }
  else
    {
      this->errStringClear();
      _errString << "Failed to create CAN interface object, type "
		 << interface_type << endl;
      _errStatus |= BSCAN_ERR_CANINTF;
      return false;
    }

  _errStatus &= ~BSCAN_ERR_CANINTF;

  // Get the configuration of nodes and B-sensor modules
  this->findNodes();

  bool success = true;
  // Now get the B-sensor configuration from the nodes found
  if( this->getBsensorConfig() == false ) success = false;
  _refreshBsensorOffsGains = true; // Offsets/gains not yet read out

  // Ready the nodes for data acquisition
  if( this->prepareNodes() == false ) success = false;

  // Re-enable parallel CAN message readout and processing
  _canMsgReader->resume();

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::connected()
{
  return( _canIntf != 0 );
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getConfiguration( bool include_offsgains )
{
  // (Re)read the B-sensor configuration of the current system of CAN nodes
  if( !_canIntf ) return false;

  // Disable parallel CAN readout temporarily
  // to allow synchronous (send+receive) SDO operations
  _canMsgReader->pause();

  bool success = this->getBsensorConfig( include_offsgains );
  if( include_offsgains )
    _refreshBsensorOffsGains = false;
  else
    _refreshBsensorOffsGains = true;

  // Re-enable parallel CAN message readout and processing
  _canMsgReader->resume();

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorProbe( uint32_t node_i )
{
  if( !_canIntf ) return false;

  // Probe for B-sensors on all connected CAN nodes (when node_i==BSCAN_ALL)
  // or only on the CAN node with index i,
  // and store the new configuration(s) onboard
  bool     success = true;
  CanNode *node;
  uint32_t start_i, end_i;

  if( node_i == BSCAN_ALL )
    {
      // All nodes
      start_i = 0;
      end_i   = this->nodeCount();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // A single node
      start_i = node_i;
      end_i   = node_i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }

  for( uint32_t n=start_i; n<end_i; ++n )
    {
      node = _nodeInfo[n].pNode;

      node->sdoCancel(); // Just in case of a pending reset/probe

      // Send the probe request; the reply may take quite some time;
      // it gets handled in the read thread
      if( node->sdoReadRequest( OD_BSENSOR_PROBE, 0, 60000 ) )
	{
	  _nodeInfo[n].bProbeInProgress = true;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Request probe, Node " << node->nodeId()
		     << " index " << n
		     << ": " << node->errString();
	  _nodeInfo[n].errStatus |= BSCAN_ERR_COMM;
	}
    }

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorReset( uint32_t node_i )
{
  if( !_canIntf ) return false;

  // Send a request for reset of B-sensors on all connected CAN nodes
  // (when node_i==BSCAN_ALL) or only on the CAN node with index 'i'
  bool     success = true;
  CanNode *node;
  int      od_index;
  uint32_t start_i, end_i;

  if( node_i == BSCAN_ALL )
    {
      // All nodes
      start_i = 0;
      end_i   = this->nodeCount();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // A single node
      start_i = node_i;
      end_i   = node_i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }

  int no_of_bytes = 1, data = 0xAB;
  for( uint32_t n=start_i; n<end_i; ++n )
    {
      node = _nodeInfo[n].pNode;

      if( _nodeInfo[n].bTotal > 1 )
	od_index = OD_BSENSOR_RESET_ALL;
      else
	od_index = OD_BSENSOR_RESET;

      node->sdoCancel(); // Just in case of a pending reset/probe

      // Send the reset request
      if( node->sdoWriteExpeditedRequest( od_index, 0,
					  no_of_bytes, data, 60000 ) )
	{
	  _nodeInfo[n].bResetInProgress = true;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Request B-reset, Node " << node->nodeId()
		     << " index " << n
		     << ": " << node->errString();
	  if( node->sdoTimeout() )
	    _nodeInfo[n].errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    _nodeInfo[n].errStatus |= BSCAN_ERR_COMM;
	}
    }

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorProbeInProgress( uint32_t node_i )
{
  // Check if the reply or replies to a probe request have been received;
  // when node_i==BSCAN_ALL for all CAN nodes or only on the CAN node
  // with index 'node_i'
  bool     in_progress = false;
  uint32_t start_i, end_i;

  if( node_i == BSCAN_ALL )
    {
      // All nodes
      start_i = 0;
      end_i   = this->nodeCount();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // A single node
      start_i = node_i;
      end_i   = node_i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }

  for( uint32_t n=start_i; n<end_i; ++n )
    if( _nodeInfo[n].bProbeInProgress ) in_progress = true;

  return in_progress;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorResetInProgress( uint32_t node_i )
{
  // Check if the reply or replies to a reset request have been received;
  // when node_i==BSCAN_ALL for all CAN nodes or only on the CAN node
  // with index 'node_i'
  bool     in_progress = false;
  uint32_t start_i, end_i;

  if( node_i == BSCAN_ALL )
    {
      // All nodes
      start_i = 0;
      end_i   = this->nodeCount();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // A single node
      start_i = node_i;
      end_i   = node_i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }

  for( uint32_t n=start_i; n<end_i; ++n )
    if( _nodeInfo[n].bResetInProgress ) in_progress = true;

  return in_progress;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorProbeAtPowerup( uint32_t node_i )
{
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      return _nodeInfo[node_i].bProbeAtPowerup;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::setBsensorProbeAtPowerup( uint32_t node_i, bool enable )
{
  bool success = true;
  int no_of_bytes = 1;
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      CanNode *node = _nodeInfo[node_i].pNode;

      // Disable parallel CAN readout temporarily
      // to allow synchronous (send+receive) SDO operations
      _canMsgReader->pause();

      // Set the probe-at-powerup parameter
      if( node->sdoWriteExpedited( OD_BSENSOR_RST_PROBE, 0, no_of_bytes,
				   static_cast<int> (enable), 100 ) )
	{
	  // Update (local) parameter as well
	  _nodeInfo[node_i].bProbeAtPowerup = enable;

	  // Save the settings to EEPROM
	  success = node->saveConfig();
	  if( success == false )
	    {
	      this->errStringClear();
	      _errString << "Save setting probe-at-powerup, Node " << node_i
			 << ": " << node->errString();
	    }
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Set probe-at-powerup, Node " << node_i
		     << ": " << node->errString();
	}

      // Re-enable parallel CAN message readout and processing
      _canMsgReader->resume();
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      success = false;
    }
  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::triggerInputEnabled( uint32_t node_i,
				     uint32_t trigger_input )
{
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // There are 4 possible trigger inputs (but only on mBATCAN)
      if( trigger_input < 4 )
	return( _nodeInfo[node_i].trigInputConfig & (1<<trigger_input) );
      else
	return false;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::triggerInputRisingEdge( uint32_t node_i,
					uint32_t trigger_input )
{
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // There are 4 possible trigger inputs (but only on mBATCAN)
      if( trigger_input < 4 )
	return( _nodeInfo[node_i].trigInputConfig & ((1<<trigger_input)<<4) );
      else
	return false;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::setTriggerInputEnabled( uint32_t node_i,
					uint32_t trigger_input,
					bool     enable,
					bool     rising_edge )
{
  bool success = true;
  int no_of_bytes = 1;
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      if( !_nodeInfo[node_i].isMbatcan )
	{
	  this->errStringClear();
	  _errString << "Node #" << node_i << " is not an mBATCAN";
	  return false;
	}

      CanNode *node = _nodeInfo[node_i].pNode;

      // Disable parallel CAN readout temporarily
      // to allow synchronous (send+receive) SDO operations
      _canMsgReader->pause();

      // Set trigger input's rising-edge parameter
      if( node->sdoWriteExpedited( OD_BSENSOR_TRIGGER + trigger_input,
				   2, no_of_bytes,
				   static_cast<int> (rising_edge),
				   100 ) == false )
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Set rising-edge trigger input, Node " << node_i
		     << ": " << node->errString();
	}
      else
	{
	  // Update (local) trigger input configuration as well
	  if( rising_edge )
	    _nodeInfo[node_i].trigInputConfig |= ((1<<trigger_input) << 4);
	  else
	    _nodeInfo[node_i].trigInputConfig &= ~((1<<trigger_input) << 4);
	}

      // Enable or disable the trigger input
      if( success &&
	  node->sdoWriteExpedited( OD_BSENSOR_TRIGGER + trigger_input,
				   1, no_of_bytes,
				   static_cast<int> (enable), 100 ) )
	{
	  // Update (local) trigger input configuration as well
	  if( enable )
	    _nodeInfo[node_i].trigInputConfig |= ((1<<trigger_input) << 0);
	  else
	    _nodeInfo[node_i].trigInputConfig &= ~((1<<trigger_input) << 0);

	  // Save the settings to EEPROM
	  success = node->saveConfig();
	  if( success == false )
	    {
	      this->errStringClear();
	      _errString << "Save trigger input settings, Node " << node_i
			 << ": " << node->errString();
	    }
	}
      else
	{
	  if( success )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Enable/disable trigger input, Node " << node_i
			 << ": " << node->errString();
	    }
	}

      // Re-enable parallel CAN message readout and processing
      _canMsgReader->resume();
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      success = false;
    }
  return success;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::nodeCount()
{
  // Return the number of B-sensor CAN nodes (BATCAN plus mBATCAN)
  return _nodeInfo.size();
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorCount( uint32_t node_i )
{
  // Return the total number of B-sensors in this system i.e. connected to
  // this CAN port, if node_i==BSCAN_ALL, or return the number of B-sensors
  // connected to the CAN node with index 'node_i'
  uint32_t result = 0;
  if( node_i == BSCAN_ALL )
    {
      result = _bSensorInfo.size();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() > 0 )
    {
      result = _nodeInfo[node_i].bTotal;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
    }
  return result;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorStringCount( uint32_t node_i, uint32_t str_no )
{
  // Return the number of B-sensors on string 'str_no' connected to
  // the CAN node with index 'node_i'
  uint32_t result = 0;
  if( node_i < this->nodeCount() && this->nodeCount() > 0 &&
      str_no < BSCAN_STRINGS_PER_NODE )
    {
      result = _nodeInfo[node_i].bInString[str_no];
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
    }
  return result;
}

// ----------------------------------------------------------------------------

int BsCanImpl::bSensorFirstIndex( uint32_t node_i )
{
  // Return the index into _bSensorInfo of the first B-sensor of
  // the node with index 'node_i'
  if( node_i < this->nodeCount() && this->nodeCount() > 0 )
    {
      // Find the first string of this node containing B-sensors,
      // then the corresponding bStringIndex[] entry is the wanted index
      for( int str=0; str<BSCAN_STRINGS_PER_NODE; ++str )
	if( _nodeInfo[node_i].bInString[str] )
	  return _nodeInfo[node_i].bStringIndex[str];
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
    }
  return -1;
}

// ----------------------------------------------------------------------------

int BsCanImpl::nodeId( uint32_t node_i )
{
  // Return the node identifier of CAN node with index 'node_i' in our list
  int result = -1;
  if( node_i < this->nodeCount() && this->nodeCount() > 0 )
    {
      result = _nodeInfo[node_i].pNode->nodeId();
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
    }
  return result;
}

// ----------------------------------------------------------------------------

string BsCanImpl::nodeFirmwareVersion( uint32_t node_i )
{
  // Return a node's firmware version
  if( node_i < this->nodeCount() && this->nodeCount() > 0 )
    {
      return _nodeInfo[node_i].firmwareVersion;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return string( "" );
    }
}

// ----------------------------------------------------------------------------

int BsCanImpl::bSensorNodeId( uint32_t i )
{
  // Returns the node identifier of the CAN node
  // the B-sensor with index 'i' connects to
  int result = -1;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      result = _bSensorInfo[i].pNodeInfo->pNode->nodeId();
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

int BsCanImpl::bSensorRemoteIndex( uint32_t i )
{
  // Returns the (remote) index on the CAN node of B-sensor with index 'i'
  int result = -1;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      result = _bSensorInfo[i].remoteIndex;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorIdHi( uint32_t i )
{
  // Returns the 4 upper bytes of B-sensor 'i's ID as a 32-bit integer
  uint32_t result = 0;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      result = _bSensorInfo[i].idHi;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorIdLo( uint32_t i )
{
  // Returns the 4 lower bytes of B-sensor 'i's ID as a 32-bit integer
  uint32_t result = 0;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      result = _bSensorInfo[i].idLo;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorId( uint32_t i, uint8_t *id )
{
  // Return B-sensor 'i's ID as a byte array (8 bytes)
  bool result = false;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      BsensorInfo *b_info = &_bSensorInfo[i];
      for( int byt=0; byt<8; ++byt ) id[byt] = b_info->id[byt];
      result = true;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

string BsCanImpl::bSensorIdString( uint32_t i )
{
  // Return B-sensor 'i's ID as a string
  ostringstream oss;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      BsensorInfo *b_info = &_bSensorInfo[i];
      oss << hex << setfill('0') << uppercase;
      for( int j=0; j<8; ++j )
	oss << setw(2) << static_cast<int> (b_info->id[j]);
      return oss.str();
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
      return string( "" );
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorOffsets( uint32_t i, uint32_t *offs )
{
  bool result = false;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      // If necessary, first read out the Offsets from the nodes
      if( _refreshBsensorOffsGains ) this->getBsensorOffsGains();

      BsensorInfo *b_info = &_bSensorInfo[i];
      for( int chan=0; chan<4; ++chan )
	{
	  offs[chan] = b_info->offs[chan];
	  // Consider all values equal to zero as an error
	  // (this is set by getBsensorOffsGains() in case of error)
	  if( offs[chan] != 0 ) result = true;
	}
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorGains( uint32_t i, uint32_t *gain )
{
  bool result = false;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      // If necessary, first read out the Gains from the nodes
      if( _refreshBsensorOffsGains ) this->getBsensorOffsGains();

      BsensorInfo *b_info = &_bSensorInfo[i];
      for( int chan=0; chan<4; ++chan )
	{
	  gain[chan] = b_info->gain[chan];
	  // Consider all values equal to zero as an error
	  // (this is set by getBsensorOffsGains() in case of error)
	  if( gain[chan] != 0 ) result = true;
	}
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorErrStatus( uint32_t i )
{
  // Returns the error status of B-sensor with index 'i'
  uint32_t result = 0;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      result = _bSensorInfo[i].errStatus;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return result;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorError()
{
  // Check for any B-sensor status unequal to 0
  for( uint32_t b=0; b<this->bSensorCount(); ++b )
    if( _bSensorInfo[b].errStatus != 0 )
      return true;
  return false;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getBsensorErrStatus( uint32_t i )
{
  bool success = true;
  uint32_t start_i, end_i;
  if( i == BSCAN_ALL )
    {
      // All B-sensors
      start_i = 0;
      end_i   = this->bSensorCount();
    }
  else if( i < this->bSensorCount() && this->bSensorCount() )
    {
      // A single B-sensor
      start_i = i;
      end_i   = i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
      return false;
    }

  // Disable parallel CAN readout temporarily
  // to allow synchronous (send+receive) SDO operations
  _canMsgReader->pause();

  BsensorInfo *b_info;
  NodeInfo    *n_info;
  CanNode     *node;
  int          no_of_bytes, data;
  for( uint32_t b=start_i; b<end_i; ++b )
    {
      b_info = &_bSensorInfo[b];
      n_info = b_info->pNodeInfo;
      node   = n_info->pNode;
      if( node->sdoReadExpedited( OD_BSENSOR_STATUS + b_info->remoteIndex, 0,
				  &no_of_bytes, &data, 100 ) )
	{
	  b_info->errStatus = data;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Read B-sensor status, Node " << node->nodeId()
		     << " index " << b
		     << ": " << node->sdoStatusString()
		     << ", " << node->errString();

	  if( node->sdoTimeout() )
	    n_info->errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info->errStatus |= BSCAN_ERR_COMM;
	}
    }

  this->adjustBindexHi();

  // Re-enable parallel CAN message readout and processing
  _canMsgReader->resume();

  // Reset the 'request' boolean (in case it was set)
  _refreshBsensorStatus = false;

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getBsensorOffsGains( uint32_t i )
{
  bool success = true;
  uint32_t start_i, end_i;
  if( i == BSCAN_ALL )
    {
      // All B-sensors
      start_i = 0;
      end_i   = this->bSensorCount();
      _refreshBsensorOffsGains = false;
    }
  else if( i < this->bSensorCount() && this->bSensorCount() )
    {
      // A single B-sensor
      start_i = i;
      end_i   = i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
      return false;
    }

  // Disable parallel CAN readout temporarily
  // to allow synchronous (send+receive) SDO operations
  _canMsgReader->pause();

  for( uint32_t b=start_i; b<end_i; ++b )
    {
      if( this->getBsensorOffsGains( _bSensorInfo[b] ) == false )
	success = false;
    }

  // Re-enable parallel CAN message readout and processing
  _canMsgReader->resume();

  return success;
}

// ----------------------------------------------------------------------------

string BsCanImpl::configSummaryString()
{
  // Return the complete configuration as a string (containing multiple lines)
  // meant for showing on a screen or writing to a (log)file
  ostringstream oss;

  oss << "==> BsCAN system: ";
  if( !_canIntf )
    {
      oss << "none configured" << endl;
      return oss.str();
    }

  oss << _canIntf->manufacturerName() << " Port " << _canIntf->portNumber()
      << ", " << this->nodeCount() << " Nodes, "
      << this->bSensorCount() << " B-sensors" << endl;

  for( uint32_t n=0; n<this->nodeCount(); ++n )
    {
      string version = this->nodeFirmwareVersion( n );
      oss << " Node " << setw(3) << this->nodeId( n ) << ": Firmw=" << version
	  << ", " << setw(2) << this->bSensorCount( n ) << " B-sensor";
      if( this->bSensorCount( n ) != 1 )
	oss << "s";
      else
	oss << " ";
      oss << " (";
      for( int str=0; str<BSCAN_STRINGS_PER_NODE-1; ++str )
	oss << setw(2) << this->bSensorStringCount( n, str ) << ", ";
      oss << setw(2) << this->bSensorStringCount( n, BSCAN_STRINGS_PER_NODE-1 )
	  << ") ";
      //if( version[1] == 's' || version[1] == 'S' ) // If a BATsCAN module..
      if( _nodeInfo[n].isMbatcan ) // If a BATsCAN module..
	oss << "PowerupProbe=" << _nodeInfo[n].bProbeAtPowerup;
      //oss << " ###" << _nodeInfo[n].fwVersionNr; // DEBUG output
      oss << endl;
    }

  BsensorInfo *b_info;
  for( uint32_t i=0; i<this->bSensorCount(); ++i )
    {
      b_info = &_bSensorInfo[i];
      oss << " B #" << setw(2) << setfill('0') << i << " at (" << setfill(' ')
	  << setw(3) << b_info->pNodeInfo->pNode->nodeId() << ", "
	  << setw(2) << b_info->remoteIndex << "): "
	  << "ID=" << this->bSensorIdString( i )
	  << " Stat=" << setw(2) << setfill('0') << b_info->errStatus
	  << endl;
    }

  return oss.str();
}

// ----------------------------------------------------------------------------
// Data-acquisition
// ----------------------------------------------------------------------------

void BsCanImpl::setReadoutTriggerSec( int secs )
{
  this->setReadoutTriggerMsec( secs*1000 );
}

// ----------------------------------------------------------------------------

void BsCanImpl::setReadoutTriggerMsec( int msecs )
{
  if( _canIntf && msecs == 0 && _syncGenerator->interval() != 0 )
    {
      // To cancel any ongoing ADC conversions
      _canIntf->nmt( NMT_ENTER_PREOPERATIONAL_STATE );
      // And set the nodes back to Operational state
      for( uint32_t i=0; i<this->nodeCount(); ++i )
	// Set this particular CANopen node back to Operational state
	_nodeInfo[i].pNode->nmt( NMT_START_REMOTE_NODE );
      // Make sure..
      for( uint32_t i=this->nodeCount(); i>0; --i )
	_nodeInfo[i-1].pNode->nmt( NMT_START_REMOTE_NODE );
    }
  _syncGenerator->setInterval( msecs );
}

// ----------------------------------------------------------------------------

int BsCanImpl::readoutTriggerMsec()
{
  return _syncGenerator->interval();
}

// ----------------------------------------------------------------------------

void BsCanImpl::triggerSingleReadout()
{
  // Send a CANopen SYNC message
  if( _canIntf ) _canIntf->sendSync();

  // If necessary (re)read B-sensor statuses first
  if( _refreshBsensorStatus ) this->getBsensorErrStatus();

  // Let the SYNC generator take a timestamp
  _syncGenerator->takeTimeStamp();
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorDataAvailable( uint32_t node_i )
{
  if( !_canIntf ) return false;

  // Check if the reply or replies to a reset request have been received;
  // when node_i==BSCAN_ALL for all CAN nodes or only on the CAN node with
  // index 'i'
  bool     data_available = true;
  uint32_t start_i, end_i;

  if( node_i == BSCAN_ALL && this->nodeCount() > 0 )
    {
      // All nodes
      start_i = 0;
      end_i   = this->nodeCount();
    }
  else if( node_i < this->nodeCount() && this->nodeCount() > 0 )
    {
      // A single node
      start_i = node_i;
      end_i   = node_i + 1;
    }
  else
    {
      // Invalid index
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
      return false;
    }

  // When a node has zero B-sensors declare it to have 'data available';
  // and also when a node has a single B-sensor which is in error state
  // to allow data-acquisition to continue
  // (but what if it's the only one...?)
  for( uint32_t n=start_i; n<end_i; ++n )
    if( !_nodeInfo[n].bDataAvailable )
      {
	if( !(_nodeInfo[n].bTotal == 0 ||
	      (_nodeInfo[n].bTotal == 1 &&
	       _bSensorInfo[this->bSensorFirstIndex(n)].errStatus != 0)) )
	  data_available = false;
      }

  return data_available;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorWaitDataAvailable( uint32_t timeout_ms )
{
  if( !_canIntf ) return false;

  bool not_timeout;

  _mutex.lock();
  not_timeout = _condDataAvailable.wait( &_mutex, timeout_ms );
  _mutex.unlock();

  // Check for time-out, if not not, check if data is really available
  if( not_timeout )
    return this->bSensorDataAvailable();
  else
    return false;
}

// ----------------------------------------------------------------------------

void BsCanImpl::bSensorWaitDataAbort()
{
  // Abort any ongoing bSensorWaitDataAvailable() call
  // (to be called from another thread)
 _condDataAvailable.wakeAll();
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorDataRaw( int32_t *data, uint32_t i )
{
  uint32_t sz = 0;
  if( i == BSCAN_ALL )
    {
      // Copies all B-sensor data -if available- to 'data';
      // temperature sensor values are included too (in millidegrees)
      if( this->bSensorDataAvailable() )
	{
	  struct BsensorData *bdata;
	  for( uint32_t b=0; b<_bSensorData.size(); ++b )
	    {
	      bdata = &_bSensorData[b];
	      *data = bdata->hall[0]; ++data;
	      *data = bdata->hall[1]; ++data;
	      *data = bdata->hall[2]; ++data;
	      *data = bdata->t;       ++data;
	    }
	  sz = _bSensorData.size()*4;
	}
    }
  else if( i < _bSensorData.size() )
    {
      // Copies the data of B-sensor with index 'i'
      // (irrespective of validity) to 'data'
      struct BsensorData *bdata = &_bSensorData[i];
      *data = bdata->hall[0]; ++data;
      *data = bdata->hall[1]; ++data;
      *data = bdata->hall[2]; ++data;
      *data = bdata->t;
      sz = 4;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  // Return the number of datawords stored in 'data'
  return sz;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorDataCalibrated( double  *data,
					   int32_t *err,
					   uint32_t i )
{
  uint32_t sz = 0;
  *err = 0;
  if( i == BSCAN_ALL )
    {
      // Copies all B-sensor Hall data -if available- to 'data'
      // converted into calibrated physical values;
      // temperature sensor values are included (in degrees)
      if( this->bSensorDataAvailable() )
	{
	  struct BsensorData *bdata;
	  for( uint32_t b=0; b<_bSensorData.size(); ++b )
	    {
	      bdata = &_bSensorData[b];

	      // Convert the raw Hall sensor readings (chan 0,1 and 2)
	      // to calibrated physical values in units of Tesla
	      char   *bidcal  = (char *) &_bSensorInfo[b].id[0];
	      int    *bval    = bdata->hall;
	      double *bvalcal = bdata->hallCalib;
	      double temp     = (double) bdata->t/1000.0; // Temp in degrees
	      int    info;
	      _calibCalc.calvtb( bidcal, bval[0], bval[1], bval[2], temp,
				 bvalcal[0], bvalcal[1], bvalcal[2],
				 info );
	      // Extract the status/error bits
	      info &= 0x0F;
	      if( !(info == 1 || info == 2 || info == 3) )
		*err |= info; // Error != 0 means something went wrong

	      // Copy the converted data values
	      *data = bdata->hallCalib[0]; ++data;
	      *data = bdata->hallCalib[1]; ++data;
	      *data = bdata->hallCalib[2]; ++data;
	      *data = temp;                ++data;
	    }
	  sz = _bSensorData.size()*4;
	}
    }
  else if( i < _bSensorData.size() )
    {
      // Copies the Hall data of B-sensor with index 'i' to 'data'
      // (irrespective of validity) converted into calibrated physical values
      struct BsensorData *bdata = &_bSensorData[i];

      // Convert the raw Hall sensor readings (chan 0,1 and 2)
      // to calibrated physical values in units of Tesla
      char   *bidcal  = (char *) &_bSensorInfo[i].id[0];
      int    *bval    = bdata->hall;
      double *bvalcal = bdata->hallCalib;
      double temp     = (double) bdata->t/1000.0; // Temp in degrees
      int    info;
      _calibCalc.calvtb( bidcal, bval[0], bval[1], bval[2], temp,
			 bvalcal[0], bvalcal[1], bvalcal[2],
			 info );
      // Extract the status/error bits
      info &= 0x0F;
      if( !(info == 1 || info == 2 || info == 3) )
	*err |= info; // Error != 0 means something went wrong

      // Copy the converted data values
      *data = bdata->hallCalib[0]; ++data;
      *data = bdata->hallCalib[1]; ++data;
      *data = bdata->hallCalib[2]; ++data;
      *data = temp;

      sz = 4;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  // Return the number of datawords stored in 'data'
  return sz;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorDataHallRaw( int32_t *data )
{
  // Copies all B-sensor Hall data -if available- to 'data'
  uint32_t sz = 0;
  if( this->bSensorDataAvailable() )
    {
      struct BsensorData *bdata;
      for( uint32_t b=0; b<_bSensorData.size(); ++b )
	{
	  bdata = &_bSensorData[b];
	  *data = bdata->hall[0]; ++data;
	  *data = bdata->hall[1]; ++data;
	  *data = bdata->hall[2]; ++data;
	}
      sz = _bSensorData.size()*3;
    }
  // Return the number of datawords stored in 'data'
  return 0;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::bSensorDataTemperature( int32_t *data )
{
  // Copies all B-sensor temperatures -if available- to 'data'
  // (temperatures are in millidegrees centigrade)
  uint32_t sz = 0;
  if( this->bSensorDataAvailable() )
    {
      for( uint32_t b=0; b<_bSensorData.size(); ++b )
	{
	  *data = _bSensorData[b].t;
	  ++data;
	}
      sz = _bSensorData.size();
    }
  // Return the number of datawords stored in 'data'
  return sz;
}

// ----------------------------------------------------------------------------

void BsCanImpl::bSensorDataRelease()
{
  // Indicate that data has been read and processed,
  // allowing new data to be acquired

  // Invalidate the data
  for( uint32_t b=0; b<_bSensorData.size(); ++b )
    {
      struct BsensorData *bdata = &_bSensorData[b];
      bdata->hall[0]      = 0;
      bdata->hall[1]      = 0;
      bdata->hall[2]      = 0;
      bdata->t            = 0;
      bdata->hallCalib[0] = 0.0;
      bdata->hallCalib[1] = 0.0;
      bdata->hallCalib[2] = 0.0;
    }

  // Set data-available booleans to false
  for( uint32_t n=0; n<this->nodeCount(); ++n )
    _nodeInfo[n].bDataAvailable = false;

  // If necessary (re)read B-sensor statuses first
  if( _refreshBsensorStatus ) this->getBsensorErrStatus();

  // Allow the SYNC generator to generate a next SYNC
  _syncGenerator->setEnabled( true );
}

// ----------------------------------------------------------------------------

string BsCanImpl::dataTimeStampString( bool include_date )
{
  if( include_date )
    return _syncGenerator->dateTimeStampString();
  else
    return _syncGenerator->timeStampString();
}

// ----------------------------------------------------------------------------

long long BsCanImpl::dataTimeStampMsec()
{
  return _syncGenerator->timeStampMsec();
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorDataItemUpdate( uint32_t node_i,
				       int      b_remote_i,
				       int      chan,
				       int      val )
{
  // Find location of this B-sensor in our local data structure
  // on the basis of the B-sensor remote index and the node index
  bool found = false;
  int  str_no = b_remote_i / BSCAN_MAX_STRING_SZ;
  int  i      = b_remote_i - (str_no * BSCAN_MAX_STRING_SZ);
  // The B-sensor local index into _bSensorInfo[]
  int bindex = _nodeInfo[node_i].bStringIndex[str_no] + i;

  // Check if we've got the expected sensor, by checking the remote index
  if( _bSensorInfo[bindex].remoteIndex == b_remote_i )
    found = true;

  if( found )
    {
      // Store just the 'raw' value
      // (calibrated values are calculated only on demand)
      if( chan <= 2 && chan >= 0 )
	_bSensorData[bindex].hall[chan] = val;
      else if( chan == 3 )
	_bSensorData[bindex].t = val;

      // All B-sensor data received from this node ?
      if( b_remote_i == _nodeInfo[node_i].bIndexHi && chan == 3 )
	{
	  // Yes, so indicate it
	  _nodeInfo[node_i].bDataAvailable = true;

	  // If appropriate signal the data-available condition
	  if( this->bSensorDataAvailable() ) _condDataAvailable.wakeOne();
	}
    }

  return found;
}

// ----------------------------------------------------------------------------
// Various
// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorAdcCalibrate( uint32_t i,
				     uint32_t calib_cnt,
				     double   max_sigma,
				     double  *sigma,
				     bool     include_tsensor )
{
  // This function will execute 'calib_cnt' resets on B-sensor 'i',
  // read the resulting ADC Offset and Gain register values and
  // determine an average and sigma for each one, which will then
  // be written in the B-sensor ADC registers, provided the calculated
  // sigma falls within the given limit 'max_sigma'.
  //
  // 'Sigma', if unequal to 0, will receive a copy of the sigma's calculated
  // for each of the 4 ADC channels' Offsets and Gains
  // (in the order offset0,gain0,offset1,gain1,etc.
  //
  // Optionally the T-sensor channel (the 4th) can be left out of
  // the calculations (include_tsensor == false).

  // lsigma[] is used to compile the sigmas locally
  double lsigma[8];
  for( int n=0; n<2*4; ++n ) lsigma[n] = 0.0;

  if( calib_cnt == 0 )
    {
      this->errStringClear();
      _errString << "B-sensor ADC calibration: sample count == 0 ";
      return false;
    }

  if( !(i < this->bSensorCount() && this->bSensorCount()) )
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
      return false;
    }

  // Disable parallel CAN readout temporarily
  // to allow synchronous (send+receive) SDO operations
  _canMsgReader->pause();

  double sum_offs[4], sum2_offs[4];
  double sum_gain[4], sum2_gain[4];
  for( int chan=0; chan<4; ++chan )
    {
      sum_offs[chan] = 0.0; sum2_offs[chan] = 0.0;
      sum_gain[chan] = 0.0; sum2_gain[chan] = 0.0;
    }

  // Do we include the T-sensor ADC channel in the procedure or not?
  int sz = 4;
  if( !include_tsensor ) sz = 3;

  // Perform 'n' B-sensor resets to obtain 'n' Offset and Gain register values
  BsensorInfo *b_info = &_bSensorInfo[i];
  double       val;
  int32_t      val_i;
  bool         pause_reader = false; // Already paused
  for( uint32_t rst=0; rst<calib_cnt; ++rst )
    {
      // Do a B-sensor reset plus subsequent offsets/gains read-out
      if( this->bSensorResetSingle( i, pause_reader ) == false )
	return false;

      // Process the newly determined and obtained Offsets and Gains,
      // i.e. compile a sum of the samples and a sum of the samples squared
      // NB: Offset is in fact a signed 24-bit number,
      //     Gain an unsigned 24-bit number
      for( int chan=0; chan<sz; ++chan )
	{
	  val_i =  b_info->offs[chan]; // Turn into a signed int...
	  if( val_i & 0x00800000 ) val_i |= 0xFF000000; // Extend sign bit
	  val = (double) val_i;
	  sum_offs[chan]  += val;
	  sum2_offs[chan] += val * val;
	  val = (double) b_info->gain[chan];
	  sum_gain[chan]  += val;
	  sum2_gain[chan] += val * val;
#ifdef DEBUG_OUTPUT
	  std::cout << chan << ": offs/gain "
		    << val_i << ", " << (unsigned int) val << endl;
#endif // DEBUG_OUTPUT
	}
    }

  // Calculate average and sigma; if calculated sigma > max_sigma reject,
  // if sigma <= max_sigma accept and write averages to the B-sensor's
  // ADC Offset and Gain registers
  //
  // The sigma of a population is the square-root of the average of the squares
  // of the samples Xi in the population minus their average squared:
  //   sigma = sqrt( sum(Xi**2)/n - (sum(Xi)/n)**2 )
  double dcnt = (double) calib_cnt;
  for( int chan=0; chan<sz; ++chan )
    {
      // Offset sigma
      lsigma[2*chan]   = sqrt( sum2_offs[chan]/dcnt -
			       (sum_offs[chan]/dcnt)*(sum_offs[chan]/dcnt) );
      // Gain sigma
      lsigma[2*chan+1] = sqrt( sum2_gain[chan]/dcnt -
			       (sum_gain[chan]/dcnt)*(sum_gain[chan]/dcnt) );
    }

  // Check if the sigmas all fall within the given limit 'max_sigma'
  bool success = true;
  for( int n=0; n<2*sz; ++n )
    if( lsigma[n] > max_sigma )
      {
	success = false;
	this->errStringClear();
	_errString << "ADC-calibration B-sensor index " << i;
	if( n & 1 )
	  _errString << ": gain ";
	else
	  _errString << ": offs ";
	_errString << "chan " << n/2 << " sigma="
		   << lsigma[n] << " > " << max_sigma;
	break;
      }

  // Copy the sigma values to the user if he wants them
  if( sigma )
    {
      int n;
      for( n=0; n<2*sz; ++n )   sigma[n] = lsigma[n];
      for( n=2*sz; n<2*4; ++n ) sigma[n] = 0;
    }

  if( success )
    {
      // Write the calculated average Offset and Gain register settings
      // to the B-sensor's ADC
      int32_t offs_avg[4], gain_avg[4];
      for( int chan=0; chan<sz; ++chan )
	{
	  // Offset/gain rounded to the nearest integer
	  offs_avg[chan] = (int32_t) (sum_offs[chan]/dcnt + 0.5);
	  gain_avg[chan] = (int32_t) (sum_gain[chan]/dcnt + 0.5);
#ifdef DEBUG_OUTPUT
	  std::cout << chan << ": offs/gain avg "
		    << offs_avg[chan] << ", " << gain_avg[chan] << endl;
#endif // DEBUG_OUTPUT
	}
      // Check the values
      for( int chan=0; chan<sz; ++chan )
	{
	  // Offset should fit in 24 bits
	  if( offs_avg[chan] > (int32_t) 0x00FFFFFF ||
	      offs_avg[chan] < (int32_t) 0xFF800000 )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "ADC-calibration B-sensor index " << i
			 << "offs: " << "chan " << chan
			 << " offs=" << hex << offs_avg[chan];
	    }
	  // Gain should be positive and fit in 24 bits
	  if( gain_avg[chan] > (int32_t) 0x00FFFFFF ||
	      gain_avg[chan] < (int32_t) 0x00000000 )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "ADC-calibration B-sensor index " << i
			 << " gain: " << "chan " << chan
			 << " gain=" << hex << gain_avg[chan];
	    }
	}
      if( success )
	{
	  // Turn the average offsets into 24-bit signed values
	  // stored in an unsigned integer and
	  // the average gains also into 24-bit (but unsigned) values
	  uint32_t offs_avg_u[4], gain_avg_u[4];
	  for( int chan=0; chan<sz; ++chan )
	    {
	      offs_avg_u[chan] = (uint32_t) (offs_avg[chan] & 0x00FFFFFF);
	      gain_avg_u[chan] = (uint32_t) (gain_avg[chan] & 0x00FFFFFF);
	    }
	  // Write the values to the B-sensor module
	  success = this->setBsensorOffsGains( i, offs_avg_u, gain_avg_u, sz );
	}
    }

  // Re-enable parallel CAN message readout and processing
  _canMsgReader->resume();

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorSelect( uint32_t i )
{
  // Select the single B-sensor with index 'i':
  // its LED will switch on and stay on until deselected
  // (don't wait for the SDO reply)
  bool success = true;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      BsensorInfo *b_info = &_bSensorInfo[i];
      NodeInfo    *n_info = b_info->pNodeInfo;
      CanNode     *node   = n_info->pNode;

      // Send the select request; we don't wait for a reply here...
      if( node->sdoReadRequest( OD_BSENSOR_SELECT,
				b_info->remoteIndex, 100 ) == false )
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Request select B-sensor " << i
		     << ", Node " << node->nodeId()
		     << " index " << b_info->remoteIndex
		     << ": " << node->errString();

	  n_info->errStatus |= BSCAN_ERR_COMM;
	}
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorSelectSync( uint32_t i )
{
  // Select the single B-sensor with index 'i':
  // its LED will switch on and stay on until deselected
  // (wait for the SDO reply)
  bool success = true;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      BsensorInfo *b_info = &_bSensorInfo[i];
      NodeInfo    *n_info = b_info->pNodeInfo;
      CanNode     *node   = n_info->pNode;
      int          no_of_bytes, data;

      // Disable parallel CAN readout temporarily
      // to allow synchronous (send+receive) SDO operations
      _canMsgReader->pause();

      // Do the selection request
      if( node->sdoReadExpedited( OD_BSENSOR_SELECT, b_info->remoteIndex,
				  &no_of_bytes, &data, 100 ) == false )
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Select B-sensor " << i
		     << ", Node " << node->nodeId()
		     << " index " << b_info->remoteIndex
		     << ": " << node->errString();

	  if( node->sdoTimeout() )
	    n_info->errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info->errStatus |= BSCAN_ERR_COMM;
	}

      // Re-enable parallel CAN message readout and processing
      _canMsgReader->resume();
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorDeselect( uint32_t i )
{
  bool success = true;
  CanNode *node;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      // Deselect B-sensors on the node with index 'i'
      // is connected to
      BsensorInfo *b_info = &_bSensorInfo[i];
      int          rindex  = b_info->remoteIndex;
      NodeInfo    *n_info = b_info->pNodeInfo;
      node = n_info->pNode;
      // Send the (de)select request; we don't wait for a reply here...
      // (30 Nov 2012: use OD_BSENSOR_SELECT rather than OD_BSENSOR_DESELECT,
      //  because the DS2413 device does not have an 'active search ROM'
      //  commmand)
      //if( node->sdoReadRequest( OD_BSENSOR_DESELECT, 0, 100 ) == false )
      if( node->sdoReadRequest( OD_BSENSOR_SELECT, rindex, 100 ) == false )
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Request deselect B-sensors"
		     << ", Node " << node->nodeId()
		     << ": " << node->errString();

	  n_info->errStatus |= BSCAN_ERR_COMM;
	}
    }
  else
    {
      // Deselect any selected B-sensors on all nodes
      NodeInfo *n_info;
      for( uint32_t n=0; n<this->nodeCount(); ++n )
	{
	  n_info = &_nodeInfo[n];
	  node   = n_info->pNode;

	  // Send the select request; we don't wait for a reply here...
	  // (30 Nov 2012: increased time-out to 2000, because the DS2413
	  //  device does not have an 'active search ROM' command
	  //  as the DS2405 does)
	  if( node->sdoReadRequest( OD_BSENSOR_DESELECT, 0, 2000 ) == false )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Request deselect B-sensors"
			 << ", Node " << node->nodeId()
			 << ": " << node->errString();

	      n_info->errStatus |= BSCAN_ERR_COMM;
	    }
	}
    }
  return success;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::nodeErrStatus( uint32_t node_i )
{
  if( node_i < this->nodeCount() && this->nodeCount() )
    {
      // Check for a possible asynchronous SDO time-out
      NodeInfo *n_info = &_nodeInfo[node_i];
      CanNode  *node   = n_info->pNode;
      if( node->sdoTimeout() )
	{
	  this->errStringClear();
	  _errString << "CAN comm time-out"
		     << ", Node " << node->nodeId();
	  n_info->errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	}
      else
	{
	  // 30 Nov 2012: do not clear the SDO time-out error bit,
	  // let the user decide..
	  //n_info->errStatus &= ~BSCAN_ERR_COMM_TIMEOUT;
	}
      return n_info->errStatus;
    }
  else
    {
      this->errStringClear();
      _errString << "Invalid Node index " << node_i;
    }
  return BSCAN_ERR_UNDEF;
}

// ----------------------------------------------------------------------------

uint32_t BsCanImpl::errStatus()
{
  // OR the current error status with the error status from all nodes
  // and return the resulting error status
  for( uint32_t n=0; n<this->nodeCount(); ++n )
    //_errStatus |= _nodeInfo[n].errStatus;
    _errStatus |= this->nodeErrStatus( n );
  return _errStatus;
}

// ----------------------------------------------------------------------------

void BsCanImpl::clearErrStatus()
{
  // Clears the current error statuses of the known nodes
  // and of the BsCan object itself, so not the B-sensor error statuses
  for( uint32_t n=0; n<this->nodeCount(); ++n )
    _nodeInfo[n].errStatus = 0;
  _errStatus = 0;
  this->errStringClear();
}

// ----------------------------------------------------------------------------

bool BsCanImpl::emgClear( uint32_t i )
{
  bool success = true;
  list<EmgInfo>::iterator it;

  // Pause CAN message read-out to allow exclusive access to _emg
  if( _canMsgReader ) _canMsgReader->pause();

  if( i == BSCAN_ALL )
    {
      // Delete all CAN message objects
      for( it=_emg.begin(); it!=_emg.end(); ++it )
	delete (*it).emgMsg;
      // Clear the list
      _emg.clear();
    }
  else if( i < _emg.size() )
    {
      it = _emg.begin();
      advance( it, i );
      // Delete the CAN message object
      delete (*it).emgMsg;
      // Remove the EmgInfo object from the list
      _emg.erase( it );
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Invalid Emergency index " << i;
    }

  if( _canMsgReader ) _canMsgReader->resume();

  return success;
}

// ----------------------------------------------------------------------------

string BsCanImpl::emgTimeStampString( uint32_t i, bool include_date )
{
  if( i >= _emg.size() )
    {
      this->errStringClear();
      _errString << "Invalid Emergency index " << i;
      return string( "" );
    }

  list<EmgInfo>::iterator it = _emg.begin();
  advance( it, i );

  if( include_date )
    return (*it).timeStamp.toString( QString("yyyy-MM-dd "
					     "hh:mm:ss:zzz") ).toStdString();
  else
    return (*it).timeStamp.toString( QString("hh:mm:ss:zzz") ).toStdString();
}

// ----------------------------------------------------------------------------

string BsCanImpl::emgDataString( uint32_t i )
{
  if( i >= _emg.size() )
    {
      this->errStringClear();
      _errString << "Invalid Emergency index " << i;
      return string( "" );
    }

  list<EmgInfo>::iterator it = _emg.begin();
  advance( it, i );
  CanMessage *msg = (*it).emgMsg;
  uint8_t    *data = msg->data();
  // For the time being just put the Error Code followed
  // by the manufacturer-specific bytes in the string
  ostringstream oss;
  oss << "ID=" << setw(3) << msg->nodeId()
      << hex << uppercase << setfill('0')
      << " ErrCod=" << setw(2) << static_cast<uint32_t> (data[1])
      << setw(2) << static_cast<uint32_t> (data[0])
      << ",";
  for( int j=3; j<=7; ++j )
    oss << " " << setw(2) << static_cast<uint32_t> (data[j]);
  return oss.str();
}

// ----------------------------------------------------------------------------

string BsCanImpl::emgDescription( uint32_t i )
{
  if( i >= _emg.size() )
    {
      this->errStringClear();
      _errString << "Invalid Emergency index " << i;
      return string( "" );
    }

  list<EmgInfo>::iterator it = _emg.begin();
  advance( it, i );
  CanMessage *msg = (*it).emgMsg;
  uint8_t *data = msg->data();
  uint32_t errcode = (static_cast<uint32_t> (data[0]) |
		      (static_cast<uint32_t> (data[1]) << 8));
  uint8_t *errbytes = &data[3];
  int val = static_cast<int> (errbytes[1]); // For various usage below

  int b, bscan_i = -1;
  // Find the local B-sensor index 'bscan_i' (appropriate for some EMGs...)
  b = this->bSensorFirstIndex( _nodeIdMap[msg->nodeId()] );
  if( b > -1 )
    for( ; b<(int)this->bSensorCount(); ++b )
      if( this->bSensorNodeId(b) == msg->nodeId() &&
	  this->bSensorRemoteIndex(b) == val )
	{
	  bscan_i = b;
	  break;
	}

  ostringstream oss;
  oss << "Node=" << setw(2) << msg->nodeId() << ": ";
  switch( errcode )
    {
    case 0x5000:
      switch( errbytes[0] )
        {
        case 0x30:
          oss << "Flash CRC error";
          break;

        case 0x41:
        case 0x42:
          if( errbytes[0] == 0x41 )
            oss << "EEPROM write error, par block ";
          else
            oss << "EEPROM read error, par block ";
          oss << static_cast<uint32_t> (errbytes[1])
	      << ", err "
	      << static_cast<uint32_t> (errbytes[2]);
	  break;

        case 0x51:
          oss << "B-index " << val;
	  if( bscan_i > -1 ) oss << " (" << bscan_i << ")";
	  oss << ", ADC chan "
	      << static_cast<uint32_t> (errbytes[2])
	      << ", conversion time-out";
          break;

        case 0x52:
        case 0x53:
        case 0x54:
	  oss << "B-index " << val;
	  if( bscan_i > -1 ) oss << " (" << bscan_i << ")";
	  if( errbytes[0] == 0x52 )
	    oss << ", ADC reset failed";
	  else if( errbytes[0] == 0x53 )
	    oss << ", Hall calib failed";
	  else
	    oss << ", T-sensor calib failed";
          break;

        case 0x55:
          oss << "B-sensor init problems, ";
          if( errbytes[1] == 0xFF )
	    {
	      oss << "no configuration found";
	    }
          else
	    {
	      oss << "on " << val << " module";
	      if( errbytes[1] > 1 ) oss << "s";
	    }
          break;

        case 0x56:
	  oss << "B-index " << val;
	  if( bscan_i > -1 ) oss << " (" << bscan_i << ")";
	  oss << ", invalid Gain found (hi/mi byte): "
	      << hex << uppercase << setfill('0')
	      << setw(2) << static_cast<uint32_t> (errbytes[2])
	      << setw(2) << static_cast<uint32_t> (errbytes[3]);
          break;

        case 0x57:
          oss << "B-index " << val;
	  if( bscan_i > -1 ) oss << " (" << bscan_i << ")";
	  oss << ", corrupted xcheck ADC reg val?: " << hex << uppercase
	      << static_cast<uint32_t> (errbytes[2]);
          break;

        case 0x58:
	  if( errbytes[2] == 0 )
	    {
	      oss << val << " B-sensor(s) deselected unexpectedly";
	    }
	  else
	    {
	      oss << "B-index " << val;
	      if( bscan_i > -1 ) oss << " (" << bscan_i << ")";
	      oss << ", (de)select error";
	    }
	  break;

	case 0xF0:
          oss << "Irregular reset: ";
	  if( errbytes[1] & 0x01 ) oss << "Power-on  ";
	  if( errbytes[1] & 0x02 ) oss << "External  ";
	  if( errbytes[1] & 0x04 ) oss << "Brown-out  ";
          if( errbytes[1] & 0x08 ) oss << "Watchdog  ";
          if( errbytes[1] & 0x10 ) oss << "JTAG";
          break;

        case 0xF1:
          oss << "No Bootloader";
          break;

        case 0xFE:
          oss << "Bootloader ";
          if( errbytes[1] == 0x80 ) oss << "in control";
          else if( errbytes[1] == 0xAA ) oss << ": no user app";
          break;

	default:
	  oss << "(no description)";
          break;
        }
      break;

    case 0x8100:
      oss << "CAN communication, data " << hex << setfill('0') << uppercase;
      for( int i=0; i<5; ++i )
        {
	  oss << setw(2) << static_cast<uint32_t> (errbytes[i]);
          if( i != 7 ) oss << ", ";
	}
      break;

    case 0x8110:
      oss << "CAN message buffer (RAM) overrun";
      break;

    case 0x8130:
      oss << "Life Guarding time-out, CAN-controller reinit";
      break;

    case 0x8210:
      oss << "too few RPDO bytes, minimum=";
      oss << static_cast<uint32_t> (errbytes[0]);
      break;

    default:
      oss << "(no description)";
      break;
    }
  return oss.str();
}

// ----------------------------------------------------------------------------
// Private / protected methods
// ----------------------------------------------------------------------------

void BsCanImpl::findNodes()
{
  // Scans for BATsCAN3(-compatible) CAN nodes
  // NB: call this function only with _canMsgReader in paused state !
  if( !_canIntf ) return;

  // Initialize the node identifier map we use
  for( int i=0; i<128; ++i ) _nodeIdMap[i] = 128;

  int nodes = _canIntf->scanBus();
  for( int i=0; i<nodes; ++i )
    {
      // Get the version string
      string version_str = _canIntf->scannedSwVersion( i );

      // Is this BATsCAN, BsCAN or BATCAN firmware ?
      // (mBATCAN modules should have BATsCAN firmware version 3.1.3 or later,
      //  BATCAN modules should have firmware version 1.1.1 or later,
      //  MDT-DCS modules with BsCAN firmware should have firmware
      //  version 3.0 or later)
      if( (version_str[0] == 'B' &&
	   (version_str[1] == 'S' || version_str[1] == 's') &&
	   version_str[2] == '3') ||
	  (version_str[0] == 'B' && version_str[1] == 'C') )
	{
	  // It's a BsCAN3, BATsCAN or BATCAN

	  // Add it to our list of B-sensor CAN nodes
	  CanNode *node = new CanNode( _canIntf, _canIntf->scannedNodeId(i) );

	  // Create the B-sensor CAN node info objects
	  NodeInfo n_info;
	  n_info.pNode = node;
	  _nodeInfo.push_back( n_info );

	  // Remember this node identifier's index in _nodeInfo
	  _nodeIdMap[_canIntf->scannedNodeId(i)] = _nodeInfo.size() - 1;
	}
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getBsensorConfig( bool include_offsgains )
{
  // NB: call this function only with _canMsgReader in paused state !

  // Read the full B-sensor configuration of the current system of CAN nodes
  bool success = true;

  // Clear the B-sensor info and data arrays
  _bSensorInfo.clear();
  _bSensorData.clear();

  NodeInfo *n_info;
  for( uint32_t i=0; i<this->nodeCount(); ++i )
    {
      n_info = &_nodeInfo[i];

      // Reset a number of node error bits
      n_info->errStatus &= ~(BSCAN_ERR_COMM | BSCAN_ERR_SENSOR);

      if( this->getNodeInfo( *n_info ) )
	{
	  // Collect/create the B-sensors info, data and objects
	  BsensorData b_data = { {0,0,0}, 0, {0.0,0.0,0.0}};
	  BsensorInfo b_info;
	  for( int str=0; str<BSCAN_STRINGS_PER_NODE; ++str )
	    {
	      // CAN node B-sensor index of first B-sensor in this string
	      int offset_i = str * BSCAN_MAX_STRING_SZ;

	      int nsensors = n_info->bInString[str];

	      // If there are sensors they will be appended
	      // to the current _bSensorInfo array, so the 'start' index
	      // of the current string is the current array size
	      if( nsensors > 0 )
		n_info->bStringIndex[str] = this->bSensorCount();

	      // Get the info from the B-sensors in this string
	      for( int sensor_i=0; sensor_i<nsensors; ++sensor_i )
		{
		  // Initialize some stuff
		  b_info.pNodeInfo   = n_info;
		  b_info.remoteIndex = offset_i + sensor_i;
		  b_info.errStatus   = 0;
		  b_info.idLo        = 0;
		  b_info.idHi        = 0;
		  for( int i=0; i<8; ++i ) b_info.id[i] = 0;

		  if( this->getBsensorInfo( b_info,
					    include_offsgains ) == false )
		    {
		      // Find out from the node which B-sensor(s) failed
		      n_info->errStatus |= BSCAN_ERR_SENSOR;
		      success = false;
		    }
		  _bSensorInfo.push_back( b_info );
		  _bSensorData.push_back( b_data );
		}
	    }
	}
      else
	{
	  // Find out from the nodes which one(s) failed
	  success = false;
	}
    }

  this->adjustBindexHi();

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getNodeInfo( NodeInfo &n_info )
{
  // NB: call this function only with _canMsgReader in paused state !
  if( !_canIntf ) return false;

  bool     success = true;
  int      i, no_of_bytes, data;
  CanNode *node = n_info.pNode;

  node->getFirmwareVersion( n_info.firmwareVersion );
  n_info.errStatus        = 0;
  n_info.bIndexHi         = 0;
  n_info.bTotal           = 0;
  n_info.bProbeAtPowerup  = false;
  n_info.trigInputConfig  = 0;
  n_info.bDataAvailable   = false;
  n_info.bProbeInProgress = false;
  n_info.bResetInProgress = false;

  // Is the node an mBATCAN ?
  if( n_info.firmwareVersion[1] == 's' || n_info.firmwareVersion[1] == 'S' )
    n_info.isMbatcan = true;
  else
    n_info.isMbatcan = false;
  // Extract the firmware version number
  n_info.fwVersionNr = 0;
  n_info.fwVersionNr += 100 * (uint32_t) (n_info.firmwareVersion[2] - '0');
  n_info.fwVersionNr +=  10 * (uint32_t) (n_info.firmwareVersion[3] - '0');
  n_info.fwVersionNr +=   1 * (uint32_t) (n_info.firmwareVersion[8] - '0');

  // The total number of B-sensors connected to this node
  if( node->sdoReadExpedited( OD_BSENSOR_LIST, 0,
			      &no_of_bytes, &data, 100 ) )
    {
      n_info.bTotal = data;
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Reading Node " << node->nodeId() << " info: "
		 << node->sdoStatusString() << ", " << node->errString();
      if( node->sdoTimeout() )
	n_info.errStatus |= BSCAN_ERR_COMM_TIMEOUT;
      else
	n_info.errStatus |= BSCAN_ERR_COMM;
    }

  // The number of B-sensors connected to this node's 4 strings
  for( i=0; i<BSCAN_STRINGS_PER_NODE; ++i )
    {
      n_info.bInString[i]    = 0;
      n_info.bStringIndex[i] = -1;
      if( node->sdoReadExpedited( OD_BSENSOR_STRING, i+1,
				  &no_of_bytes, &data, 100 ) )
	{
	  n_info.bInString[i] = data;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Reading Node " << node->nodeId() << " info: "
		     << node->sdoStatusString() << ", " << node->errString();
	  if( node->sdoTimeout() )
	    n_info.errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info.errStatus |= BSCAN_ERR_COMM;
	}

      // Adjust the highest expected B-sensor index for this node
      if( n_info.bInString[i] > 0 )
	n_info.bIndexHi = i * BSCAN_MAX_STRING_SZ + n_info.bInString[i] - 1;
    }

  // Ask BATsCAN module-specific information
  if( n_info.isMbatcan )
    {
      // If they always probe for B-sensors at power-up
      if( node->sdoReadExpedited( OD_BSENSOR_RST_PROBE, 0,
				  &no_of_bytes, &data, 100 ) )
	{
	  n_info.bProbeAtPowerup = data;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Reading Node " << node->nodeId() << " info: "
		     << node->sdoStatusString() << ", " << node->errString();
	  if( node->sdoTimeout() )
	    n_info.errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info.errStatus |= BSCAN_ERR_COMM;
	}

      // What their trigger input configuration is
      if( node->sdoReadExpedited( OD_BSENSOR_TRIGGER + 4, 0,
				  &no_of_bytes, &data, 100 ) )
	{
	  n_info.trigInputConfig = data;
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Reading Node " << node->nodeId() << " info: "
		     << node->sdoStatusString() << ", " << node->errString();
	  if( node->sdoTimeout() )
	    n_info.errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info.errStatus |= BSCAN_ERR_COMM;
	}
    }

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getBsensorInfo( BsensorInfo &b_info,
				bool         include_offsgains )
{
  // NB: call this function only with _canMsgReader in paused state !
  int      no_of_bytes, data;
  bool     success = true;
  CanNode *node    = b_info.pNodeInfo->pNode;
  int      rindex  = b_info.remoteIndex;

  // Get the B-sensor's error status
  if( node->sdoReadExpedited( OD_BSENSOR_STATUS + rindex, 0,
			      &no_of_bytes, &data, 100 ) )
    {
      // B-sensor error status as received from the node
      b_info.errStatus = data;
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Reading B-sensor info, Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  // Get the B-sensor's 64-bit ID: lower 4 bytes
  if( node->sdoReadExpedited( OD_BSENSOR_ID | rindex, 1,
			      &no_of_bytes, &data, 100 ) )
    {
      b_info.idLo = data;
      b_info.id[7] = (uint8_t) ((data & 0x000000FF) >> 0);
      b_info.id[6] = (uint8_t) ((data & 0x0000FF00) >> 8);
      b_info.id[5] = (uint8_t) ((data & 0x00FF0000) >> 16);
      b_info.id[4] = (uint8_t) ((data & 0xFF000000) >> 24);
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Reading B-sensor ID, Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  // Get the B-sensor's 64-bit ID: upper 4 bytes
  if( node->sdoReadExpedited( OD_BSENSOR_ID | rindex, 2,
			      &no_of_bytes, &data, 100 ) )
    {
      b_info.idHi = data;
      b_info.id[3] = (uint8_t) ((data & 0x000000FF) >> 0);
      b_info.id[2] = (uint8_t) ((data & 0x0000FF00) >> 8);
      b_info.id[1] = (uint8_t) ((data & 0x00FF0000) >> 16);
      b_info.id[0] = (uint8_t) ((data & 0xFF000000) >> 24);
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Reading B-sensor ID, Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  if( success && include_offsgains )
    // Get the B-sensor's Offset and Gain register contents
    success = this->getBsensorOffsGains( b_info );

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::getBsensorOffsGains( BsensorInfo &b_info )
{
  // Get the B-sensor's Offset and Gain register contents
  // NB: call this function only with _canMsgReader in paused state !
  NodeInfo *n_info  = b_info.pNodeInfo;
  CanNode  *node    = n_info->pNode;
  int       rindex  = b_info.remoteIndex;
  bool      success = true;
  int       no_of_bytes, data;
  for( int chan=0; chan<4; ++chan )
    {
      b_info.offs[chan] = 0;
      b_info.gain[chan] = 0;
    }

  // A faster way of register reading is supported by BATSCAN v3.1.3 and later,
  // and BATCAN v1.1.1 and later
  bool faster_regread_supported = false;
  if( (n_info->isMbatcan && n_info->fwVersionNr >= 313) ||
      (!n_info->isMbatcan && n_info->fwVersionNr >= 111) )
    faster_regread_supported = true;

  // Select the B-sensor ADC (BATCAN modules must have version 1.1.1 or later
  // to be compatible with BATsCAN3 firmware objects, to support the following)
  if( faster_regread_supported &&
      node->sdoReadExpedited( OD_BSENSOR_SELECT, rindex,
			      &no_of_bytes, &data, 100 ) == false )
    {
      success = false;
      this->errStringClear();
      _errString << "Reading B-sensor regs (select), Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  if( success )
    {
      // Read 4 offset and gain pairs
      int adc_config_obj_offs = 10; // For BATCAN version < 1.1.1
      if( faster_regread_supported ) adc_config_obj_offs = 30;
      for( int chan=0; chan<4; ++chan )
	{
	  // Offset
	  if( node->sdoReadExpedited( OD_BSENSOR_CONFIG | rindex,
				      adc_config_obj_offs + chan*2,
				      &no_of_bytes, &data, 100 ) )
	    {
	      b_info.offs[chan] = data;
	    }
	  else
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Reading B-sensor Offsets, Node " << node->nodeId()
			 << ", B-index " << rindex << ": "
			 << node->sdoStatusString() << ", "
			 << node->errString();
	    }

	  // Gain
	  if( node->sdoReadExpedited( OD_BSENSOR_CONFIG | rindex,
				      adc_config_obj_offs + chan*2 + 1,
				      &no_of_bytes, &data, 100 ) )
	    {
	      b_info.gain[chan] = data;
	    }
	  else
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Reading B-sensor Gains, Node " << node->nodeId()
			 << ", B-index " << rindex << ": "
			 << node->sdoStatusString() << ", "
			 << node->errString();
	    }

	  // Interrupt the loop if anything goes wrong
	  if( !success )
	    {
	      // Also reset any data obtained sofar
	      for( int r=0; r<4; ++r )
		{
		  b_info.offs[r] = 0;
		  b_info.gain[r] = 0;
		}
	      break;
	    }
	}
    }
 
  // Deselect the B-sensor ADC (BATCAN modules must have version 1.1.1 or later
  // to be compatible with BATsCAN3 firmware objects, to support the following)
  // (this in fact is: deselect *any* selected B-sensor, and return the number
  //  of deselected B-sensors)
  // (30 Nov 2012: use OD_BSENSOR_SELECT rather than OD_BSENSOR_DESELECT,
  //  because the DS2413 device does not have an 'active search ROM' commmand)
  if( faster_regread_supported &&
      node->sdoReadExpedited( OD_BSENSOR_SELECT, rindex,
      //node->sdoReadExpedited( OD_BSENSOR_DESELECT, 0,
			      &no_of_bytes, &data, 100 ) == false )
    {
      success = false;
      this->errStringClear();
      _errString << "Reading B-sensor regs (deselect), Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::setBsensorOffsGains( uint32_t  i,
				     uint32_t *offs,
				     uint32_t *gain,
				     uint32_t  sz )
{
  // Set the B-sensor's Offset and Gain register contents according
  // to the values in arrays offs[] and gain[]
  // NB: call this function only with _canMsgReader in paused state !
  if( !(i < this->bSensorCount() && this->bSensorCount()) )
    {
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
      return false;
    }

  BsensorInfo *b_info = &_bSensorInfo[i];
  NodeInfo    *n_info  = b_info->pNodeInfo;
  CanNode     *node    = n_info->pNode;
  int          rindex  = b_info->remoteIndex;
  bool         success = true;
  int          no_of_bytes;

  // A faster way of register writing is supported by BATSCAN v3.1.5 and later,
  // and BATCAN v1.1.2 and later
  bool faster_regwrite_supported = false;
  if( (n_info->isMbatcan && n_info->fwVersionNr >= 315) ||
      (!n_info->isMbatcan && n_info->fwVersionNr >= 112) )
    faster_regwrite_supported = true;

  int data;
  // Select the B-sensor ADC (BATCAN modules must have version 1.1.1 or later
  // to be compatible with BATsCAN3 firmware objects, to support the following)
  if( faster_regwrite_supported &&
      node->sdoReadExpedited( OD_BSENSOR_SELECT, rindex,
			      &no_of_bytes, &data, 100 ) == false )
    {
      success = false;
      this->errStringClear();
      _errString << "Writing B-sensor regs (select), Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  if( success )
    {
      // Write (up to 4) offset and gain pairs
      int adc_config_obj_offs = 10; // For BATCAN version < 1.1.1
      if( faster_regwrite_supported ) adc_config_obj_offs = 30;
      no_of_bytes = 4;
      for( uint32_t chan=0; chan<sz; ++chan )
	{
	  // Offset
	  if( node->sdoWriteExpedited( OD_BSENSOR_CONFIG | rindex,
				       adc_config_obj_offs + chan*2,
				       no_of_bytes,
				       offs[chan], 100 ) == false )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Writing B-sensor Offsets, Node " << node->nodeId()
			 << ", B-index " << rindex << ": "
			 << node->sdoStatusString() << ", "
			 << node->errString();
	    }

	  // Gain
	  if( node->sdoWriteExpedited( OD_BSENSOR_CONFIG | rindex,
				       adc_config_obj_offs + chan*2 + 1,
				       no_of_bytes,
				       gain[chan], 100 ) == false )
	    {
	      success = false;
	      this->errStringClear();
	      _errString << "Writing B-sensor Gains, Node " << node->nodeId()
			 << ", B-index " << rindex << ": "
			 << node->sdoStatusString() << ", "
			 << node->errString();
	    }

	  // Interrupt the loop if anything goes wrong
	  if( !success ) break;
	}
    }
 
  // Deselect the B-sensor ADC (BATCAN modules must have version 1.1.1 or later
  // to be compatible with BATsCAN3 firmware objects, to support the following)
  // (this in fact is: deselect *any* selected B-sensor, and return the number
  //  of deselected B-sensors)
  // (30 Nov 2012: use OD_BSENSOR_SELECT rather than OD_BSENSOR_DESELECT,
  //  because the DS2413 device does not have an 'active search ROM' commmand)
  if( faster_regwrite_supported &&
      node->sdoReadExpedited( OD_BSENSOR_SELECT, rindex,
      //node->sdoReadExpedited( OD_BSENSOR_DESELECT, 0,
			      &no_of_bytes, &data, 100 ) == false )
    {
      success = false;
      this->errStringClear();
      _errString << "Writing B-sensor regs (deselect), Node " << node->nodeId()
		 << ", B-index " << rindex << ": "
		 << node->sdoStatusString() << ", " << node->errString();
    }

  // Now read back the B-sensor's Offset and Gain register contents
  if( success )
    success = this->getBsensorOffsGains( *b_info );

  return success;
}

// ----------------------------------------------------------------------------

bool BsCanImpl::bSensorResetSingle( uint32_t i, bool pause_reader )
{
  // Trigger a reset of the single B-sensor with index 'i' and subsequently
  // read out the resulting ADC Offset and Gain register values
  // (for use in a multiple-reset, average Offset/Gain determining procedure)
  // NB: call this function only with _canMsgReader in paused state
  //     or -alternatively- with pause_reader == true !
  bool success = true;
  if( i < this->bSensorCount() && this->bSensorCount() )
    {
      BsensorInfo *b_info = &_bSensorInfo[i];
      NodeInfo    *n_info = b_info->pNodeInfo;
      CanNode     *node   = n_info->pNode;
      int          no_of_bytes = 1, data = 0xAA;

      // Disable parallel CAN readout temporarily
      // to allow synchronous (send+receive) SDO operations
      // (when this function is used 'stand-alone')
      if( pause_reader ) _canMsgReader->pause();

      // Send the reset request (and wait for the reply)
      if( node->sdoWriteExpedited( OD_BSENSOR_RESET + b_info->remoteIndex,
				   0, no_of_bytes, data, 1000 ) )
	{
	  // Okay, so now get the B-sensor's Offset and Gain register contents
	  success = this->getBsensorOffsGains( *b_info );
	}
      else
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Reset B-sensor " << i
		     << ", Node " << node->nodeId()
		     << " index " << b_info->remoteIndex
		     << ": " << node->errString();

	  if( node->sdoTimeout() )
	    n_info->errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    n_info->errStatus |= BSCAN_ERR_COMM;
	}

      // Re-enable parallel CAN message readout and processing
      // (when this function is used 'stand-alone')
      if( pause_reader ) _canMsgReader->resume();
    }
  else
    {
      success = false;
      this->errStringClear();
      _errString << "Invalid B-sensor index " << i;
    }
  return success;
}

// ----------------------------------------------------------------------------

void BsCanImpl::adjustBindexHi()
{
  // Depending on B-sensor error status adjust the 'bIndexHi' parameter
  // of the (BATsCAN) nodes with more than 1 B-sensor module
  // ('bIndexHi' is used to determine whether a node completed read-out)
  int btotal;
  for( uint32_t n=0; n<this->nodeCount(); ++n )
    {
      btotal = this->bSensorCount( n );
      if( btotal > 1 )
	{
	  int bfirst = this->bSensorFirstIndex( n );
	  int blast  = bfirst + btotal - 1;
	  // Loop from last to first B-sensor of this node
	  for( int32_t i=blast; i>=bfirst; --i )
	    if( _bSensorInfo[i].errStatus == 0 || i == bfirst )
	      {
		_nodeInfo[n].bIndexHi = _bSensorInfo[i].remoteIndex;
		// Break out of the for-loop
		// at the first B-sensor error status equal to 0
		break;
	      }
	}
    }
}

// ----------------------------------------------------------------------------

bool BsCanImpl::prepareNodes()
{
  // NB: call this function only with _canMsgReader in paused state !
  if( !_canIntf ) return false;

  // Prepare the B-sensor CAN nodes for data acquisition
  bool     success = true;
  int      no_of_bytes, data;
  CanNode *node;

  // Set any nodes (also non-B-sensor types) on the bus to Preoperational
  _canIntf->nmt( NMT_ENTER_PREOPERATIONAL_STATE );

  for( uint32_t i=0; i<this->nodeCount(); ++i )
    {
      node = _nodeInfo[i].pNode;

      // Deselect any permanently selected B-sensors on this node
      // (30 Nov 2012: increased time-out to 1000, because the DS2413 device
      //  does not have an 'active search ROM' command as the DS2405 does)
      if( node->sdoReadExpedited( OD_BSENSOR_DESELECT, 0,
				  //&no_of_bytes, &data, 50 ) == false )
				  &no_of_bytes, &data, 2000 ) == false )
	{
	  success = false;
	  this->errStringClear();
	  _errString << "Preparing (deselect), Node " << node->nodeId()
		     << " index " << i
		     << ": " << node->sdoStatusString()
		     << ", " << node->errString();
	  if( node->sdoTimeout() )
	    _nodeInfo[i].errStatus |= BSCAN_ERR_COMM_TIMEOUT;
	  else
	    _nodeInfo[i].errStatus |= BSCAN_ERR_COMM;
	}

      // Set this particular CANopen node to Operational state
      node->nmt( NMT_START_REMOTE_NODE );
    }

  return success;
}

// ----------------------------------------------------------------------------

void BsCanImpl::clearConfig()
{
  // Delete the CanNode objects
  for( uint32_t i=0; i<this->nodeCount(); ++i )
    {
      _nodeInfo[i].pNode->stop(); // Stop its timer threads cleanly
      delete _nodeInfo[i].pNode;
    }
 
  // Clear the various info and data arrays
  _nodeInfo.clear();
  _bSensorInfo.clear();
  _bSensorData.clear();
  _emg.clear();

  // Clear any errors
  this->clearErrStatus();
}

// ----------------------------------------------------------------------------
