#
# Project file for the BsCAN library
#
# To generate a Visual Studio project:
#   qmake -t vclib BsCAN.pro
# To generate a Makefile:
#   qmake BsCAN.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = lib
TARGET   = BsCAN

# Create a shared library, uses Qt
CONFIG += shared qt warn_on exceptions debug_and_release

QT -= gui
QT += core

DEFINES += MY_LIB_EXPORT

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  LIBS        = -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  LIBS        = -L../Release
}

LIBS += -lCANopen

INCLUDEPATH += ../CAN/CANopen

SOURCES += bscan.cpp
SOURCES += bscanimpl.cpp
SOURCES += calibcalc.cpp
SOURCES += canreadthread.cpp
SOURCES += syncthread.cpp
SOURCES += bscan_c.cpp
win32 {
  # Uncomment the next line to enable proper use of the library under LabView
  #SOURCES += dllmain.cpp
}

HEADERS += bscan.h
HEADERS += bscanimpl.h
HEADERS += calibcalc.h
HEADERS += canreadthread.h
HEADERS += syncthread.h
HEADERS += bscan_c.h
HEADERS += ../CAN/CANopen/CANopen.h
HEADERS += ../CAN/CANopen/CanInterface.h
HEADERS += ../CAN/CANopen/CanNode.h
HEADERS += ../CAN/CANopen/CanMessage.h
win32 {
  HEADERS += stdint.h
}
