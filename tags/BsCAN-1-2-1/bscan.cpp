/* -------------------------------------------------------------------------
FILE   : bscan.cpp

DESCR  : Class for interfacing to a system of (strings of) B-sensor modules
         connected to multiple BATCAN and/or mBATCAN modules connected to
         a single CAN-bus.
	 Works by means of the 'pointer to implementation' idiom:
	 the actual implementation is hidden in the BsCanImpl class.
	 The user only has to include the bscan.h file, which hides many
	 of the stuff in bscanimpl.h required for implementation only.

HISTORY:
JAN12-JUL12: Development (see bscanimpl.h for details).
---------------------------------------------------------------------------- */

#include "bscanimpl.h"
#include "bscan.h"

// ----------------------------------------------------------------------------
// Constructors / destructor
// ----------------------------------------------------------------------------

BsCan::BsCan()
{
  _bscanImpl = new BsCanImpl();
}

// ----------------------------------------------------------------------------

BsCan::BsCan( int portno, int interface_type )
{
  _bscanImpl = new BsCanImpl( portno, interface_type );
}

// ----------------------------------------------------------------------------

BsCan::~BsCan()
{
  if( _bscanImpl ) delete _bscanImpl;
}

// ----------------------------------------------------------------------------

void BsCan::stop()
{
  if( _bscanImpl ) _bscanImpl->stop();
}

// ----------------------------------------------------------------------------
// Configuration
// ----------------------------------------------------------------------------

bool BsCan::setCanPort( int portno, int interface_type )
{
  return _bscanImpl->setCanPort( portno, interface_type );
}

bool BsCan::connected()
{
  return _bscanImpl->connected();
}

bool BsCan::getConfiguration( bool include_offsgains )
{
  return _bscanImpl->getConfiguration( include_offsgains );
}

bool BsCan::bSensorProbe( uint32_t node_i )
{
  return _bscanImpl->bSensorProbe( node_i );
}

bool BsCan::bSensorProbeInProgress( uint32_t node_i )
{
  return _bscanImpl->bSensorProbeInProgress( node_i );
}

bool BsCan::bSensorReset( uint32_t node_i )
{
  return _bscanImpl->bSensorReset( node_i );
}

bool BsCan::bSensorResetInProgress( uint32_t node_i )
{
  return _bscanImpl->bSensorResetInProgress( node_i );
}

bool BsCan::bSensorProbeAtPowerup( uint32_t node_i )
{
  return _bscanImpl->bSensorProbeAtPowerup( node_i );
}

bool BsCan::setBsensorProbeAtPowerup( uint32_t node_i, bool enable )
{
  return _bscanImpl->setBsensorProbeAtPowerup( node_i, enable );
}

bool BsCan::triggerInputEnabled( uint32_t node_i,
				 uint32_t trigger_input )
{
  return _bscanImpl->triggerInputEnabled( node_i, trigger_input );
}

bool BsCan::triggerInputRisingEdge( uint32_t node_i,
				    uint32_t trigger_input )
{
  return _bscanImpl->triggerInputRisingEdge( node_i, trigger_input );
}

bool BsCan::setTriggerInputEnabled( uint32_t node_i, uint32_t trigger_input,
				    bool enable, bool rising_edge )
{
  return _bscanImpl->setTriggerInputEnabled( node_i, trigger_input,
					     enable, rising_edge );
}

uint32_t BsCan::nodeCount()
{
  return _bscanImpl->nodeCount();
}

uint32_t BsCan::bSensorCount( uint32_t node_i )
{
  return _bscanImpl->bSensorCount( node_i );
}

uint32_t BsCan::bSensorStringCount( uint32_t node_i, uint32_t str_no )
{
  return _bscanImpl->bSensorStringCount( node_i, str_no );
}

int BsCan::bSensorFirstIndex( uint32_t node_i )
{
  return _bscanImpl->bSensorFirstIndex( node_i );
}

int BsCan::nodeId( uint32_t node_i )
{
  return _bscanImpl->nodeId( node_i );
}

string BsCan::nodeFirmwareVersion( uint32_t node_i )
{
  return _bscanImpl->nodeFirmwareVersion( node_i );
}

int BsCan::bSensorNodeId( uint32_t i )
{
  return _bscanImpl->bSensorNodeId( i );
}

int BsCan::bSensorRemoteIndex( uint32_t i )
{
  return _bscanImpl->bSensorRemoteIndex( i );
}

uint32_t BsCan::bSensorIdHi( uint32_t i )
{
  return _bscanImpl->bSensorIdHi( i );
}

uint32_t BsCan::bSensorIdLo( uint32_t i )
{
  return _bscanImpl->bSensorIdLo( i );
}

bool BsCan::bSensorId( uint32_t i, uint8_t *id )
{
  return _bscanImpl->bSensorId( i, id );
}

string BsCan::bSensorIdString( uint32_t i )
{
  return _bscanImpl->bSensorIdString( i );
}

bool BsCan::bSensorOffsets( uint32_t i, uint32_t *offs )
{
  return _bscanImpl->bSensorOffsets( i, offs );
}

bool BsCan::bSensorGains( uint32_t i, uint32_t *gain )
{
  return _bscanImpl->bSensorGains( i, gain );
}

uint32_t BsCan::bSensorErrStatus( uint32_t i )
{
  return _bscanImpl->bSensorErrStatus( i );
}

bool BsCan::bSensorError()
{
  return _bscanImpl->bSensorError();
}

bool BsCan::getBsensorErrStatus( uint32_t i )
{
  return _bscanImpl->getBsensorErrStatus( i );
}

bool BsCan::getBsensorOffsGains( uint32_t i )
{
  return _bscanImpl->getBsensorOffsGains( i );
}

string BsCan::configSummaryString()
{
  return _bscanImpl->configSummaryString();
}

void BsCan::setCalibConstFile( string filename )
{
  _bscanImpl->setCalibConstFile( filename );
}

string BsCan::calibConstFile()
{
  return _bscanImpl->calibConstFile();
}

// ----------------------------------------------------------------------------
// Data-acquisition
// ----------------------------------------------------------------------------

void BsCan::setReadoutTriggerSec( int secs )
{
  _bscanImpl->setReadoutTriggerSec( secs );
}

void BsCan::setReadoutTriggerMsec( int msecs )
{
  _bscanImpl->setReadoutTriggerMsec( msecs );
}

int BsCan::readoutTriggerMsec()
{
  return _bscanImpl->readoutTriggerMsec();
}

void BsCan::triggerSingleReadout()
{
  _bscanImpl->triggerSingleReadout();
}

bool BsCan::bSensorDataAvailable( uint32_t node_i )
{
  return _bscanImpl->bSensorDataAvailable( node_i );
}

bool BsCan::bSensorWaitDataAvailable( uint32_t timeout_ms )
{
  return _bscanImpl->bSensorWaitDataAvailable( timeout_ms );
}

void BsCan::bSensorWaitDataAbort()
{
  _bscanImpl->bSensorWaitDataAbort();
}

uint32_t BsCan::bSensorDataRaw( int32_t *data, uint32_t i )
{
  return _bscanImpl->bSensorDataRaw( data, i );
}

uint32_t BsCan::bSensorDataCalibrated( double *data, int32_t *err, uint32_t i )
{
  return _bscanImpl->bSensorDataCalibrated( data, err, i );
}

uint32_t BsCan::bSensorDataHallRaw( int32_t *data )
{
  return _bscanImpl->bSensorDataHallRaw( data );
}

uint32_t BsCan::bSensorDataTemperature( int32_t *data )
{
  return _bscanImpl->bSensorDataTemperature( data );
}

void BsCan::bSensorDataRelease()
{
  _bscanImpl->bSensorDataRelease();
}

string BsCan::dataTimeStampString( bool include_date )
{
  return _bscanImpl->dataTimeStampString( include_date );
}

long long BsCan::dataTimeStampMsec()
{
  return _bscanImpl->dataTimeStampMsec();
}

// ----------------------------------------------------------------------------
// Various
// ----------------------------------------------------------------------------

bool BsCan::bSensorAdcCalibrate( uint32_t i,
				 uint32_t calib_cnt,
				 double   max_sigma,
				 double  *sigma,
				 bool     include_tsensor )
{
  return _bscanImpl->bSensorAdcCalibrate( i, calib_cnt,
					  max_sigma, sigma, include_tsensor );
}

bool BsCan::bSensorSelect( uint32_t i )
{
  return _bscanImpl->bSensorSelect( i );
}

bool BsCan::bSensorDeselect( uint32_t i )
{
  return _bscanImpl->bSensorDeselect( i );
}

uint32_t BsCan::nodeErrStatus( uint32_t node_i )
{
  return _bscanImpl->nodeErrStatus( node_i );
}

uint32_t BsCan::errStatus()
{
  return _bscanImpl->errStatus();
}

void BsCan::clearErrStatus()
{
  _bscanImpl->clearErrStatus();
}

string BsCan::errString()
{
  return _bscanImpl->errString();
}

uint32_t BsCan::emgCount()
{
  return _bscanImpl->emgCount();
}

bool BsCan::emgClear( uint32_t i )
{
  return _bscanImpl->emgClear( i );
}

string BsCan::emgTimeStampString( uint32_t i, bool include_date )
{
  return _bscanImpl->emgTimeStampString( i, include_date );
}

string BsCan::emgDataString( uint32_t i )
{
  return _bscanImpl->emgDataString( i );
}

string BsCan::emgDescription( uint32_t i )
{
  return _bscanImpl->emgDescription( i );
}

string BsCan::version()
{
  return BsCanImpl::version();
}
