#ifndef BSCANIMPL_H
#define BSCANIMPL_H

#include <list>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

#include <QDateTime>
#include <QMutex>
#include <QWaitCondition>

#include "bscan.h"
#include "calibcalc.h"

class CanInterface;
class CanMessage;
class CanNode;
class CanReadThread;
class SyncThread;

// Some system constants
const int BSCAN_STRINGS_PER_NODE = 4;
const int BSCAN_MAX_STRING_SZ    = 32;

// BATsCAN Object Dictionary indices
const int OD_BSENSOR_CONFIG    = 0x5000;
const int OD_BSENSOR_STAT_SUMM = 0x5100;
const int OD_BSENSOR_STATUS    = 0x5200;
const int OD_BSENSOR_RESET     = 0x5300;
const int OD_BSENSOR_RESET_ALL = 0x5380;
const int OD_BSENSOR_TRIGGER   = 0x5400;
const int OD_BSENSOR_ANALOG_IN = 0x5500;
const int OD_BSENSOR_LIST      = 0x5600;
const int OD_BSENSOR_STRING    = 0x5700;
const int OD_BSENSOR_MAP       = 0x5800;
const int OD_BSENSOR_ID        = 0x5900;
const int OD_BSENSOR_PROBE     = 0x5B00;
const int OD_BSENSOR_DESELECT  = 0x5B03;
const int OD_BSENSOR_SELECT    = 0x5B04;
const int OD_BSENSOR_RST_PROBE = 0x5B05;

class BsCanImpl
{
public:
  BsCanImpl();
  BsCanImpl( int portno,
	     int interface_type = 0 ); // = KVASER_INTF_TYPE
  ~BsCanImpl();
  void stop();

  // Configuration
  bool     setCanPort               ( int portno,
				      int interface_type = 0 );
  bool     connected                ();

  bool     getConfiguration         ( bool include_offsgains = false );

  bool     bSensorProbe             ( uint32_t node_i = BSCAN_ALL );
  bool     bSensorProbeInProgress   ( uint32_t node_i = BSCAN_ALL );

  bool     bSensorReset             ( uint32_t node_i = BSCAN_ALL );
  bool     bSensorResetInProgress   ( uint32_t node_i = BSCAN_ALL );

  bool     bSensorProbeAtPowerup    ( uint32_t node_i );
  bool     setBsensorProbeAtPowerup ( uint32_t node_i, bool enable );

  bool     triggerInputEnabled      ( uint32_t node_i,
				      uint32_t trigger_input );
  bool     triggerInputRisingEdge   ( uint32_t node_i,
				      uint32_t trigger_input );
  bool     setTriggerInputEnabled   ( uint32_t node_i,
				      uint32_t trigger_input,
				      bool     enable,
				      bool     rising_edge = true );

  uint32_t nodeCount                ();
  uint32_t bSensorCount             ( uint32_t node_i = BSCAN_ALL );
  uint32_t bSensorStringCount       ( uint32_t node_i, uint32_t str_no );
  int      bSensorFirstIndex        ( uint32_t node_i );
  int      nodeId                   ( uint32_t node_i );
  string   nodeFirmwareVersion      ( uint32_t node_i );
  int      bSensorNodeId            ( uint32_t i );
  int      bSensorRemoteIndex       ( uint32_t i );
  uint32_t bSensorIdHi              ( uint32_t i );
  uint32_t bSensorIdLo              ( uint32_t i );
  bool     bSensorId                ( uint32_t i, uint8_t *id );
  string   bSensorIdString          ( uint32_t i );
  bool     bSensorOffsets           ( uint32_t i, uint32_t *offs );
  bool     bSensorGains             ( uint32_t i, uint32_t *gain );
  uint32_t bSensorErrStatus         ( uint32_t i );
  bool     bSensorError             ();

  bool     getBsensorErrStatus      ( uint32_t i = BSCAN_ALL );
  bool     getBsensorOffsGains      ( uint32_t i = BSCAN_ALL );

  string   configSummaryString      ();

  void     setCalibConstFile        ( string filename )
    { _calibCalc.setCalibConstFile( filename ); };
  string   calibConstFile           ()
    { return _calibCalc.calibConstFile(); };

  // Data-acquisition
  void     setReadoutTriggerSec     ( int secs );
  void     setReadoutTriggerMsec    ( int msecs );
  int      readoutTriggerMsec       ();
  void     triggerSingleReadout     ();
  bool     bSensorDataAvailable     ( uint32_t node_i = BSCAN_ALL );
  bool     bSensorWaitDataAvailable ( uint32_t timeout_ms = ULONG_MAX );
  void     bSensorWaitDataAbort     ();
  uint32_t bSensorDataRaw           ( int32_t *data, uint32_t i = BSCAN_ALL );
  uint32_t bSensorDataCalibrated    ( double  *data,
				      int32_t *err,
				      uint32_t i = BSCAN_ALL );
  uint32_t bSensorDataHallRaw       ( int32_t *data );
  uint32_t bSensorDataTemperature   ( int32_t *data );
  void     bSensorDataRelease       ();
  string    dataTimeStampString     ( bool include_date = false );
  long long dataTimeStampMsec       ();

  bool     bSensorDataItemUpdate    ( uint32_t node_i, int b_remote_i,
				      int chan, int val );

  // Various

  // Multiple B-sensor ADC reset/calibrate sequences plus
  // average offset/gain register contents calculations with
  // an upper limit on the allowed spread in values
  bool     bSensorAdcCalibrate      ( uint32_t i,
				      uint32_t calib_count,
				      double   max_sigma,
				      double  *sigma = 0,
				      bool     include_tsensor = true );

  // Explicit B-sensor selection and deselection
  bool     bSensorSelectSync        ( uint32_t i );
  bool     bSensorSelect            ( uint32_t i );
  bool     bSensorDeselect          ( uint32_t i = BSCAN_ALL );

  // CAN node error status
  uint32_t nodeErrStatus            ( uint32_t node_i );
  // System error status
  uint32_t errStatus                ();
  // Clear system error status
  void     clearErrStatus           ();
  // System error string
  string   errString                () { return _errString.str(); }

  // Emergency messages stuff
  uint32_t emgCount                 () { return _emg.size(); };
  bool     emgClear                 ( uint32_t i = BSCAN_ALL );
  string   emgTimeStampString       ( uint32_t i, bool include_date = false );
  string   emgDataString            ( uint32_t i );
  string   emgDescription           ( uint32_t i );

  static string version             () { return VERSION_STR; }

private:
  struct NodeInfo
  {
    CanNode  *pNode;
    string    firmwareVersion;
    uint32_t  fwVersionNr;
    bool      isMbatcan;
    int       bTotal;
    int       bInString[BSCAN_STRINGS_PER_NODE];
    // Where in _bSensorInfo array starts each string
    int       bStringIndex[BSCAN_STRINGS_PER_NODE];
    // Highest B-sensor module index connected to this node, info which is
    // used to detect the end of a readout cycle for this particular node
    int       bIndexHi;
    // BATsCAN modules: probe at every powerup/reset
    bool      bProbeAtPowerup;
    // BATsCAN modules: trigger input configuration (enabled/rising-edge)
    int       trigInputConfig;
    // To indicate fresh data from a readout cycle has arrived
    bool      bDataAvailable;
    // To indicate a B-sensor probe operation is in progress
    bool      bProbeInProgress;
    // To indicate a B-sensor reset operation is in progress
    bool      bResetInProgress;
    uint32_t  errStatus;
  };

  struct BsensorInfo
  {
    NodeInfo *pNodeInfo;    // Pointer to struct with info about the node
                            // this B-sensor connects to
    int       remoteIndex;  // B-sensor's index in the CAN node
    uint32_t  errStatus;
    uint32_t  idLo, idHi;   // The B-sensor's 64-bit ID as 2 unsigned longs
    uint8_t   id[8];        // The B-sensor's 64-bit ID as 8 unsigned chars
    uint32_t  offs[4];      // ADC Offset register contents
    uint32_t  gain[4];      // ADC Gain register contents
  };

  struct BsensorData
  {
    int32_t   hall[3];      // Raw Hall-sensor ADC readings for X, Y and Z
    int32_t   t;            // B-sensor temperature
    double    hallCalib[3]; // Calibrated Hall-sensor readings for X, Y and X
  };

  struct EmgInfo
  {
    CanMessage *emgMsg;
    QDateTime   timeStamp;
  };

  void   findNodes          ();
  bool   getBsensorConfig   ( bool include_offsgains = false );
  bool   getNodeInfo        ( NodeInfo &n_info );
  bool   getBsensorInfo     ( BsensorInfo &b_info,
			      bool include_offsgains );

  bool   getBsensorOffsGains( BsensorInfo &b_info );
  bool   setBsensorOffsGains( uint32_t  i,
			      uint32_t *offs,
			      uint32_t *gain,
			      uint32_t  sz = 4 );

  bool   bSensorResetSingle ( uint32_t i, bool pause_reader = true );

  void   adjustBindexHi     ();
  bool   prepareNodes       ();
  void   clearConfig        ();

  void   errStringClear     () { _errString.str( "" ); }

  static const string VERSION_STR;

  CanInterface       *_canIntf;

  bool                _refreshBsensorStatus;
  bool                _refreshBsensorOffsGains;

  uint32_t            _errStatus;
  ostringstream       _errString;

  friend class CanReadThread;
  CanReadThread      *_canMsgReader;
  SyncThread         *_syncGenerator;

  // Waiting for data
  QMutex              _mutex;
  QWaitCondition      _condDataAvailable;

  // BsCAN system configuration data
  vector<NodeInfo>    _nodeInfo;
  vector<BsensorInfo> _bSensorInfo;
  vector<BsensorData> _bSensorData;

  // Emergency message administration
  list<EmgInfo>       _emg;

  // Array to map node identifier into the _nodeInfo vector
  // (to make life a little easier: no need to search in _nodeInfo[])
  uint32_t            _nodeIdMap[128];

  // Calibrated B-sensor data calculator
  CalibCalc           _calibCalc;
};

#endif // BSCANIMPL_H
