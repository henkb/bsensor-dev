#include <CanInterface.h>
#include "syncthread.h"

// ----------------------------------------------------------------------------

SyncThread::SyncThread( CanInterface *intf, QObject *parent )
  : QThread( parent ),
    _stop( false ),
    _enabled( true ),
    _intervalMs( 0 ),
    _canIntf( intf )
{
  this->start();
}

// ----------------------------------------------------------------------------

SyncThread::~SyncThread()
{
  // In case still running...
  this->stop();
}

// ----------------------------------------------------------------------------

void SyncThread::stop()
{
  if( this->isRunning() )
    {
      _stop = true;
      _mutex.lock();
      _condition.wakeAll();
      _mutex.unlock();
      this->wait(); // Wait until this thread (i.e. function run()) exits
    }
}

// ----------------------------------------------------------------------------

void SyncThread::run()
{
  while( !_stop )
    {
      _mutex.lock();
      if( _intervalMs > 0 )
	_condition.wait( &_mutex, _intervalMs );
      else
	_condition.wait( &_mutex, 1000 ); // Just idle
      _mutex.unlock();

      if( _intervalMs > 0 && _enabled && !_stop )
	{
	  // Send a CANopen SYNC message
	  if( _canIntf ) _canIntf->sendSync();

	  // Set the timestamp for this SYNC
	  _timeStamp = QDateTime::currentDateTime();

	  // Don't generate the next SYNC until we are cleared to do so
	  _enabled = false;
	}
    }
}

// ----------------------------------------------------------------------------

void SyncThread::setEnabled( bool b )
{
  // Prevent or allow the sending of SYNC messages; we can use this to prevent
  // data not yet read by the user to be overwritten, while the SYNC generator
  // timing continues normally
  _enabled = b;
}

// ----------------------------------------------------------------------------

void SyncThread::setInterval( int ms )
{
  _intervalMs = ms;

  _mutex.lock();
  _condition.wakeOne();
  _mutex.unlock();
}

// ----------------------------------------------------------------------------

void SyncThread::takeTimeStamp()
{
  // Take a timestamp (in case SYNC was generated externally...)
  _timeStamp = QDateTime::currentDateTime();
}

// ----------------------------------------------------------------------------

std::string SyncThread::dateTimeStampString()
{
  return _timeStamp.toString( QString("yyyy-MM-dd "
				      "hh:mm:ss:zzz") ).toStdString();
}

// ----------------------------------------------------------------------------

std::string SyncThread::timeStampString()
{
  return _timeStamp.toString( QString("hh:mm:ss:zzz") ).toStdString();
}

// ----------------------------------------------------------------------------

long long SyncThread::timeStampMsec()
{
  // Return the time in (milli)seconds since 1970-01-01T00:00:00,
  // Coordinated Universal Time
#if QT_VERSION >= 0x040700
  return _timeStamp.toMSecsSinceEpoch();
#else
  return _timeStamp.toTime_t();
#endif
}

// ----------------------------------------------------------------------------
