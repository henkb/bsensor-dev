#ifdef WIN32
 // Differentiate between building the DLL or using it
 #ifdef MY_LIB_EXPORT
 #define MY_LIB __declspec(dllexport)
 #else
 #define MY_LIB __declspec(dllimport)
 #endif
#include "stdint.h"
#else
#define MY_LIB
#include </usr/include/stdint.h>
#endif // WIN32

#ifdef __cplusplus
extern "C" {
#endif
// ----------------------------------------------------------------------------
// Configuration
// ----------------------------------------------------------------------------
  MY_LIB int          BsCanSetCanPort( int portno );
  MY_LIB int          BsCanConnected( void );
  MY_LIB int          BsCanGetConfiguration( int include_offsgains );
  MY_LIB int          BsCanBsensorProbe( uint32_t node_i );
  MY_LIB int          BsCanBsensorProbeInProgress( uint32_t node_i );
  MY_LIB int          BsCanBsensorReset( uint32_t node_i );
  MY_LIB int          BsCanBsensorResetInProgress( uint32_t node_i );
  MY_LIB int          BsCanBsensorProbeAtPowerup( uint32_t node_i );
  MY_LIB int          BsCanSetBsensorProbeAtPowerup( uint32_t node_i,
						     int enable );
  MY_LIB int          BsCanTriggerInputEnabled( uint32_t node_i,
						uint32_t trigger_input );
  MY_LIB int          BsCanTriggerInputRisingEdge( uint32_t node_i,
						   uint32_t trigger_input );
  MY_LIB int          BsCanSetTriggerInputEnabled( uint32_t node_i,
						   uint32_t trigger_input,
						   int enable,
						   int rising_edge );
  MY_LIB uint32_t     BsCanNodeCount( void );
  MY_LIB uint32_t     BsCanBsensorCount( uint32_t node_i );
  MY_LIB uint32_t     BsCanBsensorStringCount( uint32_t node_i,
					       uint32_t str_no );
  MY_LIB int          BsCanBsensorFirstIndex( uint32_t node_i );
  MY_LIB int          BsCanNodeId( uint32_t node_i );
  MY_LIB const char * BsCanNodeFirmwareVersion( uint32_t node_i );
  MY_LIB int          BsCanBsensorNodeId( uint32_t i );
  MY_LIB int          BsCanBsensorRemoteIndex( uint32_t i );
  MY_LIB uint32_t     BsCanBsensorIdHi( uint32_t i );
  MY_LIB uint32_t     BsCanBsensorIdLo( uint32_t i );
  MY_LIB int          BsCanBsensorId( uint32_t i, uint8_t *id );
  MY_LIB const char * BsCanBsensorIdString( uint32_t i );
  MY_LIB int          BsCanBsensorOffsets( uint32_t i, uint32_t *offs );
  MY_LIB int          BsCanBsensorGains( uint32_t i, uint32_t *gain );
  MY_LIB uint32_t     BsCanBsensorErrStatus( uint32_t i );
  MY_LIB int          BsCanBsensorError( void );
  MY_LIB int          BsCanGetBsensorErrStatus( uint32_t i );
  MY_LIB int          BsCanGetBsensorOffsGains( uint32_t i );
  MY_LIB const char * BsCanConfigSummaryString( void );
  MY_LIB void         BsCanSetCalibConstFile( char *filename );
  MY_LIB const char * BsCanCalibConstFile( void );

// ----------------------------------------------------------------------------
// Data-acquisition
// ----------------------------------------------------------------------------
  MY_LIB void         BsCanSetReadoutTriggerSec( int secs );
  MY_LIB void         BsCanSetReadoutTriggerMsec( int msecs );
  MY_LIB int          BsCanReadoutTriggerMsec( void );
  MY_LIB void         BsCanTriggerSingleReadout( void );
  MY_LIB int          BsCanBsensorDataAvailable( uint32_t node_i );
  MY_LIB int          BsCanBsensorWaitDataAvailable( uint32_t timeout_ms );
  MY_LIB void         BsCanBsensorWaitDataAbort( void );
  MY_LIB uint32_t     BsCanBsensorDataRaw( int32_t *data, uint32_t i );
  MY_LIB uint32_t     BsCanBsensorDataCalibrated( double *data, int32_t *err,
						  uint32_t i );
  MY_LIB uint32_t     BsCanBsensorDataHallRaw( int32_t *data );
  MY_LIB uint32_t     BsCanBsensorDataTemperature( int32_t *data );
  MY_LIB void         BsCanBsensorDataRelease( void );
  MY_LIB const char * BsCanDataTimeStampString( int include_date );
  MY_LIB long long    BsCanDataTimeStampMsec( void );

// ----------------------------------------------------------------------------
// Various
// ----------------------------------------------------------------------------
  MY_LIB int          BsCanBsensorAdcCalibrate( uint32_t i,
						uint32_t calib_cnt,
						double   max_sigma,
						double  *sigma,
						int      include_tsensor );
  MY_LIB int          BsCanBsensorSelect( uint32_t i );
  MY_LIB int          BsCanBsensorDeselect( uint32_t i );
  MY_LIB uint32_t     BsCanNodeErrStatus( uint32_t node_i );
  MY_LIB uint32_t     BsCanErrStatus( void );
  MY_LIB void         BsCanClearErrStatus( void );
  MY_LIB const char * BsCanErrString( void );
  MY_LIB uint32_t     BsCanEmgCount( void );
  MY_LIB int          BsCanEmgClear( uint32_t i );
  MY_LIB const char * BsCanEmgTimeStampString( uint32_t i, int include_date );
  MY_LIB const char * BsCanEmgDataString( uint32_t i );
  MY_LIB const char * BsCanEmgDescription( uint32_t i );
  MY_LIB const char * BsCanVersion( void );

// ----------------------------------------------------------------------------
// Clean-up
// ----------------------------------------------------------------------------
  MY_LIB void         BsCanClose( void );

// ----------------------------------------------------------------------------
#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif
