#ifndef BSCAN3UI_H
#define BSCAN3UI_H

#include <QFile>
#include <QString>

#include "ui_bscan3ui.h"

class BsCan;

class BsCan3Ui: public QDialog, private Ui_BsCan3Ui
{
  Q_OBJECT

 public:
  BsCan3Ui();
  ~BsCan3Ui();

 private slots:
  void findCanPorts();
  void connectOrDisconnect();
  void reset();
  void probe();
  void startStopReadout();
  void startStopLogging();
  void setSamplesToLog();
  void selectCalibConstFile();
  void calibratedChanged();
  void processDataAndErrors();
  void clearDiagnostics();
  void hideDataLed();
  void hideErrLed();

 private:
  void closeEvent( QCloseEvent *ce );
  void readAppSettings();
  void writeAppSettings();
  void startReadout();
  void stopReadout();
  void startLogging();
  void stopLogging();
  void displayConfig();
  void updateConfig();
  void updateBsensorData( bool calibrated, bool gauss );
  void logConfig();
  void logData();

  void addDiagnostic( const QString &qs );
  void addDiagnostic( const char *str );

 private:
  BsCan *_bscan;

  // Directory to search for calibration constants files
  QString _calibConstDir;

  // Directory to write data log files to
  QString _logDir;

  // BsCAN3 system control
  bool _resetInProgress;
  bool _probeInProgress;
  int  _inProgressCntr;

  // Palettes and brushes for okay/error color indications
  QBrush _qbOkay, _qbError;

  // Data logging
  QFile            _logFile;
  QString          _convStartTimeStr;
  int              _loggedSamples;
  int              _samplesToLog;
  bool             _configLogged;
  QIntValidator   *_intValidator;
};

#endif // BSCAN3UI_H
