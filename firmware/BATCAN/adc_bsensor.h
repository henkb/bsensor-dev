/* ------------------------------------------------------------------------
File   : adc_bsensor.h

Descr  : Headerfile for adc_bsensor.c (with CS5524 ADC).

History: 28FEB.01; Henk B&B; Definition.
         24FEB.08; Henk B&B; Adapt to BATCAN module, connected to just one
                             B-sensor module.
--------------------------------------------------------------------------- */

#ifndef ADC_BSENSOR_H
#define ADC_BSENSOR_H

/* ------------------------------------------------------------------------ */
/* Configuration */

/* Configuration of the SPI serial interface for the BATCAN B-sensor ADC */

#define ADCB_SET_SCLK()             SETBIT( PORTB, 1 )
#define ADCB_CLEAR_SCLK()           CLEARBIT( PORTB, 1 )

#define ADCB_SET_SDI()              SETBIT( PORTE, 0 )
#define ADCB_CLEAR_SDI()            CLEARBIT( PORTE, 0 )

#define ADCB_SDO_HIGH()             (PINE & BIT(1))
#define ADCB_SDO_LOW()              (!ADCB_SDO_HIGH())

#define ADCB_SET_CS()               SETBIT( PORTB, 5 )
#define ADCB_CLEAR_CS()             CLEARBIT( PORTB, 5 )
#define ADCB_CS_SET()               (PORTB & BIT(5))

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR()             {SETBIT(DDRB,1);SETBIT(DDRE,0); \
                                     CLEARBIT(DDRE,1);SETBIT(PORTE,1); \
                                     SETBIT(DDRB,5);}

/* ------------------------------------------------------------------------ */

/* Maximum number of ADC devices (B-sensor modules) */
#define ADCB_MAX_CNT                1

/* Number of analog inputs per B-sensor ADC */
#define ADCB_CHANS_PER_ADC          7

/* Number of analog inputs per B-sensor ADC, during a scan ! */
#define ADCB_CHANS_PER_ADC_SCAN     4

/* Conversion parameters */
#define ADCB_CONV_PARS_B            ((CS23_WORDRATE_15 << \
                                      CS23_CSR_WORDRATE_SHIFT) | \
                                     (CS23_GAIN_100MV  << \
                                      CS23_CSR_GAIN_SHIFT) | \
                                     CS23_CSR_BIPOLAR)
#define ADCB_CONV_PARS_T            ((CS23_WORDRATE_15 << \
                                      CS23_CSR_WORDRATE_SHIFT) | \
                                     (CS23_GAIN_2V5  << \
                                      CS23_CSR_GAIN_SHIFT) | \
                                     CS23_CSR_UNIPOLAR)

/* Configuration Register */
#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_
#define ADCB_CSR_DEPTH              1
#define ADCB_CONVERSION_MODE        CS23_CNF_SINGLE_CONV
#else
#define ADCB_CSR_DEPTH              2
#define ADCB_CONVERSION_MODE        CS23_CNF_MULTIPLE_CONV
#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */
#define ADCB_CSR_DEPTH_MASK         ((ADCB_CSR_DEPTH*2-1) << \
                                     CS23_CNF_CSR_DEPTH_SHIFT)
#define ADCB_CNFREG_0               0
#define ADCB_CNFREG_1               ADCB_CSR_DEPTH_MASK
#define ADCB_CNFREG_2_CHOP256       ((CS23_CHOPFREQ_256 << \
                                      CS23_CNF_CHOPFREQ_SHIFT) | \
                                     ADCB_CONVERSION_MODE)
#define ADCB_CNFREG_2_CHOP4096      ((CS23_CHOPFREQ_4096 << \
                                      CS23_CNF_CHOPFREQ_SHIFT) | \
                                      ADCB_CONVERSION_MODE)

/* There is no signal rise/fall times to take into account
   (no opto-couplers between BATCAN module and the B-sensor module) */
#define ADC_SIGNAL_RISETIME_BSENSOR  10
#define ADC_SIGNAL_FALLTIME_BSENSOR  10

/* ------------------------------------------------------------------------ */
/* Error ID bits */

/* In 'AdcError' status byte */
#define ADC_ERR_RESET                0x01
#define ADC_ERR_CALIBRATION          0x02
#define ADC_ERR_TIMEOUT              0x04
#define ADC_ERR_CALIB_CNST           0x08
#define ADC_ABSENT                   0xFF
#define ADC_ERR_IN_HARDWARE          (ADC_ERR_RESET|ADC_ERR_CALIBRATION| \
                                      ADC_ERR_TIMEOUT)

/* In CAN-messages with ADC data */
#define ADC_ERR_CONVERSION           0x80

/* ------------------------------------------------------------------------ */
/* Function prototypes */

BOOL adcb_init                 ( void );
BOOL adcb_read                 ( BYTE adc_no,
                                 BYTE channel_no,
                                 BYTE *conversion_data );
BYTE adcb_status               ( BYTE *status );
BOOL adcb_get_config           ( BYTE adc_no,  BYTE subindex,
                                 BYTE *nbytes, BYTE *par );
BOOL adcb_set_config           ( BYTE adc_no,  BYTE subindex,
                                 BYTE nbytes,  BYTE *par );

BOOL adcb_reset_and_calibrate  ( BYTE adc_no, BOOL send_emergency );
BOOL adcb_reset                ( BYTE adc_no, BOOL send_emergency );
BOOL adcb_calibrate            ( BYTE adc_no, BOOL send_emergency );

void adcb_pdo_scan_start       ( void );
void adcb_pdo_scan_stop        ( void );
void adcb_pdo_scan             ( void );

BOOL adcb_store_config         ( void );

void adcb_select               ( BYTE adc_no );
void adcb_deselect             ( void );
BOOL adcb_selected             ( void );

BOOL adcb_set_mask             ( BYTE mask );
BYTE adcb_get_mask             ( void );

BOOL adcb_set_calib_before_scan( BOOL calib );
BOOL adcb_get_calib_before_scan( void );

BOOL adcb_set_degrees_in_pdo   ( BOOL degrees );
BOOL adcb_get_degrees_in_pdo   ( void );

BOOL adcb_set_batscan3_pdo     ( BOOL batscan3 );
BOOL adcb_get_batscan3_pdo     ( void );

/* ------------------------------------------------------------------------ */
#endif /* ADC_BSENSOR_H */
