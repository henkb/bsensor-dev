/* ------------------------------------------------------------------------
File   : ds2401.c

Descr  : Functions for operating the DALLAS DS2401 Serial Number chip
         with 1-wire interface.
         Delays in microseconds according to the DS2401 datasheet 
         recommendations, the number between brackets shows general 1-Wire
         timing recommendations by manufacturer DALLAS.
         (I don't know why these do not match...)

         From manufacturer doc:
         The DS2401 enhanced Silicon Serial Number is a low-cost, electronic
         registration number that provides an absolutely unique identity which
         can be determined with a minimal electronic interface (typically,
         a single port pin of a microcontroller).
         The DS2401 consists of a factory-lasered, 64-bit ROM that includes
         a unique 48-bit serial number, an 8-bit CRC, and an 8-bit Family Code
         (01h). Data is transferred serially via the 1-Wire protocol
         that requires only a single data lead and a ground return.
         It operates at up to 16.3 kbits/s.
         'Present Pulses' acknowledges when the reader first applies voltage.
         Multiple DS2401s can reside on a common 1-Wire Net.
         Power for reading and writing the device is derived from the data line
         itself with no need for an external power source (zero standby power).

History: 30DEC.02; Henk B&B; Start of development.
         --JUN.08; Henk B&B; Support for DS2405; found that it has tighter
                             timing, when testing; needed to get rid of
                             the function pointers;
                             added ds2401_search_single() and
                             ds2401_bit() functions.
         26AUG.08; Henk B&B; Added ds2401_match() function (to toggle
                             the DS2405 digital output, for test purposes).
         09SEP;08; Henk B&B; Added ds2401_write_bit() and
                             ds2401_search_bit() functions;
                             added ds2401_search_init() and
                             ds2401_search_next() functions;
                             removed ds2401_select() function (as there is
                             only one B-sensor, in principle).
         23FEB.09; Henk B&B; Removed ds2401_search_single();
                             break out of for-loop in ds2401_search_next().
         27NOV.12; Henk B&B; Added support for the DS2413;
                             renamed ds2401_match() to ds2405_toggle().
--------------------------------------------------------------------------- */

#include "general.h"
#include "ds2401.h"
#include "timer.h"

/* ------------------------------------------------------------------------ */
/* Globals */

/* For ID searches */
static BYTE BranchBitNo; /* At which bit there is a choice of 1 or 0 */
static BOOL LastFound;   /* Whether the last ID has been found */

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BOOL ds2401_init      ( void );
static BYTE ds2401_read_byte ( void );
static void ds2401_write_byte( BYTE byt );
static BYTE ds2401_bit       ( void );
static void ds2401_write_bit ( BYTE bit );
static BYTE ds2401_search_bit( void );

/* ------------------------------------------------------------------------ */

BOOL ds2401_read( BYTE *id )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the
     8-bits CRC from the DS24xx; at 4 MHz this function takes about 8.5 ms */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2401_init() == TRUE )
    {
      ds2401_write_byte( DS2401_READROM_CMD );
      
      for( i=0; i<DS2401_BYTES; ++i ) id[i] = ds2401_read_byte();

      result = TRUE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BYTE ds2401_crc( BYTE *id )
{
  /* Calculate CRC-8 value of the first 7 bytes of DS2401 data
     (1 byte family code and 6 bytes serial number);
     uses The CCITT-8 polynomial, expressed as X^8 + X^5 + X^4 + 1 */
  BYTE crc = 0x00;
  BYTE index;
  BYTE b;
  for( index=0; index<DS2401_BYTES-1; ++index )
    {
      BYTE byt = id[index];
      for( b=0; b<8; ++b )
        {
          if( (byt^crc) & 0x01 )
            {
              crc ^= 0x18;
              crc >>= 1;
              crc |= 0x80;
            }
          else
            {
              crc >>= 1;
            }
          byt >>= 1;
        }
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

void ds2401_search_init( void )
{
  /* Get things ready for executing the "Search ROM" command
     repeatedly until all IDs have been found */
  BranchBitNo = 0;
  LastFound   = FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL ds2401_search_next( BOOL active, BYTE *id )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the 8-bits
     CRC from a next DS24xx by executing the "Search ROM" command */
  BOOL ints_enabled = FALSE;
  BOOL result = TRUE;

  if( LastFound ) return FALSE;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2401_init() == TRUE )
    {
      BYTE i, byte_i, bitmask, bitresult;
      BYTE last_zero = 0;

      /* From the datasheet:
         "The Active-Only Search ROM command operates similarly to
         the Search ROM command except that only devices with their output
         pulldown turned on are allowed to participate in the search.
         This provides an efficient means for the bus master to determine
         devices on a multidrop system that are active (PIO pin driven low).
      */
      if( active == TRUE )
        ds2401_write_byte( DS2401_SEARCHROM_ACTIVE_CMD );
      else
        ds2401_write_byte( DS2401_SEARCHROM_CMD );

      /* Search 64 bits */
      for( i=1; i<=DS2401_BITS; ++i )
        {
          byte_i    = (i-1) / 8;
          bitmask   = 1 << ((i-1)&7);
          bitresult = ds2401_search_bit();
          switch( bitresult )
            {
            case DS2401_ZERO:
              ds2401_write_bit( 0 );
              /* Clear ID bit */
              id[byte_i] &= ~bitmask;
              break;

            case DS2401_ONE:
              ds2401_write_bit( 1 );
              /* Set ID bit */
              id[byte_i] |= bitmask;
              break;

            case DS2401_DUAL:
              /* Select the path to follow */
              if( i == BranchBitNo )
                {
                  /* Latest branch (passed it before): this time follow 1 */
                  ds2401_write_bit( 1 );
                  /* Set ID bit */
                  id[byte_i] |= bitmask;
                }
              else if( i > BranchBitNo )
                {
                  /* Encountered a new branch: follow 0 */
                  ds2401_write_bit( 0 );
                  /* Clear ID bit */
                  id[byte_i] &= ~bitmask;
                  /* Keep track of last branch where 0 was chosen
                     (i.e. it is an unfinished branch) */
                  last_zero = i;
                }
              else if( i < BranchBitNo )
                {
                  /* 'Old' branch (passed it before):
                     follow the direction taken last time */
                  if( id[byte_i] & bitmask )
                    {
                      ds2401_write_bit( 1 );
                    }
                  else
                    {
                      ds2401_write_bit( 0 );
                      /* Keep track of last branch where 0 was chosen
                         (i.e. it is an unfinished branch) */
                      last_zero = i;
                    }
                }
              break;

            case DS2401_NONE:
            default:
              result = FALSE;
              break;
            }
          if( result == FALSE ) break; /* Break out of for-loop */
        }
      /* The last unfinished branch */
      BranchBitNo = last_zero;
      if( BranchBitNo == 0 ) LastFound = TRUE;
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  if( result == FALSE ) LastFound = TRUE;
  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_toggle( BYTE *id, BYTE *output_val )
{
  /* Writes the data in id[] to the DS2401/5, thereby causing
     a toggle of the output bit in case of a 'Match ROM' provided
     the device is a DS2405; the 'out' byte is filled by executing
     one extra byte read which will result in 0xFF when the DS2405's
     output is set to 1, and to 0x00 when its output is set to 0;
     function for test purposes */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2401_init() == TRUE )
    {
      ds2401_write_byte( DS2401_MATCHROM_CMD );

      for( i=0; i<DS2401_BYTES; ++i ) ds2401_write_byte( id[i] );

      *output_val = ds2401_read_byte();

      result = TRUE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2413_toggle( BYTE *id, BYTE *output_val )
{
  /* Writes the 64-bit ID in id[] to the DS2413, thereby allowing
     access to its I/O pins; then using subsequent 'PIO Read' and/or
     'PIO Write' commands the first PIOA pin is toggled;
     function for test purposes */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i, stat;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2401_init() == TRUE )
    {
      ds2401_write_byte( DS2401_MATCHROM_CMD );

      for( i=0; i<DS2401_BYTES; ++i ) ds2401_write_byte( id[i] );

      ds2401_write_byte( DS2413_PIO_READ_CMD );

      // Get the PIOA Output Latch State
      stat = (ds2401_read_byte() & 0x02) >> 1;

      if( ds2401_init() == TRUE )
        {
          ds2401_write_byte( DS2401_MATCHROM_CMD );

          for( i=0; i<DS2401_BYTES; ++i ) ds2401_write_byte( id[i] );

          ds2401_write_byte( DS2413_PIO_WRITE_CMD );

          /* Write new output followed by its bit-inverted value */
          ds2401_write_byte( 0xFE | ~stat );
          ds2401_write_byte( ~(0xFE | ~stat) );

          /* Expect byte 0xAA followed by the new PIO pin status in reply */
          stat = ds2401_read_byte();
          if( stat == 0xAA )
            {
              /* Read the new PIO pin status */
              *output_val = ds2401_read_byte();
              result = TRUE;
            }
        }
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

static BOOL ds2401_init( void )
{
  BOOL present;

  DS2401_TO_OUTPUT();
  DS2401_CLEAR();

  // Delay of 500 (480) microseconds
  timer0_delay_mus( 250 );
  timer0_delay_mus( 250 );

  DS2401_TO_INPUT();
  DS2401_SET();   // Enable pull-up

  // Delay of 100 (70) microseconds
  timer0_delay_mus( 70 );

  // If a DS2401 is present it will pull the line low
  if( DS2401_HIGH() )
    present = FALSE;
  else
    present = TRUE;

  // Delay of 400 (410) microseconds
  timer0_delay_mus( 200 );
  timer0_delay_mus( 200 );

  return present;
}

/* ------------------------------------------------------------------------ */

static BYTE ds2401_read_byte( void )
{
  BYTE i, byt = 0;
  for( i=0; i<8; ++i )
    {
      // Make space for the next bit by shifting the rest down
      byt >>= 1;

      DS2401_TO_OUTPUT();
      DS2401_CLEAR();

      // Delay of <1 (6) microseconds
      NOP();NOP();

      DS2401_TO_INPUT();
      DS2401_SET();   // Enable pull-up

      // Delay of 5 (9) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
        BYTE tick;
        for( tick=0; tick<4; ++tick );
      }

      // LSB first
      if( DS2401_HIGH() ) byt |= 0x80;

      // Delay of 60 (55) microseconds
      timer0_delay_mus( 60 );
    }
  return byt;
}

/* ------------------------------------------------------------------------ */

static void ds2401_write_byte( BYTE byt )
{
  BYTE i;
  for( i=0; i<8; ++i )
    {
      // LSB first
      ds2401_write_bit( byt & 0x01 );
      byt >>= 1;
    }
}

/* ------------------------------------------------------------------------ */

static BYTE ds2401_bit( void )
{
  BYTE result, bit;
  result = ds2401_search_bit();
  if( result == DS2401_ZERO )
    bit = 0;
  else
    bit = 1;

  /* Write value of 'bit' to keep the ID-chip selected */
  ds2401_write_bit( bit );

  return bit;
}

/* ------------------------------------------------------------------------ */

static void ds2401_write_bit( BYTE bit )
{
  DS2401_TO_OUTPUT();
  DS2401_CLEAR();
  if( bit == 1 )
    {
      // Write a 1
      // Delay of 5 (6) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
        BYTE tick;
        for( tick=0; tick<4; ++tick );
      }
      DS2401_TO_INPUT();
      DS2401_SET();   // Enable pull-up
      // Delay of 65 (64) microseconds
      timer0_delay_mus( 65 );
    }
  else
    {
      // Write a 0
      // Delay of 70 (60) microseconds
      timer0_delay_mus( 70 );
      DS2401_TO_INPUT();
      DS2401_SET();   // Enable pull-up
      // Delay of 5 (10) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
        BYTE tick;
        for( tick=0; tick<4; ++tick );
      }
    }
}

/* ------------------------------------------------------------------------ */

static BYTE ds2401_search_bit( void )
{
  BYTE bit, bit_c, result;

  /* Read bit */
  DS2401_TO_OUTPUT();
  DS2401_CLEAR();
  // Delay of <1 (6) microseconds
  NOP();NOP();
  DS2401_TO_INPUT();
  DS2401_SET();   // Enable pull-up
  // Delay of 5 (9) microseconds
  { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
    BYTE tick;
    for( tick=0; tick<4; ++tick );
  }
  if( DS2401_HIGH() )
    bit = 1;
  else
    bit = 0;
  // Delay of 60 (55) microseconds
  timer0_delay_mus( 60 );

  /* Read bit complement */
  DS2401_TO_OUTPUT();
  DS2401_CLEAR();
  // Delay of <1 (6) microseconds
  NOP();NOP();
  DS2401_TO_INPUT();
  DS2401_SET();   // Enable pull-up
  // Delay of 5 (9) microseconds
  { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
    BYTE tick;
    for( tick=0; tick<4; ++tick );
  }
  if( DS2401_HIGH() )
    bit_c = 1;
  else
    bit_c = 0;
  // Delay of 60 (55) microseconds
  timer0_delay_mus( 60 );

  /* What did we find ? */
  if( bit == bit_c )
    {
      if( bit == 0 )
        result = DS2401_DUAL;
      else
        result = DS2401_NONE;
    }
  else
    {
      if( bit == 0 )
        result = DS2401_ZERO;
      else
        result = DS2401_ONE;
    }
  return result;
}

/* ------------------------------------------------------------------------ */
