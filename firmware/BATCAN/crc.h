/* ------------------------------------------------------------------------
File   : crc.h

Descr  : Declarations of CRC calculation functions.

History: 01FEB.08; Henk B&B; Definition, based on MDT-DCS version.
--------------------------------------------------------------------------- */

#ifndef CRC_H
#define CRC_H

/* ------------------------------------------------------------------------ */
/* Function prototypes */

UINT16 crc16_ram   ( BYTE *byt, UINT16 size );
UINT16 crc16_eeprom( UINT16 addr, UINT16 size );
BOOL   crc_app_code( UINT16 *pcrc );
BOOL   crc_get     ( BYTE   *pcrc_byte );

/* ------------------------------------------------------------------------ */
#endif /* CRC_H */
