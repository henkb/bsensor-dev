/* ------------------------------------------------------------------------
File   : general.h

Descr  : Some useful general-purpose definitions +
         includes of appropriate processor type include file and
         file with our application-specific processor configuration.

History: 30NOV.99; Henk B&B; Version for AVR applications.
         01FEB.08; Henk B&B; Version for AT90CAN applications.
--------------------------------------------------------------------------- */

#ifndef GENERAL_H
#define GENERAL_H 

/* ------------------------------------------------------------------------ */
/* Type definitions */

typedef unsigned char     BOOL;
typedef unsigned char     BYTE;
typedef signed char       CHAR;
typedef unsigned int      UINT16;
typedef signed int        INT16;
typedef unsigned long     UINT32;
typedef signed long       INT32;

/* ------------------------------------------------------------------------ */
/* Constants */

#define TRUE              1
#define FALSE             0

/* ------------------------------------------------------------------------ */
/* Macros */

#define BIT(x)            (1 << (x))
#define SETBIT(addr,x)    (addr |= BIT(x))
#define CLEARBIT(addr,x)  (addr &= ~BIT(x))

#define CLI()             asm( "CLI" )
#define NOP()             asm( "NOP" )
#define SEI()             asm( "SEI" )
#define SLEEP()           asm( "SLEEP" )
#define SPM()             asm( "SPM" )
#define WDR()             asm( "WDR" )

/* ------------------------------------------------------------------------ */

/* Choose the following include file according to the processor type used */
#ifdef AT90CAN128
#include "ioCAN128v.h"
#endif

#ifdef AT90CAN64
#include "ioCAN64v.h"
#endif

#ifdef AT90CAN32
#include "ioCAN32v.h"
#endif

/* Application-specific processor configuration */
#include "config.h"

/* ------------------------------------------------------------------------ */
#endif /* GENERAL_H */
