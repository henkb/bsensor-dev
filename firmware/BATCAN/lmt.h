/* ------------------------------------------------------------------------
File   : lmt.h

Descr  : Definitions and declarations for LMT (CANopen Layer ManagemenT).

History: 04FEB.04; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#ifndef LMT_H
#define LMT_H

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void lmt_init                      ( void );
BOOL lmt_by_sdo_set_nodeid         ( BYTE node_id );
BOOL lmt_by_sdo_nodeid_write_enable( BYTE *serial );

/* ------------------------------------------------------------------------ */
#endif /* LMT_H */
