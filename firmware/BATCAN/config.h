/* ------------------------------------------------------------------------
File   : config.h

Descr  : Configuration of the AT90CANxxx plug-on board for the NIKHEF B-sensor
         module (called BATCAN).

History: 01FEB.08; Henk B&B; Start of development, based on MDT-DCS version.
         xxFEB.08; Henk B&B; BATCAN Version 1.0.0000.
         26AUG.08; Henk B&B; BATCAN Version 1.0.0001
                             - ds2401.c/h, b_ident.c/h: added ds2401_match()
                               and b_ident_match() functions,
                               to toggle the I/O bit of the DS2405
                               (for test purposes).
                             - sdo.c: add call to b_ident_match() to
                               object OD_BSENSOR_IDENT_HI.
         05SEP.08; Henk B&B; BATCAN Version 1.0.0002
                             - BATCAN.c, can.c: moved blinking of LED,
                               indicating activity into can.c; blink LED when
                               receiving as well as sending CAN-messages.
                             - BATCAN.c: bugfix: remove 'stat[1]' from
                               Emergency message concerning B-sensor.
                             - ds2401.c/h: some name changes and additional
                               comment.
         01OCT.08; Henk B&B; BATCAN Version 1.0.0003
                             - adc_bsensor.c: extend temperature range
                               in conversion function
                               adcb_convert_ohms_to_degrees().
                             - BATCAN.c: added SLEEP() in main loop
                               (wake-up by interrupts) to limit current.
                             - b_ident.c/h: added function b_ident_match()
                               and b_ident_search() (for test purposes).
                             - can.c: add some bus-off handling to function
                               can_check_for_errors().
                             - ds2401.c/h: added functions for searching.
                             - objects.h: added object for ID-chip search.
                             - sdo.c: added object for ID-chip search.
         13JAN.10; Henk B&B; BATCAN Version 1.1.0000
                             - B(AT)sCAN3 compatibility mode, i.e.
                               reply to SDO requests concerning
                               BATsCAN3-specific objects:
                               additions to objects.h, sdo.c.
                             - ID search (test) object moved from OD 2910h to
                               29F0h.
                             - adc_bsensor.h/c: added AdcBatscan3Pdo to control
                               PDO compatibility with BATsCAN3 (i.e. extra
                               byte containing module address/index).
                             - b_ident.c: add check for all ID-bytes being
                               zeroes which results in a valid CRC.
                             - can.c: slightly different LED on/off policy
                               to make sure it blinks under heavy traffic.
                             - ds2401.c: when appropriate break out of for-loop
                               in ds2401_search_next().
         19FEB.10; Henk B&B; BATCAN Version 1.1.0001
                             - adc_bsensor: added some entries to ADC config in
                               adcb_get_config() for BATsCAN3 compatibility
                               (register reading without DS2405 ADC selection).
         05JUL.12; Henk B&B; BATCAN Version 1.1.0002
                             - adc_bsensor: added some entries to ADC config in
                               adcb_set_config() for BATsCAN3 compatibility
                               (register reading without DS2405 ADC selection).
                             - sdo.c: added Object 5380h for BATsCAN3 comp'ty.
         27NOV.12; Henk B&B; BATCAN Version 1.2.0000
                             - ds2401.c, b_ident.c:
                               added support for the DS2413 ID+I/O-chip.
                             - ds2401.h: added some def's for the DS2413.
--------------------------------------------------------------------------- */

#ifndef CONFIG_H
#define CONFIG_H

/* ------------------------------------------------------------------------ */
/* Node identification */

/* CANopen device type = 0x00000000,
   i.e. no particular device profile supported */
#define DEVICE_TYPE_CHAR0           0x00
#define DEVICE_TYPE_CHAR1           0x00
#define DEVICE_TYPE_CHAR2           0x00
#define DEVICE_TYPE_CHAR3           0x00

/* Manufacturer device name: BATC */
#define MNFCT_DEV_NAME_CHAR0        'B'
#define MNFCT_DEV_NAME_CHAR1        'A'
#define MNFCT_DEV_NAME_CHAR2        'T'
#define MNFCT_DEV_NAME_CHAR3        'C'

/* Hardware version: BATCAN version 1.0 */
#define MNFCT_HARDW_VERSION_CHAR0   'B'
#define MNFCT_HARDW_VERSION_CHAR1   'C'
#define MNFCT_HARDW_VERSION_CHAR2   '1'
#define MNFCT_HARDW_VERSION_CHAR3   '0'

/* Firmware version: BATCAN firmware version 1.1) */
#define MNFCT_SOFTW_VERSION_CHAR0   'B'
#define MNFCT_SOFTW_VERSION_CHAR1   'C'
#define MNFCT_SOFTW_VERSION_CHAR2   '1'
#define MNFCT_SOFTW_VERSION_CHAR3   '2'

/* Firmware 'minor' version */
#define SOFTW_MINOR_VERSION_CHAR0   '0'
#define SOFTW_MINOR_VERSION_CHAR1   '0'
#define SOFTW_MINOR_VERSION_CHAR2   '0'
#define SOFTW_MINOR_VERSION_CHAR3   '0'

#define VERSION_STRING              " BATCAN v1.2.0, 27 Nov 2012, HB, NIKHEF "

/* ------------------------------------------------------------------------ */

/* Set pins connected to LEDs to output,
   and enable the pull-up resistor on unconnected inputs,
   as recommended in the datasheet.
   Used are (see other include files, or schematic): PB1, PB4, PB5, PE0, PE1,
   and PD5 (TXCAN output) and PD6 (RXCAN input) */
#define BATCAN_INIT_DDR()           {PORTA=0xFF; PORTB=0xCD; PORTC=0xFF; \
                                     PORTE=0xFC; PORTF=0xFF; PORTG=0xFF; \
                                     SETBIT(DDRC,0);SETBIT(DDRC,1); \
                                     PORTD=0xFF; \
                                     SETBIT(DDRD,5); }

/* LED control */
#define LED_GREEN_ON()              CLEARBIT( PORTC, 0 )
#define LED_GREEN_OFF()             SETBIT( PORTC, 0 )
#define LED_GREEN_IS_OFF()          (PORTC & BIT(0))
#define LED_RED_ON()                CLEARBIT( PORTC, 1 )
#define LED_RED_OFF()               SETBIT( PORTC, 1 )

/* ------------------------------------------------------------------------ */
#endif /* CONFIG_H */
