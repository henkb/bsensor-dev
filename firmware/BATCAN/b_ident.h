/* ------------------------------------------------------------------------
File   : b_ident.h

Descr  : Declarations of B-sensor module Identifier functions.

History: 17MAR.03; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#ifndef B_IDENT_H
#define B_IDENT_H

/* ------------------------------------------------------------------------ */
/* Function prototypes */

BOOL b_ident_get   ( BYTE mod_no, BYTE subindex, BYTE *id );
BOOL b_ident_match ( BYTE mod_no, BYTE *output_val );
BOOL b_ident_search( BYTE subindex, BYTE *id );

/* ------------------------------------------------------------------------ */
#endif /* B_IDENT_H */
