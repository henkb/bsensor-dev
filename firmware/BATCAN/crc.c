/* ------------------------------------------------------------------------
File   : crc.c

Descr  : Functions for CRC calculations by an ATmega/AT90CAN processor.

History: 01FEB.08; Henk B&B; Definition, based on MDT-DCS version.
         04JUN.08; Henk B&B; Rename get_code_hi() to get_code(),
                             since is much simpler for devices <128k flash.
--------------------------------------------------------------------------- */

#include "general.h"
#include "crc.h"
#include "eeprom.h"

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static UINT16 crc16_code_upto_64k( UINT16 index_until );
static UINT16 crc16_code_over_64k( UINT16 crc, UINT16 index_until );
static BYTE   get_flash_byte           ( UINT16 addr );

/* ------------------------------------------------------------------------ */

UINT16 crc16_ram( BYTE *byt, UINT16 size )
{
  /* Calculate CRC-16 value; uses The CCITT-16 Polynomial,
     expressed as X^16 + X^12 + X^5 + 1
     CRC start value: 0xFFFF */

  UINT16 crc = (UINT16) 0xFFFF;
  UINT16 index;
  BYTE   b;

  for( index=0; index<size; ++index )
    {
      crc ^= (((UINT16) byt[index]) << 8);
      for( b=0; b<8; ++b )
        {
          if( crc & (UINT16) 0x8000 )
            crc = (crc << 1) ^ (UINT16) 0x1021;
          else
            crc = (crc << 1);
        }
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

UINT16 crc16_eeprom( UINT16 addr, UINT16 size )
{
  /* Calculate CRC-16 value of datablock in EEPROM;
     uses The CCITT-16 Polynomial, expressed as X^16 + X^12 + X^5 + 1
     CRC start value: 0xFFFF */

  UINT16 crc = (UINT16) 0xFFFF;
  UINT16 index;
  BYTE   b;

  for( index=addr; index<addr+size; ++index )
    {
      crc ^= (((UINT16) eepromw_read(index)) << 8);
      for( b=0; b<8; ++b )
        {
          if( crc & (UINT16) 0x8000 )
            crc = (crc << 1) ^ (UINT16) 0x1021;
          else
            crc = (crc << 1);
        }
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

/* Take into account that on an ATmega128/AT90CANxxx space for a bootloader
   is reserved in the high 4 kWords (8 kBytes) */
#ifdef AT90CAN128
#define ADDR_CRC_APP_FLASH  0xDFFF
#elif defined( AT90CAN64 )
#define ADDR_CRC_APP_FLASH  0xDFFF
#elif defined( AT90CAN32 )
#define ADDR_CRC_APP_FLASH  0x5FFF
#endif

BOOL crc_app_code( UINT16 *pcrc )
{
  UINT16 size;
  BYTE   byt;

  *pcrc = (UINT16) 0;

  /* Get the size of the program code (in bytes) */
  byt   = get_flash_byte( ADDR_CRC_APP_FLASH-1 );
  size  = (UINT16) byt;
  byt   = get_flash_byte( ADDR_CRC_APP_FLASH );
  size |= (((UINT16) byt) << 8);

  /* Check for presence of CRC in location ADDR_CRC_APP_FLASH-2,
     which should have value 0 (code <= 64k) or 1 (64k<code<=128k) */
  if( get_flash_byte( ADDR_CRC_APP_FLASH-2 ) == 0x00 )
    {
      /* Parameter is the memory index to include in the CRC calculation */
      *pcrc = crc16_code_upto_64k( size-1 );

      return TRUE;
    }
  else
    {
      if( get_flash_byte( ADDR_CRC_APP_FLASH-2 ) == 0x01 )
        {
          /* Code size is 64k+ .....: the size calculated in the above
             statements is the size of the code over 64k (in bytes) */

          /* Got to calculate the CRC in 2 parts: upto 64k and above... */
          *pcrc = crc16_code_upto_64k( (UINT16) 0xFFFF );
          if( size > (UINT16) 0 )
            *pcrc = crc16_code_over_64k( *pcrc, size-1 );

          return TRUE;
        }
      else
        {
          /* Returning FALSE and a CRC equal to zero means
             no CRC was present ! */
          return FALSE;
        }
    }
}

/* ------------------------------------------------------------------------ */

BOOL crc_get( BYTE *pcrc_byte )
{
  UINT16 size;
  BYTE   byt;
  BYTE   k64;

  /* Get the size of the program code (in bytes) */
  byt   = get_flash_byte( ADDR_CRC_APP_FLASH-1 );
  size  = (UINT16) byt;
  byt   = get_flash_byte( ADDR_CRC_APP_FLASH );
  size |= (((UINT16) byt) << 8);
  k64   = get_flash_byte( ADDR_CRC_APP_FLASH-2 );

  /* Check for presence of CRC */
  if( k64 <= 0x01 )
    {
      if( k64 == 0x01 )
        {
          pcrc_byte[0] = get_flash_byte( size - 1 );
          pcrc_byte[1] = get_flash_byte( size - 2 );
        }
      else
        {
          const BYTE *codebyte = (const BYTE *) 0x0000;
          pcrc_byte[0] = codebyte[size - 1];
          pcrc_byte[1] = codebyte[size - 2];
        }
      return TRUE;
    }
  else
    {
      /* No CRC present */
      return FALSE;
    }
}

/* ------------------------------------------------------------------------ */

static UINT16 crc16_code_upto_64k( UINT16 index_until )
{
  /* Calculate CRC-16 value; uses The CCITT-16 Polynomial,
     expressed as X^16 + X^12 + X^5 + 1 */

  /* NB: does not cover any code over the 64 kbytes limit ! */

  const BYTE *codebyte = (const BYTE *) 0x0000;
  UINT16 crc = (UINT16) 0xFFFF;
  UINT16 index;
  BYTE   b;

  /* Must include index_until, but take into account
     the case where index_until==0xFFFF */
  for( index=0; index<index_until; ++index, ++codebyte )
    {
      WDR(); /* In case of a free-running watchdog timer */
      crc ^= (((UINT16) *codebyte) << 8);
      for( b=0; b<8; ++b )
        {
          if( crc & (UINT16) 0x8000 )
            crc = (crc << 1) ^ (UINT16) 0x1021;
          else
            crc = (crc << 1);
        }
    }
  crc ^= (((UINT16) *codebyte) << 8);
  for( b=0; b<8; ++b )
    {
      if( crc & (UINT16) 0x8000 )
        crc = (crc << 1) ^ (UINT16) 0x1021;
      else
        crc = (crc << 1);
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

static UINT16 crc16_code_over_64k( UINT16 crc, UINT16 index_until )
{
  /* Calculate CRC-16 value, initializing CRC with 'crc';
     uses The CCITT-16 Polynomial, expressed as X^16 + X^12 + X^5 + 1 */

  /* NB: covers the code part beyond the 64 kbytes limit only ! */

  UINT16 lcrc = crc;
  UINT16 index;
  BYTE   b;

  for( index=0; index<=index_until; ++index )
    {
      WDR(); /* In case of a free-running watchdog timer */
      lcrc ^= (((UINT16) get_flash_byte(index)) << 8);
      for( b=0; b<8; ++b )
        {
          if( lcrc & (UINT16) 0x8000 )
            lcrc = (lcrc << 1) ^ (UINT16) 0x1021;
          else
            lcrc = (lcrc << 1);
        }
    }
  return lcrc;
}

/* ------------------------------------------------------------------------ */

static BYTE get_flash_byte( UINT16 addr )
{
  /* Read and return a byte from the flash memory */
  BYTE codbyt;

#ifndef AT90CAN128
  /* AT90CAN devices other than AT90CAN128 have 64k flash or less */
  codbyt = *((const BYTE *) addr);
  return codbyt;
#else
  /* In the following a byte is read from the upper 64k of the program memory;
     the upper 64k can _not_ be read by defining simply a 'const BYTE *' ! */

  /* Move 'addr' to the proper registers (for ELPM) */
  asm( "mov R30,R16" );
  asm( "mov R31,R17" );

  /* Set RAMPZ register to access the upper 64k page of program memory */
  RAMPZ = 1;

  /* Read the program memory byte and store it in 'codbyt' */
  asm( "elpm" );
  asm( "mov %codbyt, R0" );

  /* Reset RAMPZ register */
  RAMPZ = 0;

  return codbyt;
#endif /* AT90CAN128 */
}

/* ------------------------------------------------------------------------ */
