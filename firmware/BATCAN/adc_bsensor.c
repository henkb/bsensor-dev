/* ------------------------------------------------------------------------
File   : adc_bsensor.c

Descr  : Routines for read-out and control of the MDT-DCS B-sensors with
         onboard CRYSTAL CS5524 24-bit ADC, using the ELMB or BATCAN module.

         CS5524 ADC connections:
         -----------------------
         AIN0: connected to Hall-sensor X (A1A0 is 'don't care').
         AIN1: connected to Hall-sensor Y (A1A0 is 'don't care').
         AIN2: connected to Hall-sensor Z (A1A0 is 'don't care').
         AIN3:
          A1A0=00: ca. 87 mV (fixed: connected to a resistor in the
                   Hall-sensor chain; serves as the Hall-sensor full-scale
                   calibration input)
          A1A0=01: NTC value -> circa 72 mV < value < 2000 mV
          A1A0=10:   0 degrees C calibration input = 2.065 Volt
                   !! but measured versus the Vref=2.5V --> 0.435 is measured
          A1A0=11: 100 degrees C calibration input = 0.072 Volt
                   !! but measured versus the Vref=2.5V --> 2.428 is measured

History: 26FEB.01; Henk B&B; Start of development, based on read-out software
                             for the ELMB on-board ADC.
         22MAR.01; Henk B&B; There is only one configuration for all ADCs:
                             taken into account in adcb_set_config().
         19NOV.03; Henk B&B; A hack enabling up to 4 B-sensors to be connected
                             to one MDT-DCS module, by multiplexing the ID-chip
                             readout of the 2 extra modules with the
                             chip-select of the first 2 modules, and viceversa.
         11AUG.04; Henk B&B; Added adcb_autoconfig().
--------------------------------------------------------------------------- */

#include <math.h>

#include "general.h"
#include "adc_bsensor.h"
#include "can.h"
#include "cs5523.h"
#include "eeprom.h"
#include "objects.h"
#include "store.h"
#include "timer.h"

/* ------------------------------------------------------------------------ */
/* Globals */

/* (Highest) number of ADCs actually connected */
static BYTE AdcMsk;        /* (stored in EEPROM) */

/* ADC configuration (word rate, voltage range, unipolar/bipolar)
   for the Hall-sensors */
static BYTE AdcConfigB;    /* (stored in EEPROM) */

/* ADC configuration (word rate, voltage range, unipolar/bipolar)
   for the T-sensor */
static BYTE AdcConfigT;    /* (stored in EEPROM) */

/* ADC Configuration Register to use
   (chop frequency: depends on selected wordrate) */
static BYTE AdcConfRegB_2; /* (stored in EEPROM) */

/* Storage space for error bits concerning the ADCs */
static BYTE AdcError[ADCB_MAX_CNT];

/* Signal hold time due to opto-coupler (in microseconds) */
BYTE        AdcOptoDelayBsensor; /* (stored in EEPROM) */

/* ------------------------------------------------------------------------ */
/* Global variables for ADC channel scanning operations */

/* ADC and ADC-channel indices */
static BYTE AdcNo;
static BYTE AdcChanNo;

/* ADC scanning-operation-in-progress boolean */
static BOOL AdcScanInProgress;

/* ADC conversion-in-progress boolean */
static BOOL AdcConvInProgress;

/* ADC recalibrate before every scan */
static BOOL AdcCalibBeforeScan;

/* ADC recovery after time-out */
static BOOL AdcRecovery;

/* Temperature in PDO in units of degrees instead of Ohms */
BOOL AdcDegreesInPdo;

/* mBATCAN/BsCAN3 compatible PDO readout, i.e. one extra data byte
   (data byte 0) containing B-sensor address/index (here equal to 0) */
BOOL AdcBatscan3Pdo;

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static void adcb_set_powersave( BOOL powersave );
static BOOL adcb_scan_next    ( void );
static void adcb_load_config  ( void );
static void adcb_convert_ohms_to_degrees( float ohms, BYTE *degrees );

/* ------------------------------------------------------------------------ */

BOOL adcb_init( void )
{
  BYTE adc_no, wordrate;
  BOOL result;

  /* Initialise B-sensor ADC configuration parameters */
  adcb_load_config();

  /* Set wordrate-dependent setting for ADC Configuration Register:
     Chop Frequency:  256 Hz (at freq <= 30 Hz), 4096 Hz (other freq)
                      (recommended by Crystal, CS5524 datasheet pg 14) */

  /* Let it depend on the Hall-sensor wordrate */
  wordrate = ((AdcConfigB & CS23_CSR_WORDRATE_MASK) >>
              CS23_CSR_WORDRATE_SHIFT);
  if( wordrate == CS23_WORDRATE_61 ||
      wordrate == CS23_WORDRATE_84 ||
      wordrate == CS23_WORDRATE_101 )
    AdcConfRegB_2 = ADCB_CNFREG_2_CHOP4096;
  else
    AdcConfRegB_2 = ADCB_CNFREG_2_CHOP256;

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_ADCCONFREGB2_BSENSOR ) != AdcConfRegB_2 )
    eeprom_write( EE_ADCCONFREGB2_BSENSOR, AdcConfRegB_2 );
#endif /* __VARS_IN_EEPROM__ */

  /* Initialise error statuses */
  for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
    {
      if( AdcMsk & (1<<adc_no) )
        AdcError[adc_no] = 0;
      else
        AdcError[adc_no] = ADC_ABSENT;
    }

  /* Initialize variables for scanning operations */
  //AdcNo             = 0;
  //AdcChanNo         = 0;
  AdcScanInProgress = FALSE;
  AdcConvInProgress = FALSE;
  AdcRecovery       = TRUE;

  /* Initialise I/O-pins for the B-sensor ADCs */
  ADCB_CLEAR_SCLK();
  ADCB_CLEAR_SDI();
  adcb_deselect();
  ADCB_INIT_DDR();
  /* Initialize: they might be present although not configured to be so,
     and NOT initializing them then proves to be fatal...!! (June 2003) */
  for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
    {
      adcb_reset( adc_no, FALSE );
      WDR();
    }

  /* Reset and calibrate (configured) B-sensor ADCs */
  result = TRUE;
  for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
    {
      if( AdcMsk & (1<<adc_no) )
        {
          if( adcb_reset_and_calibrate( adc_no, FALSE ) == FALSE )
            result = FALSE;
        }
    }

  /* Set (all) B-sensor ADCs to Powersave mode */
  adcb_set_powersave( TRUE );

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_read( BYTE adc_no, BYTE chan_no, BYTE *conversion_data )
{
  BOOL result;

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      /* Can't do anything if the ADC is in use... */
      /*conversion_data[0] = 0xFF;
        conversion_data[1] = 0xFF;
        conversion_data[2] = 0xFE;*/
      return FALSE;
    }
  else
    {

/*#define __DO_NOT_CONVERT_WHEN_ERROR__ */

#ifdef __DO_NOT_CONVERT_WHEN_ERROR__
      if( AdcError[adc_no] != 0 )
        {
          /* Something has gone wrong with this ADC previously */
          /*conversion_data[0] = 0xFF;
            conversion_data[1] = 0xFF;
            conversion_data[2] = 0xFD;*/
          return FALSE;
        }
      else
#endif /* __DO_NOT_CONVERT_WHEN_ERROR__ */
        {
          /* Perform an ADC conversion */
          BYTE a1a0, cs1cs0;
          BYTE csr_data[1*2*2];
          BYTE adc_config;
          BYTE adc_conf_reg[3];

#ifdef __VARS_IN_EEPROM__
          AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif
          /* Take (all) B-sensor ADCs out of Powersave mode */
          adcb_set_powersave( FALSE );

          adcb_select( adc_no );

          /* Read/write Configuration Register in order
             to set the Depth Pointer to 1 and
             the Multiple Conversion bit to FALSE */
          cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
          adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
          adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
          adc_conf_reg[1] |= (1<<CS23_CNF_CSR_DEPTH_SHIFT);
          cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

          /* Select the proper channel:
             chan 0, 1, 2   : the Hall-sensors
             chan 3, 4, 5, 6: 87mV, T-sensor, 0C calib, 100C calib */

          if( chan_no > 2 )
            {
              a1a0   = chan_no - 3;
              cs1cs0 = 0x03;
#ifdef __VARS_IN_EEPROM__
              AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
              adc_config = AdcConfigT;
            }
          else
            {
              a1a0   = 0x00; /* Don't care */
              cs1cs0 = chan_no;
#ifdef __VARS_IN_EEPROM__
              AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
              adc_config = AdcConfigB;
            }

          /* Set A1-A0 and physical channel in LC 1 (+2)
             while maintaining proper wordrate and gain */
          csr_data[0] = (adc_config |
                         ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
                          CS23_CSR_PHYSCHAN_SEL_LO_MASK));
          csr_data[1] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
                         ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
                          CS23_CSR_PHYSCHAN_SEL_HI_MASK));
          csr_data[2] = 0x00;
          csr_data[3] = 0x00;
          cs5523_write_csr( 1, csr_data, ADCB_CSR_DEPTH );

          /* Do a conversion of LC 1 and read the result */
          result = cs5523_read_adc( 0, conversion_data );

          /* Restore Configuration Register */
          adc_conf_reg[2] |= ADCB_CONVERSION_MODE;
          adc_conf_reg[1] |= ADCB_CSR_DEPTH_MASK;
          cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

          adcb_deselect();

          /* Set (all) B-sensor ADCs to Powersave mode */
          adcb_set_powersave( TRUE );

          if( result == FALSE )
            {
              /* Conversion timed out ! */
              AdcError[adc_no] |= ADC_ERR_TIMEOUT;

              /* CANopen Error Code 0x5000: device hardware */
              can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE,
                                   adc_no, chan_no, 0,
                                   ERRREG_MANUFACTURER );

              return FALSE;
            }
        }
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BYTE adcb_status( BYTE *status )
{
  BYTE i;

  /* Two ADC statuses per byte */
  for( i=0; i<ADCB_MAX_CNT; ++i )
    {
      if( i & 1 )
        status[i>>1] |= ((AdcError[i] & 0x0F) << 4);
      else
        status[i>>1]  = (AdcError[i] & 0x0F);
    }
  return ADCB_MAX_CNT/2;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_get_config( BYTE adc_no, BYTE subindex, BYTE *nbytes, BYTE *par )
{
  *nbytes = 1;

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif

  switch( subindex )
    {
    case 0:
      /* Number of entries */
      par[0] = 37;
      break;

    case 1:
      par[0] = ADCB_CHANS_PER_ADC;
      break;

    case 2:
      par[0] = ((AdcConfigB & CS23_CSR_WORDRATE_MASK) >>
                CS23_CSR_WORDRATE_SHIFT);
      break;

    case 3:
      par[0] = ((AdcConfigB & CS23_CSR_GAIN_MASK) >>
                CS23_CSR_GAIN_SHIFT);
      break;

    case 4:
      par[0] = (AdcConfigB & CS23_CSR_UNIPOLAR);
      break;

    case 5:
      par[0] = ((AdcConfigT & CS23_CSR_WORDRATE_MASK) >>
                CS23_CSR_WORDRATE_SHIFT);
      break;

    case 6:
      par[0] = ((AdcConfigT & CS23_CSR_GAIN_MASK) >>
                CS23_CSR_GAIN_SHIFT);
      break;

    case 7:
      par[0] = (AdcConfigT & CS23_CSR_UNIPOLAR);
      break;

    case 9:
    case 29:
      {
        if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

        adcb_select( adc_no );

        /* Read the Configuration Register */
        cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, par );

        adcb_deselect();

        *nbytes = 4;
      }
      break;

    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 30:
    case 31:
    case 32:
    case 33:
    case 34:
    case 35:
    case 36:
    case 37:
      {
        BYTE reg, phys_chan_no;

        if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

        if( subindex & 1 ) reg = CS23_CMD_GAIN_REG;
        else reg = CS23_CMD_OFFSET_REG;

        if( subindex < 20 )
          phys_chan_no = (subindex - 10) >> 1;
        else
          phys_chan_no = (subindex - 30) >> 1;

        adcb_select( adc_no );

        /* Read Register */
        cs5523_read_reg( reg, phys_chan_no, par );

        adcb_deselect();

        *nbytes = 4;
      }
      break;

    case 18:
    case 19:
    case 20:
    case 21:
      {
        BYTE csr_data[4*2*2], *csr, i;

        if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

        adcb_select( adc_no );

        /* Read current CSR content */
        cs5523_read_csr( csr_data );

        subindex -= 18;

        /* Extract required CSR data */
        csr = &csr_data[subindex << 2];
        for( i=0; i<4; ++i ) par[i] = csr[i];

        adcb_deselect();

        *nbytes = 4;
      }
      break;

    case 22:
      par[0] = AdcOptoDelayBsensor;
      break;

    case 23:
      par[0] = AdcRecovery;
      break;

    default:
      return FALSE;
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_config( BYTE adc_no, BYTE subindex, BYTE nbytes, BYTE *par )
{
  BYTE i;

  /* If 'nbytes' is zero it means the data set size was
     not indicated in the SDO message */

  /* Can't do anything if the ADC is in use... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  AdcMsk              = eeprom_read( EE_ADCMSK_BSENSOR );
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcConfRegB_2       = eeprom_read( EE_ADCCONFREGB2_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif

  switch( subindex )
    {
    case 2:
      {
        BYTE adc_conf_reg[3];

        if( nbytes > 1 || par[0] > CS23_WORDRATE_7 ) return FALSE;

        AdcConfigB &= ~CS23_CSR_WORDRATE_MASK;
        AdcConfigB |= (par[0] << CS23_CSR_WORDRATE_SHIFT);

        /* Prepare wordrate-dependent setting for ADC Configuration Register */
        if( par[0] == CS23_WORDRATE_61 ||
            par[0] == CS23_WORDRATE_84 ||
            par[0] == CS23_WORDRATE_101 )
          AdcConfRegB_2 = ADCB_CNFREG_2_CHOP4096;
        else
          AdcConfRegB_2 = ADCB_CNFREG_2_CHOP256;

        adc_conf_reg[0] = ADCB_CNFREG_0;
        adc_conf_reg[1] = ADCB_CNFREG_1;
        adc_conf_reg[2] = AdcConfRegB_2;

        /* Set this configuration for all connected B-sensor ADCs ! */
        for( i=0; i<ADCB_MAX_CNT; ++i )
          {
            if( AdcError[i] == 0 )
              {
                adcb_select( i );

                /* Write the updated setting to the Config Register */
                cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

                adcb_deselect();
              }
          }
      }
      break;

    case 3:
      if( nbytes > 1 || par[0] > CS23_GAIN_2V5 ) return FALSE;

      AdcConfigB &= ~CS23_CSR_GAIN_MASK;
      AdcConfigB |= (par[0] << CS23_CSR_GAIN_SHIFT);

      break;

    case 4:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      if( par[0] == 1 )
        AdcConfigB |= CS23_CSR_UNIPOLAR;
      else
        AdcConfigB &= ~CS23_CSR_UNIPOLAR;

      break;

    case 5:
      if( nbytes > 1 || par[0] > CS23_WORDRATE_7 ) return FALSE;

      AdcConfigT &= ~CS23_CSR_WORDRATE_MASK;
      AdcConfigT |= (par[0] << CS23_CSR_WORDRATE_SHIFT);

      break;

    case 6:
      if( nbytes > 1 || par[0] > CS23_GAIN_2V5 ) return FALSE;

      AdcConfigT &= ~CS23_CSR_GAIN_MASK;
      AdcConfigT |= (par[0] << CS23_CSR_GAIN_SHIFT);

      break;

    case 7:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      if( par[0] == 1 )
        AdcConfigT |= CS23_CSR_UNIPOLAR;
      else
        AdcConfigT &= ~CS23_CSR_UNIPOLAR;

      break;

    case 8:
      {
        BYTE adc_conf_reg[3];

        if( nbytes > 1 || par[0] > 1 ) return FALSE;

        if( par[0] == 1 )
          adc_conf_reg[1] = ADCB_CNFREG_1 | CS23_CNF_POWER_SAVE;
        else
          adc_conf_reg[1] = ADCB_CNFREG_1 & (~CS23_CNF_POWER_SAVE);

        adc_conf_reg[0] = ADCB_CNFREG_0;
        adc_conf_reg[2] = AdcConfRegB_2;

        /* Set this configuration for all connected B-sensor ADCs ! */
        for( i=0; i<ADCB_MAX_CNT; ++i )
          {
            if( AdcError[i] == 0 )
              {
                adcb_select( i );

                /* Write the new setting to the Config Register */
                cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

                adcb_deselect();
              }
          }
      }
      break;

    case 9:
      {
        if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

        /* Set this configuration for all connected B-sensor ADCs ! */
        for( i=0; i<ADCB_MAX_CNT; ++i )
          {
            if( AdcError[i] == 0 )
              {
                adcb_select( i );

                /* Write the Configuration Register */
                cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, par );

                adcb_deselect();
              }
          }
      }
      break;

    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 30:
    case 31:
    case 32:
    case 33:
    case 34:
    case 35:
    case 36:
    case 37:
      {
        BYTE reg, phys_chan_no;

        if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

        if( subindex & 1 ) reg = CS23_CMD_GAIN_REG;
        else reg = CS23_CMD_OFFSET_REG;

        if( subindex < 20 )
          phys_chan_no = (subindex - 10) >> 1;
        else
          phys_chan_no = (subindex - 30) >> 1;

        adcb_select( adc_no );

        /* Write Register */
        cs5523_write_reg( reg, phys_chan_no, par );

        adcb_deselect();
      }
      break;

    case 18:
    case 19:
    case 20:
    case 21:
      {
        BYTE csr_data[4*2*2], *csr, i;

        if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

        adcb_select( adc_no );

        /* Read current CSR content */
        cs5523_read_csr( csr_data );

        subindex -= 18;

        /* Adjust CSR content */
        csr = &csr_data[subindex << 2];
        for( i=0; i<4; ++i ) csr[i] = par[i];

        /* Write new CSR content */
        cs5523_write_csr( 4, csr_data, ADCB_CSR_DEPTH );

        adcb_deselect();
      }
      break;

    case 22:
      if( nbytes > 1 || par[0] < 10 ) return FALSE;

      AdcOptoDelayBsensor = par[0];

      break;

    case 23:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      AdcRecovery = par[0];

      break;

    default:
      return FALSE;
    }

#ifdef __VARS_IN_EEPROM__
  /* Store values in EEPROM, if changed */
  if( eeprom_read( EE_ADCCONFIGB_BSENSOR ) != AdcConfigB )
    eeprom_write( EE_ADCCONFIGB_BSENSOR, AdcConfigB );
  if( eeprom_read( EE_ADCCONFIGT_BSENSOR ) != AdcConfigT )
    eeprom_write( EE_ADCCONFIGT_BSENSOR, AdcConfigT );
  if( eeprom_read( EE_ADCCONFREGB2_BSENSOR ) != AdcConfRegB_2 )
    eeprom_write( EE_ADCCONFREGB2_BSENSOR, AdcConfRegB_2 );
  if( eeprom_read( EE_ADCOPTODELAY_BSENSOR ) != AdcOptoDelayBsensor )
    eeprom_write( EE_ADCOPTODELAY_BSENSOR, AdcOptoDelayBsensor );
#endif /* __VARS_IN_EEPROM__ */

  if( subindex == 3 || subindex == 4 || subindex == 6 || subindex == 7 )
    {
      /* Recalibrate all B-sensor ADCs using the new setting ! */
      for( i=0; i<ADCB_MAX_CNT; ++i )
        if( AdcError[i] == 0 )
          adcb_reset_and_calibrate( i, TRUE );
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */

static void adcb_set_powersave( BOOL powersave )
{
  BOOL ps   = powersave;
  BOOL *pps = &ps;
  adcb_set_config( 0, 8, 1, pps );
}

/* ------------------------------------------------------------------------ */

BOOL adcb_reset_and_calibrate( BYTE adc_no, BOOL send_emergency )
{
  BOOL result;

  /* Not allowed when ADC scan in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

  /* Initialise error status */
  AdcError[adc_no] = 0;

  /* Perform calibration only if reset succeeded */
  if( (result = adcb_reset( adc_no, send_emergency )) )
    result = adcb_calibrate( adc_no, send_emergency );

  WDR();

  if( result == FALSE ) LED_RED_ON();
  else LED_RED_OFF();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_reset( BYTE adc_no, BOOL send_emergency )
{
  BOOL reset_result;
  BYTE err_id;
  BYTE adc_conf_reg[3];

  /* Not allowed when ADC scan in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  AdcConfRegB_2       = eeprom_read( EE_ADCCONFREGB2_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif

  /* Select ADC */
  adcb_select( adc_no );

  /* Initialize the ADC's serial port interface */
  cs5523_serial_init();

  /* Reset the ADC */
  reset_result = cs5523_reset( &err_id );

  /* NB: if required, initialize Logical Channels here */

  /* Initialize Configuration Register:
     Chop Frequency:  256 Hz (at freq <= 30 Hz), 4096 Hz (other freq)
                      (recommended by Crystal, CS5524 datasheet pg 14)
     Depth Pointer : 003 (read/write 2 CSRs at a time) */
  adc_conf_reg[0] = ADCB_CNFREG_0;
  adc_conf_reg[1] = ADCB_CNFREG_1;
  adc_conf_reg[2] = AdcConfRegB_2;

  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

  /* Deselect ADC */
  adcb_deselect();

  if( reset_result == FALSE )
    {
      /* Reset failed */
      AdcError[adc_no] |= ADC_ERR_RESET;

      /* Generate emergency message ? */
      if( send_emergency )
        {
          /* CANopen Error Code 0x5000: device hardware */
          can_write_emergency( 0x00, 0x50, EMG_ADC_RESET_BSE,
                               adc_no, err_id, 0,
                               ERRREG_MANUFACTURER );
        }

      return FALSE;
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_calibrate( BYTE adc_no, BOOL send_emergency )
{
  BYTE ain, a1a0;
  BOOL calib_result;
  BYTE adc_conf_reg[3];

  /* Not allowed when ADC conversion in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

  /* =================================== */
  /* Hall-sensor calibration procedure:
     1. perform a 'Self' Offset calibration on each of the 3 Hall sensor
     inputs AIN1, AIN2 and AIN3, and also AIN4, voltage range 100 mV,
     A1A0 mux-setting irrelevant;
     2. perform a 'System' Gain calibration on AIN4 with mux-setting A1A0=00,
     thus selecting the current monitor input, voltage range 100 mV;
     the resulting gain value is read from the AIN4 Gain Register and
     copied to the Gain Registers of inputs AIN1, AIN2 and AIN3. */

  adcb_select( adc_no );

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif

  calib_result = TRUE;

  /* Read/write Configuration Register in order to set the Depth Pointer to 1,
     and the Multiple Conversion bit to FALSE */
  cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
  adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
  adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
  adc_conf_reg[1] |= (1<<CS23_CNF_CSR_DEPTH_SHIFT);
  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

  /* Offset calibration of all inputs, using the B-sensor ADC-configuration */
  a1a0 = 0; /* Don't care... */
  for( ain=0; ain<4; ++ain )
    {
      if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_OFFSET_SELF_CALIB,
                                 AdcConfigB ) == FALSE )
        calib_result = FALSE;
      WDR();
    }

  /* The current monitor input is the B-sensor full-scale calib input */
  ain  = 3;
  a1a0 = 0; /* Full-scale input (ca. 87 mV) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
                             AdcConfigB ) == FALSE )
    calib_result = FALSE;
  WDR();

  /* Now read the AIN4 Gain Register and use this gain value
     for the HALL sensor inputs AIN1, AIN2 and AIN3 */
  {
    BYTE gain[3];
    cs5523_read_reg ( CS23_CMD_GAIN_REG, 3, gain );
    cs5523_write_reg( CS23_CMD_GAIN_REG, 0, gain );
    cs5523_write_reg( CS23_CMD_GAIN_REG, 1, gain );
    cs5523_write_reg( CS23_CMD_GAIN_REG, 2, gain );
  }

  adcb_deselect();

  if( calib_result == FALSE )
    {
      /* Calibration error... */
      AdcError[adc_no] |= ADC_ERR_CALIBRATION;

      if( send_emergency )
        /* CANopen Error Code 0x5000: device hardware */
        can_write_emergency( 0x00, 0x50, EMG_ADC_HALL_CALIB_BSE,
                             adc_no, 0, 0, ERRREG_MANUFACTURER );

      adcb_select( adc_no );
      cs5523_serial_init();
      adcb_deselect();

      return FALSE;
    }

  /* =================================== */
  /* T-sensor calibration procedure:
     1. perform a 'System' Offset calibration on AIN4 using mux-setting A1A0=10
     thus selecting the 0 degrees C offset calibration input,
     voltage range 2.5V.
     2. perform a 'System' Gain calibration on AIN4 using mux-setting A1A0=11
     thus selecting the 100 degrees C full-scale calibration input,
     voltage range 2.5V */

  adcb_select( adc_no );

#ifdef __VARS_IN_EEPROM__
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif

  calib_result = TRUE;

  /* The T-sensor 0 degrees C offset calibration input */
  ain  = 3;
  a1a0 = 2; /* 'Ground'/zero value (at 0 degrees C) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_OFFSET_SYSTEM_CALIB,
                             AdcConfigT ) == FALSE )
    calib_result = FALSE;
  WDR();

  /* The T-sensor 100 degrees C full-scale calibration input */
  ain  = 3;
  a1a0 = 3; /* Full scale (at 100 degrees C) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
                             AdcConfigT ) == FALSE )
    calib_result = FALSE;
  WDR();

  adcb_deselect();

  if( calib_result == FALSE )
    {
      /* Calibration error... */
      AdcError[adc_no] |= ADC_ERR_CALIBRATION;

      if( send_emergency )
        /* CANopen Error Code 0x5000: device hardware */
        can_write_emergency( 0x00, 0x50, EMG_ADC_T_CALIB_BSE,
                             adc_no, 0, 0, ERRREG_MANUFACTURER );

      adcb_select( adc_no );
      cs5523_serial_init();
      adcb_deselect();

      return FALSE;
    }

  /* =================================== */

  /* Restore Configuration Register */
  adc_conf_reg[2] |= ADCB_CONVERSION_MODE;
  adc_conf_reg[1] |= ADCB_CSR_DEPTH_MASK;
  adcb_select( adc_no );
  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
  adcb_deselect();

  return TRUE;
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan_start( void )
{
  /* Start scanning only if scanning not already in progress */
  if( (AdcScanInProgress & TRUE) == FALSE )
    {
#ifdef __VARS_IN_EEPROM__
      AdcCalibBeforeScan  = eeprom_read( EE_ADCCALIBBEFORESCAN_BSENSOR );
      AdcMsk              = eeprom_read( EE_ADCMSK_BSENSOR );
      AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

      /* Is there actually any ADC...? */
      if( AdcMsk == 0 ) return;

      if( AdcCalibBeforeScan )
        {
          /* Perform a reset/calibration procedure before each scan... */
          BYTE adc_no;
          for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
            {
              if( AdcError[adc_no] == 0 )
                adcb_reset_and_calibrate( adc_no, TRUE );
            }
        }
      else
        {
          /* Refresh the Configuration Register before each scan... */
          BYTE adc_conf_reg[3];
          BYTE adc_no;

#ifdef __VARS_IN_EEPROM__
          AdcConfRegB_2 = eeprom_read( EE_ADCCONFREGB2_BSENSOR );
#endif
          adc_conf_reg[0] = ADCB_CNFREG_0;
          adc_conf_reg[1] = ADCB_CNFREG_1;
          adc_conf_reg[2] = AdcConfRegB_2;

          for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
            {
              if( AdcError[adc_no] == 0 )
                {
                  adcb_select( adc_no );

                  /* Write the Configuration Register */
                  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

                  adcb_deselect();
                }
            }
        }

      {
        /* Prepare the Configuration Setup Registers in all connected
           B-sensor modules for conversions of the following channels:
           ADC-channel 0, 1, 2 : the Hall-sensors
           ADC-channel 3       : the T-sensor */

        BYTE a1a0, cs1cs0;
        BYTE csr_data[2*2*2];
        BYTE adc_no;

#ifdef __VARS_IN_EEPROM__
        AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
        AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif

        /* Set A1-A0 and physical channel in LC 1, 2, 3 and 4
           while maintaining proper wordrate and gain */
        a1a0   = 1; /* Select the T-sensor, when cs1cs0==3 */
        cs1cs0 = 0; /* Hall sensor H1 */
        csr_data[0] = (AdcConfigB |
                       ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_LO_MASK));
        csr_data[1] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
                       ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_HI_MASK));
        cs1cs0 = 1; /* Hall sensor H2 */
        csr_data[2] = (AdcConfigB |
                       ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_LO_MASK));
        csr_data[3] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
                       ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_HI_MASK));
        cs1cs0 = 2; /* Hall sensor H3 */
        csr_data[4] = (AdcConfigB |
                       ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_LO_MASK));
        csr_data[5] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
                       ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_HI_MASK));
        cs1cs0 = 3; /* T sensor */
        csr_data[6] = (AdcConfigT |
                       ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_LO_MASK));
        csr_data[7] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
                       ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
                        CS23_CSR_PHYSCHAN_SEL_HI_MASK));

        /* Write the CSR content to all B-sensor ADCs */
        for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
          {
            if( AdcMsk & (1<<adc_no) )
              {
                adcb_select( adc_no );
                cs5523_write_csr( 2, csr_data, ADCB_CSR_DEPTH );
                adcb_deselect();
              }
          }
      }

      {
        BYTE adc_no;
        /* Start an ADC-channels-to-PDOs scan */
        for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
          if( AdcMsk & (1<<adc_no) ) break; /* Break out of for-loop */
        AdcNo             = adc_no;
        AdcChanNo         = adc_no*ADCB_CHANS_PER_ADC_SCAN;
        AdcScanInProgress = adcb_scan_next();
      }
    }
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan_stop( void )
{
  /* We have to stop ongoing ADC conversions... */

  if( (AdcScanInProgress & TRUE) == TRUE )
    {
      if( (AdcConvInProgress & TRUE) == TRUE )
        {
          /* Wait for the conversion(s) to finish and read out the ADC data */

#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_
          adcb_select( AdcNo );

          WDR();

          /* Wait for SDO to go low flagging the conversion is done,
             but use a timeout (ca. 600 ms, sufficient at 1.88 Hz wordrate) */
          if( cs5523_await_sdo_low( 120 ) ) // Make that 1200 ms...
            {
              /* Generate 8 SCLKs to clear the SDO flag */
              cs5523_write_byte( 0 );

              /* Read 24 bits (3 bytes) of conversion data */
              cs5523_read_byte();
              cs5523_read_byte();
              cs5523_read_byte();
            }

          adcb_deselect();
#else
          /* Loop over all ADCs that still need to be read out */
          BYTE adc_no;
          for( adc_no=AdcNo; adc_no<ADCB_MAX_CNT; ++adc_no )
            {
              if( AdcMsk & (1<<adc_no) )
                {
                  adcb_select( adc_no );

                  WDR();

                  /* Wait for SDO to go low flagging the conversion is done,
                     but use a timeout (ca. 600 ms,
                     sufficient at 1.88 Hz wordrate) */
                  if( cs5523_await_sdo_low( 120 ) ) // Make that 1200 ms...
                    {
                      BYTE chan;

                      /* Generate 8 SCLKs to clear the SDO flag */
                      cs5523_write_byte( 0 );

                      /* Read 24 bits (3 bytes) of conversion data per chan */
                      for( chan=0; chan<ADCB_CHANS_PER_ADC_SCAN; ++chan )
                        {
                          /* Read 24 bits (3 bytes) of conversion data */
                          cs5523_read_byte();
                          cs5523_read_byte();
                          cs5523_read_byte();
                        }
                    }
                  adcb_deselect();
                }
            }
#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */
        }
    }

  /* Initialize variables for scanning operations */
  //AdcNo             = 0;
  //AdcChanNo         = 0;
  AdcScanInProgress = FALSE;
  AdcConvInProgress = FALSE;

  /* Set (all) B-sensor ADCs to Powersave mode */
  adcb_set_powersave( TRUE );
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan( void )
{
  /* Handle an on-going ADC channel scan, otherwise do nothing */

  if( (AdcScanInProgress & TRUE) == TRUE )
    {
      /* Read out next ADC channel */
      AdcScanInProgress = adcb_scan_next();

      if( AdcScanInProgress == FALSE )
        /* Set (all) B-sensor ADCs to Powersave mode */
        adcb_set_powersave( TRUE );
    }
}

/* ------------------------------------------------------------------------ */

#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_

static BOOL adcb_scan_next( void )
{
  /* The B-sensor scan reads out the 3 Hall-sensors and the T-sensor,
     so only 4 channels per B-sensor are in this type (PDO) of read-out;
     other T-sensor related inputs (e.g. the calibration inputs) can
     be read out using SDOs.

     NB: The analog values are converted and read out one-by-one */

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      adcb_select( AdcNo );

      /* Need to insert a delay before SDO can be checked,
         if a (slow) optocoupler is in the line */
      timer0_delay_mus( AdcOptoDelayBsensor );

      /* Conversion in progress: check if done */
      if( ADCB_SDO_LOW() )
        {
          /* SDO went low --> conversion ready ! */
          BYTE configreg[3];
          BYTE adc_config;
          BYTE pdo_data[CC_TPDO4_LEN+1];
          BYTE offs;

          /* This conversion results in real data
             that we're interested in... */

          /* Should only generate a next PDO when the previous one
             has been sent */
          if( can_busy(CC_TPDO4) )
            {
              /* Sending of CAN message not done, so wait... */
              return TRUE;
            }

          /* Generate 8 SCLKs to clear the SDO flag */
          cs5523_write_byte( 0 );

          /* mBATCAN PDO compatibility ? */
          if( AdcBatscan3Pdo == TRUE )
            offs = 1;
          else
            offs = 0;

          /* Read 24 bits (3 bytes) of ADC conversion data */
          pdo_data[4+offs] = cs5523_read_byte();
          pdo_data[3+offs] = cs5523_read_byte();
          pdo_data[2+offs] = cs5523_read_byte();

          /* Read Configuration Register in order
             to check the data conversion error flags */
          cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, configreg );

          adcb_deselect();

          /* Add a status byte containing
             the ADC-configuration (wordrate, gain, uni/bipolar)
             and one bit with an 'or' of the OF and OD bits
             (here bit 7, the highest bit) */
          if( (AdcChanNo & 0x03) == 0x03 )
            {
              /* Reading out the T-sensor */
#ifdef __VARS_IN_EEPROM__
              AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
              adc_config = AdcConfigT;
            }
          else
            {
              /* Reading out a B-sensor */
#ifdef __VARS_IN_EEPROM__
              AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
              adc_config = AdcConfigB;
            }

          /* Set the upper bit of the status byte if any of the
             data conversion error bits are set... */
          if( configreg[0] & 0xC0 )
            pdo_data[1+offs] = adc_config | ADC_ERR_CONVERSION;
          else
            pdo_data[1+offs] = adc_config;

          /* Set to FALSE only if message can be sent... */
          AdcConvInProgress = FALSE;

          /* Put the ADC channel number in the message */
          pdo_data[0+offs] = AdcChanNo;

          /* mBATCAN PDO compatibility ? */
          if( AdcBatscan3Pdo == TRUE ) pdo_data[0] = 0;

          /* Send as a Transmit-PDO4 CAN-message */
          can_write( CC_TPDO4, CC_TPDO4_LEN+offs, pdo_data );

          /* Next time, next channel */
          ++AdcChanNo;
          AdcNo = AdcChanNo / ADCB_CHANS_PER_ADC_SCAN;

#ifdef __VARS_IN_EEPROM__
          AdcMsk = eeprom_read( EE_ADCMSK_BSENSOR );
#endif

          /* Are we done with the current scan ? */
          if( (AdcMsk & (1<<AdcNo)) == 0 )
            {
              /* Search for a next B-sensor */
              BYTE adc_no;
              for( adc_no=AdcNo; adc_no<ADCB_MAX_CNT; ++adc_no )
                if( AdcMsk & (1<<adc_no) ) break; /* Break out of for-loop */

              if( adc_no == ADCB_MAX_CNT )
                {
                  /* Yes, we're done... */
                  return FALSE;
                }

              AdcNo = adc_no;
              AdcChanNo = AdcNo*ADCB_CHANS_PER_ADC_SCAN;
            }
        }
      else
        {
          /* Conversion in progress...
             check for possible timeout... */

          /* ###Do not deselect/select: causes noise on the converted data */
          //adcb_deselect();

          if( timer2_timeout(ADC_BSENSOR) )
            {
              /* Conversion timed out ! */
              AdcError[AdcNo] |= ADC_ERR_TIMEOUT;

              /* CANopen Error Code 0x5000: device hardware */
              can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE, AdcNo,
                                   (AdcChanNo % ADCB_CHANS_PER_ADC_SCAN), 0,
                                   ERRREG_MANUFACTURER );

              //adcb_select( AdcNo );
              //cs5523_serial_init();
              //adcb_deselect();
              AdcConvInProgress = FALSE; /* To enable reset+calib procedure */
              adcb_reset_and_calibrate( AdcNo, TRUE );
              AdcConvInProgress = TRUE;

#ifdef __VARS_IN_EEPROM__
              AdcMsk = eeprom_read( EE_ADCMSK_BSENSOR );
#endif
              /* The ADC has an error status, go to the next (if any)*/
              ++AdcNo;

              /* Are we done with the current scan ? */
              if( (AdcMsk & (1<<AdcNo)) == 0 )
                {
                  /* Search for a next B-sensor */
                  BYTE adc_no;
                  for( adc_no=AdcNo; adc_no<ADCB_MAX_CNT; ++adc_no )
                    if( AdcMsk & (1<<adc_no) ) break; /* Break out of 'for' */

                  if( adc_no == ADCB_MAX_CNT )
                    {
                      AdcConvInProgress = FALSE;

                      /* Yes, we're done... */
                      return FALSE;
                    }

                  AdcNo = adc_no;
                }

              /* We continue with a next ADC:
                 determine appropriate channel number */
              AdcChanNo = AdcNo * ADCB_CHANS_PER_ADC_SCAN;
            }
          else
            {
              /* Wait some more... */
              return TRUE;
            }
        }
    }

  {
    /* Initiate the next ADC conversion */
    BYTE logchan_ptr;

    logchan_ptr = (AdcChanNo & 3) << CS23_CMD_LOGCHAN_SELECT_SHIFT;

    /* Start a conversion of a LC */
    adcb_select( AdcNo );
    cs5523_start_conversion_cmd( CS23_CMD_NORMAL_CONVERSION | logchan_ptr );
    /* ###Do not deselect/select: causes noise on the converted data */
    //adcb_deselect();

    AdcConvInProgress = TRUE;

    /* Set a timeout on the conversion of about 800 ms... */
    timer2_set_timeout_10ms( ADC_BSENSOR, 120 ); // Make that 1200 ms...
  }

  return TRUE;
}

#else /* not _SINGLE_CHAN_BSENSOR_CONVERSION_ */
/* ------------------------------------------------------------------------ */

static BOOL adcb_scan_next( void )
{
  /* The B-sensor scan reads out the 3 Hall-sensors and the T-sensor,
     so only 4 channels per B-sensor are in this type (PDO) of read-out;
     other T-sensor related inputs (e.g. the calibration inputs) can
     be read out using SDOs.

     NB: The analog values are converted and read out in
         one conversion sequence */

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      adcb_select( AdcNo );

      /* Need to insert a delay before SDO can be checked,
         if a (slow) optocoupler is in the line */
      timer0_delay_mus( AdcOptoDelayBsensor );

      /* Conversion in progress: check if done */
      if( ADCB_SDO_LOW() )
        {
          /* SDO went low --> conversion ready ! */
          BYTE chan;
          BYTE pdo_data[CC_TPDO4_LEN+1];
          BYTE offs;

          /* This conversion results in real data
             that we're interested in... */

          /* Should only generate a next PDO when the previous one
             has been sent */
          if( can_busy(CC_TPDO4) )
            {
              /* Sending of CAN message not finished, so wait... */
              return TRUE;
            }

          LED_RED_OFF();

          /* Generate 8 SCLKs to clear the SDO flag */
          cs5523_write_byte( 0 );

          /* mBATCAN PDO compatibility ? */
          if( AdcBatscan3Pdo == TRUE )
            offs = 1;
          else
            offs = 0;

          for( chan=0; chan<ADCB_CHANS_PER_ADC_SCAN; ++chan )
            {
              /* Read 24 bits (3 bytes) of ADC conversion data */
              pdo_data[4+offs] = cs5523_read_byte();
              pdo_data[3+offs] = cs5523_read_byte();
              pdo_data[2+offs] = cs5523_read_byte();

              /* Add a status byte containing
                 the ADC-configuration (wordrate, gain, uni/bipolar) */
              if( chan == 3 )
                {
                  /* Provide temperature in (milli)degrees celcius ? */
                  if( AdcDegreesInPdo & TRUE )
                    {
                      BYTE   *ptr;
                      UINT32 a_l;
                      float  a, ohms_f;
                      BYTE   degrees[4];
                      ptr    = (BYTE *) &a_l;
                      *ptr++ = pdo_data[2+offs];
                      *ptr++ = pdo_data[3+offs];
                      *ptr++ = pdo_data[4+offs];
                      *ptr   = 0x00;
                      a      = ((float) 1.996 * (float) a_l) / 16777215.0;
                      ohms_f = (float) 23200.0*((2.0685-a)/(2.9315+a));
                      adcb_convert_ohms_to_degrees( ohms_f, degrees );
                      if( degrees[3] != 0x00 )
                        {
                          degrees[0] = 0xFF;
                          degrees[1] = 0xFF;
                          degrees[2] = 0xFF;
                        }
                      pdo_data[2+offs] = degrees[0];
                      pdo_data[3+offs] = degrees[1];
                      pdo_data[4+offs] = degrees[2];
                    }
#ifdef __VARS_IN_EEPROM__
                  AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
                  /* Reading out the T-sensor */
                  pdo_data[1+offs] = AdcConfigT;
                }
              else
                {
#ifdef __VARS_IN_EEPROM__
                  AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
                  /* Reading out a Hall-sensor */
                  pdo_data[1+offs] = AdcConfigB;
                }

              /* Put the ADC channel number in the message */
              pdo_data[0+offs] = AdcChanNo;

              /* mBATCAN PDO compatibility ? */
              if( AdcBatscan3Pdo == TRUE ) pdo_data[0] = 0;

              /* Increment the channel number */
              ++AdcChanNo;

              /* Should only generate a next PDO when
                 the previous one has been sent */
              while( can_busy(CC_TPDO4) );

              /* Send as a Transmit-PDO4 CAN-message */
              can_write( CC_TPDO4, CC_TPDO4_LEN+offs, pdo_data );
            }

          if( AdcRecovery )
            {
              /* Do checks to keep B-sensor data integrity,
                 e.g. check a calibration constant for a reasonable value */
            }

          adcb_deselect();

#ifdef __VARS_IN_EEPROM__
          AdcMsk = eeprom_read( EE_ADCMSK_BSENSOR );
#endif
          {
            /* Search for a next B-sensor */
            BYTE adc_no;
            for( adc_no=AdcNo+1; adc_no<ADCB_MAX_CNT; ++adc_no )
              if( AdcMsk & (1<<adc_no) ) break; /* Break out of for-loop */

            /* Are we done with the current scan ? */
            if( adc_no == ADCB_MAX_CNT )
              {
                AdcConvInProgress = FALSE;

                /* Yes, we're done... */
                return FALSE;
              }

            AdcNo = adc_no;
          }

          /* We continue with a next ADC:
             determine appropriate channel number */
          AdcChanNo = AdcNo * ADCB_CHANS_PER_ADC_SCAN;

          return TRUE;
        }
      else
        {
          /* Conversion in progress...
             check for possible timeout... */
          BOOL timeout;
          timeout = timer2_timeout( ADC_BSENSOR );

          /* ###Do not deselect/select: causes noise on conversion */
          //adcb_deselect();

          if( AdcError[AdcNo] || timeout )
            {
              if( timeout )
                {
                  LED_RED_ON();

                  /* Conversion timed out ! */
                  AdcError[AdcNo] |= ADC_ERR_TIMEOUT;

                  /* CANopen Error Code 0x5000: device hardware */
                  can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE,
                                       AdcNo,
                                       (AdcChanNo % ADCB_CHANS_PER_ADC_SCAN),
                                       0, ERRREG_MANUFACTURER );

                  //adcb_select( AdcNo );
                  //cs5523_serial_init();
                  //adcb_deselect();

                  /* Perhaps next time it will work ? */
                  if( AdcRecovery )
                    {
                      AdcConvInProgress = FALSE;
                      if( adcb_reset_and_calibrate( AdcNo, TRUE ) == FALSE )
                        adcb_reset_and_calibrate( AdcNo, TRUE );
                      AdcConvInProgress = TRUE;
                    }
                }

#ifdef __VARS_IN_EEPROM__
              AdcMsk = eeprom_read( EE_ADCMSK_BSENSOR );
#endif
              /* The ADC has an error status, go to the next (if any)*/
              {
                /* Search for a next B-sensor */
                BYTE adc_no;
                for( adc_no=AdcNo+1; adc_no<ADCB_MAX_CNT; ++adc_no )
                  if( AdcMsk & (1<<adc_no) ) break; /* Break out of for-loop */

                /* Are we done with the current scan ? */
                if( adc_no == ADCB_MAX_CNT )
                  {
                    AdcConvInProgress = FALSE;

                    /* Yes, we're done... */
                    return FALSE;
                  }

                AdcNo = adc_no;
              }

              /* We continue with a next ADC:
                 determine appropriate channel number */
              AdcChanNo = AdcNo * ADCB_CHANS_PER_ADC_SCAN;
            }

          /* Wait some more... */
          return TRUE;
        }
    }

  {
    /* Initiate ADC conversions on all B-sensor ADCs simultaneously */
    BYTE adc_no;

    /* Write conversion command to all B-sensor ADCs */
    for( adc_no=AdcNo; adc_no<ADCB_MAX_CNT; ++adc_no )
      {
        if( AdcMsk & (1<<adc_no) )
          {
            /* Start a conversion of LC 1 to 4 */
            adcb_select( adc_no );
            cs5523_start_conversion_cmd( CS23_CMD_NORMAL_CONVERSION | 0 );
            adcb_deselect();
          }
      }
    /* ###Do not deselect/select: causes noise on conversion */
    adcb_select( AdcNo );

    AdcConvInProgress = TRUE;

    /* Set a timeout on the conversions (4x) of about 1000 ms...
       (NB: this means the 1.88 and 3.76 Hz conversions don't work!) */
    timer2_set_timeout_10ms( ADC_BSENSOR, 100 );
  }

  return TRUE;
}

#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */

/* ------------------------------------------------------------------------ */

#define ADCB_STORE_SIZE 7

/* ------------------------------------------------------------------------ */

BOOL adcb_store_config( void )
{
  BYTE block[ADCB_STORE_SIZE];

#ifdef __VARS_IN_EEPROM__
  AdcMsk              = eeprom_read( EE_ADCMSK_BSENSOR );
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcCalibBeforeScan  = eeprom_read( EE_ADCCALIBBEFORESCAN_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  AdcDegreesInPdo     = eeprom_read( EE_ADCDEGREESINPDO_BSENSOR );
  AdcBatscan3Pdo      = eeprom_read( EE_ADCBATSCAN3PDO_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  block[0] = AdcMsk;
  block[1] = AdcConfigB;
  block[2] = AdcConfigT;
  block[3] = AdcCalibBeforeScan;
  block[4] = AdcOptoDelayBsensor;
  block[5] = AdcDegreesInPdo;
  block[6] = AdcBatscan3Pdo;

  return( storage_write_block( STORE_ADC_BSENSOR, ADCB_STORE_SIZE, block ) );
}

/* ------------------------------------------------------------------------ */

static void adcb_load_config( void )
{
  BYTE block[ADCB_STORE_SIZE];

  /* Preset block with parameter defaults */
  block[0] = (1<<ADCB_MAX_CNT)-1;
  block[1] = ADCB_CONV_PARS_B;
  block[2] = ADCB_CONV_PARS_T;
  block[3] = FALSE;
  block[4] = ADC_SIGNAL_RISETIME_BSENSOR;
  block[5] = TRUE;
  block[6] = FALSE;

  /* Read the configuration from EEPROM, if any */
  storage_read_block( STORE_ADC_BSENSOR, ADCB_STORE_SIZE, block );

  AdcMsk              = block[0];
  AdcConfigB          = block[1];
  AdcConfigT          = block[2];
  AdcCalibBeforeScan  = block[3];
  AdcOptoDelayBsensor = block[4];
  AdcDegreesInPdo     = block[5];
  AdcBatscan3Pdo      = block[6];

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_ADCMSK_BSENSOR ) != AdcMsk )
    eeprom_write( EE_ADCMSK_BSENSOR, AdcMsk );
  if( eeprom_read( EE_ADCCONFIGB_BSENSOR ) != AdcConfigB )
    eeprom_write( EE_ADCCONFIGB_BSENSOR, AdcConfigB );
  if( eeprom_read( EE_ADCCONFIGT_BSENSOR ) != AdcConfigT )
    eeprom_write( EE_ADCCONFIGT_BSENSOR, AdcConfigT );
  if( eeprom_read( EE_ADCCALIBBEFORESCAN_BSENSOR ) != AdcCalibBeforeScan )
    eeprom_write( EE_ADCCALIBBEFORESCAN_BSENSOR, AdcCalibBeforeScan );
  if( eeprom_read( EE_ADCOPTODELAY_BSENSOR ) != AdcOptoDelayBsensor )
    eeprom_write( EE_ADCOPTODELAY_BSENSOR, AdcOptoDelayBsensor );
  if( eeprom_read( EE_ADCDEGREESINPDO_BSENSOR ) != AdcDegreesInPdo )
    eeprom_write( EE_ADCDEGREESINPDO_BSENSOR, AdcDegreesInPdo );
  if( eeprom_read( EE_ADCBATSCAN3PDO_BSENSOR ) != AdcBatscan3Pdo )
    eeprom_write( EE_ADCBATSCAN3PDO_BSENSOR, AdcBatscan3Pdo );
#endif /* __VARS_IN_EEPROM__ */
}

/* ------------------------------------------------------------------------ */

void adcb_select( BYTE adc_no )
{
  /* Refresh Data-Direction Register settings */
  ADCB_INIT_DDR();

  /* Activate one of the B-sensor select signals */
  switch( adc_no )
    {
    case 0:
      ADCB_CLEAR_CS();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

void adcb_deselect( void )
{
  /* Deactivate all select signals */
  ADCB_SET_CS();
}

/* ------------------------------------------------------------------------ */

BOOL adcb_selected( void )
{
  if( ADCB_CS_SET() )
    return FALSE;
  else
    return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_mask( BYTE mask )
{
  BYTE adc_no;

  if( mask > ((1<<ADCB_MAX_CNT)-1) ) return FALSE;

  AdcMsk = mask;

  /* Initialise error statuses */
  for( adc_no=0; adc_no<ADCB_MAX_CNT; ++adc_no )
    {
      if( AdcMsk & (1<<adc_no) )
        AdcError[adc_no] = 0;
      else
        AdcError[adc_no] = ADC_ABSENT;
    }

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_ADCMSK_BSENSOR ) != AdcMsk )
    eeprom_write( EE_ADCMSK_BSENSOR, AdcMsk );
#endif /* __VARS_IN_EEPROM__ */

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BYTE adcb_get_mask( void )
{
#ifdef __VARS_IN_EEPROM__
  AdcMsk = eeprom_read( EE_ADCMSK_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  return AdcMsk;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_calib_before_scan( BOOL calib )
{
  if( calib > 1 ) return FALSE;
  if( calib )
    AdcCalibBeforeScan = TRUE;
  else
    AdcCalibBeforeScan = FALSE;

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_ADCCALIBBEFORESCAN_BSENSOR ) != AdcCalibBeforeScan )
    eeprom_write( EE_ADCCALIBBEFORESCAN_BSENSOR, AdcCalibBeforeScan );
#endif /* __VARS_IN_EEPROM__ */
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_get_calib_before_scan( void )
{
#ifdef __VARS_IN_EEPROM__
  AdcCalibBeforeScan = eeprom_read( EE_ADCCALIBBEFORESCAN_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  return AdcCalibBeforeScan;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_degrees_in_pdo( BOOL degrees )
{
  if( degrees > 1 ) return FALSE;
  if( degrees )
    AdcDegreesInPdo = TRUE;
  else
    AdcDegreesInPdo = FALSE;

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_ADCDEGREESINPDO_BSENSOR ) != AdcDegreesInPdo )
    eeprom_write( EE_ADCDEGREESINPDO_BSENSOR, AdcDegreesInPdo );
#endif /* __VARS_IN_EEPROM__ */
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_get_degrees_in_pdo( void )
{
#ifdef __VARS_IN_EEPROM__
  AdcDegreesInPdo = eeprom_read( EE_ADCDEGREESINPDO_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  return AdcDegreesInPdo;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_batscan3_pdo( BOOL batscan3 )
{
  if( batscan3 > 1 ) return FALSE;
  if( batscan3 )
    AdcBatscan3Pdo = TRUE;
  else
    AdcBatscan3Pdo = FALSE;

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_ADCBATSCAN3PDO_BSENSOR ) != AdcBatscan3Pdo )
    eeprom_write( EE_ADCBATSCAN3PDO_BSENSOR, AdcBatscan3Pdo );
#endif /* __VARS_IN_EEPROM__ */
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_get_batscan3_pdo( void )
{
#ifdef __VARS_IN_EEPROM__
  AdcBatscan3Pdo = eeprom_read( EE_ADCBATSCAN3PDO_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  return AdcBatscan3Pdo;
}

/* ------------------------------------------------------------------------ */

/* Constants used in NTC temperature conversion (Ohms to degrees) */

/* Rt/R25 range 68.600 to 3.274 (T from -50 to 0 degrees) */
#define ALL 3.3538646E-03
#define BLL 2.5654090E-04
#define CLL 1.9243889E-06
#define DLL 1.0969244E-07

/* Rt/R25 range 3.274 to 0.36036 (T from 0 to 50 degrees) */
#define AL  3.3540154E-03
#define BL  2.5627725E-04
#define CL  2.0829210E-06
#define DL  7.3003206E-08

/* Rt/R25 range 0.36036 to 0.06831 (T from 50 to 100 degrees) */
#define AH  3.3539264E-03
#define BH  2.5609446E-04
#define CH  1.9621987E-06
#define DH  4.6045930E-08

/* Rt/R25 range 0.06831 to 0.01872 (T from 100 to 150 degrees) */
#define AHH 3.3368620E-03
#define BHH 2.4057263E-04
#define CHH (-2.6687093E-06)
#define DHH (-4.0719355E-07)

static void adcb_convert_ohms_to_degrees( float ohms, BYTE *degrees )
{
  /* Using the AVR Studio Simulator it was determined that
     this function takes roughly about 3.5 ms to complete,
     about 2 ms of which is taken up by the call to log() */
  float r, ln_r, ln_r2, ln_r3;
  float temp;
  INT32 temp_l;
  BYTE  *ptr;

  r     = ohms / (float) 5000.0;
  ln_r  = log( r );
  ln_r2 = ln_r * ln_r;
  ln_r3 = ln_r2 * ln_r;

  if( r <= (float) 0.36036 )
    {
      if( r <= 0.06831 )
        /* 0.06831 >= r > 0.01872 ==> 100C <= Temperature < 150C */
        temp = AHH + BHH*ln_r + CHH*ln_r2 + DHH*ln_r3;
      else
        /* 0.36036 >= r >= 0.06831 ==> 50C <= Temperature < 100C */
        temp = AH + BH*ln_r + CH*ln_r2 + DH*ln_r3;
    }
  else
    {
      if( r > 3.274 )
        /* 68.600 >= r > 3.274 ==> -50C <= Temperature < 0C */
        temp = ALL + BLL*ln_r + CLL*ln_r2 + DLL*ln_r3;
      else
        /* 3.274 >= r > 0.36036 ==> 0C <= Temperature < 50C */
        temp = AL + BL*ln_r + CL*ln_r2 + DL*ln_r3;
    }

  /* Temperature in millidegrees centigrade */
  temp = (((float) 1.0/temp) - (float) 273.15) * (float) 1000.0;

  /* Check for reasonable value, i.e. 24-bit significant at maximum */
  //if( temp > (float) 2147483647.0 || temp < (float) -2147483648.0 )
  if( temp > (float) 8388607.0 || temp < (float) -8388608.0 )
    temp_l = (INT32) 0xFFFFFFFF;
  else
    temp_l = (INT32) temp;

  /* Store INT32 value in byte array */
  ptr = (BYTE *) &temp_l;
  degrees[0] = *ptr++;
  degrees[1] = *ptr++;
  degrees[2] = *ptr++;
  degrees[3] = *ptr;
}

/* ------------------------------------------------------------------------ */
