/* ------------------------------------------------------------------------
File   : store.h

Descr  : Constants and prototypes for parameter EEPROM storage routines.

History: 21SEP.00; Henk B&B; Definition.
               01;  "    " ; Added space for global variable storage;
                             added space for digital output settings storage.
         28MAY.01;  "    " ; Added space for test purposes.
         23JUL.01;  "    " ; Added PDO stuff and more stuff for Digital I/O;
                             increased STORE_INFO_SIZE from 3 to 4;
                             decreased STORE_BLOCK_SIZE from 32 to 16;
                             rearranged variable storage.
         24JUL.01;  "    " ; Go from WORD to BYTE addresses, which is alright
                             upto 256 bytes of EEPROM storage...
         31OCT.01;  "    " ; Don't use EEPROM address 0.
         01FEB.08;  "    " ; Version for ATCAN board.
--------------------------------------------------------------------------- */

#ifndef STORE_H
#define STORE_H

/* The number of individual data storage blocks */
#define STORE_BLOCK_CNT                 8

/* Maximum size of a data block (plus length word), in bytes (16) */
#define STORE_BLOCK_SIZE                0x10

/* Size of the info on a data block, in bytes (3 in use, 1 unused) */
#define STORE_INFO_SIZE                 4

/* Byte that signals the presence of a valid stored data block */
#define STORE_VALID_CHAR                'V'

/* ------------------------------------------------------------------------ */
/* EEPROM indices and addresses */

/* Parameter- and info-block indices */
#define STORE_PDO                       0
#define STORE_GUARDING                  1
#define STORE_ADC_BSENSOR               2
#define STORE_CAN                       3

/* Other */
#define STORE_SN                        0xFF

/* EEPROM address offset for info blocks */
#define STORE_INFO_ADDR                 0x01

/* EEPROM address offset for data blocks, stored behind the info blocks */
#define STORE_DATA_ADDR                 (STORE_INFO_ADDR + \
                                         STORE_BLOCK_CNT*STORE_INFO_SIZE)

/* EEPROM address offset for (more radiation-tolerant) variable storage */
#define STORE_VAR_ADDR                  (STORE_DATA_ADDR + \
                                         STORE_BLOCK_CNT*STORE_BLOCK_SIZE)

/* Using the above constants STORE_VAR_ADDR = 1 + 8*4 + 8*16 = 161 = 0xA1,
   which means there are still up to 95 = 0x5F EEPROM locations (bytes)
   available for the stuff shown below */

/* ------------------------------------------------------------------------ */
/* EEPROM variable storage:
   (Global) variables that do not change very often are stored in EEPROM
   after initialisation and reread from EEPROM before every use;
   this makes the application more radiation-tolerant */

/* CAN stuff */
#define EE_NODEID                       (STORE_VAR_ADDR + 0x00)
#define EE_RTRIDHI                      (STORE_VAR_ADDR + 0x01)
#define EE_RTRIDLO                      (STORE_VAR_ADDR + 0x02)
#define EE_RTR_DISABLED                 (STORE_VAR_ADDR + 0x03)
#define EE_CANOPEN_OPSTATE_INIT         (STORE_VAR_ADDR + 0x04)
#define EE_CAN_BUSOFF_MAXCNT            (STORE_VAR_ADDR + 0x05)

/* B-sensor ADC stuff */
#define EE_ADCMSK_BSENSOR               (STORE_VAR_ADDR + 0x08)
#define EE_ADCCONFIGB_BSENSOR           (STORE_VAR_ADDR + 0x09)
#define EE_ADCCONFIGT_BSENSOR           (STORE_VAR_ADDR + 0x0A)
#define EE_ADCCONFREGB2_BSENSOR         (STORE_VAR_ADDR + 0x0B)
#define EE_ADCCALIBBEFORESCAN_BSENSOR   (STORE_VAR_ADDR + 0x0C)
#define EE_ADCOPTODELAY_BSENSOR         (STORE_VAR_ADDR + 0x0D)
#define EE_ADCDEGREESINPDO_BSENSOR      (STORE_VAR_ADDR + 0x0E)
#define EE_ADCBATSCAN3PDO_BSENSOR       (STORE_VAR_ADDR + 0x0F)

/* Guarding stuff */
#define EE_LIFETIMEFACTOR               (STORE_VAR_ADDR + 0x10)
#define EE_HEARTBEATTIME                (STORE_VAR_ADDR + 0x11)

/* PDO stuff (reserve enough space for the settings of multiple PDOs);
   we currently have 9 PDOs in MDT-DCS */
#define EE_PDO_MAX                      10
#define EE_PDO_TTYPE                    (STORE_VAR_ADDR + 0x20)
#define EE_PDO_ETIMER                   (EE_PDO_TTYPE + EE_PDO_MAX)
#define EE_TPDO_ONTIMER                 (EE_PDO_ETIMER + EE_PDO_MAX)
/* Note: in fact we only need the ONTIMER variables for TPDOs, so we could
   save space here by only reserving space for TPDOs (4 in this case);
   we could even squeeze these booleans in one byte (when less than 8 TPDOs) */

/* Next free location, with values as above: 0x..+.. = 0xNN */

/* ------------------------------------------------------------------------ */
/* EEPROM storage for addresses 256 and up */

/* Storage for a serial number */
/* =========================== */

/* Sizes */
#define STORE_SN_SIZE                   4

/* Location where the Serial Number is stored (there is no copy in RAM).
   The serial number is followed by a 2-byte CRC and a 'valid' byte */
#define STORE_SN_ADDR                   0x100
#define STORE_SN_VALID_ADDR             (STORE_SN_ADDR+STORE_SN_SIZE+2)

/* ------------------------------------------------------------------------ */
/* Error IDs */

#define STORE_OKAY                      0x00
#define STORE_ERR_CRC                   0x01
#define STORE_ERR_LENGTH                0x02
#define STORE_ERR_INFO                  0x04

/* ------------------------------------------------------------------------ */
/* Function prototypes */

BOOL storage_save_parameters  ( BYTE od_subindex );
BOOL storage_set_defaults     ( BYTE od_subindex );
void storage_check_load_status( void );
BOOL storage_write_block      ( BYTE storage_index,
                                BYTE size,
                                BYTE *block );
BOOL storage_read_block       ( BYTE storage_index,
                                BYTE expected_size,
                                BYTE *block );
/* ------------------------------------------------------------------------ */
#endif /* STORE_H */
