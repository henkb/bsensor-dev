/* ------------------------------------------------------------------------
File   : ds2401.h

Descr  : Headerfile for ds2401.c.

History: 30DEC.02; Henk B&B; Definition.
         20JUN.08; Henk B&B; Define READROM command as 0x33 to be compatible
                             with the DS2405.
         26AUG.08; Henk B&B; Added DS2401_MATCHROM_CMD and ds2401_match().
         23FEB.09; Henk B&B; Removed ds2401_search_single().
         27NOV.12; Henk B&B; Added support for the DS2413;
                             renamed ds2401_match() to ds2405_toggle().
--------------------------------------------------------------------------- */

#ifndef DS2401_H
#define DS2401_H

/* ------------------------------------------------------------------------ */

#define DS2401_FAMILY_CODE          0x01
#define DS2405_FAMILY_CODE          0x05
#define DS2413_FAMILY_CODE          0x3A

#define DS2401_READROM_CMD          0x33
#define DS2401_MATCHROM_CMD         0x55
#define DS2401_SEARCHROM_CMD        0xF0
#define DS2401_SEARCHROM_ACTIVE_CMD 0xEC

#define DS2413_PIO_READ_CMD         0xF5
#define DS2413_PIO_WRITE_CMD        0x5A

#define DS2401_BYTES                8
#define DS2401_BITS                 64

/* Search-ROM results */
#define DS2401_ZERO                 0
#define DS2401_ONE                  1
#define DS2401_DUAL                 2
#define DS2401_NONE                 3

/* ------------------------------------------------------------------------ */
/* Configuration */

/* The One-Wire basic bit operations on the first ID chip */
#define DS2401_TO_INPUT()           CLEARBIT( DDRB, 4 )
#define DS2401_TO_OUTPUT()          SETBIT( DDRB, 4 )
#define DS2401_HIGH()               (PINB & BIT(4))
#define DS2401_LOW()                (!DS2401_1_HIGH())
#define DS2401_SET()                SETBIT( PORTB, 4 )
#define DS2401_CLEAR()              CLEARBIT( PORTB, 4 )

/* ------------------------------------------------------------------------ */
/* Function prototypes */

BOOL ds2401_read       ( BYTE *id );
BYTE ds2401_crc        ( BYTE id[DS2401_BYTES] );

void ds2401_search_init( void );
BOOL ds2401_search_next( BOOL active, BYTE *id );

BOOL ds2405_toggle     ( BYTE *id, BYTE *output_val );
BOOL ds2413_toggle     ( BYTE *id, BYTE *output_val );

/* ------------------------------------------------------------------------ */
#endif /* DS2401_H */
