/* ------------------------------------------------------------------------
File   : b_ident.c

Descr  : Function for reading a NIKHEF ATLAS B-sensor module Identifier
         (from a DALLAS DS2401 chip) in the MDT-DCS system.
         The function is called by the SDO server.

History: 17MAR.03; Henk B&B; Start of development.
         20AUG.04; Henk B&B; Because order of readout (of 1st or 2nd set
                             of 4 bytes of the Identifier) cannot be
                             guaranteed by the CANopen OPC-server,
                             the order can now be any.
         26AUG.08; Henk B&B; Added b_ident_match() function.
         23FEB.09; Henk B&B; Added b_ident_is_zeroes() function and use it
                             wherever b_ident_crc_correct() is used.
         27NOV.12; Henk B&B; Added support for the DS2413.
--------------------------------------------------------------------------- */

#include "general.h"
#include "ds2401.h"

#define B_IDENT_MAX_TRIES 3

/* ------------------------------------------------------------------------ */
/* Globals */

/* Temporary storage for the identifier in RAM */
static BYTE BmoduleId[DS2401_BYTES] = { 0xFF };

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BOOL b_ident_crc_correct( void );
static BOOL b_ident_all_zeroes( void );
static void b_ident_invalidate( void );

/* ------------------------------------------------------------------------ */

BOOL b_ident_get( BYTE mod_no, BYTE subindex, BYTE *id )
{
  BOOL result = TRUE;
  BYTE i;

  switch( subindex )
    {
    case 1:
    case 2:
      {
        BYTE trial;
        BOOL read_okay;

        /* Try to read the Identifier a maximum of B_IDENT_MAX_TRIES times */
        trial = 0;
        while( trial < B_IDENT_MAX_TRIES )
          {
            read_okay = ds2401_read( BmoduleId );

            if( read_okay &&
                b_ident_crc_correct() == TRUE &&
                b_ident_all_zeroes() == FALSE )
              /* Okay, all's well ! */
              break;

            ++trial;
            read_okay = FALSE;
          }

        /* Did we succeed reading a correct Identifier ? */
        if( read_okay == TRUE )
          {
            if( subindex == 1 )
              /* Copy the first set of 4 Identifier bytes to 'id[]' */
              for( i=0; i<4; ++i ) id[i] = BmoduleId[i];
            else
              /* Copy the second set of 4 Identifier bytes to 'id[]' */
              for( i=0; i<4; ++i ) id[i] = BmoduleId[4+i];
          }
        else
          {
            result = FALSE;
          }

        /* Invalidate all Identifier bytes just in case.. */
        b_ident_invalidate();
      }
      break;

    default:
      result = FALSE;
      break;
    }
  return result;
}

/* ------------------------------------------------------------------------ */

BOOL b_ident_match( BYTE mod_no, BYTE *output_val )
{
  BOOL read_okay;
  BOOL result = FALSE;

  read_okay = ds2401_read( BmoduleId );

  if( read_okay &&
      b_ident_crc_correct() == TRUE &&
      b_ident_all_zeroes() == FALSE )
    {
      /* Okay, all's well ! */

      if( BmoduleId[0] == DS2405_FAMILY_CODE )
        {
          /* Toggle the output bit (of the DS2405 device):
             output_val will contain the value of the bit 8 times over:
             an output value of 0 means the B-sensor is selected,
             an output value of 1 means the B-sensor is deselected */
          if( ds2405_toggle(BmoduleId, output_val) == TRUE )
            result = TRUE;
        }
      else if( BmoduleId[0] == DS2413_FAMILY_CODE )
        {
          /* Toggle the output bit (of the DS2413 device):
             output_val will contain the output status byte */
          if( ds2413_toggle(BmoduleId, output_val) == TRUE )
            result = TRUE;
        }
    }

  /* Invalidate all Identifier bytes just in case.. */
  b_ident_invalidate();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL b_ident_search( BYTE subindex, BYTE *id )
{
  BOOL result = TRUE;
  BYTE i;

  switch( subindex )
    {
    case 0:
      ds2401_search_init();
      for( i=0; i<DS2401_BYTES; ++i ) BmoduleId[i] = 0x00;
      break;

    case 1:
    case 3:
    case 5:
      if( subindex == 1 || subindex == 5 )
        /* Find all */
        result = ds2401_search_next( FALSE, BmoduleId );
      else
        /* Find 'active-only' */
        result = ds2401_search_next( TRUE, BmoduleId );

      if( result == TRUE &&
          b_ident_crc_correct() == TRUE &&
          b_ident_all_zeroes() == FALSE )
        {
          /* Copy the first set of 4 Identifier bytes to 'id[]' */
          for( i=0; i<4; ++i ) id[i] = BmoduleId[i];

          if( subindex == 5 )
            {
              if( BmoduleId[0] == DS2405_FAMILY_CODE )
                {
                  /* Toggle the output bit (of the DS2405 device):
                     id[0] will contain the value of the bit 8 times over */
                  if( ds2405_toggle(BmoduleId, id) == FALSE )
                    result = FALSE;
                }
              else if( BmoduleId[0] == DS2413_FAMILY_CODE )
                {
                  /* Toggle the output bit (of the DS2413 device):
                     id[0] will contain the output status byte */
                  if( ds2413_toggle(BmoduleId, id) == FALSE )
                    result = FALSE;
                }
              else
                {
                  result = FALSE;
                }
            }
        }
      else
        {
          result = FALSE;
        }
      break;

    case 2:
    case 4:
      if( result == TRUE &&
          b_ident_crc_correct() == TRUE &&
          b_ident_all_zeroes() == FALSE )
        {
          /* Copy the second set of 4 Identifier bytes to 'id[]' */
          for( i=0; i<4; ++i ) id[i] = BmoduleId[4+i];
        }
      else
        {
          result = FALSE;
        }
      break;

    default:
      result = FALSE;
      break;
    }
  return result;
}

/* ------------------------------------------------------------------------ */

static BOOL b_ident_crc_correct( void )
{
  if( ds2401_crc(BmoduleId) == BmoduleId[DS2401_BYTES-1] )
    return TRUE;
  else
    return FALSE;
}

/* ------------------------------------------------------------------------ */

static BOOL b_ident_all_zeroes( void )
{
  BOOL all_zero = TRUE;
  BYTE i;
  for( i=0; i<DS2401_BYTES; ++i )
    if( BmoduleId[i] != 0x00 ) all_zero = FALSE;
  return all_zero;
}

/* ------------------------------------------------------------------------ */

static void b_ident_invalidate( void )
{
  BYTE i;
  for( i=0; i<DS2401_BYTES; ++i ) BmoduleId[i] = 0xFF;
}

/* ------------------------------------------------------------------------ */
