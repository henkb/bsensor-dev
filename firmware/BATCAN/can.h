/* ------------------------------------------------------------------------
File   : can.h

Descr  : Definitions and declarations for CAN message-to-controller-buffer
         mappings, CANopen message sending and low-level CAN-controller access
         functions.

History: 09MAR.08; Henk B&B; Start development for AT90CANxxx.
--------------------------------------------------------------------------- */

#ifndef CAN_H
#define CAN_H

#include "at90can.h"
#include "canopen.h"

/* ------------------------------------------------------------------------ */
/* Globals */

extern BYTE NodeID;
extern BYTE CANopenErrorReg;

/* ------------------------------------------------------------------------ */
/* CANopen object mapping to the AT90CANxxx CAN-controller buffers */

/* Message/buffer number for the CANopen objects */
#define CC_NMT                         0
#define CC_SYNC                        1
#define CC_EMERGENCY                   2
#define CC_SDOTX                       3
#define CC_SDORX                       4
#define CC_NODEGUARD                   5
#define CC_NODEGUARD_RTR               6
#define CC_TPDO4                       7
#define CC_TPDO4_RTR                   8
/* The rest of the buffers are used as alternative transmit buffers */
#define CC_TRANSMIT                    9
#define CC_TRANSMIT_BUFFERS            CC_NO_OF_MOBS-CC_TRANSMIT

/* Same COB-ID for 2 different CANopen objects */
#define CC_BOOTUP                      CC_NODEGUARD

/* If no CAN-message is available the following ID is returned */
#define NO_OBJECT                       32

/* ------------------------------------------------------------------------ */
/* CANopen object message lengths */

#define CC_NMT_LEN                     2
#define CC_SYNC_LEN                    0
#define CC_EMERGENCY_LEN               8
#define CC_SDOTX_LEN                   8
#define CC_SDORX_LEN                   8
#define CC_NODEGUARD_LEN               1
#define CC_TPDO4_LEN                   5
#define CC_BOOTUP_LEN                  CC_NODEGUARD_LEN

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void can_init             ( BOOL init_msg_buffer );
BOOL can_msg_available    ( void );
BYTE can_read             ( BYTE *pdlc, BYTE **ppmsg_data );
void can_write            ( BYTE object_no, BYTE len, BYTE *msg_data );
void can_write_bootup     ( void );
void can_write_emergency  ( BYTE err_low,
                            BYTE err_high,
                            BYTE mfct_field_0,
                            BYTE mfct_field_1,
                            BYTE mfct_field_2,
                            BYTE mfct_field_3,
                            BYTE canopen_err_bit );
BOOL can_busy             ( BYTE object_no );
void can_check_for_errors ( void );
BYTE canopen_init_state   ( void );
BOOL can_set_opstate_init ( BOOL enable );
BOOL can_get_opstate_init ( void );
BOOL can_set_busoff_maxcnt( BYTE cntr );
BYTE can_get_busoff_maxcnt( void );
BYTE can_get_recvmsg_cnt  ( void );
void can_get_formaterr_cnt( BYTE *byt );
BOOL can_store_config     ( void );

/* ------------------------------------------------------------------------ */
#endif /* CAN_H */
