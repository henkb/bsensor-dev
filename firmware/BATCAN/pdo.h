/* ------------------------------------------------------------------------
File   : pdo.h

Descr  : Include file for the PDO routines in pdo.c.

History: 19JUL.00; Henk B&B; Definition.
           AUG.00; Henk B&B; Addition of PDOs for digital input and output.
           NOV.00; Henk B&B; Addition of PDO for analogue output (DAC).
         03OCT.09; Henk B&B; Remove most TPDOs and all RPDOs for BATCAN.
--------------------------------------------------------------------------- */

#ifndef PDO_H
#define PDO_H

/* Number of Transmit-PDOs */
#define TPDO_CNT            4

/* Number of Receive-PDOs */
#define RPDO_CNT            0

/* Which PDO is used for what */
#define TPDO_DUMMY_1        (1-1)
#define TPDO_DUMMY_2        (2-1)
#define TPDO_DUMMY_3        (3-1)
#define TPDO_ADC_BSENSOR    (4-1)

/* ------------------------------------------------------------------------ */
/* Globals */

/* For timer-triggered PDO transmissions */
extern BOOL TPdoOnTimer[];

/* Keeps track of time for the timer-triggered PDO transmissions */
extern BYTE TPdoTimerCntr[];

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void pdo_init          ( void );
void tpdo_scan         ( void );
void pdo_on_nmt        ( BYTE nmt_request );
void tpdo_on_sync      ( void );
void tpdo4_on_rtr      ( void );

BOOL tpdo_get_comm_par ( BYTE pdo_no,
                         BYTE od_subind,
                         BYTE *nbytes,
                         BYTE *par );
BOOL rpdo_get_comm_par ( BYTE pdo_no,
                         BYTE od_subind,
                         BYTE *nbytes,
                         BYTE *par );

BOOL tpdo_get_mapping  ( BYTE pdo_no,
                         BYTE od_subind,
                         BYTE *nbytes,
                         BYTE *par );
BOOL rpdo_get_mapping  ( BYTE pdo_no,
                         BYTE od_subind,
                         BYTE *nbytes,
                         BYTE *par );

BOOL tpdo_set_comm_par ( BYTE pdo_no,
                         BYTE od_subind,
                         BYTE nbytes,
                         BYTE *par );

BOOL pdo_store_config  ( void );

/* ------------------------------------------------------------------------ */
#endif /* PDO_H */
