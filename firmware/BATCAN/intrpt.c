/* ------------------------------------------------------------------------
File   : intrpt.c

Descr  : Empty interrupt routine for all unused interrupts on the
         AT90CANxx microcontroller, to handle any spurious interrupts.

History: 24FEB.08; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#include "general.h"

/* ------------------------------------------------------------------------ */

/* Intrpt in use: iv_RESET */
#pragma interrupt_handler empty_handler:iv_INT0
#pragma interrupt_handler empty_handler:iv_INT1
#pragma interrupt_handler empty_handler:iv_INT2
#pragma interrupt_handler empty_handler:iv_INT3
#pragma interrupt_handler empty_handler:iv_INT4
#pragma interrupt_handler empty_handler:iv_INT5
#pragma interrupt_handler empty_handler:iv_INT6
#pragma interrupt_handler empty_handler:iv_INT7
#pragma interrupt_handler empty_handler:iv_TIMER2_COMP
/* Intrpt in use: iv_TIMER2_OVF */
#pragma interrupt_handler empty_handler:iv_TIMER1_CAPT
#pragma interrupt_handler empty_handler:iv_TIMER1_COMPA
#pragma interrupt_handler empty_handler:iv_TIMER1_COMPB
#pragma interrupt_handler empty_handler:iv_TIMER1_COMPC
/* Intrpt in use: iv_TIMER1_OVF */
#pragma interrupt_handler empty_handler:iv_TIMER0_COMP
#pragma interrupt_handler empty_handler:iv_TIMER0_OVF
/* Intrpt in use: iv_CAN */
#pragma interrupt_handler empty_handler:iv_CAN_TIM_OVF
#pragma interrupt_handler empty_handler:iv_SPI_STC
#pragma interrupt_handler empty_handler:iv_USART0_RX
#pragma interrupt_handler empty_handler:iv_USART0_DRE
#pragma interrupt_handler empty_handler:iv_USART0_TX
#pragma interrupt_handler empty_handler:iv_ANALOG_COMP
#pragma interrupt_handler empty_handler:iv_ADC
#pragma interrupt_handler empty_handler:iv_EE_READY
#pragma interrupt_handler empty_handler:iv_TIMER3_CAPT
#pragma interrupt_handler empty_handler:iv_TIMER3_COMPA
#pragma interrupt_handler empty_handler:iv_TIMER3_COMPB
#pragma interrupt_handler empty_handler:iv_TIMER3_COMPC
#pragma interrupt_handler empty_handler:iv_TIMER3_OVF
#pragma interrupt_handler empty_handler:iv_USART1_RX
#pragma interrupt_handler empty_handler:iv_USART1_DRE
#pragma interrupt_handler empty_handler:iv_USART1_TX
#pragma interrupt_handler empty_handler:iv_TWI
#pragma interrupt_handler empty_handler:iv_SPM_READY

void empty_handler( void )
{
  /* Nothing... */
}

/* ------------------------------------------------------------------------ */
