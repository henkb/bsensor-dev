/* ------------------------------------------------------------------------
File   : adc_bsensor.c

Descr  : Routines for read-out and control of the MDT-DCS B-sensors with
         onboard CRYSTAL CS5524 24-bit ADC, using the ELMB.
	 This version for strings of addressable B-sensor modules.

	 CS5524 ADC connections:
	 -----------------------
	 AIN0: connected to Hall-sensor X (A1A0 is 'don't care').
	 AIN1: connected to Hall-sensor Y (A1A0 is 'don't care').
	 AIN2: connected to Hall-sensor Z (A1A0 is 'don't care').
	 AIN3:
	  A1A0=00: ca. 87 mV (fixed: connected to a resistor in the
	           Hall-sensor chain; serves as the Hall-sensor full-scale
		   calibration input)
	  A1A0=01: NTC value -> circa 72 mV < value < 2000 mV
	  A1A0=10:   0 degrees C calibration input = 2.065 Volt
	           !! but measured versus the Vref=2.5V --> 0.435 is measured
	  A1A0=11: 100 degrees C calibration input = 0.072 Volt
	           !! but measured versus the Vref=2.5V --> 2.428 is measured

History: ..APR.04; Henk B&B; Start of development of version for BsCAN2 system,
                             implemented on an ELMB based MDT-DCS module,
                             based on both MDT-DCS firmware and an older
			     version of BsCAN firmware (for a SPICAN module).
--------------------------------------------------------------------------- */

#include <math.h>

#include "general.h"
#include "adc.h"
#include "adc_bsensor.h"
#include "bsmicro.h"
#include "can.h"
#include "crc.h"
#include "cs5523.h"
#include "eeprom.h"
#include "objects.h"
#include "store.h"
#include "timer103.h"

/* ------------------------------------------------------------------------ */
/* Globals */

/* Currently selected string */
BYTE        StringId;

#define adcb_string_port_select(string_no) StringId=string_no

/* Number of ADCs in the current configuration */
static BYTE AdcCnt;

/* Array holding a list of B-sensor addresses in the current configuration */
static BYTE AdcAddrList[ADCB_MAX_CNT];

/* Array holding mapping of B-sensor address to 'string' */
static BYTE AdcStringMap[ADCB_MAX_CNT];

/* Array holding number of B-sensors in each string */
static BYTE AdcInString[ADCB_MAX_STRINGS];

/* ADC configuration (word rate, voltage range, unipolar/bipolar)
   for the Hall-sensors */
static BYTE AdcConfigB;    /* (stored in EEPROM) */

/* ADC configuration (word rate, voltage range, unipolar/bipolar)
   for the T-sensor */
static BYTE AdcConfigT;    /* (stored in EEPROM) */

/* ADC Configuration Register to use
   (chop frequency: depends on selected wordrate) */
static BYTE AdcConfRegB_2; /* (stored in EEPROM) */

/* Storage space for error bits concerning the ADCs */
static BYTE AdcError[ADCB_MAX_CNT];

/* Signal hold time due to opto-coupler (in microseconds) */
BYTE        AdcOptoDelayBsensor; /* (stored in EEPROM) */

/* Signal hold time when talking to the B-sensor microcontroller
   (in microseconds) */
BYTE        MicroDelayBsensor; /* (stored in EEPROM) */

/* Signal hold time when selecting the B-sensor microcontroller/ADC
   (in microseconds) */
BYTE        MicroSelectDelayBsensor; /* (stored in EEPROM) */

/* ------------------------------------------------------------------------ */
/* Global variables for ADC channel scanning operations */

/* ADC and ADC-channel indices */
static BYTE AdcNo;
static BYTE AdcChanNo;

/* ADC scanning-operation-in-progress boolean */
static BOOL AdcScanInProgress;

/* ADC conversion-in-progress boolean */
static BOOL AdcConvInProgress;

/* Temperature in PDO in units of degrees instead of ADC-counts */
static BOOL AdcDegreesInPdo;

/* ADC recovery after time-out */
static BOOL AdcRecovery;

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static void adcb_await_calib_all( BOOL send_emergency, BYTE err_id,
				  BYTE *no_of_err );

static void adcb_init_csr     ( BYTE addr );

static BOOL adcb_scan_next    ( void );

static void adcb_init_config  ( BYTE init_errstat );
static void adcb_load_config  ( void );
static BOOL adcb_load_map     ( void );
static BOOL adcb_store_map    ( void );

static void adcb_select       ( BYTE addr );
static void adcb_brc_select   ( BYTE string_no );
static void adcb_brc_deselect ( BYTE string_no );
static void adcb_brc_select_all_strings( void );
/* This function not needed:
   ###For MDT-DCS without SPI-board YES, NEED IT,
   to prevent interference between strings with shared SCK/SDI/SDO */
#define adcb_deselect()       bsmicro_brc_deselect()

static void adcb_convert_ohms_to_degrees( float ohms, BYTE *degrees );

/* ------------------------------------------------------------------------ */

void adcb_preinit( BOOL set_ddr )
{
  BYTE str_no;

  if( set_ddr == TRUE )
    {
      /* Initialise I/O-pins for the B-sensor ADCs */
      ADCB_CLEAR_SCLK_0(); ADCB_CLEAR_SDI_0(); ADCB_INIT_DDR_0();
      ADCB_CLEAR_SCLK_1(); ADCB_CLEAR_SDI_1(); ADCB_INIT_DDR_1();
      ADCB_CLEAR_SCLK_2(); ADCB_CLEAR_SDI_2(); ADCB_INIT_DDR_2();
      ADCB_CLEAR_SCLK_3(); ADCB_CLEAR_SDI_3(); ADCB_INIT_DDR_3();
    }

  /* Make sure to get any B-sensors-with-micro
     in a proper (listening) state at powerup (why?) */
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      adcb_brc_select( str_no );
      cs5523_serial_init();
      adcb_brc_deselect( str_no );
    }
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      /* Get all ADC serial interfaces on this string in a defined state... */
      adcb_brc_select( str_no );
      cs5523_serial_init();

      /* Get all ADCs on this string in a defined state... */
      {
	/* Write 'reset system' command to the Configuration Register
	   by setting the RS-bit */
	BYTE config[3];
	config[0] = CS23_CNF_RESET_SYSTEM;
	config[1] = 0;
	config[2] = 0;
	cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, config ); 

	timer2_delay_ms( 400 );

	/* Reset the RS-bit */
	config[0] = 0;
	cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, config );

	/* Read Configuration Register once more to reset the RV-bit */
	cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, config );
      }
      adcb_brc_deselect( str_no );
      WDR();
    }
}

/* ------------------------------------------------------------------------ */

BOOL adcb_init( BYTE *no_of_err, BOOL send_emergency )
{
  /* If _FAST_INIT_ is defined this whole procedure
     takes in the order of 2 s maximum;
     if not it takes about N*0.55 s, with 'N'
     the number of B-sensor modules in the configuration */
  BYTE addr, adc_no;
  BYTE wordrate;

  /* Initialise B-sensor ADC configuration parameters */
  adcb_load_config();

  /* Get B-sensor string map from EEPROM */
  *no_of_err = 0;
  if( adcb_load_map() == FALSE )
    {
      /* No map or faulty map: set all B-sensors to 'not connected' */
      BYTE addr;
      for( addr=0; addr<ADCB_MAX_CNT; ++addr ) AdcStringMap[addr] = 0xFF;
      *no_of_err = 0xFF;
    }

  /* Initialize configuration data/lists */
  adcb_init_config( TRUE );

  /* Set wordrate-dependent setting for ADC Configuration Register:
     Chop Frequency:  256 Hz (at freq <= 30 Hz), 4096 Hz (other freq)
     		      (recommended by Crystal, CS5524 datasheet pg 14) */

  /* Let it depend on the Hall-sensor wordrate */
  wordrate = ((AdcConfigB & CS23_CSR_WORDRATE_MASK) >>
	      CS23_CSR_WORDRATE_SHIFT);
  if( wordrate == CS23_WORDRATE_61 ||
      wordrate == CS23_WORDRATE_84 ||
      wordrate == CS23_WORDRATE_101 )
    AdcConfRegB_2 = ADCB_CNFREG_2_CHOP4096;
  else
    AdcConfRegB_2 = ADCB_CNFREG_2_CHOP256;

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_ADCCONFREGB2_BSENSOR ) != AdcConfRegB_2 )
    eeprom_write( EE_ADCCONFREGB2_BSENSOR, AdcConfRegB_2 );
#endif /* __VARS_IN_EEPROM__ */

  /* Initialize variables for scanning operations */
  //AdcNo             = 0;
  //AdcChanNo         = 0;
  AdcScanInProgress = FALSE;
  AdcConvInProgress = FALSE;
  //AdcDegreesInPdo   = FALSE;
  AdcDegreesInPdo   = TRUE;
  AdcRecovery       = TRUE;

  /* Initialise I/O-pins for the B-sensor ADCs */
  ADCB_CLEAR_SCLK_0(); ADCB_CLEAR_SDI_0(); ADCB_INIT_DDR_0();
  ADCB_CLEAR_SCLK_1(); ADCB_CLEAR_SDI_1(); ADCB_INIT_DDR_1();
  ADCB_CLEAR_SCLK_2(); ADCB_CLEAR_SDI_2(); ADCB_INIT_DDR_2();
  ADCB_CLEAR_SCLK_3(); ADCB_CLEAR_SDI_3(); ADCB_INIT_DDR_3();

#define _FAST_INIT_

#ifdef _FAST_INIT_
  /* Reset (configured) B-sensor ADCs */
  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      /* Reset and calibrate all B-sensors present in the configuration */
      addr = AdcAddrList[adc_no];

      /* Perform reset */
      if( adcb_reset( addr, send_emergency ) == FALSE )
	{
	  ++(*no_of_err);
	}
      WDR();
    }

  /* Calibrate (configured) B-sensor ADCs simultaneously */
  adcb_calibrate_all( no_of_err, send_emergency );

  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      addr = AdcAddrList[adc_no];
      if( AdcError[addr] == 0 ) adcb_init_csr( addr );
    }

#else

  /* Reset and calibrate (configured) B-sensor ADCs one-by-one */
  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      /* Reset and calibrate all B-sensors present in the configuration */
      addr = AdcAddrList[adc_no];

      /* Perform calibration only if reset succeeded */
      if( adcb_reset( addr, send_emergency ) == TRUE )
	{
	  if( adcb_calibrate( addr, send_emergency ) == TRUE )
	    adcb_init_csr( addr );
	  else
	    ++(*no_of_err);
	}
      else
	{
	  ++(*no_of_err);
	}
      WDR();
    }
#endif /* _FAST_INIT_ */

  if( *no_of_err != 0 )
    return FALSE;
  else
    return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_read( BYTE addr, BYTE chan_no, BYTE *conversion_data )
{
  BOOL result;

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      /* Can't do anything if the ADC is in use... */
      /*conversion_data[0] = 0xFF;
	conversion_data[1] = 0xFF;
	conversion_data[2] = 0xFE;*/
      return FALSE;
    }
  else
    {

/*#define __DO_NOT_CONVERT_WHEN_ERROR__ */

#ifdef __DO_NOT_CONVERT_WHEN_ERROR__
      if( AdcError[addr] != 0 )
	{
	  /* Something has gone wrong with this ADC previously */
	  /*conversion_data[0] = 0xFF;
	    conversion_data[1] = 0xFF;
	    conversion_data[2] = 0xFD;*/
	  return FALSE;
	}
      else
#endif /* __DO_NOT_CONVERT_WHEN_ERROR__ */
	{
	  /* Perform an ADC conversion */
	  BYTE a1a0, cs1cs0;
	  BYTE csr_data[1*2*2];
	  BYTE adc_config;
	  BYTE adc_conf_reg[3];

#ifdef __VARS_IN_EEPROM__
	  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
	  MicroDelayBsensor = eeprom_read( EE_MICRODELAY_BSENSOR );
	  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif
	  adcb_select( addr );

	  /* Read/write Configuration Register in order
	     to set the Depth Pointer to 1 and
	     the Multiple Conversion bit to FALSE */
	  cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
	  adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
	  adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
	  adc_conf_reg[1] |= (1<<CS23_CNF_CSR_DEPTH_SHIFT);
	  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

	  /* Select the proper channel:
	     chan 0, 1, 2   : the Hall-sensors
	     chan 3, 4, 5, 6: 87mV, T-sensor, 0C calib, 100C calib */

	  if( chan_no > 2 )
	    {
	      a1a0   = chan_no - 3;
	      cs1cs0 = 0x03;
#ifdef __VARS_IN_EEPROM__
	      AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
	      adc_config = AdcConfigT;
	    }
	  else
	    {
	      a1a0   = 0x00; /* Don't care */
	      cs1cs0 = chan_no;
#ifdef __VARS_IN_EEPROM__
	      AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
	      adc_config = AdcConfigB;
	    }

	  /* Set A1-A0 and physical channel in LC 1 (+2)
	     while maintaining proper wordrate and gain */
	  csr_data[0] = (adc_config |
			 ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
			  CS23_CSR_PHYSCHAN_SEL_LO_MASK));
	  csr_data[1] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
			 ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
			  CS23_CSR_PHYSCHAN_SEL_HI_MASK));
	  csr_data[2] = 0x00;
	  csr_data[3] = 0x00;
	  cs5523_write_csr( 1, csr_data, ADCB_CSR_DEPTH );

	  /* Do a conversion of LC 1 and read the result */
	  result = cs5523_read_adc( 0, conversion_data );

	  /* Restore Configuration Register */
	  adc_conf_reg[2] |= ADCB_CONVERSION_MODE;
	  adc_conf_reg[1] |= ADCB_CSR_DEPTH_MASK;
	  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

	  adcb_deselect();

	  if( result == FALSE )
	    {
	      /* Conversion timed out ! */
	      AdcError[addr] |= ADC_ERR_TIMEOUT;

	      /* CANopen Error Code 0x5000: device hardware */
	      can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE,
				   addr, chan_no, 0,
				   ERRREG_MANUFACTURER );

	      return FALSE;
	    }
	}
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_status_summary( BYTE subindex, BYTE *status )
{
  BYTE i, cnt, byt, bit;

  if( subindex >= (ADCB_MAX_CNT+31)/32 ) return FALSE;

  /* Set status bits to all error... */
  for( i=0; i<4; ++i ) status[i] = 0xFF;

  /* One ADC status per bit, start at B-sensor address 32*subindex */
  cnt = ADCB_MAX_CNT - subindex*32;
  if( cnt > 32 ) cnt = 32;

  for( i=0; i<cnt; ++i )
    {
      byt = i / 8;
      bit = i & 7;

      /* If ADC status is okay, reset the appropriate bit */
      if( AdcError[subindex*32 + i] == 0 )
	status[byt] &= ~(1 << bit); 
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_status( BYTE addr, BYTE *status )
{
  if( addr >= ADCB_MAX_CNT ) return FALSE;

  *status = AdcError[addr];

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BYTE adcb_total_cnt( void )
{
  /* Return the total number of B-sensors connected to this node */
  return AdcCnt;
}

/* ------------------------------------------------------------------------ */

BYTE adcb_string_cnt( BYTE str_no )
{
  /* Return the number of B-sensors in string 'str_no' */
  return AdcInString[str_no];
}

/* ------------------------------------------------------------------------ */

BYTE adcb_mapping( BYTE addr )
{
  /* Return the string number to which B-sensor
     with address 'addr' is connected;
     if number > ADCB_MAX_STRINGS (or rather 0xFF == 'ADC_ABSENT')
     it means a B-sensor with this address is not present */
  return AdcStringMap[addr];
}

/* ------------------------------------------------------------------------ */

BYTE adcb_list( BYTE index )
{
  /* Return the address of the 'index'-th B-sensor of the list of
     B-sensors in the current configuration, or 0xFF) */
  if( index < AdcCnt ) return AdcAddrList[index];
  return 0xFF;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_get_config( BYTE addr, BYTE subindex, BYTE *nbytes, BYTE *par )
{
  *nbytes = 1;

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  switch( subindex )
    {
    case 0:
      /* Number of entries */
      par[0] = 26;
      break;

    case 1:
      par[0] = ADCB_CHANS_PER_ADC;
      break;

    case 2:
      par[0] = ((AdcConfigB & CS23_CSR_WORDRATE_MASK) >>
	        CS23_CSR_WORDRATE_SHIFT);
      break;

    case 3:
      par[0] = ((AdcConfigB & CS23_CSR_GAIN_MASK) >>
		CS23_CSR_GAIN_SHIFT);
      break;

    case 4:
      par[0] = (AdcConfigB & CS23_CSR_UNIPOLAR);
      break;

    case 5:
      par[0] = ((AdcConfigT & CS23_CSR_WORDRATE_MASK) >>
	        CS23_CSR_WORDRATE_SHIFT);
      break;

    case 6:
      par[0] = ((AdcConfigT & CS23_CSR_GAIN_MASK) >>
		CS23_CSR_GAIN_SHIFT);
      break;

    case 7:
      par[0] = (AdcConfigT & CS23_CSR_UNIPOLAR);
      break;

    case 9:
      {
	if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

	adcb_select( addr );

	/* Read the Configuration Register */
	cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, par );

	adcb_deselect();

	*nbytes = 4;
      }
      break;

    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
      {
	BYTE reg, phys_chan_no;

	if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

	if( subindex & 1 ) reg = CS23_CMD_GAIN_REG;
	else reg = CS23_CMD_OFFSET_REG;

	phys_chan_no = (subindex - 10) >> 1;

	adcb_select( addr );

	/* Read Register */
	cs5523_read_reg( reg, phys_chan_no, par );

	adcb_deselect();

	*nbytes = 4;
      }
      break;

    case 18:
    case 19:
    case 20:
    case 21:
      {
	BYTE csr_data[4*2*2], *csr, i;

	if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

	adcb_select( addr );

	/* Read current CSR content */
	cs5523_read_csr( csr_data );

	subindex -= 18;

	/* Extract required CSR data */
	csr = &csr_data[subindex << 2];
	for( i=0; i<4; ++i ) par[i] = csr[i];

	adcb_deselect();

	*nbytes = 4;
      }
      break;

    case 22:
      par[0] = AdcOptoDelayBsensor;
      break;

    case 23:
      par[0] = MicroDelayBsensor;
      break;

    case 24:
      par[0] = MicroSelectDelayBsensor;
      break;

    case 25:
      par[0] = addr;
      *nbytes = 4;
      break;

    case 26:
      par[0] = AdcRecovery;
      break;

    default:
      return FALSE;
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_set_config( BYTE addr, BYTE subindex, BYTE nbytes, BYTE *par )
{
  BYTE adc_no;

  /* If 'nbytes' is zero it means the data set size was
     not indicated in the SDO message */

  /* Can't do anything if the ADC is in use... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcConfRegB_2       = eeprom_read( EE_ADCCONFREGB2_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  switch( subindex )
    {
    case 2:
      {
	BYTE adc_conf_reg[3];

	if( nbytes > 1 || par[0] > CS23_WORDRATE_7 ) return FALSE;

	AdcConfigB &= ~CS23_CSR_WORDRATE_MASK;
	AdcConfigB |= (par[0] << CS23_CSR_WORDRATE_SHIFT);

	/* Prepare wordrate-dependent setting for ADC Configuration Register */
	if( par[0] == CS23_WORDRATE_61 ||
	    par[0] == CS23_WORDRATE_84 ||
	    par[0] == CS23_WORDRATE_101 )
	  AdcConfRegB_2 = ADCB_CNFREG_2_CHOP4096;
	else
	  AdcConfRegB_2 = ADCB_CNFREG_2_CHOP256;

	adc_conf_reg[0] = ADCB_CNFREG_0;
	adc_conf_reg[1] = ADCB_CNFREG_1;
	adc_conf_reg[2] = AdcConfRegB_2;

        /* Set this configuration in all connected B-sensor ADCs ! */
	for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	  {
	    if( TRUE ) //AdcError[AdcAddrList[adc_no]] == 0 )
	      {
		adcb_select( AdcAddrList[adc_no] );

                /* Write the updated setting to the Config Register */
                cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

                adcb_deselect();
              }
          }
      }
      break;

    case 3:
      if( nbytes > 1 || par[0] > CS23_GAIN_2V5 ) return FALSE;

      AdcConfigB &= ~CS23_CSR_GAIN_MASK;
      AdcConfigB |= (par[0] << CS23_CSR_GAIN_SHIFT);

      break;

    case 4:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      if( par[0] == 1 )
	AdcConfigB |= CS23_CSR_UNIPOLAR;
      else
	AdcConfigB &= ~CS23_CSR_UNIPOLAR;

      break;

    case 5:
      if( nbytes > 1 || par[0] > CS23_WORDRATE_7 ) return FALSE;

      AdcConfigT &= ~CS23_CSR_WORDRATE_MASK;
      AdcConfigT |= (par[0] << CS23_CSR_WORDRATE_SHIFT);

      break;

    case 6:
      if( nbytes > 1 || par[0] > CS23_GAIN_2V5 ) return FALSE;

      AdcConfigT &= ~CS23_CSR_GAIN_MASK;
      AdcConfigT |= (par[0] << CS23_CSR_GAIN_SHIFT);

      break;

    case 7:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      if( par[0] == 1 )
	AdcConfigT |= CS23_CSR_UNIPOLAR;
      else
	AdcConfigT &= ~CS23_CSR_UNIPOLAR;

      break;

    case 8:
      {
	BYTE adc_conf_reg[3];

	if( nbytes > 1 || par[0] > 1 ) return FALSE;

	if( par[0] == 1 )
	  adc_conf_reg[1] = ADCB_CNFREG_1 | CS23_CNF_POWER_SAVE;
	else
	  adc_conf_reg[1] = ADCB_CNFREG_1 & (~CS23_CNF_POWER_SAVE);

	adc_conf_reg[0] = ADCB_CNFREG_0;
	adc_conf_reg[2] = AdcConfRegB_2;

        /* Set this configuration in all connected B-sensor ADCs ! */
	for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	  {
	    if( TRUE ) //AdcError[AdcAddrList[adc_no]] == 0 )
	      {
		adcb_select( AdcAddrList[adc_no] );

		/* Write the new setting to the Config Register */
		cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

		adcb_deselect();
	      }
	  }
      }
      break;

    case 9:
      {
	if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

        /* Set this configuration in all connected B-sensor ADCs ! */
	for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	  {
	    if( TRUE ) //AdcError[AdcAddrList[adc_no]] == 0 )
	      {
		adcb_select( AdcAddrList[adc_no] );

		/* Write the Configuration Register */
		cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, par );

		adcb_deselect();
	      }
	  }
      }
      break;

    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
      {
	BYTE reg, phys_chan_no;

	if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

	if( subindex & 1 ) reg = CS23_CMD_GAIN_REG;
	else reg = CS23_CMD_OFFSET_REG;

	phys_chan_no = (subindex - 10) >> 1;

	adcb_select( addr );

	/* Write Register */
	cs5523_write_reg( reg, phys_chan_no, par );

	adcb_deselect();
      }
      break;

    case 18:
    case 19:
    case 20:
    case 21:
      {
	BYTE csr_data[4*2*2], *csr, i;

	if( !(nbytes == 4 || nbytes == 0) ) return FALSE;

	adcb_select( addr );

	/* Read current CSR content */
	cs5523_read_csr( csr_data );

	subindex -= 18;

	/* Adjust CSR content */
	csr = &csr_data[subindex << 2];
	for( i=0; i<4; ++i ) csr[i] = par[i];

	/* Write new CSR content */
	cs5523_write_csr( 4, csr_data, ADCB_CSR_DEPTH );

	adcb_deselect();
      }
      break;

    case 22:
      if( nbytes > 1 || par[0] < 10 ) return FALSE;

      AdcOptoDelayBsensor = par[0];

      break;

    case 23:
      if( nbytes > 1 || par[0] < 10 ) return FALSE;

      MicroDelayBsensor = par[0];

      break;

    case 24:
      if( nbytes > 1 || par[0] < 10 ) return FALSE;

      MicroSelectDelayBsensor = par[0];

      break;

    case 25:
      if( !(nbytes == 4 || nbytes == 0) ) return FALSE;
      if( par[0] >= ADCB_MAX_CNT || par[1] != 0xFF ||
	  par[2] != 0xEE /*|| par[3] != 0xFF*/ ) return FALSE;

      if( addr == 0xFF )
	{
	  /* Use value in par[3] to select string,
	     with erased (actual illegal) address */
	  switch( par[3] )
	    {
	    case 0xFF:
	      adcb_string_port_select( 0 );
	      break;
	    case 0xFE:
	      adcb_string_port_select( 1 );
	      break;
	    case 0xFD:
	      adcb_string_port_select( 2 );
	      break;
	    case 0xFC:
	      adcb_string_port_select( 3 );
	      break;
	    default:
	      return FALSE;
	    }
	  bsmicro_set_addr( addr, par[0] );
	}
      else
	{
	  /* Select the SPI-port on which this addressable B-sensor
	     is to be found */
	  if( AdcStringMap[addr] < ADCB_MAX_STRINGS )
	    {
	      adcb_string_port_select( AdcStringMap[addr] );
	      bsmicro_set_addr( addr, par[0] );
	    }
	  else
	    {
	      return FALSE; /* Not in B-sensor configuration */
	    }
	}
      break;

    case 26:
      if( nbytes > 1 || par[0] > 1 ) return FALSE;

      AdcRecovery = par[0];

      break;

    default:
      return FALSE;
    }

#ifdef __VARS_IN_EEPROM__
  /* Store values in EEPROM, if changed */
  if( eeprom_read( EE_ADCCONFIGB_BSENSOR ) != AdcConfigB )
    eeprom_write( EE_ADCCONFIGB_BSENSOR, AdcConfigB );
  if( eeprom_read( EE_ADCCONFIGT_BSENSOR ) != AdcConfigT )
    eeprom_write( EE_ADCCONFIGT_BSENSOR, AdcConfigT );
  if( eeprom_read( EE_ADCCONFREGB2_BSENSOR ) != AdcConfRegB_2 )
    eeprom_write( EE_ADCCONFREGB2_BSENSOR, AdcConfRegB_2 );
  if( eeprom_read( EE_ADCOPTODELAY_BSENSOR ) != AdcOptoDelayBsensor )
    eeprom_write( EE_ADCOPTODELAY_BSENSOR, AdcOptoDelayBsensor );
  if( eeprom_read( EE_MICRODELAY_BSENSOR ) != MicroDelayBsensor )
    eeprom_write( EE_MICRODELAY_BSENSOR, MicroDelayBsensor );
  if( eeprom_read( EE_MICROSELECTDELAY_BSENSOR ) != MicroSelectDelayBsensor )
    eeprom_write( EE_MICROSELECTDELAY_BSENSOR, MicroSelectDelayBsensor );
#endif /* __VARS_IN_EEPROM__ */

  if( subindex == 3 || subindex == 4 || subindex == 6 || subindex == 7 )
    {
      /* Recalibrate all B-sensor ADCs using the new setting ! */
      adcb_reset_and_calibrate( ADCB_MAX_CNT, TRUE );
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_reset_and_calibrate( BYTE addr, BOOL send_emergency )
{
  BOOL result;

  if( addr == ADCB_MAX_CNT )
    {
      BYTE adc_no, dummy;

#ifdef _FAST_INIT_

      /* Recalibrate all ADCs in the current configuration ! */
      for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	{
	  /* Initialise error status */
	  AdcError[AdcAddrList[adc_no]] = 0;

	  adcb_reset( AdcAddrList[adc_no], send_emergency );
	}

      adcb_calibrate_all( &dummy, send_emergency );

      for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	{
	  if( AdcError[AdcAddrList[adc_no]] == 0 )
	    adcb_init_csr( AdcAddrList[adc_no] );
	}

#else

      /* Recalibrate all ADCs in the current configuration ! */
      for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	{
	  /* Initialise error status */
	  AdcError[AdcAddrList[adc_no]] = 0;

	  /* Perform calibration only if reset succeeded */
	  if( (result = adcb_reset( AdcAddrList[adc_no], send_emergency )) )
	    result = adcb_calibrate( AdcAddrList[adc_no], send_emergency );
	}

#endif /* _FAST_INIT_ */

    }
  else
    {
      if( addr < ADCB_MAX_CNT )
	{
	  /* Initialise error status */
	  AdcError[addr] = 0;

	  /* Perform calibration only if reset succeeded */
	  if( (result = adcb_reset( addr, send_emergency )) )
	    result = adcb_calibrate( addr, send_emergency );

	  /* For B-sensor strings: we do initialization of registers
	     only once (rather than before *every* channel scan, for example),
	     i.e. here */
	  adcb_init_csr( addr );
	}
      else
	{
	  result = FALSE;
	}
    }

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_reset( BYTE addr, BOOL send_emergency )
{
  BOOL reset_result;
  BYTE err_id;
  BYTE adc_conf_reg[3];

  /* Not allowed when ADC scan in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  AdcConfRegB_2       = eeprom_read( EE_ADCCONFREGB2_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  /* Select ADC */
  adcb_select( addr );

  /* Initialize the ADC's serial port interface */
  cs5523_serial_init();

  /* Reset the ADC */
  reset_result = cs5523_reset( &err_id );

  /* NB: if required, initialize Logical Channels here */

  /* Initialize Configuration Register:
     Chop Frequency:  256 Hz (at freq <= 30 Hz), 4096 Hz (other freq)
     		      (recommended by Crystal, CS5524 datasheet pg 14)
     Depth Pointer : 003 (read/write 2 CSRs at a time) */
  adc_conf_reg[0] = ADCB_CNFREG_0;
  adc_conf_reg[1] = ADCB_CNFREG_1;
  adc_conf_reg[2] = AdcConfRegB_2;

  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

  /* Deselect ADC */
  adcb_deselect();

  if( reset_result == FALSE )
    {
      /* Reset failed */
      AdcError[addr] |= ADC_ERR_RESET;

      /* Generate emergency message ? */
      if( send_emergency )
	{
	  /* CANopen Error Code 0x5000: device hardware */
	  can_write_emergency( 0x00, 0x50, EMG_ADC_RESET_BSE,
			       addr, err_id, 0,
			       ERRREG_MANUFACTURER );
	}

      return FALSE;
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_calibrate( BYTE addr, BOOL send_emergency )
{
  BYTE ain, a1a0;
  BOOL calib_result;
  BYTE calib_phase;
  BYTE adc_conf_reg[3];

  /* Not allowed when ADC conversion in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

  /* =================================== */
  /* Hall-sensor calibration procedure:
     1. perform a 'Self' Offset calibration on each of the 3 Hall sensor
     inputs AIN1, AIN2 and AIN3, and also AIN4, voltage range 100 mV,
     A1A0 mux-setting irrelevant;
     2. perform a 'System' Gain calibration on AIN4 with mux-setting A1A0=00,
     thus selecting the current monitor input, voltage range 100 mV;
     the resulting gain value is read from the AIN4 Gain Register and
     copied to the Gain Registers of inputs AIN1, AIN2 and AIN3. */

  adcb_select( addr );

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  calib_result = TRUE;
  calib_phase  = 1;

  /* Read/write Configuration Register in order to set the Depth Pointer to 1,
     and the Multiple Conversion bit to FALSE */
  cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
  adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
  adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
  adc_conf_reg[1] |= (1<<CS23_CNF_CSR_DEPTH_SHIFT);
  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

  /* Offset calibration of all inputs, using the B-sensor ADC-configuration */
  a1a0 = 0; /* Don't care... */
  for( ain=0; ain<4; ++ain )
    {
      if( calib_result == TRUE )
	{
	  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_OFFSET_SELF_CALIB,
				     AdcConfigB ) == FALSE )
	    calib_result = FALSE;
	  else
	    ++calib_phase;
	  WDR();
	}
    }

  /* The current monitor input is the B-sensor full-scale calib input */
  ain  = 3;
  a1a0 = 0; /* Full-scale input (ca. 87 mV) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
			     AdcConfigB ) == FALSE )
    calib_result = FALSE;
  else
    ++calib_phase;

  if( calib_result == TRUE )
    /* Now read the AIN4 Gain Register and use this gain value
       for the HALL sensor inputs AIN1, AIN2 and AIN3 */
    {
      BYTE gain[3];
      cs5523_read_reg ( CS23_CMD_GAIN_REG, 3, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 0, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 1, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 2, gain );
    }

  adcb_deselect();

  if( calib_result == FALSE )
    {
      /* Calibration error... */
      AdcError[addr] |= ADC_ERR_CALIBRATION;

      if( send_emergency )
	/* CANopen Error Code 0x5000: device hardware */
	can_write_emergency( 0x00, 0x50, EMG_ADC_HALL_CALIB_BSE,
			     addr, calib_phase, 0, ERRREG_MANUFACTURER );

      adcb_select( addr );
      cs5523_serial_init();
      adcb_deselect();

      return FALSE;
    }

  /* =================================== */
  /* T-sensor calibration procedure:
     1. perform a 'System' Offset calibration on AIN4 using mux-setting A1A0=10
     thus selecting the 0 degrees C offset calibration input,
     voltage range 2.5V.
     2. perform a 'System' Gain calibration on AIN4 using mux-setting A1A0=11
     thus selecting the 100 degrees C full-scale calibration input,
     voltage range 2.5V */

  adcb_select( addr );

#ifdef __VARS_IN_EEPROM__
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  calib_result = TRUE;

  /* The T-sensor 0 degrees C offset calibration input */
  ain  = 3;
  a1a0 = 2; /* 'Ground'/zero value (at 0 degrees C) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_OFFSET_SYSTEM_CALIB,
			     AdcConfigT ) == FALSE )
    calib_result = FALSE;
  else
    ++calib_phase;

  WDR();

  /* The T-sensor 100 degrees C full-scale calibration input */
  ain  = 3;
  a1a0 = 3; /* Full scale (at 100 degrees C) */
  if( cs5523_calibrate_chan( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
			     AdcConfigT ) == FALSE )
    calib_result = FALSE;
  else
    ++calib_phase;

  WDR();

  adcb_deselect();

  if( calib_result == FALSE )
    {
      /* Calibration error... */
      AdcError[addr] |= ADC_ERR_CALIBRATION;

      if( send_emergency )
	/* CANopen Error Code 0x5000: device hardware */
	can_write_emergency( 0x00, 0x50, EMG_ADC_T_CALIB_BSE,
			     addr, calib_phase, 0, ERRREG_MANUFACTURER );

      adcb_select( addr );
      cs5523_serial_init();
      adcb_deselect();

      return FALSE;
    }

  /* =================================== */

  /* Restore Configuration Register */
  adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
  adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
  adc_conf_reg[2] |= ADCB_CONVERSION_MODE;
  adc_conf_reg[1] |= ADCB_CSR_DEPTH_MASK;
  adcb_select( addr );
  cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
  adcb_deselect();

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_calibrate_all( BYTE *no_of_err, BOOL send_emergency )
{
  BYTE str_no, adc_no;
  BYTE ain, a1a0;
  BYTE adc_conf_reg[3];

  /* Not allowed when ADC conversion in progress... */
  if( (AdcConvInProgress & TRUE) == TRUE ) return FALSE;

  /* =================================== */
  /* Hall-sensor calibration procedure:
     1. perform a 'Self' Offset calibration on each of the 3 Hall sensor
     inputs AIN1, AIN2 and AIN3, and also AIN4, voltage range 100 mV,
     A1A0 mux-setting irrelevant;
     2. perform a 'System' Gain calibration on AIN4 with mux-setting A1A0=00,
     thus selecting the current monitor input, voltage range 100 mV;
     the resulting gain value is read from the AIN4 Gain Register and
     copied to the Gain Registers of inputs AIN1, AIN2 and AIN3. */

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  /* Read/write Configuration Register in order to set the Depth Pointer to 1,
     and the Multiple Conversion bit to FALSE */
  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      if( AdcError[AdcAddrList[adc_no]] != 0 ) continue;

      adcb_select( AdcAddrList[adc_no] );

      cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
      adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
      adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
      adc_conf_reg[1] |= (1<<CS23_CNF_CSR_DEPTH_SHIFT);
      cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );

      adcb_deselect();
    }

  /* Offset calibration of all inputs, using the B-sensor ADC-configuration */
  a1a0 = 0; /* Don't care... */
  for( ain=0; ain<4; ++ain )
    {
      //adcb_brc_select_all_strings();
      for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
	{
	  if( AdcInString[str_no] > 0 )
	    {
	      adcb_brc_select( str_no );
	      //adcb_string_port_select( str_no );
	      cs5523_calibrate_chan_nowait( ain, a1a0,
					    CS23_CMD_OFFSET_SELF_CALIB,
					    AdcConfigB );
	      adcb_brc_deselect( str_no );
	    }
	}

      /* Now wait for the calibrations on the various B-modules to finish */
      adcb_await_calib_all( send_emergency, EMG_ADC_HALL_CALIB_BSE,
			    no_of_err );
    }

  /* The current monitor input is the B-sensor full-scale calib input */
  ain  = 3;
  a1a0 = 0; /* Full-scale input (ca. 87 mV) */

  //adcb_brc_select_all_strings();
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      if( AdcInString[str_no] > 0 )
	{
	  adcb_brc_select( str_no );
	  //adcb_string_port_select( str_no );
	  cs5523_calibrate_chan_nowait( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
					AdcConfigB );
	  adcb_brc_deselect( str_no );
	}
    }

  /* Now wait for the calibrations on the various B-modules to finish */
  adcb_await_calib_all( send_emergency, EMG_ADC_HALL_CALIB_BSE, no_of_err );

  /* Now read the AIN4 Gain Register and use this gain value
     for the HALL sensor inputs AIN1, AIN2 and AIN3 */
  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      BYTE gain[3];

      if( AdcError[AdcAddrList[adc_no]] != 0 ) continue;

      adcb_select( AdcAddrList[adc_no] );

      cs5523_read_reg ( CS23_CMD_GAIN_REG, 3, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 0, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 1, gain );
      cs5523_write_reg( CS23_CMD_GAIN_REG, 2, gain );

      adcb_deselect();
    }

  /* =================================== */
  /* T-sensor calibration procedure:
     1. perform a 'System' Offset calibration on AIN4 using mux-setting A1A0=10
     thus selecting the 0 degrees C offset calibration input,
     voltage range 2.5V.
     2. perform a 'System' Gain calibration on AIN4 using mux-setting A1A0=11
     thus selecting the 100 degrees C full-scale calibration input,
     voltage range 2.5V */

#ifdef __VARS_IN_EEPROM__
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif

  /* The T-sensor 0 degrees C offset calibration input */
  ain  = 3;
  a1a0 = 2; /* 'Ground'/zero value (at 0 degrees C) */

  //adcb_brc_select_all_strings();
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      if( AdcInString[str_no] > 0 )
	{
	  adcb_brc_select( str_no );
	  //adcb_string_port_select( str_no );
	  cs5523_calibrate_chan_nowait( ain, a1a0,
					CS23_CMD_OFFSET_SYSTEM_CALIB,
					AdcConfigT );
	  adcb_brc_deselect( str_no );
	}
    }

  /* Now wait for the calibrations on the various B-modules to finish */
  adcb_await_calib_all( send_emergency, EMG_ADC_T_CALIB_BSE, no_of_err );

  /* The T-sensor 100 degrees C full-scale calibration input */
  ain  = 3;
  a1a0 = 3; /* Full scale (at 100 degrees C) */

  //adcb_brc_select_all_strings();
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      if( AdcInString[str_no] > 0 )
	{
	  adcb_brc_select( str_no );
	  //adcb_string_port_select( str_no );
	  cs5523_calibrate_chan_nowait( ain, a1a0, CS23_CMD_GAIN_SYSTEM_CALIB,
					AdcConfigT );
	  adcb_brc_deselect( str_no );
	}
    }

  /* Now wait for the calibrations on the various B-modules to finish */
  adcb_await_calib_all( send_emergency, EMG_ADC_T_CALIB_BSE, no_of_err );

  /* =================================== */

  /* Restore Configuration Registers (assuming they were all alike) */
  adc_conf_reg[2] &= ~CS23_CNF_MULTIPLE_CONV;
  adc_conf_reg[1] &= ~CS23_CNF_CSR_DEPTH_MASK;
  adc_conf_reg[2] |= ADCB_CONVERSION_MODE;
  adc_conf_reg[1] |= ADCB_CSR_DEPTH_MASK;
  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      if( AdcError[AdcAddrList[adc_no]] != 0 ) continue;

      adcb_select( AdcAddrList[adc_no] );
      cs5523_write_reg( CS23_CMD_CONFIG_REG, 0, adc_conf_reg );
      adcb_deselect();
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */

static void adcb_await_calib_all( BOOL send_emergency,
				  BYTE err_id,
				  BYTE *no_of_err )
{
  BYTE adc_no, addr;

  for( adc_no=0; adc_no<AdcCnt; ++adc_no )
    {
      addr = AdcAddrList[adc_no];

      if( AdcError[addr] != 0 ) continue;

      WDR();

      adcb_select( addr );

      /* Busy-wait for calibration-ready or time-out */
      if( !cs5523_await_sdo_low( 120 ) ) // Make that 1200 ms...
	{
	  /* Time out ! Calibration error... */
	  AdcError[addr] |= ADC_ERR_CALIBRATION;

	  if( send_emergency )
	    /* CANopen Error Code 0x5000: device hardware */
	    can_write_emergency( 0x00, 0x50, err_id,
				 addr, 0, 0, ERRREG_MANUFACTURER );
	  ++(*no_of_err);

	  WDR();
	}
      else
	{
	  /* Calibration done ! */

	  /* Generate 8 SCLKs to clear the SDO flag:
	     ! not needed after calibration (says CRYSTAL) ! */
	  cs5523_write_byte( 0 );
	}
      adcb_deselect();
    }
}

/* ------------------------------------------------------------------------ */

static void adcb_init_csr( BYTE addr )
{
  /* Prepare the Configuration Setup Registers in (a) B-sensor module(s)
     for conversions of the following channels:
     ADC-channel 0, 1, 2 : the Hall-sensors
     ADC-channel 3       : the T-sensor
     In addition: store the B-sensor address in the next unused CSR */

  BYTE a1a0, cs1cs0;
  BYTE csr_data[3*2*2];

#ifdef __VARS_IN_EEPROM__
  AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif

  /* Set A1-A0 and physical channel in LC 1, 2, 3 and 4
     while maintaining proper wordrate and gain */
  a1a0   = 1; /* Select the T-sensor, when cs1cs0==3 */
  cs1cs0 = 0; /* Hall sensor H1 */
  csr_data[0] = (AdcConfigB |
		 ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_LO_MASK));
  csr_data[1] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
		 ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_HI_MASK));
  cs1cs0 = 1; /* Hall sensor H2 */
  csr_data[2] = (AdcConfigB |
		 ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_LO_MASK));
  csr_data[3] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
		 ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_HI_MASK));
  cs1cs0 = 2; /* Hall sensor H3 */
  csr_data[4] = (AdcConfigB |
		 ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_LO_MASK));
  csr_data[5] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
		 ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_HI_MASK));
  cs1cs0 = 3; /* T sensor */
  csr_data[6] = (AdcConfigT |
		 ((cs1cs0 << CS23_CSR_PHYSCHAN_SEL_LO_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_LO_MASK));
  csr_data[7] = ((a1a0 << CS23_CSR_A1A0_SHIFT) |
		 ((cs1cs0 >> CS23_CSR_PHYSCHAN_SEL_HI_SHIFT) &
		  CS23_CSR_PHYSCHAN_SEL_HI_MASK));

  /* To be used for storing other information (see below: address) */
  csr_data[8]  = 0;
  csr_data[9]  = 0;
  csr_data[10] = 0;
  csr_data[11] = 0;

  /* Write the CSR content to the B-sensor ADC(s) */
  if( addr == ADCB_MAX_CNT )
    {
      BYTE adc_no;
      for( adc_no=0; adc_no<AdcCnt; ++adc_no )
	{
	  /* In addition: store the B-sensor address in the next unused CSR */
	  csr_data[8] = AdcAddrList[adc_no];

	  adcb_select( AdcAddrList[adc_no] );
	  cs5523_write_csr( 3, csr_data, ADCB_CSR_DEPTH );
	  adcb_deselect();
	}
    }
  else
    {
      if( addr < ADCB_MAX_CNT )
	{
	  /* In addition: store the B-sensor address in the next unused CSR */
	  csr_data[8] = addr;

	  adcb_select( addr );
	  cs5523_write_csr( 3, csr_data, ADCB_CSR_DEPTH );
	  adcb_deselect();
	}
    }
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan_start( void )
{
  /* Start scanning only if scanning not already in progress */
  if( (AdcScanInProgress & TRUE) == TRUE ) return;

  /* Refresh Data-Direction Register settings */
  ADCB_INIT_DDR_0();
  ADCB_INIT_DDR_1();
  ADCB_INIT_DDR_2();
  ADCB_INIT_DDR_3();

#ifdef __VARS_IN_EEPROM__
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  /* Is there actually any ADC...? */
  if( AdcCnt == 0 ) return;

  AdcNo     = 0;
  AdcChanNo = 0;
  AdcScanInProgress = adcb_scan_next();
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan_stop( void )
{
  /* We have to stop ongoing ADC conversions... */

  if( (AdcScanInProgress & TRUE) == TRUE )
    {
      if( (AdcConvInProgress & TRUE) == TRUE )
	{
	  /* Wait for the conversion(s) to finish and read out the ADC data */

#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_
	  adcb_select( AdcAddrList[AdcNo] );

	  WDR();

	  /* Wait for SDO to go low flagging the conversion is done, but
	     use a timeout (ca. 600 ms, sufficient at 1.88 Hz wordrate) */
	  if( cs5523_await_sdo_low( 120 ) ) // Make that 1200 ms...
	    {
	      /* Generate 8 SCLKs to clear the SDO flag */
	      cs5523_write_byte( 0 );

	      /* Read 24 bits (3 bytes) of conversion data */
	      cs5523_read_byte();
	      cs5523_read_byte();
	      cs5523_read_byte();
	    }

	  adcb_deselect();
#else
	  /* Loop over all ADCs that still need to be read out */
	  BYTE adc_no;
	  for( adc_no=AdcNo; adc_no<AdcCnt; ++adc_no )
	    {
	      adcb_select( AdcAddrList[adc_no] );

	      WDR();

	      /* Wait for SDO to go low flagging the conversion is done,
		 but use a timeout (ca. 600 ms,
		 sufficient at 1.88 Hz wordrate) */
	      if( cs5523_await_sdo_low( 120 ) ) // Make that 1200 ms...
		{
		  BYTE chan;

		  /* Generate 8 SCLKs to clear the SDO flag */
		  cs5523_write_byte( 0 );

		  /* Read 24 bits (3 bytes) of conversion data per chan */
		  for( chan=0; chan<ADCB_CHANS_PER_ADC_SCAN; ++chan )
		    {
		      /* Read 24 bits (3 bytes) of conversion data */
		      cs5523_read_byte();
		      cs5523_read_byte();
		      cs5523_read_byte();
		    }
		}
	      adcb_deselect();
	    }
#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */
	}
    }

  /* Initialize variables for scanning operations */
  //AdcNo             = 0;
  //AdcChanNo         = 0;
  AdcScanInProgress = FALSE;
  AdcConvInProgress = FALSE;
}

/* ------------------------------------------------------------------------ */

void adcb_pdo_scan( void )
{
  /* Handle an on-going ADC channel scan, otherwise do nothing */

  if( (AdcScanInProgress & TRUE) == TRUE )
    {
      /* Read out next ADC channel */
      AdcScanInProgress = adcb_scan_next();
    }
}

/* ------------------------------------------------------------------------ */

#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_

static BOOL adcb_scan_next( void )
{
  /* The B-sensor scan reads out the 3 Hall-sensors and the T-sensor,
     so only 4 channels per B-sensor are in this type (PDO) of read-out;
     other T-sensor related inputs (e.g. the calibration inputs) can
     be read out using SDOs.

     NB: The analog values are converted and read out one-by-one */

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      /* Conversion in progress: check if done */
      BOOL sdo_low;
      switch( StringId )
	{
	case 3:
	  sdo_low = ADCB_SDO_LOW_3();
	  break;
	case 2:
	  sdo_low = ADCB_SDO_LOW_2();
	  break;
	case 1:
	  sdo_low = ADCB_SDO_LOW_1();
	  break;
	case 0:
	  sdo_low = ADCB_SDO_LOW_0();
	  break;
	default:
	  sdo_low = FALSE;
	  break;
	}

      if( sdo_low )
	{
	  /* SDO went low --> conversion ready ! */
	  BYTE configreg[3];
	  BYTE adc_config;
	  BYTE pdo_data[C91_TPDO4_LEN];

	  /* This conversion results in real data
	     that we're interested in... */

	  /* Should only generate a next PDO when the previous one
	     has been sent */
	  if( can_transmitting(C91_TPDO4) )
	    {
	      /* Sending of CAN message not done, so wait... */
	      return TRUE;
	    }

	  /* Generate 8 SCLKs to clear the SDO flag */
	  cs5523_write_byte( 0 );

	  /* Read 24 bits (3 bytes) of ADC conversion data */
	  pdo_data[5] = cs5523_read_byte();
	  pdo_data[4] = cs5523_read_byte();
	  pdo_data[3] = cs5523_read_byte();

	  /* Read Configuration Register in order
	     to check the data conversion error flags */
	  cs5523_read_reg( CS23_CMD_CONFIG_REG, 0, configreg );

	  adcb_deselect();

	  /* Add a status byte containing
	     the ADC-configuration (wordrate, gain, uni/bipolar)
	     and one bit with an 'or' of the OF and OD bits
	     (here bit 7, the highest bit) */
	  if( (AdcChanNo & 0x03) == 0x03 )
	    {
	      /* Reading out the T-sensor */
#ifdef __VARS_IN_EEPROM__
	      AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
	      adc_config = AdcConfigT;
	    }
	  else
	    {
	      /* Reading out a B-sensor */
#ifdef __VARS_IN_EEPROM__
	      AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
	      adc_config = AdcConfigB;
	    }

	  /* Set the upper bit of the status byte if any of the
	     data conversion error bits are set... */
	  if( configreg[0] & 0xC0 )
	    pdo_data[2] = adc_config | ADC_ERR_CONVERSION;
	  else
	    pdo_data[2] = adc_config;

	  /* Set to FALSE only if message can be sent... */
	  AdcConvInProgress = FALSE;

	  /* Put the ADC channel number in the message */
	  pdo_data[1] = AdcChanNo;

	  /* Put the ADC address in the message */
	  pdo_data[0] = AdcAddrList[AdcNo];

	  /* Send as a Transmit-PDO4 CAN-message */
	  can_write( C91_TPDO4, C91_TPDO4_LEN, pdo_data );

	  /* Next time, next channel */
	  ++AdcChanNo;
	  if( AdcChanNo == ADCB_CHANS_PER_ADC_SCAN )
	    {
	      AdcChanNo = 0;
	      ++AdcNo;
	    }

	  /* Are we done with the current scan ? */
	  if( AdcNo == AdcCnt )
	    {
	      /* Yes, we're done... */
	      return FALSE;
	    }
	}
      else
	{
	  /* Conversion in progress...
	     check for possible timeout... */

	  /* ###Do not deselect/select: causes noise on the converted data */
	  //adcb_deselect();

	  if( timer0_timeout(ADC_BSENSOR) )
	    {
	      /* Conversion timed out ! */
	      AdcError[AdcNo] |= ADC_ERR_TIMEOUT;

	      /* CANopen Error Code 0x5000: device hardware */
	      can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE,
				   AdcAddrList[AdcNo], AdcChanNo, 0,
				   ERRREG_MANUFACTURER );

	      //adcb_select( AdcAddrList[AdcNo] );
	      //cs5523_serial_init();
	      //adcb_deselect();
	      AdcConvInProgress = FALSE;
	      adcb_reset_and_calibrate( AdcAddrList[AdcNo], TRUE );
	      AdcConvInProgress = TRUE;

	      /* The ADC has an error status, go to the next (if any)*/
	      AdcChanNo = 0;
	      ++AdcNo;

	      /* Are we done with the current scan ? */
	      if( AdcNo == AdcCnt )
		{
		  AdcConvInProgress = FALSE;

		  /* Yes, we're done... */
		  return FALSE;
		}
	    }
	  else
	    {
	      /* Wait some more... */
	      return TRUE;
	    }
	}
    }

  {
    /* Initiate the next ADC conversion */
    BYTE logchan_ptr;

    logchan_ptr = (AdcChanNo & 3) << CS23_CMD_LOGCHAN_SELECT_SHIFT;

    /* Start a conversion of a LC */
    adcb_select( AdcAddrList[AdcNo] );
    cs5523_start_conversion_cmd( CS23_CMD_NORMAL_CONVERSION | logchan_ptr );
    /* ###Do not deselect/select: causes noise on the converted data */
    //adcb_deselect();

    AdcConvInProgress = TRUE;

    /* Set a timeout on the conversion of about 800 ms... */
    timer0_set_timeout_10ms( ADC_BSENSOR, 120 ); // Make that 1200 ms...
  }

  return TRUE;
}

#else /* not _SINGLE_CHAN_BSENSOR_CONVERSION_ */
/* ------------------------------------------------------------------------ */

static BOOL adcb_scan_next( void )
{
  /* The B-sensor scan reads out the 3 Hall-sensors and the T-sensor,
     so only 4 channels per B-sensor are in this type (PDO) of read-out;
     other T-sensor related inputs (e.g. the calibration inputs) can
     be read out using SDOs.

     NB: The analog values are converted and read out in
         one conversion sequence */

  if( (AdcConvInProgress & TRUE) == TRUE )
    {
      /* Conversion in progress: check if done */
      BOOL sdo_low;
      switch( StringId )
	{
	case 3:
	  sdo_low = ADCB_SDO_LOW_3();
	  break;
	case 2:
	  sdo_low = ADCB_SDO_LOW_2();
	  break;
	case 1:
	  sdo_low = ADCB_SDO_LOW_1();
	  break;
	case 0:
	  sdo_low = ADCB_SDO_LOW_0();
	  break;
	default:
	  sdo_low = FALSE;
	  break;
	}

      if( sdo_low )
	{
	  /* SDO went low --> conversion ready ! */
	  BYTE chan;
	  BYTE pdo_data[C91_TPDO4_LEN];

	  /* This conversion results in real data
	     that we're interested in... */

	  /* Generate 8 SCLKs to clear the SDO flag */
	  cs5523_write_byte( 0 );

	  for( chan=0; chan<ADCB_CHANS_PER_ADC_SCAN; ++chan )
	    {
	      /* Read 24 bits (3 bytes) of ADC conversion data */
	      pdo_data[5] = cs5523_read_byte();
	      pdo_data[4] = cs5523_read_byte();
	      pdo_data[3] = cs5523_read_byte();

	      if( chan == 3 )
		{
		  /* Reading out the T-sensor */
		  if( AdcDegreesInPdo & TRUE )
		    {
		      /* Provide temperature in (milli)degrees celcius */
		      BYTE   *ptr;
		      UINT32 a_l;
		      float  a, ohms_f;
		      BYTE   degrees[4];
		      ptr    = (BYTE *) &a_l;
		      *ptr++ = pdo_data[3];
		      *ptr++ = pdo_data[4];
		      *ptr++ = pdo_data[5];
		      *ptr   = 0x00;
		      a      = ((float) 1.996 * (float) a_l) / 16777215.0;
		      ohms_f = (float) 23200.0*((2.0685-a)/(2.9315+a));
		      adcb_convert_ohms_to_degrees( ohms_f, degrees );
		      if( degrees[3] != 0x00 )
			{
			  degrees[0] = 0xFF;
			  degrees[1] = 0xFF;
			  degrees[2] = 0xFF;
			}
		      pdo_data[3] = degrees[0];
		      pdo_data[4] = degrees[1];
		      pdo_data[5] = degrees[2];
		    }

		  /* Add a status byte containing
		     the ADC-configuration (wordrate, gain, uni/bipolar) */
#ifdef __VARS_IN_EEPROM__
		  AdcConfigT = eeprom_read( EE_ADCCONFIGT_BSENSOR );
#endif
		  pdo_data[2] = AdcConfigT;
		}
	      else
		{
		  /* Add a status byte containing
		     the ADC-configuration (wordrate, gain, uni/bipolar) */
#ifdef __VARS_IN_EEPROM__
		  AdcConfigB = eeprom_read( EE_ADCCONFIGB_BSENSOR );
#endif
		  pdo_data[2] = AdcConfigB;
		}

	      /* Put the ADC channel number in the message */
	      pdo_data[1] = chan;

	      /* Put the ADC address in the message */
	      pdo_data[0] = AdcAddrList[AdcNo];

	      /* Should only generate a next PDO when
		 the previous one has been sent */
	      while( can_transmitting(C91_TPDO4) );

	      /* Send as a Transmit-PDO4 CAN-message */
	      can_write( C91_TPDO4, C91_TPDO4_LEN, pdo_data );
	    }

	  if( AdcRecovery )
	    {
	      BOOL reinitialized = FALSE;
	      BYTE reg[3];
	      BYTE csr_data[4*2*2];

	      /* ----------------------------------------------------------- */
	      /* Try to keep adc calibration in order, by checking if one of
		 the calibration constants falls within a certain range */
	      cs5523_read_reg( CS23_CMD_GAIN_REG, 0, reg );
	      //if( reg[2] < 0x41 || reg[2] > 0x4B )
	      if( reg[2] < 0x31 || reg[2] > 0x5F )
		{
		  /* Try again, just to make sure.. */
		  cs5523_read_reg( CS23_CMD_GAIN_REG, 0, reg );
		  //if( reg[2] < 0x41 || reg[2] > 0x4B )
		  if( reg[2] < 0x31 || reg[2] > 0x5F )
		    {
		      can_write_emergency( 0x00, 0x50,
					   EMG_ADC_CALIBCONST_BSE,
					   AdcAddrList[AdcNo], reg[2], reg[1],
					   ERRREG_MANUFACTURER );

		      /* Register value probably not correct, let's re-init */
		      AdcConvInProgress = FALSE;
		      adcb_reset_and_calibrate( AdcAddrList[AdcNo], TRUE );
		      AdcConvInProgress = TRUE;
		      reinitialized     = TRUE;
		    }
		}

	      /* ----------------------------------------------------------- */
	      /* Check if we are addressing the expected B-sensor */

	      /* Need to readdress, if reinitialization above took place.. */
	      if( reinitialized == TRUE )
		adcb_select( AdcAddrList[AdcNo] );

	      /* Read current CSR content */
	      cs5523_read_csr( csr_data );

	      if( csr_data[8] != AdcAddrList[AdcNo] )
		{
		  /* Try again, just to make sure.. */
		  cs5523_read_csr( csr_data );

		  if( csr_data[8] != AdcAddrList[AdcNo] )
		    {
		      can_write_emergency( 0x00, 0x50,
					   EMG_ADC_BADDR_BSE,
					   AdcAddrList[AdcNo], csr_data[8], 0,
					   ERRREG_MANUFACTURER );

		      /* Register value probably not correct, let's re-init */
		      AdcConvInProgress = FALSE;
		      adcb_reset_and_calibrate( AdcAddrList[AdcNo], TRUE );
		      AdcConvInProgress = TRUE;
		      reinitialized     = TRUE;
		    }
		}
	      /* ----------------------------------------------------------- */
	    }

	  adcb_deselect();

	  /* Next time, next ADC */
	  ++AdcNo;

          /* Are we done with the current scan ? */
          if( AdcNo == AdcCnt )
            {
              AdcConvInProgress = FALSE;

              /* Yes, we're done... */
              return FALSE;
            }

	  /* Select the next ADC */
	  adcb_select( AdcAddrList[AdcNo] );

	  return TRUE;
	}
      else
	{
	  /* Conversion in progress...
	     check for possible timeout... */
	  BOOL timeout;
	  timeout = timer0_timeout( ADC_BSENSOR );

	  /* ###Do not deselect/select: causes noise on conversion */
	  //adcb_deselect();

	  if( AdcError[AdcAddrList[AdcNo]] != 0 || timeout )
	    {
	      if( timeout )
		{
		  /* Conversion timed out ! */
		  AdcError[AdcAddrList[AdcNo]] |= ADC_ERR_TIMEOUT;

		  /* CANopen Error Code 0x5000: device hardware */
		  can_write_emergency( 0x00, 0x50, EMG_ADC_CONVERSION_BSE,
				       AdcAddrList[AdcNo], 0, 0,
				       ERRREG_MANUFACTURER );

		  //adcb_select( AdcAddrList[AdcNo] );
		  //cs5523_serial_init();
		  //adcb_deselect();

		  /* Perhaps next time it will work ? */
		  if( AdcRecovery )
		    {
		      AdcConvInProgress = FALSE;
		      if( adcb_reset_and_calibrate( AdcAddrList[AdcNo],
						    TRUE ) == FALSE )
			adcb_reset_and_calibrate( AdcAddrList[AdcNo], TRUE );
		      AdcConvInProgress = TRUE;
		    }
		}

	      /* The ADC has an error status, go to the next (if any)*/
	      ++AdcNo;

	      /* Are we done with the current scan ? */
	      if( AdcNo == AdcCnt )
		{
		  AdcConvInProgress = FALSE;

		  /* Yes, we're done... */
		  return FALSE;
		}

	      /* Select the next ADC */
	      adcb_select( AdcAddrList[AdcNo] );
	    }

	  /* Wait some more... */
	  return TRUE;
	}
    }

  {
    /* Initiate ADC conversions on all B-sensor ADCs,
       per string simultaneously */
    BYTE str_no;

    /* Broadcast conversion command on all strings with B-sensors:
       first enable all ADCs with the broadcast address, then in quick
       succession send the conversion command out on all strings
       ###NOTE: doesn't work when the SCLK/SDI/SDO signals are shared
                between strings ! */

    //adcb_brc_select_all_strings();
    for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
      {
	if( AdcInString[str_no] > 0 )
	  {
	    adcb_brc_select( str_no );
	    //adcb_string_port_select( str_no );
	    /* Start a conversion of Logical Channels 1 to 4 */
	    cs5523_start_conversion_cmd( CS23_CMD_NORMAL_CONVERSION | 0 );
	    adcb_brc_deselect( str_no );
	  }
      }
    for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
      adcb_brc_deselect( str_no );

    /* ###Do not deselect/select: causes noise on conversion */

    /* Select the first ADC on our list */
    //adcb_select( AdcAddrList[AdcNo] );
    adcb_select( AdcAddrList[0] );

    AdcConvInProgress = TRUE;

    /* Set a timeout on the conversions (4x) of about 1000 ms...
       (NB: this means the 1.88 and 3.76 Hz conversions don't work!) */
    timer0_set_timeout_10ms( ADC_BSENSOR, 100 );
  }

  return TRUE;
}

#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */

/* ------------------------------------------------------------------------ */

BOOL adcb_probe( void )
{
  /* 1. Scan all strings for connected B-sensors, scanning for all
        possible addresses (by writing a defined value to a Gain Register
	and reading the register value back)
     2. Store the result (the AdcStringMap[] array) in EEPROM.
     3. Reset and calibrate all B-sensors found */

  /* If _FAST_INIT_ is defined this whole procedure
     takes in the order of 6 s maximum;
     if _not_ it takes about 4+N*0.55 s, with 'N'
     the number of B-sensor modules in the configuration
     (example: 30 modules then takes about 20 s) */

  BYTE str_no, addr, dummy;

  /* Can't do anything if the ADCs are in use... */
  if( AdcConvInProgress ) return FALSE;

  /* Reset current mapping */
  for( addr=0; addr<ADCB_MAX_CNT; ++addr ) AdcStringMap[addr] = 0xFF;

  /* Get all ADCs and their interfaces on all strings in a defined state... */
  adcb_preinit( FALSE );

  /* Scan all strings for all possible B-sensor addresses */
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      /* Select the SPI-port with the corresponding string of B-sensors */
      adcb_string_port_select( str_no );
      WDR();

      /* Loop through all possible B-sensor addresses */
      for( addr=0; addr<ADCB_MAX_CNT; ++addr )
	{
	  /* Scan only for addresses not yet assigned */
	  if( AdcStringMap[addr] == 0xFF )
	    {
	      BYTE regdata[3];

	      bsmicro_select( addr );

	      /* Write a defined byte in a Gain Register
		 (there is no guarantee that the Gain Register
		 at power-up comes up in a defined state...) */
	      regdata[2] = 0xAA;
	      cs5523_write_reg( CS23_CMD_GAIN_REG, 0, regdata );

	      /* Read back this Gain Register */
	      regdata[2] = 0x00;
	      cs5523_read_reg( CS23_CMD_GAIN_REG, 0, regdata );

	      /* Do I read something sensible there ? */
	      if( regdata[2] == 0xAA )
		{
		  /* Okay, found a B-sensor at this address */
		  AdcStringMap[addr] = str_no;
		}
	    }
	}
    }

  /* Store the new B-sensor map */
  if( adcb_store_map() == FALSE ) return FALSE;

  /* Finally, reset and calibrate all B-sensors found
     (send Emergencies if necessary) and initialize the configuration data */
  //if( adcb_init( &dummy, TRUE ) == FALSE ) return FALSE;
  adcb_init( &dummy, TRUE ); // adcb_init() reports its own problems!

  /* Everything went fine... */
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_probe_addr( BYTE addr )
{
  /* 1. Scan all strings for B-sensor with address 'addr'
        (by writing a defined value to a Gain Register
	and reading the register value back)
     2. Store the result (the updated AdcStringMap[] array) in EEPROM.
     3. Reset and calibrate the B-sensor, if it was found */

  BYTE str_no;
  BYTE regdata[3];
  BOOL found = FALSE;

  /* Can't do anything if the ADCs are in use... */
  if( AdcConvInProgress ) return FALSE;

  /* Valid address ? */
  if( addr >= ADCB_MAX_CNT ) return FALSE;

  /* Reset current mapping */
  AdcStringMap[addr] = 0xFF;

  /* Get all ADCs and their interfaces on all strings in a defined state... */
  //adcb_preinit( FALSE );
  /* Make sure to get any B-sensors-with-micro in a proper (listening) state */
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      adcb_brc_select( str_no );
      cs5523_serial_init();
      adcb_brc_deselect( str_no );
    }

  /* Scan all strings for the given address */
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    {
      /* Select the SPI-port with the corresponding string of B-sensors */
      adcb_string_port_select( str_no );
      WDR();

      bsmicro_select( addr );

      /* Write a defined byte in a Gain Register
	 (there is no guarantee that the Gain Register
	 at power-up comes up in a defined state...) */
      regdata[2] = 0xAA;
      cs5523_write_reg( CS23_CMD_GAIN_REG, 0, regdata );

      /* Read back this Gain Register */
      regdata[2] = 0x00;
      cs5523_read_reg( CS23_CMD_GAIN_REG, 0, regdata );

      /* Do I read something sensible there ? */
      if( regdata[2] == 0xAA )
	{
	  /* Okay, found the B-sensor */
	  AdcStringMap[addr] = str_no;
	  found = TRUE;
	  break; /* Break out of for-loop */
	}
    }

  /* Store the new B-sensor map */
  if( adcb_store_map() == FALSE ) return FALSE;

  /* Get our configuration data up-to-date (but don't touch AdcError[]) */
  adcb_init_config( FALSE );

  /* Finally, reset and calibrate the B-sensor (if found)
     (send Emergencies if necessary) */
  if( found )
    {
      /* Initialise error status for this B-sensor */
      AdcError[addr] = 0;

      /* Perform calibration only if reset succeeded */
      if( adcb_reset( addr, TRUE ) == TRUE )
	{
	  if( adcb_calibrate( addr, TRUE ) == TRUE )
	    adcb_init_csr( addr );
	  return TRUE;
	}
    }

  return FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL adcb_remove_addr( BYTE addr )
{
  /* Can't do anything if the ADCs are in use... */
  if( AdcConvInProgress ) return FALSE;

  /* Valid address ? */
  if( addr >= ADCB_MAX_CNT ) return FALSE;

  /* Is address in the configuration ? */
  if( AdcStringMap[addr] < ADCB_MAX_STRINGS )
    /* Okay, now remove it... */
    AdcStringMap[addr] = 0xFF;
  else
    return FALSE;

  /* Store the new B-sensor map */
  if( adcb_store_map() == FALSE ) return FALSE;

  /* Get our configuration data up-to-date (but don't touch AdcError[]) */
  adcb_init_config( FALSE );

  return TRUE;
}

/* ------------------------------------------------------------------------ */

#define ADCB_STORE_SIZE 7

/* ------------------------------------------------------------------------ */

BOOL adcb_store_config( void )
{
  BYTE block[ADCB_STORE_SIZE];

#ifdef __VARS_IN_EEPROM__
  AdcConfigB          = eeprom_read( EE_ADCCONFIGB_BSENSOR );
  AdcConfigT          = eeprom_read( EE_ADCCONFIGT_BSENSOR );
  AdcOptoDelayBsensor = eeprom_read( EE_ADCOPTODELAY_BSENSOR );
  MicroDelayBsensor   = eeprom_read( EE_MICRODELAY_BSENSOR );
  MicroSelectDelayBsensor = eeprom_read( EE_MICROSELECTDELAY_BSENSOR );
#endif /* __VARS_IN_EEPROM__ */

  block[0] = 0;
  block[1] = AdcConfigB;
  block[2] = AdcConfigT;
  block[3] = 0;
  block[4] = AdcOptoDelayBsensor;
  block[5] = MicroDelayBsensor;
  block[6] = MicroSelectDelayBsensor;

  return( store_write_block( STORE_ADC_BSENSOR, ADCB_STORE_SIZE, block ) );
}

/* ------------------------------------------------------------------------ */

static void adcb_init_config( BOOL init_errstat )
{
  BYTE str_no, addr;

  /* Initialize B-sensor-in-string counters */
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    AdcInString[str_no] = 0;

  /* Determine the number of B-sensors present, also per string */
  AdcCnt = 0;
  for( addr=0; addr<ADCB_MAX_CNT; ++addr )
    {
      /* B-sensor 'addr' present in string ? */
      if( AdcStringMap[addr] < ADCB_MAX_STRINGS )
	{
	  /* Make a consecutive list of B-sensors */
	  AdcAddrList[AdcCnt] = addr;

	  /* Keep track of the total number of B-sensors */
	  ++AdcCnt;
	  
	  /* Keep track of the number of B-sensors per string */
	  ++AdcInString[AdcStringMap[addr]];

	  /* Initialise error status for this B-sensor */
	  if( init_errstat ) AdcError[addr] = 0;
	}
      else
	{
	  /* Error value signifies that the B-sensor
	     is not present in the configuration */
	  AdcError[addr] = ADC_ABSENT;
	}
    }
}

/* ------------------------------------------------------------------------ */

static void adcb_load_config( void )
{
  BYTE block[ADCB_STORE_SIZE];

  /* Preset */
  block[0] = 0;
  block[1] = ADCB_CONV_PARS_B;
  block[2] = ADCB_CONV_PARS_T;
  block[3] = FALSE;
  block[4] = ADC_SIGNAL_RISETIME_BSENSOR;
  block[5] = MICRO_SIGNAL_HOLDTIME_BSENSOR;
  block[6] = MICRO_SELECT_HOLDTIME_BSENSOR;

  /* Read the configuration from EEPROM, if any */
  if( store_read_block( STORE_ADC_BSENSOR, ADCB_STORE_SIZE, block ) )
    {
      //AdcMsk              = block[0];
      AdcConfigB          = block[1];
      AdcConfigT          = block[2];
      //AdcCalibBeforeScan  = block[3];
      AdcOptoDelayBsensor = block[4];
      MicroDelayBsensor   = block[5];
      MicroSelectDelayBsensor = block[6];
    }
  else
    {
      /* No valid parameters in EEPROM: use defaults */
      AdcConfigB          = ADCB_CONV_PARS_B;
      AdcConfigT          = ADCB_CONV_PARS_T;
      AdcOptoDelayBsensor = ADC_SIGNAL_RISETIME_BSENSOR;
      MicroDelayBsensor   = MICRO_SIGNAL_HOLDTIME_BSENSOR;
      MicroSelectDelayBsensor = MICRO_SELECT_HOLDTIME_BSENSOR;
    }

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_ADCCONFIGB_BSENSOR ) != AdcConfigB )
    eeprom_write( EE_ADCCONFIGB_BSENSOR, AdcConfigB );
  if( eeprom_read( EE_ADCCONFIGT_BSENSOR ) != AdcConfigT )
    eeprom_write( EE_ADCCONFIGT_BSENSOR, AdcConfigT );
  if( eeprom_read( EE_ADCOPTODELAY_BSENSOR ) != AdcOptoDelayBsensor )
    eeprom_write( EE_ADCOPTODELAY_BSENSOR, AdcOptoDelayBsensor );
  if( eeprom_read( EE_MICRODELAY_BSENSOR ) != MicroDelayBsensor )
    eeprom_write( EE_MICRODELAY_BSENSOR, MicroDelayBsensor );
  if( eeprom_read( EE_MICROSELECTDELAY_BSENSOR ) != MicroSelectDelayBsensor )
    eeprom_write( EE_MICROSELECTDELAY_BSENSOR, MicroSelectDelayBsensor );
#endif /* __VARS_IN_EEPROM__ */
}

/* ------------------------------------------------------------------------ */

#define STORE_BSENSOR_MAP           8
#define STORE_BSENSOR_MAP_ADDR      ((UINT16) 0x200)
#define STORE_BSENSOR_MAP_INFO_ADDR ((UINT16) 0x300)

static BOOL adcb_load_map( void )
{
  /* Read the B-sensor mapping from EEPROM:
     function based on 'store_read_block()' in store.c */

  /* This function can not send CANopen EMERGENCY messages because
     at the time the map data is read the CAN interface
     possibly is not yet initialised or it might not yet be allowed
     to send any CAN messages... */

  UINT16 crc;
  UINT16 ee_offs;
  BYTE   i, sz, byt;
  BOOL   result = FALSE;

  /* Determine address of info block */
  ee_offs = STORE_BSENSOR_MAP_INFO_ADDR;

  /* Valid map data ? */
  if( eepromw_read( ee_offs ) == STORE_VALID_CHAR )
    {
      /* Get the CRC word */
      byt  = eepromw_read( ee_offs + 1 );
      crc  = (UINT16) byt;
      byt  = eepromw_read( ee_offs + 2 );
      crc |= (((UINT16) byt) << 8);

      /* Address of map data */
      ee_offs = STORE_BSENSOR_MAP_ADDR + 1;

      /* Read the length byte from EEPROM */
      sz = eepromw_read( ee_offs - 1 );

      /* Check if the length word makes sense */
      if( sz == ADCB_MAX_CNT )
	{
	  /* Read the data bytes from EEPROM */
	  for( i=0; i<sz; ++i ) AdcStringMap[i] = eepromw_read( ee_offs + i );

	  /* Check the CRC */
	  if( crc16_ram( AdcStringMap, sz ) == crc )
	    result = TRUE;  /* Okay! */
	}
    }
  else
    {
      /* If the data block is not valid the 'valid' byte and
	 the CRC bytes should all be equal to 0xFF */
      if( eepromw_read( ee_offs ) != 0xFF ||
	  eepromw_read( ee_offs+1 ) != 0xFF ||
	  eepromw_read( ee_offs+2 ) != 0xFF )
	/* Something is wrong with the info block */
	{
	}
      else
	{
	}
    }

  return result;
}

/* ------------------------------------------------------------------------ */

static BOOL adcb_store_map( void )
{
  /* Write the B-sensor mapping to EEPROM:
     function based on 'store_write_block()' in store.c */

  UINT16 crc;
  UINT16 ee_offs;
  BYTE   i, byt;
  BOOL   result = TRUE;

  /* Determine address of map data */
  ee_offs = STORE_BSENSOR_MAP_ADDR + 1;

  /* Store length byte in EEPROM and check */
  if( !write_and_check_w( ee_offs - 1, ADCB_MAX_CNT ) ) result = FALSE;

  WDR();

  /* Store data bytes in EEPROM and check */
  for( i=0; i<ADCB_MAX_CNT; ++i )
    {
      if( !write_and_check_w( ee_offs + i, AdcStringMap[i] ) ) result = FALSE;
      if( (i & 0x1F) == 0 ) WDR();
    }

  if( result == TRUE )
    {
      /* Address of info block */
      ee_offs = STORE_BSENSOR_MAP_INFO_ADDR;

      /* Calculate CRC */
      crc = crc16_ram( AdcStringMap, ADCB_MAX_CNT );

      /* Store CRC in EEPROM and check */
      byt = (BYTE) (crc & 0x00FF);
      if( !write_and_check_w( ee_offs + 1, byt ) ) result = FALSE;
      byt = (BYTE) ((crc & 0xFF00) >> 8);
      if( !write_and_check_w( ee_offs + 2, byt ) ) result = FALSE;

      if( result == TRUE )
	{
	  /* Store 'valid' and check it */
	  if( !write_and_check_w( ee_offs, STORE_VALID_CHAR ) ) result = FALSE;
	}
    }

  if( result == FALSE )
    {
      /* CANopen Error Code 0x5000: device hardware */
      can_write_emergency( 0x00, 0x50, EMG_EEPROM_WRITE_PARS,
			   STORE_BSENSOR_MAP, ADCB_MAX_CNT,
			   0, ERRREG_MANUFACTURER );

      /* Invalidate info block: write 0xFF to all locations in it */
      for( i=0; i<STORE_INFO_SIZE; ++i )
	write_and_check_w( ee_offs + i, 0xFF );
    }

  return result;
}

/* ------------------------------------------------------------------------ */

static void adcb_select( BYTE addr )
{
  /* Select the SPI-port on which this addressable B-sensor can be found */
  if( AdcStringMap[addr] < ADCB_MAX_STRINGS )
    adcb_string_port_select( AdcStringMap[addr] );
  else
    adcb_string_port_select( 0xFF );

  bsmicro_select( addr );
}

/* ------------------------------------------------------------------------ */

static void adcb_brc_select( BYTE string_no )
{
  /* Select the SPI-port with the corresponding string of B-sensors */
  adcb_string_port_select( string_no );

  /* Select all B-sensors on this string for a subsequent write operation */
  bsmicro_brc_select();
}

/* ------------------------------------------------------------------------ */

static void adcb_brc_deselect( BYTE string_no )
{
  /* Select the SPI-port with the corresponding string of B-sensors */
  adcb_string_port_select( string_no );

  /* Deselect all B-sensors on this string */
  bsmicro_brc_deselect();
}

/* ------------------------------------------------------------------------ */

static void adcb_brc_select_all_strings( void )
{
  BYTE str_no;
  for( str_no=0; str_no<ADCB_MAX_STRINGS; ++str_no )
    adcb_brc_select( str_no );
}

/* ------------------------------------------------------------------------ */

/* Constants used in temperature conversion */
#define AL 3.3540154E-03
#define BL 2.5627725E-04
#define CL 2.0829210E-06
#define DL 7.3003206E-08

#define AH 3.3539264E-03
#define BH 2.5609446E-04
#define CH 1.9621987E-06
#define DH 4.6045930E-08

static void adcb_convert_ohms_to_degrees( float ohms, BYTE *degrees )
{
  /* Using the AVR Studio Simulator it was determined that
     this function takes roughly about 3.5 ms to complete,
     about 2 ms of which is taken up by the call to log() */
  float r, ln_r, ln_r2, ln_r3;
  float temp;
  INT32 temp_l;
  BYTE  *ptr;

  r     = ohms / (float) 5000.0;
  ln_r  = log( r );
  ln_r2 = ln_r * ln_r;
  ln_r3 = ln_r2 * ln_r;

  if( r <= (float) 0.36036 )
    {
      /* 0.36036>=r>=0.06831 ==> 50C <= Temperature <= 100C */
      temp = AH + BH*ln_r + CH*ln_r2 + DH*ln_r3;
    }
  else
    {
      /* 3.274>=r>0.36036 ==> 0C <= Temperature < 50C */
      temp = AL + BL*ln_r + CL*ln_r2 + DL*ln_r3;
    }

  /* Temperature in millidegrees centigrade */
  temp = ((1/temp) - (float) 273.15) * (float) 1000.0;

  /* Check for reasonable value */
  if( temp > (float) 2147483647.0 || temp < (float) -2147483648.0 )
    temp_l = (INT32) 0xFFFFFFFF;
  else
    temp_l = (INT32) temp;

  /* Store INT32 value in byte array */
  ptr = (BYTE *) &temp_l;
  degrees[0] = *ptr++;
  degrees[1] = *ptr++;
  degrees[2] = *ptr++;
  degrees[3] = *ptr;
}

/* ------------------------------------------------------------------------ */
