/* ------------------------------------------------------------------------
File   : adc_bsensor.h

Descr  : Headerfile for adc_bsensor.c (with CS5524 ADC).

History: 28FEB.01; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#ifndef ADC_BSENSOR_H
#define ADC_BSENSOR_H

/* ------------------------------------------------------------------------ */
/* Configuration */

/* Configuration of the SPI serial interface for the MDT-DCS B-sensor ADCs */

#define __TESTING__

#ifndef __TESTING__

/* The real thing... */

/* String #0: connected to JTAG connector, pins 3, 5, 7 and 9 */
#define ADCB_SET_SCLK_0()           SETBIT( PORTA, 0 )
#define ADCB_CLEAR_SCLK_0()         CLEARBIT( PORTA, 0 )

#define ADCB_SET_SDI_0()            SETBIT( PORTA, 1 )
#define ADCB_CLEAR_SDI_0()          CLEARBIT( PORTA, 1 )

#define ADCB_SDO_HIGH_0()           (PINA & BIT(2))
#define ADCB_SDO_LOW_0()            (!(PINA & BIT(2)))

#define ADCB_SET_CS_0()             SETBIT( PORTA, 3 )
#define ADCB_CLEAR_CS_0()           CLEARBIT( PORTA, 3 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_0()           {SETBIT(DDRA,0);SETBIT(DDRA,1); \
                                     CLEARBIT(DDRA,2);SETBIT(PORTA,2); \
                                     SETBIT(DDRA,3);}

/* String #1: connected to JTAG connector, pins 11, 13, 15 and 17 */
#define ADCB_SET_SCLK_1()           SETBIT( PORTA, 4 )
#define ADCB_CLEAR_SCLK_1()         CLEARBIT( PORTA, 4 )

#define ADCB_SET_SDI_1()            SETBIT( PORTA, 5 )
#define ADCB_CLEAR_SDI_1()          CLEARBIT( PORTA, 5 )

#define ADCB_SDO_HIGH_1()           (PINA & BIT(6))
#define ADCB_SDO_LOW_1()            (!(PINA & BIT(6)))

#define ADCB_SET_CS_1()             SETBIT( PORTA, 7 )
#define ADCB_CLEAR_CS_1()           CLEARBIT( PORTA, 7 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_1()           {SETBIT(DDRA,4);SETBIT(DDRA,5); \
                                     CLEARBIT(DDRA,6);SETBIT(PORTA,6); \
                                     SETBIT(DDRA,7);}

/* String #2: connected to CSM-ADC connector, pins 3, 5, 7 and 9 */
#define ADCB_SET_SCLK_2()           SETBIT( PORTE, 4 )
#define ADCB_CLEAR_SCLK_2()         CLEARBIT( PORTE, 4 )

#define ADCB_SET_SDI_2()            SETBIT( PORTE, 5 )
#define ADCB_CLEAR_SDI_2()          CLEARBIT( PORTE, 5 )

#define ADCB_SDO_HIGH_2()           (PINE & BIT(6))
#define ADCB_SDO_LOW_2()            (!(PINE & BIT(6)))

#define ADCB_SET_CS_2()             SETBIT( PORTC, 3 )
#define ADCB_CLEAR_CS_2()           CLEARBIT( PORTC, 3 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_2()           {SETBIT(DDRE,4);SETBIT(DDRE,5); \
                                     CLEARBIT(DDRE,6);SETBIT(PORTE,6); \
                                     SETBIT(DDRC,3);}

/* String #3: connected to CSM-ADC connector, pins 11, 13, 15 and 17*/
#define ADCB_SET_SCLK_3()           SETBIT( PORTE, 7 )
#define ADCB_CLEAR_SCLK_3()         CLEARBIT( PORTE, 7 )

#define ADCB_SET_SDI_3()            SETBIT( PORTF, 2 )
#define ADCB_CLEAR_SDI_3()          CLEARBIT( PORTF, 2 )

#define ADCB_SDO_HIGH_3()           (PINF & BIT(3))
#define ADCB_SDO_LOW_3()            (!(PINF & BIT(3)))

#define ADCB_SET_CS_3()             SETBIT( PORTF, 4 )
#define ADCB_CLEAR_CS_3()           CLEARBIT( PORTF, 4 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_3()           {SETBIT(DDRE,7);SETBIT(DDRF,2); \
                                     CLEARBIT(DDRF,3);SETBIT(PORTF,3); \
                                     SETBIT(DDRF,4);}

#else

/* Testing with MDT-DCS module with string attached to B-sensor 0 connector
   and string connected to CSM-ADC or JTAG connector via Jaap's test board */

/* String #0: connected to connector "B-sensor 0" (so without buffers) */
#define ADCB_SET_SCLK_0()           SETBIT( PORTE, 3 )
#define ADCB_CLEAR_SCLK_0()         CLEARBIT( PORTE, 3 )

#define ADCB_SET_SDI_0()            SETBIT( PORTD, 3 )
#define ADCB_CLEAR_SDI_0()          CLEARBIT( PORTD, 3 )

#define ADCB_SDO_HIGH_0()           (PINF & BIT(7))
#define ADCB_SDO_LOW_0()            (!(PINF & BIT(7)))

#define ADCB_SET_CS_0()             SETBIT( PORTC, 0 )
#define ADCB_CLEAR_CS_0()           CLEARBIT( PORTC, 0 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_0()           {SETBIT(DDRE,3);SETBIT(DDRD,3); \
                                     CLEARBIT(DDRF,7);SETBIT(PORTF,7); \
                                     SETBIT(DDRC,0);}

/* String #1: connected to connector "B-sensor 0" (so without buffers),
   as 2nd sensor */
#define ADCB_SET_SCLK_1()           SETBIT( PORTE, 3 )
#define ADCB_CLEAR_SCLK_1()         CLEARBIT( PORTE, 3 )

#define ADCB_SET_SDI_1()            SETBIT( PORTD, 3 )
#define ADCB_CLEAR_SDI_1()          CLEARBIT( PORTD, 3 )

#define ADCB_SDO_HIGH_1()           (PINF & BIT(7))
#define ADCB_SDO_LOW_1()            (!(PINF & BIT(7)))

#define ADCB_SET_CS_1()             SETBIT( PORTC, 1 )
#define ADCB_CLEAR_CS_1()           CLEARBIT( PORTC, 1 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_1()           {SETBIT(DDRE,3);SETBIT(DDRD,3); \
                                     CLEARBIT(DDRF,7);SETBIT(PORTF,7); \
                                     SETBIT(DDRC,1);}

/* String #2: connected to connector "B-sensor 1" (so without buffers) */
#define ADCB_SET_SCLK_2()           SETBIT( PORTE, 3 )
#define ADCB_CLEAR_SCLK_2()         CLEARBIT( PORTE, 3 )

#define ADCB_SET_SDI_2()            SETBIT( PORTD, 3 )
#define ADCB_CLEAR_SDI_2()          CLEARBIT( PORTD, 3 )

#define ADCB_SDO_HIGH_2()           (PINF & BIT(7))
#define ADCB_SDO_LOW_2()            (!(PINF & BIT(7)))

#define ADCB_SET_CS_2()             SETBIT( PORTF, 0 )
#define ADCB_CLEAR_CS_2()           CLEARBIT( PORTF, 0 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_2()           {SETBIT(DDRE,3);SETBIT(DDRD,3); \
                                     CLEARBIT(DDRF,7);SETBIT(PORTF,7); \
                                     SETBIT(DDRF,0);}

/* String #3: connected to connector "B-sensor 1" (so without buffers),
   as 2nd sensor */
#define ADCB_SET_SCLK_3()           SETBIT( PORTE, 3 )
#define ADCB_CLEAR_SCLK_3()         CLEARBIT( PORTE, 3 )

#define ADCB_SET_SDI_3()            SETBIT( PORTD, 3 )
#define ADCB_CLEAR_SDI_3()          CLEARBIT( PORTD, 3 )

#define ADCB_SDO_HIGH_3()           (PINF & BIT(7))
#define ADCB_SDO_LOW_3()            (!(PINF & BIT(7)))

#define ADCB_SET_CS_3()             SETBIT( PORTF, 1 )
#define ADCB_CLEAR_CS_3()           CLEARBIT( PORTF, 1 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_3()           {SETBIT(DDRE,3);SETBIT(DDRD,3); \
                                     CLEARBIT(DDRF,7);SETBIT(PORTF,7); \
                                     SETBIT(DDRF,1);}

#endif /* __TESTING__ */
/* ------------------------------------------------------------------------ */

/* Maximum number of ADC devices (B-sensor modules) */
#define ADCB_MAX_CNT                0xF0

/* Maximum number of B-sensor strings */
#define ADCB_MAX_STRINGS            4

/* Number of analog inputs per B-sensor ADC */
#define ADCB_CHANS_PER_ADC          7

/* Number of analog inputs per B-sensor ADC, during a scan ! */
#define ADCB_CHANS_PER_ADC_SCAN     4

/* ------------------------------------------------------------------------ */

/* Conversion parameters */
#define ADCB_CONV_PARS_B            ((CS23_WORDRATE_15 << \
				      CS23_CSR_WORDRATE_SHIFT) | \
				     (CS23_GAIN_100MV  << \
				      CS23_CSR_GAIN_SHIFT) | \
				     CS23_CSR_BIPOLAR)
#define ADCB_CONV_PARS_T            ((CS23_WORDRATE_15 << \
				      CS23_CSR_WORDRATE_SHIFT) | \
				     (CS23_GAIN_2V5  << \
				      CS23_CSR_GAIN_SHIFT) | \
				     CS23_CSR_UNIPOLAR)

/* Configuration Register */
#ifdef _SINGLE_CHAN_BSENSOR_CONVERSION_
#define ADCB_CSR_DEPTH              1
#define ADCB_CONVERSION_MODE	    CS23_CNF_SINGLE_CONV
#else
#define ADCB_CSR_DEPTH              2
#define ADCB_CONVERSION_MODE	    CS23_CNF_MULTIPLE_CONV
#endif /* _SINGLE_CHAN_BSENSOR_CONVERSION_ */
#define ADCB_CSR_DEPTH_MASK         ((ADCB_CSR_DEPTH*2-1) << \
                                     CS23_CNF_CSR_DEPTH_SHIFT)
#define ADCB_CNFREG_0               0
#define ADCB_CNFREG_1               ADCB_CSR_DEPTH_MASK
#define ADCB_CNFREG_2_CHOP256       ((CS23_CHOPFREQ_256 << \
				      CS23_CNF_CHOPFREQ_SHIFT) | \
                                     ADCB_CONVERSION_MODE)
#define ADCB_CNFREG_2_CHOP4096      ((CS23_CHOPFREQ_4096 << \
				      CS23_CNF_CHOPFREQ_SHIFT) | \
				      ADCB_CONVERSION_MODE)

/* There is no signal rise/fall times to take into account,
   (no opto-couplers between ELMB and the B-sensor modules),
   but 10 mus is the minimum */
#define ADC_SIGNAL_RISETIME_BSENSOR   10
#define ADC_SIGNAL_FALLTIME_BSENSOR   10

/* Including the 10 mus overhead in the called timer routine
   the hold time should be about ... microseconds */
#define MICRO_SIGNAL_HOLDTIME_BSENSOR 30
#define MICRO_SELECT_HOLDTIME_BSENSOR 50

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void adcb_preinit              ( BOOL set_ddr );
BOOL adcb_init                 ( BYTE *err_id, BOOL send_emergency );
BOOL adcb_read                 ( BYTE addr,
			         BYTE channel_no,
			         BYTE *conversion_data );

BOOL adcb_status_summary       ( BYTE subindex, BYTE *status );
BOOL adcb_status               ( BYTE subindex, BYTE *status );

BYTE adcb_chan_cnt             ( void );
BYTE adcb_total_cnt            ( void );
BYTE adcb_string_cnt           ( BYTE str_no );
BYTE adcb_mapping              ( BYTE addr );
BYTE adcb_list                 ( BYTE index );

BOOL adcb_get_config           ( BYTE addr,    BYTE subindex,
			         BYTE *nbytes, BYTE *par );
BOOL adcb_set_config           ( BYTE addr,    BYTE subindex,
			         BYTE nbytes,  BYTE *par );

BOOL adcb_reset_and_calibrate  ( BYTE addr,       BOOL send_emergency );
BOOL adcb_reset                ( BYTE addr,       BOOL send_emergency );
BOOL adcb_calibrate            ( BYTE addr,       BOOL send_emergency );
BOOL adcb_calibrate_all        ( BYTE *no_of_err, BOOL send_emergency );

void adcb_pdo_scan_start       ( void );
void adcb_pdo_scan_stop        ( void );
void adcb_pdo_scan             ( void );

BOOL adcb_probe                ( void );
BOOL adcb_probe_addr           ( BYTE addr );
BOOL adcb_remove_addr          ( BYTE addr );

BOOL adcb_store_config         ( void );

/* ------------------------------------------------------------------------ */
#endif /* ADC_BSENSOR_H */
