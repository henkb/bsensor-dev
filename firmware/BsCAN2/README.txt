The directories in this folder contain BsCAN firmware versions
for an MDT-DCS module (MDM) for addressable B-sensors
of the 'microcontroller'-type (i.e. with B-sensor onboard microcontroller
to implement addressability, e.g. the MICE B-sensor modules).
The hardware and software constitutes the socalled BsCAN2 system,
the predecessor of the BsCAN3 system with B-sensor modules with 1-Wire chip
for addressing and selection (through the BATCAN and mBATCAN interface modules, instead of MDM).

The MDM module can be connected to an SPI breakout board ('SPI interface')
providing 4 connectors to 4 B-sensor 'strings' (chains of B-sensor modules)
as well as signal buffering (BsCAN126-MDM firmware required,
MDM B-sensor #0 connected to SPI interface for power, and MDM connectors
'JTAG' and 'CSM-ADC' connected for the B-sensor string signals).

The MDM module with B-sensor strings can also be operated without SPI interface
(this is not documented in the BsCAN2 firmware documentation),
where up to 2 B-sensor strings are connected to MDM connector 'B-sensor #0'
and 'B-sensor #1' (no buffering, not sure about length or number of sensors possible).
The MDM must run either the BsCAN126-MDM-without-SPIboard or BsCAN300-MDM-without-SPIboard firmware,
requiring BsCAN2ui (BsCAN2 system) and BsCAN3ui (BsCAN3 system) read-out software, respectively.
