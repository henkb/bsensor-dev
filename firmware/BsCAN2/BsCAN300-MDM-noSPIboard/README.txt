The BsCAN firmware version are dedicated to MDT-DCS modules (MDM).

BsCAN firmware version v3.0.0 is functionally the same as, but incompatible with BsCAN v1.2.6
because the latter is compatible with BsCAN3 host software (e.g. BsCAN3ui;
and BATCAN and BATsCAN3 firmware for BATCAN and mBATCAN modules resp.),
so e.g. the CANopen OD index for the B-sensor configuration has changed
(Objects 0x4xxx became 0x5xxx), and some (dummy) Object indices have been
added to keep BsCAN3 software tools happy.
