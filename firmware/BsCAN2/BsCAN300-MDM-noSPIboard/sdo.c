/* ------------------------------------------------------------------------
File   : sdo.c

Descr  : The CANopen SDO server, which serves read/write requests to the
	 Object Dictionary.

History: 25JAN.00; Henk B&B; Start of development of a version for the ELMB.
--------------------------------------------------------------------------- */

#include "general.h"
#include "adc.h"
#include "adc_bsensor.h"
#include "bsmicro.h"
#include "can.h"
#include "crc.h"
#include "guarding.h"
#include "lmt.h"
#include "objects.h"
#include "pdo.h"
#include "serialno.h"
#include "store.h"
#include "timer103.h"
#include "watchdog.h"

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BYTE sdo_expedited_read( BYTE *msg_data );
static BYTE sdo_expedited_write( BYTE *msg_data );
static void sdo_abort( BYTE error_class,
		       BYTE error_code,
		       BYTE *msg_data );

static void jump_to_bootloader( void );

/* ------------------------------------------------------------------------ */

void sdo_server( BYTE *msg_data )
{
  BYTE sdo_mode;
  BYTE cs;
  BYTE sdo_error;

  /* SDO modifier bits are in the first data byte */
  sdo_mode = msg_data[0];

  /* Determine the command specifier (cs) from
     the SDO modifier bits in the first byte */
  cs = sdo_mode & SDO_COMMAND_SPECIFIER_MASK;

  switch( cs )
    {
    case SDO_INITIATE_UPLOAD_REQ:
      /* ==> Read from the Object Dictionary <== */

      /* Expedited transfer only (data: 4 bytes or less) */

      /* Process the SDO received */
      sdo_error = sdo_expedited_read( msg_data );

      /* Send the SDO reply... */
      if( !sdo_error )
	{
	  /* All went okay */
	  can_write( C91_SDOTX, C91_SDOTX_LEN, msg_data );
	}
      else
	{
	  /* Aborted... */
	  sdo_abort( SDO_ECLASS_ACCESS, sdo_error, msg_data );
	}
      break;

    case SDO_INITIATE_DOWNLOAD_REQ:
      /* ==> Write to the Object Dictionary <== */

      if( sdo_mode & SDO_EXPEDITED )
	{
	  /* Expedited transfer (data: 4 bytes or less) */

	  /* Process the SDO received */
	  sdo_error = sdo_expedited_write( msg_data );

	  /* Send the SDO reply... */
	  if( !sdo_error )
	    {
	      /* All went okay */
	      can_write( C91_SDOTX, C91_SDOTX_LEN, msg_data );
	    }
	  else
	    {
	      /* Aborted... */
	      sdo_abort( SDO_ECLASS_ACCESS, sdo_error, msg_data );
	    }
	}
      else
	{
	  /* Start of segmented transfer (OD write): no service... */
	  sdo_abort( SDO_ECLASS_SERVICE, SDO_ECODE_PAR_ILLEGAL, msg_data );
	}
      break;

    case SDO_UPLOAD_SEGMENT_REQ:
      /* ==> Read from the Object Dictionary (segmented): no service... <== */
      sdo_abort( SDO_ECLASS_SERVICE, SDO_ECODE_PAR_ILLEGAL, msg_data );
      break;

    default:
      /* Unknown command specifier !? */
      sdo_abort( SDO_ECLASS_SERVICE, SDO_ECODE_PAR_ILLEGAL, msg_data );
      break;
    }
}

/* ------------------------------------------------------------------------ */

static BYTE sdo_expedited_read( BYTE *msg_data )
{
  BYTE sdo_error;
  BYTE nbytes;
  BYTE od_index_hi, od_index_lo, od_subind;

  /* No error */
  sdo_error   = SDO_ECODE_OKAY;

  /* Extract Object Dictionary indices */
  od_index_lo = msg_data[1];
  od_index_hi = msg_data[2];
  od_subind   = msg_data[3];

  /* Initialise data bytes to zero */
  msg_data[4] = 0;
  msg_data[5] = 0;
  msg_data[6] = 0;
  msg_data[7] = 0;

  /* Default number of significant bytes:
     set to a different value if it saves more statements,
     now default assuming 32-bit data item... */
  nbytes = 4;

  /* Get the requested object */
  switch( od_index_hi )
    {
    case OD_DEVICE_INFO_HI:
      switch( od_index_lo )
	{
	case OD_DEVICE_TYPE_LO:
	  if( od_subind == 0 )
	    {
	      msg_data[4] = DEVICE_TYPE_CHAR0;
	      msg_data[5] = DEVICE_TYPE_CHAR1;
	      msg_data[6] = DEVICE_TYPE_CHAR2;
	      msg_data[7] = DEVICE_TYPE_CHAR3;
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_ERROR_REG_LO:
	  if( od_subind == 0 )
	    {
	      msg_data[4] = CANopenErrorReg;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_STATUS_REG_LO:
	  if( od_subind == 0 )
	    {
	      /* B-sensor status ? */
	      //adcb_status( &msg_data[4] );
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_DEVICE_NAME_LO:
	  if( od_subind == 0 )
	    {
	      msg_data[4] = MNFCT_DEV_NAME_CHAR0;
	      msg_data[5] = MNFCT_DEV_NAME_CHAR1;
	      msg_data[6] = MNFCT_DEV_NAME_CHAR2;
	      msg_data[7] = MNFCT_DEV_NAME_CHAR3;
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_HW_VERSION_LO:
	  if( od_subind == 0 )
	    {
	      msg_data[4] = MNFCT_HARDW_VERSION_CHAR0;
	      msg_data[5] = MNFCT_HARDW_VERSION_CHAR1;
	      msg_data[6] = MNFCT_HARDW_VERSION_CHAR2;
	      msg_data[7] = MNFCT_HARDW_VERSION_CHAR3;
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_SW_VERSION_LO:
	  if( od_subind == 0 )
	    {
	      msg_data[4] = MNFCT_SOFTW_VERSION_CHAR0;
	      msg_data[5] = MNFCT_SOFTW_VERSION_CHAR1;
	      msg_data[6] = MNFCT_SOFTW_VERSION_CHAR2;
	      msg_data[7] = MNFCT_SOFTW_VERSION_CHAR3;
	    }
	  else
	    {
	      if( od_subind == 1 )
		{
		  msg_data[4] = SOFTW_MINOR_VERSION_CHAR0;
		  msg_data[5] = SOFTW_MINOR_VERSION_CHAR1;
		  msg_data[6] = SOFTW_MINOR_VERSION_CHAR2;
		  msg_data[7] = SOFTW_MINOR_VERSION_CHAR3;
		}
	      else
		{
		  /* The sub-index does not exist */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  break;

	case OD_GUARDTIME_LO:
	  if( od_subind == 0 )
	    {
	      nbytes = guarding_get_guardtime( &msg_data[4] );
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_LIFETIME_FACTOR_LO:
	  if( od_subind == 0 )
	    {
	      nbytes = guarding_get_lifetime( &msg_data[4] );
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_STORE_PARAMETERS_LO:
	case OD_DFLT_PARAMETERS_LO:
	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 3;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    case 1:
	    case 2:
	    case 3:
	      /* Device saves parameters on command (OD_STORE_PARAMETERS),
		 restores parameters (OD_DFLT_PARAMETERS) */
	      msg_data[4] = 0x01;

	      /* ###??? Device saves parameters autonomously
		 (OD_STORE_PARAMETERS_LO) */
	      /*if( od_ind_lo == OD_STORE_PARAMETERS_LO )
		msg_data[4] = 0x03; */
	      break;
	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	  break;

	case OD_HEARTBEAT_TIME_LO:
	  if( od_subind == 0 )
	    {
	      nbytes = guarding_get_heartbeattime( &msg_data[4] );
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_IDENTITY_LO:
	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 1;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    case 1:
	      msg_data[4] = 0x78;
	      msg_data[5] = 0x56;
	      msg_data[6] = 0x34;
	      msg_data[7] = 0x12;
	      break;
	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	  break;

	default:
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;

    case OD_RPDO_PAR_HI:
      if( od_index_lo < RPDO_CNT )
	{
	  if( rpdo_get_comm_par( od_index_lo, od_subind,
				 &nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_RPDO_MAP_HI:
      if( od_index_lo < RPDO_CNT )
	{
	  if( rpdo_get_mapping( od_index_lo, od_subind,
				&nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_TPDO_PAR_HI:
      if( od_index_lo < TPDO_CNT )
	{
	  if( tpdo_get_comm_par( od_index_lo, od_subind,
				 &nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_TPDO_MAP_HI:
      if( od_index_lo < TPDO_CNT )
	{
	  if( tpdo_get_mapping( od_index_lo, od_subind,
				&nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_CAN_CONFIG_HI:
      if( od_index_lo == OD_CAN_CONFIG_LO )
	{
	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 3;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    case 1:
	      msg_data[4] = can_get_rtr_disabled();
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    case 2:
	      msg_data[4] = can_get_opstate_init();
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    case 3:
	      msg_data[4] = can_get_busoff_maxcnt();
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;
	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_ANALOG_HI:
      if( od_index_lo < ADCB_MAX_CNT )
	{
	  if( od_subind == OD_NO_OF_ENTRIES )
	    {
	      msg_data[4] = ADCB_CHANS_PER_ADC;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      if( od_subind <= ADCB_CHANS_PER_ADC )
		{
		  /* Use lower byte of OD index to address B-sensor */
		  if( adcb_read( od_index_lo, od_subind-1,
				 &msg_data[4] ) == FALSE )
		    {
		      /* Something went wrong... */
		      sdo_error = SDO_ECODE_HARDWARE;
		    }
		  else
		    {
		      /* Performed a conversion */
		      nbytes = 3;  /* Significant bytes < 4 */
		    }
		}
	      else
		{
		  /* The sub-index does not exist */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_CONFIG_HI:
      if( od_index_lo < ADCB_MAX_CNT )
	{
	  /* Use lower byte of OD index to address B-sensor */
	  if( adcb_get_config( od_index_lo, od_subind,
			       &nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The sub-index does not exist or
		 cannot be accessed at the moment (ADC in use) */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_STAT_SUMM_HI:
      if( od_index_lo == OD_BSENSOR_STAT_SUMM_LO )
	{
	  if( od_subind == OD_NO_OF_ENTRIES )
	    {
	      msg_data[4] = (ADCB_MAX_CNT+31)/32;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      if( adcb_status_summary( od_subind-1, &msg_data[4] ) == FALSE )
		{
		  /* The sub-index does not exist */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_STATUS_HI:
      if( od_index_lo < ADCB_MAX_CNT )
	{
	  if( od_subind == 0 )
	    {
	      /* Use lower byte of OD index to address B-sensor */
	      if( adcb_status( od_index_lo, &msg_data[4] ) == FALSE )
		{
		  /* The sub-index does not exist or
		     cannot be accessed at the moment (ADC in use) */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_TRIGGER_HI:
      if( od_index_lo < 4 ) //BTRIGGER_INPUTS )
	{
	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 2;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;

	    case 1:
	      msg_data[4] = 0; //btrigger_get_enabled( od_index_lo );
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;

	    case 2:
	      msg_data[4] = 0; //btrigger_get_rising_edge( od_index_lo );
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;

	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	}
      else if( od_index_lo == 4 ) //BTRIGGER_INPUTS )
	{
	  msg_data[4] = 0xF0; //btrigger_get_config();
	  nbytes = 1;  /* Significant bytes < 4 */
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_LIST_HI:
      if( od_index_lo == OD_BSENSOR_LIST_LO )
	{
	  if( od_subind == OD_NO_OF_ENTRIES )
	    {
	      msg_data[4] = adcb_total_cnt();
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      if( od_subind <= adcb_total_cnt() )
		{
		  msg_data[4] = adcb_list( od_subind-1 );
		  nbytes = 1;  /* Significant bytes < 4 */
		}
	      else
		{
		  /* The sub-index does not exist */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_STRNG_CNT_HI:
      if( od_index_lo == OD_BSENSOR_STRNG_CNT_LO )
	{
	  if( od_subind == OD_NO_OF_ENTRIES )
	    {
	      msg_data[4] = ADCB_MAX_STRINGS;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      if( od_subind <= ADCB_MAX_STRINGS )
		{
		  msg_data[4] = adcb_string_cnt( od_subind-1 );
		  nbytes = 1;  /* Significant bytes < 4 */
		}
	      else
		{
		  /* The sub-index does not exist */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_MAP_HI:
      if( od_index_lo == OD_BSENSOR_MAP_LO )
	{
	  if( od_subind < ADCB_MAX_CNT )
	    {
	      msg_data[4] = adcb_mapping( od_subind );
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_ID_HI:
      if( od_index_lo < ADCB_MAX_CNT )
	{
	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 2;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;

	    case 1:
	      // ID low bytes are zeroes...
	      break;

	    case 2:
	      msg_data[7] = od_index_lo;
	      // The other 3 bytes are zeroes...
	      break;

	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_PROBE_HI:
      switch( od_index_lo )
	{
	case OD_BSENSOR_PROBE_LO:
	  if( od_subind == 0 )
	    {
	      if( adcb_probe() )
		{
		  /* Return the total number of B-sensors found */
		  msg_data[4] = adcb_total_cnt();
		  nbytes = 1;  /* Significant bytes < 4 */
		}
	      else
		{
		  /* Probing operation failed */
		  sdo_error = SDO_ECODE_HARDWARE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_BSENSOR_PROBE_ADR_LO:
	  if( od_subind < ADCB_MAX_CNT )
	    {
	      /* Probe for a B-sensor with address 'od_subind' */
	      if( adcb_probe_addr( od_subind ) )
		{
		  /* Return the current total number of B-sensors */
		  msg_data[4] = adcb_total_cnt();
		  nbytes = 1;  /* Significant bytes < 4 */
		}
	      else
		{
		  /* Probing operation failed */
		  sdo_error = SDO_ECODE_HARDWARE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_BSENSOR_REMOV_ADR_LO:
	  if( od_subind < ADCB_MAX_CNT )
	    {
	      /* Remove B-sensor with address 'od_subind'
		 from the configuration */
	      if( adcb_remove_addr( od_subind ) )
		{
		  /* Return the current total number of B-sensors */
		  msg_data[4] = adcb_total_cnt();
		  nbytes = 1;  /* Significant bytes < 4 */
		}
	      else
		{
		  /* Removal operation failed */
		  sdo_error = SDO_ECODE_HARDWARE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_BSENSOR_DESELECT_LO:
	  if( od_subind == 0 )
	    {
	      /* Return the total number of selected B-sensors
		 that should now be deselected */
	      //msg_data[4] = adcb_deactivate();
	      bsmicro_brc_deselect(); //adcb_deselect();
	      msg_data[4] = 0;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_BSENSOR_SELECT_LO:
	  if( od_subind < ADCB_MAX_CNT )
	    {
	      /* Toggle the output of the B-sensor's DS2405 device */
	      //if( adcb_toggle_select(od_subind, &msg_data[4]) == FALSE )
	      //{
	      //  /* No ID data or something wrong with ID data
	      //     or problem accessing the DS2405 */
	      //  sdo_error = SDO_ECODE_HARDWARE;
	      //}
	      msg_data[4] = 0;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_BSENSOR_AT_INIT_LO:
	  if( od_subind == 0 )
	    {
	      //msg_data[4] = adcb_get_probe_at_init();
	      msg_data[4] = 0;
	      nbytes = 1;  /* Significant bytes < 4 */
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	default:
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;

    case OD_CALC_CRC_HI:
      if( od_index_lo == OD_CALC_CRC_LO )
	{
	  UINT16 crc;
	  BYTE   result;

	  switch( od_subind )
	    {
	    case OD_NO_OF_ENTRIES:
	      msg_data[4] = 3;
	      nbytes = 1;  /* Significant bytes < 4 */
	      break;

	    case OD_CRC_MASTER_FLASH:
	    case OD_CRC_SLAVE_FLASH:
	      if( od_subind == OD_CRC_MASTER_FLASH )
		result = crc_master( &crc );
	      else
		result = crc_slave( &crc );

	      if( result == FALSE )
		{
		  /* Something went wrong... */
		  if( crc == (UINT16) 0 )
		    {
		      /* No CRC found... */
		      sdo_error = SDO_ECODE_ACCESS;
		    }
		  else
		    {
		      /* Access error while reading Master FLASH */
		      sdo_error = SDO_ECODE_HARDWARE;
		    }
		}
	      else
		{
		  nbytes = 2;  /* Significant bytes < 4 */
		  msg_data[4] = (BYTE) (crc & 0x00ff);
		  msg_data[5] = (BYTE) ((crc & 0xff00) >> 8);
		}
	      break;

	    case OD_CRC_MASTER_FLASH_GET:
	      result = crc_get( &msg_data[4] );
	      if( result == FALSE )
		{
		  /* No CRC found... */
		  sdo_error = SDO_ECODE_ACCESS;
		}
	      nbytes = 2;  /* Significant bytes < 4 */
	      break;

	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_ELMB_SERIAL_NO_HI:
      if( od_index_lo == OD_ELMB_SERIAL_NO_LO )
	{
	  if( od_subind == 0 )
	    {
	      if( sn_get_serial_number( &msg_data[4] ) == FALSE )
		{
		  /* EEPROM read operation failed
		     or Serial Number simply not present */
		  sdo_error = SDO_ECODE_HARDWARE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_COMPILE_OPTIONS_HI:
      if( od_index_lo == OD_COMPILE_OPTIONS_LO )
	{
	  if( od_subind == 0 )
	    {
#ifdef __7BIT_NODEID__
	      msg_data[4] |= 0x20;
#endif
#ifdef __RS232__
	      msg_data[4] |= 0x40;
#endif
#ifdef __VARS_IN_EEPROM__
	      msg_data[5] |= 0x01;
#endif
#ifdef __CAN_REFRESH__
	      msg_data[5] |= 0x10;
#endif
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    default:
      /* The index can not be accessed, does not exist */
      sdo_error = SDO_ECODE_NONEXISTENT;
      break;
    }

  /* Set appropriate SDO command specifier for reply... */
  msg_data[0]  = SDO_INITIATE_UPLOAD_RESP | SDO_EXPEDITED;

  /* ...and segment size (count of non-significant bytes) */
  msg_data[0] |= (SDO_SEGMENT_SIZE_INDICATED |
		  ((4-nbytes) << SDO_DATA_SIZE_SHIFT));

  return sdo_error;
}

/* ------------------------------------------------------------------------ */

static BYTE sdo_expedited_write( BYTE *msg_data )
{
  BYTE sdo_error;
  BYTE sdo_mode, nbytes;
  BYTE od_index_hi, od_index_lo, od_subind;

  /* No error */
  sdo_error   = SDO_ECODE_OKAY;

  /* Get the number of significant bytes */
  sdo_mode = msg_data[0];
  if( sdo_mode & SDO_DATA_SIZE_INDICATED )
    /* Size indicated */
    nbytes = 4-((sdo_mode & SDO_DATA_SIZE_MASK)>>SDO_DATA_SIZE_SHIFT);
  else
    /* If number of bytes is zero, size was not indicated... */
    nbytes = 0;

  /* Extract Object Dictionary indices */
  od_index_lo = msg_data[1];
  od_index_hi = msg_data[2];
  od_subind   = msg_data[3];

  /* Write the requested object */
  switch( od_index_hi )
    {
    case OD_BSENSOR_CONFIG_HI:
      // Allow address 255 to be able to reprogram address in erased EEPROM
      // (EEPROM erased: B-sensor module has address 255)
      if( od_index_lo < ADCB_MAX_CNT ||
	  (od_index_lo == 0xFF && od_subind == 25))
	{
	  /* Use lower byte of OD index to address B-sensor */
	  if( adcb_set_config( od_index_lo, od_subind,
			       nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* Setting the parameter did not succeed:
		 - the sub-index does not exist or
		 - the parameter is read-only or
		 - the number of bytes is incorrect or
		 - parameter value is out of range or
		 - parameter can not be set: ADC in use */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_BSENSOR_RST_CALIB_HI:
      if( od_index_lo < ADCB_MAX_CNT )
	{
	  if( od_subind == 0 )
	    {
	      if( nbytes <= 1 )
		{
		  /* Use lower byte of OD index to address B-sensor */
		  if( adcb_reset_and_calibrate( od_index_lo, TRUE ) == FALSE )
		    {
		      /* Reset or calibrate operation failed */
		      sdo_error = SDO_ECODE_HARDWARE;
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_TPDO_PAR_HI:
      if( od_index_lo < TPDO_CNT )
	{
	  if( tpdo_set_comm_par( od_index_lo, od_subind,
				 nbytes, &msg_data[4] ) == FALSE )
	    {
	      /* The subindex does not exist or the number of bytes
		 is incorrect or the parameter could not be written */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_SWITCH_TO_LOADER_HI:
      if( od_index_lo == OD_SWITCH_TO_LOADER_LO )
	{
	  if( od_subind == 0 )
	    {
	      if( nbytes <= 1 )
		{
		  /* Send a reply before making the jump... */
		  msg_data[0] = SDO_INITIATE_DOWNLOAD_RESP;
		  msg_data[4] = 0;
		  can_write( C91_SDOTX, C91_SDOTX_LEN, msg_data );
		  timer2_delay_ms( 5 );

		  /* ...there is a Bootloader: it will take control
		     (and also keep the Slave happy, if present) */
		  jump_to_bootloader();
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	}
      break;

    case OD_DEVICE_INFO_HI:
      switch( od_index_lo )
	{
	case OD_LIFETIME_FACTOR_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes <= 1 )
		{
		  /* Set new Life Time Factor */
		  if( guarding_set_lifetime( msg_data[4] ) == FALSE )
		    sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_STORE_PARAMETERS_LO:
	  if( od_subind == 1 || od_subind == 2 || od_subind == 3 )
	    {
	      if( nbytes == 4 || nbytes == 0 )
		{
		  /* Check for correct signature */
		  if( msg_data[4] == 's' && msg_data[5] == 'a' &&
		      msg_data[6] == 'v' && msg_data[7] == 'e' )
		    {
		      if( store_save_parameters( od_subind ) == FALSE )
			{
			  /* Something went wrong */
			  sdo_error = SDO_ECODE_HARDWARE;
			}
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_DFLT_PARAMETERS_LO:
	  if( od_subind == 1 || od_subind == 2 || od_subind == 3 )
	    {
	      if( nbytes == 4 || nbytes == 0 )
		{
		  /* Check for correct signature */
		  if( msg_data[4] == 'l' && msg_data[5] == 'o' &&
		      msg_data[6] == 'a' && msg_data[7] == 'd' )
		    {
		      if( store_set_defaults( od_subind ) == FALSE )
			{
			  /* Something went wrong */
			  sdo_error = SDO_ECODE_HARDWARE;
			}
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_HEARTBEAT_TIME_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes == 2 || nbytes == 0 )
		{
		  /* Set new Heartbeat Time */
		  if( guarding_set_heartbeattime( &msg_data[4] ) == FALSE )
		    sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	default:
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;

    case OD_CAN_CONFIG_HI:
      if( od_index_lo == OD_CAN_CONFIG_LO )
	{
	  switch( od_subind )
	    {
	    case 1:
	      if( nbytes <= 1 )
		{
		  if( can_set_rtr_disabled( msg_data[4] ) == FALSE )
		    sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      else
		/* Wrong number of bytes provided */
		sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    case 2:
	      if( nbytes <= 1 )
		{
		  if( can_set_opstate_init( msg_data[4] ) == FALSE )
		    sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      else
		/* Wrong number of bytes provided */
		sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    case 3:
	      if( nbytes <= 1 )
		{
		  if( can_set_busoff_maxcnt( msg_data[4] ) == FALSE )
		    sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	      else
		/* Wrong number of bytes provided */
		sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    default:
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	      break;
	    }
	}
      else
	{
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;

    case OD_ELMB_SERIAL_NO_HI:
      switch( od_index_lo )
	{
	case OD_ELMB_SERIAL_NO_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes == 4 || nbytes == 0 )
		{
		  /* Set the ELMB Serial Number */
		  if( sn_set_serial_number( &msg_data[4] ) == FALSE )
		    {
		      /* Something went wrong */
		      sdo_error = SDO_ECODE_HARDWARE;
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_ELMB_SN_WRITE_ENA_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes <= 1 )
		{
		  /* Enable a write-operation to the ELMB Serial Number */
		  if( sn_serial_number_write_enable( msg_data[4] ) == FALSE )
		    {
		      /* Something wrong with parameters */
		      sdo_error = SDO_ECODE_ATTRIBUTE;
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	default:
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;

    case OD_NODEID_CONFIG_HI:
      switch( od_index_lo )
	{
	case OD_NODEID_CONFIG_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes <= 1 )
		{
		  /* Set a new Node-ID */
		  if( lmt_by_sdo_set_nodeid( msg_data[4] ) == FALSE )
		    {
		      /* Something went wrong */
		      sdo_error = SDO_ECODE_HARDWARE;
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	case OD_NODEID_WRITE_ENA_LO:
	  if( od_subind == 0 )
	    {
	      if( nbytes == 4 || nbytes == 0 )
		{
		  /* Enable a write-operation to the Node-ID */
		  if( lmt_by_sdo_nodeid_write_enable( &msg_data[4] ) == FALSE )
		    {
		      /* Something wrong with parameters */
		      sdo_error = SDO_ECODE_ATTRIBUTE;
		    }
		}
	      else
		{
		  /* Wrong number of bytes provided */
		  sdo_error = SDO_ECODE_ATTRIBUTE;
		}
	    }
	  else
	    {
	      /* The sub-index does not exist */
	      sdo_error = SDO_ECODE_ATTRIBUTE;
	    }
	  break;

	default:
	  /* The index can not be accessed, does not exist */
	  sdo_error = SDO_ECODE_NONEXISTENT;
	  break;
	}
      break;
      
    default:
      /* The index can not be accessed, does not exist */
      sdo_error = SDO_ECODE_NONEXISTENT;
      break;
    }

  /* Set appropriate SDO command specifier for reply */
  msg_data[0] = SDO_INITIATE_DOWNLOAD_RESP;

  /* CANopen: bytes 4 to 7 reserved, so set to zero, except when programming
     the Slave: the SDO reply possibly contains a read memory byte... */
  if( od_index_hi != OD_PROGRAM_CODE_HI )
    {
      msg_data[4] = 0;
      msg_data[5] = 0;
      msg_data[6] = 0;
      msg_data[7] = 0;
    }

  return sdo_error;
}

/* ------------------------------------------------------------------------ */

static void sdo_abort( BYTE error_class,
		       BYTE error_code,
		       BYTE *msg_data )
{
  msg_data[0] = SDO_ABORT_TRANSFER;

  /* msg_data[1], msg_data[2], msg_data[3] should contain
     index and sub-index: leave intact */

  /* Error class */
  msg_data[7] = error_class;

  /* Error code */
  msg_data[6] = error_code;

  /* Additional code, not filled in for the time being */
  msg_data[5] = 0;
  msg_data[4] = 0;

  can_write( C91_SDOTX, C91_SDOTX_LEN, msg_data );
}

/* ------------------------------------------------------------------------ */
/* Function call which results in a jump to address 0xF000
   which starts the Bootloader program
   (provided the Bootloader size is set (by the fuses) to 4 kWords!) */

static void jump_to_bootloader( void )
{
  BYTE flashbyte;

  /* Set (byte) address in the proper registers (for ELPM access) */
  asm( "ldi R30,0x00" );
  asm( "ldi R31,0xE0" );

  /* Set RAMPZ register to access the upper 64k page of program memory */
  RAMPZ = 1;

  /* Read the program memory byte and store it in 'flashbyte' */
  asm( "elpm" );
  asm( "mov %flashbyte, R0" );

  /* Reset RAMPZ register */
  RAMPZ = 0;

  /* If there is no Bootloader, return to the user application ! */
  if( flashbyte == 0xFF )
    {
      /* CANopen Error Code 0x6000: device software */
      can_write_emergency( 0x00, 0x50, EMG_NO_BOOTLOADER,
			   0, 0, 0, ERRREG_MANUFACTURER );
      return;
    }
  
  /* Disable watchdog timer (if possible) */
  watchdog_disable();

  /* Disable all interrupts */
  CLI();

  /* Z-pointer: 0xF000 (word address) */
  asm( "ldi R30,0x00" );
  asm( "ldi R31,0xF0" );
  
  /* Jump to the Bootloader at (word) address 0xF000 */
  asm( "ijmp" );
}

/* ------------------------------------------------------------------------ */
