/* ------------------------------------------------------------------------
File   : lmt.c

Descr  : Contains functions implementing a kind of CAL LMT (Layer ManagemenT)
         protocol.

History: 03FEB.04; Henk B&B; Start of development.
--------------------------------------------------------------------------- */

#include "general.h"
#include "eeprom.h"
#include "serialno.h"

static BOOL LmtConfigMode = FALSE;
static BYTE LmtNodeId     = 0xFF;

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BOOL lmt_switch_mode          ( BOOL mode );
static BOOL lmt_switch_mode_selective( BYTE *serial, BOOL mode );
static BOOL lmt_config_nodeid        ( BYTE node_id );
static BOOL lmt_store_config         ( void );

/* ------------------------------------------------------------------------ */

void lmt_init( void )
{
  LmtConfigMode = FALSE;
  LmtNodeId     = 0xFF;
}

/* ------------------------------------------------------------------------ */

static BOOL lmt_switch_mode( BOOL mode )
{
  if( mode > 1 ) return FALSE;
  LmtConfigMode = mode;
  return TRUE;
}

/* ------------------------------------------------------------------------ */

static BOOL lmt_switch_mode_selective( BYTE *serial, BOOL mode )
{
  /* According to the LMT protocol manufacturer name (given by CiA),
     product name and serial number are to be given:
     here we only use the serial number, and not as BCD numbers
     but as a character string (namely the ELMB's Serial Number) */
  BYTE elmb_sn[4];

  sn_get_serial_number( elmb_sn );

  if( serial[0] == elmb_sn[0] && serial[1] == elmb_sn[1] &&
      serial[2] == elmb_sn[2] && serial[3] == elmb_sn[3] )
    return lmt_switch_mode( mode );
  else
    {
      lmt_switch_mode( FALSE );
      return FALSE;
    }
}

/* ------------------------------------------------------------------------ */

static BOOL lmt_config_nodeid( BYTE node_id )
{
  if( node_id > 127 && node_id != 0xFF ) return FALSE;
  LmtNodeId = node_id;
  return TRUE;
}

/* ------------------------------------------------------------------------ */

static BOOL lmt_store_config( void )
{
  UINT16 ee_addr;

  ee_addr = (UINT16) 0x107;
  eepromw_write( ee_addr, LmtNodeId );

  /* Check */
  if( eepromw_read( ee_addr ) != LmtNodeId )
    {
      eepromw_write( ee_addr, 0xFF );
      eepromw_write( ee_addr, 0xFF );
      return FALSE;
    }
  return TRUE;
}

/* ------------------------------------------------------------------------ */
/* The following functions implement ways to do LMT stuff using SDOs...,
   which of course is not CAL/CANopen, but is a lot simpler to do ! */
/* ------------------------------------------------------------------------ */

BOOL lmt_by_sdo_set_nodeid( BYTE node_id )
{
  BOOL result = FALSE;
  if( LmtConfigMode )
    {
      if( lmt_config_nodeid( node_id ) )
	{
	  if( lmt_store_config() ) result = TRUE;
	}
      lmt_switch_mode( FALSE );
    }
  return result;
}

/* ------------------------------------------------------------------------ */

BOOL lmt_by_sdo_nodeid_write_enable( BYTE *serial )
{
  return lmt_switch_mode_selective( serial, TRUE );
}

/* ------------------------------------------------------------------------ */
