/* ------------------------------------------------------------------------
File   : bsmicro.h

Descr  : Include file with definitions and function prototypes concerning the
         addressable B-sensor.

History: 11MAR.01; Henk B&B; Definition.
         22APR.04; Henk B&B; Adapted for MDT-DCS module based 4-string setup.
--------------------------------------------------------------------------- */

#ifndef BSMICRO_H
#define BSMICRO_H

#include "adc_bsensor.h"

/* ------------------------------------------------------------------------ */
/* Signals */

/* String #0 */
#define BSMICRO_SET_SCLK_0()        ADCB_SET_SCLK_0()
#define BSMICRO_CLEAR_SCLK_0()      ADCB_CLEAR_SCLK_0()

#define BSMICRO_SET_SDI_0()         ADCB_SET_SDI_0()
#define BSMICRO_CLEAR_SDI_0()       ADCB_CLEAR_SDI_0()

#define BSMICRO_SET_CS_0()          ADCB_SET_CS_0()
#define BSMICRO_CLEAR_CS_0()        ADCB_CLEAR_CS_0()

#define BSMICRO_MICROS_SELECT_0()   BSMICRO_SET_CS_0()
#define BSMICRO_ADCS_SELECT_0()     BSMICRO_CLEAR_CS_0()

/* String #1 */
#define BSMICRO_SET_SCLK_1()        ADCB_SET_SCLK_1()
#define BSMICRO_CLEAR_SCLK_1()      ADCB_CLEAR_SCLK_1()

#define BSMICRO_SET_SDI_1()         ADCB_SET_SDI_1()
#define BSMICRO_CLEAR_SDI_1()       ADCB_CLEAR_SDI_1()

#define BSMICRO_SET_CS_1()          ADCB_SET_CS_1()
#define BSMICRO_CLEAR_CS_1()        ADCB_CLEAR_CS_1()

#define BSMICRO_MICROS_SELECT_1()   BSMICRO_SET_CS_1()
#define BSMICRO_ADCS_SELECT_1()     BSMICRO_CLEAR_CS_1()

/* String #2 */
#define BSMICRO_SET_SCLK_2()        ADCB_SET_SCLK_2()
#define BSMICRO_CLEAR_SCLK_2()      ADCB_CLEAR_SCLK_2()

#define BSMICRO_SET_SDI_2()         ADCB_SET_SDI_2()
#define BSMICRO_CLEAR_SDI_2()       ADCB_CLEAR_SDI_2()

#define BSMICRO_SET_CS_2()          ADCB_SET_CS_2()
#define BSMICRO_CLEAR_CS_2()        ADCB_CLEAR_CS_2()

#define BSMICRO_MICROS_SELECT_2()   BSMICRO_SET_CS_2()
#define BSMICRO_ADCS_SELECT_2()     BSMICRO_CLEAR_CS_2()

/* String #3 */
#define BSMICRO_SET_SCLK_3()        ADCB_SET_SCLK_3()
#define BSMICRO_CLEAR_SCLK_3()      ADCB_CLEAR_SCLK_3()

#define BSMICRO_SET_SDI_3()         ADCB_SET_SDI_3()
#define BSMICRO_CLEAR_SDI_3()       ADCB_CLEAR_SDI_3()

#define BSMICRO_SET_CS_3()          ADCB_SET_CS_3()
#define BSMICRO_CLEAR_CS_3()        ADCB_CLEAR_CS_3()

#define BSMICRO_MICROS_SELECT_3()   BSMICRO_SET_CS_3()
#define BSMICRO_ADCS_SELECT_3()     BSMICRO_CLEAR_CS_3()

/* ------------------------------------------------------------------------ */
/* Protocol */

/* B-sensor addresses/IDs */
#define BSMICRO_BROADC_ID           0xFE
#define BSMICRO_MAX_ID              0xEF

/* B-sensor addressing SPI protocol bytes */
#define BSMICRO_SYNC_BYTE           0xF5
#define BSMICRO_SELECT              0x11
#define BSMICRO_SET_ID              0x21

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void bsmicro_select       ( BYTE addr );
void bsmicro_brc_select   ( void );
void bsmicro_brc_deselect ( void );
void bsmicro_set_addr     ( BYTE old_addr, BYTE new_addr );

/* ------------------------------------------------------------------------ */
#endif /* BSMICRO_H */
