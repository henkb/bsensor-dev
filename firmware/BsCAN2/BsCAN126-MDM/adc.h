/* ------------------------------------------------------------------------
File   : adc.h

Descr  : Include file with general ADC stuff.

History: 19JUL.01; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#ifndef ADC_H
#define ADC_H

/* ------------------------------------------------------------------------ */
/* ADC identifiers */

#define ADC_BSENSOR                 0

/* ------------------------------------------------------------------------ */
/* Error ID bits */

/* In 'AdcError' status byte */
#define ADC_ERR_RESET               0x01
#define ADC_ERR_CALIBRATION         0x02
#define ADC_ERR_TIMEOUT             0x04
#define ADC_ERR_CALIB_CNST          0x08
#define ADC_ABSENT                  0xFF
#define ADC_ERR_IN_HARDWARE         (ADC_ERR_RESET|ADC_ERR_CALIBRATION| \
                                     ADC_ERR_TIMEOUT)

/* In CAN-messages with ADC data */
#define ADC_ERR_CONVERSION          0x80

/* ------------------------------------------------------------------------ */
#endif /* ADC_H */

