/* ------------------------------------------------------------------------
File   : 103conf.h

Descr  : ELMB system configuration stuff for the ELMB Master processor
         (ATMega103/128).

History: 04APR.00; Henk B&B; Start of development.
         30NOV.00; Henk B&B; Version 3.1.
         16JAN.01; Henk B&B; Version 3.2.
	 07MAR.01; Henk B&B; MDT-DCS Version 1.0, based on ELMBio app v3.2.
	 04FEB.02; Henk B&B; MDT-DCS Version 1.3.
	 ..MAR.02; Henk B&B; MDT-DCS Version 1.4.
	 ..FEB.03; Henk B&B; MDT-DCS Version 2.0, for ELMB128 or ELMB103
	                     (no official release, but used anyway !)
	 ..NOV.03; Henk B&B; MDT-DCS Version 2.1.
	 18NOV.03; Henk B&B; MDT-DCS Version 2.1.0101.
	 18NOV.03; Henk B&B; MDT-DCS Version 2.1.0102:
	                     ds2401.c: ds2401_select() now may return FALSE;
			     adc_bsensor.c: bugfix in setting OD 2100h, sub 22
			     (opto-delay).
	 05JAN.04; Henk B&B; MDT-DCS Version 2.1.0103:
	                     - number of B-sensor modules now up to 4 instead
			       of 2.
	 06JAN.04; Henk B&B; MDT-DCS Version 2.2.0001:
	                     - T-sensor 'raw' readout (an ADC count for each
			       ADC channel, up to 64) can be configured.
			     - Error in placement of JTAG TDI and TDO: they
			       need to be swapped.
			     - Bugfix: RTR message may get lost when sending
			       a message at the same time, due to CAN send
			       buffer refresh (so don't refresh RTR buffer).
	 29JAN.04; Henk B&B; MDT-DCS Version 2.2.0002:
	                     - store.c, serialno.c, adc_ntc.c: new #defines
			       for errors.
			     - store.c: prepare for accepting parameter block
			       with wrong length (in case in a new version
			       the number of parameters change: don't ignore
			       previously saved parameters).
	 17MAR.04; Henk B&B; MDT-DCS Version 2.3.0000:
	                     - lmt.c/h: new files with functions for NodeID
			       configuration via CAN-bus.
			     - Additional objects for Node-ID configuration
			       via SDO (calling functions from lmt.c).
			     - JTAG-action implementation,
			       including status and sequences.
			     - MDTDCS.c:
			       - added calls to jtag_action_init() and
			         jtag_action().
			       - version string was not in Flash, as intended.
			     - digio.c:
			       - make sure never to loose a JTAG action
			         completion confirmation TPDO message.
			       - bug fix in reading inputs (PORTx --> PINx).
			       - digital-out init now on bit-by-bit basis.
			     - In 'xxx_load_config()' functions: preset
			       defaults and set parameters with saved
			       values even if number of parameters changed
			       (the ones not saved thus get their default).
			     - sdo.h: define 'no error' code SDO_ECODE_OKAY.
			     - jtag_ops.c: check on valid od_subind forgotten.
			     - can.c: added a toggle bit to Emergency message.
			     - adc_bsensor.c: renamed AdcCnt to AdcMsk, because
			       B-sensor presence is now mask-controlled
			       (any B-sensor in the mask of 0xF may be present)
			     - sdo.c: reply to 'activate Bootloader' request,
			       before actually jumping to the Bootloader.
			     - mdttest.c/sdo.c: added additional test.
			     - pdo.c: enable timer-triggered readout,
			       independent of PDO transmission type.
			     - adc_ntc.c:
			       - bugfix in adc_scan_next() (check
			         on ADC problem was wrong).
			       - bugfix in temperature conversion: constants
			         for the 2 temperature ranges were swapped.
	 17MAR.04; Henk B&B; MDT-DCS Version 2.3.0001:
	                     - adc_ntc.c/h: added 'readout-on-delta-change'
			       feature controlled by PDO event timer +
			       1 delta value for all channels.
	                     - adc_csm.c/h: idem.
			     - store.h: new defines for 'on-change' feature.
			     - pdo.c: make sure ADC ref values will get
			       initialized when going Operational (for the
			       'readout-on-delta-change' feature).
	 29MAR.04; Henk B&B; MDT-DCS Version 2.3.0002:
	                     - mdttest.c: fixes to signal settle delays.
			     - adc_ntc.c/adc_csm.c: delta-change now also works
			       for bipolar signals (signed ADC-count).
			     - adc_csm.h:
			       - 5V bipolar mode is the default.
			       - channel 11 is the default reference input.
			     - can.c: remove can_recv_descriptor_refresh()
	                       from can_write(): message loss is possible.
			     - jtag_act.c: several bugfixes.
	 16APR.04; Henk B&B; MDT-DCS Version 2.3.0003:
	                     - adc_csm.c: bugfix in csm_calibrate() (forgotten
			       to update Gain Register 3, i.e. division by 2).
			     - store.h: bugfix: too little space for working
			       copy of PDO parameters.
			     - adc_xxx.c, cs5523.c:
			       - ADC time-out now 1200 ms.
			       - after conversion time-out (PDO): reset/calib.
	 29APR.04; Henk B&B; BsCAN Version 1.0.0:
	 24SEP.04; Henk B&B; BsCAN Version 1.1.0:
	                     - Made B-sensor microcontroller signal holdtime
			       configurable (default=10 mus).
	                     - Made B-sensor microcontroller select signal
			       holdtime configurable (default=50 mus).
	 28SEP.04; Henk B&B; BsCAN Version 1.1.1:
	                     - Add object to change B-sensor address
			       (for expert use only...).
			     - Make sure an invalid StringId results in no
			       operation (rather than operations on String #0).
	 10OCT.04; Henk B&B; BsCAN Version 1.1.2:
	                     - Add more ADC initialization before each
			       string probe op in adcb_probe(), as well as
			       in adcb_preinit().
			     - Add possibility to change address 255 (0xFF)
			       on string #0 (in addition to address change
			       for existing B-sensor addresses (<=239)).
	 25NOV.04; Henk B&B; BsCAN Version 1.1.3:
	                     - Add enable/disable of reset-and-calibrate
			       recover procedure after ADC time-out in
			       readout-loop.
	 01MAR.05; Henk B&B; BsCAN Version 1.2.0:
	                     - Add Object 5B01h, through which it is possible
			       to probe for an individual B-sensor module
			       with a particular address.
	 01SEP.05; Henk B&B; BsCAN Version 1.2.1:
	                     - Bugfixes in sdo.c and adc_bsensor.c to make sure
			       the possibility to change address 255 (0xFF)
			       on string #0 (see above, version 1.1.2)
			       works properly.
	 16SEP.05; Henk B&B; BsCAN Version 1.2.2:
	                     - Make it possible to change address 255 (0x255)
			       on all strings (not just #0), using high byte.
			     - Add Object 5B02h, through which it is possible
			       to remove individual B-sensor modules from
			       the current configuration.
	 16SEP.05; Henk B&B; BsCAN Version 1.2.3:
	                     adc_bsensor.c:
	                     - Set Object 40XXh, subindex 26 ('ADC recovery')
			       default to true (=1), in adc_bsensor.c
			     - Check a calibration constant of each B-sensor
			       module after each scan, and reinit if necessary;
			       if this happens send an Emergency with a new
			       id (0x56=EMG_ADC_CALIBCONST_BSE);
			       valid: 0x41 <= calibconst high byte <= 0x4A
	 05OCT.05; Henk B&B; BsCAN Version 1.2.4:
	                     adc_bsensor.c:
			     - Add some WDR() (watchdog reset) around
			       reset and calibration sequence.
	 20JAN.06; Henk B&B; BsCAN Version 1.2.5:
	                     adc_bsensor.c:
			     - Check for valid calibration constant:
			       changed valid range to:
			       0x41 <= calibconst high byte <= 0x4B
			     - Store B-sensor address in unused ADC Channel-
			       Setup-Register at powerup/reset and reread
			       after each scan, send Emergency and reinit
			       if mismatch is found.
			     - Bugfix: adcb_set_config(): writing to Offset
			       and Gain Reg was reversed (even index was Gain
			       but should be Offset).
	 19JUL.06; Henk B&B; BsCAN Version 1.2.6:
	                     adc_bsensor.c:
			     - Check for valid calibration constant:
			       changed valid range to:
			       0x31 <= calibconst high byte <= 0x5F
--------------------------------------------------------------------------- */

#ifndef CONF103_H
#define CONF103_H

/* ------------------------------------------------------------------------ */
/* Node identification */

/* CANopen device type = 0x000F0191,
   i.e. DSP-401 device profile supporting analog in- and outputs
   and digital in- and outputs */
#define DEVICE_TYPE_CHAR0               0x91
#define DEVICE_TYPE_CHAR1               0x01
#define DEVICE_TYPE_CHAR2               0x07
#define DEVICE_TYPE_CHAR3               0x00

/* Manufacturer device name: ELMB */
#define MNFCT_DEV_NAME_CHAR0            'E'
#define MNFCT_DEV_NAME_CHAR1            'L'
#define MNFCT_DEV_NAME_CHAR2            'M'
#define MNFCT_DEV_NAME_CHAR3            'B'

/* Hardware version: ELMB version 4.0 (ELMB128) */
#define MNFCT_HARDW_VERSION_CHAR0       'e'
#define MNFCT_HARDW_VERSION_CHAR1       'l'
#define MNFCT_HARDW_VERSION_CHAR2       '4'
#define MNFCT_HARDW_VERSION_CHAR3       '0'

/* Firmware version */
#define MNFCT_SOFTW_VERSION_CHAR0       'B'
#define MNFCT_SOFTW_VERSION_CHAR1       'S'
#define MNFCT_SOFTW_VERSION_CHAR2       '1'
#define MNFCT_SOFTW_VERSION_CHAR3       '2'

/* Firmware 'minor' version */
#define SOFTW_MINOR_VERSION_CHAR0       '0'
#define SOFTW_MINOR_VERSION_CHAR1       '0'
#define SOFTW_MINOR_VERSION_CHAR2       '0'
#define SOFTW_MINOR_VERSION_CHAR3       '6'

#define VERSION_STRING              " BsCAN v1.2.5, 20 Jan 2006, HB, NIKHEF "

/* ------------------------------------------------------------------------ */
/* Port pin usage */

/*
  PORTB (Normal operational mode)
  ======
  BIT	FUNCTION	I/O	DfltDATA  DDRB  PORTB
  ---   --------        ---     --------  ----  -----
  b7	---		in	pullup      0	  1
  b6	CAN_CS_		out	1	    1	  1 
  b5	CAN_W_		out	1           1     1
  b4	---		in	pullup	    0     1
  b3	SDO		in	pullup	    0     1
  b2	SDI		out	0	    1     0
  b1	SCLK		out	0	    1     0
  b0	---		in	pullup	    0     1
                                           ---   ---
				           $66   $F9
  PORTB (Read jumper settings mode)
  ======
  (note: PB0 and PB1 have double functions):
  BIT	FUNCTION	I/O	DfltData  DDRB  PORTB
  ---   --------        ---     --------  ----  -----
  b7	NODEID_SELECT	out	1	    1     1
  b6	---		in	pullup      0	  1
  b5	ID bit5		in	pullup      0	  1
  b4	ID bit4		in	pullup	    0	  1
  b3	ID bit3 	in	pullup	    0     1
  b2	ID bit2		in	pullup	    0     1
  b1	ID/Baud	bit1/1	in	pullup	    0	  1
  b0	ID/Baud	bit0/0	in	pullup	    0     1
                                           ---   ---
				           $80   $FF  
  Jumpers are configured as follows:
  BIT   JUMPERS  FUNCTION
  ---   -------  --------
  PB0	  o o    Baudrate  
  PB1     o o    Baudrate
  PB0     o o    CAN Node-ID LSB bit0 (1)
  PB1	  o o    CAN Node-ID	 bit1 (2)
  PB2	  o o    CAN Node-ID	 bit2 (4)
  PB3	  o o    CAN Node-ID	 bit3 (8)
  PB4	  o o    CAN Node-ID	 bit4 (16)
  PB5	  o o    CAN Node-ID MSB bit5 (32)

  PORTD (Normal operational mode)
  ======
  BIT	FUNCTION	I/O	DfltData  DDRD  PORTD
  ---   --------        ---     --------  ----  -----
  d7	(user defined)	---	---	    0     0
  d6	(user defined)	---	---	    0     0
  d5	(user defined)	---	---	    0     0
  d4	(user defined)	---	---	    0     0
  d3	(user defined)	---	---	    0     0
  d2	MASTERtoSLAVE	out	1	    1	  1
  d1	CAN_INT_	in	pullup	    0     1
  d0	SLAVE_RESET_	in	pullup	    0	  1
                                           ---   ---
					   $04	 $07
  PORTD (Slave programming mode)
  ======
  BIT	FUNCTION	I/O	DfltData  DDRD  PORTD
  ---   --------        ---     --------  ----  -----
  d7	(user defined)	---	---	    0     0
  d6	(user defined)	---	---	    0     0
  d5	(user defined)	---	---	    0     0
  d4	(user defined)	---	---	    0     0
  d3	(user defined)	---	---	    0     0
  d2	(MASTERtoSLAVE)	in	pullup	    0	  1
  d1	ISP_SCK		out	0	    1     0
  d0	SLAVE_RESET_	out	1	    1	  1
                                           ---   ---
					   $03	 $05

  PORTE (Normal operational mode)
  ======
  BIT	FUNCTION	I/O	DfltData  DDRE  PORTE
  ---   --------        ---     --------  ----  -----
  e7	(user defined)	---	---	    0     0
  e6	(user defined)	---	---	    0     0
  e5	(user defined)	---	---	    0     0
  e4	(user defined)	---	---	    0     0
  e3	(user defined)	---	---	    0     0
  e2	BAUDRATE_SELECT	out	1	    1     1
  e1	(ISP_MISO)	in	pullup      0     1
  e0	(ISP_MOSI)	in	pullup      0     1
                                           ---   ---
					   $04	 $07
  (NOTE: PE1 is forced to TXD output if UART-TXEN bit is set;
         PE0 is forced to RXD input  if UART-RXEN bit is set)

  PORTE (Slave programming mode)
  ======
  BIT	FUNCTION	I/O	DfltData  DDRE  PORTE
  ---   --------        ---     --------  ----  -----
  e7	(user defined)	---	---	    0     0
  e6	(user defined)	---	---	    0     0
  e5	(user defined)	---	---	    0     0
  e4	(user defined)	---	---	    0     0
  e3	(user defined)	---	---	    0     0
  e2	BAUDRATE_SELECT	out	1	    1     1
  e1	ISP_MISO	in	pullup      0     1
  e0	ISP_MOSI	out	1           1     1
                                           ---   ---
					   $05	 $07
*/

/* ------------------------------------------------------------------------ */
/* Default port settings */

/* PORTB data direction and default data bitmasks */
#define PORTB_DDR_OPERATIONAL           0x66
#define PORTB_DATA_OPERATIONAL          0xF9
#define PORTB_DDR_FOR_JUMPERS           0x80
#define PORTB_DATA_FOR_JUMPERS          0xFF

/* PORTD data direction and default data bitmasks */
#define PORTD_DDR_OPERATIONAL           0x04
#define PORTD_DATA_OPERATIONAL          0x07
#define PORTD_DDR_FOR_ISP               0x03
#define PORTD_DATA_FOR_ISP              0x05
#define PORTD_USERBITS_MASK             0xF8

/* PORTE data direction and default data bitmasks */
#define PORTE_DDR_OPERATIONAL           0x04
#define PORTE_DATA_OPERATIONAL          0x07
#define PORTE_DDR_FOR_ISP               0x05
#define PORTE_DATA_FOR_ISP              0x07
#define PORTE_USERBITS_MASK             0xF8

/* ------------------------------------------------------------------------ */
/* Pin usages */

/* Reset to the 'Slave' microcontroller */
#define SLAVE_RESET_                    0
#define SET_SLAVE_RESET()               CLEARBIT( PORTD, SLAVE_RESET_ )
#define CLEAR_SLAVE_RESET()             SETBIT( PORTD, SLAVE_RESET_ )

/* Master/Slave activity monitoring ('watchdog' function) */
#define MASTER_TO_SLAVE                 2
#define SET_MASTER_TO_SLAVE_HIGH()      SETBIT( PORTD, MASTER_TO_SLAVE )
#define SET_MASTER_TO_SLAVE_LOW()       CLEARBIT( PORTD, MASTER_TO_SLAVE )
#define SET_MASTER_TO_SLAVE_OUTPUT()    SETBIT( DDRD, MASTER_TO_SLAVE )
#define SET_MASTER_TO_SLAVE_INPUT()     CLEARBIT( DDRD, MASTER_TO_SLAVE )
#define MASTER_TO_SLAVE_HIGH()          (PIND & BIT(MASTER_TO_SLAVE))
#define MASTER_TO_SLAVE_LOW()           !(PIND & BIT(MASTER_TO_SLAVE))

/* Jumpers */
#define BAUDRATE_ENABLE                 2
#define BAUDRATE_JUMPERS_SELECT()       CLEARBIT( PORTE, BAUDRATE_ENABLE )
#define BAUDRATE_JUMPERS_DESELECT()     SETBIT( PORTE, BAUDRATE_ENABLE )
#define BAUDRATE_JUMPERS_MASK           0x03
#define NODEID_ENABLE                   7
#define NODEID_JUMPERS_SELECT()         CLEARBIT( PORTB, NODEID_ENABLE )
#define NODEID_JUMPERS_DESELECT()       SETBIT( PORTB, NODEID_ENABLE )
#define NODEID_JUMPERS_MASK             0x3f

/* ISP serial port for programming of Slave microcontroller */
#define ISP_SCK                         1
#define SET_ISP_SCK()                   SETBIT( PORTD, ISP_SCK )
#define CLEAR_ISP_SCK()                 CLEARBIT( PORTD, ISP_SCK )
#define ISP_MOSI                        0
#define SET_ISP_MOSI()                  SETBIT( PORTE, ISP_MOSI )
#define CLEAR_ISP_MOSI()                CLEARBIT( PORTE, ISP_MOSI )
#define ISP_MISO                        1
#define ISP_MISO_SET()                  (PINE & BIT(ISP_MISO))

/* SAE81C91 CAN-controller */
#define CAN_W_                          5
#define CAN_WRITE_ENABLE()              CLEARBIT( PORTB, CAN_W_ )
#define CAN_READ_ENABLE()               SETBIT( PORTB, CAN_W_ )
#define CAN_CS_                         6
#define CAN_SELECT()                    CLEARBIT( PORTB, CAN_CS_ )
#define CAN_DESELECT()                  SETBIT( PORTB, CAN_CS_ )
#define CAN_INT_                        1
#define CAN_INT_HIGH()                  (PIND & BIT(CAN_INT_))
#define CAN_INT_ENABLE()                EIMSK |= BIT(INT1)
#define CAN_INT_DISABLE()               EIMSK &= ~BIT(INT1)

/* SPI serial interface */
#define SCLK                            1
#define SET_SCLK()                      SETBIT( PORTB, SCLK )
#define CLEAR_SCLK()                    CLEARBIT( PORTB, SCLK )
#define SDI                             2
#define SET_SDI()                       SETBIT( PORTB, SDI )
#define CLEAR_SDI()                     CLEARBIT( PORTB, SDI )
#define SDO                             3
#define SDO_SET()                       (PINB & BIT(SDO))

/* ------------------------------------------------------------------------ */
/* Baudrate jumper readings */

#define BAUD50K                         0x00
#define BAUD500K                        0x01
#define BAUD250K                        0x02
#define BAUD125K                        0x03

#endif

/* ------------------------------------------------------------------------ */
