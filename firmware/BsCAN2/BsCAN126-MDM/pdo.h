/* ------------------------------------------------------------------------
File   : pdo.h

Descr  : Include file for the PDO routines in pdo.c.

History: 19JUL.00; Henk B&B; Definition.
	   AUG.00; Henk B&B; Addition of PDOs for digital input and output.
	   NOV.00; Henk B&B; Addition of PDO for analogue output (DAC).
--------------------------------------------------------------------------- */

#ifndef PDO_H
#define PDO_H

/* Number of Transmit-PDOs */
#define TPDO_CNT            4

/* Number of Receive-PDOs */
#define RPDO_CNT            4

/* Which PDO is used for what */
#define TPDO_DIGITAL_IN     (1-1)
#define TPDO_ADC_NTC        (2-1)
#define TPDO_ADC_CSM        (3-1)
#define TPDO_ADC_BSENSOR    (4-1)
#define RPDO_DIGITAL_OUT    (1-1)
#define RPDO_JTAG_INSTR     (2-1)
#define RPDO_JTAG_DATA      (3-1)
#define RPDO_JTAG_INSTR_BRC (4-1)
#define RPDO_JTAG_DATA_BRC  (5-1)

/* ------------------------------------------------------------------------ */
/* Globals */

/* For timer-triggered PDO transmissions */
extern BOOL   TPdoOnTimer[];

/* Keeps track of time for the timer-triggered PDO transmissions */
extern BYTE TPdoTimerCntr[];

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void pdo_init          ( void );
void tpdo_scan         ( void );
void pdo_on_nmt        ( BYTE nmt_request );
void tpdo_on_sync      ( void );
void tpdo4_on_rtr      ( void );
BOOL pdo_rtr_required  ( void );

BOOL tpdo_get_comm_par ( BYTE pdo_no,
			 BYTE od_subind,
			 BYTE *nbytes,
			 BYTE *par );
BOOL rpdo_get_comm_par ( BYTE pdo_no,
			 BYTE od_subind,
			 BYTE *nbytes,
			 BYTE *par );

BOOL tpdo_get_mapping  ( BYTE pdo_no,
			 BYTE od_subind,
			 BYTE *nbytes,
			 BYTE *par );
BOOL rpdo_get_mapping  ( BYTE pdo_no,
			 BYTE od_subind,
			 BYTE *nbytes,
			 BYTE *par );

BOOL tpdo_set_comm_par ( BYTE pdo_no,
			 BYTE od_subind,
			 BYTE nbytes,
			 BYTE *par );

BOOL pdo_store_config  ( void );

#endif /* PDO_H */
/* ------------------------------------------------------------------------ */
