/* ------------------------------------------------------------------------
File   : bsmicro.c

Descr  : Routines implementing selection of B-sensor modules that have
         an on-board microcontroller for addressability;
	 a protocol has been designed that is described in a separate
	 document: "B-sensor with Addressable SPI-interface"

History: 11MAR.01; Henk B&B; Definition.
         22APR.04; Henk B&B; Adapted for MDT-DCS module based 4-string setup.
--------------------------------------------------------------------------- */

#include "general.h"
#include "bsmicro.h"
#include "timer103.h"

/* ID of the currently selected B-sensor string */
extern BYTE StringId;

extern BYTE MicroDelayBsensor;
extern BYTE MicroSelectDelayBsensor;

/* ------------------------------------------------------------------------ */

static void bsmicro_write_byte( BYTE byt );

/* ------------------------------------------------------------------------ */

void bsmicro_select( BYTE addr )
{
  /* Wake up all connected B-sensor microcontrollers */
  switch( StringId )
    {
    case 0:
      BSMICRO_MICROS_SELECT_0();
      break;
    case 1:
      BSMICRO_MICROS_SELECT_1();
      break;
    case 2:
      BSMICRO_MICROS_SELECT_2();
      break;
    case 3:
      BSMICRO_MICROS_SELECT_3();
      break;
    default:
      break;
    }

  //timer2_delay_mus( 50 );
  timer2_delay_mus( MicroSelectDelayBsensor );

  bsmicro_write_byte( BSMICRO_SYNC_BYTE );
  bsmicro_write_byte( BSMICRO_SELECT );
  bsmicro_write_byte( addr );

  /* Selected B-sensor ADC is enabled, microcontrollers go back to sleep */
  switch( StringId )
    {
    case 0:
      BSMICRO_ADCS_SELECT_0();
      break;
    case 1:
      BSMICRO_ADCS_SELECT_1();
      break;
    case 2:
      BSMICRO_ADCS_SELECT_2();
      break;
    case 3:
      BSMICRO_ADCS_SELECT_3();
      break;
    default:
      break;
    }

  //timer2_delay_mus( 50 );
  timer2_delay_mus( MicroSelectDelayBsensor );
}

/* ------------------------------------------------------------------------ */

void bsmicro_brc_select( void )
{
  bsmicro_select( BSMICRO_BROADC_ID );
}

/* ------------------------------------------------------------------------ */

void bsmicro_brc_deselect( void )
{
  /* Wake up all connected B-sensor microcontrollers */
  switch( StringId )
    {
    case 0:
      BSMICRO_MICROS_SELECT_0();
      break;
    case 1:
      BSMICRO_MICROS_SELECT_1();
      break;
    case 2:
      BSMICRO_MICROS_SELECT_2();
      break;
    case 3:
      BSMICRO_MICROS_SELECT_3();
      break;
    default:
      break;
    }

  //timer2_delay_mus( 50 );
  timer2_delay_mus( MicroSelectDelayBsensor );

  /* Selected B-sensor ADC is enabled, microcontrollers go back to sleep */
  switch( StringId )
    {
    case 0:
      BSMICRO_ADCS_SELECT_0();
      break;
    case 1:
      BSMICRO_ADCS_SELECT_1();
      break;
    case 2:
      BSMICRO_ADCS_SELECT_2();
      break;
    case 3:
      BSMICRO_ADCS_SELECT_3();
      break;
    default:
      break;
    }

  //timer2_delay_mus( 50 );
  timer2_delay_mus( MicroSelectDelayBsensor );
}

/* ------------------------------------------------------------------------ */

void bsmicro_set_addr( BYTE old_addr, BYTE new_addr )
{
  /* Wake up all connected B-sensor microcontrollers */
  switch( StringId )
    {
    case 0:
      BSMICRO_MICROS_SELECT_0();
      break;
    case 1:
      BSMICRO_MICROS_SELECT_1();
      break;
    case 2:
      BSMICRO_MICROS_SELECT_2();
      break;
    case 3:
      BSMICRO_MICROS_SELECT_3();
      break;
    default:
      break;
    }

  //timer2_delay_mus( 50 );
  timer2_delay_mus( MicroSelectDelayBsensor );

  bsmicro_write_byte( BSMICRO_SYNC_BYTE );
  bsmicro_write_byte( BSMICRO_SET_ID );
  bsmicro_write_byte( old_addr );
  bsmicro_write_byte( new_addr );

  /* New address is written to EEPROM, microcontrollers go back to sleep */
  switch( StringId )
    {
    case 0:
      BSMICRO_ADCS_SELECT_0();
      break;
    case 1:
      BSMICRO_ADCS_SELECT_1();
      break;
    case 2:
      BSMICRO_ADCS_SELECT_2();
      break;
    case 3:
      BSMICRO_ADCS_SELECT_3();
      break;
    default:
      break;
    }

  /* Note: delay in millisecond ! */
  timer2_delay_ms( 5 );
}

/* ------------------------------------------------------------------------ */

static void bsmicro_write_byte( BYTE byt )
{
  BYTE i, b;

  b = byt;

  /* Clock the data out to the B-sensor microcontroller, MSB first */
  switch( StringId )
    {
    case 0:
      for( i=0; i<8; ++i )
	{
	  if( b & 0x80 ) BSMICRO_SET_SDI_0();
	  else BSMICRO_CLEAR_SDI_0();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_SET_SCLK_0();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_CLEAR_SCLK_0();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  b <<= 1;
	}
      /* Reset SDI to zero when not writing */
      //BSMICRO_CLEAR_SDI_0();
      break;

    case 1:
      for( i=0; i<8; ++i )
	{
	  if( b & 0x80 ) BSMICRO_SET_SDI_1();
	  else BSMICRO_CLEAR_SDI_1();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_SET_SCLK_1();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_CLEAR_SCLK_1();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  b <<= 1;
	}
      break;

    case 2:
      for( i=0; i<8; ++i )
	{
	  if( b & 0x80 ) BSMICRO_SET_SDI_2();
	  else BSMICRO_CLEAR_SDI_2();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_SET_SCLK_2();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_CLEAR_SCLK_2();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  b <<= 1;
	}
      break;

    case 3:
      for( i=0; i<8; ++i )
	{
	  if( b & 0x80 ) BSMICRO_SET_SDI_3();
	  else BSMICRO_CLEAR_SDI_3();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_SET_SCLK_3();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  BSMICRO_CLEAR_SCLK_3();
	  //timer2_delay_mus( BSMICRO_SIGNAL_HOLDTIME );
	  timer2_delay_mus( MicroDelayBsensor );
	  b <<= 1;
	}
      break;

    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */
