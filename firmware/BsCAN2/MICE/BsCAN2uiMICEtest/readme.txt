========================================================================
        Qt Application "BsCAN2uiMICEtest"
========================================================================


This Developer Studio Project file has been created by the QMsDev
plugin and is a basic implementation of a Qt application based on
a Dialog interface.

The project contains the following files in which you should add the
code for your project:

main.cpp
	The starting point for your application. Add startup routines, e.g.
	commandline processing here.

bscan2uimicetestdialog.h
	A basic declaration of your application window. Add declarations of
	methods here.
bscan2uimicetestdialog.cpp
	A basic implementation of the methods declared in
	bscan2uimicetestdialog.h. Add implementation code here.
bscan2uimicetestdialogbase.ui
	A dialog with basic buttons.
	Use the Qt GUI Designer change the layout.

The following files will be generated during the Qt specific build
steps, and you shouldn't modify them:

bscan2uimicetestdialogbase.h
bscan2uimicetestdialogbase.cpp
moc_bscan2uimicetestdialogbase.cpp
moc_bscan2uimicetestdialog.cpp

