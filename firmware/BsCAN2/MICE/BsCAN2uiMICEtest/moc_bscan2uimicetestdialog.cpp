/****************************************************************************
** BsCAN2uiMICEtestDialog meta object code from reading C++ file 'bscan2uimicetestdialog.h'
**
** Created: Wed Jan 16 09:18:52 2008
**      by: The Qt MOC ($Id: //depot/qt/main/src/moc/moc.y#178 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#define Q_MOC_BsCAN2uiMICEtestDialog
#if !defined(Q_MOC_OUTPUT_REVISION)
#define Q_MOC_OUTPUT_REVISION 8
#elif Q_MOC_OUTPUT_REVISION != 8
#error "Moc format conflict - please regenerate all moc files"
#endif

#include "bscan2uimicetestdialog.h"
#include <qmetaobject.h>
#include <qapplication.h>

#if defined(Q_SPARCWORKS_FUNCP_BUG)
#define Q_AMPERSAND
#else
#define Q_AMPERSAND &
#endif


const char *BsCAN2uiMICEtestDialog::className() const
{
    return "BsCAN2uiMICEtestDialog";
}

QMetaObject *BsCAN2uiMICEtestDialog::metaObj = 0;

void BsCAN2uiMICEtestDialog::initMetaObject()
{
    if ( metaObj )
	return;
    if ( strcmp(BsCAN2uiMICEtestDialogBase::className(), "BsCAN2uiMICEtestDialogBase") != 0 )
	badSuperclassWarning("BsCAN2uiMICEtestDialog","BsCAN2uiMICEtestDialogBase");
    (void) staticMetaObject();
}

#ifndef QT_NO_TRANSLATION
QString BsCAN2uiMICEtestDialog::tr(const char* s)
{
    return ((QNonBaseApplication*)qApp)->translate("BsCAN2uiMICEtestDialog",s);
}

#endif // QT_NO_TRANSLATION
QMetaObject* BsCAN2uiMICEtestDialog::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    (void) BsCAN2uiMICEtestDialogBase::staticMetaObject();
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    typedef void(BsCAN2uiMICEtestDialog::*m1_t0)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t1)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t2)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t3)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t4)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t5)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t6)();
    typedef void(BsCAN2uiMICEtestDialog::*m1_t7)(int);
    typedef void(BsCAN2uiMICEtestDialog::*m1_t8)();
    m1_t0 v1_0 = Q_AMPERSAND BsCAN2uiMICEtestDialog::connectOrDisconnect;
    m1_t1 v1_1 = Q_AMPERSAND BsCAN2uiMICEtestDialog::requestData;
    m1_t2 v1_2 = Q_AMPERSAND BsCAN2uiMICEtestDialog::startScan;
    m1_t3 v1_3 = Q_AMPERSAND BsCAN2uiMICEtestDialog::stopScan;
    m1_t4 v1_4 = Q_AMPERSAND BsCAN2uiMICEtestDialog::setInterval;
    m1_t5 v1_5 = Q_AMPERSAND BsCAN2uiMICEtestDialog::socketConnected;
    m1_t6 v1_6 = Q_AMPERSAND BsCAN2uiMICEtestDialog::socketDisconnected;
    m1_t7 v1_7 = Q_AMPERSAND BsCAN2uiMICEtestDialog::socketError;
    m1_t8 v1_8 = Q_AMPERSAND BsCAN2uiMICEtestDialog::socketRead;
    QMetaData *slot_tbl = QMetaObject::new_metadata(9);
    QMetaData::Access *slot_tbl_access = QMetaObject::new_metaaccess(9);
    slot_tbl[0].name = "connectOrDisconnect()";
    slot_tbl[0].ptr = *((QMember*)&v1_0);
    slot_tbl_access[0] = QMetaData::Public;
    slot_tbl[1].name = "requestData()";
    slot_tbl[1].ptr = *((QMember*)&v1_1);
    slot_tbl_access[1] = QMetaData::Public;
    slot_tbl[2].name = "startScan()";
    slot_tbl[2].ptr = *((QMember*)&v1_2);
    slot_tbl_access[2] = QMetaData::Public;
    slot_tbl[3].name = "stopScan()";
    slot_tbl[3].ptr = *((QMember*)&v1_3);
    slot_tbl_access[3] = QMetaData::Public;
    slot_tbl[4].name = "setInterval()";
    slot_tbl[4].ptr = *((QMember*)&v1_4);
    slot_tbl_access[4] = QMetaData::Public;
    slot_tbl[5].name = "socketConnected()";
    slot_tbl[5].ptr = *((QMember*)&v1_5);
    slot_tbl_access[5] = QMetaData::Public;
    slot_tbl[6].name = "socketDisconnected()";
    slot_tbl[6].ptr = *((QMember*)&v1_6);
    slot_tbl_access[6] = QMetaData::Public;
    slot_tbl[7].name = "socketError(int)";
    slot_tbl[7].ptr = *((QMember*)&v1_7);
    slot_tbl_access[7] = QMetaData::Public;
    slot_tbl[8].name = "socketRead()";
    slot_tbl[8].ptr = *((QMember*)&v1_8);
    slot_tbl_access[8] = QMetaData::Public;
    metaObj = QMetaObject::new_metaobject(
	"BsCAN2uiMICEtestDialog", "BsCAN2uiMICEtestDialogBase",
	slot_tbl, 9,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    metaObj->set_slot_access( slot_tbl_access );
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    return metaObj;
}
