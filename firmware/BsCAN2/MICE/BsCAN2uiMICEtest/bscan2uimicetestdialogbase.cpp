/****************************************************************************
** Form implementation generated from reading ui file '.\bscan2uimicetestdialogbase.ui'
**
** Created: Wed Jan 16 09:18:05 2008
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "bscan2uimicetestdialogbase.h"

#include <qgroupbox.h>
#include <qlineedit.h>
#include <qmultilineedit.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/* 
 *  Constructs a BsCAN2uiMICEtestDialogBase which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
BsCAN2uiMICEtestDialogBase::BsCAN2uiMICEtestDialogBase( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "BsCAN2uiMICEtestDialogBase" );
    resize( 525, 550 ); 
    setCaption( tr( "BsCAN2uiMICEtest" ) );
    setSizeGripEnabled( FALSE );

    GroupBoxConnection = new QGroupBox( this, "GroupBoxConnection" );
    GroupBoxConnection->setGeometry( QRect( 12, 10, 503, 58 ) ); 
    GroupBoxConnection->setTitle( tr( "Server Connection" ) );

    PushButtonConnect = new QPushButton( GroupBoxConnection, "PushButtonConnect" );
    PushButtonConnect->setGeometry( QRect( 193, 19, 93, 26 ) ); 
    PushButtonConnect->setText( tr( "Connect" ) );

    LineEditHostName = new QLineEdit( GroupBoxConnection, "LineEditHostName" );
    LineEditHostName->setGeometry( QRect( 16, 20, 150, 22 ) ); 
    LineEditHostName->setText( tr( "localhost" ) );

    buttonOk = new QPushButton( this, "buttonOk" );
    buttonOk->setGeometry( QRect( 431, 512, 80, 26 ) ); 
    buttonOk->setCaption( tr( "" ) );
    buttonOk->setText( tr( "&Quit" ) );
    buttonOk->setAutoDefault( TRUE );
    buttonOk->setDefault( FALSE );

    ViewDiagnostic = new QMultiLineEdit( this, "ViewDiagnostic" );
    ViewDiagnostic->setGeometry( QRect( 13, 139, 503, 365 ) ); 

    PushButtonStopScan = new QPushButton( this, "PushButtonStopScan" );
    PushButtonStopScan->setEnabled( FALSE );
    PushButtonStopScan->setGeometry( QRect( 236, 92, 99, 26 ) ); 
    PushButtonStopScan->setText( tr( "Stop Scan" ) );

    PushButtonStartScan = new QPushButton( this, "PushButtonStartScan" );
    PushButtonStartScan->setEnabled( FALSE );
    PushButtonStartScan->setGeometry( QRect( 126, 92, 99, 26 ) ); 
    PushButtonStartScan->setText( tr( "Start Scan" ) );

    PushButtonRequestData = new QPushButton( this, "PushButtonRequestData" );
    PushButtonRequestData->setEnabled( FALSE );
    PushButtonRequestData->setGeometry( QRect( 16, 92, 99, 26 ) ); 
    PushButtonRequestData->setText( tr( "Request Data" ) );

    SpinBoxInterval = new QSpinBox( this, "SpinBoxInterval" );
    SpinBoxInterval->setGeometry( QRect( 459, 94, 54, 21 ) ); 
    SpinBoxInterval->setMaxValue( 255 );
    SpinBoxInterval->setMinValue( 1 );
    SpinBoxInterval->setValue( 1 );

    PushButtonSetInterval = new QPushButton( this, "PushButtonSetInterval" );
    PushButtonSetInterval->setEnabled( FALSE );
    PushButtonSetInterval->setGeometry( QRect( 348, 92, 99, 26 ) ); 
    PushButtonSetInterval->setText( tr( "Set Scan Interval" ) );

    // signals and slots connections
    connect( buttonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
BsCAN2uiMICEtestDialogBase::~BsCAN2uiMICEtestDialogBase()
{
    // no need to delete child widgets, Qt does it all for us
}

