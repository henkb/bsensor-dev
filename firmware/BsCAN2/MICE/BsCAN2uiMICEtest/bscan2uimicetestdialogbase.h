/****************************************************************************
** Form interface generated from reading ui file '.\bscan2uimicetestdialogbase.ui'
**
** Created: Wed Jan 16 09:18:05 2008
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef BSCAN2UIMICETESTDIALOGBASE_H
#define BSCAN2UIMICETESTDIALOGBASE_H

#include <qvariant.h>
#include <qdialog.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QGroupBox;
class QLineEdit;
class QMultiLineEdit;
class QPushButton;
class QSpinBox;

class BsCAN2uiMICEtestDialogBase : public QDialog
{ 
    Q_OBJECT

public:
    BsCAN2uiMICEtestDialogBase( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~BsCAN2uiMICEtestDialogBase();

    QGroupBox* GroupBoxConnection;
    QPushButton* PushButtonConnect;
    QLineEdit* LineEditHostName;
    QPushButton* buttonOk;
    QMultiLineEdit* ViewDiagnostic;
    QPushButton* PushButtonStopScan;
    QPushButton* PushButtonStartScan;
    QPushButton* PushButtonRequestData;
    QSpinBox* SpinBoxInterval;
    QPushButton* PushButtonSetInterval;

};

#endif // BSCAN2UIMICETESTDIALOGBASE_H
