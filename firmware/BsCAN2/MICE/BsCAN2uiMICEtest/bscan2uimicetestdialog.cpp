#include <qdatetime.h>
#include <qlineedit.h>
#include <qmultilineedit.h>
#include <qpushbutton.h>
#include <qspinbox.h>

#include "bscan2uimicetestdialog.h"

/* ------------------------------------------------------------------------ */

BsCAN2uiMICEtestDialog::BsCAN2uiMICEtestDialog( QWidget*    parent,
						const char* name,
						bool        modal,
						WFlags      f )
  : BsCAN2uiMICEtestDialogBase( parent, name, modal, f )
{
  this->ViewDiagnostic->setReadOnly( true );

  // Connect buttons
  connect( this->PushButtonConnect, SIGNAL( clicked() ),
	   this, SLOT( connectOrDisconnect() ) );
  connect( this->PushButtonRequestData, SIGNAL( clicked() ),
	   this, SLOT( requestData() ) );
  connect( this->PushButtonSetInterval, SIGNAL( clicked() ),
	   this, SLOT( setInterval() ) );
  connect( this->PushButtonStartScan, SIGNAL( clicked() ),
	   this, SLOT( startScan() ) );
  connect( this->PushButtonStopScan, SIGNAL( clicked() ),
	   this, SLOT( stopScan() ) );

  // Connect the socket
  connect( &_qsock, SIGNAL( connected() ), this, SLOT( socketConnected() ) );
  connect( &_qsock, SIGNAL( connectionClosed() ),
	   this, SLOT( socketDisconnected() ) );
  connect( &_qsock, SIGNAL( error(int) ), this, SLOT( socketError(int) ) );
  connect( &_qsock, SIGNAL( readyRead() ), this, SLOT( socketRead() ) );
}

/* ------------------------------------------------------------------------ */

BsCAN2uiMICEtestDialog::~BsCAN2uiMICEtestDialog()
{
  _qsock.close();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::requestData()
{
  _qsock.putch( 0 );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::startScan()
{
  _qsock.putch( 253 );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::stopScan()
{
  _qsock.putch( 254 );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::setInterval()
{
  _qsock.putch( SpinBoxInterval->value() );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::connectOrDisconnect()
{
  if( this->PushButtonConnect->text() == QString("Connect") )
    {
      // Connect
      if( !this->LineEditHostName->text().isEmpty() )
	{
	  _qsock.connectToHost( this->LineEditHostName->text(),
				(Q_UINT16) 54321 );
	  this->PushButtonConnect->setText( "Disconnect" );
	  this->LineEditHostName->setEnabled( false );
	}
    }
  else
    {
      // Disconnect
      _qsock.close();
      this->PushButtonConnect->setText( "Connect" );
      this->LineEditHostName->setEnabled( true );
      this->socketDisconnected();
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::socketConnected()
{
  this->PushButtonRequestData->setEnabled( true );
  this->PushButtonStartScan->setEnabled( true );
  this->PushButtonStopScan->setEnabled( true );
  this->PushButtonSetInterval->setEnabled( true );

  this->addDiagnostic( "Connected to server" );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::socketDisconnected()
{
  this->PushButtonRequestData->setEnabled( false );
  this->PushButtonStartScan->setEnabled( false );
  this->PushButtonStopScan->setEnabled( false );
  this->PushButtonSetInterval->setEnabled( false );

  _qsock.close();
  this->PushButtonConnect->setText( "Connect" );
  this->LineEditHostName->setEnabled( true );

  this->addDiagnostic( "Disconnected from server" );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::socketError( int err_id )
{
  QString qs;
  qs.append( "###Error connecting to server, error=" );
  qs.append( QString::number(err_id) );
  qs.append( " (" );
  switch( err_id )
    {
    case QSocket::ErrConnectionRefused:
      qs.append( "ErrConnectionRefused" );
      break;
    case QSocket::ErrHostNotFound:
      qs.append( "ErrHostNotFound" );
      break;
    case QSocket::ErrSocketRead:
      qs.append( "ErrSocketRead" );
      break;
    default:
      qs.append( "???" );
      break;
    }
  qs.append( ")" );
  this->addDiagnostic( qs );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::socketRead()
{
  char block[512];
  QString qs;

  if( _qsock.bytesAvailable() )
    {
      unsigned int val;
      int bytecnt = _qsock.readBlock( block, 512 );
      if( bytecnt > 0 )
	{
	  qs.append( "Received bytecnt=" );
	  qs.append( QString::number(bytecnt) );
	  this->addDiagnostic( qs );
	  qs.truncate( 0 );

	  for( int i=0; i<bytecnt; ++i )
	    {
	      val = (unsigned int) block[i];
	      if( val < 0x10 ) qs.append( "0" );
	      qs.append( QString::number(val,16) );
	      qs.append( "," );
	    }
	  this->addDiagnostic( qs );
	}
      else
	{
	  qs.append( "###Error: bytecnt=" );
	  qs.append( QString::number(bytecnt) );
	  this->addDiagnostic( qs );
	}
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::addDiagnostic( QString &qs )
{
  QString p;
  p.append( QTime::currentTime().toString() );
  p.append( QString("  ") );
  p.append( qs );
  this->ViewDiagnostic->append( p );
  int nlines = this->ViewDiagnostic->numLines();
  this->ViewDiagnostic->setCursorPosition( nlines, 0 );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiMICEtestDialog::addDiagnostic( char *str )
{
  this->addDiagnostic( QString(str) );
}

/* ------------------------------------------------------------------------ */
