#include "bscan2uimicetestdialogbase.h"

#include <qsocket.h>

class BsCAN2uiMICEtestDialog : public BsCAN2uiMICEtestDialogBase
{
  Q_OBJECT
    public:
  BsCAN2uiMICEtestDialog( QWidget* parent = 0,
			  const char* name = 0,
			  bool modal = FALSE,
			  WFlags f = 0 );
  ~BsCAN2uiMICEtestDialog();

  public slots:
    void connectOrDisconnect();
  void requestData();
  void startScan();
  void stopScan();
  void setInterval();
  void socketConnected();
  void socketDisconnected();
  void socketError( int err_id );
  void socketRead();

 private:
  void addDiagnostic( QString &qs );
  void addDiagnostic( char *str );

  QSocket _qsock;
};

