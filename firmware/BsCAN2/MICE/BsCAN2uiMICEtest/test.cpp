#include <windows.h>

#include <iostream>
#include <iomanip>

#include <qapplication.h>
#include <qsocket.h>
#include <qstring.h>

void showState( int state );

//-----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  std::cout << "BsCAN2uiMICE Test of Server" << std::endl;

  // Apparently this is necessary here, although not used by me...
  QApplication app( argc, argv );

  QSocket qs;
  qs.connectToHost( "localhost", (Q_UINT16) 54321 );

  while( qs.state() != QSocket::Connection )
    {
      qApp->processEvents();
      Sleep( 1000 );
      showState( qs.state() );
    }
  showState( qs.state() );

  if( qs.state() == QSocket::Connection )
    {
      qs.putch( 3 );
      qApp->processEvents();

      int loopcnt = 0;
      int bytecnt;
      while( loopcnt < 45 && qs.state() == QSocket::Connection )
	{
	  qApp->processEvents();

	  char block[512];
	  //if( qs.canReadLine() )
	  if( qs.bytesAvailable() )
	    {
	      bytecnt = qs.readBlock( block, 512 );
	      if( bytecnt > 0 )
		{
		  std::cout << "Received bytecnt=" << bytecnt << std::endl;
		  for( int i=0; i<bytecnt; ++i )
		    {
		      //std::cout << block[i];
		      std::cout << std::hex << (unsigned int) block[i] << ',';
		    }
		  std::cout << std::endl;
		}
	      else
		{
		  std::cout << "###Error: bytecnt=" << bytecnt << std::endl;
		}
	      qApp->processEvents();
	    }
	  else
	    {
	      std::cout << "waiting..." << std::endl;
	    }

	  Sleep( 1000 );
	  ++loopcnt;
	  if( (loopcnt % 6) == 0 ) qs.putch( 0 );
	}
    }

  qApp->processEvents();
  showState( qs.state() );

  qs.close();

  Sleep( 2000 );

  return 0;
}

//-----------------------------------------------------------------------------

void showState( int state )
{
  std::cout << "Socket State: ";
  switch( state )
    {
    case QSocket::Idle:
      std::cout << "idle";
      break;
    case QSocket::HostLookup:
      std::cout << "hostlookup";
      break;
    case QSocket::Connecting:
      std::cout << "connecting";
      break;
    case QSocket::Connection:
      std::cout << "connection";
      break;
    case QSocket::Closing:
      std::cout << "closing";
      break;
    default:
      std::cout << "???";
      break;
    }
  std::cout << std::endl;
}

//-----------------------------------------------------------------------------
