/****************************************************************************
** BsCAN2uiMICEtestDialogBase meta object code from reading C++ file 'bscan2uimicetestdialogbase.h'
**
** Created: Wed Jan 16 09:18:05 2008
**      by: The Qt MOC ($Id: //depot/qt/main/src/moc/moc.y#178 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#define Q_MOC_BsCAN2uiMICEtestDialogBase
#if !defined(Q_MOC_OUTPUT_REVISION)
#define Q_MOC_OUTPUT_REVISION 8
#elif Q_MOC_OUTPUT_REVISION != 8
#error "Moc format conflict - please regenerate all moc files"
#endif

#include "bscan2uimicetestdialogbase.h"
#include <qmetaobject.h>
#include <qapplication.h>

#if defined(Q_SPARCWORKS_FUNCP_BUG)
#define Q_AMPERSAND
#else
#define Q_AMPERSAND &
#endif


const char *BsCAN2uiMICEtestDialogBase::className() const
{
    return "BsCAN2uiMICEtestDialogBase";
}

QMetaObject *BsCAN2uiMICEtestDialogBase::metaObj = 0;

void BsCAN2uiMICEtestDialogBase::initMetaObject()
{
    if ( metaObj )
	return;
    if ( strcmp(QDialog::className(), "QDialog") != 0 )
	badSuperclassWarning("BsCAN2uiMICEtestDialogBase","QDialog");
    (void) staticMetaObject();
}

#ifndef QT_NO_TRANSLATION
QString BsCAN2uiMICEtestDialogBase::tr(const char* s)
{
    return ((QNonBaseApplication*)qApp)->translate("BsCAN2uiMICEtestDialogBase",s);
}

#endif // QT_NO_TRANSLATION
QMetaObject* BsCAN2uiMICEtestDialogBase::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    (void) QDialog::staticMetaObject();
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    QMetaData::Access *slot_tbl_access = 0;
    metaObj = QMetaObject::new_metaobject(
	"BsCAN2uiMICEtestDialogBase", "QDialog",
	0, 0,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    metaObj->set_slot_access( slot_tbl_access );
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    return metaObj;
}
