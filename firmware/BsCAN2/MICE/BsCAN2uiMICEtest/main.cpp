#include <qapplication.h>
#include "bscan2uimicetestdialog.h"


int main( int argc, char** argv )
{
	QApplication app( argc, argv );

	BsCAN2uiMICEtestDialog dialog( 0, 0, TRUE );
	app.setMainWidget(&dialog);

	dialog.exec();

	return 0;
}

