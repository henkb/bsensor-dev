The software projects in these directories is old, made with Qt v2.3.0 and
need substantial re-writing with an up-to-date version of Qt,
including the UI.

(Note that a system with MDM plus MICE B-sensor modules can be read out using
BsCAN3 software and tools (which have been made with a modern version of Qt),
provided the MDM interfacing to the B-sensors is programmed
with the BsCAN v3.0.0 firmware).
