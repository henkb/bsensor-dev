#include <qlineedit.h>

#include "MySpinBox.h"

/* ------------------------------------------------------------------------ */

MySpinBox::MySpinBox( QSpinBox &sb )
  : QSpinBox( sb.parentWidget() )
{
  // Copy the settings
  this->setGeometry( sb.geometry() ); 
  this->setFocusPolicy( sb.focusPolicy() );
  this->setButtonSymbols( sb.buttonSymbols() );
  this->setMaxValue( sb.maxValue() );
  this->setMinValue( sb.minValue() );
  this->setValue( sb.value() );

  // Now make sure that all edits send a signal ! (QSpinBox doesn't...)
  connect( this->editor(), SIGNAL( textChanged(const QString& ) ),
	   this, SLOT( changed(const QString&) ) );
}

/* ------------------------------------------------------------------------ */

void MySpinBox::changed( const QString &qs )
{
  emit valueChanged( qs );
}

/* ------------------------------------------------------------------------ */
