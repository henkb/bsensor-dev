#include <qserversocket.h>
#include <qsocket.h>
#include <qsocketnotifier.h>
#include <qstring.h>

const int MAX_CONNECTIONS = 3;

/* ------------------------------------------------------------------------ */

class MyServerSocket: public QServerSocket
{
  Q_OBJECT
    public:
  MyServerSocket( int port, QObject *parent=0 );
  ~MyServerSocket();

  void newConnection( int socket );

  void writeBlock( const char *data, int len );

  int numberOfConnections();

  QString connectionInfo( int conn_id );

  private slots:
    void connectionClosed();
    void requestReceived( int socket );

 signals:
  void connected();
  void disconnected();
  void request( int req_id );

 private:
  QSocket         _qs[MAX_CONNECTIONS];
  QSocketNotifier *_qsn[MAX_CONNECTIONS];

  int             _bytesWritten[MAX_CONNECTIONS];

  int             _closeIndex;
};

/* ------------------------------------------------------------------------ */

