#include <vector>

#include <qfile.h>
#include <qfileinfo.h>
#include <qsocket.h>

#include "bscan2uimicedialogbase.h"

class CanInterface;
class ELMB;
class MySpinBox;
class MyServerSocket;

#define NO_OF_STR     4
#define MAX_B_PER_STR 32

class BsCAN2uiDialog : public BsCAN2uiDialogBase
{
  Q_OBJECT
 public:
  BsCAN2uiDialog( QWidget* parent = 0,
		  const char* name = 0,
		  bool modal = FALSE,
		  WFlags f = 0 );

 public slots:
  void setNodeID( int node_id );
  void setNodeID( const QString &qs );
  void setStringID( int string_id );
  void resetNode();
  void readConfig();
  void probeForBsensors();
  void removeBsensor();
  void checkBoxProbeAllChanged();
  void checkBoxGaussChanged();
  void startStopReadout();
  void startStopLogging();
  void startStopServer();
  void showServerInfo();
  void tcpOpened();
  void tcpClosed();
  void tcpRequest( int req_id );
  void quit();
  void getData();
  void getUnexpectedMessages();
  void clearDiagnostic();

 private:

  void addDiagnostic( QString &qs );
  void addDiagnostic( char *str );

  bool readFirmwareVersion();
  bool readBsensorConfig();

  void showConfigView();
  void clearConfigView();

  void startReadout();
  void stopReadout();
  void startLogging();
  void stopLogging();
  void logData();
  void sendData();

  // CAN-bus and CAN-node
  CanInterface             *_canIntf;
  ELMB                     *_canNode;

  // B-sensor configuration
  bool                     _bConfigValid;
  int                      _bTotal;
  int                      _bInString[NO_OF_STR];
  int                      _bAddr[NO_OF_STR][MAX_B_PER_STR];
  int                      _bStat[NO_OF_STR][MAX_B_PER_STR];

  // B-sensor data
  int                      _bVal[NO_OF_STR][MAX_B_PER_STR][4];
  int                      _bValCopy[NO_OF_STR][MAX_B_PER_STR][4];
  int                      _bAddrHi; // Highest B-module address present

  // B-sensor data display
  int                      _currStringID;

  // Data logging
  QFile                    _logFile;
  QFileInfo                _logFileInfo;

  // TCP server
  MyServerSocket           *_serverSocket;

  // Arrays of widgets
  std::vector<QLineEdit *> _aDisplay;
  std::vector<QLineEdit *> _tDisplay;
  std::vector<QLineEdit *> _xDisplay;
  std::vector<QLineEdit *> _yDisplay;
  std::vector<QLineEdit *> _zDisplay;

  // Palettes for okay/error color indications
  QPalette                 _qpOkay, _qpErr;

  MySpinBox                *MySpinBoxNodeID;
  MySpinBox                *MySpinBoxBaddr;
};
