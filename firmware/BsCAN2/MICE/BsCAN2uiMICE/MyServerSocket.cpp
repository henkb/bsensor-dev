#include <windows.h>
#include "MyServerSocket.h"

/* ------------------------------------------------------------------------ */

MyServerSocket::MyServerSocket( int port, QObject *parent )
  //: QServerSocket( QHostAddress(0x7F000001), // == "localhost"
  //	   (Q_UINT16) port, MAX_CONNECTIONS, parent, "MYSERVER" )
  : QServerSocket( (Q_UINT16) port, MAX_CONNECTIONS, parent, "MYSERVER" )
{
  for( int i=0; i<MAX_CONNECTIONS; ++i )
    {
      //connect( &_qs[i], SIGNAL( connectionClosed() ),
      //       this, SLOT( connectionClosed() ) );

      _qsn[i] = 0; // Wait for a connection before creating the notifier
    }
  _closeIndex = 0;
}

/* ------------------------------------------------------------------------ */

MyServerSocket::~MyServerSocket()
{
  int i;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    if( _qs[i].state() != QSocket::Idle ) _qs[i].close();
}

/* ------------------------------------------------------------------------ */

void MyServerSocket::newConnection( int socket )
{
  // Check available sockets
  int i, no_of_connections;
  no_of_connections = 0;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    {
      if( _qs[i].state() != QSocket::Idle )
	{
	  ++no_of_connections;
	}
      else
	{
	  // Clean up notifier
	  if( _qsn[i] )
	    {
	      _qsn[i]->disconnect();
	      delete _qsn[i];
	      _qsn[i] = 0;
	    }
	  _qs[i].close();
	}
    }

  // Accept up to MAX_CONNECTIONS connections
  if( no_of_connections == MAX_CONNECTIONS )
    {
      // Free up one connection

      // Clean up notifier
      if( _qsn[_closeIndex] )
	{
	  _qsn[_closeIndex]->disconnect();
	  delete _qsn[_closeIndex];
	  _qsn[_closeIndex] = 0;
	}

      _qs[_closeIndex].close();
      _closeIndex = (_closeIndex + 1) % MAX_CONNECTIONS;
      Sleep( 2000 );
    }

  // Find an available socket
  bool socket_available = false;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    {
      if( _qs[i].state() == QSocket::Idle )
	{
	  _qs[i].setSocket( socket );

	  connect( &_qs[i], SIGNAL( connectionClosed() ),
		   this, SLOT( connectionClosed() ) );

	  _qsn[i] = new QSocketNotifier( socket,
					 QSocketNotifier::Read, this );
	  connect( _qsn[i], SIGNAL( activated(int) ),
		   this, SLOT( requestReceived(int) ) );

	  _bytesWritten[i] = 0;

	  socket_available = true;

	  emit connected();
	  break;
	}
    }
  if( !socket_available )
    {
      QSocket qsock;
      qsock.setSocket( socket );
      qsock.close();
    }
}

/* ------------------------------------------------------------------------ */

void MyServerSocket::connectionClosed()
{
  emit disconnected();
}

/* ------------------------------------------------------------------------ */

void MyServerSocket::requestReceived( int socket )
{
  // Get one request identifier byte and get rid of any other input...
  int req_id = -1;
  for( int i=0; i<MAX_CONNECTIONS; ++i )
    if( _qs[i].state() == QSocket::Connection && _qs[i].socket() == socket )
      {
	int bytes = _qs[i].bytesAvailable();
	if( bytes > 0 )
	  {
	    req_id = _qs[i].getch();
	    for( int j=1; j<bytes; ++j ) _qs[i].getch();
	  }
	break;
      }
  if( req_id != -1 ) emit request( req_id );
}

/* ------------------------------------------------------------------------ */

void MyServerSocket::writeBlock( const char *data, int len )
{
  int i, bytes;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    if( _qs[i].state() == QSocket::Connection )
      {
	bytes = _qs[i].writeBlock( data, len );
	if( bytes > 0 ) _bytesWritten[i] += bytes;
      }
}

/* ------------------------------------------------------------------------ */

int MyServerSocket::numberOfConnections()
{
  // Check state of sockets
  int i, no_of_connections;
  no_of_connections = 0;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    if( _qs[i].state() != QSocket::Idle ) ++no_of_connections;

  return no_of_connections;
}

/* ------------------------------------------------------------------------ */

QString MyServerSocket::connectionInfo( int conn_id )
{
  {
    //QString qs;
    //qs.append( "this is a test of the server\n" );
    //this->writeBlock( qs.latin1(), qs.length() );
  }
  // Find the connected socket
  QString str;
  int i;
  int n = -1;
  for( i=0; i<MAX_CONNECTIONS; ++i )
    {
      if( _qs[i].state() != QSocket::Idle )
	{
	  ++n;
	  if( n == conn_id )
	    {
	      str.append( "No." );
	      str.append( QString::number(conn_id+1) );
	      str.append( ": addr=" );
	      str.append( _qs[i].peerAddress().toString() );
	      QString peer_name = _qs[i].peerName();
	      if( peer_name.length() != 0 )
		{
		  str.append( " (" );
		  str.append( peer_name );
		  str.append( ")" );
		}
	      str.append( ", port=" );
	      str.append( QString::number(_qs[i].port()) );
	      str.append( "," );
	      str.append( QString::number(_qs[i].peerPort()) );
	      str.append( ", state=" );
	      switch( _qs[i].state() )
		{
		case QSocket::Idle:
		  str.append( "idle" );
		  break;
		case QSocket::HostLookup:
		  str.append( "hostlookup" );
		  break;
		case QSocket::Connecting:
		  str.append( "connecting" );
		  break;
		case QSocket::Connection:
		  str.append( "connection" );
		  break;
		case QSocket::Closing:
		  str.append( "closing" );
		  break;
		default:
		  str.append( "???" );
		  break;
		}
	      str.append( ", bytes=" );
	      str.append( QString::number(_bytesWritten[i]) );
	      break;
	    }
	}
    }
  return str;
}

/* ------------------------------------------------------------------------ */
