#include <qspinbox.h>

/* ------------------------------------------------------------------------ */

class MySpinBox: public QSpinBox
{
  Q_OBJECT
    public:
  MySpinBox( QSpinBox &sb );
  MySpinBox( QWidget *p ):QSpinBox(p){};
  MySpinBox():QSpinBox(){};

 public slots:
  void changed( const QString &qs );
};

/* ------------------------------------------------------------------------ */

