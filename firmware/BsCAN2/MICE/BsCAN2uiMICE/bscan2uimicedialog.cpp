#include <qapplication.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdatetime.h>
#include <qfiledialog.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qmessagebox.h>
#include <qmultilineedit.h>
#include <qpalette.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qstring.h>
#include <qtabwidget.h>
#include <qtextstream.h>
#include <qtimer.h>

#include "MySpinBox.h"
#include "MyServerSocket.h"

#ifdef __KVASER_INTERFACE__
#include "KVASERintf\KvaserInterface.h"
#endif
#ifdef __NICAN_INTERFACE__
#include "NiCanIntf\NiCanInterface.h"
#endif
#include "ELMB\ELMB.h"
#include "BscanObjectDictionary.h"
#include "CAN\CANopen.h"

#include "bscan2uimicedialog.h"

#define VERSION_STR      "version 1.3.0"
#define DEFAULT_NODEID   24

/* ------------------------------------------------------------------------ */

BsCAN2uiDialog::BsCAN2uiDialog( QWidget* parent,
				const char* name,
				bool modal,
				WFlags f )
  : BsCAN2uiDialogBase( parent, name, modal, f )
{
  this->TextLabelVersion->setText( QString(VERSION_STR) );
  this->SpinBoxNodeID->setValue( DEFAULT_NODEID );

  this->ViewDiagnostic->setReadOnly( true );

  // Instantiate the CAN interface
#ifdef __KVASER_INTERFACE__
  //this->TextLabelCanInterface->setText( "(for KVASER)" );
  _canIntf = new KvaserInterface( 0 );
#endif
#ifdef __NICAN_INTERFACE__
  //this->TextLabelCanInterface->setText( "(for NICAN)" );
  _canIntf = new NiCanInterface();
#endif
  _canIntf->setSkipMessages( false );

  // Instantiate the CAN node
  if( _canIntf )
    {
      _canNode  = new ELMB( _canIntf,
			    this->SpinBoxNodeID->value() );
    }

  QString init_result = QString( _canIntf->initResultString().c_str() );
  this->addDiagnostic( init_result );

  // Replace the ELMB Node-ID spinbox (because QSpinBox doesn't work properly)
  this->MySpinBoxNodeID = new MySpinBox( *this->SpinBoxNodeID );
  this->SpinBoxNodeID->hide();

  // Replace the B-sensor address spinbox
  // (because QSpinBox doesn't work properly)
  this->MySpinBoxBaddr = new MySpinBox( *this->SpinBoxBaddr );
  this->MySpinBoxBaddr->setEnabled( false );
  this->SpinBoxBaddr->hide();

  // Connect spinboxes
  //connect( this->MySpinBoxNodeID, SIGNAL( valueChanged(int) ),
  //   this, SLOT( setNodeID(int) ) );
  connect( this->MySpinBoxNodeID, SIGNAL( valueChanged(const QString&) ),
	   this, SLOT( setNodeID(const QString&) ) );
  connect( this->SpinBoxStringID, SIGNAL( valueChanged(int) ),
	   this, SLOT( setStringID(int) ) );

  // Connect check boxes
  connect( this->CheckBoxProbeAll, SIGNAL( clicked() ),
	   this, SLOT( checkBoxProbeAllChanged() ) );
  connect( this->CheckBoxGauss, SIGNAL( clicked() ),
	   this, SLOT( checkBoxGaussChanged() ) );

  // Connect buttons
  connect( this->PushButtonReset, SIGNAL( clicked() ),
	   this, SLOT( resetNode() ) );
  connect( this->PushButtonReadConfig, SIGNAL( clicked() ),
	   this, SLOT( readConfig() ) );
  connect( this->PushButtonProbe, SIGNAL( clicked() ),
	   this, SLOT( probeForBsensors() ) );
  connect( this->PushButtonRemove, SIGNAL( clicked() ),
	   this, SLOT( removeBsensor() ) );
  connect( this->PushButtonStartStopReadout, SIGNAL( clicked() ),
	   this, SLOT( startStopReadout() ) );
  connect( this->PushButtonStartStopLog, SIGNAL( clicked() ),
	   this, SLOT( startStopLogging() ) );
  connect( this->PushButtonStartStopServer, SIGNAL( clicked() ),
	   this, SLOT( startStopServer() ) );
  connect( this->PushButtonServerInfo, SIGNAL( clicked() ),
	   this, SLOT( showServerInfo() ) );
  connect( this->PushButtonExit, SIGNAL( clicked() ),
	   this, SLOT( quit() ) );
  connect( this->PushButtonClearDiagnostic, SIGNAL( clicked() ),
	   this, SLOT( clearDiagnostic() ) );

  // Connect combo box
  connect( this->ComboBoxFullScale, SIGNAL( activated(int) ),
	   this, SLOT( checkBoxGaussChanged() ) );

  _bConfigValid = false;
  _currStringID = 0;

  // Populate the widget vectors

  _aDisplay.push_back( LineEditAddr1 );
  _aDisplay.push_back( LineEditAddr2 );
  _aDisplay.push_back( LineEditAddr3 );
  _aDisplay.push_back( LineEditAddr4 );
  _aDisplay.push_back( LineEditAddr5 );
  _aDisplay.push_back( LineEditAddr6 );
  _aDisplay.push_back( LineEditAddr7 );
  _aDisplay.push_back( LineEditAddr8 );
  _aDisplay.push_back( LineEditAddr9 );
  _aDisplay.push_back( LineEditAddr10 );
  _aDisplay.push_back( LineEditAddr11 );
  _aDisplay.push_back( LineEditAddr12 );
  _aDisplay.push_back( LineEditAddr13 );
  _aDisplay.push_back( LineEditAddr14 );
  _aDisplay.push_back( LineEditAddr15 );
  _aDisplay.push_back( LineEditAddr16 );

  _aDisplay.push_back( LineEditAddr1_2 );
  _aDisplay.push_back( LineEditAddr2_2 );
  _aDisplay.push_back( LineEditAddr3_2 );
  _aDisplay.push_back( LineEditAddr4_2 );
  _aDisplay.push_back( LineEditAddr5_2 );
  _aDisplay.push_back( LineEditAddr6_2 );
  _aDisplay.push_back( LineEditAddr7_2 );
  _aDisplay.push_back( LineEditAddr8_2 );
  _aDisplay.push_back( LineEditAddr9_2 );
  _aDisplay.push_back( LineEditAddr10_2 );
  _aDisplay.push_back( LineEditAddr11_2 );
  _aDisplay.push_back( LineEditAddr12_2 );
  _aDisplay.push_back( LineEditAddr13_2 );
  _aDisplay.push_back( LineEditAddr14_2 );
  _aDisplay.push_back( LineEditAddr15_2 );
  _aDisplay.push_back( LineEditAddr16_2 );

  _tDisplay.push_back( LineEditTemp1 );
  _tDisplay.push_back( LineEditTemp2 );
  _tDisplay.push_back( LineEditTemp3 );
  _tDisplay.push_back( LineEditTemp4 );
  _tDisplay.push_back( LineEditTemp5 );
  _tDisplay.push_back( LineEditTemp6 );
  _tDisplay.push_back( LineEditTemp7 );
  _tDisplay.push_back( LineEditTemp8 );
  _tDisplay.push_back( LineEditTemp9 );
  _tDisplay.push_back( LineEditTemp10 );
  _tDisplay.push_back( LineEditTemp11 );
  _tDisplay.push_back( LineEditTemp12 );
  _tDisplay.push_back( LineEditTemp13 );
  _tDisplay.push_back( LineEditTemp14 );
  _tDisplay.push_back( LineEditTemp15 );
  _tDisplay.push_back( LineEditTemp16 );

  _tDisplay.push_back( LineEditTemp1_2 );
  _tDisplay.push_back( LineEditTemp2_2 );
  _tDisplay.push_back( LineEditTemp3_2 );
  _tDisplay.push_back( LineEditTemp4_2 );
  _tDisplay.push_back( LineEditTemp5_2 );
  _tDisplay.push_back( LineEditTemp6_2 );
  _tDisplay.push_back( LineEditTemp7_2 );
  _tDisplay.push_back( LineEditTemp8_2 );
  _tDisplay.push_back( LineEditTemp9_2 );
  _tDisplay.push_back( LineEditTemp10_2 );
  _tDisplay.push_back( LineEditTemp11_2 );
  _tDisplay.push_back( LineEditTemp12_2);
  _tDisplay.push_back( LineEditTemp13_2 );
  _tDisplay.push_back( LineEditTemp14_2 );
  _tDisplay.push_back( LineEditTemp15_2 );
  _tDisplay.push_back( LineEditTemp16_2 );

  _xDisplay.push_back( LineEditX1 );
  _xDisplay.push_back( LineEditX2 );
  _xDisplay.push_back( LineEditX3 );
  _xDisplay.push_back( LineEditX4 );
  _xDisplay.push_back( LineEditX5 );
  _xDisplay.push_back( LineEditX6 );
  _xDisplay.push_back( LineEditX7 );
  _xDisplay.push_back( LineEditX8 );
  _xDisplay.push_back( LineEditX9 );
  _xDisplay.push_back( LineEditX10 );
  _xDisplay.push_back( LineEditX11 );
  _xDisplay.push_back( LineEditX12 );
  _xDisplay.push_back( LineEditX13 );
  _xDisplay.push_back( LineEditX14 );
  _xDisplay.push_back( LineEditX15 );
  _xDisplay.push_back( LineEditX16 );

  _yDisplay.push_back( LineEditY1 );
  _yDisplay.push_back( LineEditY2 );
  _yDisplay.push_back( LineEditY3 );
  _yDisplay.push_back( LineEditY4 );
  _yDisplay.push_back( LineEditY5 );
  _yDisplay.push_back( LineEditY6 );
  _yDisplay.push_back( LineEditY7 );
  _yDisplay.push_back( LineEditY8 );
  _yDisplay.push_back( LineEditY9 );
  _yDisplay.push_back( LineEditY10 );
  _yDisplay.push_back( LineEditY11 );
  _yDisplay.push_back( LineEditY12 );
  _yDisplay.push_back( LineEditY13 );
  _yDisplay.push_back( LineEditY14 );
  _yDisplay.push_back( LineEditY15 );
  _yDisplay.push_back( LineEditY16 );

  _zDisplay.push_back( LineEditZ1 );
  _zDisplay.push_back( LineEditZ2 );
  _zDisplay.push_back( LineEditZ3 );
  _zDisplay.push_back( LineEditZ4 );
  _zDisplay.push_back( LineEditZ5 );
  _zDisplay.push_back( LineEditZ6 );
  _zDisplay.push_back( LineEditZ7 );
  _zDisplay.push_back( LineEditZ8 );
  _zDisplay.push_back( LineEditZ9 );
  _zDisplay.push_back( LineEditZ10 );
  _zDisplay.push_back( LineEditZ11 );
  _zDisplay.push_back( LineEditZ12 );
  _zDisplay.push_back( LineEditZ13 );
  _zDisplay.push_back( LineEditZ14 );
  _zDisplay.push_back( LineEditZ15 );
  _zDisplay.push_back( LineEditZ16 );

  _xDisplay.push_back( LineEditX1_2 );
  _xDisplay.push_back( LineEditX2_2 );
  _xDisplay.push_back( LineEditX3_2 );
  _xDisplay.push_back( LineEditX4_2 );
  _xDisplay.push_back( LineEditX5_2 );
  _xDisplay.push_back( LineEditX6_2 );
  _xDisplay.push_back( LineEditX7_2 );
  _xDisplay.push_back( LineEditX8_2 );
  _xDisplay.push_back( LineEditX9_2 );
  _xDisplay.push_back( LineEditX10_2 );
  _xDisplay.push_back( LineEditX11_2 );
  _xDisplay.push_back( LineEditX12_2 );
  _xDisplay.push_back( LineEditX13_2 );
  _xDisplay.push_back( LineEditX14_2 );
  _xDisplay.push_back( LineEditX15_2 );
  _xDisplay.push_back( LineEditX16_2 );

  _yDisplay.push_back( LineEditY1_2 );
  _yDisplay.push_back( LineEditY2_2 );
  _yDisplay.push_back( LineEditY3_2 );
  _yDisplay.push_back( LineEditY4_2 );
  _yDisplay.push_back( LineEditY5_2 );
  _yDisplay.push_back( LineEditY6_2 );
  _yDisplay.push_back( LineEditY7_2 );
  _yDisplay.push_back( LineEditY8_2 );
  _yDisplay.push_back( LineEditY9_2 );
  _yDisplay.push_back( LineEditY10_2 );
  _yDisplay.push_back( LineEditY11_2 );
  _yDisplay.push_back( LineEditY12_2 );
  _yDisplay.push_back( LineEditY13_2 );
  _yDisplay.push_back( LineEditY14_2 );
  _yDisplay.push_back( LineEditY15_2 );
  _yDisplay.push_back( LineEditY16_2 );

  _zDisplay.push_back( LineEditZ1_2 );
  _zDisplay.push_back( LineEditZ2_2 );
  _zDisplay.push_back( LineEditZ3_2 );
  _zDisplay.push_back( LineEditZ4_2 );
  _zDisplay.push_back( LineEditZ5_2 );
  _zDisplay.push_back( LineEditZ6_2 );
  _zDisplay.push_back( LineEditZ7_2 );
  _zDisplay.push_back( LineEditZ8_2 );
  _zDisplay.push_back( LineEditZ9_2 );
  _zDisplay.push_back( LineEditZ10_2 );
  _zDisplay.push_back( LineEditZ11_2 );
  _zDisplay.push_back( LineEditZ12_2 );
  _zDisplay.push_back( LineEditZ13_2 );
  _zDisplay.push_back( LineEditZ14_2 );
  _zDisplay.push_back( LineEditZ15_2 );
  _zDisplay.push_back( LineEditZ16_2 );

  // Set certain widget properties
  std::vector<QLineEdit *>::const_iterator it;
  QFont qf( "Arial", 10 );
  for( it=_aDisplay.begin(); it!=_aDisplay.end(); ++it )
    {
      (*it)->setFont( qf );
    }
  for( it=_tDisplay.begin(); it!=_tDisplay.end(); ++it )
    {
      (*it)->setFont( qf );
      (*it)->setAlignment( Qt::AlignRight );
    }
  for( it=_xDisplay.begin(); it!=_xDisplay.end(); ++it )
    {
      (*it)->setFont( qf );
      (*it)->setAlignment( Qt::AlignRight );
    }
  for( it=_yDisplay.begin(); it!=_yDisplay.end(); ++it )
    {
      (*it)->setFont( qf );
      (*it)->setAlignment( Qt::AlignRight );
    }
  for( it=_zDisplay.begin(); it!=_zDisplay.end(); ++it )
    {
      (*it)->setFont( qf );
      (*it)->setAlignment( Qt::AlignRight );
    }
  // Change appearance of some of the text labels
  {
    QPalette qp = this->TextLabelAddr->palette();
    qp.setColor( QColorGroup::Foreground, QColor("blue") );

    this->TextLabelAddr->setPalette( qp );
    this->TextLabelTemp->setPalette( qp );
    this->TextLabelX->setPalette( qp );
    this->TextLabelY->setPalette( qp );
    this->TextLabelZ->setPalette( qp );

    this->TextLabelAddr_2->setPalette( qp );
    this->TextLabelTemp_2->setPalette( qp );
    this->TextLabelX_2->setPalette( qp );
    this->TextLabelY_2->setPalette( qp );
    this->TextLabelZ_2->setPalette( qp );

    this->TextLabelName1->setPalette( qp );
    this->TextLabelName2->setPalette( qp );
    this->TextLabelVersion->setPalette( qp );
    qp.setColor( QColorGroup::Foreground, QColor("purple") );
    //this->TextLabelCanInterface->setPalette( qp );
  }

  // Indicate okay/error status of B-sensor modules by color
  _qpOkay = this->LineEditAddr1->palette();
  _qpErr = _qpOkay;
  _qpErr.setColor( QColorGroup::Base, QColor("yellow") );

  // Populate the 'full-scale' combo box
  this->ComboBoxFullScale->insertItem( "4.5T" );
  this->ComboBoxFullScale->insertItem( "2.5T" );
  this->ComboBoxFullScale->insertItem( "1.4T" );

  this->clearConfigView();

  _serverSocket = new MyServerSocket( this->SpinBoxServerPortNo->value(),
				      this );
  connect( _serverSocket, SIGNAL( connected() ),
	   this, SLOT( tcpOpened() ) );
  connect( _serverSocket, SIGNAL( disconnected() ),
	   this, SLOT( tcpClosed() ) );
  connect( _serverSocket, SIGNAL( request(int) ),
	   this, SLOT( tcpRequest(int) ) );

  QTimer::singleShot( 1500, this, SLOT( getUnexpectedMessages() ) );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::addDiagnostic( QString &qs )
{
  QString p;
  p.append( QTime::currentTime().toString() );
  p.append( QString("  ") );
  p.append( qs );
  this->ViewDiagnostic->append( p );
  if( qs[0] == '#' && _canIntf && _canIntf->opStatus() != 0 )
    {
      // This diagnostic message reports an error;
      // it might be useful to see the CAN-interface status
      QString err;
      err.append( "\tCAN status: " );
      QString n = QString::number( (unsigned int) _canIntf->opStatus(),
				   16 );
      err.append( n.upper() );
      err.append( ", " );
      err.append( QString(_canIntf->errString( "diagnosis" ).c_str()) );
      this->ViewDiagnostic->append( err );
    }
  int nlines = this->ViewDiagnostic->numLines();
  this->ViewDiagnostic->setCursorPosition( nlines, 0 );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::addDiagnostic( char *str )
{
  this->addDiagnostic( QString(str) );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::clearDiagnostic()
{
  this->ViewDiagnostic->clear();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::setNodeID( int node_id )
{
  // NB: now spinbox is disabled while read-out is in progress
  //this->stopReadout();

  this->LineEditFirmwareVersion->clear();

  if( !_canNode ) return;

  _canNode->setNodeID( node_id );

  _bConfigValid = false;

  this->clearConfigView();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::setNodeID( const QString &qs )
{
  int node_id = 0;
  for( int j=0; j<qs.length(); ++j )
    node_id = node_id*10 + (qs[j]-'0');

  this->setNodeID( node_id );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::setStringID( int string_id )
{
  /*
    QString qs;
    qs.append( "String #" );
    qs.append( QString::number(string_id) );

    this->GroupBoxString->setTitle( qs );
  */
  // We now use a tab widget..
  int curr_id = this->TabWidgetBstringData->currentPageIndex();
  QString qs0, qs1;
  qs0.append( "String #" );
  qs0.append( QString::number(string_id) );
  qs0.append( "  1-16" );
  TabWidgetBstringData->setCurrentPage( 0 );
  TabWidgetBstringData->changeTab( TabWidgetBstringData->currentPage(), qs0 );
  qs1.append( "String #" );
  qs1.append( QString::number(string_id) );
  qs1.append( "  17-32" );
  TabWidgetBstringData->setCurrentPage( 1 );
  TabWidgetBstringData->changeTab( TabWidgetBstringData->currentPage(), qs1 );

  this->TabWidgetBstringData->setCurrentPage( curr_id );

  _currStringID = string_id;

  this->clearConfigView();
  this->showConfigView();
}

/* ------------------------------------------------------------------------ */

bool BsCAN2uiDialog::readFirmwareVersion()
{
  if( !_canNode ) return false;

  char    version[32];
  QString qs;
  bool    result;

  if( _canNode->getFirmwareVersion( version ) )
    {
      this->LineEditFirmwareVersion->setText( QString( version ) );

      qs.append( QString("Read ELMB firmware version ") );
      qs.append( this->LineEditFirmwareVersion->text() );
      qs.append( QString(", node ") );

      result = true;
    }
  else
    {
      this->LineEditFirmwareVersion->setText( QString("????") );

      qs.append( QString("###Failed to read ELMB firmware version, node ") );

      result = false;
    }
  qs.append( QString::number( _canNode->nodeID() ) );
  this->addDiagnostic( qs );
  return result;
}

/* ------------------------------------------------------------------------ */

bool BsCAN2uiDialog::readBsensorConfig()
{
  int no_of_bytes, data=0;
  int str_id, i;

  _bConfigValid = false;

  // Initialize values
  _bTotal = 0;
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( i=0; i<MAX_B_PER_STR; ++i )
      _bAddr[str_id][i] = -1;

  // Get the total number of B-sensors in the configuration
  if( _canNode->sdoRead( OD_BSENSOR_LIST_HI, OD_BSENSOR_LIST_LO,
			 0, &no_of_bytes, &data, 100 ) )
    {
      _bTotal = data;
    }
  else
    {
      this->addDiagnostic( "###Failed to read B config (total)" );
      return false;
    }

  // Get the B-sensor addresses and their string number
  for( i=0; i<NO_OF_STR; ++i ) _bInString[i] = 0;
  _bAddrHi = -1;
  for( i=1; i<=_bTotal; ++i )
    {
      // Its address
      if( _canNode->sdoRead( OD_BSENSOR_LIST_HI,
			     OD_BSENSOR_LIST_LO,
			     i, &no_of_bytes, &data, 100 ) )
	{
	  int addr = data;

	  // Remember the highest address in the configuration
	  // (data for this address comes out last)
	  if( addr > _bAddrHi ) _bAddrHi = addr;

	  // Its string ID
	  if( _canNode->sdoRead( OD_BSENSOR_MAP_HI,
				 OD_BSENSOR_MAP_LO,
				 addr, &no_of_bytes, &data, 100 ) )
	    {
	      str_id = data;

	      if( str_id >= 0 && str_id < NO_OF_STR )
		{
		  if( _bInString[str_id] < MAX_B_PER_STR )
		    _bAddr[str_id][_bInString[str_id]] = addr;
		  ++_bInString[str_id];
		}
	    }
	  else
	    {
	      this->addDiagnostic( "###Failed to read B config (str)" );
	      return false;
	    }
	}
      else
	{
	  this->addDiagnostic( "###Failed to read B config (addr)" );
	  return false;
	}
    }

  // Check the number of B-sensors we found on each string with total
  int total = 0;
  for( i=0; i<NO_OF_STR; ++i ) total += _bInString[i];
  if( total != _bTotal )
    {
      this->addDiagnostic( "###Mismatch in total number of modules found " );
      return false;
    }

  // Get the B-sensor statuses
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( i=0; i<_bInString[str_id]; ++i )
      {
	if( i < MAX_B_PER_STR )
	  {
	    if( _canNode->sdoRead( OD_BSENSOR_STATUS_HI,
				   _bAddr[str_id][i],
				   0, &no_of_bytes, &data, 100 ) )
	      {
		_bStat[str_id][i] = data;
	      }
	    else
	      {
		QString qs( "###Failed to read B config (stat)" );
		this->addDiagnostic( qs );
		return false;
	      }
	  }
	else
	  {
	    QString qs( "###Too many B-modules in config: can't read stat" );
	    this->addDiagnostic( qs );
	  }
      }

  this->addDiagnostic( "B-sensor configuration read" );

  _bConfigValid = true;

  return true;
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::showConfigView()
{
  QString qs;

  if( _bConfigValid == false ) return;

  qs = QString::number( _bTotal );
  this->LineEditBtotal->setText( qs );

  int str_id = _currStringID;

  qs = QString::number( _bInString[str_id] );
  this->LineEditBtotalStr->setText( qs );

  for( int i=0; i<_bInString[str_id]; ++i )
    {
      if( i < MAX_B_PER_STR )
	{
	  _aDisplay[i]->setEnabled( true );
	  qs = QString::number( _bAddr[str_id][i] );
	  _aDisplay[i]->setText( qs );

	  _tDisplay[i]->setEnabled( true );
	  _xDisplay[i]->setEnabled( true );
	  _yDisplay[i]->setEnabled( true );
	  _zDisplay[i]->setEnabled( true );

	  if( _bStat[str_id][i] != 0 )
	    {
	      _aDisplay[i]->setPalette( _qpErr );
	      _tDisplay[i]->setPalette( _qpErr );
	      _xDisplay[i]->setPalette( _qpErr );
	      _yDisplay[i]->setPalette( _qpErr );
	      _zDisplay[i]->setPalette( _qpErr );
	    }
	  else
	    {
	      _aDisplay[i]->setPalette( _qpOkay );
	      _tDisplay[i]->setPalette( _qpOkay );
	      _xDisplay[i]->setPalette( _qpOkay );
	      _yDisplay[i]->setPalette( _qpOkay );
	      _zDisplay[i]->setPalette( _qpOkay );
	    }
	}
    }

  this->LineEditBtotal->setPalette( _qpOkay );
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( int i=0; i<_bInString[str_id]; ++i )
      {
	if( i < MAX_B_PER_STR )
	  {
	    if( _bStat[str_id][i] != 0 )
	      this->LineEditBtotal->setPalette( _qpErr );
	  }
	else
	  {
	    // More B-modules in this string than we can handle: show it
	    this->LineEditBtotal->setPalette( _qpErr );
	  }
      }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::clearConfigView()
{
  this->LineEditBtotal->clear();
  this->LineEditBtotalStr->clear();

  std::vector<QLineEdit *>::const_iterator it;

  // Clear and disable widgets
  for( it=_aDisplay.begin(); it!=_aDisplay.end(); ++it )
    {
      (*it)->clear();
      (*it)->setEnabled( false );
    }
  for( it=_tDisplay.begin(); it!=_tDisplay.end(); ++it )
    {
      (*it)->clear();
      (*it)->setEnabled( false );
    }
  for( it=_xDisplay.begin(); it!=_xDisplay.end(); ++it )
    {
      (*it)->clear();
      (*it)->setEnabled( false );
    }
  for( it=_yDisplay.begin(); it!=_yDisplay.end(); ++it )
    {
      (*it)->clear();
      (*it)->setEnabled( false );
    }
  for( it=_zDisplay.begin(); it!=_zDisplay.end(); ++it )
    {
      (*it)->clear();
      (*it)->setEnabled( false );
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::resetNode()
{
  if( !_canNode ) return;

  // NB: now reset button is disabled while read-out is in progress
  //this->stopReadout();

  _canIntf->flush();

  // Reset the CAN node
  if( _canNode->nmt( NMT_RESET_NODE ) == false )
    {
      this->addDiagnostic( "###Failed to send NMT-Reset" );
    }
  else
    {
      QApplication::setOverrideCursor( Qt::waitCursor );

      this->addDiagnostic( "Sent NMT-Reset" );

      // Wait for the node to reboot
      if( _canNode->waitForBootup( 15000 ) == false )
	this->addDiagnostic( "###Failed to receive (proper) BOOTUP message" );
      else
	this->addDiagnostic( "Received BOOTUP message" );

      QApplication::restoreOverrideCursor();
    }
  
  this->readConfig();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::readConfig()
{
  this->clearConfigView();

  if( this->readFirmwareVersion() == false ) return;

  if( this->readBsensorConfig() == false ) return;

  this->showConfigView();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::probeForBsensors()
{
  int no_of_bytes, data=0;
  int probe_obj, subind;
  int timeout_ms;

  // NB: now probe button is disabled while read-out is in progress
  //this->stopReadout();

  this->clearConfigView();

  this->addDiagnostic( "Start probe operation" );

  QApplication::setOverrideCursor( Qt::waitCursor );

  if( this->CheckBoxProbeAll->isChecked() )
    {
      probe_obj  = OD_BSENSOR_PROBE_LO;
      subind     = 0;
      timeout_ms = 12000;
    }
  else
    {
      probe_obj  = OD_BSENSOR_PROBE_ADR_LO;
      subind     = this->MySpinBoxBaddr->value();
      timeout_ms = 2000;
    }

  if( _canNode->sdoRead( OD_BSENSOR_PROBE_HI, probe_obj,
			 subind, &no_of_bytes, &data, timeout_ms ) )
    {
      QApplication::restoreOverrideCursor();
    }
  else
    {
      if( _canNode->sdoResult == 0 )
        this->addDiagnostic( "###Time-out on B-sensor probe op" );
      else
        this->addDiagnostic( "###Error in B-sensor probe op" );
      if( subind != 0 )
	{
	  QString qs;
	  qs.append( "###while probing for B-sensor addr " );
	  qs.append( QString::number(subind) );
	  this->addDiagnostic( qs );
	}
      QApplication::restoreOverrideCursor();
      //return;
    }

  this->addDiagnostic( "Probe operation finished" );

  if( this->readBsensorConfig() == false ) return;

  this->showConfigView();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::removeBsensor()
{
  int no_of_bytes, data=0;
  int subind;

  if( this->CheckBoxProbeAll->isChecked() ) return;

  // NB: now 'All' checkbox is disabled while read-out is in progress
  //this->stopReadout();

  this->clearConfigView();

  this->addDiagnostic( "Start remove single B-sensor operation" );

  QApplication::setOverrideCursor( Qt::waitCursor );

  subind = this->MySpinBoxBaddr->value();

  if( _canNode->sdoRead( OD_BSENSOR_PROBE_HI, OD_BSENSOR_REMOV_ADR_LO,
			 subind, &no_of_bytes, &data, 100 ) )
    {
      QApplication::restoreOverrideCursor();
    }
  else
    {
      if( _canNode->sdoResult == 0 )
        this->addDiagnostic( "###Time-out on B-sensor remove op" );
      else
        this->addDiagnostic( "###Error in B-sensor remove op" );
      if( subind != 0 )
	{
	  QString qs;
	  qs.append( "###while removing B-sensor addr " );
	  qs.append( QString::number(subind) );
	  this->addDiagnostic( qs );
	}
      QApplication::restoreOverrideCursor();
      //return;
    }

  this->addDiagnostic( "Remove operation finished" );

  if( this->readBsensorConfig() == false ) return;

  this->showConfigView();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::checkBoxProbeAllChanged()
{
  if( this->CheckBoxProbeAll->isChecked() )
    {
      this->MySpinBoxBaddr->setEnabled( false );
      this->PushButtonRemove->setEnabled( false );
    }
  else
    {
      this->MySpinBoxBaddr->setEnabled( true );
      this->PushButtonRemove->setEnabled( true );
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::checkBoxGaussChanged()
{
  this->ComboBoxFullScale->setEnabled( this->CheckBoxGauss->isChecked() );

  // Clear Hall sensor entries
  for( int i=0; i<_bInString[_currStringID]; ++i )
    {
      if( i < MAX_B_PER_STR )
	{
	  _xDisplay[i]->clear();
	  _yDisplay[i]->clear();
	  _zDisplay[i]->clear();
	}
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::startStopReadout()
{
  if( this->PushButtonStartStopReadout->isOn() )
    this->startReadout();
  else
    this->stopReadout();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::startReadout()
{
  if( _bConfigValid == false )
    {
      this->readConfig();
    }
  else
    {
      for( int i=0; i<_bInString[_currStringID]; ++i )
	{
	  if( i < MAX_B_PER_STR )
	    {
	      _tDisplay[i]->clear();
	      _xDisplay[i]->clear();
	      _yDisplay[i]->clear();
	      _zDisplay[i]->clear();
	    }
	}
    }

  this->PushButtonStartStopReadout->setText( QString("Stop") );
  this->PushButtonStartStopLog->setEnabled( false );
  this->PushButtonReset->setEnabled( false );
  this->PushButtonProbe->setEnabled( false );
  this->CheckBoxProbeAll->setEnabled( false );
  this->MySpinBoxNodeID->setEnabled( false );
  this->SpinBoxInterval->setEnabled( false );

  // Set PDO Transmission Mode to 255
  int subindex    = 2;
  int no_of_bytes = 1;
  int data        = 0xFF;
  _canNode->sdoWrite( OD_TPDO_PAR_HI,
		      OD_TPDO4_PAR_LO, subindex,
		      no_of_bytes, data, 100 );

  // Set PDO Event Timer to the requested interval value (in seconds)
  subindex    = 5;
  no_of_bytes = 2;
  data        = this->SpinBoxInterval->value();
  _canNode->sdoWrite( OD_TPDO_PAR_HI,
		      OD_TPDO4_PAR_LO, subindex,
		      no_of_bytes, data, 100 );

  _canIntf->setSkipMessages( false );

  this->addDiagnostic( "Start read-out" );

  QTimer::singleShot( 1, this, SLOT( getData() ) );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::stopReadout()
{
  int subindex    = 5;
  int no_of_bytes = 2;
  int data        = 0;
  _canNode->sdoWrite( OD_TPDO_PAR_HI,
		      OD_TPDO4_PAR_LO, subindex,
		      no_of_bytes, data, 50 );

  _canIntf->setSkipMessages( true );

  this->addDiagnostic( "Stop read-out" );

  this->PushButtonStartStopReadout->setText( QString("Start") );
  this->PushButtonStartStopLog->setEnabled( true );
  this->PushButtonReset->setEnabled( true );
  this->PushButtonProbe->setEnabled( true );
  this->CheckBoxProbeAll->setEnabled( true );
  this->MySpinBoxNodeID->setEnabled( true );
  this->SpinBoxInterval->setEnabled( true );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::startStopLogging()
{
  if( this->PushButtonStartStopLog->isOn() )
    this->startLogging();
  else
    this->stopLogging();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::startLogging()
{
  QString filename =
    QFileDialog::getSaveFileName( _logFileInfo.filePath(),
				  "All Files (*.*)",
				  this, "logfiledialog",
				  QString("Select file for data logging") );
  if ( filename.isEmpty() )
    {
      this->PushButtonStartStopLog->setOn( false );
      return;
    }

  _logFile.setName( filename );

  bool file_opened;
  if( _logFile.exists() )
    {
      // Ask: overwrite, append or cancel
      int result;
      result = QMessageBox::warning( this, "Data file",
				     "File exists, what would you like to do?",
				     "Overwrite", "Append", "Cancel", 1 );
      switch( result )
	{
	case 0:
	  file_opened = _logFile.open( IO_WriteOnly ) ;
	  break;
	case 1:
	  file_opened = _logFile.open( IO_WriteOnly | IO_Append ) ;
	  break;
	case 2:
	  this->PushButtonStartStopLog->setOn( false );
	  return;
	}
    }
  else
    {
      file_opened = _logFile.open( IO_WriteOnly ) ;
    }

  if( !file_opened )
    {
      this->PushButtonStartStopLog->setOn( false );
      QMessageBox::critical( this, "Opening data file", "Failed to open file");
      return;
    }

  _logFileInfo.setFile( filename );

  QString qs;
  qs.append( QString("Opened data file: ") );
  qs.append( filename );
  this->addDiagnostic( qs );

  this->LineEditLogFileName->setText( _logFileInfo.fileName() );
  this->LineEditLogFileName->setEnabled( true );
  this->PushButtonStartStopLog->setText( QString("Stop Log") );

  // Reset B-sensor data values
  for( int str_id=0; str_id<NO_OF_STR; ++str_id )
    for( int i=0; i<MAX_B_PER_STR; ++i )
      for( int j=0; j<4; ++j )
	{
	  _bVal[str_id][i][j] = 0x00000000;
	  _bValCopy[str_id][i][j] = 0x00000000;
	}

//#define _LOG_TEST_
#ifdef _LOG_TEST_
 {
   _bTotal = 0;
   for( int str_id=0; str_id<NO_OF_STR; ++str_id )
     for( int i=0; i<MAX_B_PER_STR; ++i )
       _bAddr[str_id][i] = -1;

   // Date
   QDate   d = QDate::currentDate();
   // Time of day (format HH:MM:SS:mmm)
   QTime   t = QTime::currentTime();
   QString line;
   line.append( d.toString() );
   line.append( ' ' );
   line.append( t.toString() );
   line.append( ':' );
   int ms = t.msec();
   if( ms < 100 ) line.append( '0' );
   if( ms < 10 ) line.append( '0' );
   line.append( QString::number(ms) );
   line.append( ' ' );

   // B-sensor address
   line.append( QString("Addr %1 ").arg(QString::number(_bAddr[1][2]),3) );
   // Data
   int *val = _bVal[1][2];
   line.append( QString("%1 ").arg(QString::number(val[0]),8) );
   line.append( QString("%1 ").arg(QString::number(val[1]),8) );
   line.append( QString("%1 ").arg(QString::number(val[2]),8) );
   line.append( QString("%1 ").arg(QString::number(val[3]),8) );
   line.append( '\n' );

   // Write line to file
   QTextStream to( &_logFile );
   to << line;
 }
#endif
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::stopLogging()
{
  // Close log file
  if( _logFile.isOpen() )
    {
      _logFile.flush();
      _logFile.close();

      QString qs;
      qs.append( QString("Closed data file: ") );
      qs.append( _logFile.name() );
      this->addDiagnostic( qs );
    }

  //this->LineEditLogFileName->setText( QString("") );
  this->LineEditLogFileName->setEnabled( false );
  this->PushButtonStartStopLog->setText( QString("Start Log") );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::logData()
{
  if( _logFile.isOpen() == false ) return;

  QString line;
  int     str_id, i;

  // Store all B-sensor data in one ASCII line,
  // and start with the current time-of-day

  // Date (format "Tue Feb 13 2007")
  QDate d = QDate::currentDate();
  line.append( d.toString() );
  line.append( ' ' );

  // Time of day (format HH:MM:SS:mmm)
  QTime t = QTime::currentTime();
  line.append( t.toString() );
  line.append( ':' );
  int ms = t.msec();
  if( ms < 100 ) line.append( '0' );
  if( ms < 10 ) line.append( '0' );
  line.append( QString::number(ms) );
  line.append( ' ' );

  // B-sensor data
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( i=0; i<_bInString[str_id]; ++i )
      {
	// B-sensor address
	line.append( QString(" Addr %1 ").
		     arg(QString::number(_bAddr[str_id][i]),3) );

	// Data
	int *val = _bVal[str_id][i];
	line.append( QString("%1 ").arg(QString::number(val[0]),8) );
	line.append( QString("%1 ").arg(QString::number(val[1]),8) );
	line.append( QString("%1 ").arg(QString::number(val[2]),8) );
	line.append( QString("%1 ").arg(QString::number(val[3]),8) );
      }
  line.append( '\n' );

  // Write line to file
  QTextStream to( &_logFile );
  to << line;

  // ###THIS IS FOR DEBUGGING
  //if( _serverSocket )
  //  _serverSocket->writeBlock( line.latin1(), line.length() );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::sendData()
{
  if( _serverSocket == 0 ) return;
  if( _serverSocket->numberOfConnections == 0 ) return;

  if( !_bConfigValid )
    {
      this->addDiagnostic( "###No valid B-sensor configuration read (yet)" );
      return;
    }

  QByteArray  block;
  QDataStream ds( block, IO_WriteOnly );

  // B-sensor data
  int str_id, i;
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( i=0; i<_bInString[str_id]; ++i )
      {
	// B-sensor address
	ds << _bAddr[str_id][i];

	// Data
	int *val = _bValCopy[str_id][i];
	ds << val[0] << val[1] << val[2] << val[3];
      }

  _serverSocket->writeBlock( block.data(), block.size() );

  // Reset B-sensor data values
  for( str_id=0; str_id<NO_OF_STR; ++str_id )
    for( i=0; i<_bInString[str_id]; ++i )
      for( int j=0; j<4; ++j )
	_bValCopy[str_id][i][j] = 0x00000000;
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::startStopServer()
{
  if( this->PushButtonStartStopServer->isOn() )
    {
      _serverSocket = new MyServerSocket( this->SpinBoxServerPortNo->value(),
					  this );
      connect( _serverSocket, SIGNAL( connected() ),
	       this, SLOT( tcpOpened() ) );
      connect( _serverSocket, SIGNAL( disconnected() ),
	       this, SLOT( tcpClosed() ) );
      connect( _serverSocket, SIGNAL( request(int) ),
	       this, SLOT( tcpRequest(int) ) );
      this->PushButtonStartStopServer->setText( QString("Stop") );
      this->SpinBoxServerPortNo->setEnabled( false );
    }
  else
    {
      if( _serverSocket )
	{
	  delete _serverSocket;
	  _serverSocket = 0;
	}
      this->PushButtonStartStopServer->setText( QString("Start") );
      this->SpinBoxServerPortNo->setEnabled( true );
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::showServerInfo()
{
  if( _serverSocket == 0 )
    {
      this->addDiagnostic( "TCP-server not active" );
      return;
    }
  QString qs;
  //qs.append( "TCP-server: ok = " );
  //if( _serverSocket->ok() )
  //  qs.append( "true" );
  //else
  //  qs.append( "false" );
  //this->addDiagnostic( qs );
  //qs.truncate( 0 );
  //qs.append( "TCP-server: address = " );
  //qs.append( _serverSocket->address().toString() );
  //this->addDiagnostic( qs );
  //qs.truncate( 0 );
  qs.append( "TCP-server: number of connections = " );
  int no_of_connections = _serverSocket->numberOfConnections();
  qs.append( QString::number(no_of_connections) );
  this->addDiagnostic( qs );

  for( int i=0; i<no_of_connections; ++i )
    this->addDiagnostic( _serverSocket->connectionInfo(i) );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::tcpOpened()
{
  this->addDiagnostic( "TCP connection opened" );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::tcpClosed()
{
  this->addDiagnostic( "TCP connection closed" );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::tcpRequest( int req_id )
{
  switch( req_id )
    {
    case 254:
      // Request to stop read-out
      if( this->PushButtonStartStopReadout->isOn() )
	{
	  this->addDiagnostic( "TCP request to stop" );
	  this->PushButtonStartStopReadout->setOn( false );
	  this->stopReadout();
	}
      else
	{
	  this->addDiagnostic( "TCP request to stop ignored" );
	}
      break;

    case 253:
      // Request to start read-out
      if( !this->PushButtonStartStopReadout->isOn() )
	{
	  this->addDiagnostic( "TCP request to start" );
	  this->PushButtonStartStopReadout->setOn( true );
	  this->startReadout();
	}
      else
	{
	  this->addDiagnostic( "TCP request to start ignored" );
	}
      break;

    case 0:
      // Request for data
      if( this->CheckBoxPullMode->isChecked() )
	{
	  this->sendData();
	  this->addDiagnostic( "TCP request for data" );
	}
      else
	{
	  this->addDiagnostic( "TCP request for data ignored" );
	}
      break;

    default:
      {
	// Assume the request is for setting the sampling interval
	bool start = false;
	if( this->PushButtonStartStopReadout->isOn() )
	  {
	    this->startStopReadout();
	    start = true;
	  }
	this->SpinBoxInterval->setValue( req_id );
	if( start ) this->startStopReadout();
	this->addDiagnostic( "TCP request for setting" );
      }
      break;
    }
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::quit()
{
  this->stopReadout();
  this->stopLogging();
  this->accept();
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::getData()
{
  int           no_of_bytes;
  unsigned char data[8];

  // Read any TPDO4 (COBID=0x480) messages
  while( _canNode->objRead( TPDO4_OBJ, &no_of_bytes, data ) )
    {
      if( no_of_bytes == 6 )
	{
	  int addr = data[0];
	  int chan = data[1];
	  int val  = data[3] | (data[4]<<8) | (data[5]<<16);

	  // Value is a 24-bit signed integer
	  if( val & 0x00800000 ) val |= 0xFF000000;

	  // Find location of this B-sensor
	  bool found = false;
	  int  str_id, i;
	  for( str_id = 0; str_id<NO_OF_STR; ++str_id )
	    {
	      for( i=0; i<_bInString[str_id]; ++i )
		{
		  if( i < MAX_B_PER_STR )
		    if( addr == _bAddr[str_id][i] )
		      {
			found = true;
			break;
		      }
		}
	      if( found ) break;
	    }

	  // Store 'raw' value
	  if( found ) _bVal[str_id][i][chan] = val;

	  // Convert Hall sensor data to (approximate) Gauss if required
	  if( chan != 3 && this->CheckBoxGauss->isChecked() )
	    {
	      int full_scale = 14000;
	      QString qs = this->ComboBoxFullScale->currentText();
	      if( qs == QString("1.4T") ) full_scale = 14000;
	      if( qs == QString("2.5T") ) full_scale = 25000;
	      if( qs == QString("4.5T") ) full_scale = 45000;
	      val = (val * full_scale) / 0x7FFFFF;
	    }

	  // If the B-sensor is visible in the current view,
	  // update the data in the displayed table
	  if( found && str_id == _currStringID )
	    {
	      QString qs;
	      switch( chan )
		{
		case 0:
		  qs = QString::number( val );
		  _xDisplay[i]->setText( qs );
		  break;
		case 1:
		  qs = QString::number( val );
		  _yDisplay[i]->setText( qs );
		  break;
		case 2:
		  qs = QString::number( val );
		  _zDisplay[i]->setText( qs );
		  break;
		case 3:
		  qs = QString::number( (double) val/1000.0, 'f', 2 );
		  _tDisplay[i]->setText( qs );
		  break;
		default:
		  break;
		}
	    }

	  // Log the data or send the data to TCP clients
	  // (after the last value of the last B-sensor
	  //  in the configuration is received)
	  if( addr == _bAddrHi && chan == 3 )
	    {
	      // Copy B-sensor data values for (pull) server
	      int str_id, i, j;
	      for( str_id=0; str_id<NO_OF_STR; ++str_id )
		for( i=0; i<_bInString[str_id]; ++i )
		  for( j=0; j<4; ++j )
		    _bValCopy[str_id][i][j] = _bVal[str_id][i][j];

	      // Only sent here when the server is in 'push' mode
	      if( !this->CheckBoxPullMode->isChecked() ) this->sendData();
	      this->logData();

	      // Reset B-sensor data values
	      for( str_id=0; str_id<NO_OF_STR; ++str_id )
		for( i=0; i<_bInString[str_id]; ++i )
		  for( j=0; j<4; ++j )
		    _bVal[str_id][i][j] = 0x00000000;
	    }
	}
    }

  // Read any EMERGENCY messages
  while( _canNode->objRead( EMERGENCY_OBJ, &no_of_bytes, data ) )
    {
      int emg_code = data[0] | (data[1]<<8);
      int id       = data[3];
      int addr     = data[4];
      if( emg_code == 0x5000 &&
	  (id == EMG_ADC_CONVERSION_BSE ||
	   id == EMG_ADC_RESET_BSE      ||
	   id == EMG_ADC_HALL_CALIB_BSE ||
	   id == EMG_ADC_T_CALIB_BSE    ||
	   id == EMG_ADC_CALIBCONST_BSE ||
	   id == EMG_ADC_BADDR_BSE) )
	{
	  // B-sensor 'addr' ADC-related emergency
	  for( int str_id=0; str_id<NO_OF_STR; ++str_id )
	    for( int i=0; i<_bInString[str_id]; ++i )
	      {
		if( i < MAX_B_PER_STR && addr == _bAddr[str_id][i] )
		  {
		    _bStat[str_id][i] |= 1;
		    if( str_id == _currStringID )
		      {
			// Clear data fields
			_tDisplay[i]->clear();
			_xDisplay[i]->clear();
			_yDisplay[i]->clear();
			_zDisplay[i]->clear();

			// Notify user by means of color
			_aDisplay[i]->setPalette( _qpErr );
			_tDisplay[i]->setPalette( _qpErr );
			_xDisplay[i]->setPalette( _qpErr );
			_yDisplay[i]->setPalette( _qpErr );
			_zDisplay[i]->setPalette( _qpErr );
		      }
		    this->LineEditBtotal->setPalette( _qpErr );
		    break;
		  }
	      }
	}

      QString qs;
      qs.append( QString("###EMERGENCY message received: code ") );
      qs.append( QString::number(emg_code,16).upper() );
      qs.append( QString(", data ") );
      for( int i=3; i<8; ++i )
	{
	  if( data[i] < 0x10 ) qs.append( QString("0") );
	  qs.append( QString::number(data[i],16).upper() );
	  if( i != 7 ) qs.append( QString(", ") );
	}
      this->addDiagnostic( qs );
    }

  // Stop collecting data if the 'start/stop' button has been pressed
  // (to become 'not-pressed-down')
  // or else trigger a next call of this function
  if( this->PushButtonStartStopReadout->isOn() == true )
    QTimer::singleShot( 1, this, SLOT( getData() ) );
}

/* ------------------------------------------------------------------------ */

void BsCAN2uiDialog::getUnexpectedMessages()
{
  int           no_of_bytes;
  unsigned char data[8];

  // Not if readout is in progress... */
  if( this->PushButtonStartStopReadout->isDown() == true )
    {
      QTimer::singleShot( 1500, this, SLOT( getUnexpectedMessages() ) );
      return;
    }

  // Read any EMERGENCY messages and display
  while( _canNode->objRead( EMERGENCY_OBJ, &no_of_bytes, data ) )
    {
      int emg_code = data[0] | (data[1]<<8);
      QString qs;
      qs.append( QString("###EMERGENCY message received: code ") );
      qs.append( QString::number(emg_code,16).upper() );
      qs.append( QString(", data ") );
      for( int i=3; i<8; ++i )
	{
	  qs.append( QString::number(data[i],16).upper() );
	  if( i != 7 ) qs.append( QString(", ") );
	}
      this->addDiagnostic( qs );
    }

  if( _canNode->objRead( BOOTUP_OBJ, &no_of_bytes, data ) )
    {
      // Proper Bootup message received ?
      if( no_of_bytes == 1 && data[0] == 0 )
	{
	  // Bootup message received
	  QString qs;
	  qs.append( "###BOOTUP message received" );
	  this->addDiagnostic( qs );
	  this->readConfig();
	}
    }

  QTimer::singleShot( 1500, this, SLOT( getUnexpectedMessages() ) );
}

/* ------------------------------------------------------------------------ */
