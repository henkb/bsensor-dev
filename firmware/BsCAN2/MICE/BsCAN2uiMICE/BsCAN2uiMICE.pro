#
# Project file for the BsCAN2uiMICE tool
# (NB: created with ancient Qt v2.3.0)
#
# To generate a Visual Studio project:
#   qmake -t vcapp BsCAN2uiMICE.pro
# To generate a Makefile:
#   qmake BsCAN2uiMICE.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = app
TARGET   = BsCAN2uiMICE

# Create a Qt app
CONFIG += qt thread warn_on exceptions debug_and_release
contains(QT_MAJOR_VERSION,5) {
  QT += widgets
}

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../../Debug
  LIBS       += -L../../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../../Release
  LIBS       += -L../../Release
}

LIBS += -lBsCAN
LIBS += -lCANopen

INCLUDEPATH += ../../CAN/CANopen
INCLUDEPATH += ../../BsCAN

win32 {
  RC_FILE = bscan3ui.rc
}

FORMS    += bscan2uimicedialogbase.ui
RESOURCES = bscan3ui.qrc

SOURCES  += main.cpp
SOURCES  += bscan2uimicedialog.cpp
SOURCES  += bscan2uimicedialogbase.cpp
HEADERS  += bscan2uimicedialog.h
HEADERS  += bscan2uimicedialogbase.h
