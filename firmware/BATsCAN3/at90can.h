/* ------------------------------------------------------------------------
File   : at90can.h

Descr  : Definitions for the AT90CANxxx onchip CAN-controller.

History: 09MAR.08; Henk B&B; Start development.
--------------------------------------------------------------------------- */

#ifndef AT90CAN_H
#define AT90CAN_H

/* ------------------------------------------------------------------------ */
/* CiA recommended bit timing settings:
   ------------------------------------
     Bit Rate   Quanta Per Bit      Sample Location  Bus Length
     --------   --------------      ---------------  -----------    
     1 Mbit/s:  8 quanta @ 125 ns,   sample at  6,  bus length < 25 m
   800 Kbit/s: 10 quanta @ 125 ns,   sample at  8,  bus length < 50 m
   500 Kbit/s: 16 quanta @ 125 ns,   sample at 14,  bus length < 100 m
   250 Kbit/s: 16 quanta @ 250 ns,   sample at 14,  bus length < 250 m
   125 Kbit/s: 16 quanta @ 500 ns,   sample at 14,  bus length < 500 m
    50 Kbit/s: 16 quanta @ 1.25 us,  sample at 14,  bus length < 1000 m
    20 Kbit/s: 16 quanta @ 3.125 us, sample at 14,  bus length < 2500 m
    10 Kbit/s: 16 quanta @ 6.25 us,  sample at 14,  bus length < 5000 m

   Other settings:
   ---------------
   Sampling mode             : single sampling
   Synchronisation mode      : recessive to dominant edges only
   Synchronisation jump width: 1 * time quantum
   Phase Segment 2           : 2 * time quantum

   AT90CANxxx: 
   With a 4 MHz processor clock up to 250 kbit/s bit rate can be achieved
   when using the settings above on the AT90CANxxx,
   and note that 20 Kbit/s is also not possible.

   Tbit = Tsyns + Tprs + Tphs1 + Tphs2
   250 Kbit/s: 16 = 1 + 8 + 5 + 2, BRP=0, PRS=7, PHS1=4, PHS2=1, SJW=0
   125 Kbit/s: 16 = 1 + 8 + 5 + 2, BRP=1, PRS=7, PHS1=4, PHS2=1, SJW=0
    50 Kbit/s: 16 = 1 + 8 + 5 + 2, BRP=4, PRS=7, PHS1=4, PHS2=1, SJW=0
    20 KBit/s: 20 = 1 + 8 + 8 + 3, BRP=9, PRS=7, PHS1=7, PHS2=2, SJW=0
    10 Kbit/s: 16 = 1 + 8 + 5 + 2, BRP=24, PRS=7, PHS1=4, PHS2=1, SJW=0

   Other:
   500 Kbit/s:  8 = 1 + 5 + 1 + 1, BRP=0, PRS=4, PHS1=0, PHS2=0, SJW=0
*/

/* Baud Rate Prescaler (BRP) */
#define CC_CANBT1_500K       (0<<1)
#define CC_CANBT1_250K       (0<<1)
#define CC_CANBT1_125K       (1<<1)
#define CC_CANBT1_50K        (4<<1)
#define CC_CANBT1_20K        (9<<1)
#define CC_CANBT1_10K        (24<<1)

/* Synchronization Jump Width (SJW) and Propagation Time Segment (PRS) */
#define CC_CANBT2_500K       ((0<<5) | (4<<1))
#define CC_CANBT2_250K       ((0<<5) | (7<<1))
#define CC_CANBT2_125K       ((0<<5) | (7<<1))
#define CC_CANBT2_50K        ((0<<5) | (7<<1))
#define CC_CANBT2_20K        ((0<<5) | (7<<1))
#define CC_CANBT2_10K        ((0<<5) | (7<<1))

/* Phase Segment 2 and 1 (PHS2 and PHS1) and Sample Points (SMP) */
#define CC_CANBT3_500K       ((0<<4) | (0<<1) | 0)
#define CC_CANBT3_250K       ((1<<4) | (4<<1) | 0)
#define CC_CANBT3_125K       ((1<<4) | (4<<1) | 0)
#define CC_CANBT3_50K        ((1<<4) | (4<<1) | 0)
#define CC_CANBT3_20K        ((2<<4) | (7<<1) | 0)
#define CC_CANBT3_10K        ((1<<4) | (4<<1) | 0)

/* ------------------------------------------------------------------------ */

/* Number of message object buffers */
#define CC_NO_OF_MOBS        15

/* Control */
#define CC_STANDBY()         CANGCON &= ~BIT(ENA)
#define CC_ENABLE()          CANGCON |= BIT(ENA)
#define CC_RESET()           CANGCON |= BIT(SWRES)
#define CC_LISTEN()          CANGCON |= BIT(LISTEN)

/* Status */
#define CC_ENABLED()         (CANGSTA & BIT(ENFG))
#define CC_BUSOFF()          (CANGSTA & BIT(BOFF))
#define CC_TXBUSY()          (CANGSTA & BIT(TXBSY))
#define CC_RXBUSY()          (CANGSTA & BIT(RXBSY))
#define CC_ERRPASSIVE()      (CANGSTA & BIT(ERRP))

/* Interrupts */
#define CC_INTRPT_ENABLE()   CANGIE =BIT(ENRX)|BIT(ENBOFF)|BIT(ENERG)|BIT(ENIT)
#define CC_INTRPT_DISABLE()  CANGIE &= ~BIT(ENIT)

/* MOBs */
#define CC_MOB_TRANSMIT      BIT(CONMOB0)
#define CC_MOB_RECEIVE       BIT(CONMOB1)
#define CC_MOB_DISABLE       0x00
#define CC_CONMOB_MASK       (BIT(CONMOB0) | BIT(CONMOB1))

/* ------------------------------------------------------------------------ */
#endif /* AT90CAN_H */
