/* ------------------------------------------------------------------------
File   : config.h

Descr  : Configuration of the AT90CANxxx plug-on board (called BATCAN),
         now for a string of NIKHEF B-sensor modules.

History: 23FEB.09; Henk B&B; BsCAN Version 3.1.0
                             (for ELMB on MDT-DCS motherboard):
                             - (Re)introduce broadcast-to-B-sensors possibility
                               in case the (CERN) B-sensor adapter boards
                               (with SDO output block) are used;
                               the option of using the adapter board or not
                               -and thus the ability to broadcast-
                               must be configured (a new ADC configuration
                               object subindex has been made available
                               for this purpose).
                             - ds2405.c: extend ds2405_search_next() with
                               check on CRC and on all-data-bytes-are-zero.
         03OCT.09; Henk B&B; BATsCAN Version 3.1.1:
                             - version for BATCAN with single B-sensor string,
                               based on version 3.1.0 for ELMB/MDT-DCS.
         14JAN.10; Henk B&B; BATsCAN Version 3.1.2:
                             - version for BATCAN as well as mBATCAN
                               with four B-sensor strings.
                             - for compatibility with BATCAN software
                               add index 2900 for reading out the ID.
                             - broadcast to B-sensor ADCs enabled by default.
                             - probe for B-sensors at every (hard) reset or
                               power-up enabled by default (but configurable,
                               i.e. feature can be disabled).
                             - switch red LED off only when all B-sensors
                               connected are error-free.
         16FEB.10; Henk B&B; BATsCAN Version 3.1.3:
                             - adc_bsensor.c:
                               - WDR() added to adcb_init_csr().
                               - added sub-indices to adcb_get_config() to
                                 to allow read-out of registers without DS2405
                                 selection (needs to be done separately).
                             - btrigger.c: trigger inputs need internal pull-up
                               resistor enabled.
                             - can.c: slightly different LED on/off policy
                               to make sure it blinks under heavy traffic.
                             - sdo.c: allow ADCB_MAX_CNT for adcb_reset_and_
                               calibrate(), resets all (bugfix).
         24FEB.10; Henk B&B; BATsCAN Version 3.1.4:
                             - adc_bsensor.c: report unexpected necessary
                               ADC (de)selection via the DS2405 chip.
         06JUL.12; Henk B&B; BATsCAN Version 3.1.5:
                             - adc_bsensor.c:
                               - added sub-indices to adcb_set_config() to
                                 to allow write of registers without DS2405
                                 selection (needs to be done separately);
                                 NB: also requires mod to BATCAN firmware
                                     (v1.1.1->1.1.2) to remain compatible.
                               - BUG fix: undesired select-error
                                 Emergency in adcb_pdo_scan_stop() due to
                                 v3.1.4 change.
                             - btrigger.c: added btrigger_get_config() which
                               returns the configuration of all trigger inputs
                               in one byte.
                             - sdo.c: add index 5404 to read out the trigger
                               input configuration byte.
                             - intrpt.c: BUG fix: remove EXT_INT numbers used
                               in btrigger.c.
         28NOV.12; Henk B&B; BATsCAN Version 3.2.0:
                             - Added support for B-sensors with the DS2413
                               ID-chip.
                             - adc_bsensor.c: in adcb_scan_next() be more
                               careful deselecting a B-sensor in case of
                               time-out+reinit.
         16JUL.17; Henk B&B; BATsCAN Version 3.2.1:
                             - adcb_bsensor.c:
                               added WDR() to adcb_deactivate(), because
                               experienced watchdog reset when more than
                               about 40 to 60 DS2413 B-sensors attached.
--------------------------------------------------------------------------- */

#ifndef CONFIG_H
#define CONFIG_H

/* ------------------------------------------------------------------------ */
/* Node identification */

/* CANopen device type = 0x00000000,
   i.e. no particular device profile supported */
#define DEVICE_TYPE_CHAR0           0x00
#define DEVICE_TYPE_CHAR1           0x00
#define DEVICE_TYPE_CHAR2           0x00
#define DEVICE_TYPE_CHAR3           0x00

/* Manufacturer device name: BATC */
#define MNFCT_DEV_NAME_CHAR0        'B'
#define MNFCT_DEV_NAME_CHAR1        'A'
#define MNFCT_DEV_NAME_CHAR2        'T'
#define MNFCT_DEV_NAME_CHAR3        'C'

/* Hardware version: BATCAN version 1.0 */
#define MNFCT_HARDW_VERSION_CHAR0   'B'
#define MNFCT_HARDW_VERSION_CHAR1   'C'
#define MNFCT_HARDW_VERSION_CHAR2   '1'
#define MNFCT_HARDW_VERSION_CHAR3   '0'

/* Firmware version: BATsCAN firmware version 3.2) */
#define MNFCT_SOFTW_VERSION_CHAR0   'B'
#define MNFCT_SOFTW_VERSION_CHAR1   's'
#define MNFCT_SOFTW_VERSION_CHAR2   '3'
#define MNFCT_SOFTW_VERSION_CHAR3   '2'

/* Firmware 'minor' version */
#define SOFTW_MINOR_VERSION_CHAR0   '0'
#define SOFTW_MINOR_VERSION_CHAR1   '0'
#define SOFTW_MINOR_VERSION_CHAR2   '0'
#define SOFTW_MINOR_VERSION_CHAR3   '1'

#define VERSION_STRING              " BATsCAN v3.2.1, 16 Jul 2017, HB, NIKHEF "
//#define VERSION_STRING            " BATsCAN v3.2.0, 28 Nov 2012, HB, NIKHEF "

/* ------------------------------------------------------------------------ */

/* Set pins connected to LEDs to output,
   and enable the pull-up resistor on unconnected inputs,
   as recommended in the datasheet.
   Used are (see other include files, or schematic): PB1, PB4, PB5, PE0, PE1,
   and PD5 (TXCAN output) and PD6 (RXCAN input) */
#define BATCAN_INIT_DDR()           {PORTA=0xFF; PORTB=0xCD; PORTC=0xFF; \
                                     PORTE=0xFC; /*PORTF=0xFF;*/ PORTG=0xFF; \
                                     SETBIT(DDRC,0);SETBIT(DDRC,1); \
                                     PORTD=0xFF; \
                                     SETBIT(DDRD,5); }

/* LED control */
#define LED_GREEN_ON()              CLEARBIT( PORTC, 0 )
#define LED_GREEN_OFF()             SETBIT( PORTC, 0 )
#define LED_GREEN_IS_OFF()          (PORTC & BIT(0))
#define LED_RED_ON()                CLEARBIT( PORTC, 1 )
#define LED_RED_OFF()               SETBIT( PORTC, 1 )

/* ------------------------------------------------------------------------ */
#endif /* CONFIG_H */
