/* ------------------------------------------------------------------------
File   : watchdog.c

Descr  : Contains watchdog functions.

History: 20JUL.00; Henk B&B; Version for ATmega103.
--------------------------------------------------------------------------- */

#include "general.h"

/* ------------------------------------------------------------------------ */

void watchdog_init( void )
{
  BOOL global_int_enabled;

  /* Disable interrupts during this operation */
  global_int_enabled = FALSE;
  if( SREG & 0x80 ) global_int_enabled = TRUE;
  CLI();

  /* Watchdog timer part: set prescale to maximum (ca. 1.9 s) and enable */
  WDR();                        /* Recommended before enabling */
  WDTCR = BIT(WDCE) | BIT(WDE); /* To change prescale need timed sequence
                                   on ATmega128 */
  WDTCR = BIT(WDE) | BIT(WDP0) | BIT(WDP1) | BIT(WDP2);
  WDTCR = BIT(WDE);             /* Enable watchdog */

  /* Re-enable interrupts if required */
  if( global_int_enabled ) SEI();
}

/* ------------------------------------------------------------------------ */

void watchdog_disable( void )
{
  BOOL global_int_enabled;

  /* Disable interrupts during this operation */
  global_int_enabled = FALSE;
  if( SREG & 0x80 ) global_int_enabled = TRUE;
  CLI();

  /* Watchdog timer disable */
  WDTCR = BIT(WDCE) | BIT(WDE);
  WDTCR = BIT(WDP0) | BIT(WDP1) | BIT(WDP2); /* Keep prescale bits */

  /* Re-enable interrupts if required */
  if( global_int_enabled ) SEI();
}

/* ------------------------------------------------------------------------ */

void watchdog( void )
{
  /* Watchdog timer reset */
  WDR();
}

/* ------------------------------------------------------------------------ */
