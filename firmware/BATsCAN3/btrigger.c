/* ------------------------------------------------------------------------
File   : btrigger.c

Descr  : Functions for External Interrupt generated B-sensor readout;
         INT0 to INT3 are used (these are available on the mBATCAN module),
         on microcontroller PORTD pins 0 to 3.

History: 07DEC.09; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#include "general.h"
#include "adc_bsensor.h"
#include "btrigger.h"
#include "store.h"

extern BOOL AdcScanTrigger;

/* ------------------------------------------------------------------------ */
/* Globals */

/* Byte indicating which interrupts are enabled (4 bits significant) */
static BYTE BtrigIntrEna;    /* (stored in EEPROM) */

/* Byte indicating for each interrupt input whether
   the signal triggers at rising or falling edge */
static BYTE BtrigRisingEdge; /* (stored in EEPROM) */

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static void btrigger_load_config( void );

/* ------------------------------------------------------------------------ */

void btrigger_init( void )
{
  BYTE i;

  /* Get parameters */
  btrigger_load_config();

  /* Disable INT0-INT3 External Interrupts */
  EIMSK &= 0xF0;

  /* Set PORTD pins 0 to 3 to inputs, with pull-up resistors */
  DDRD  &= ~0x0F;
  PORTD |= 0x0F;

  /* Configure the INT0 to INT4 external interrupt inputs */
  for( i=0; i<BTRIGGER_INPUTS; ++i )
    {
      if( BtrigRisingEdge & (1<<i) )
        {
          /* INTn rising edge: ISCn1=1, ISCn0=1 */
          EICRA |= (3 << (i*2)); 
        }
      else
        {
          /* INTn falling edge: ISCn1=1, ISCn0=0 */
          EICRA |= (2 << (i*2)); 
          EICRA &= ~(1 << (i*2)); 
        }
    }

  /* Enable selected External Interrupts (limited to INT0 to INT3) */
  EIMSK |= (BtrigIntrEna & 0x0F);
}

/* ------------------------------------------------------------------------ */

BOOL btrigger_get_enabled( BYTE i )
{
  if( i > BTRIGGER_INPUTS-1 ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  BtrigIntrEna = eeprom_read( EE_BTRIGINTRENA );
#endif
  if( BtrigIntrEna & (1 << i) )
    return TRUE;
  else
    return FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL btrigger_set_enabled( BYTE i, BOOL enable )
{
  if( i > BTRIGGER_INPUTS-1 ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  BtrigIntrEna = eeprom_read( EE_BTRIGINTRENA );
#endif /* __VARS_IN_EEPROM__ */

  if( enable )
    {
      BtrigIntrEna |= (1 << i);
      EIMSK |= (1 << i);
    }
  else
    {
      BtrigIntrEna &= ~(1 << i);
      EIMSK &= ~(1 << i);
    }
#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_BTRIGINTRENA ) != BtrigIntrEna )
    eeprom_write( EE_BTRIGINTRENA, BtrigIntrEna );
#endif /* __VARS_IN_EEPROM__ */

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL btrigger_get_rising_edge( BYTE i )
{
  if( i > BTRIGGER_INPUTS-1 ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  BtrigRisingEdge = eeprom_read( EE_BTRIGRISINGEDGE );
#endif
  if( BtrigRisingEdge & (1 << i) )
    return TRUE;
  else
    return FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL btrigger_set_rising_edge( BYTE i, BOOL rising )
{
  if( i > BTRIGGER_INPUTS-1 ) return FALSE;

#ifdef __VARS_IN_EEPROM__
  BtrigRisingEdge = eeprom_read( EE_BTRIGRISINGEDGE );
#endif /* __VARS_IN_EEPROM__ */

  if( rising )
    {
      BtrigRisingEdge |= (1 << i);

      /* INTn rising edge: ISCn1=1, ISCn0=1 */
      EICRA |= (3 << (i*2)); 
    }
  else
    {
      BtrigRisingEdge &= ~(1 << i);

      /* INTn falling edge: ISCn1=1, ISCn0=0 */
      EICRA |= (2 << (i*2)); 
      EICRA &= ~(1 << (i*2)); 
    }
#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_BTRIGRISINGEDGE ) != BtrigRisingEdge )
    eeprom_write( EE_BTRIGRISINGEDGE, BtrigRisingEdge );
#endif /* __VARS_IN_EEPROM__ */

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BYTE btrigger_get_config( void )
{
  /* Condense into one byte the settings of 4 trigger inputs:
     enabled/disabled and rising-edge/falling-edge */
  BYTE config = 0x00;
#ifdef __VARS_IN_EEPROM__
  BtrigIntrEna    = eeprom_read( EE_BTRIGINTRENA );
  BtrigRisingEdge = eeprom_read( EE_BTRIGRISINGEDGE );
#endif
  config |= ((BtrigIntrEna & 0x0F) << 0);
  config |= ((BtrigRisingEdge & 0x0F) << 4);
  return config;
}

/* ------------------------------------------------------------------------ */

#define BTRIGGER_STORE_SIZE 2

/* ------------------------------------------------------------------------ */

BOOL btrigger_store_config( void )
{
  BYTE block[BTRIGGER_STORE_SIZE];

#ifdef __VARS_IN_EEPROM__
  BtrigIntrEna    = eeprom_read( EE_BTRIGINTRENA );
  BtrigRisingEdge = eeprom_read( EE_BTRIGRISINGEDGE );
#endif
  block[0] = BtrigIntrEna;
  block[1] = BtrigRisingEdge;

  return( storage_write_block( STORE_BTRIGGER, BTRIGGER_STORE_SIZE, block ) );
}

/* ------------------------------------------------------------------------ */

static void btrigger_load_config( void )
{
  BYTE block[BTRIGGER_STORE_SIZE];

  /* Preset block with parameter defaults */
  block[0] = 0;    /* BtrigIntrEna */
  block[1] = 0x0F; /* BtrigRisingEdge */

  /* Read the configuration from EEPROM, if any */
  storage_read_block( STORE_BTRIGGER, BTRIGGER_STORE_SIZE, block );

  BtrigIntrEna    = block[0];
  BtrigRisingEdge = block[1];

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_BTRIGINTRENA ) != BtrigIntrEna )
    eeprom_write( EE_BTRIGINTRENA, BtrigIntrEna );
  if( eeprom_read( EE_BTRIGRISINGEDGE ) != BtrigRisingEdge )
    eeprom_write( EE_BTRIGRISINGEDGE, BtrigRisingEdge );
#endif /* __VARS_IN_EEPROM__ */
}

/* ------------------------------------------------------------------------ */
/* INT interrupt handlers */

#pragma interrupt_handler btrigger_readout_0:iv_EXT_INT0
#pragma interrupt_handler btrigger_readout_1:iv_EXT_INT1
#pragma interrupt_handler btrigger_readout_2:iv_EXT_INT2
#pragma interrupt_handler btrigger_readout_3:iv_EXT_INT3

/* When appropriate the interrupt handlers flag the main function
   to start up a B-sensor ADC channel scan */

void btrigger_readout_0( void )
{
  if( BtrigIntrEna & 0x1 ) AdcScanTrigger = TRUE;
}

void btrigger_readout_1( void )
{
  if( BtrigIntrEna & 0x2 ) AdcScanTrigger = TRUE;
}

void btrigger_readout_2( void )
{
  if( BtrigIntrEna & 0x4 ) AdcScanTrigger = TRUE;
}

void btrigger_readout_3( void )
{
  if( BtrigIntrEna & 0x8 ) AdcScanTrigger = TRUE;
}

/* ------------------------------------------------------------------------ */
