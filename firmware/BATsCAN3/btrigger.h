/* ------------------------------------------------------------------------
File   : btrigger.h

Descr  : Include file for btrigger.c, functions for B-sensor readout
         triggered by external interrupts.

History: 07DEC.09; Henk B&B; Definition.
--------------------------------------------------------------------------- */

#ifndef BTRIGGER_H
#define BTRIGGER_H

/* ------------------------------------------------------------------------ */

#define BTRIGGER_INPUTS      4

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void btrigger_init           ( void );

BOOL btrigger_get_enabled    ( BYTE i );
BOOL btrigger_set_enabled    ( BYTE i, BOOL enable );
BOOL btrigger_get_rising_edge( BYTE i );
BOOL btrigger_set_rising_edge( BYTE i, BOOL rising );

BYTE btrigger_get_config     ( void );

BOOL btrigger_store_config   ( void );

/* ------------------------------------------------------------------------ */
#endif /* BTRIGGER_H */
