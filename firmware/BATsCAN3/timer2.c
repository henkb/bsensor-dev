/* ------------------------------------------------------------------------
File   : timer2.c

Descr  : ATMEL AVR microcontroller Timer/Counter2 routines.

History: 23MAR.01; Henk B&B; Timer2 used for time-outs on operations.
         06JUN.08; Henk B&B; Swap Timer0 and Timer2 functions for AT90CANxxx.
--------------------------------------------------------------------------- */

#include "general.h"
#include "timer.h"

/* Counters for time-outs */
static BYTE T2_Countdown[T2_CLIENTS];

/* ------------------------------------------------------------------------ */

void timer2_init( void )
{
  /* Initialize Timer2 for our purposes: time-outs;
     use a 'clocktick' of 10 milliseconds */

  /* Set to a 10 ms clocktick */
  SET_TIMER2_10MS();

  /* Enable Timer2 interrupt */
  TIMSK2 |= BIT( T2_OVERFLOW_IE );

  /* Global interrupts enabled elsewhere... */
}

/* ------------------------------------------------------------------------ */

void timer2_set_timeout_10ms( BYTE client, BYTE ticks )
{
  /* Sets a timeout for a period of ticks*10 milliseconds on Timer2
     for client 'client' on an interrupt basis;
     to be monitored by calling 'timer2_timeout(client)';
     'ticks' can be from 1 up to 255
     (so a maximum of 255*10 ms = 2.55 s can be set);
     note that if 'count' is 1 there could be an immediate time-out
     because the clock is running continuously;
     to ensure a minimum of 10 ms choose ticks>=2 */

  /* Disable Timer2 interrupt */
  TIMSK2 &= ~BIT( T2_OVERFLOW_IE );

  /* Initialize countdown counter */
  T2_Countdown[client] = ticks;

  /* Enable Timer2 interrupt */
  TIMSK2 |= BIT( T2_OVERFLOW_IE );
}

/* ------------------------------------------------------------------------ */

BOOL timer2_timeout( BYTE client )
{
  return( T2_Countdown[client] == 0 );
}

/* ------------------------------------------------------------------------ */
/* TIMER2 Overflow interrupt */

#pragma interrupt_handler timer2ovf_handler:iv_TIMER2_OVF

void timer2ovf_handler( void )
{
  BYTE i;

  /* Stop the timer */
  TCCR2 = T2_STOP;

  /* Reinitialize the timer */
  SET_TIMER2_10MS();

  /* Decrement countdown counters, where appropriate */
  for( i=0; i<T2_CLIENTS; ++i )
    if( T2_Countdown[i] ) --T2_Countdown[i];
}

/* ------------------------------------------------------------------------ */
