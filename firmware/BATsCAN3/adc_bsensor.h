/* ------------------------------------------------------------------------
File   : adc_bsensor.h

Descr  : Include file for adc_bsensor.c (with CS5524 ADC).

History: 28FEB.01; Henk B&B; Definition.
         28NOV.08; Henk B&B; Modifications for strings of B-sensors of the
                             simple type selected by means of the DS2405
                             ID-chip with I/O-pin.
         04OCT.09; Henk B&B; Version for BATCAN module, with 1 string only.
--------------------------------------------------------------------------- */

#ifndef ADC_BSENSOR_H
#define ADC_BSENSOR_H

/* ------------------------------------------------------------------------ */
/* Configuration */

/* Configuration of the SPI serial interface for the BATCAN module */

/* String #0: connected to the standard BATCAN B-sensor ADC pins */
#define ADCB_SET_SCLK_0()             SETBIT( PORTB, 1 )
#define ADCB_CLEAR_SCLK_0()           CLEARBIT( PORTB, 1 )

#define ADCB_SET_SDI_0()              SETBIT( PORTE, 0 )
#define ADCB_CLEAR_SDI_0()            CLEARBIT( PORTE, 0 )

#define ADCB_SDO_HIGH_0()             (PINE & BIT(1))
#define ADCB_SDO_LOW_0()              (!ADCB_SDO_HIGH_0())

#define ADCB_SET_CS_0()               SETBIT( PORTB, 5 )
#define ADCB_CLEAR_CS_0()             CLEARBIT( PORTB, 5 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_0()           {SETBIT(DDRB,1);SETBIT(DDRE,0); \
                                     CLEARBIT(DDRE,1);SETBIT(PORTE,1); \
                                     SETBIT(DDRB,5);}

/* String #1: NB: not connected on BATCAN */
#define ADCB_SET_SCLK_1()           SETBIT( PORTB, 0 )
#define ADCB_CLEAR_SCLK_1()         CLEARBIT( PORTB, 0 )

#define ADCB_SET_SDI_1()            SETBIT( PORTB, 2 )
#define ADCB_CLEAR_SDI_1()          CLEARBIT( PORTB, 2 )

#define ADCB_SDO_HIGH_1()           (PINB & BIT(3))
#define ADCB_SDO_LOW_1()            (!(PINB & BIT(3)))

#define ADCB_SET_CS_1()             SETBIT( PORTB, 6 )
#define ADCB_CLEAR_CS_1()           CLEARBIT( PORTB, 6 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_1()           {SETBIT(DDRB,0);SETBIT(DDRB,2); \
                                     CLEARBIT(DDRB,3);SETBIT(PORTB,3); \
                                     SETBIT(DDRB,6);}

/* String #2: NB: not connected on BATCAN */
#define ADCB_SET_SCLK_2()           SETBIT( PORTE, 2 )
#define ADCB_CLEAR_SCLK_2()         CLEARBIT( PORTE, 2 )

#define ADCB_SET_SDI_2()            SETBIT( PORTE, 3 )
#define ADCB_CLEAR_SDI_2()          CLEARBIT( PORTE, 3 )

#define ADCB_SDO_HIGH_2()           (PINE & BIT(4))
#define ADCB_SDO_LOW_2()            (!(PINE & BIT(4)))

#define ADCB_SET_CS_2()             SETBIT( PORTE, 5 )
#define ADCB_CLEAR_CS_2()           CLEARBIT( PORTE, 5 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_2()           {SETBIT(DDRE,2);SETBIT(DDRE,3); \
                                     CLEARBIT(DDRE,4);SETBIT(PORTE,4); \
                                     SETBIT(DDRE,5);}

/* String #3: NB: not connected on BATCAN */
#define ADCB_SET_SCLK_3()           SETBIT( PORTE, 7 )
#define ADCB_CLEAR_SCLK_3()         CLEARBIT( PORTE, 7 )

#define ADCB_SET_SDI_3()            SETBIT( PORTC, 4 )
#define ADCB_CLEAR_SDI_3()          CLEARBIT( PORTC, 4 )

#define ADCB_SDO_HIGH_3()           (PINC & BIT(5))
#define ADCB_SDO_LOW_3()            (!(PINC & BIT(5)))

#define ADCB_SET_CS_3()             SETBIT( PORTC, 6 )
#define ADCB_CLEAR_CS_3()           CLEARBIT( PORTC, 6 )

/* Make sure this corresponds to the stuff above ! */
#define ADCB_INIT_DDR_3()           {SETBIT(DDRE,7);SETBIT(DDRC,4); \
                                     CLEARBIT(DDRC,5);SETBIT(PORTC,5); \
                                     SETBIT(DDRC,6);}

/* ------------------------------------------------------------------------ */

/* Maximum number of ADC devices (B-sensor modules) */
#define ADCB_MAX_CNT                128

/* Maximum number of ADC devices (B-sensor modules) per string */
#define ADCB_MAX_CNT_PER_STRING     32

/* Maximum number of B-sensor strings */
#define ADCB_MAX_STRINGS            4

/* Number of analog inputs per B-sensor ADC */
#define ADCB_CHANS_PER_ADC          7

/* Number of analog inputs per B-sensor ADC, during a scan ! */
#define ADCB_CHANS_PER_ADC_SCAN     4

/* ------------------------------------------------------------------------ */

/* Conversion parameters */
#define ADCB_CONV_PARS_B            ((CS23_WORDRATE_15 << \
                                      CS23_CSR_WORDRATE_SHIFT) | \
                                     (CS23_GAIN_100MV  << \
                                      CS23_CSR_GAIN_SHIFT) | \
                                     CS23_CSR_BIPOLAR)
#define ADCB_CONV_PARS_T            ((CS23_WORDRATE_15 << \
                                      CS23_CSR_WORDRATE_SHIFT) | \
                                     (CS23_GAIN_2V5  << \
                                      CS23_CSR_GAIN_SHIFT) | \
                                     CS23_CSR_UNIPOLAR)

/* Configuration Register */
#define ADCB_CSR_DEPTH              2
#define ADCB_CONVERSION_MODE        CS23_CNF_MULTIPLE_CONV
#define ADCB_CSR_DEPTH_MASK         ((ADCB_CSR_DEPTH*2-1) << \
                                     CS23_CNF_CSR_DEPTH_SHIFT)
#define ADCB_CNFREG_0               0
#define ADCB_CNFREG_1               ADCB_CSR_DEPTH_MASK
#define ADCB_CNFREG_2_CHOP256       ((CS23_CHOPFREQ_256 << \
                                      CS23_CNF_CHOPFREQ_SHIFT) | \
                                     ADCB_CONVERSION_MODE)
#define ADCB_CNFREG_2_CHOP4096      ((CS23_CHOPFREQ_4096 << \
                                      CS23_CNF_CHOPFREQ_SHIFT) | \
                                      ADCB_CONVERSION_MODE)

/* There is no signal rise/fall times to take into account,
   (no opto-couplers between ELMB and the B-sensor modules),
   but 10 mus is the minimum */
#define ADC_SIGNAL_RISETIME_BSENSOR   10
#define ADC_SIGNAL_FALLTIME_BSENSOR   10

/* ------------------------------------------------------------------------ */
/* Error ID bits */

/* In 'AdcError' status byte */
#define ADC_ERR_RESET               0x01
#define ADC_ERR_CALIBRATION         0x02
#define ADC_ERR_TIMEOUT             0x04
#define ADC_ERR_CALIB_CNST          0x08
#define ADC_ABSENT                  0xFF
#define ADC_ERR_IN_HARDWARE         (ADC_ERR_RESET|ADC_ERR_CALIBRATION| \
                                     ADC_ERR_TIMEOUT)

/* In CAN-messages with ADC data */
#define ADC_ERR_CONVERSION          0x80

/* ------------------------------------------------------------------------ */
/* Function prototypes */

BOOL adcb_init                 ( BYTE *err_id, BOOL send_emergency,
                                 BOOL hard_reset );
BOOL adcb_read                 ( BYTE addr,
                                 BYTE channel_no,
                                 BYTE *conversion_data );

BOOL adcb_status_summary       ( BYTE subindex, BYTE *status );
BOOL adcb_status               ( BYTE subindex, BYTE *status );

BYTE adcb_chan_cnt             ( void );
BYTE adcb_total_cnt            ( void );
BYTE adcb_string_cnt           ( BYTE string_no );
BYTE adcb_mapping              ( BYTE addr );
BYTE adcb_list                 ( BYTE index );

BOOL adcb_id_lo                ( BYTE addr, BYTE *id_lo );
BOOL adcb_id_hi                ( BYTE addr, BYTE *id_hi );

BOOL adcb_get_config           ( BYTE addr,    BYTE subindex,
                                 BYTE *nbytes, BYTE *par );
BOOL adcb_set_config           ( BYTE addr,    BYTE subindex,
                                 BYTE nbytes,  BYTE *par );

BOOL adcb_reset_and_calibrate  ( BYTE addr,       BOOL send_emergency );
BOOL adcb_reset                ( BYTE addr,       BOOL send_emergency );
BOOL adcb_calibrate            ( BYTE addr,       BOOL send_emergency );
BOOL adcb_calibrate_all        ( BYTE *no_of_err, BOOL send_emergency );

void adcb_pdo_scan_start       ( void );
void adcb_pdo_scan_stop        ( void );
void adcb_pdo_scan             ( void );

BOOL adcb_probe                ( BOOL init_after_probe );

BOOL adcb_set_probe_at_init    ( BOOL probe );
BOOL adcb_get_probe_at_init    ( void );

BYTE adcb_deactivate           ( void );

BOOL adcb_toggle_select        ( BYTE addr, BYTE *output );

BOOL adcb_store_config         ( void );

/* ------------------------------------------------------------------------ */
#endif /* ADC_BSENSOR_H */
