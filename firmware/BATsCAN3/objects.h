/* ------------------------------------------------------------------------
File   : objects.h

Descr  : Definitions for CANopen dictionary objects.

History: --JUL.00; Henk B&B; First version.
--------------------------------------------------------------------------- */

#ifndef OBJECTS_H
#define OBJECTS_H

/* ------------------------------------------------------------------------ */

#define OD_NO_OF_ENTRIES        0               /* Often used for subindex 0 */

/* ============================================================== */
/* Communication Profile objects */

#define OD_DEVICE_INFO_HI       0x10            /* Objects 0x10.. */
#define OD_DEVICE_TYPE_LO       0x00            /* Object  0x1000 */
#define OD_ERROR_REG_LO         0x01            /* Object  0x1001 */
#define OD_STATUS_REG_LO        0x02            /* Object  0x1002 */
#define OD_DEVICE_NAME_LO       0x08            /* Object  0x1008 */
#define OD_HW_VERSION_LO        0x09            /* Object  0x1009 */
#define OD_SW_VERSION_LO        0x0A            /* Object  0x100A */
#define OD_GUARDTIME_LO         0x0C            /* Object  0x100C */
#define OD_LIFETIME_FACTOR_LO   0x0D            /* Object  0x100D */
#define OD_STORE_PARAMETERS_LO  0x10            /* Object  0x1010 */
#define OD_DFLT_PARAMETERS_LO   0x11            /* Object  0x1011 */
#define OD_HEARTBEAT_TIME_LO    0x17            /* Object  0x1017 */
#define OD_IDENTITY_LO          0x18            /* Object  0x1018 */
#define OD_STORE_ALL            1
#define OD_STORE_COMM_PARS      2
#define OD_STORE_APP_PARS       3

#define OD_RPDO_PAR_HI          0x14            /* Objects 0x14.. */
#define OD_RPDO1_PAR_LO         0x00            /* Object  0x1400 */

#define OD_RPDO_MAP_HI          0x16            /* Objects 0x16.. */
#define OD_RPDO1_MAP_LO         0x00            /* Object  0x1600 */

#define OD_TPDO_PAR_HI          0x18            /* Objects 0x18.. */
#define OD_TPDO1_PAR_LO         0x00            /* Object  0x1800 */
#define OD_TPDO2_PAR_LO         0x01            /* Object  0x1801 */
#define OD_TPDO3_PAR_LO         0x02            /* Object  0x1802 */
#define OD_TPDO4_PAR_LO         0x03            /* Object  0x1803 */
#define OD_PDO_COBID            1
#define OD_PDO_TRANSMTYPE       2
#define OD_PDO_INHIBITTIME      3
#define OD_PDO_DUMMY_ENTRY      4
#define OD_PDO_EVENT_TIMER      5

#define OD_TPDO_MAP_HI          0x1A            /* Objects 0x1A.. */
#define OD_TPDO1_MAP_LO         0x00            /* Object  0x1A00 */
#define OD_TPDO2_MAP_LO         0x01            /* Object  0x1A01 */
#define OD_TPDO3_MAP_LO         0x02            /* Object  0x1A02 */
#define OD_TPDO4_MAP_LO         0x03            /* Object  0x1A03 */

/* ============================================================== */
/* Manufacturer-specific objects */

/* CRC */
#define OD_CALC_CRC_HI          0x30            /* Objects 0x30.. */
#define OD_CALC_CRC_LO          0x00            /* Object  0x3000 */
#define OD_CRC_APPCODE_FLASH     1
#define OD_CRC_DUMMY             2
#define OD_CRC_APPCODE_FLASH_GET 3

/* Device Serial Number */
#define OD_SERIAL_NO_HI         0x31            /* Objects 0x31.. */
#define OD_SERIAL_NO_LO         0x00            /* Object  0x3100 */
#define OD_SN_WRITE_ENA_LO      0x01            /* Object  0x3101 */

/* CAN-controller configuration */
#define OD_CAN_CONFIG_HI        0x32            /* Objects 0x32.. */
#define OD_CAN_CONFIG_LO        0x00            /* Object  0x3200 */

/* CANopen Node-ID configuration */
#define OD_NODEID_CONFIG_HI     0x33            /* Objects 0x33.. */
#define OD_NODEID_CONFIG_LO     0x00            /* Object  0x3300 */
#define OD_NODEID_WRITE_ENA_LO  0x01            /* Object  0x3301 */

/* Parameters for B-sensors */

#define OD_BSENSOR_CONFIG_HI    0x50            /* Objects 0x50.. */

#define OD_BSENSOR_STAT_SUMM_HI 0x51            /* Objects 0x51.. */
#define OD_BSENSOR_STAT_SUMM_LO 0x00            /* Object  0x5100 */

#define OD_BSENSOR_STATUS_HI    0x52            /* Objects 0x52.. */

#define OD_BSENSOR_RST_CALIB_HI 0x53            /* Objects 0x53.. */

#define OD_BSENSOR_TRIGGER_HI   0x54            /* Objects 0x54.. */

#define OD_BSENSOR_ANALOG_HI    0x55            /* Objects 0x55.. */

#define OD_BSENSOR_LIST_HI      0x56            /* Objects 0x56.. */
#define OD_BSENSOR_LIST_LO      0x00            /* Object  0x5600 */

#define OD_BSENSOR_STRNG_CNT_HI 0x57            /* Objects 0x57.. */
#define OD_BSENSOR_STRNG_CNT_LO 0x00            /* Objects 0x5700 */

#define OD_BSENSOR_MAP_HI       0x58            /* Objects 0x58.. */
#define OD_BSENSOR_MAP_LO       0x00            /* Object  0x5800 */

#define OD_BSENSOR_ID_HI        0x59            /* Objects 0x59.. */
#define OD_BSENSOR_ID_BATCAN_HI 0x29            /* Objects 0x29.. */

#define OD_BSENSOR_PROBE_HI     0x5B            /* Objects 0x5B.. */
#define OD_BSENSOR_PROBE_LO     0x00            /* Object  0x5B00 */
#define OD_BSENSOR_PROBE_ADR_LO 0x01            /* Object  0x5B01 */
#define OD_BSENSOR_REMOV_ADR_LO 0x02            /* Object  0x5B02 */
#define OD_BSENSOR_DESELECT_LO  0x03            /* Object  0x5B03 */
#define OD_BSENSOR_SELECT_LO    0x04            /* Object  0x5B04 */
#define OD_BSENSOR_AT_INIT_LO   0x05            /* Object  0x5B05 */

/* Other */
#define OD_COMPILE_OPTIONS_HI   0x5C            /* Objects 0x5C.. */
#define OD_COMPILE_OPTIONS_LO   0x00            /* Object  0x5C00 */

#define OD_SWITCH_TO_LOADER_HI  0x5E            /* Objects 0x5E.. */
#define OD_SWITCH_TO_LOADER_LO  0x00            /* Object  0x5E00 */

/* ============================================================== */
/* Standardised Device Profile objects */

/* ...........nothing so far.... */

/* ============================================================== */
/* Emergency objects: stuff for the Manufacturer-specific Error Field
   (first byte) */

#define EMG_CRC                 0x30

#define EMG_EEPROM_WRITE_PARS   0x41
#define EMG_EEPROM_READ_PARS    0x42

#define EMG_ADC_CONVERSION_BSE  0x51
#define EMG_ADC_RESET_BSE       0x52
#define EMG_ADC_HALL_CALIB_BSE  0x53
#define EMG_ADC_T_CALIB_BSE     0x54
#define EMG_ADC_INIT_BSE        0x55
#define EMG_ADC_CALIBCONST_BSE  0x56
#define EMG_ADC_BADDR_BSE       0x57
#define EMG_ADC_SELECTERR_BSE   0x58

#define EMG_IRREGULAR_RESET     0xF0
#define EMG_NO_BOOTLOADER       0xF1

/* ------------------------------------------------------------------------ */
#endif /* OBJECTS_H */
