/* ------------------------------------------------------------------------
File   : ds2405.c

Descr  : Functions for operating the DALLAS DS2401/DS2405/DS2413 Serial Number
         chip with 1-wire interface; the DS2405 has one digital I/O pin,
         the DS2413 has two such pins (only one used in the B-sensor module).
         Delays in microseconds according to the DS2405 datasheet 
         recommendations, the number between brackets shows general 1-Wire
         timing recommendations by manufacturer DALLAS.
         (I don't know why these do not match...)

         From manufacturer doc:
         The DS2401 enhanced Silicon Serial Number is a low-cost, electronic
         registration number that provides an absolutely unique identity which
         can be determined with a minimal electronic interface (typically,
         a single port pin of a microcontroller).
         The DS2401 consists of a factory-lasered, 64-bit ROM that includes
         a unique 48-bit serial number, an 8-bit CRC, and an 8-bit Family Code
         (01h). Data is transferred serially via the 1-Wire protocol
         that requires only a single data lead and a ground return.
         It operates at up to 16.3 kbits/s.
         'Present Pulses' acknowledges when the reader first applies voltage.
         Multiple DS2401s can reside on a common 1-Wire Net.
         Power for reading and writing the device is derived from the data line
         itself with no need for an external power source (zero standby power).

History: 30DEC.02; Henk B&B; Start of development.
         --JUN.08; Henk B&B; Support for DS2405; found that it has tighter
                             timing, when testing; needed to get rid of
                             the function pointers;
                             added ds2405_search_single() and
                             ds2405_bit() functions.
         26AUG.08; Henk B&B; Added ds2405_match() function (to toggle
                             the DS2405 digital output, for test purposes).
         09SEP.08; Henk B&B; Added ds2405_write_bit() and
                             ds2405_search_bit() functions;
                             added ds2405_search_init() and
                             ds2405_search_next() functions.
         28NOV.08; Henk B&B; Extend to 4 different (strings of) ID-chip(s),
                             with selection of the 1-Wire interface
                             through global variable 'DsSelected'.
         23FEB.09; Henk B&B; Break out of for-loop in ds2401_search_next();
                             extend ds2401_search_next() with check on CRC and
                             on all-data-bytes-are-zero.
         28NOV;12; Henk B&B; Added support for the DS2413.
--------------------------------------------------------------------------- */

#include "general.h"
#include "ds2405.h"
#include "timer.h"

/* ------------------------------------------------------------------------ */
/* Globals */

/* Selected ID-chip 'bus number' */
static BYTE OneWireBus;

/* For ID searches */
static BYTE BranchBitNo; /* At which bit there is a choice of 1 or 0 */
static BOOL LastFound;   /* Whether the last ID has been found */

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BOOL ds2405_init      ( void );
static BYTE ds2405_read_byte ( void );
static void ds2405_write_byte( BYTE byt );
static void ds2405_write_bit ( BYTE bit );
static BYTE ds2405_search_bit( void );

/* Delay of ca. 9 microseconds:
   ca. 1 plus 1 mus per increment @ 4MHz (measured) */
#define DELAY_9MUS() \
{ \
  BYTE tick; \
  for( tick=0; tick<8; ++tick ); \
}

/* Function calls are too slow so it appears
   (can not properly access buses 2 to 4) so the functions below
   have been redefined as macros for the time being,
   but even that proved to be too variable in processing time,
   so it was necessary to provide partly separate code for
   each of the 4 1-Wire buses... */

//static void ds2405_to_input  ( void );
//static void ds2405_to_output ( void );
//static BOOL ds2405_high      ( void );
//static void ds2405_set       ( void );
//static void ds2405_clear     ( void );

#ifdef _DS2405_MACROS_
#define ds2405_to_input() \
{ \
  if( OneWireBus == 0 )      DS2405_1_TO_INPUT(); \
  else if( OneWireBus == 1 ) DS2405_2_TO_INPUT(); \
  else if( OneWireBus == 2 ) DS2405_3_TO_INPUT(); \
  else if( OneWireBus == 3 ) DS2405_4_TO_INPUT(); \
}

#define ds2405_to_output() \
{ \
  if( OneWireBus == 0 )      DS2405_1_TO_OUTPUT(); \
  else if( OneWireBus == 1 ) DS2405_2_TO_OUTPUT(); \
  else if( OneWireBus == 2 ) DS2405_3_TO_OUTPUT(); \
  else if( OneWireBus == 3 ) DS2405_4_TO_OUTPUT(); \
}

#define ds2405_high() ((OneWireBus == 0) ? DS2405_1_HIGH() : \
((OneWireBus == 1) ? DS2405_2_HIGH() : \
((OneWireBus == 2) ? DS2405_3_HIGH() : \
((OneWireBus == 3) ? DS2405_4_HIGH() : 0))))

#define ds2405_set() \
{ \
  if( OneWireBus == 0 )      DS2405_1_SET(); \
  else if( OneWireBus == 1 ) DS2405_2_SET(); \
  else if( OneWireBus == 2 ) DS2405_3_SET(); \
  else if( OneWireBus == 3 ) DS2405_4_SET(); \
}

#define ds2405_clear() \
{ \
  if( OneWireBus == 0 )      DS2405_1_CLEAR(); \
  else if( OneWireBus == 1 ) DS2405_2_CLEAR(); \
  else if( OneWireBus == 2 ) DS2405_3_CLEAR(); \
  else if( OneWireBus == 3 ) DS2405_4_CLEAR(); \
}
#endif /* _DS2405_MACROS_ */

/* ------------------------------------------------------------------------ */

void ds2405_select( BYTE bus_nr )
{
  OneWireBus = bus_nr;
}

/* ------------------------------------------------------------------------ */

BYTE ds2405_crc( BYTE *id )
{
  /* Calculate CRC-8 value of the first 7 bytes of DS2405 data
     (1 byte family code and 6 bytes serial number);
     uses The CCITT-8 polynomial, expressed as X^8 + X^5 + X^4 + 1 */
  BYTE crc = 0x00;
  BYTE index;
  BYTE b;
  for( index=0; index<DS2405_BYTES-1; ++index )
    {
      BYTE byt = id[index];
      for( b=0; b<8; ++b )
        {
          if( (byt^crc) & 0x01 )
            {
              crc ^= 0x18;
              crc >>= 1;
              crc |= 0x80;
            }
          else
            {
              crc >>= 1;
            }
          byt >>= 1;
        }
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

void ds2405_search_init( void )
{
  /* Get things ready for executing the "Search ROM" command
     repeatedly until all IDs have been found */
  BranchBitNo = 0;
  LastFound   = FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_search_next( BOOL active, BYTE *id )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the 8-bits
     CRC from a next DS24xx by executing the "Search ROM" command */
  BOOL ints_enabled = FALSE;
  BOOL result = TRUE;

  if( LastFound ) return FALSE;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2405_init() == TRUE )
    {
      BYTE i, byte_i, bitmask, bitresult;
      BYTE last_zero = 0;

      if( active == TRUE )
        ds2405_write_byte( DS2405_SEARCHROM_ACTIVE_CMD );
      else
        ds2405_write_byte( DS2405_SEARCHROM_CMD );

      /* Search 64 bits */
      for( i=1; i<=DS2405_BITS; ++i )
        {
          byte_i    = (i-1) / 8;
          bitmask   = 1 << ((i-1)&7);
          bitresult = ds2405_search_bit();
          switch( bitresult )
            {
            case DS2405_ZERO:
              ds2405_write_bit( 0 );
              /* Clear ID bit */
              id[byte_i] &= ~bitmask;
              break;

            case DS2405_ONE:
              ds2405_write_bit( 1 );
              /* Set ID bit */
              id[byte_i] |= bitmask;
              break;

            case DS2405_DUAL:
              /* Select the path to follow */
              if( i == BranchBitNo )
                {
                  /* Latest branch (passed it before): this time follow 1 */
                  ds2405_write_bit( 1 );
                  /* Set ID bit */
                  id[byte_i] |= bitmask;
                }
              else if( i > BranchBitNo )
                {
                  /* Encountered a new branch: follow 0 */
                  ds2405_write_bit( 0 );
                  /* Clear ID bit */
                  id[byte_i] &= ~bitmask;
                  /* Keep track of last branch where 0 was chosen
                     (i.e. it is an unfinished branch) */
                  last_zero = i;
                }
              else if( i < BranchBitNo )
                {
                  /* 'Old' branch (passed it before):
                     follow the direction taken last time */
                  if( id[byte_i] & bitmask )
                    {
                      ds2405_write_bit( 1 );
                    }
                  else
                    {
                      ds2405_write_bit( 0 );
                      /* Keep track of last branch where 0 was chosen
                         (i.e. it is an unfinished branch) */
                      last_zero = i;
                    }
                }
              break;

            case DS2405_NONE:
            default:
              result = FALSE;
              break;
            }
          if( result == FALSE ) break; /* Break out of for-loop */
        }
      /* The last unfinished branch */
      BranchBitNo = last_zero;
      if( BranchBitNo == 0 ) LastFound = TRUE;

      /* Some additional checks */
      if( result == TRUE )
        {
          BOOL all_zeroes = TRUE;

          /* Check the CRC */
          if( ds2405_crc(id) != id[DS2405_BYTES-1] )
            result = FALSE;

          /* Check for all zeroes (which results in a correct CRC!) */
          for( i=0; i<DS2405_BYTES; ++i )
            if( id[i] != 0x00 )
              {
                all_zeroes = FALSE;
                break;
              }
          if( all_zeroes == TRUE ) result = FALSE;
        }
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  if( result == FALSE ) LastFound = TRUE;
  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_toggle( BYTE *id, BYTE *output_val )
{
  /* Writes the ID data in id[] to the DS2405, thereby causing
     a toggle of the output bit in case of a 'Match ROM';
     the 'out' byte is filled by executing one extra byte read
     which will result in 0xFF when the output is set to 1,
     and to 0x00 when the output is set to 0 */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_MATCHROM_CMD );

      for( i=0; i<DS2405_BYTES; ++i ) ds2405_write_byte( id[i] );

      *output_val = ds2405_read_byte();

      result = TRUE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2413_toggle( BYTE *id, BYTE *output_val )
{
  /* Writes the 64-bit ID in id[] to the DS2413, thereby allowing
     access to its I/O pins; then using subsequent 'PIO Read' and
     'PIO Write' commands the first PIOA pin is toggled */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i, stat;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_MATCHROM_CMD );

      for( i=0; i<DS2405_BYTES; ++i ) ds2405_write_byte( id[i] );

      ds2405_write_byte( DS2413_PIO_READ_CMD );

      // Get the PIOA Output Latch State
      stat = (ds2405_read_byte() & 0x02) >> 1;

      if( ds2405_init() == TRUE )
        {
          ds2405_write_byte( DS2405_MATCHROM_CMD );

          for( i=0; i<DS2405_BYTES; ++i ) ds2405_write_byte( id[i] );

          ds2405_write_byte( DS2413_PIO_WRITE_CMD );

          /* Write new output followed by its bit-inverted value */
          ds2405_write_byte( 0xFE | ~stat );
          ds2405_write_byte( ~(0xFE | ~stat) );

          /* Expect byte 0xAA followed by the new PIO pin status in reply */
          stat = ds2405_read_byte();
          if( stat == 0xAA )
            {
              /* Read the new PIO pin status */
              *output_val = ds2405_read_byte();
              result = TRUE;
            }
        }
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2413_set( BYTE *id, BYTE val, BYTE *output_stat )
{
  /* Writes the 64-bit ID in id[] to the DS2413, thereby allowing
     access to set its I/O pin(s); only the PIOA pin is set or reset */
  BOOL ints_enabled = FALSE;
  BOOL result = FALSE;
  BYTE i;

  /* 'val' should be 0 or 1 */
  if( val ) val = 1;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_MATCHROM_CMD );

      for( i=0; i<DS2405_BYTES; ++i ) ds2405_write_byte( id[i] );

      ds2405_write_byte( DS2413_PIO_WRITE_CMD );

      /* Write new output followed by its bit-inverted value */
      ds2405_write_byte( 0xFE | val );
      ds2405_write_byte( ~(0xFE | val) );

      /* Expect byte 0xAA followed by the new PIO pin status in reply */
      if( ds2405_read_byte() == 0xAA )
        {
          /* Read the new PIO pin status */
          *output_stat = ds2405_read_byte();
          result = TRUE;
        }
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

static BOOL ds2405_init( void )
{
  BOOL present;

  switch( OneWireBus )
    {
    case 0:
      DS2405_1_TO_OUTPUT();
      DS2405_1_CLEAR();

      /* Delay of 500 (480) microseconds */
      timer0_delay_mus( 250 );
      timer0_delay_mus( 250 );

      DS2405_1_TO_INPUT();
      DS2405_1_SET();   /* Enable pull-up */

      /* Delay of 100 (70) microseconds */
      timer0_delay_mus( 70 );

      /* If a DS2405 is present it will pull the line low */
      if( DS2405_1_HIGH() )
        present = FALSE;
      else
        present = TRUE;

      /* Delay of 400 (410) microseconds */
      timer0_delay_mus( 200 );
      timer0_delay_mus( 200 );

      break;

    case 1:
      DS2405_2_TO_OUTPUT();
      DS2405_2_CLEAR();

      /* Delay of 500 (480) microseconds */
      timer0_delay_mus( 250 );
      timer0_delay_mus( 250 );

      DS2405_2_TO_INPUT();
      DS2405_2_SET();   /* Enable pull-up */

      /* Delay of 100 (70) microseconds */
      timer0_delay_mus( 70 );

      /* If a DS2405 is present it will pull the line low */
      if( DS2405_2_HIGH() )
        present = FALSE;
      else
        present = TRUE;

      /* Delay of 400 (410) microseconds */
      timer0_delay_mus( 200 );
      timer0_delay_mus( 200 );

      break;

    case 2:
      DS2405_3_TO_OUTPUT();
      DS2405_3_CLEAR();

      /* Delay of 500 (480) microseconds */
      timer0_delay_mus( 250 );
      timer0_delay_mus( 250 );

      DS2405_3_TO_INPUT();
      DS2405_3_SET();   /* Enable pull-up */

      /* Delay of 100 (70) microseconds */
      timer0_delay_mus( 70 );

      /* If a DS2405 is present it will pull the line low */
      if( DS2405_3_HIGH() )
        present = FALSE;
      else
        present = TRUE;

      /* Delay of 400 (410) microseconds */
      timer0_delay_mus( 200 );
      timer0_delay_mus( 200 );

      break;

    case 3:
      DS2405_4_TO_OUTPUT();
      DS2405_4_CLEAR();

      /* Delay of 500 (480) microseconds */
      timer0_delay_mus( 250 );
      timer0_delay_mus( 250 );

      DS2405_4_TO_INPUT();
      DS2405_4_SET();   /* Enable pull-up */

      /* Delay of 100 (70) microseconds */
      timer0_delay_mus( 70 );

      /* If a DS2405 is present it will pull the line low */
      if( DS2405_4_HIGH() )
        present = FALSE;
      else
        present = TRUE;

      /* Delay of 400 (410) microseconds */
      timer0_delay_mus( 200 );
      timer0_delay_mus( 200 );

      break;

    default:
      present = FALSE;
      break;
    }

  return present;
}

/* ------------------------------------------------------------------------ */

static BYTE ds2405_read_byte( void )
{
  BYTE i, byt = 0;
  switch( OneWireBus )
    {
    case 0:
      for( i=0; i<8; ++i )
        {
          /* Make space for the next bit by shifting the rest down */
          byt >>= 1;

          DS2405_1_TO_OUTPUT();
          DS2405_1_CLEAR();

          /* Delay of <1 (6) microseconds */
          NOP();NOP();

          DS2405_1_TO_INPUT();
          DS2405_1_SET();   /* Enable pull-up */

          /* Delay of 5 (9) microseconds */
          DELAY_9MUS()

          /* LSB first */
          if( DS2405_1_HIGH() ) byt |= 0x80;

          /* Delay of 60 (55) microseconds */
          timer0_delay_mus( 60 );
        }
      break;

    case 1:
      for( i=0; i<8; ++i )
        {
          /* Make space for the next bit by shifting the rest down */
          byt >>= 1;

          DS2405_2_TO_OUTPUT();
          DS2405_2_CLEAR();

          /* Delay of <1 (6) microseconds */
          NOP();NOP();

          DS2405_2_TO_INPUT();
          DS2405_2_SET();   /* Enable pull-up */

          /* Delay of 5 (9) microseconds */
          DELAY_9MUS()

          /* LSB first */
          if( DS2405_2_HIGH() ) byt |= 0x80;

          /* Delay of 60 (55) microseconds */
          timer0_delay_mus( 60 );
        }
      break;

    case 2:
      for( i=0; i<8; ++i )
        {
          /* Make space for the next bit by shifting the rest down */
          byt >>= 1;

          DS2405_3_TO_OUTPUT();
          DS2405_3_CLEAR();

          /* Delay of <1 (6) microseconds */
          NOP();NOP();

          DS2405_3_TO_INPUT();
          DS2405_3_SET();   /* Enable pull-up */

          /* Delay of 5 (9) microseconds */
          DELAY_9MUS()

          /* LSB first */
          if( DS2405_3_HIGH() ) byt |= 0x80;

          /* Delay of 60 (55) microseconds */
          timer0_delay_mus( 60 );
        }
      break;

    case 3:
      for( i=0; i<8; ++i )
        {
          /* Make space for the next bit by shifting the rest down */
          byt >>= 1;

          DS2405_4_TO_OUTPUT();
          DS2405_4_CLEAR();

          /* Delay of <1 (6) microseconds */
          NOP();NOP();

          DS2405_4_TO_INPUT();
          DS2405_4_SET();   /* Enable pull-up */

          /* Delay of 5 (9) microseconds */
          DELAY_9MUS()

          /* LSB first */
          if( DS2405_4_HIGH() ) byt |= 0x80;

          /* Delay of 60 (55) microseconds */
          timer0_delay_mus( 60 );
        }
      break;

    default:
      break;
    }

  return byt;
}

/* ------------------------------------------------------------------------ */

static void ds2405_write_byte( BYTE byt )
{
  BYTE i;
  for( i=0; i<8; ++i )
    {
      /* LSB first */
      ds2405_write_bit( byt & 0x01 );
      byt >>= 1;
    }
}

/* ------------------------------------------------------------------------ */

static void ds2405_write_bit( BYTE bit )
{
  switch( OneWireBus )
    {
    case 0:
      DS2405_1_TO_OUTPUT();
      DS2405_1_CLEAR();
      if( bit == 1 )
        {
          /* Write a 1 */
          /* Delay of 5 (6) microseconds */
          DELAY_9MUS();
          DS2405_1_TO_INPUT();
          DS2405_1_SET();   /* Enable pull-up */
          /* Delay of 65 (64) microseconds */
          timer0_delay_mus( 65 );
        }
      else
        {
          /* Write a 0 */
          /* Delay of 70 (60) microseconds */
          timer0_delay_mus( 70 );
          DS2405_1_TO_INPUT();
          DS2405_1_SET();   /* Enable pull-up */
          /* Delay of 5 (10) microseconds */
          DELAY_9MUS();
        }
      break;

    case 1:
      DS2405_2_TO_OUTPUT();
      DS2405_2_CLEAR();
      if( bit == 1 )
        {
          /* Write a 1 */
          /* Delay of 5 (6) microseconds */
          DELAY_9MUS();
          DS2405_2_TO_INPUT();
          DS2405_2_SET();   /* Enable pull-up */
          /* Delay of 65 (64) microseconds */
          timer0_delay_mus( 65 );
        }
      else
        {
          /* Write a 0 */
          /* Delay of 70 (60) microseconds */
          timer0_delay_mus( 70 );
          DS2405_2_TO_INPUT();
          DS2405_2_SET();   /* Enable pull-up */
          /* Delay of 5 (10) microseconds */
          DELAY_9MUS();
        }
      break;

    case 2:
      DS2405_3_TO_OUTPUT();
      DS2405_3_CLEAR();
      if( bit == 1 )
        {
          /* Write a 1 */
          /* Delay of 5 (6) microseconds */
          DELAY_9MUS();
          DS2405_3_TO_INPUT();
          DS2405_3_SET();   /* Enable pull-up */
          /* Delay of 65 (64) microseconds */
          timer0_delay_mus( 65 );
        }
      else
        {
          /* Write a 0 */
          /* Delay of 70 (60) microseconds */
          timer0_delay_mus( 70 );
          DS2405_3_TO_INPUT();
          DS2405_3_SET();   /* Enable pull-up */
          /* Delay of 5 (10) microseconds */
          DELAY_9MUS();
        }
      break;

    case 3:
      DS2405_4_TO_OUTPUT();
      DS2405_4_CLEAR();
      if( bit == 1 )
        {
          /* Write a 1 */
          /* Delay of 5 (6) microseconds */
          DELAY_9MUS();
          DS2405_4_TO_INPUT();
          DS2405_4_SET();   /* Enable pull-up */
          /* Delay of 65 (64) microseconds */
          timer0_delay_mus( 65 );
        }
      else
        {
          /* Write a 0 */
          /* Delay of 70 (60) microseconds */
          timer0_delay_mus( 70 );
          DS2405_4_TO_INPUT();
          DS2405_4_SET();   /* Enable pull-up */
          /* Delay of 5 (10) microseconds */
          DELAY_9MUS();
        }
      break;

    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

static BYTE ds2405_search_bit( void )
{
  BYTE bit, bit_c, result;

  switch( OneWireBus )
    {
    case 0:
      /* Read bit */
      DS2405_1_TO_OUTPUT();
      DS2405_1_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_1_TO_INPUT();
      DS2405_1_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_1_HIGH() )
        bit = 1;
      else
        bit = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );

      /* Read bit complement */
      DS2405_1_TO_OUTPUT();
      DS2405_1_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_1_TO_INPUT();
      DS2405_1_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_1_HIGH() )
        bit_c = 1;
      else
        bit_c = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );
      break;

    case 1:
      /* Read bit */
      DS2405_2_TO_OUTPUT();
      DS2405_2_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_2_TO_INPUT();
      DS2405_2_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_2_HIGH() )
        bit = 1;
      else
        bit = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );

      /* Read bit complement */
      DS2405_2_TO_OUTPUT();
      DS2405_2_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_2_TO_INPUT();
      DS2405_2_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_2_HIGH() )
        bit_c = 1;
      else
        bit_c = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );
      break;

    case 2:
      /* Read bit */
      DS2405_3_TO_OUTPUT();
      DS2405_3_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_3_TO_INPUT();
      DS2405_3_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_3_HIGH() )
        bit = 1;
      else
        bit = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );

      /* Read bit complement */
      DS2405_3_TO_OUTPUT();
      DS2405_3_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_3_TO_INPUT();
      DS2405_3_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_3_HIGH() )
        bit_c = 1;
      else
        bit_c = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );
      break;

    case 3:
      /* Read bit */
      DS2405_4_TO_OUTPUT();
      DS2405_4_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_4_TO_INPUT();
      DS2405_4_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_4_HIGH() )
        bit = 1;
      else
        bit = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );

      /* Read bit complement */
      DS2405_4_TO_OUTPUT();
      DS2405_4_CLEAR();
      /* Delay of <1 (6) microseconds */
      NOP();NOP();
      DS2405_4_TO_INPUT();
      DS2405_4_SET();   /* Enable pull-up */
      DELAY_9MUS();
      if( DS2405_4_HIGH() )
        bit_c = 1;
      else
        bit_c = 0;
      /* Delay of 60 (55) microseconds */
      timer0_delay_mus( 60 );
      break;

    default:
      bit = 1; bit_c = 1;
      break;
    }

  /* What did we find ? */
  if( bit == bit_c )
    {
      if( bit == 0 )
        result = DS2405_DUAL;
      else
        result = DS2405_NONE;
    }
  else
    {
      if( bit == 0 )
        result = DS2405_ZERO;
      else
        result = DS2405_ONE;
    }
  return result;
}

/* ------------------------------------------------------------------------ */
#ifdef _DS2405_FUNCTIONS_
static void ds2405_to_input( void )
{
  if( OneWireBus == 0 )      DS2405_1_TO_INPUT();
  else if( OneWireBus == 1 ) DS2405_2_TO_INPUT();
  else if( OneWireBus == 2 ) DS2405_3_TO_INPUT();
  else if( OneWireBus == 3 ) DS2405_4_TO_INPUT();
}

/* ------------------------------------------------------------------------ */

static void ds2405_to_output( void )
{
  if( OneWireBus == 0 )      DS2405_1_TO_OUTPUT();
  else if( OneWireBus == 1 ) DS2405_2_TO_OUTPUT();
  else if( OneWireBus == 2 ) DS2405_3_TO_OUTPUT();
  else if( OneWireBus == 3 ) DS2405_4_TO_OUTPUT();
}

/* ------------------------------------------------------------------------ */

static BOOL ds2405_high( void )
{
  /* Return the level of the currently selected 1-Wire bus */
  if( OneWireBus == 0 )      return DS2405_1_HIGH();
  else if( OneWireBus == 1 ) return DS2405_2_HIGH();
  else if( OneWireBus == 2 ) return DS2405_3_HIGH();
  else if( OneWireBus == 3 ) return DS2405_4_HIGH();
  return 0;
}

/* ------------------------------------------------------------------------ */

static void ds2405_set( void )
{
  if( OneWireBus == 0 )      DS2405_1_SET();
  else if( OneWireBus == 1 ) DS2405_2_SET();
  else if( OneWireBus == 2 ) DS2405_3_SET();
  else if( OneWireBus == 3 ) DS2405_4_SET();
}

/* ------------------------------------------------------------------------ */

static void ds2405_clear( void )
{
  if( OneWireBus == 0 )      DS2405_1_CLEAR();
  else if( OneWireBus == 1 ) DS2405_2_CLEAR();
  else if( OneWireBus == 2 ) DS2405_3_CLEAR();
  else if( OneWireBus == 3 ) DS2405_4_CLEAR();
}
#endif /* _DS2405_FUNCTIONS_ */
/* ------------------------------------------------------------------------ */
