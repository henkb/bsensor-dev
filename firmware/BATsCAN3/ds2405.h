/* ------------------------------------------------------------------------
File   : ds2405.h

Descr  : Include file for ds2405.c.

History: 30DEC.02; Henk B&B; Definition.
         20JUN.08; Henk B&B; Define READROM command as 0x33 to be compatible
                             with the DS2405.
         26AUG.08; Henk B&B; Added DS2405_MATCHROM_CMD and ds2405_match().
         28NOV.08; Henk B&B; Extend to 4 different (strings of) ID-chip(s),
                             i.e. 1-Wire I/Os (connected to the MDT-DCS module
                             I/Os of the "B-sensor #0" connector);
                             added an output to provide a pull-up to V+
                             in combination with an added (4K7) resistor from
                             each of the four 1-Wire I/Os to this output,
                             configured by calling DS2405_PULLUP().
         04OCT.09; Henk B&B; Single string version for BATCAN
                             (but keep definitions for string #1 to #3).
         04DEC.09; Henk B&B; 4-string version for BATCAN as well as mBATCAN
                             with pin placements for mBATCAN.
         28NOV;12; Henk B&B; Added support for the DS2413.
--------------------------------------------------------------------------- */

#ifndef DS2405_H
#define DS2405_H

/* ------------------------------------------------------------------------ */

#define DS2401_FAMILY_CODE          0x01
#define DS2405_FAMILY_CODE          0x05
#define DS2413_FAMILY_CODE          0x3A

#define DS2405_READROM_CMD          0x33
#define DS2405_MATCHROM_CMD         0x55
#define DS2405_SEARCHROM_CMD        0xF0
#define DS2405_SEARCHROM_ACTIVE_CMD 0xEC

#define DS2413_PIO_READ_CMD         0xF5
#define DS2413_PIO_WRITE_CMD        0x5A

#define DS2405_BYTES                8
#define DS2405_BITS                 64

/* Search-ROM results */
#define DS2405_ZERO                 0
#define DS2405_ONE                  1
#define DS2405_DUAL                 2
#define DS2405_NONE                 3

/* ------------------------------------------------------------------------ */
/* Configuration */

/* The One-Wire basic bit operations on the first (string of) ID chip(s) */
#define DS2405_1_TO_INPUT()         CLEARBIT( DDRB, 4 )
#define DS2405_1_TO_OUTPUT()        SETBIT( DDRB, 4 )
#define DS2405_1_HIGH()             (PINB & BIT(4))
#define DS2405_1_SET()              SETBIT( PORTB, 4 )
#define DS2405_1_CLEAR()            CLEARBIT( PORTB, 4 )

/* The One-Wire basic bit operations on the second (string of) ID chip(s) */
#define DS2405_2_TO_INPUT()         CLEARBIT( DDRB, 7 )
#define DS2405_2_TO_OUTPUT()        SETBIT( DDRB, 7 )
#define DS2405_2_HIGH()             (PINB & BIT(7))
#define DS2405_2_SET()              SETBIT( PORTB, 7 )
#define DS2405_2_CLEAR()            CLEARBIT( PORTB, 7 )

/* The One-Wire basic bit operations on the third (string of) ID chip(s) */
#define DS2405_3_TO_INPUT()         CLEARBIT( DDRE, 6 )
#define DS2405_3_TO_OUTPUT()        SETBIT( DDRE, 6 )
#define DS2405_3_HIGH()             (PINE & BIT(6))
#define DS2405_3_SET()              SETBIT( PORTE, 6 )
#define DS2405_3_CLEAR()            CLEARBIT( PORTE, 6 )

/* The One-Wire basic bit operations on the fourth (string of) ID chip(s) */
#define DS2405_4_TO_INPUT()         CLEARBIT( DDRC, 7 )
#define DS2405_4_TO_OUTPUT()        SETBIT( DDRC, 7 )
#define DS2405_4_HIGH()             (PINC & BIT(7))
#define DS2405_4_SET()              SETBIT( PORTC, 7 )
#define DS2405_4_CLEAR()            CLEARBIT( PORTC, 7 )

/* ------------------------------------------------------------------------ */
/* Function prototypes */

void ds2405_select     ( BYTE bus_nr );
BYTE ds2405_crc        ( BYTE id[DS2405_BYTES] );

void ds2405_search_init( void );
BOOL ds2405_search_next( BOOL active, BYTE *id );

BOOL ds2405_toggle     ( BYTE *id, BYTE *output_val );
BOOL ds2413_toggle     ( BYTE *id, BYTE *output_val );
BOOL ds2413_set        ( BYTE *id, BYTE val, BYTE *output_stat );

/* ------------------------------------------------------------------------ */
#endif /* DS2405_H */
