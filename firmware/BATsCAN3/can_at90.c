/* ------------------------------------------------------------------------
File   : can_at90.c

Descr  : Functions for operating an AT90CANxxx CAN-controller.

History: 09MAR.08; Henk B&B; Start development.
--------------------------------------------------------------------------- */

#include "general.h"
#include "can.h"
#include "eeprom.h"
#include "store.h"
#include "timer.h"

/* CANopen state of this node (declared in the C-file containing main()) */
extern BYTE NodeState;

/* ------------------------------------------------------------------------ */
/* CAN message buffering in RAM */

/* Number of message buffers (here must be a power of 2!) */
#define CAN_BUFS        64
#define CAN_BUFS_MASK   CAN_BUFS-1
/* Size of message buffer */
#define CAN_BUF_SIZE    11

/* Indices into the individual buffers */
#define MSG_DATA_I      0
#define MSG_DLC_I       8
#define MSG_OBJECT_I    9
#define MSG_VALID_I     10

/* Buffer values:
   'message-present' byte in location MSG_VALID_I:
   0x00 means 'message present', 0xFF means 'no message present'.
   To tolerate up to 2 bitflips:
   bits-in-byte <= 2: message
   bits-in-byte >= 6: no message */
#define BUF_EMPTY       0xFF
#define BUF_NOT_EMPTY   0x00

/* Array of CAN message buffers */
static BYTE CanMsgBuf[CAN_BUFS][CAN_BUF_SIZE];

static BOOL CanBufFull;

static BYTE CanMsgRecvCnt;

/* Parameters which are being used according to a majority voting mechanism:
   MsgOutIndex_ : index of the first-to-be-handled CAN-message buffer,
   MsgCounter_: number of unhandled CAN-messages in buffers */
static BYTE MsgOutIndex1, MsgOutIndex2, MsgOutIndex3;
static BYTE MsgCounter1,  MsgCounter2,  MsgCounter3;

/* NB: make sure to disable the CAN INT interrupt before writing or reading
   anything to/from the CAN-controller in the main program loop !
   (and also when updating the buffer counter in the main loop) */

/* ------------------------------------------------------------------------ */
/* CAN-controller message object descriptors */

/* Settings for NMT, SDO-tx/SDO-rx, Emergency,
   Bootup/Nodeguard, SYNC, PDOs, etc.
   (according to the CANopen Predefined Connection Set);
   in the following array is space for all available message buffers;
   NB: make sure this list matches the message/buffer numbering in can.h ! */

const BYTE MOB_SETTING[CC_NO_OF_MOBS][2] =
{
  /* 0: NMT */
  { NMT_OBJ,       CC_MOB_RECEIVE  | CC_NMT_LEN },

  /* 1: SYNC */
  { SYNC_OBJ,      CC_MOB_RECEIVE  | CC_SYNC_LEN },

  /* 2: EMERGENCY */
  { EMERGENCY_OBJ, CC_MOB_TRANSMIT | CC_EMERGENCY_LEN },

  /* 3: SDO-Transmit */
  { SDOTX_OBJ,     CC_MOB_TRANSMIT | CC_SDOTX_LEN },

  /* 4: SDO-Receive */
  { SDORX_OBJ,     CC_MOB_RECEIVE  | CC_SDORX_LEN },

  /* 5: Bootup/Nodeguard */
  { NODEGUARD_OBJ, CC_MOB_TRANSMIT | CC_NODEGUARD_LEN },

  /* 6: Nodeguard RTR */
  { NODEGUARD_OBJ, CC_MOB_RECEIVE  | CC_NODEGUARD_LEN },

  /* 7: Transmit-PDO4 */
  { TPDO4_OBJ,     CC_MOB_TRANSMIT | CC_TPDO4_LEN },

  /* 8: Transmit-PDO4 RTR */
  { TPDO4_OBJ,     CC_MOB_RECEIVE  | CC_TPDO4_LEN },

  /* 9-14: used as alternative transmit buffers
     (for when any of the above transmit buffers is still in use) */
  { 0x00,          CC_MOB_DISABLE },
  { 0x00,          CC_MOB_DISABLE },
  { 0x00,          CC_MOB_DISABLE },
  { 0x00,          CC_MOB_DISABLE },
  { 0x00,          CC_MOB_DISABLE },
  { 0x00,          CC_MOB_DISABLE }
};

/* ------------------------------------------------------------------------ */
/* Globals */

BYTE        NodeID;      /* (stored in EEPROM) */

BYTE        CANopenErrorReg;

/* Counter for errors in the CAN communication */
static BYTE CanErrorCntr;

/* Boolean to enable 'autostart' (automatically goto Operational mode) */
static BOOL CANopenOpStateInit; /* (copy in EEPROM) */

/* Max counter value for bus access retries after Bus-off events */
static BYTE CanBusOffMaxCnt;    /* (copy in EEPROM) */

/* Bus-off interrupt counter */
BYTE        CanBusOffCnt = 0;

/* SERG, CERG, FERG, AERG interrupt counters */
BYTE        SergCnt = 0;
BYTE        CergCnt = 0;
BYTE        FergCnt = 0;
BYTE        AergCnt = 0;

/* Toggle bit for the Emergency CAN-message */
static BYTE CanEmgToggle = 0x80;

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BYTE get_buf_index     ( void );
static BYTE get_buf_cntr      ( void );
static BYTE bits_in_byte      ( BYTE val );

static void can_load_config   ( void );

/* ------------------------------------------------------------------------ */

void can_init( BOOL init_msg_buffer )
{
  BYTE id_hi, id_lo;
  BYTE mobno;

  /* Disable interrupt */
  CC_INTRPT_DISABLE();

  /* Set controller to standby */
  CC_STANDBY();

  /* Wait for controller to be standby */
  while( CC_ENABLED() );

  /* Reset the controller */
  CC_RESET();

  /* Initialise configuration parameters */
  can_load_config();

  if( init_msg_buffer )
    {
      /* Reset error counter */
      CanErrorCntr = 0;

      /* Initialize CANopen Error Register Object (Object 0x1001) */
      CANopenErrorReg = 0x00;
    }
  else
    {
      /* Reset CAN communication error bit */
      CANopenErrorReg &= ~ERRREG_COMMUNICATION;
    }

  /* Read the Node-ID from EEPROM or use 63 */
  NodeID = eepromw_read( (UINT16) 0x107 );
  if( NodeID > 127 ) NodeID = 31;

  /* Node-ID for MOB is split over 2 bytes */
  id_hi = NodeID >> 3;
  id_lo = NodeID << 5;

  /* Configure the Message Objects (MOBs) */
  for( mobno=0; mobno<CC_NO_OF_MOBS; ++mobno )
    {
      BYTE canidt1, canidt2;

      canidt1  = MOB_SETTING[mobno][0];
      canidt2  = 0x00;

      /* NMT and SYNC are broadcast messages: Node-ID is not in */
      if( mobno != CC_NMT && mobno != CC_SYNC )
        {
          /* Node-ID is included in the COB-ID */
          canidt1 |= id_hi;
          canidt2 |= id_lo;
        }

      /* Select MOB */
      CANPAGE  = (mobno << 4);

      CANCDMOB = CC_MOB_DISABLE;
      CANSTMOB = 0x00;

      /* Set CAN identifier registers */
      CANIDT1 = canidt1;
      CANIDT2 = canidt2;
      CANIDT3 = 0x00;
      CANIDT4 = 0x00;

      /* Set CAN identifier mask registers */
      CANIDM1 = 0xFF;
      CANIDM2 = 0xE0;
      CANIDM3 = 0x00;
      CANIDM4 = 0x00;
    }

  /* Configure the CAN bit timing for 125 Kbit/s */
  CANBT1 = CC_CANBT1_125K;
  CANBT2 = CC_CANBT2_125K;
  CANBT3 = CC_CANBT3_125K;

  /* Enable MOB interrupts */
  CANIE2 = 0xFF;
  CANIE1 = 0x7F;

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_NODEID ) != NodeID )
    eeprom_write( EE_NODEID, NodeID );
#endif /* __VARS_IN_EEPROM__ */

  if( init_msg_buffer )
    {
      BYTE bufno;
      /* Initialize the CAN-message buffer and management stuff */
      for( bufno=0; bufno<CAN_BUFS; ++bufno )
        CanMsgBuf[bufno][MSG_VALID_I] = BUF_EMPTY;
      CanBufFull = FALSE;
      MsgOutIndex1 = 0; MsgOutIndex2 = 0; MsgOutIndex3 = 0;
      MsgCounter1  = 0; MsgCounter2  = 0; MsgCounter3  = 0;
    }

  /* Start waiting for messages to be received */
  for( mobno=0; mobno<CC_NO_OF_MOBS; ++mobno )
    {
      BYTE cancdmob;
      CANPAGE  = (mobno << 4);
      cancdmob = MOB_SETTING[mobno][1];
      if( (cancdmob & CC_CONMOB_MASK) == CC_MOB_RECEIVE )
        {
          CANCDMOB = CC_MOB_DISABLE;
          CANCDMOB = cancdmob;
        }
    }

  /* Enable interrupt */
  CC_INTRPT_ENABLE();

  /* Set controller to enabled */
  CC_ENABLE();

  CanMsgRecvCnt = 0;
}

/* ------------------------------------------------------------------------ */

BOOL can_msg_available( void )
{
  BOOL available;

  /* Need undisturbed access to buffer management variables !
     (because even the get_buf_xxx() functions may alter variables,
     due to the (self-correcting) majority voting mechanism) */
  CC_INTRPT_DISABLE();

  /* CAN-message available in buffer ? */
  available = (get_buf_cntr() > 0);

  CC_INTRPT_ENABLE();

#ifdef __VARS_IN_EEPROM__
  /* EEPROM access removed from CAN interrupt handling, do it here instead ! */
  if( !available )
    {
      NodeID = eeprom_read( EE_NODEID );
    }
#endif /* __VARS_IN_EEPROM__ */

  /* Switch on the 'OKAY' LED (may blink due to CAN activity) */
  if( timer2_timeout(LED_CONTROL) ) LED_GREEN_ON();

  return available;
}

/* ------------------------------------------------------------------------ */

BYTE can_read( BYTE *pdlc, BYTE **ppmsg_data )
{
  BYTE cntr, object_no;

  /* Need undisturbed access to buffer management variables !
     (because even the get_buf_xxx() functions may alter variables,
     due to the (self-correcting) majority voting mechanism) */
  CC_INTRPT_DISABLE();

  cntr = get_buf_cntr();

  /* Is any message available in the buffer ? */
  if( cntr > 0 )
    {
      BYTE index, *msg;

      index     = get_buf_index();
      msg       = CanMsgBuf[index];
      *pdlc     = msg[MSG_DLC_I];
      object_no = msg[MSG_OBJECT_I];

      /* We don't make a copy of the data in the buffer
         (so make sure it does not get overwritten: see 'canint_handler()');
         return a pointer that points to the databytes in the message buffer */
      *ppmsg_data = msg;

      /* Make sure the message in the buffer is valid;
         skip any 'empty' buffers (although this should be very unlikely...) */
      if( bits_in_byte(msg[MSG_VALID_I]) >= 6 )
        {
          /* Not valid..., skip this message... */
          object_no = NO_OBJECT;
        }

      /* Mark buffer as empty
         (NB: the contained message has not been handled yet!) */
      msg[MSG_VALID_I] = BUF_EMPTY;

      /* Decrement the messages-in-buffer counter */
      --cntr;
      MsgCounter1 = cntr;  MsgCounter2 = cntr;  MsgCounter3 = cntr;

      /* Increment the buffer(-out) index to point to
         the next message to be handled (next time around) */
      index = (index + 1) & CAN_BUFS_MASK;
      MsgOutIndex1 = index;  MsgOutIndex2 = index;  MsgOutIndex3 = index;

      ++CanMsgRecvCnt;
    }
  else
    {
      object_no = NO_OBJECT;
    }

  CC_INTRPT_ENABLE();

  /* Blink LED to indicate (receive) activity;
     LED is switched back on by can_msg_available() */
  if( timer2_timeout(LED_CONTROL) ) /* Make sure it blinks */
    {
      /* Make sure it blinks */
      if( LED_GREEN_IS_OFF() ) LED_GREEN_ON();
      else LED_GREEN_OFF();
      timer2_set_timeout_10ms( LED_CONTROL, 12 );
    }

  return object_no;;
}

/* ------------------------------------------------------------------------ */

void can_write( BYTE object_no, BYTE len, BYTE *msg_data )
{
  BYTE byt;

  /* Legal message object ? */
  if( object_no > CC_NO_OF_MOBS-1 ) return;

  CC_INTRPT_DISABLE(); /* Need undisturbed access to CAN-controller ! */

  if( can_busy(object_no) == FALSE )
    {
      /* Select MOB */
      CANPAGE = (object_no << 4);

      /* Write the data bytes to the message buffer */
      for( byt=0; byt<len; ++byt ) CANMSG = msg_data[byt];

      /* Trigger the transmission of this message */
      CANCDMOB = CC_MOB_DISABLE;
      CANCDMOB = MOB_SETTING[object_no][1];
    }
  else
    {
      /* Find a free Message Object (MOB) for this transmission
         (possibly we don't find any, but this should be very unlikely..) */
      BYTE mobno;
      for( mobno=CC_TRANSMIT; mobno<CC_NO_OF_MOBS; ++mobno )
        {
          BYTE canidt1, canidt2;

          if( can_busy(mobno) ) continue;

          canidt1  = MOB_SETTING[object_no][0];
          canidt2  = 0x00;

          /* Node-ID is included in the COB-ID */
          canidt1 |= (NodeID >> 3);
          canidt2 |= (NodeID << 5);

          /* Select MOB */
          CANPAGE  = (mobno << 4);

          CANSTMOB = 0x00;

          /* Set CAN identifier registers, rest is already set okay */
          CANIDT1 = canidt1;
          CANIDT2 = canidt2;

          /* Write the data bytes to the message buffer */
          for( byt=0; byt<len; ++byt ) CANMSG = msg_data[byt];

          /* Trigger the transmission of this message */
          CANCDMOB = CC_MOB_DISABLE;
          CANCDMOB = MOB_SETTING[object_no][1];

          /* Break out of this for-loop */
          break;
        }
    }

  CC_INTRPT_ENABLE();

  /* Blink LED to indicate (send) activity;
     LED is switched back on by can_msg_available() */
  if( timer2_timeout(LED_CONTROL) ) /* Make sure it blinks */
    {
      /* Make sure it blinks */
      if( LED_GREEN_IS_OFF() ) LED_GREEN_ON();
      else LED_GREEN_OFF();
      timer2_set_timeout_10ms( LED_CONTROL, 12 );
    }
}

/* ------------------------------------------------------------------------ */

void can_write_bootup( void )
{
  BYTE can_data[CC_BOOTUP_LEN];

  /* Send the CANopen Bootup message */
  can_data[0] = 0;
  can_write( CC_BOOTUP, CC_BOOTUP_LEN, can_data );
}

/* ------------------------------------------------------------------------ */

void can_write_emergency( BYTE err_low,
                          BYTE err_high,
                          BYTE mfct_field_0,
                          BYTE mfct_field_1,
                          BYTE mfct_field_2,
                          BYTE mfct_field_3,
                          BYTE canopen_err_bit )
{
  BYTE msg_data[8];

  /* CANopen error code */
  msg_data[0] = err_low;
  msg_data[1] = err_high;

  /* Update CANopen Error Register */
  CANopenErrorReg |= canopen_err_bit;

  /* Add CANopen Error Register (OD object 0x1001) to message */
  msg_data[2] = CANopenErrorReg;

  /* Toggle the toggle bit */
  CanEmgToggle ^= 0x80;

  /* CANopen manufacturer specific error field */
  msg_data[3] = mfct_field_0;
  msg_data[4] = mfct_field_1;
  msg_data[5] = mfct_field_2;
  msg_data[6] = mfct_field_3;
  msg_data[7] = (CanEmgToggle & 0x80);

  /* Make sure the message does not get lost:
     wait for the previous one to be sent */
  {
    BYTE delay=0;
    while( can_busy(CC_EMERGENCY) && delay < 50 )
      {
        /* So wait, but not forever... */
        timer0_delay_ms( 1 );
        ++delay;
      }
  }

  can_write( CC_EMERGENCY, CC_EMERGENCY_LEN, msg_data );
}

/* ------------------------------------------------------------------------ */

void can_check_for_errors( void )
{
  if( CC_BUSOFF() )
    {
      if( CanBusOffCnt <= CanBusOffMaxCnt ) can_init( FALSE );
    }
  else
    {
      /* Check generated error interrupts etc. and report */
      if( CanErrorCntr > 0 )
        {
          /* NB: this value may overflow */
          BYTE format_errs = SergCnt + CergCnt + FergCnt + AergCnt;

          can_write_emergency( 0x00, 0x81, 0, format_errs,
                               CanErrorCntr, CanBusOffCnt,
                               ERRREG_COMMUNICATION );
          CanErrorCntr = 0;
        }

      if( (CanBufFull & TRUE) == TRUE )
        {
          /* Report it: 'CAN overrun' emergency type */
          can_write_emergency( 0x10, 0x81, 0, 0, 0, 0, ERRREG_COMMUNICATION );
          CanBufFull = FALSE;
        }
    }
}

/* ------------------------------------------------------------------------ */

BOOL can_busy( BYTE object_no )
{
  BOOL busy = FALSE;
  if( object_no < 8 )
    {
      if( CANEN2 & (1<<object_no) ) busy = TRUE;
    }
  else
    {
      object_no -= 8;
      if( CANEN1 & (1<<object_no) ) busy = TRUE;
    }
  return busy;
}

/* ------------------------------------------------------------------------ */

BYTE canopen_init_state( void )
{
  BYTE state;

#ifdef __VARS_IN_EEPROM__
  CANopenOpStateInit = eeprom_read( EE_CANOPEN_OPSTATE_INIT );
#endif /* __VARS_IN_EEPROM__ */

  if( CANopenOpStateInit )
    state = NMT_OPERATIONAL;
  else
    state = NMT_PREOPERATIONAL;

  return state;
}

/* ------------------------------------------------------------------------ */

BOOL can_set_opstate_init( BOOL enable )
{
  if( enable > 1 ) return FALSE;
  if( enable )
    CANopenOpStateInit = TRUE;
  else
    CANopenOpStateInit = FALSE;

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_CANOPEN_OPSTATE_INIT ) != CANopenOpStateInit )
    eeprom_write( EE_CANOPEN_OPSTATE_INIT, CANopenOpStateInit );
#endif /* __VARS_IN_EEPROM__ */
  return TRUE;
}

/* ------------------------------------------------------------------------ */

BOOL can_get_opstate_init( void )
{
#ifdef __VARS_IN_EEPROM__
  CANopenOpStateInit = eeprom_read( EE_CANOPEN_OPSTATE_INIT );
#endif /* __VARS_IN_EEPROM__ */

  return CANopenOpStateInit;
}

/* ------------------------------------------------------------------------ */

BOOL can_set_busoff_maxcnt( BYTE cnt )
{
  CanBusOffMaxCnt = cnt;

#ifdef __VARS_IN_EEPROM__
  if( eeprom_read( EE_CAN_BUSOFF_MAXCNT ) != CanBusOffMaxCnt )
    eeprom_write( EE_CAN_BUSOFF_MAXCNT, CanBusOffMaxCnt );
#endif /* __VARS_IN_EEPROM__ */

  return TRUE;
}

/* ------------------------------------------------------------------------ */

BYTE can_get_busoff_maxcnt( void )
{
#ifdef __VARS_IN_EEPROM__
  CanBusOffMaxCnt = eeprom_read( EE_CAN_BUSOFF_MAXCNT );
#endif /* __VARS_IN_EEPROM__ */

  return CanBusOffMaxCnt;
}

/* ------------------------------------------------------------------------ */

BYTE can_get_recvmsg_cnt( void )
{
  return CanMsgRecvCnt;
}

/* ------------------------------------------------------------------------ */

void can_get_formaterr_cnt( BYTE *byt )
{
  byt[0] = SergCnt;
  byt[1] = CergCnt;
  byt[2] = FergCnt;
  byt[3] = AergCnt;
}

/* ------------------------------------------------------------------------ */

/* Up to 16 bytes of configuration parameters can be stored */
#define CAN_STORE_SIZE 2

/* ------------------------------------------------------------------------ */

BOOL can_store_config( void )
{
  BYTE block[CAN_STORE_SIZE];

#ifdef __VARS_IN_EEPROM__
  CANopenOpStateInit = eeprom_read( EE_CANOPEN_OPSTATE_INIT );
  CanBusOffMaxCnt    = eeprom_read( EE_CAN_BUSOFF_MAXCNT );
#endif /* __VARS_IN_EEPROM__ */

  block[0] = CANopenOpStateInit;
  block[1] = CanBusOffMaxCnt;

  return( storage_write_block( STORE_CAN, CAN_STORE_SIZE, block ) );
}

/* ------------------------------------------------------------------------ */

static void can_load_config( void )
{
  BYTE block[CAN_STORE_SIZE];

  /* Preset block with parameter defaults */
  block[0] = TRUE;
  block[1] = 2;

  /* Read the configuration from EEPROM, if any
     (errors in reading this datablock are caught and
      reported by functions in store.c...) */
  storage_read_block( STORE_CAN, CAN_STORE_SIZE, block );

  CANopenOpStateInit = block[0];
  CanBusOffMaxCnt    = block[1];

#ifdef __VARS_IN_EEPROM__
  /* Create working copies of configuration globals in EEPROM */
  if( eeprom_read( EE_CANOPEN_OPSTATE_INIT ) != CANopenOpStateInit )
    eeprom_write( EE_CANOPEN_OPSTATE_INIT, CANopenOpStateInit );
  if( eeprom_read( EE_CAN_BUSOFF_MAXCNT ) != CanBusOffMaxCnt )
    eeprom_write( EE_CAN_BUSOFF_MAXCNT, CanBusOffMaxCnt );
#endif /* __VARS_IN_EEPROM__ */
}

/* ------------------------------------------------------------------------ */
/* CAN interrupt handler */

#pragma interrupt_handler canint_handler:iv_CAN

void canint_handler( void )
{
  /* The following interrupts are enabled
     (see CC_INTRPT_ENABLE() in cancon.h):
     ENRX, ENBOFF, ENERG,
     or (in other words)
     MOB receive interrupt, Bus Off interrupt, General Errors interrupt
     in which the General Errors can be subdivided in:
     SERG, CERG, FERG, AERG
     or (in other words)
     Stuff Error, CRC Error, Form Error, Acknowledgement Error
  */
  BYTE cangit, canhpmob;

  /* Check the General Interrupt Register */
  cangit = CANGIT;
  if( cangit != 0 )
    {
      if( cangit & (BIT(BOFFIT)|BIT(SERG)|BIT(CERG)|BIT(FERG)|BIT(AERG)) )
        ++CanErrorCntr;
      if( cangit & BIT(BOFFIT) ) ++CanBusOffCnt;
      if( cangit & BIT(SERG) )   ++SergCnt;
      if( cangit & BIT(CERG) )   ++CergCnt;
      if( cangit & BIT(FERG) )   ++FergCnt;
      if( cangit & BIT(AERG) )   ++AergCnt;
      /* Reset all interrupt flags */
      CANGIT = 0x7F;
    }

  /* Check the CAN Status Interrupt MOB Registers, CANSIT2 (MOBs 0 to 7) and
     CANSIT1 (MOBs 8 to 14) for messages received.
     The CANHPMOB register is a more efficient way to do this.
     It has the format of the CANPAGE register, so can be used directly
     to address the proper Message Object (MOB); it reads value 0xF0 when
     no message is available */
  canhpmob = CANHPMOB;
  while( canhpmob != 0xF0 )
    {
      /* CAN message received: copy it to the CAN message buffer in RAM */
      BYTE object_no;
      BYTE cntr;
      BYTE index;
      BYTE *msg;
      BYTE dlc;

      object_no = canhpmob >> 4;

      /* Select the MOB */
      CANPAGE = canhpmob;

      cntr = get_buf_cntr();

      /* If buffer is full (keep one buffer 'free', it might be the one
         currently being processed by the application, and since the data
         bytes are not copied they should not be overwritten yet; also the
         buffer space is used for assembling a reply, by the SDO server),
         disable further interrupts to prevent overwriting message buffers
         (interrupt will get enabled again by the message handling) */
      if( cntr == CAN_BUFS-1 )
        {
          CC_INTRPT_DISABLE();

          /* A message is lost: get it reported; also because the interrupt
             has been disabled more messages might get lost! */
          CanBufFull = TRUE;

          /* Clear all interrupt flags pertaining to this MOB */
          CANSTMOB = 0x00;

          /* Re-enable this MOB to receive a next message */
          CANCDMOB = CC_MOB_DISABLE;
          CANCDMOB = MOB_SETTING[object_no][1];

          return;
        }

      /* Calculate index (of first empty buffer) from the 'done-reading-until'
         index (MsgOutIndex_) and the 'number-of-full-buffers' counter
         (MsgCounter_) parameters, which are stored in a fault-tolerant way
         (3 copies of each parameter, majority voting mechanism) */
      index = (get_buf_index() + cntr) & CAN_BUFS_MASK;

      /* Location to copy CAN message to */
      msg = CanMsgBuf[index];

      /* Get received DLC */
      dlc = CANCDMOB & 0xF;

      /* Is it a Remote Frame ? */
      if( (CANIDT4 & BIT(RTRTAG)) == 0 )
        {
          /* No, so read the data bytes from the MOB Data Message Register */
          for( index=0; index<dlc; ++index ) msg[index] = CANMSG;
        }

      /* Store Object ID, DLC and mark buffer as 'not empty' */
      msg[MSG_OBJECT_I] = object_no;
      msg[MSG_DLC_I]    = dlc;
      msg[MSG_VALID_I]  = BUF_NOT_EMPTY;

      /* Increment the CAN-message-in-buffer counter */
      ++cntr;
      MsgCounter1 = cntr;  MsgCounter2 = cntr;  MsgCounter3 = cntr;

      /* Clear all interrupt flags pertaining to this MOB */
      CANSTMOB = 0x00;

      /* Re-enable this MOB to receive a next message */
      CANCDMOB = CC_MOB_DISABLE;
      CANCDMOB = MOB_SETTING[object_no][1];

      /* Reread CANHPMOB for a next available message, if any */
      canhpmob = CANHPMOB;
    }
}

/* ------------------------------------------------------------------------ */

static BYTE get_buf_index( void )
{
  /* Majority voting */
  if( MsgOutIndex1 == MsgOutIndex2 ) MsgOutIndex3 = MsgOutIndex1;
  else if( MsgOutIndex1 == MsgOutIndex3 ) MsgOutIndex2 = MsgOutIndex1;
  else if( MsgOutIndex2 == MsgOutIndex3 ) MsgOutIndex1 = MsgOutIndex3;
  else
    {
      /* All 3 are different: do a majority vote on a bit-by-bit basis... */
      BYTE byt, bitmask, bits, i;
      byt     = 0x00; /* Start value */
      bitmask = 0x80; /* Start with MSB */
      for( i=0; i<8; ++i )
        {
          bits = 0;
          if( MsgOutIndex1 & bitmask ) ++bits;
          if( MsgOutIndex2 & bitmask ) ++bits;
          if( MsgOutIndex3 & bitmask ) ++bits;
          byt <<= 1; /* Shift value one bit up*/
          /* Bit is set or not */
          if( bits > 1 ) ++byt; /* 2 or 3 of the bits (majority) are set */
          bitmask >>= 1; /* Next bit to check */
        }
      /* Set the new MsgOutIndex1/2/3 value */
      MsgOutIndex1 = byt;  MsgOutIndex2 = byt;  MsgOutIndex3 = byt;
    }
  return MsgOutIndex1;
}

/* ------------------------------------------------------------------------ */

static BYTE get_buf_cntr( void )
{
  /* Majority voting */
  if( MsgCounter1 == MsgCounter2 ) MsgCounter3 = MsgCounter1;
  else if( MsgCounter1 == MsgCounter3 ) MsgCounter2 = MsgCounter1;
  else if( MsgCounter2 == MsgCounter3 ) MsgCounter1 = MsgCounter3;
  else
    {
      /* All 3 are different: do a majority vote on a bit-by-bit basis... */
      BYTE byt, bitmask, bits, i;
      byt     = 0x00; /* Start value */
      bitmask = 0x80; /* Start with MSB */
      for( i=0; i<8; ++i )
        {
          bits = 0;
          if( MsgCounter1 & bitmask ) ++bits;
          if( MsgCounter2 & bitmask ) ++bits;
          if( MsgCounter3 & bitmask ) ++bits;
          byt <<= 1; /* Shift value one bit up*/
          if( bits > 1 ) ++byt; /* 2 or 3 of the bits (majority) are set */
          bitmask >>= 1; /* Next bit to check */
        }
      /* Set the new MsgCounter1/2/3 value */
      MsgCounter1 = byt;  MsgCounter2 = byt;  MsgCounter3 = byt;
    }
  return MsgCounter1;
}

/* ------------------------------------------------------------------------ */

static BYTE bits_in_byte( BYTE val )
{
  /* Calculate the number of bits set in a byte
     (method taken from www.devx.com, 'C++ tips'):
     Add up 4 pairs of 1-bit numbers (resulting in four 2-bit numbers):
     val = ((val >> 1) & 0x55) + (val & 0x55);
     Optimisation: save an AND (&) instruction by exploiting this clever
     relationship for each pair of bits: the two bits 'ab' represent the
     number 2a+b; to count the bits we subtract 'a' (i.e. 2a+b - a = a+b) */

  val = val - ((val >> 1) & 0x55);

  /* Add up four 2-bit numbers (resulting in two 4-bit numbers */
  val = ((val >> 2) & 0x33) + (val & 0x33);

  /* Add up two 4-bit numbers resulting in one 8-bit number */
  val = ((val >> 4) & 0x0F) + (val & 0x0F);

  return val;
}

/* ------------------------------------------------------------------------ */
