/* ------------------------------------------------------------------------
File   : ds2405.c

Descr  : Functions for operating the DALLAS DS2405 Serial Number chip
         with 1-wire interface.
	 Delays in microseconds according to the DS2405 datasheet 
	 recommendations, the number between brackets shows general 1-Wire
	 timing recommendations by manufacturer DALLAS.
	 (I don't know why these do not match...)

         From manufacturer doc:
	 The DS2405 enhanced Silicon Serial Number is a low-cost, electronic
	 registration number that provides an absolutely unique identity which
	 can be determined with a minimal electronic interface (typically,
	 a single port pin of a microcontroller).
	 The DS2405 consists of a factory-lasered, 64-bit ROM that includes
	 a unique 48-bit serial number, an 8-bit CRC, and an 8-bit Family Code
	 (01h). Data is transferred serially via the 1-Wire protocol
	 that requires only a single data lead and a ground return.
	 It operates at up to 16.3 kbits/s.
	 'Present Pulses' acknowledges when the reader first applies voltage.
	 Multiple DS2405s can reside on a common 1-Wire Net.
	 Power for reading and writing the device is derived from the data line
	 itself with no need for an external power source (zero standby power).

History: 30DEC.02; Henk B&B; Start of development.
         --JUN.08; Henk B&B; Support for DS2405; found that it has tighter
	                     timing, when testing; needed to get rid of
			     the function pointers;
			     added ds2405_search_single() and
			     ds2405_bit() functions.
	 26AUG.08; Henk B&B; Added ds2405_match() function (to toggle
	                     the DS2405 digital output, for test purposes).
	 09SEP.08; Henk B&B; Added ds2405_write_bit() and
	                     ds2405_search_bit() functions;
			     added ds2405_search_init() and
			     ds2405_search_next() functions;
			     removed ds2405_select() function (as there is
			     only one B-sensor, in principle).
	 28NOV.08; Henk B&B; Extend to 4 different (strings of) ID-chip(s),
	                     with selection on via global 'DsSelected'.
--------------------------------------------------------------------------- */

#include "general.h"
#include "ds2405.h"
#include "timer.h"

/* ------------------------------------------------------------------------ */
/* Globals */

/* Selected ID-chip 'bus number' */
static BYTE DsSelected;

/* For ID searches */
static BYTE BranchBitNo; /* At which bit there is a choice of 1 or 0 */
static BOOL LastFound;   /* Whether the last ID has been found */

/* ------------------------------------------------------------------------ */
/* Local prototypes */

static BOOL ds2405_init      ( void );
static BYTE ds2405_read_byte ( void );
static void ds2405_write_byte( BYTE byt );
static BYTE ds2405_bit       ( void );
static void ds2405_write_bit ( BYTE bit );
static BYTE ds2405_search_bit( void );

static void ds2405_to_input  ( void );
static void ds2405_to_output ( void );
static BOOL ds2405_high      ( void );
static void ds2405_set       ( void );
static void ds2405_clear     ( void );

/* ------------------------------------------------------------------------ */

BOOL ds2405_select( BYTE busno )
{
  DsSelected = busno;
  if( busno < 4 )
    return TRUE;
  else
    return FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_read( BYTE *ds2405_data )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the
     8-bits CRC from the DS2405; at 4 MHz this function takes about 8.5 ms */
  BOOL ints_enabled;
  BYTE i;
  BOOL result;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }
  else
    {
      ints_enabled = FALSE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_READROM_CMD );
      
      for( i=0; i<DS2405_BYTES; ++i )
	ds2405_data[i] = ds2405_read_byte();

      result = TRUE;
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

void ds2405_search_init( void )
{
  /* Get things ready for executing the "Search ROM" command
     repeatedly until all IDs have been found */
  BranchBitNo = 0;
  LastFound   = FALSE;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_search_next( BOOL active, BYTE *ds2405_data )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the 8-bits
     CRC from a next DS240x by executing the "Search ROM" command */
  BOOL ints_enabled;
  BOOL result;

  if( LastFound ) return FALSE;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }
  else
    {
      ints_enabled = FALSE;
    }

  result = TRUE;

  if( ds2405_init() == TRUE )
    {
      BYTE i, byte_i, bitmask, bitresult;
      BYTE last_zero = 0;

      if( active == TRUE )
	ds2405_write_byte( DS2405_SEARCHROM_ACTIVE_CMD );
      else
	ds2405_write_byte( DS2405_SEARCHROM_CMD );

      /* Search 64 bits */
      for( i=1; i<=DS2405_BITS; ++i )
	{
	  byte_i    = (i-1) / 8;
	  bitmask   = 1 << ((i-1)&7);
	  bitresult = ds2405_search_bit();
	  switch( bitresult )
	    {
	    case DS2405_ZERO:
	      ds2405_write_bit( 0 );
	      /* Clear ID bit */
	      ds2405_data[byte_i] &= ~bitmask;
	      break;

	    case DS2405_ONE:
	      ds2405_write_bit( 1 );
	      /* Set ID bit */
	      ds2405_data[byte_i] |= bitmask;
	      break;

	    case DS2405_DUAL:
	      /* Select the path to follow */
	      if( i == BranchBitNo )
		{
		  /* Latest branch (passed it before): this time follow 1 */
		  ds2405_write_bit( 1 );
		  /* Set ID bit */
		  ds2405_data[byte_i] |= bitmask;
		}
	      else if( i > BranchBitNo )
		{
		  /* Encountered a new branch: follow 0 */
		  ds2405_write_bit( 0 );
		  /* Clear ID bit */
		  ds2405_data[byte_i] &= ~bitmask;
		  /* Keep track of last branch where 0 was chosen
		     (i.e. it is an unfinished branch) */
		  last_zero = i;
		}
	      else if( i < BranchBitNo )
		{
		  /* 'Old' branch (passed it before):
		     follow the direction taken last time */
		  if( ds2405_data[byte_i] & bitmask )
		    {
		      ds2405_write_bit( 1 );
		    }
		  else
		    {
		      ds2405_write_bit( 0 );
		      /* Keep track of last branch where 0 was chosen
			 (i.e. it is an unfinished branch) */
		      last_zero = i;
		    }
		}
	      break;

	    case DS2405_NONE:
	    default:
	      result = FALSE;
	      break;
	    }
	}
      /* The last unfinished branch */
      BranchBitNo = last_zero;
      if( BranchBitNo == 0 ) LastFound = TRUE;
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  if( result == FALSE ) LastFound = TRUE;
  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_search_single( BYTE *ds2405_data )
{
  /* Reads the 8-bits family code, the 48-bits serial number and the 8-bits
     CRC from one single DS240x by executing the "Search ROM" command;
     ignores the fact that there may be more DS240x chips connected
     to the 1-Wire bus; ###function for test purposes### */
  BOOL ints_enabled;
  BYTE i;
  BOOL result;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }
  else
    {
      ints_enabled = FALSE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_SEARCHROM_CMD );

      /* Reset the ID bytes to all zeroes */
      for( i=0; i<DS2405_BYTES; ++i ) ds2405_data[i] = 0;

      /* Get 64 bits */
      for( i=0; i<DS2405_BITS; ++i )
	ds2405_data[i/8] |= (ds2405_bit() << (i&7));

      result = TRUE;
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BOOL ds2405_match( BYTE *ds2405_data, BYTE *out )
{
  /* Writes the ID data in ds2405_data[] to the DS2405, thereby causing
     a toggle of the output bit in case of a 'Match ROM';
     the 'out' byte is filled by executing one extra byte read
     which will result in 0xFF when the output is set to 1,
     and to 0x00 when the output is set to 0 */
  BOOL ints_enabled;
  BYTE i;
  BOOL result;

  /* If necessary, disable interrupts globally */
  if( SREG & 0x80 )
    {
      CLI();
      ints_enabled = TRUE;
    }
  else
    {
      ints_enabled = FALSE;
    }

  if( ds2405_init() == TRUE )
    {
      ds2405_write_byte( DS2405_MATCHROM_CMD );

      for( i=0; i<DS2405_BYTES; ++i ) ds2405_write_byte( ds2405_data[i] );

      *out = ds2405_read_byte();

      result = TRUE;
    }
  else
    {
      result = FALSE;
    }

  /* Reenable interrupts */
  if( ints_enabled ) SEI();

  return result;
}

/* ------------------------------------------------------------------------ */

BYTE ds2405_crc( BYTE *data )
{
  /* Calculate CRC-8 value of the first 7 bytes of DS2405 data
     (1 byte family code and 6 bytes serial number);
     uses The CCITT-8 polynomial, expressed as X^8 + X^5 + X^4 + 1 */
  BYTE crc = 0x00;
  BYTE index;
  BYTE b;
  for( index=0; index<DS2405_BYTES-1; ++index )
    {
      BYTE byt = data[index];
      for( b=0; b<8; ++b )
	{
	  if( (byt^crc) & 0x01 )
	    {
	      crc ^= 0x18;
	      crc >>= 1;
	      crc |= 0x80;
	    }
	  else
	    {
	      crc >>= 1;
	    }
	  byt >>= 1;
        }
    }
  return crc;
}

/* ------------------------------------------------------------------------ */

static BOOL ds2405_init( void )
{
  BOOL present;

  ds2405_to_output();
  ds2405_clear();

  // Delay of 500 (480) microseconds
  timer2_delay_mus( 250 );
  timer2_delay_mus( 250 );

  ds2405_to_input();
  ds2405_set();   // Enable pull-up

  // Delay of 100 (70) microseconds
  timer2_delay_mus( 70 );

  // If a DS2405 is present it will pull the line low
  if( ds2405_high() )
    present = FALSE;
  else
    present = TRUE;

  // Delay of 400 (410) microseconds
  timer2_delay_mus( 200 );
  timer2_delay_mus( 200 );

  return present;
}

/* ------------------------------------------------------------------------ */

static BYTE ds2405_read_byte( void )
{
  BYTE i, byt = 0;
  for( i=0; i<8; ++i )
    {
      // Make space for the next bit by shifting the rest down
      byt >>= 1;

      ds2405_to_output();
      ds2405_clear();

      // Delay of <1 (6) microseconds
      NOP();NOP();

      ds2405_to_input();
      ds2405_set();   // Enable pull-up

      // Delay of 5 (9) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
	BYTE tick;
	for( tick=0; tick<4; ++tick );
      }

      // LSB first
      if( ds2405_high() ) byt |= 0x80;

      // Delay of 60 (55) microseconds
      timer2_delay_mus( 60 );
    }
  return byt;
}

/* ------------------------------------------------------------------------ */

static void ds2405_write_byte( BYTE byt )
{
  BYTE i;
  for( i=0; i<8; ++i )
    {
      // LSB first
      ds2405_write_bit( byt & 0x01 );
      byt >>= 1;
    }
}

/* ------------------------------------------------------------------------ */

static BYTE ds2405_bit( void )
{
  BYTE result, bit;
  result = ds2405_search_bit();
  if( result == DS2405_ZERO )
    bit = 0;
  else
    bit = 1;

  /* Write value of 'bit' to keep the ID-chip selected */
  ds2405_write_bit( bit );

  return bit;
}

/* ------------------------------------------------------------------------ */

static void ds2405_write_bit( BYTE bit )
{
  ds2405_to_output();
  ds2405_clear();
  if( bit == 1 )
    {
      // Write a 1
      // Delay of 5 (6) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
	BYTE tick;
	for( tick=0; tick<4; ++tick );
      }
      ds2405_to_input();
      ds2405_set();   // Enable pull-up
      // Delay of 65 (64) microseconds
      timer2_delay_mus( 65 );
    }
  else
    {
      // Write a 0
      // Delay of 70 (60) microseconds
      timer2_delay_mus( 70 );
      ds2405_to_input();
      ds2405_set();   // Enable pull-up
      // Delay of 5 (10) microseconds
      { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
	BYTE tick;
	for( tick=0; tick<4; ++tick );
      }
    }
}

/* ------------------------------------------------------------------------ */

static BYTE ds2405_search_bit( void )
{
  BYTE bit, bit_c, result;

  /* Read bit */
  ds2405_to_output();
  ds2405_clear();
  // Delay of <1 (6) microseconds
  NOP();NOP();
  ds2405_to_input();
  ds2405_set();   // Enable pull-up
  // Delay of 5 (9) microseconds
  { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
    BYTE tick;
    for( tick=0; tick<4; ++tick );
  }
  if( ds2405_high() )
    bit = 1;
  else
    bit = 0;
  // Delay of 60 (55) microseconds
  timer2_delay_mus( 60 );

  /* Read bit complement */
  ds2405_to_output();
  ds2405_clear();
  // Delay of <1 (6) microseconds
  NOP();NOP();
  ds2405_to_input();
  ds2405_set();   // Enable pull-up
  // Delay of 5 (9) microseconds
  { /* Ca. 1 plus 1 mus per increment @ 4MHz (measured) */
    BYTE tick;
    for( tick=0; tick<4; ++tick );
  }
  if( ds2405_high() )
    bit_c = 1;
  else
    bit_c = 0;
  // Delay of 60 (55) microseconds
  timer2_delay_mus( 60 );

  /* What did we find ? */
  if( bit == bit_c )
    {
      if( bit == 0 )
	result = DS2405_DUAL;
      else
	result = DS2405_NONE;
    }
  else
    {
      if( bit == 0 )
	result = DS2405_ZERO;
      else
	result = DS2405_ONE;
    }
  return result;
}

/* ------------------------------------------------------------------------ */

static void ds2405_to_input( void )
{
  switch( DsSelected )
    {
    case 0:
      DS2405_1_TO_INPUT();
      break;
    case 1:
      DS2405_2_TO_INPUT();
      break;
    case 2:
      DS2405_3_TO_INPUT();
      break;
    case 3:
      DS2405_4_TO_INPUT();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

static void ds2405_to_output( void )
{
  switch( DsSelected )
    {
    case 0:
      DS2405_1_TO_OUTPUT();
      break;
    case 1:
      DS2405_2_TO_OUTPUT();
      break;
    case 2:
      DS2405_3_TO_OUTPUT();
      break;
    case 3:
      DS2405_4_TO_OUTPUT();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

static BOOL ds2405_high( void )
{
  /* Return the status of the 1-wire of the currently selected DS chip */
  switch( DsSelected )
    {
    case 0:
      return DS2405_1_HIGH();
    case 1:
      return DS2405_2_HIGH();
    case 2:
      return DS2405_3_HIGH();
    case 3:
      return DS2405_4_HIGH();
    default:
      return 0;
    }
}

/* ------------------------------------------------------------------------ */

static void ds2405_set( void )
{
  switch( DsSelected )
    {
    case 0:
      DS2405_1_SET();
      break;
    case 1:
      DS2405_2_SET();
      break;
    case 2:
      DS2405_3_SET();
      break;
    case 3:
      DS2405_4_SET();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */

static void ds2405_clear( void )
{
  switch( DsSelected )
    {
    case 0:
      DS2405_1_CLEAR();
      break;
    case 1:
      DS2405_2_CLEAR();
      break;
    case 2:
      DS2405_3_CLEAR();
      break;
    case 3:
      DS2405_4_CLEAR();
      break;
    default:
      break;
    }
}

/* ------------------------------------------------------------------------ */
